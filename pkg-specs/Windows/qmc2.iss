; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{DB53A3CB-C0CA-494E-AFD4-11C8B66A7B67}
AppName=QMC2 - M.A.M.E./M.E.S.S. Catalog Launcher II
AppVerName=QMC2 0.34
AppPublisher=The QMC2 Development Team
AppPublisherURL=http://qmc2.arcadehits.net/wordpress
AppSupportURL=http://qmc2.arcadehits.net/wordpress
AppUpdatesURL=http://qmc2.arcadehits.net/wordpress
DefaultDirName={pf}\QMC2
DefaultGroupName=QMC2
AllowNoIcons=yes
LicenseFile=c:\projects\gpl-2.0.txt
OutputDir=c:\projects\InstallerOutput
OutputBaseFilename=qmc2-win32-0.34
Compression=lzma2/max
SolidCompression=yes
InfoAfterFile=c:\projects\qmc2\package\doc\install-en.rtf

[Languages]
Name: "English"; MessagesFile: "compiler:Default.isl"; InfoAfterFile: "c:\projects\qmc2\package\doc\install-en.rtf"
Name: "French"; MessagesFile: "compiler:Languages\French.isl"; InfoAfterFile: "c:\projects\qmc2\package\doc\install-en.rtf"
Name: "German"; MessagesFile: "compiler:Languages\German.isl"; InfoAfterFile: "c:\projects\qmc2\package\doc\install-de.rtf"
Name: "Polish"; MessagesFile: "compiler:Languages\Polish.isl"; InfoAfterFile: "c:\projects\qmc2\package\doc\install-en.rtf"
Name: "Portuguese"; MessagesFile: "compiler:Languages\Portuguese.isl"; InfoAfterFile: "c:\projects\qmc2\package\doc\install-en.rtf"

[Files]
Source: "c:\projects\qmc2\package\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\{cm:ProgramOnTheWeb,QMC2}"; Filename: "http://qmc2.arcadehits.net/wordpress"
Name: "{group}\{cm:UninstallProgram,QMC2}"; Filename: "{uninstallexe}"
Name: "{group}\QMC2 (M.A.M.E.)"; Filename: "{app}\qmc2-mame.exe"; WorkingDir: "{app}"; IconFilename: "{app}\data\img\mame.ico"
Name: "{group}\QMC2 (M.E.S.S.)"; Filename: "{app}\qmc2-mess.exe"; WorkingDir: "{app}"; IconFilename: "{app}\data\img\mess.ico"
