<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>About</name>
    <message>
        <location filename="../../about.cpp" line="120"/>
        <source>Version </source>
        <translation>Version </translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="124"/>
        <source>built for</source>
        <translation>erzeugt für</translation>
    </message>
    <message>
        <location filename="../../about.ui" line="15"/>
        <source>About QMC2</source>
        <translation>Über QMC2</translation>
    </message>
    <message>
        <location filename="../../about.ui" line="579"/>
        <source>System information</source>
        <translation>System Informationen</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="149"/>
        <source>Build OS:</source>
        <translation>Systemumgebung bei Erzeugung:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="163"/>
        <source>Phonon backend / supported MIME types:</source>
        <translation>Phonon Backend / unterstützte MIME Typen:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="175"/>
        <source>Environment variables:</source>
        <translation>Umgebungsvariablen:</translation>
    </message>
    <message>
        <location filename="../../about.ui" line="549"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Project homepage:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;http://www.mameworld.net/mamecat&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Development site:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;http://sourceforge.net/projects/qmc2&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;QMC2 development mailing list:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;qmc2-devel@lists.sourceforge.net (subscribers only)&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;List subscription:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;https://lists.sourceforge.net/lists/listinfo/qmc2-devel&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="140"/>
        <source>Project homepage:</source>
        <translation>Projekt Homepage:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="141"/>
        <source>Development site:</source>
        <translation>Entwicklungsseite:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="142"/>
        <source>QMC2 development mailing list:</source>
        <translation>QMC2 Entwicklungs-Mailingliste:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="143"/>
        <source>List subscription:</source>
        <translation>Liste abonnieren:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <source>Qt version:</source>
        <translation>Qt Version:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <location filename="../../about.cpp" line="160"/>
        <source>Compile-time:</source>
        <translation>Zur Kompilationszeit:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <location filename="../../about.cpp" line="160"/>
        <location filename="../../about.cpp" line="163"/>
        <source>Run-time:</source>
        <translation>Zur Laufzeit:</translation>
    </message>
    <message>
        <location filename="../../about.ui" line="515"/>
        <source>Project details</source>
        <translation>Projekt Details</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="136"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="136"/>
        <source>Germany</source>
        <translation>Deutschland</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="156"/>
        <source>Emulator version:</source>
        <translation>Emulator Version:</translation>
    </message>
    <message utf8="true">
        <location filename="../../about.ui" line="527"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;QMC2 - M.A.M.E. Catalog / Launcher II&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Qt 4 based UNIX multi-emulator frontend&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Version X.Y[.bZ], built for SDLMAME&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright © 2006 - 2008 R. Reucher, Germany&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Total: %1 MB</source>
        <translation>Gesamt: %1 MB</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Free: %1 MB</source>
        <translation>Frei: %1 MB</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Used: %1 MB</source>
        <translation>Benutzt: %1 MB</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Physical memory:</source>
        <translation>Physischer Speicher:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="174"/>
        <source>Number of CPUs:</source>
        <translation>Anzahl CPUs:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="174"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Template information:</source>
        <translation>Template Information:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="72"/>
        <source>Windows 7 or Windows Server 2008 R2 (Windows 6.1)</source>
        <translation>Windows 7 oder Windows Server 2008 R2 (Windows 6.1)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Emulator:</source>
        <translation>Emulator:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Format:</source>
        <translation>Format:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <source>Build key:</source>
        <translation>Build Schlüssel:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="160"/>
        <source>SDL version:</source>
        <translation>SDL Version:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="59"/>
        <source>Mac OS X 10.3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="60"/>
        <source>Mac OS X 10.4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="61"/>
        <source>Mac OS X 10.5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="62"/>
        <source>Mac OS X 10.6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="63"/>
        <source>Mac (unkown)</source>
        <translation>Mac (unbekannt)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="67"/>
        <source>Windows NT (Windows 4.0)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="68"/>
        <source>Windows 2000 (Windows 5.0)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="69"/>
        <source>Windows XP (Windows 5.1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="70"/>
        <source>Windows Server 2003, Windows Server 2003 R2, Windows Home Server or Windows XP Professional x64 Edition (Windows 5.2)</source>
        <translation>Windows Server 2003, Windows Server 2003 R2, Windows Home Server oder Windows XP Professional x64 Edition (Windows 5.2)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="71"/>
        <source>Windows Vista or Windows Server 2008 (Windows 6.0)</source>
        <translation>Windows Vista oder Windows Server 2008 (Windows 6.0)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="73"/>
        <source>Windows (unknown)</source>
        <translation>Windows (unbekannt)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="122"/>
        <source>SVN r%1</source>
        <translation>SVN r%1</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="142"/>
        <source>subscription required</source>
        <translation>Subskription erforderlich</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="144"/>
        <source>Bug tracking system:</source>
        <translation>Bug Tracking System:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="152"/>
        <location filename="../../about.cpp" line="154"/>
        <source>Running OS:</source>
        <translation>Systemumgebung zur Laufzeit:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="119"/>
        <source>Qt 4 based multi-platform/multi-emulator front end</source>
        <translation>Qt 4 basiertes Multi-Plattform/Multi-Emulator Frontend</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="163"/>
        <source>Phonon version:</source>
        <translation>Phonon Version:</translation>
    </message>
</context>
<context>
    <name>ArcadeScene</name>
    <message>
        <location filename="../../arcade/arcadescene.cpp" line="36"/>
        <source>FPS: --</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescene.cpp" line="258"/>
        <location filename="../../arcade/arcadescene.cpp" line="260"/>
        <source>FPS: %1</source>
        <translation>FPS: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescene.cpp" line="303"/>
        <location filename="../../arcade/arcadescene.cpp" line="306"/>
        <source>Paused</source>
        <translation>Pausiert</translation>
    </message>
</context>
<context>
    <name>ArcadeScreenshotSaverThread</name>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="48"/>
        <source>ArcadeScreenshotSaverThread: Started</source>
        <translation>ArcadeScreenshotSaverThread: Gestarted</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="66"/>
        <source>ArcadeScreenshotSaverThread: Saving screen shot</source>
        <translation>ArcadeScreenshotSaverThread: Speichere Screenshot</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="80"/>
        <source>ArcadeScreenshotSaverThread: Screen shot successfully saved as &apos;%1&apos;</source>
        <translation>ArcadeScreenshotSaverThread: Screenshot erfolgreich unter &apos;%1&apos; gespeichert</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="82"/>
        <source>ArcadeScreenshotSaverThread: Failed to save screen shot as &apos;%1&apos;</source>
        <translation>ArcadeScreenshotSaverThread: Screenshot speichern unter &apos;%1&apos; fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="92"/>
        <source>ArcadeScreenshotSaverThread: Ended</source>
        <translation>ArcadeScreenshotSaverThread: Beendet</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="71"/>
        <source>ArcadeScreenshotSaverThread: Failed to create screen shot directory &apos;%1&apos; - aborting screen shot creation</source>
        <translation>ArcadeScreenshotSaverThread: Erzeugung des Screenshot-Verzeichnisses &apos;%1&apos; fehlgeschlagen - breche Screenshot Erzeugung ab</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="83"/>
        <source>Saving screen shot</source>
        <translation>Speichere Screenshot</translation>
    </message>
</context>
<context>
    <name>ArcadeSetupDialog</name>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="15"/>
        <source>Arcade setup</source>
        <translation>Arcade Setup</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="25"/>
        <source>Graphics mode settings</source>
        <translation>Einstellungen für Grafik Modus</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="58"/>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="648"/>
        <source>Arcade font (= system default if empty)</source>
        <translation>Arcade Schriftart (= System-Standard, falls leer)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="658"/>
        <source>Browse arcade font</source>
        <translation>Arcade Schriftart auswählen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="295"/>
        <source>Snapshot directory</source>
        <translation>Snapshot Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="304"/>
        <source>Directory to store snapshots (write)</source>
        <translation>Verzeichnis in das Snapshots gespeichert werden (schreiben)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="311"/>
        <source>Browse snapshot directory</source>
        <translation>Snapshot Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="72"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="171"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="786"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1007"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1228"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1449"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1670"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1891"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2112"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2333"/>
        <source>X:</source>
        <translation>X:</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="95"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="194"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="812"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1033"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1254"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1475"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1696"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1917"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2138"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2359"/>
        <source>Y:</source>
        <translation>Y:</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="216"/>
        <source>Display arcade scene in full screen mode or windowed</source>
        <translation>Arcade Szene im Vollbild-Modus darstellen oder im Fenster</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="219"/>
        <source>Full screen</source>
        <translation>Vollbild</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="229"/>
        <source>Use window resolution in full screen mode (for slow systems)</source>
        <translation>Fenster-Auflösung auch im Vollbild-Modus verwenden (für langsame Systeme)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="232"/>
        <source>Use window resolution</source>
        <translation>Fenster-Auflösung</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="158"/>
        <source>Aspect ratio</source>
        <translation>Seitenverhältnis</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="239"/>
        <source>Show frames per second counter in the lower left corner</source>
        <translation>FPS Zähler in der unteren linken Ecke anzeigen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="242"/>
        <source>Show FPS</source>
        <translation>FPS anzeigen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="276"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="590"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="634"/>
        <source>Keep aspect ratio</source>
        <translation>Seitenverh. beibehalten</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="683"/>
        <source>Virtual resolution</source>
        <translation>Virtuelle Auflösung</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="711"/>
        <source>Virtual width of scene</source>
        <translation>Virtuelle Breite der Szene</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="744"/>
        <source>Virtual height of scene</source>
        <translation>Virtuelle Höhe der Szene</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="253"/>
        <source>Enable anti aliasing on primitive drawing</source>
        <translation>Anti-Aliasing für Primitive-Zeichnungen aktivieren</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="256"/>
        <source>Primitive AA</source>
        <translation>Primitive AA</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="263"/>
        <source>Scale items smoothly</source>
        <translation>Hochwertige Skalierung der Szenen-Elemente</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="266"/>
        <source>Smooth item scaling</source>
        <translation>Hochw. Skalierung</translation>
    </message>
    <message utf8="true">
        <location filename="../../arcade/arcadesetupdialog.ui" line="2540"/>
        <source>°</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="329"/>
        <source>OpenGL</source>
        <translation>OpenGL</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="343"/>
        <source>Enable direct rendering</source>
        <translation>Direct Rendering aktivieren</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="346"/>
        <source>Direct rendering</source>
        <translation>Dierect Rendering</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="353"/>
        <source>Enable enhanced OpenGL anti aliasing</source>
        <translation>Erweitertes OpenGL Anti-Aliasing aktivieren</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="356"/>
        <source>Anti aliasing</source>
        <translation>Anti-Aliasing</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="363"/>
        <source>Synchronize buffer swaps with screen</source>
        <translation>Puffer-Wechsel mit dem Bildschirm synchronisieren</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="366"/>
        <source>Sync to screen</source>
        <translation>Bildschirm-Sync</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="373"/>
        <source>Enable double buffering (avoids flicker)</source>
        <translation>Double Buffering aktivieren (vermeidet Flackern)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="376"/>
        <source>Double buffering</source>
        <translation>Double Buffering</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="383"/>
        <source>Enable depth buffering (Z-buffer)</source>
        <translation>Tiefenpufferung aktivieren (Z-Puffer)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="386"/>
        <source>Depth buffering</source>
        <translation>Depth Buffering</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="393"/>
        <source>Use RGBA color mode</source>
        <translation>RGBA Farbmodus verwenden</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="396"/>
        <source>RGBA</source>
        <translation>RGBA</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="406"/>
        <source>Alpha channel</source>
        <translation>Alpha Channel</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="413"/>
        <source>Enable multi sampling</source>
        <translation>Multi-Sampling aktivieren</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="416"/>
        <source>Multi sampling</source>
        <translation>Multi-Sampling</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="423"/>
        <source>Enable OpenGL overlays</source>
        <translation>OpenGL Overlays aktivieren</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="426"/>
        <source>Overlays</source>
        <translation>Overlays</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="433"/>
        <source>Enable accumulator buffer</source>
        <translation>Accumulator Buffer aktivieren</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="436"/>
        <source>Accumulator buffer</source>
        <translation>Accumulator Buffer</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="443"/>
        <source>Enable stencil buffer</source>
        <translation>Sencil Buffer aktivieren</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="446"/>
        <source>Stencil buffer</source>
        <translation>Stencil Buffer</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="453"/>
        <source>Enable stereo buffer</source>
        <translation>Stereo Buffer aktivieren</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="456"/>
        <source>Stereo buffer</source>
        <translation>Stereo Buffer</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="486"/>
        <source>Scene layout</source>
        <translation>Szenen Layout</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="492"/>
        <source>Layout name</source>
        <translation>Layout Name</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="505"/>
        <source>Select the layout you want to edit / use</source>
        <translation>Layout zur Bearbeitung / Verwendung auswählen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2303"/>
        <source>Control display of MAWS lookup</source>
        <translation>Anzeige des MAWS Lookups steuern</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2306"/>
        <source>MAWS lookup</source>
        <translation>MAWS Lookup</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2584"/>
        <source>Apply settings</source>
        <translation>Einstellungen übernehmen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2587"/>
        <source>&amp;Apply</source>
        <translation>&amp;Anwenden</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2601"/>
        <source>Restore currently applied settings</source>
        <translation>Aktuell gesicherte Einstellungen restaurieren</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2604"/>
        <source>&amp;Restore</source>
        <translation>&amp;Restaurieren</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2618"/>
        <source>Reset to default settings (click &lt;i&gt;Restore&lt;/i&gt; to restore currently applied settings!)</source>
        <translation>Auf Standard-Einstellungen zurücksetzen (klicke auf &lt;i&gt;Restaurieren&lt;/i&gt; um die gespeicherte Konfiguration wiederherzustellen)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2621"/>
        <source>&amp;Default</source>
        <translation>Stan&amp;dard</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2645"/>
        <source>Close and apply settings</source>
        <translation>Schließen und Einstellungen übernehmen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2648"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2655"/>
        <source>Close and discard changes</source>
        <translation>Schließen und Änderungen verwerfen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2658"/>
        <source>&amp;Cancel</source>
        <translation>Abbre&amp;chen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="273"/>
        <source>Keep aspect ratio when resizing scene window</source>
        <translation>Seitenverhältnis beibehalten, wenn die Größe des Szenen-Fensters verändert wird</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="403"/>
        <source>Use alpha channel information for transparency</source>
        <translation>Alpha Channel Information für Transparenz verwenden</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2537"/>
        <source>Scene rotation angle in degrees</source>
        <translation>Rotationswinkel der Szene in Grad</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="518"/>
        <source>Items, placements and parameters</source>
        <translation>Gegenstände, Platzierungen und Parameter</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="556"/>
        <source>Background image</source>
        <translation>Hintergrund Bild</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="600"/>
        <source>Foreground image</source>
        <translation>Vordergrund Bild</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="759"/>
        <source>Game list</source>
        <translation>Spieleliste</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="771"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="992"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1213"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1434"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1655"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1876"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2097"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2318"/>
        <source>Geometry</source>
        <translation>Geometrie</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="796"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1017"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1238"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1459"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1680"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1901"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2122"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2343"/>
        <source>X coordinate of item position</source>
        <translation>X Koordinate der Position des Gegenstands</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="822"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1043"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1264"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1485"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1706"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1927"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2148"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2369"/>
        <source>Y coordinate of item position</source>
        <translation>Y Koordinate der Position des Gegenstands</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="118"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="698"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="838"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1059"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1280"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1501"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1722"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1943"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2164"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2385"/>
        <source>W:</source>
        <translation>B:</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="848"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1069"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1290"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1511"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1732"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1953"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2174"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2395"/>
        <source>Item width</source>
        <translation>Breite des Gegenstands</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="141"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="731"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="864"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1085"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1306"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1527"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1748"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1969"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2190"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2411"/>
        <source>H:</source>
        <translation>H:</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="874"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1095"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1316"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1537"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1758"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1979"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2200"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2421"/>
        <source>Item height</source>
        <translation>Höhe des Gegenstands</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="886"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1107"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1328"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1549"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1770"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1991"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2212"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2433"/>
        <source>Use background for this item</source>
        <translation>Hintergrund für diesen Gegenstand verwenden</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="889"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1110"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1331"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1552"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1773"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1994"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2215"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2436"/>
        <source>Background</source>
        <translation>Hintergrund</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="904"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1125"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1346"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1567"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1788"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2009"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2230"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2451"/>
        <source>Select background color</source>
        <translation>Hintergrundfarbe auswählen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="918"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1139"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1360"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1581"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1802"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2023"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2244"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2465"/>
        <source>T:</source>
        <translation>T:</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="925"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1146"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1367"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1588"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1809"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2030"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2251"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2472"/>
        <source>Select background transparency</source>
        <translation>Hintergrundtransparenz auswählen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="928"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1149"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1370"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1591"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1812"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2033"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2254"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2475"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="938"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1159"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1380"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1601"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1822"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2043"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2264"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2485"/>
        <source>Use texture bitmap for background</source>
        <translation>Textur für Hintergrund verwenden</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="941"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1162"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1383"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1604"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1825"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2046"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2267"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2488"/>
        <source>Texture</source>
        <translation>Textur</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="948"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1169"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1390"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1611"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1832"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2053"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2274"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2495"/>
        <source>Texture bitmap for background</source>
        <translation>Textur für Hintergrund</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="961"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1182"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1403"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1624"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1845"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2066"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2287"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2508"/>
        <source>Browse texture bitmap for background</source>
        <translation>Textur für Hintergrund auswählen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="980"/>
        <source>Preview image</source>
        <translation>Vorschau Bild</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1201"/>
        <source>Flyer image</source>
        <translation>Flyer Bild</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="641"/>
        <source>Arcade font</source>
        <translation>Arcade Schriftart</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="669"/>
        <source>Select font color</source>
        <translation>Schriftfarbe auswählen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="672"/>
        <source>Font color</source>
        <translation>Schriftfarbe</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.cpp" line="25"/>
        <source>Machine list</source>
        <translation>Maschinenliste</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.cpp" line="26"/>
        <source>Control display of machine list</source>
        <translation>Anzeige der Maschinenliste steuern</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="553"/>
        <source>Use a background image</source>
        <translation>Hintergrund Bild verwenden</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="563"/>
        <source>Background image file (read)</source>
        <translation>Hintergrund Bild Datei (lesen)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="576"/>
        <source>Browse background image file</source>
        <translation>Hintergrund Bild Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="587"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="631"/>
        <source>Keep image&apos;s aspect ratio when scaling</source>
        <translation>Seitenverhältnis des Bildes beim Skalieren aufrecht erhalten</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="597"/>
        <source>Use a foreground image</source>
        <translation>Vordergrund Bild verwenden</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="607"/>
        <source>Foreground image file (read)</source>
        <translation>Vordergrund Bild Datei (lesen)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="620"/>
        <source>Browse foreground image file</source>
        <translation>Vordergrund Bild Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="756"/>
        <source>Control display of game list</source>
        <translation>Anzeige der Spieleliste steuern</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="977"/>
        <source>Control display of preview image</source>
        <translation>Anzeige des Vorschaubilds steuern</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1198"/>
        <source>Control display of flyer image</source>
        <translation>Anzeige des Flyer Bildes steuern</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="79"/>
        <source>X coordinate of scene window position</source>
        <translation>X Koordinate der Szenen Fenster-Position</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="102"/>
        <source>Y coordinate of scene window position</source>
        <translation>Y Koordinate der Szenen Fenster-Position</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="125"/>
        <source>Width of scene window</source>
        <translation>Breite des Szenen-Fensters</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="148"/>
        <source>Height of scene window</source>
        <translation>Höhe des Szenen-Fensters</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="178"/>
        <source>X portion of scene aspect ratio (should be equal to screen&apos;s aspect ratio)</source>
        <translation>X-Anteil des Seitenverhältnisses der Szene (sollte dem Seitenverhältnis des Bildschirms entsprechen)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="201"/>
        <source>Y portion of scene aspect ratio (should be equal to screen&apos;s aspect ratio)</source>
        <translation>Y-Anteil des Seitenverhältnisses der Szene (sollte dem Seitenverhältnis des Bildschirms entsprechen)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2530"/>
        <source>Scene rotation</source>
        <translation>Szenen-Rotation</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="285"/>
        <source>Center arcade window on screen</source>
        <translation>Arcade Fenster auf Bildschirm zentrieren</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="288"/>
        <source>Center window</source>
        <translation>Fenster zentrieren</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1419"/>
        <source>Control display of cabinet image</source>
        <translation>Anzeige des Gehäuse-Bildes steuern</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1422"/>
        <source>Cabinet image</source>
        <translation>Gehäuse-Bild</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1640"/>
        <source>Control display of controller image</source>
        <translation>Anzeige des Controller-Bildes steuern</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1643"/>
        <source>Controller image</source>
        <translation>Controller-Bild</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1861"/>
        <source>Control display of marquee image</source>
        <translation>Anzeige des Marquee-Bildes steuern</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1864"/>
        <source>Marquee image</source>
        <translation>Marquee-Bild</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2082"/>
        <source>Control display of title image</source>
        <translation>Anzeige des Titel-Bildes steuern</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2085"/>
        <source>Title image</source>
        <translation>Titel-Bild</translation>
    </message>
</context>
<context>
    <name>ArcadeView</name>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="53"/>
        <source>QMC2 - ArcadeView</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="88"/>
        <source>ArcadeView: Cleaning up</source>
        <translation>ArcadeView: Räume auf</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="120"/>
        <source>ArcadeView: Switching to windowed mode</source>
        <translation>ArcadeView: Schalte in den Fenster-Modus um</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="130"/>
        <source>ArcadeView: Resolution switching is not yet supported</source>
        <translation>ArcadeView: Auflösungsumschaltung wird noch nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="146"/>
        <source>ArcadeView: Setting window size to %1x%2</source>
        <translation>ArcadeView: Stelle Fenster-Größe auf %1x%2 ein</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="156"/>
        <source>ArcadeView: Setting window position to %1, %2</source>
        <translation>ArcadeView: Setze Fenster-Position auf %1, %2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="170"/>
        <source>ArcadeView: This system does not appear to support OpenGL -- reverting to non-OpenGL / software renderer</source>
        <translation>ArcadeView: Dieses System scheint keine OpenGL Unterstzützung zu haben -- stelle auf Nicht-OpenGL / Software-Renderer um</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="175"/>
        <source>ArcadeView: Using OpenGL renderer</source>
        <translation>ArcadeView: Verwende OpenGL Renderer</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="179"/>
        <source>ArcadeView: This system does not appear to support vertical syncing -- disabling SyncToScreen</source>
        <translation>ArcadeView: Dieses System scheint vertikale Bild-Synchronisation nicht zu unterstützen -- deaktiviere SyncToScreen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="182"/>
        <source>ArcadeView: OpenGL: SyncToScreen: %1</source>
        <translation>ArcadeView: OpenGL: SyncToScreen: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="182"/>
        <location filename="../../arcade/arcadeview.cpp" line="184"/>
        <location filename="../../arcade/arcadeview.cpp" line="186"/>
        <location filename="../../arcade/arcadeview.cpp" line="188"/>
        <location filename="../../arcade/arcadeview.cpp" line="190"/>
        <location filename="../../arcade/arcadeview.cpp" line="192"/>
        <location filename="../../arcade/arcadeview.cpp" line="194"/>
        <location filename="../../arcade/arcadeview.cpp" line="196"/>
        <location filename="../../arcade/arcadeview.cpp" line="198"/>
        <location filename="../../arcade/arcadeview.cpp" line="204"/>
        <location filename="../../arcade/arcadeview.cpp" line="210"/>
        <location filename="../../arcade/arcadeview.cpp" line="212"/>
        <source>on</source>
        <translation>an</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="182"/>
        <location filename="../../arcade/arcadeview.cpp" line="184"/>
        <location filename="../../arcade/arcadeview.cpp" line="186"/>
        <location filename="../../arcade/arcadeview.cpp" line="188"/>
        <location filename="../../arcade/arcadeview.cpp" line="190"/>
        <location filename="../../arcade/arcadeview.cpp" line="192"/>
        <location filename="../../arcade/arcadeview.cpp" line="194"/>
        <location filename="../../arcade/arcadeview.cpp" line="196"/>
        <location filename="../../arcade/arcadeview.cpp" line="198"/>
        <location filename="../../arcade/arcadeview.cpp" line="204"/>
        <location filename="../../arcade/arcadeview.cpp" line="210"/>
        <location filename="../../arcade/arcadeview.cpp" line="212"/>
        <source>off</source>
        <translation>aus</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="184"/>
        <source>ArcadeView: OpenGL: DoubleBuffer: %1</source>
        <translation>ArcadeView: OpenGL: DoubleBuffer: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="186"/>
        <source>ArcadeView: OpenGL: DepthBuffer: %1</source>
        <translation>ArcadeView: OpenGL: DepthBuffer: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="188"/>
        <source>ArcadeView: OpenGL: RGBA: %1</source>
        <translation>ArcadeView: OpenGL: RGBA: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="190"/>
        <source>ArcadeView: OpenGL: AlphaChannel: %1</source>
        <translation>ArcadeView: OpenGL: AlphaChannel: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="192"/>
        <source>ArcadeView: OpenGL: AccumulatorBuffer: %1</source>
        <translation>ArcadeView: OpenGL: AccumulatorBuffer: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="194"/>
        <source>ArcadeView: OpenGL: StencilBuffer: %1</source>
        <translation>ArcadeView: OpenGL: StencilBuffer: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="196"/>
        <source>ArcadeView: OpenGL: Stereo: %1</source>
        <translation>ArcadeView: OpenGL: Stereo: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="198"/>
        <source>ArcadeView: OpenGL: DirectRendering: %1</source>
        <translation>ArcadeView: OpenGL: DirectRendering: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="201"/>
        <source>ArcadeView: This system does not appear to support OpenGL overlays -- disabling OpenGL overlays</source>
        <translation>ArcadeView: Dieses System scheint OpenGL Overlays nicht zu unterstützen -- deaktiviere OpenGL Overlays</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="204"/>
        <source>ArcadeView: OpenGL: Overlay: %1</source>
        <translation>ArcadeView: OpenGL: Overlay: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="207"/>
        <source>ArcadeView: This system does not appear to support OpenGL multi sampling -- disabling MultiSample</source>
        <translation>ArcadeView: Dieses System scheint OpenGL Multi-Sampling nicht zu unterstützen -- deaktiviere MultiSample</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="210"/>
        <source>ArcadeView: OpenGL: MultiSample: %1</source>
        <translation>ArcadeView: OpenGL: MultiSample: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="212"/>
        <source>ArcadeView: OpenGL: AntiAliasing: %1</source>
        <translation>ArcadeView: OpenGL: AntiAliasing: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="222"/>
        <source>ArcadeView: Using software renderer</source>
        <translation>ArcadeView: Verwende Software Renderer</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="227"/>
        <source>ArcadeView: X11: Screen number: %1</source>
        <translation>ArcadeView: X11: Screen number: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="228"/>
        <source>ArcadeView: X11: Color depth: %1</source>
        <translation>ArcadeView: X11: Farbtiefe: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="229"/>
        <source>ArcadeView: X11: DPI-X: %1</source>
        <translation>ArcadeView: X11: DPI-X: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="230"/>
        <source>ArcadeView: X11: DPI-Y: %1</source>
        <translation>ArcadeView: X11: DPI-Y: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="231"/>
        <source>ArcadeView: X11: Compositing manager: %1</source>
        <translation>ArcadeView: X11: Compositing Manager: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="231"/>
        <source>running</source>
        <translation>rennt</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="231"/>
        <source>not running</source>
        <translation>rennt nicht</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="235"/>
        <source>ArcadeView: Screen geometry: %1x%2</source>
        <translation>ArcadeView: Bildschirm-Geometrie: %1x%2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="236"/>
        <source>ArcadeView: Virtual resolution: %1x%2</source>
        <translation>ArcadeView: Virtuelle Auflösung: %1x%2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="237"/>
        <source>ArcadeView: Selected aspect ratio: %1:%2</source>
        <translation>ArcadeView: Ausgewähltes Seitenverhältnis: %1:%2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="241"/>
        <source>ArcadeView: Virtual resolution doesn&apos;t fit aspect ratio -- scene coordinates may be stretched or compressed</source>
        <translation>ArcadeView: Virtuelle Auflösung passt nicht zu Seitenverhältnis -- Szenen-Koordinaten könnten gestreckt oder gestaucht sein</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="242"/>
        <source>ArcadeView: Aspect ratio will be maintained</source>
        <translation>ArcadeView: Seitenverhältnis wird beibehalten</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="242"/>
        <source>ArcadeView: Aspect ratio will not be maintained</source>
        <translation>ArcadeView: Seitenverhältnis wird nicht beibehalten</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="243"/>
        <source>ArcadeView: FPS counter display %1</source>
        <translation>ArcadeView: FPS Anzeige %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="243"/>
        <location filename="../../arcade/arcadeview.cpp" line="246"/>
        <source>activated</source>
        <translation>aktiviert</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="243"/>
        <location filename="../../arcade/arcadeview.cpp" line="246"/>
        <source>deactivated</source>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="246"/>
        <source>ArcadeView: Primitive antialiasing %1</source>
        <translation>ArcadeView: Antialiasing für Primitives %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="250"/>
        <source>ArcadeView: Centering window on screen</source>
        <translation>ArcadeView: Zentriere Fenster auf Bildschirm</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="254"/>
        <source>ArcadeView: Restoring saved window position</source>
        <translation>ArcadeView: Restauriere gespeicherte Fenster-Position</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="300"/>
        <source>ArcadeView: Adjusting window size to %1x%2 to maintain the aspect ratio</source>
        <translation>ArcadeView: Passe Fenster-Größe auf %1x%2 an um Seitenverhältnis beizubehalten</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="281"/>
        <source>ArcadeView: Rendering screen shot</source>
        <translation>ArcadeView: Zeichne Screenshot</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="290"/>
        <source>Saving screen shot</source>
        <translation>Speichere Screenshot</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="238"/>
        <source>ArcadeView: Scene rotation angle: %1 degrees</source>
        <translation>ArcadeView: Szenen-Rotationswinkel: %1 Grad</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="127"/>
        <source>ArcadeView: Switching to full screen mode</source>
        <translation>ArcadeView: Schalte in den Vollbild-Modus um</translation>
    </message>
</context>
<context>
    <name>AudioEffectDialog</name>
    <message>
        <location filename="../../audioeffects.cpp" line="35"/>
        <location filename="../../audioeffects.cpp" line="194"/>
        <location filename="../../audioeffects.cpp" line="199"/>
        <source>Enable effect &apos;%1&apos;</source>
        <translation>Effekt &apos;%1&apos; einschalten</translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="52"/>
        <source>Setup effect &apos;%1&apos;</source>
        <translation>Effekteinstellungen für &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="186"/>
        <location filename="../../audioeffects.cpp" line="207"/>
        <source>Disable effect &apos;%1&apos;</source>
        <translation>Effekt &apos;%1&apos; abschalten</translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="195"/>
        <source>WARNING: audio player: can&apos;t insert effect &apos;%1&apos;</source>
        <translation>WARNUNG: Audio Player: Kann Effekt &apos;%1&apos; nicht einfügen</translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="208"/>
        <source>WARNING: audio player: can&apos;t remove effect &apos;%1&apos;</source>
        <translation>WARNUNG: Audio Player: Kann Effekt &apos;%1&apos; nicht entfernen</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="14"/>
        <source>Audio effects</source>
        <translation>Audio Effekte</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="36"/>
        <source>Close audio effects dialog</source>
        <translation>Audio Effekt Dialog schließen</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="39"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="59"/>
        <location filename="../../audioeffects.ui" line="62"/>
        <source>List of available audio effects</source>
        <translation>Liste der verfügbaren Audio Effekte</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="81"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="86"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="91"/>
        <source>Enable</source>
        <translation>Aktivieren</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="96"/>
        <source>Setup</source>
        <translation>Einstellen</translation>
    </message>
</context>
<context>
    <name>Cabinet</name>
    <message>
        <location filename="../../cabinet.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>In Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="56"/>
        <location filename="../../cabinet.cpp" line="57"/>
        <source>Game cabinet image</source>
        <translation>Bild des Spiel-Gehäuses</translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="59"/>
        <location filename="../../cabinet.cpp" line="60"/>
        <source>Machine cabinet image</source>
        <translation>Bild des Maschinen-Gehäuses</translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="68"/>
        <location filename="../../cabinet.cpp" line="72"/>
        <source>FATAL: can&apos;t open cabinet file, please check access permissions for %1</source>
        <translation>FATAL: kann Gehäuse-Datei nicht öffnen; bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Warte auf Daten...</translation>
    </message>
</context>
<context>
    <name>Controller</name>
    <message>
        <location filename="../../controller.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>In Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="56"/>
        <location filename="../../controller.cpp" line="57"/>
        <source>Game controller image</source>
        <translation>Bild des Spiel-Controllers</translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="59"/>
        <location filename="../../controller.cpp" line="60"/>
        <source>Machine controller image</source>
        <translation>Bild des Maschinen-Controllers</translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="68"/>
        <location filename="../../controller.cpp" line="72"/>
        <source>FATAL: can&apos;t open controller file, please check access permissions for %1</source>
        <translation>FATAL: kann Controller-Datei nicht öffnen; bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Warte auf Daten...</translation>
    </message>
</context>
<context>
    <name>DemoModeDialog</name>
    <message>
        <location filename="../../demomode.cpp" line="105"/>
        <source>demo mode stopped</source>
        <translation>Demo Modus angehalten</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="263"/>
        <location filename="../../demomode.cpp" line="107"/>
        <source>Run &amp;demo</source>
        <translation>&amp;Demo starten</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="260"/>
        <location filename="../../demomode.cpp" line="108"/>
        <source>Run demo now</source>
        <translation>Demo jetzt starten</translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="124"/>
        <source>please wait for reload to finish and try again</source>
        <translation>Bitte warte bis die Spieleliste aktualisiert wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="128"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation>Bitte warte bis die ROM Verifikation abgeschlossen wurde und versuche es dann erneut</translation>
    </message>
    <message numerus="yes">
        <location filename="../../demomode.cpp" line="170"/>
        <source>demo mode started -- %n game(s) selected by filter</source>
        <translation>
            <numerusform>Demo Modus gestartet -- %n Spiel durch Filter ausgewählt</numerusform>
            <numerusform>Demo Modus gestartet -- %n Spiele durch Filter ausgewählt</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="172"/>
        <source>demo mode cannot start -- no games selected by filter</source>
        <translation>Demo Modus kann nicht gestartet werden -- kein Spiel durch Filter ausgewählt</translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="176"/>
        <source>Stop &amp;demo</source>
        <translation>&amp;Demo anhalten</translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="177"/>
        <source>Stop demo now</source>
        <translation>Demo jetzt anhalten</translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="240"/>
        <source>starting emulation in demo mode for &apos;%1&apos;</source>
        <translation>Starte Emulation im Demo Modus für &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="14"/>
        <source>Demo mode</source>
        <translation>Demo Modus</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="26"/>
        <source>ROM state filter</source>
        <translation>ROM Status Filter</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="44"/>
        <source>Select ROM state C (correct)?</source>
        <translation>ROM Status K auswählen (korrekt)?</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="67"/>
        <source>Select ROM state M (mostly correct)?</source>
        <translation>ROM Status B auswählen (beinahe korrekt)?</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="90"/>
        <source>Select ROM state I (incorrect)?</source>
        <translation>ROM Status I auswählen (inkorrekt)?</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="110"/>
        <source>Select ROM state N (not found)?</source>
        <translation>ROM Status N auswählen (nicht gefunden)?</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="130"/>
        <source>Select ROM state U (unknown)?</source>
        <translation>ROM Status U auswählen (unbekannt)?</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="152"/>
        <source>Seconds to run</source>
        <translation>Laufzeit in Sekunden</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="159"/>
        <source>Number of seconds to run an emulator in demo mode</source>
        <translation>Anzahl der Sekunden, die ein Emulator im Demo Modus laufen soll</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="213"/>
        <source>Use only tagged games</source>
        <translation>Nur markierte Spiele verwenden</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="216"/>
        <source>Tagged</source>
        <translation>Markierte</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="289"/>
        <source>Pause (seconds)</source>
        <translation>Pause (Sekunden)</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="296"/>
        <source>Number of seconds to pause between emulator runs</source>
        <translation>Anzahl der Sekunden zum Pausieren zwischen Emulator-Läufen</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="174"/>
        <source>Start emulators in full screen mode (otherwise use windowed mode)</source>
        <translation>Emulatoren im Vollbild Modus starten (andernfalls im Fenster-Modus ausführen)</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="177"/>
        <source>Full screen</source>
        <translation>Vollbild</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="190"/>
        <source>Maximize emulators when in windowed mode</source>
        <translation>Emulatoren im Fenster Modus maximieren</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="193"/>
        <source>Maximized</source>
        <translation>Maximiert</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="203"/>
        <source>Embed windowed emulators</source>
        <translation>Emulator Fenster einbetten</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="206"/>
        <source>Embedded</source>
        <translation>Eingebettet</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="231"/>
        <source>Close this dialog (and stop running demo)</source>
        <translation>Dialog schließen (und Demo anhalten)</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="234"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
</context>
<context>
    <name>DetailSetup</name>
    <message>
        <location filename="../../detailsetup.ui" line="15"/>
        <source>Detail setup</source>
        <translation>Einstellung der Details</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="201"/>
        <source>&amp;Cancel</source>
        <translation>Abbre&amp;chen</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="36"/>
        <source>List of available details</source>
        <translation>Liste verfügbarer Details</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="91"/>
        <source>List of active details and their order</source>
        <translation>Liste aktiver Details und ihrer Reigenfolge</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="68"/>
        <source>Activate selected details</source>
        <translation>Ausgewählte Details aktivieren</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="106"/>
        <source>Deactivate selected details</source>
        <translation>Ausgewählte Details deaktivieren</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="173"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="21"/>
        <location filename="../../detailsetup.cpp" line="98"/>
        <source>Pre&amp;view</source>
        <translation>&amp;Vorschau</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="22"/>
        <source>Game preview image</source>
        <translation>Spiel-Vorschaubild</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="24"/>
        <location filename="../../detailsetup.cpp" line="101"/>
        <source>Fl&amp;yer</source>
        <translation>Fl&amp;yer</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="25"/>
        <source>Game flyer image</source>
        <translation>Spiel Flyer-Bild</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="27"/>
        <source>Game &amp;info</source>
        <translation>Spiel &amp;Info</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="28"/>
        <source>Game information</source>
        <translation>Spiel Informationen</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="30"/>
        <location filename="../../detailsetup.cpp" line="107"/>
        <source>Em&amp;ulator info</source>
        <translation>Em&amp;ulator Info</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="31"/>
        <location filename="../../detailsetup.cpp" line="108"/>
        <source>Emulator information</source>
        <translation>Emulator Informationen</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="33"/>
        <location filename="../../detailsetup.cpp" line="110"/>
        <source>&amp;Configuration</source>
        <translation>&amp;Konfiguration</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="34"/>
        <location filename="../../detailsetup.cpp" line="111"/>
        <source>Emulator configuration</source>
        <translation>Emulator Konfiguration</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="36"/>
        <location filename="../../detailsetup.cpp" line="119"/>
        <source>Ca&amp;binet</source>
        <translation>Geh&amp;äuse</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="37"/>
        <source>Arcade cabinet image</source>
        <translation>Arcade-Gehäuse Bild</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="39"/>
        <source>C&amp;ontroller</source>
        <translation>C&amp;ontroller</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="40"/>
        <source>Control panel image</source>
        <translation>Controller Bild</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="42"/>
        <source>Mar&amp;quee</source>
        <translation>Mar&amp;quee</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="43"/>
        <source>Marquee image</source>
        <translation>Marquee Bild</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="45"/>
        <source>Titl&amp;e</source>
        <translation>Tit&amp;el</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="46"/>
        <source>Title screen image</source>
        <translation>Titel Bild</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="48"/>
        <source>MA&amp;WS</source>
        <translation>MA&amp;WS</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="51"/>
        <location filename="../../detailsetup.cpp" line="116"/>
        <source>&amp;PCB</source>
        <translation>&amp;PCB</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="52"/>
        <location filename="../../detailsetup.cpp" line="117"/>
        <source>PCB image</source>
        <translation>PCB Bild</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="58"/>
        <location filename="../../detailsetup.cpp" line="126"/>
        <source>&amp;YouTube</source>
        <translation>&amp;YouTube</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="59"/>
        <location filename="../../detailsetup.cpp" line="127"/>
        <source>YouTube videos</source>
        <translation>YouTube Videos</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="99"/>
        <source>Machine preview image</source>
        <translation>Machinen Vorschaubild</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="102"/>
        <source>Machine flyer image</source>
        <translation>Machinen Flyer-Bild</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="104"/>
        <source>Machine &amp;info</source>
        <translation>Machinen &amp;Info</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="105"/>
        <source>Machine information</source>
        <translation>Maschinen Informationen</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="113"/>
        <source>De&amp;vices</source>
        <translation>&amp;Geräte</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="114"/>
        <source>Device configuration</source>
        <translation>Geräte Konfiguration</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="54"/>
        <location filename="../../detailsetup.cpp" line="122"/>
        <source>Softwar&amp;e list</source>
        <translation>Softwar&amp;e Liste</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="55"/>
        <location filename="../../detailsetup.cpp" line="123"/>
        <source>Software list</source>
        <translation>Software Liste</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="367"/>
        <source>MAWS configuration (1/2)</source>
        <translation>MAWS Konfiguration (1/2)</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="379"/>
        <source>Enable MAWS quick download?</source>
        <translation>MAWS Schnell-Download aktivieren?</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="30"/>
        <source>Available details</source>
        <translation>Verfügbare Details</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="85"/>
        <source>Active details</source>
        <translation>Aktive Details</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="120"/>
        <source>Move selected detail up</source>
        <translation>Ausgewähltes Detail rauf bewegen</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="137"/>
        <source>Move selected detail down</source>
        <translation>Ausgewähltes Detail runter bewegen</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="170"/>
        <source>Apply detail setup and close dialog</source>
        <translation>Detail-Einstellung übernehmen und Dialog schließen</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="198"/>
        <source>Cancel detail setup and close dialog</source>
        <translation>Detail-Einstellung abbrechen und Dialog schließen</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="184"/>
        <source>Apply detail setup</source>
        <translation>Detail-Einstellung übernehmen</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="187"/>
        <source>&amp;Apply</source>
        <translation>&amp;Anwenden</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="49"/>
        <source>MAWS page (web lookup)</source>
        <translation>MAWS Seite (Web-Lookup)</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="120"/>
        <source>Machine cabinet image</source>
        <translation>Bild des Maschinen-Gehäuses</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="377"/>
        <location filename="../../detailsetup.cpp" line="382"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="377"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="379"/>
        <source>MAWS configuration (2/2)</source>
        <translation>MAWS Konfiguration (2/2)</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="402"/>
        <source>Choose the YouTube cache directory</source>
        <translation>YouTube Cache Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="414"/>
        <source>FATAL: can&apos;t create new YouTube cache directory, path = %1</source>
        <translation>FATAL: kann neues YouTube Cache Verzeichnis nicht anlegen, Pfad = %1</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="444"/>
        <source>INFO: the configuration tab can&apos;t be removed</source>
        <translation>INFO: der Konfigurations-Tab kann nicht entfernt werden</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="51"/>
        <source>Configure current detail</source>
        <translation>Aktuelles Detail konfigurieren</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="54"/>
        <source>Configure...</source>
        <translation>Konfigurieren...</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="368"/>
        <source>MAWS URL pattern (use %1 as placeholder for game ID):</source>
        <translation>MAWS URL Schablone (%1 als Platzhalter für Spiele-ID verwenden):</translation>
    </message>
</context>
<context>
    <name>DirectoryEditWidget</name>
    <message>
        <location filename="../../direditwidget.cpp" line="44"/>
        <source>Choose directory</source>
        <translation>Verzeichnis auswählen</translation>
    </message>
</context>
<context>
    <name>DocBrowser</name>
    <message>
        <location filename="../../docbrowser.ui" line="15"/>
        <location filename="../../docbrowser.cpp" line="86"/>
        <location filename="../../docbrowser.cpp" line="91"/>
        <location filename="../../docbrowser.cpp" line="93"/>
        <location filename="../../docbrowser.cpp" line="96"/>
        <source>MiniWebBrowser</source>
        <translation>MiniWebBrowser</translation>
    </message>
</context>
<context>
    <name>Embedder</name>
    <message>
        <location filename="../../embedder.cpp" line="92"/>
        <source>emulator #%1 released, window ID = 0x%2</source>
        <translation>Emulator #%1 freigegeben, Fenster ID = 0x%2</translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="103"/>
        <source>emulator #%1 embedded, window ID = 0x%2</source>
        <translation>Emulator #%1 eingebettet, Fenster ID = 0x%2</translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="128"/>
        <source>emulator #%1 closed, window ID = 0x%2</source>
        <translation>Emulator #%1 geschlossen, Fenster ID = 0x%2</translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="148"/>
        <source>WARNING: embedder: unknown error, window ID = 0x%1</source>
        <translation>WARNUNG: Einbettung: unbekannter Fehler, Fenster ID = 0x%1</translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="143"/>
        <source>WARNING: embedder: invalid window ID = 0x%1</source>
        <translation>WARNUNG: Einbettung: ungültige Fenster ID = 0x%1</translation>
    </message>
</context>
<context>
    <name>EmbedderOptions</name>
    <message>
        <location filename="../../embedderopt.ui" line="14"/>
        <source>Embedder options</source>
        <translation>Einbettungs-Optionen</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="31"/>
        <source>Snapshots</source>
        <translation>Schnappschüsse</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="37"/>
        <source>Take a snapshot of the current window content -- hold to take snapshots repeatedly (every 100ms)</source>
        <translation>Schnappschuss des aktuellen Fensterinhalts aufnehmen - halten um wiederholend Schnappschüsse aufzunehmen (alle 100ms)</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="40"/>
        <source>Take snapshot</source>
        <translation>Schnappschuss aufnehmen</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="98"/>
        <source>Clear snapshots</source>
        <translation>Schnappschüsse leeren</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="101"/>
        <source>Clear</source>
        <translation>Leeren</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="115"/>
        <source>Scale snapshots to the native resolution</source>
        <translation>Snapshots auf die originale Auflösung skalieren</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="118"/>
        <source>Native resolution</source>
        <translation>Originale Auflösung</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="130"/>
        <source>Movies</source>
        <translation>Filme</translation>
    </message>
</context>
<context>
    <name>EmulatorOptionDelegate</name>
    <message>
        <location filename="../../emuopt.cpp" line="159"/>
        <source>All files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="164"/>
        <location filename="../../emuopt.cpp" line="176"/>
        <source>Browse: </source>
        <translation>Auswählen: </translation>
    </message>
</context>
<context>
    <name>EmulatorOptions</name>
    <message>
        <location filename="../../emuopt.cpp" line="393"/>
        <location filename="../../emuopt.cpp" line="845"/>
        <location filename="../../emuopt.cpp" line="911"/>
        <location filename="../../emuopt.cpp" line="959"/>
        <location filename="../../emuopt.cpp" line="960"/>
        <location filename="../../emuopt.cpp" line="961"/>
        <location filename="../../emuopt.cpp" line="1053"/>
        <location filename="../../emuopt.cpp" line="1055"/>
        <location filename="../../emuopt.cpp" line="1057"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="399"/>
        <source>Game specific emulator configuration</source>
        <translation>Spiel-spezifische Emulator-Konfiguration</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="405"/>
        <source>Global emulator configuration</source>
        <translation>Globale Emulator-Konfiguration</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="413"/>
        <source>Option / Attribute</source>
        <translation>Option / Attribut</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="414"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="858"/>
        <source>true</source>
        <translation>wahr</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="860"/>
        <source>false</source>
        <translation>falsch</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="806"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="809"/>
        <source>bool</source>
        <translation>bool</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="813"/>
        <source>int</source>
        <translation>int</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="817"/>
        <source>float</source>
        <translation>float</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="821"/>
        <source>float2</source>
        <translation>float2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="825"/>
        <source>float3</source>
        <translation>float3</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="837"/>
        <source>choice</source>
        <translation>choice</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="841"/>
        <source>string</source>
        <translation>string</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="850"/>
        <source>Short name</source>
        <translation>Kurzname</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="855"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="870"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="954"/>
        <source>creating template configuration map</source>
        <translation>Erzeuge Konfigurations-Vorlage</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="991"/>
        <source>FATAL: XML error reading template: &apos;%1&apos; in file &apos;%2&apos; at line %3, column %4</source>
        <translation>FATAL: XML Fehler beim Lesen des Templates:  &apos;%1&apos; in Datei &apos;%2&apos; in Zeile %3, Spalte %4</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1044"/>
        <source>template info: emulator = %1, version = %2, format = %3</source>
        <translation>Template Info: Emulator = %1, Version = %2, Format = %3</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1051"/>
        <source>FATAL: can&apos;t open options template file</source>
        <translation>FATAL: kann Optionsvorlage-Datei nicht öffnen</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1054"/>
        <source>WARNING: couldn&apos;t determine emulator type of template</source>
        <translation>WARNUNG: konnte Emulator-Typ des Templates nicht ermitteln</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1056"/>
        <source>WARNING: couldn&apos;t determine template version</source>
        <translation>WARNUNG: konnte Vorlage Version nicht ermitteln</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1058"/>
        <source>WARNING: couldn&apos;t determine template format</source>
        <translation>WARNUNG: konnte Format des Templates nicht ermitteln</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1068"/>
        <source>please wait for reload to finish and try again</source>
        <translation>Bitte warte bis die Spieleliste aktualisiert wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1077"/>
        <source>checking template configuration map against selected emulator</source>
        <translation>Vergleiche Abbildungsvorlage mit ausgewähltem Emulator</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1109"/>
        <source>FATAL: can&apos;t start MAME executable within a reasonable time frame, giving up</source>
        <translation>FATAL: kann ausführbare MAME Datei nicht in einem angemessenen Zeitraum starten, gebe auf</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1111"/>
        <source>FATAL: can&apos;t start MESS executable within a reasonable time frame, giving up</source>
        <translation>FATAL: kann ausführbare MESS Datei nicht in einem angemessenen Zeitraum starten, gebe auf</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1167"/>
        <source>FATAL: can&apos;t create temporary file, please check emulator executable and permissions</source>
        <translation>FATAL: kann temporäre Arbeitsdatei nicht erzeugen, bitte ausführbare Emulator Datei und Zugriffsrechte prüfen</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1184"/>
        <location filename="../../emuopt.cpp" line="1193"/>
        <location filename="../../emuopt.cpp" line="1202"/>
        <location filename="../../emuopt.cpp" line="1211"/>
        <location filename="../../emuopt.cpp" line="1223"/>
        <location filename="../../emuopt.cpp" line="1240"/>
        <source>emulator uses a different default value for option &apos;%1&apos; (&apos;%2&apos; vs. &apos;%3&apos;); assumed option type is &apos;%4&apos;</source>
        <translation>Emulator verwendet einen anderen Standardwert für &apos;%1&apos; (&apos;%2&apos; vs. &apos;%3&apos;); angemommener Optionstyp ist &apos;%4&apos;</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1247"/>
        <source>template option &apos;%1&apos; is unknown to the emulator</source>
        <translation>Option &apos;%1&apos; der Abbildungsvorlage ist dem Emulator unbekannt</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1257"/>
        <source>emulator option &apos;%1&apos; with default value &apos;%2&apos; is unknown to the template</source>
        <translation>Emulator-Option &apos;%1&apos; mit Standardwert &apos;%2&apos; ist nicht in der Abbildungsvorlage enthalten</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1261"/>
        <source>done (checking template configuration map against selected emulator)</source>
        <translation>Fertig (Vergleiche Abbildungsvorlage mit ausgewähltem Emulator)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../emuopt.cpp" line="1262"/>
        <source>check results: %n difference(s)</source>
        <translation>
            <numerusform>Prüfergebnis: %n Unterschied</numerusform>
            <numerusform>Prüfergebnis: %n Unterschiede</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1370"/>
        <source>WARNING: ini-export: no writable ini-paths found</source>
        <translation>WARNUNG: Ini-Export: es konnte kein beschreibbarer Ini-Pfad gefunden werden</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1374"/>
        <location filename="../../emuopt.cpp" line="1534"/>
        <source>Path selection</source>
        <translation>Pfad-Auswahl</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1375"/>
        <source>Multiple ini-paths detected. Select path(s) to export to:</source>
        <translation>Mehrere Ini-Pfade entdeckt. Pfad(e) für Export auswählen:</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1394"/>
        <source>WARNING: ini-export: no path selected (or invalid inipath)</source>
        <translation>WARNUNG: Ini-Export: kein Pfad ausgewählt (oder ungültiger Ini-Pfad)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1406"/>
        <location filename="../../emuopt.cpp" line="1566"/>
        <source>Waiting for data...</source>
        <translation>Warte auf Daten...</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1422"/>
        <source>FATAL: can&apos;t open export file for writing, path = %1</source>
        <translation>FATAL: kann Export-Datei nicht zum Schreiben öffnen, Pfad = %1</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1428"/>
        <source>exporting %1 MAME configuration to %2</source>
        <translation>Exportiere %1 MAME Konfiguration nach %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1428"/>
        <location filename="../../emuopt.cpp" line="1430"/>
        <location filename="../../emuopt.cpp" line="1481"/>
        <location filename="../../emuopt.cpp" line="1483"/>
        <location filename="../../emuopt.cpp" line="1589"/>
        <location filename="../../emuopt.cpp" line="1591"/>
        <location filename="../../emuopt.cpp" line="1690"/>
        <location filename="../../emuopt.cpp" line="1692"/>
        <source>global</source>
        <translation>globale</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1428"/>
        <location filename="../../emuopt.cpp" line="1481"/>
        <location filename="../../emuopt.cpp" line="1589"/>
        <location filename="../../emuopt.cpp" line="1690"/>
        <source>game-specific</source>
        <translation>Spiel-spezifische</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1430"/>
        <source>exporting %1 MESS configuration to %2</source>
        <translation>Exportiere %1 MESS Konfiguration nach %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1481"/>
        <source>done (exporting %1 MAME configuration to %2, elapsed time = %3)</source>
        <translation>Fertig (Exportiere %1 MAME Konfiguration nach %2, benötigte Zeit = %3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1483"/>
        <source>done (exporting %1 MESS configuration to %2, elapsed time = %3)</source>
        <translation>Fertig (Exportiere %1 MESS Konfiguration nach %2, benötigte Zeit = %3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1530"/>
        <source>WARNING: ini-import: no readable ini-paths found</source>
        <translation>WARNUNG: Ini-Import: es konnte kein lesbarer Ini-Pfad gefunden werden</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1535"/>
        <source>Multiple ini-paths detected. Select path(s) to import from:</source>
        <translation>Mehrere Ini-Pfade entdeckt. Pfad(e) für Import auswählen:</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1554"/>
        <source>WARNING: ini-import: no path selected (or invalid inipath)</source>
        <translation>WARNUNG: Ini-Import: kein Pfad ausgewählt (oder ungültiger Ini-Pfad)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1583"/>
        <source>FATAL: can&apos;t open import file for reading, path = %1</source>
        <translation>FATAL: kann Import-Datei nicht zum Lesen öffnen, Pfad = %1</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1589"/>
        <source>importing %1 MAME configuration from %2</source>
        <translation>Importiere %1 MAME Konfiguration von %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1591"/>
        <source>importing %1 MESS configuration from %2</source>
        <translation>Importiere %1 MESS Konfiguration von %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1680"/>
        <source>WARNING: unknown option &apos;%1&apos; at line %2 (%3) ignored</source>
        <translation>WARNUNG: unbekannte Option &apos;%1&apos; in Zeile %2 (%3) ignoriert</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1683"/>
        <source>WARNING: invalid syntax at line %1 (%2) ignored</source>
        <translation>WARNUNG: ungültige Syntax in Zeile %1 (%2) ignoriert</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1690"/>
        <source>done (importing %1 MAME configuration from %2, elapsed time = %3)</source>
        <translation>Fertig (Importiere %1 MAME Konfiguration von %2, benötigte Zeit = %3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1692"/>
        <source>done (importing %1 MESS configuration from %2, elapsed time = %3)</source>
        <translation>Fertig (Importiere %1 MESS Konfiguration von %2, benötigte Zeit = %3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1430"/>
        <location filename="../../emuopt.cpp" line="1483"/>
        <location filename="../../emuopt.cpp" line="1591"/>
        <location filename="../../emuopt.cpp" line="1692"/>
        <source>machine-specific</source>
        <translation>Maschinen-spezifische</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="401"/>
        <source>Machine specific emulator configuration</source>
        <translation>Maschinen-spezifische Emulator-Konfiguration</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="829"/>
        <source>file</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="833"/>
        <source>directory</source>
        <translation>Verzeichnis</translation>
    </message>
</context>
<context>
    <name>FileEditWidget</name>
    <message>
        <location filename="../../fileeditwidget.cpp" line="49"/>
        <source>Choose file</source>
        <translation>Datei auswählen</translation>
    </message>
</context>
<context>
    <name>FileSystemModel</name>
    <message>
        <location filename="../../filesystemmodel.h" line="274"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="274"/>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="470"/>
        <location filename="../../filesystemmodel.h" line="484"/>
        <source> KB</source>
        <translation> KB</translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="473"/>
        <location filename="../../filesystemmodel.h" line="487"/>
        <source> MB</source>
        <translation> MB</translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="476"/>
        <location filename="../../filesystemmodel.h" line="490"/>
        <source> GB</source>
        <translation> GB</translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="479"/>
        <source> TB</source>
        <translation> TB</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Typ</translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="274"/>
        <source>Date modified</source>
        <translation>Letzte Änderung</translation>
    </message>
</context>
<context>
    <name>Flyer</name>
    <message>
        <location filename="../../flyer.cpp" line="56"/>
        <location filename="../../flyer.cpp" line="57"/>
        <source>Game flyer image</source>
        <translation>Spiel Flyer-Bild</translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Warte auf Daten...</translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="68"/>
        <location filename="../../flyer.cpp" line="72"/>
        <source>FATAL: can&apos;t open flyer file, please check access permissions for %1</source>
        <translation>FATAL: kann Flyer-Datei nicht öffnen; bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>In Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="59"/>
        <location filename="../../flyer.cpp" line="60"/>
        <source>Machine flyer image</source>
        <translation>Machinen Flyer-Bild</translation>
    </message>
</context>
<context>
    <name>Gamelist</name>
    <message>
        <location filename="../../gamelist.cpp" line="387"/>
        <location filename="../../gamelist.cpp" line="389"/>
        <location filename="../../gamelist.cpp" line="392"/>
        <location filename="../../gamelist.cpp" line="394"/>
        <location filename="../../gamelist.cpp" line="1575"/>
        <location filename="../../gamelist.cpp" line="1838"/>
        <location filename="../../gamelist.cpp" line="2115"/>
        <location filename="../../gamelist.cpp" line="2951"/>
        <source>Waiting for data...</source>
        <translation>Warte auf Daten...</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="421"/>
        <source>determining emulator version and supported sets</source>
        <translation>Ermittle Emulator Version und unterstützte Sets</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="471"/>
        <location filename="../../gamelist.cpp" line="567"/>
        <source>FATAL: can&apos;t start MAME executable within a reasonable time frame, giving up</source>
        <translation>FATAL: kann ausführbare MAME Datei nicht in einem angemessenen Zeitraum starten, gebe auf</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="473"/>
        <location filename="../../gamelist.cpp" line="569"/>
        <source>FATAL: can&apos;t start MESS executable within a reasonable time frame, giving up</source>
        <translation>FATAL: kann ausführbare MESS Datei nicht in einem angemessenen Zeitraum starten, gebe auf</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="596"/>
        <source>done (determining emulator version and supported sets, elapsed time = %1)</source>
        <translation>Fertig (Ermittle Emulator Version und unterstützte Sets, benötigte Zeit = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="621"/>
        <source>%n supported set(s)</source>
        <translation>
            <numerusform>%n unterstütztes Set</numerusform>
            <numerusform>%n unterstützte Sets</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="623"/>
        <source>FATAL: couldn&apos;t determine the number of supported sets</source>
        <translation>FATAL: konnte die Anzahl unterstützter Sets nicht ermitteln</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Source file</source>
        <translation>Quelldatei</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Clone of</source>
        <translation>Klon von</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>ROM of</source>
        <translation>ROM von</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1036"/>
        <source>Year</source>
        <translation>Jahr</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1043"/>
        <source>Manufacturer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1048"/>
        <location filename="../../gamelist.cpp" line="1475"/>
        <location filename="../../gamelist.cpp" line="1739"/>
        <source>ROM</source>
        <translation>ROM</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>CRC</source>
        <translation>CRC</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>SHA1</source>
        <translation>SHA1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Region</source>
        <translation>Region</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1063"/>
        <source>Chip</source>
        <translation>Chip</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1068"/>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1068"/>
        <source>Clock</source>
        <translation>Taktung</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Width</source>
        <translation>Breite</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Height</source>
        <translation>Höhe</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Refresh</source>
        <translation>Aktualisierung</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1082"/>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Sound</source>
        <translation>Sound</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1086"/>
        <source>Channels</source>
        <translation>Kanäle</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1091"/>
        <source>Input</source>
        <translation>Eingabe</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Players</source>
        <translation>Spieler</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Buttons</source>
        <translation>Feuerknöpfe</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Coins</source>
        <translation>Münzen</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Service</source>
        <translation>Service</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1157"/>
        <source>Driver</source>
        <translation>Treiber</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1161"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <location filename="../../gamelist.cpp" line="1206"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Emulation</source>
        <translation>Emulation</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Graphic</source>
        <translation>Grafik</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Save state</source>
        <translation>Zustand speichern</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Palette size</source>
        <translation>Größe der Farbpalette</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>good</source>
        <translation>gut</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>bad</source>
        <translation>schlecht</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>unsupported</source>
        <translation>nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>supported</source>
        <translation>unterstützt</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>imperfect</source>
        <translation>unvollständig</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>preliminary</source>
        <translation>vorläufig</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2468"/>
        <source>L:</source>
        <translation>L:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2469"/>
        <source>C:</source>
        <translation>K:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2471"/>
        <source>I:</source>
        <translation>I:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2472"/>
        <source>N:</source>
        <translation>N:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2473"/>
        <source>U:</source>
        <translation>U:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2474"/>
        <source>S:</source>
        <translation>S:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1114"/>
        <source>DIP switch</source>
        <translation>DIP Schalter</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1121"/>
        <source>DIP value</source>
        <translation>DIP Wert</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1126"/>
        <location filename="../../gamelist.cpp" line="1150"/>
        <location filename="../../gamelist.cpp" line="1171"/>
        <location filename="../../gamelist.cpp" line="1196"/>
        <location filename="../../gamelist.cpp" line="1223"/>
        <location filename="../../gamelist.cpp" line="1273"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>no</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>baddump</source>
        <translation>schlechter Dump</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>nodump</source>
        <translation>kein Dump</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>BIOS</source>
        <translation>BIOS</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Merge</source>
        <translation>Merge</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Tilt</source>
        <translation>Tilt</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Cocktail</source>
        <translation>Cocktail</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Protection</source>
        <translation>Schutz</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1166"/>
        <source>BIOS set</source>
        <translation>BIOS Set</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1171"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1176"/>
        <source>Sample</source>
        <translation>Sample</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1181"/>
        <source>Disk</source>
        <translation>Festplatte</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>MD5</source>
        <translation>MD5</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Sample of</source>
        <translation>Sample von</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2135"/>
        <location filename="../../gamelist.cpp" line="2151"/>
        <source>restoring game selection</source>
        <translation>Restauriere Spiel-Selektion</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1313"/>
        <source>loading ROM state from cache</source>
        <translation>Lade ROM Status aus dem Cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1310"/>
        <source>WARNING: can&apos;t open ROM state cache, please check ROMs</source>
        <translation>WARNUNG: kann ROM-Status Cache nicht öffnen, bitte ROMs verifizieren</translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2169"/>
        <source>%n machine(s)</source>
        <translation>
            <numerusform>%n Maschine</numerusform>
            <numerusform>%n Maschinen</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2208"/>
        <source>WARNING: ROM state cache is incomplete or not up to date, please re-check ROMs</source>
        <translation>WARNUNG: ROM-Status Cache is nicht vollständig oder nicht aktuell, bitte ROMs neu verifizieren</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2331"/>
        <source>loading favorites</source>
        <translation>Lade Favoriten</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2355"/>
        <source>done (loading favorites)</source>
        <translation>Fertig (Lade Favoriten)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2368"/>
        <source>saving favorites</source>
        <translation>Speichere Favoriten</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2390"/>
        <source>done (saving favorites)</source>
        <translation>Fertig (Speichere Favoriten)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2399"/>
        <source>loading play history</source>
        <translation>Lade Spiel-Historie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2422"/>
        <source>done (loading play history)</source>
        <translation>Fertig (Lade Spiel-Historie)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2435"/>
        <source>saving play history</source>
        <translation>Speichere Spiel-Historie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2457"/>
        <source>done (saving play history)</source>
        <translation>Fertig (Speichere Spiel-Historie)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2384"/>
        <location filename="../../gamelist.cpp" line="2386"/>
        <source>FATAL: can&apos;t open favorites file for writing, path = %1</source>
        <translation>FATAL: kann Spiel-Favoriten Datei nicht zum Schreiben öffnen, Pfad = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2451"/>
        <location filename="../../gamelist.cpp" line="2453"/>
        <source>FATAL: can&apos;t open play history file for writing, path = %1</source>
        <translation>FATAL: kann Spiel-Historien Datei nicht zum Schreiben öffnen, Pfad = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <source>vertical</source>
        <translation>vertikal</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <source>horizontal</source>
        <translation>horizontal</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <source>raster</source>
        <translation>Raster</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <location filename="../../gamelist.cpp" line="1492"/>
        <location filename="../../gamelist.cpp" line="1755"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>On</source>
        <translation>An</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>Off</source>
        <translation>Aus</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>Unused</source>
        <translation>Unbenutzt</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2101"/>
        <location filename="../../gamelist.cpp" line="2103"/>
        <location filename="../../gamelist.cpp" line="2937"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>ascending</source>
        <translation>aufsteigend</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2101"/>
        <location filename="../../gamelist.cpp" line="2103"/>
        <location filename="../../gamelist.cpp" line="2937"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>descending</source>
        <translation>absteigend</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2470"/>
        <source>M:</source>
        <translation>B:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2228"/>
        <source>ROM state filter already active</source>
        <translation>ROM Status Filter bereits aktiv</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2247"/>
        <source>applying ROM state filter</source>
        <translation>Filtere ROM Status</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2304"/>
        <source>done (applying ROM state filter, elapsed time = %1)</source>
        <translation>Fertig (Filtere ROM Status, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2233"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation>Bitte warte bis die ROM Verifikation abgeschlossen wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2238"/>
        <source>please wait for reload to finish and try again</source>
        <translation>Bitte warte bis die Spieleliste aktualisiert wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1337"/>
        <source>done (loading ROM state from cache, elapsed time = %1)</source>
        <translation>Fertig (Lade ROM Status aus dem Cache, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="165"/>
        <location filename="../../gamelist.cpp" line="169"/>
        <source>FATAL: can&apos;t open icon file, please check access permissions for %1</source>
        <translation>FATAL: kann Icon-Archiv nicht öffnen, bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="887"/>
        <source>verifying ROM status for all games</source>
        <translation>Prüfe ROM Status für alle Spiele</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2918"/>
        <source>done (verifying ROM status for all games, elapsed time = %1)</source>
        <translation>Fertig (Prüfe ROM Status für alle Spiele, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3293"/>
        <source>pre-caching icons from ZIP archive</source>
        <translation>Lade Icons aus ZIP Archiv</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3349"/>
        <source>done (pre-caching icons from ZIP archive, elapsed time = %1)</source>
        <translation>Fertig (Lade Icons aus ZIP Archiv, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3363"/>
        <source>pre-caching icons from directory</source>
        <translation>Lade Icons aus Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3416"/>
        <source>done (pre-caching icons from directory, elapsed time = %1)</source>
        <translation>Fertig (Lade Icons aus Verzeichnis, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <source>FATAL: couldn&apos;t determine supported games</source>
        <translation type="obsolete">FATAL: konnte unterstützte Spiele nicht ermitteln</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="879"/>
        <location filename="../../gamelist.cpp" line="901"/>
        <source>ERROR: can&apos;t open ROM state cache for writing, path = %1</source>
        <translation>FEHLER: Kann ROM-Status Cache nicht zum Schreiben öffnen, Pfad = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="845"/>
        <source>verifying ROM status for &apos;%1&apos;</source>
        <translation>Prüfe ROM Status für &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="989"/>
        <source>retrieving game information for &apos;%1&apos;</source>
        <translation>Ermittle Spiel-Informationen für &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2915"/>
        <source>done (verifying ROM status for &apos;%1&apos;, elapsed time = %2)</source>
        <translation>Fertig (Prüfe ROM Status für &apos;%1&apos;; benötigte Zeit = %2)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3203"/>
        <source>ROM status for &apos;%1&apos; is &apos;%2&apos;</source>
        <translation>ROM Status für &apos;%1&apos; ist &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="129"/>
        <location filename="../../gamelist.cpp" line="152"/>
        <location filename="../../gamelist.cpp" line="510"/>
        <location filename="../../gamelist.cpp" line="514"/>
        <location filename="../../gamelist.cpp" line="515"/>
        <location filename="../../gamelist.cpp" line="525"/>
        <location filename="../../gamelist.cpp" line="529"/>
        <location filename="../../gamelist.cpp" line="530"/>
        <location filename="../../gamelist.cpp" line="535"/>
        <location filename="../../gamelist.cpp" line="536"/>
        <location filename="../../gamelist.cpp" line="602"/>
        <location filename="../../gamelist.cpp" line="605"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="509"/>
        <source>FATAL: selected executable file is not MAME</source>
        <translation>FATAL: gewählte ausführbare Datei ist nicht MAME</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="668"/>
        <source>XML cache - %p%</source>
        <translation>XML Cache - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1315"/>
        <source>ROM states - %p%</source>
        <translation>ROM Stati - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1417"/>
        <location filename="../../gamelist.cpp" line="1632"/>
        <source>Game data - %p%</source>
        <translation>Spieldaten - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2250"/>
        <source>State filter - %p%</source>
        <translation>Status Filter - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="908"/>
        <source>ROM check - %p%</source>
        <translation>ROM Prüfung - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="781"/>
        <source>XML data - %p%</source>
        <translation>XML Daten - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2937"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>ROM state</source>
        <translation>ROM-Status</translation>
    </message>
    <message>
        <source>determining emulator version and supported games</source>
        <translation type="obsolete">Ermittle Emulator Version und unterstützte Spiele</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="534"/>
        <location filename="../../gamelist.cpp" line="598"/>
        <source>FATAL: can&apos;t create temporary file, please check emulator executable and permissions</source>
        <translation>FATAL: kann temporäre Arbeitsdatei nicht erzeugen, bitte ausführbare Emulator Datei und Zugriffsrechte prüfen</translation>
    </message>
    <message>
        <source>done (determining emulator version and supported games, elapsed time = %1)</source>
        <translation type="obsolete">Fertig (Ermittle Emulator Version und unterstützte Spiele, benötigte Zeit = %1)</translation>
    </message>
    <message numerus="yes">
        <source>%n supported game(s)</source>
        <translation type="obsolete">
            <numerusform>%n unterstütztes Spiel</numerusform>
            <numerusform>%n unterstützte Spiele</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="1338"/>
        <source>%n cached ROM state(s) loaded</source>
        <translation>
            <numerusform>%n ROM Status aus dem Cache geladen</numerusform>
            <numerusform>%n ROM Stati aus dem Cache geladen</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n game(s) loaded</source>
        <translation type="obsolete">
            <numerusform>%n Spiel geladen</numerusform>
            <numerusform>%n Spiele geladen</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="3350"/>
        <location filename="../../gamelist.cpp" line="3417"/>
        <source>%n icon(s) loaded</source>
        <translation>
            <numerusform>%n Icon geladen</numerusform>
            <numerusform>%n Icons geladen</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3299"/>
        <location filename="../../gamelist.cpp" line="3378"/>
        <source>Icon cache - %p%</source>
        <translation>Icon Cache - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>unused</source>
        <translation>unbenutzt</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>cpu</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>vector</source>
        <translation>Vektor</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>lcd</source>
        <translation>LCD</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="524"/>
        <source>FATAL: selected executable file is not MESS</source>
        <translation>FATAL: gewählte ausführbare Datei ist nicht MESS</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Is BIOS?</source>
        <translation>Ist BIOS?</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Runnable</source>
        <translation>Ausführbar</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1073"/>
        <source>Display</source>
        <translation>Anzeige</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Rotate</source>
        <translation>Rotieren</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Flip-X</source>
        <translation>Flip-X</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Pixel clock</source>
        <translation>Pixel Clock</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>H-Total</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>H-Bend</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>HB-Start</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>V-Total</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>V-Bend</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>VB-Start</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1068"/>
        <location filename="../../gamelist.cpp" line="1138"/>
        <location filename="../../gamelist.cpp" line="1235"/>
        <source>Tag</source>
        <translation>Markierung</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1102"/>
        <source>Control</source>
        <translation>Steuerung</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Sensitivity</source>
        <translation>Empfindlichkeit</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Key Delta</source>
        <translation>Tasten Delta</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Reverse</source>
        <translation>Umkehrung</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>joy4way</source>
        <translation>4-Wege-Joystick</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>joy8way</source>
        <translation>8-Wege-Joystick</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>trackball</source>
        <translation>Trackball</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>joy2way</source>
        <translation>2-Wege-Joystick</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>doublejoy8way</source>
        <translation>Doppelter 8-Wege-Joystick</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>dial</source>
        <translation>Lenkrad</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>paddle</source>
        <translation>Paddel</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>pedal</source>
        <translation>Pedal</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>stick</source>
        <translation>Joystick</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>vjoy2way</source>
        <translation>Vertikaler 2-Wege-Joystick</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>lightgun</source>
        <translation>Lichtpistole</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>doublejoy4way</source>
        <translation>Doppelter 4-Wege-Joystick</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>vdoublejoy2way</source>
        <translation>Doppelter vertikaler 2-Wege-Joystick</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>doublejoy2way</source>
        <translation>Doppelter 2-Wege-Joystick</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>printer</source>
        <translation>Drucker</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>cdrom</source>
        <translation>CD-ROM</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>cartridge</source>
        <translation>Steckmodul</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>cassette</source>
        <translation>Kassette</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>quickload</source>
        <translation>Schnelllader</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>floppydisk</source>
        <translation>Floppy Disk</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>serial</source>
        <translation>Serieller Port</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>snapshot</source>
        <translation>Schnappschuss</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1230"/>
        <source>Device</source>
        <translation>Gerät</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1242"/>
        <source>Instance</source>
        <translation>Instanz</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1247"/>
        <source>Brief name</source>
        <translation>Kurzname</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1254"/>
        <source>Extension</source>
        <translation>Erweiterung</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1235"/>
        <source>Mandatory</source>
        <translation>Erforderlich</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>original</source>
        <translation>original</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="159"/>
        <source>compatible</source>
        <translation>kompatibel</translation>
    </message>
    <message>
        <source>determining emulator version and supported machines</source>
        <translation type="obsolete">Ermittle Emulator Version und unterstützte Maschinen</translation>
    </message>
    <message>
        <source>done (determining emulator version and supported machines, elapsed time = %1)</source>
        <translation type="obsolete">Fertig (Ermittle Emulator Version und unterstützte Maschinen, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="603"/>
        <source>emulator info: type = %1, version = %2</source>
        <translation>Emulator Info: Typ = %1, Version = %2</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="606"/>
        <source>FATAL: couldn&apos;t determine emulator type and version</source>
        <translation>FATAL: konnte Emulator-Typ und ~-Version nicht ermitteln</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="609"/>
        <source>FATAL: couldn&apos;t determine emulator version, type identification string is &apos;%1&apos; -- please inform developers if you&apos;re sure that this is a valid MAME binary</source>
        <translation>FATAL: konnte Emulator-Version nicht ermitteln, Typ-Identifikationszeichenkette ist &apos;%1&apos; -- bitte Entwickler informieren, sofern sicher ist, dass dies eine gültige MAME Version ist</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="611"/>
        <source>FATAL: couldn&apos;t determine emulator version, type identification string is &apos;%1&apos; -- please inform developers if you&apos;re sure that this is a valid MESS binary</source>
        <translation>FATAL: konnte Emulator-Version nicht ermitteln, Typ-Identifikationszeichenkette ist &apos;%1&apos; -- bitte Entwickler informieren, sofern sicher ist, dass dies eine gültige MESS Version ist</translation>
    </message>
    <message numerus="yes">
        <source>%n supported machine(s)</source>
        <translation type="obsolete">
            <numerusform>%n unterstützte Maschine</numerusform>
            <numerusform>%n unterstützte Maschinen</numerusform>
        </translation>
    </message>
    <message>
        <source>FATAL: couldn&apos;t determine supported machines</source>
        <translation type="obsolete">FATAL: konnte unterstützte Maschinen nicht ermitteln</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="663"/>
        <source>loading XML game list data from cache</source>
        <translation>Lade XML Spielelisten Daten aus dem Cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="665"/>
        <source>loading XML machine list data from cache</source>
        <translation>Lade XML Maschinenlisten Daten aus dem Cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="706"/>
        <source>done (loading XML game list data from cache, elapsed time = %1)</source>
        <translation>Fertig (Lade XML Spielelisten Daten aus dem Cache, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="708"/>
        <source>WARNING: XML game list cache is incomplete, invalidating XML game list cache</source>
        <translation>WARNUNG: XML Spielelisten Cache ist unvollständig, invalidiere XML Spielelisten Cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="710"/>
        <source>done (loading XML machine list data from cache, elapsed time = %1)</source>
        <translation>Fertig (Lade XML Maschinenlisten Daten aus dem Cache, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="712"/>
        <source>WARNING: XML machine list cache is incomplete, invalidating XML machine list cache</source>
        <translation>WARNUNG: XML Maschinenlisten Cache ist unvollständig, invalidiere XML Maschinenlisten Cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="776"/>
        <source>loading XML game list data and (re)creating cache</source>
        <translation>Lade XML Spielelisten Daten und erzeuge Cache neu</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="778"/>
        <source>loading XML machine list data and (re)creating cache</source>
        <translation>Lade XML Maschinenlisten Daten und erzeuge Cache neu</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="798"/>
        <source>WARNING: can&apos;t open XML game list cache for writing, please check permissions</source>
        <translation>WARNUNG: kann XML Spielelisten Cache nicht zum Schreiben öffnen, bitte Zugriffsrechte prüfen</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="800"/>
        <source>WARNING: can&apos;t open XML machine list cache for writing, please check permissions</source>
        <translation>WARNUNG: kann XML Maschinenlisten Cache nicht zum Schreiben öffnen, bitte Zugriffsrechte prüfen</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="889"/>
        <source>verifying ROM status for all machines</source>
        <translation>Prüfe ROM Status für alle Maschinen</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="991"/>
        <source>retrieving machine information for &apos;%1&apos;</source>
        <translation>Ermittle Maschinen-Informationen für &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1006"/>
        <source>WARNING: couldn&apos;t find game information for &apos;%1&apos;</source>
        <translation>WARNUNG: konnte keine Spiel-Informationen für &apos;%1&apos; finden</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1008"/>
        <source>WARNING: couldn&apos;t find machine information for &apos;%1&apos;</source>
        <translation>WARNUNG: konnte keine Maschinen-Informationen für &apos;%1&apos; finden</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Is device?</source>
        <translation>Ist Gerät?</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Optional</source>
        <translation>Optional</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1058"/>
        <source>Device reference</source>
        <translation>Geräte-Referenz</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1133"/>
        <source>Configuration</source>
        <translation>Konfiguration</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1138"/>
        <source>Mask</source>
        <translation>Maske</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1145"/>
        <source>Setting</source>
        <translation>Einstellung</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1150"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1191"/>
        <source>Adjuster</source>
        <translation>Einsteller</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1201"/>
        <source>Software list</source>
        <translation>Software Liste</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1211"/>
        <source>Category</source>
        <translation>Kategorie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1218"/>
        <source>Item</source>
        <translation>Gegenstand</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1235"/>
        <source>Interface</source>
        <translation>Schnittstelle</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1347"/>
        <source>processing game list</source>
        <translation>Verarbeite Spieleliste</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1349"/>
        <source>processing machine list</source>
        <translation>Verarbeite Maschinenliste</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1384"/>
        <source>WARNING: couldn&apos;t determine emulator version of game list cache</source>
        <translation>WARNUNG: konnte Emulator Version des Spielelisten Caches nicht ermitteln</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1386"/>
        <source>WARNING: couldn&apos;t determine emulator version of machine list cache</source>
        <translation>WARNUNG: konnte Emulator Version des Maschinenlisten Caches nicht ermitteln</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1391"/>
        <location filename="../../gamelist.cpp" line="1400"/>
        <source>INFORMATION: the game list cache will now be updated due to a new format</source>
        <translation>INFORMATION: der Spielelisten Cache wird nun wegen eines neues Formats aktualisiert</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1393"/>
        <location filename="../../gamelist.cpp" line="1402"/>
        <source>INFORMATION: the machine list cache will now be updated due to a new format</source>
        <translation>INFORMATION: der Maschinenlisten Cache wird nun wegen eines neues Formats aktualisiert</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1415"/>
        <source>loading game data from game list cache</source>
        <translation>Lade Spieldaten aus dem Spielelisten Cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1473"/>
        <location filename="../../gamelist.cpp" line="1737"/>
        <source>ROM, CHD</source>
        <translation>ROM, CHD</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1477"/>
        <location filename="../../gamelist.cpp" line="1741"/>
        <source>CHD</source>
        <translation>CHD</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1481"/>
        <location filename="../../gamelist.cpp" line="1482"/>
        <location filename="../../gamelist.cpp" line="1745"/>
        <location filename="../../gamelist.cpp" line="1746"/>
        <source>N/A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1493"/>
        <location filename="../../gamelist.cpp" line="1756"/>
        <location filename="../../gamelist.cpp" line="3549"/>
        <location filename="../../gamelist.cpp" line="3652"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1609"/>
        <source>done (loading game data from game list cache, elapsed time = %1)</source>
        <translation>Fertig (Lade Spieldaten aus dem Spielelisten Cache, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1630"/>
        <source>parsing game data and (re)creating game list cache</source>
        <translation>Lese Spieldaten und erzeuge Spielelisten Cache neu</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1634"/>
        <source>parsing machine data and (re)creating machine list cache</source>
        <translation>Lese Maschinendaten und erzeuge Maschinenlisten Cache neu</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1643"/>
        <source>ERROR: can&apos;t open game list cache for writing, path = %1</source>
        <translation>FEHLER: Kann Spielelisten Cache nicht zum Schreiben öffnen, Pfad = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1645"/>
        <source>ERROR: can&apos;t open machine list cache for writing, path = %1</source>
        <translation>FEHLER: Kann Maschinenlisten Cache nicht zum Schreiben öffnen, Pfad = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2101"/>
        <location filename="../../gamelist.cpp" line="2937"/>
        <source>sorting game list by %1 in %2 order</source>
        <translation>Sortiere Spieleliste nach %1 in %2er Reihenfolge</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2103"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>sorting machine list by %1 in %2 order</source>
        <translation>Sortiere Maschinenliste nach %1 in %2er Reihenfolge</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2137"/>
        <location filename="../../gamelist.cpp" line="2153"/>
        <source>restoring machine selection</source>
        <translation>Restauriere Maschinen-Selektion</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2165"/>
        <source>done (processing game list, elapsed time = %1)</source>
        <translation>Fertig (Verarbeite Spieleliste, benötigte Zeit = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2166"/>
        <source>%n game(s)</source>
        <translation>
            <numerusform>%n Spiel</numerusform>
            <numerusform>%n Spiele</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2166"/>
        <location filename="../../gamelist.cpp" line="2169"/>
        <source> and %n device(s) loaded</source>
        <translation>
            <numerusform> und %n Gerät geladen</numerusform>
            <numerusform> und %n Geräte geladen</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2166"/>
        <location filename="../../gamelist.cpp" line="2169"/>
        <source>, %n BIOS set(s)</source>
        <translation>
            <numerusform>, %n BIOS Set</numerusform>
            <numerusform>, %n BIOS Sets</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2168"/>
        <source>done (processing machine list, elapsed time = %1)</source>
        <translation>Fertig (Verarbeite Maschinenliste, benötigte Zeit = %1)</translation>
    </message>
    <message numerus="yes">
        <source>%n machine(s) loaded</source>
        <translation type="obsolete">
            <numerusform>%n Maschine geladen</numerusform>
            <numerusform>%n Maschinen geladen</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2175"/>
        <source>WARNING: game list not fully parsed, invalidating game list cache</source>
        <translation>WARNUNG: Spieleliste nicht vollständig gelesen, invalidiere Spielelisten Cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2178"/>
        <source>WARNING: machine list not fully parsed, invalidating machine list cache</source>
        <translation>WARNUNG: Maschinenliste nicht vollständig gelesen, invalidiere Maschinenlisten Cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2184"/>
        <source>WARNING: game list cache is out of date, invalidating game list cache</source>
        <translation>WARNUNG: Spielelisten Cache ist nicht aktuell, invalidiere Spielelisten Cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2187"/>
        <source>WARNING: machine list cache is out of date, invalidating machine list cache</source>
        <translation>WARNUNG: Maschinenlisten Cache ist nicht aktuell, invalidiere Maschinenlisten Cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2193"/>
        <location filename="../../gamelist.cpp" line="2923"/>
        <source>ROM state info: L:%1 C:%2 M:%3 I:%4 N:%5 U:%6</source>
        <translation>ROM Status Info: L:%1 K:%2 B:%3 I:%4 N:%5 U:%6</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2205"/>
        <source>WARNING: ROM state cache is incomplete or not up to date, triggering an automatic ROM check</source>
        <translation>WARNUNG: ROM-Status Cache is nicht vollständig oder nicht aktuell, automatsche ROM Prüfung aktiviert</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2317"/>
        <source>saving game list</source>
        <translation>Speichere Spieleliste</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2318"/>
        <source>done (saving game list)</source>
        <translation>Fertig (Speichere Spieleliste)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2320"/>
        <source>saving machine list</source>
        <translation>Speichere Maschinenliste</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2321"/>
        <source>done (saving machine list)</source>
        <translation>Fertig (Speichere Maschinenliste)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2475"/>
        <source>T:</source>
        <translation>M:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2504"/>
        <source>done (loading XML game list data and (re)creating cache, elapsed time = %1)</source>
        <translation>Fertig (Lade XML Spielelisten Daten und erzeuge Cache neu, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2506"/>
        <source>done (loading XML machine list data and (re)creating cache, elapsed time = %1)</source>
        <translation>Fertig (Lade XML Maschinenlisten Daten und erzeuge Cache neu, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2920"/>
        <source>done (verifying ROM status for all machines, elapsed time = %1)</source>
        <translation>Fertig (Prüfe ROM Status für alle Maschinen, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3445"/>
        <source>loading catver.ini</source>
        <translation>Lade catver.ini</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3451"/>
        <source>Catver.ini - %p%</source>
        <translation>Catver.ini - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3489"/>
        <source>ERROR: can&apos;t open &apos;%1&apos; for reading -- no catver.ini data available</source>
        <translation>FEHLER: kann &apos;%1&apos; nicht zum Lesen öffnen -- keine catver.ini Daten verfügbar</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3498"/>
        <source>done (loading catver.ini, elapsed time = %1)</source>
        <translation>Fertig (Lade catver.ini, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3499"/>
        <source>%1 category / %2 version records loaded</source>
        <translation>%1 Kategorie- / %2 Versions-Datensätze geladen</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3532"/>
        <source>Category view - %p%</source>
        <translation>Kategorieansicht - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3635"/>
        <source>Version view - %p%</source>
        <translation>Versionsansicht - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1419"/>
        <source>loading machine data from machine list cache</source>
        <translation>Lade Maschinendaten aus dem Maschinenlisten Cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1611"/>
        <source>done (loading machine data from machine list cache, elapsed time = %1)</source>
        <translation>Fertig (Lade Maschinendaten aus dem Maschinenlisten Cache, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1421"/>
        <location filename="../../gamelist.cpp" line="1636"/>
        <source>Machine data - %p%</source>
        <translation>Maschinendaten - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1261"/>
        <source>RAM options</source>
        <translation>RAM Optionen</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1265"/>
        <source>Option</source>
        <translation>Option</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2738"/>
        <location filename="../../gamelist.cpp" line="2874"/>
        <location filename="../../gamelist.cpp" line="3188"/>
        <source>WARNING: can&apos;t find item map entry for &apos;%1&apos; - ROM state cannot be determined</source>
        <translation>WARNUNG: kann Eintrag für &apos;%1&apos; in Lookup-Tabelle nicht finden - ROM Status kann nicht ermittelt werden</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2499"/>
        <location filename="../../gamelist.cpp" line="2682"/>
        <source>WARNING: emulator audit call didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation>WARNUNG: Emulator-Aufruf zur Auditierung wurde nicht ordentlich beendet -- exitCode = %1, exitStatus = %2</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2499"/>
        <location filename="../../gamelist.cpp" line="2682"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2499"/>
        <location filename="../../gamelist.cpp" line="2682"/>
        <source>crashed</source>
        <translation>abgestürzt</translation>
    </message>
    <message>
        <source>ROM state info: C:%1 M:%2 I:%3 N:%4 U:%5</source>
        <translation type="obsolete">ROM Status Info: K:%1 B:%2 I:%3 N:%4 U:%5</translation>
    </message>
</context>
<context>
    <name>ImageChecker</name>
    <message>
        <location filename="../../imgcheck.ui" line="15"/>
        <source>Check images</source>
        <translation>Bilder überprüfen</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="56"/>
        <source>&amp;Previews</source>
        <translation>&amp;Vorschaubilder</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="164"/>
        <source>Remove obsolete preview images</source>
        <translation>Überflüssige Vorschaubild-Dateien entfernen</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="167"/>
        <location filename="../../imgcheck.ui" line="266"/>
        <location filename="../../imgcheck.ui" line="381"/>
        <source>&amp;Remove obsolete</source>
        <translation>Überflüssige &amp;entfernen</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="112"/>
        <source>Check preview images / stop check</source>
        <translation>Vorschaubilder überprüfen / Prüfung anhalten</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="115"/>
        <location filename="../../imgcheck.cpp" line="287"/>
        <location filename="../../imgcheck.cpp" line="611"/>
        <location filename="../../imgcheck.cpp" line="938"/>
        <source>&amp;Check previews</source>
        <translation>Vorschaubilber &amp;prüfen</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="263"/>
        <source>Remove obsolete flyer images</source>
        <translation>Überflüssige Fyler-Dateien entfernen</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="250"/>
        <source>Check flyer images / stop check</source>
        <translation>Flyer überprüfen / Prüfung anhalten</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="253"/>
        <location filename="../../imgcheck.cpp" line="288"/>
        <location filename="../../imgcheck.cpp" line="612"/>
        <location filename="../../imgcheck.cpp" line="939"/>
        <source>&amp;Check flyers</source>
        <translation>Flyer &amp;prüfen</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="309"/>
        <source>&amp;Icons</source>
        <translation>&amp;Icons</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="378"/>
        <source>Remove obsolete icon images</source>
        <translation>Überflüssige Icons löschen</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="365"/>
        <source>Check icon images / stop check</source>
        <translation>Icon überprüfen / Prüfung anhalten</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="368"/>
        <location filename="../../imgcheck.cpp" line="289"/>
        <location filename="../../imgcheck.cpp" line="613"/>
        <location filename="../../imgcheck.cpp" line="940"/>
        <source>&amp;Check icons</source>
        <translation>Icons &amp;prüfen</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="36"/>
        <source>Close image check dialog</source>
        <translation>Bild-Prüfungs Dialog schließen</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="39"/>
        <source>C&amp;lose</source>
        <translation>Sch&amp;ließen</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1072"/>
        <source>please wait for reload to finish and try again</source>
        <translation>Bitte warte bis die Spieleliste aktualisiert wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1082"/>
        <source>please wait for ROM state filter to finish and try again</source>
        <translation>Bitte warte bis der ROM Status gefiltert wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1087"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation>Bitte warte bis die ROM Verifikation abgeschlossen wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1092"/>
        <source>please wait for sample check to finish and try again</source>
        <translation>Bitte warte bis die Überprüfung der Samples abgeschlossen wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="181"/>
        <source>&amp;Flyers</source>
        <translation>&amp;Flyer</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="104"/>
        <source>checking previews from ZIP archive</source>
        <translation>Prüfe Vorschaubilder aus ZIP Archiv</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="106"/>
        <source>checking previews from directory</source>
        <translation>Prüfe Vorschaubilder aus Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="123"/>
        <location filename="../../imgcheck.cpp" line="124"/>
        <location filename="../../imgcheck.cpp" line="125"/>
        <location filename="../../imgcheck.cpp" line="447"/>
        <location filename="../../imgcheck.cpp" line="448"/>
        <location filename="../../imgcheck.cpp" line="449"/>
        <location filename="../../imgcheck.cpp" line="773"/>
        <location filename="../../imgcheck.cpp" line="774"/>
        <location filename="../../imgcheck.cpp" line="775"/>
        <source>&amp;Stop check</source>
        <translation>&amp;Anhalten</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="70"/>
        <location filename="../../imgcheck.ui" line="243"/>
        <location filename="../../imgcheck.ui" line="323"/>
        <location filename="../../imgcheck.cpp" line="137"/>
        <location filename="../../imgcheck.cpp" line="461"/>
        <location filename="../../imgcheck.cpp" line="787"/>
        <source>Found: 0</source>
        <translation>Gefunden: 0</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="83"/>
        <location filename="../../imgcheck.ui" line="230"/>
        <location filename="../../imgcheck.ui" line="336"/>
        <location filename="../../imgcheck.cpp" line="139"/>
        <location filename="../../imgcheck.cpp" line="463"/>
        <location filename="../../imgcheck.cpp" line="789"/>
        <source>Missing: 0</source>
        <translation>Fehlend: 0</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="96"/>
        <location filename="../../imgcheck.ui" line="217"/>
        <location filename="../../imgcheck.ui" line="349"/>
        <location filename="../../imgcheck.cpp" line="141"/>
        <location filename="../../imgcheck.cpp" line="465"/>
        <location filename="../../imgcheck.cpp" line="791"/>
        <source>Obsolete: 0</source>
        <translation>Überflüssig: 0</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="146"/>
        <source>check pass 1: found and missing previews</source>
        <translation>Prüf-Durchgang 1: gefundene und fehlende Vorschaubilder</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="154"/>
        <location filename="../../imgcheck.cpp" line="176"/>
        <location filename="../../imgcheck.cpp" line="478"/>
        <location filename="../../imgcheck.cpp" line="500"/>
        <location filename="../../imgcheck.cpp" line="804"/>
        <location filename="../../imgcheck.cpp" line="826"/>
        <source>Found: %1</source>
        <translation>Gefunden: %1</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="156"/>
        <location filename="../../imgcheck.cpp" line="179"/>
        <location filename="../../imgcheck.cpp" line="480"/>
        <location filename="../../imgcheck.cpp" line="503"/>
        <location filename="../../imgcheck.cpp" line="806"/>
        <location filename="../../imgcheck.cpp" line="829"/>
        <source>Missing: %1</source>
        <translation>Fehlend: %1</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="219"/>
        <location filename="../../imgcheck.cpp" line="232"/>
        <location filename="../../imgcheck.cpp" line="252"/>
        <location filename="../../imgcheck.cpp" line="261"/>
        <location filename="../../imgcheck.cpp" line="543"/>
        <location filename="../../imgcheck.cpp" line="556"/>
        <location filename="../../imgcheck.cpp" line="576"/>
        <location filename="../../imgcheck.cpp" line="585"/>
        <location filename="../../imgcheck.cpp" line="870"/>
        <location filename="../../imgcheck.cpp" line="883"/>
        <location filename="../../imgcheck.cpp" line="903"/>
        <location filename="../../imgcheck.cpp" line="912"/>
        <source>Obsolete: %1</source>
        <translation>Überflüssig: %1</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="278"/>
        <source>done (checking previews from ZIP archive, elapsed time = %1)</source>
        <translation>Fertig (Prüfe Vorschaubilder aus ZIP Archiv, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="280"/>
        <source>done (checking previews from directory, elapsed time = %1)</source>
        <translation>Fertig (Prüfe Vorschaubilder aus Verzeichnis, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="281"/>
        <location filename="../../imgcheck.cpp" line="605"/>
        <location filename="../../imgcheck.cpp" line="932"/>
        <source>%1 found, %2 missing, %3 obsolete</source>
        <translation>%1 gefunden, %2 fehlend, %3 überflüssig</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="428"/>
        <source>checking flyers from ZIP archive</source>
        <translation>Prüfe Flyer aus ZIP Archiv</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="430"/>
        <source>checking flyers from directory</source>
        <translation>Prüfe Flyer aus Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="470"/>
        <source>check pass 1: found and missing flyers</source>
        <translation>Prüf-Durchgang 1: gefundene und fehlende Flyer</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="602"/>
        <source>done (checking flyers from ZIP archive, elapsed time = %1)</source>
        <translation>Fertig (Prüfe Flyer aus ZIP Archiv, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="604"/>
        <source>done (checking flyers from directory, elapsed time = %1)</source>
        <translation>Fertig (Prüfe Flyer aus Verzeichnis, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1097"/>
        <source>stopping image check upon user request</source>
        <translation>Beende Bild-Prüfung auf Wunsch des Benutzers</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="125"/>
        <location filename="../../imgcheck.ui" line="279"/>
        <location filename="../../imgcheck.ui" line="391"/>
        <source>Select &amp;game</source>
        <translation>Spiel &amp;auswählen</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="754"/>
        <source>checking icons from ZIP archive</source>
        <translation>Prüfe Icons aus ZIP Archiv</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="756"/>
        <source>checking icons from directory</source>
        <translation>Prüfe Icons aus Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="796"/>
        <source>check pass 1: found and missing icons</source>
        <translation>Prüf-Durchgang 1: gefundene und fehlende Icons</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="929"/>
        <source>done (checking icons from ZIP archive, elapsed time = %1)</source>
        <translation>Fertig (Prüfe Icons aus ZIP Archiv, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="931"/>
        <source>done (checking icons from directory, elapsed time = %1)</source>
        <translation>Fertig (Prüfe Icons aus Verzeichnis, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="401"/>
        <source>Clear cache before checking icons?</source>
        <translation>Cache vor Überprüfung der Icons leeren?</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="404"/>
        <source>C&amp;lear cache</source>
        <translation>Cache &amp;leeren</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="187"/>
        <location filename="../../imgcheck.cpp" line="511"/>
        <location filename="../../imgcheck.cpp" line="837"/>
        <source>check pass 2: obsolete files: reading ZIP directory</source>
        <translation>Prüf-Durchgang 2: überflüssige Dateien: lese ZIP Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="236"/>
        <location filename="../../imgcheck.cpp" line="560"/>
        <location filename="../../imgcheck.cpp" line="887"/>
        <source>check pass 2: obsolete files: reading directory structure</source>
        <translation>Prüf-Durchgang 2: überflüssige Dateien: lese Verzeichnisstruktur</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="109"/>
        <source>Preview check - %p%</source>
        <translation>Vorschau Prüfung - %p%</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="433"/>
        <source>Flyer check - %p%</source>
        <translation>Flyer Prüfung - %p%</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="759"/>
        <source>Icon check - %p%</source>
        <translation>Icon Prüfung - %p%</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1077"/>
        <source>please wait for ROMAlyzer to finish the current analysis and try again</source>
        <translation>Bitte warte bis der ROMAlyzer die aktuelle Analyse abgeschlossen hat und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="70"/>
        <location filename="../../imgcheck.cpp" line="72"/>
        <source>Select machine</source>
        <translation>Maschine selektieren</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="71"/>
        <location filename="../../imgcheck.cpp" line="73"/>
        <source>Select machine in machine list if selected in check lists?</source>
        <translation>Maschine in Machinenliste auswählen, wenn in Prüflisten selektiert?</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="122"/>
        <location filename="../../imgcheck.ui" line="276"/>
        <location filename="../../imgcheck.ui" line="388"/>
        <source>Select game in game list if selected in check lists?</source>
        <translation>Spiel in Spieleliste auswählen, wenn in Prüflisten selektiert?</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="117"/>
        <location filename="../../imgcheck.cpp" line="441"/>
        <location filename="../../imgcheck.cpp" line="767"/>
        <source>WARNING: game list not fully loaded, check results may be misleading</source>
        <translation>WARNUNG: Spieleliste nicht vollständig geladen, Prüfergebnisse könnten irreführend sein</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="119"/>
        <location filename="../../imgcheck.cpp" line="443"/>
        <location filename="../../imgcheck.cpp" line="769"/>
        <source>WARNING: machine list not fully loaded, check results may be misleading</source>
        <translation>WARNUNG: Maschinenliste nicht vollständig geladen, Prüfergebnisse könnten irreführend sein</translation>
    </message>
</context>
<context>
    <name>ItemDownloader</name>
    <message>
        <location filename="../../downloaditem.cpp" line="98"/>
        <source>FATAL: can&apos;t open &apos;%1&apos; for writing</source>
        <translation>FATAL: kann &apos;%1&apos; nicht zum Schreiben öffnen</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="112"/>
        <location filename="../../downloaditem.cpp" line="190"/>
        <location filename="../../downloaditem.cpp" line="207"/>
        <location filename="../../downloaditem.cpp" line="238"/>
        <source>Source URL: %1</source>
        <translation>Quelle (URL): %1</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="113"/>
        <location filename="../../downloaditem.cpp" line="191"/>
        <location filename="../../downloaditem.cpp" line="208"/>
        <location filename="../../downloaditem.cpp" line="239"/>
        <source>Local path: %2</source>
        <translation>Lokaler Pfad: %2</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="114"/>
        <location filename="../../downloaditem.cpp" line="192"/>
        <location filename="../../downloaditem.cpp" line="209"/>
        <location filename="../../downloaditem.cpp" line="240"/>
        <source>Status: %1</source>
        <translation>Status: %1</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="114"/>
        <source>initializing download</source>
        <translation>initialisiere Download</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="115"/>
        <location filename="../../downloaditem.cpp" line="193"/>
        <location filename="../../downloaditem.cpp" line="210"/>
        <location filename="../../downloaditem.cpp" line="241"/>
        <source>Total size: %1</source>
        <translation>Gesamt: %1</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="115"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="116"/>
        <location filename="../../downloaditem.cpp" line="194"/>
        <location filename="../../downloaditem.cpp" line="211"/>
        <location filename="../../downloaditem.cpp" line="242"/>
        <source>Downloaded: %1 (%2%)</source>
        <translation>Geladen: %1 (%2%)</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="117"/>
        <source>download started: URL = %1, local path = %2, reply ID = %3</source>
        <translation>Download gestartet: URL = %1, lokaler Pfad = %2, Reply ID = %3</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="175"/>
        <source>Error #%1: </source>
        <translation>Fehler #%1: </translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="181"/>
        <source>download aborted: reason = %1, URL = %2, local path = %3, reply ID = %4</source>
        <translation>Download abgebrochen: Grund = %1, URL = %2, lokaler Pfad = %3, Reply ID = %4</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="192"/>
        <source>download aborted</source>
        <translation>Download abgebrochen</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="209"/>
        <source>downloading</source>
        <translation>lade herunter</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="235"/>
        <source>download finished: URL = %1, local path = %2, reply ID = %3</source>
        <translation>Download beendet: URL = %1, lokaler Pfad = %2, Reply ID = %3</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="240"/>
        <source>download finished</source>
        <translation>Download beendet</translation>
    </message>
</context>
<context>
    <name>ItemSelector</name>
    <message>
        <location filename="../../itemselect.ui" line="15"/>
        <source>Item selection</source>
        <translation>Element-Auswahl</translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="21"/>
        <source>Select item(s)</source>
        <translation>Element(e) auswählen</translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="54"/>
        <source>Confirm selection</source>
        <translation>Auswahl bestätigen</translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="57"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="67"/>
        <location filename="../../itemselect.ui" line="70"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Joystick</name>
    <message>
        <location filename="../../joystick.cpp" line="67"/>
        <source>ERROR: couldn&apos;t open SDL joystick #%1</source>
        <translation>FEHLER: konnte SDL Joystick #%1 nicht öffnen</translation>
    </message>
    <message>
        <location filename="../../joystick.cpp" line="23"/>
        <source>ERROR: couldn&apos;t initialize SDL joystick support</source>
        <translation>FEHLER: konnte SDL Joystick Unterstützung nicht initialisieren</translation>
    </message>
</context>
<context>
    <name>JoystickCalibrationWidget</name>
    <message>
        <location filename="../../options.cpp" line="3794"/>
        <source>Axis %1:</source>
        <translation>Achse %1:</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3803"/>
        <source>Current value of axis %1</source>
        <translation>Aktueller Wert der Achse %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3807"/>
        <source>DZ:</source>
        <translation>TZ:</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3809"/>
        <source>Deadzone of axis %1</source>
        <translation>Todeszone der Achse %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3815"/>
        <source>S:</source>
        <translation>S:</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3817"/>
        <source>Sensitivity of axis %1</source>
        <translation>Empfindlichkeit der Achse %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3795"/>
        <source>Reset calibration of axis %1</source>
        <translation>Kalibrierung der Achse %1 zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3790"/>
        <source>Enable/disable axis %1</source>
        <translation>Achse %1 ein-/ausschalten</translation>
    </message>
</context>
<context>
    <name>JoystickFunctionScanner</name>
    <message>
        <location filename="../../joyfuncscan.ui" line="15"/>
        <location filename="../../joyfuncscan.ui" line="72"/>
        <location filename="../../joyfuncscan.cpp" line="24"/>
        <location filename="../../joyfuncscan.cpp" line="25"/>
        <source>Scanning joystick function</source>
        <translation>Scanne Joystick Funktion</translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="27"/>
        <source>&lt;&lt;&lt;&gt;&gt;&gt;</source>
        <translation>&lt;&lt;&lt;&gt;&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="43"/>
        <source>Accept joystick function</source>
        <translation>Joystick Funktion übernehmen</translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="46"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="56"/>
        <source>Cancel remapping of joystick function</source>
        <translation>Neuzuweisung der Jostick Funktion abbrechen</translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="59"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>JoystickTestWidget</name>
    <message>
        <location filename="../../options.cpp" line="4038"/>
        <source>A%1: %v</source>
        <translation>A%1: %v</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4039"/>
        <source>Current value of axis %1</source>
        <translation>Aktueller Wert der Achse %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4049"/>
        <source>B%1</source>
        <translation>K%1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4050"/>
        <source>Current state of button %1</source>
        <translation>Aktueller Status von Knopf %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4060"/>
        <source>H%1: 0</source>
        <translation>H%1: 0</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4061"/>
        <source>Current value of hat %1</source>
        <translation>Aktueller Wert von Coolie-Hat %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4071"/>
        <source>T%1 DX: 0</source>
        <translation>T%1 DX: 0</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4072"/>
        <source>Current X-delta of trackball %1</source>
        <translation>Aktuelles X-Delta von Trackball %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4082"/>
        <source>T%1 DY: 0</source>
        <translation>T%1 DY: 0</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4083"/>
        <source>Current Y-delta of trackball %1</source>
        <translation>Aktuelles Y-Delta von Trackball %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4171"/>
        <source>H%1: %2</source>
        <translation>H%1: %2</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4205"/>
        <source>T%1 DX: %2</source>
        <translation>T%1 DX: %2</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4206"/>
        <source>T%1 DY: %2</source>
        <translation>T%1 DY: %2</translation>
    </message>
</context>
<context>
    <name>KeySequenceScanner</name>
    <message>
        <location filename="../../keyseqscan.ui" line="43"/>
        <source>Accept key sequence</source>
        <translation>Tastaturkürzel akzeptieren</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="56"/>
        <source>Cancel redefinition of key sequence</source>
        <translation>Festlegung des Tastaturkürzels abbrechen</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="27"/>
        <source>&lt;&lt;&lt;&gt;&gt;&gt;</source>
        <translation>&lt;&lt;&lt;&gt;&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="46"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="59"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.cpp" line="21"/>
        <location filename="../../keyseqscan.cpp" line="22"/>
        <source>Scanning special key</source>
        <translation>Scanne Spezial-Taste</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="15"/>
        <location filename="../../keyseqscan.ui" line="72"/>
        <location filename="../../keyseqscan.cpp" line="24"/>
        <location filename="../../keyseqscan.cpp" line="25"/>
        <source>Scanning shortcut</source>
        <translation>Scanne Tastaturkürzel</translation>
    </message>
</context>
<context>
    <name>MESSDeviceConfigurator</name>
    <message>
        <location filename="../../messdevcfg.ui" line="15"/>
        <source>MESS device configuration</source>
        <translation>MESS Geräte-Konfiguration</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="37"/>
        <location filename="../../messdevcfg.cpp" line="1124"/>
        <location filename="../../messdevcfg.cpp" line="1129"/>
        <source>Active device configuration</source>
        <translation>Aktive Geräte-Konfiguration</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="73"/>
        <location filename="../../messdevcfg.ui" line="76"/>
        <source>Device configuration menu</source>
        <translation>Geräte-Konfigurationsmenü</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="99"/>
        <location filename="../../messdevcfg.ui" line="102"/>
        <source>Name of device configuration</source>
        <translation>Name der Geräte-Konfiguration</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="184"/>
        <location filename="../../messdevcfg.ui" line="187"/>
        <source>Save current device configuration to list of available configurations</source>
        <translation>Aktuelle Geräte-Konfiguration in Liste der verfügbaren Konfigurationen sichern</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="208"/>
        <source>Device mappings</source>
        <translation>Geräte Abbildungen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="229"/>
        <location filename="../../messdevcfg.ui" line="232"/>
        <source>Device setup of current configuration</source>
        <translation>Geräte-Setup der aktuellen Konfiguration</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="250"/>
        <source>Brief name</source>
        <translation>Kurzname</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="279"/>
        <source>Slot options</source>
        <translation>Slot Optionen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="300"/>
        <location filename="../../messdevcfg.ui" line="303"/>
        <source>Available slot options</source>
        <translation>Verfügbare Slot Optionen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="316"/>
        <source>Slot</source>
        <translation>Slot</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="321"/>
        <source>Option</source>
        <translation>Option</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="330"/>
        <source>File chooser</source>
        <translation>Schnellauswahl</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="369"/>
        <location filename="../../messdevcfg.ui" line="372"/>
        <source>Save selected instance / file as a new device configuration</source>
        <translation>Ausgewählte Instanz / Datei als neue Geräte-Konfiguration speichern</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="392"/>
        <location filename="../../messdevcfg.ui" line="395"/>
        <source>Select the device instance the file is mapped to</source>
        <translation>Geräte-Instanz auswählen, an welche die Datei gebunden werden soll</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="411"/>
        <location filename="../../messdevcfg.ui" line="414"/>
        <source>Automatically select the first matching device instance when selecting a file with a valid extension</source>
        <translation>Automatisch die erste gültige Geräte-Instanz auswählen, wenn eine Datei selektiert wird</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="457"/>
        <location filename="../../messdevcfg.ui" line="460"/>
        <source>Process ZIP contents on item activation</source>
        <translation>ZIP-Inhalte bei Item-Aktivierung verarbeiten</translation>
    </message>
    <message>
        <source>Process ZIP contents</source>
        <translation type="obsolete">ZIP Inhalt verarbeiten</translation>
    </message>
    <message>
        <source>Auto-select</source>
        <translation type="obsolete">Automatisch</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="434"/>
        <location filename="../../messdevcfg.ui" line="437"/>
        <source>Filter files by allowed extensions for the current device instance</source>
        <translation>Dateinamen nach den für die Geräte-Instanz gültigen Dateiendungen filtern</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation type="obsolete">Filter aktivieren</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="480"/>
        <location filename="../../messdevcfg.ui" line="483"/>
        <source>Enter search string (case-insensitive)</source>
        <translation>Such-Zeichenkette eingeben (Groß-/Kleinschreibung wird nicht beachtet)</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="499"/>
        <location filename="../../messdevcfg.ui" line="502"/>
        <source>Clear search string</source>
        <translation>Such-Zeichenkette löschen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="519"/>
        <location filename="../../messdevcfg.ui" line="522"/>
        <source>Number of files scanned</source>
        <translation>Anzahl ermittelter Dateien</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="544"/>
        <location filename="../../messdevcfg.ui" line="547"/>
        <source>Reload directory contents</source>
        <translation>Verzeichnis neu lesen</translation>
    </message>
    <message>
        <source>Number of files</source>
        <translation type="obsolete">Anzahl der Dateien</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="564"/>
        <location filename="../../messdevcfg.ui" line="567"/>
        <source>Play the selected configuration</source>
        <translation>Ausgewählte Konfiguration starten</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="584"/>
        <location filename="../../messdevcfg.ui" line="587"/>
        <source>Play the selected configuration (embedded)</source>
        <translation>Ausgewählte Konfiguration starten (eingebettet)</translation>
    </message>
    <message>
        <source>Choose directory</source>
        <translation type="obsolete">Verzeichnis auswählen</translation>
    </message>
    <message>
        <source>Choose a file to map</source>
        <translation type="obsolete">Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="653"/>
        <source>Available device configurations</source>
        <translation>Verfügbare Geräte-Konfigurationen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="665"/>
        <location filename="../../messdevcfg.ui" line="668"/>
        <source>List of available device configurations</source>
        <translation>Liste der verfügbaren Geräte-Konfigurationen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="677"/>
        <location filename="../../messdevcfg.cpp" line="718"/>
        <location filename="../../messdevcfg.cpp" line="879"/>
        <location filename="../../messdevcfg.cpp" line="997"/>
        <source>No devices</source>
        <translation>Keine Geräte</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="245"/>
        <source>Device instance</source>
        <translation>Geräte-Instanz</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="255"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="260"/>
        <source>Tag</source>
        <translation>Markierung</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="265"/>
        <source>Extensions</source>
        <translation>Erweiterungen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="270"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="115"/>
        <location filename="../../messdevcfg.ui" line="118"/>
        <source>Create a new device configuration</source>
        <translation>Neue Geräte-Konfiguration erzeugen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="161"/>
        <location filename="../../messdevcfg.ui" line="164"/>
        <source>Remove current device configuration from list of available configurations</source>
        <translation>Aktuelle Geräte-Konfiguration aus Liste der verfügbaren Konfigurationen löschen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="138"/>
        <location filename="../../messdevcfg.ui" line="141"/>
        <source>Clone current device configuration</source>
        <translation>Aktuelle Geräte-Konfiguration klonen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="754"/>
        <location filename="../../messdevcfg.cpp" line="756"/>
        <source>%1. copy of </source>
        <translation>%1. Kopie von </translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="962"/>
        <location filename="../../messdevcfg.cpp" line="1531"/>
        <source>%1. variant of </source>
        <translation>%1. Variante von </translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="234"/>
        <location filename="../../messdevcfg.cpp" line="276"/>
        <source>Play selected game</source>
        <translation>Ausgewähltes Spiel spielen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="146"/>
        <location filename="../../messdevcfg.cpp" line="385"/>
        <source>Reading slot info, please wait...</source>
        <translation>Lese Slot-Infos, bitte warten...</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="149"/>
        <source>Enter configuration name</source>
        <translation>Konfigurationsname eingeben</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="166"/>
        <source>Enter search string</source>
        <translation>Such-Zeichenkette eingeben</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="235"/>
        <location filename="../../messdevcfg.cpp" line="277"/>
        <source>&amp;Play</source>
        <translation>S&amp;pielen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="225"/>
        <source>Select default device directory</source>
        <translation>Standard Geräte-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="226"/>
        <source>&amp;Default device directory for &apos;%1&apos;...</source>
        <translation>Stan&amp;dard Geräte-Verzeichnis für &apos;%1&apos;...</translation>
    </message>
    <message>
        <source>Generate device configurations</source>
        <translation type="obsolete">Geräte-Konfigurationen erzeugen</translation>
    </message>
    <message>
        <source>&amp;Generate configurations for &apos;%1&apos;...</source>
        <translation type="obsolete">&amp;Konfigurationen für &apos;%1&apos; erzeugen...</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="240"/>
        <location filename="../../messdevcfg.cpp" line="282"/>
        <source>Play selected game (embedded)</source>
        <translation>Ausgewähltes Spiel spielen (eingebettet)</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="241"/>
        <location filename="../../messdevcfg.cpp" line="283"/>
        <source>Play &amp;embedded</source>
        <translation>Spielen (&amp;eingebettet)</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="247"/>
        <source>Remove configuration</source>
        <translation>Konfiguration entfernen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="248"/>
        <source>&amp;Remove configuration</source>
        <translation>&amp;Konfiguration entfernen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="256"/>
        <source>Select a file to be mapped to this device instance</source>
        <translation>Datei auswählen, die der Geräte-Instanz zugeordnet werden soll</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="257"/>
        <source>Select file...</source>
        <translation>Datei auswählen...</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="268"/>
        <source>Use as default directory</source>
        <translation>Als Standard Geräte-Verzeichnis verwenden</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="290"/>
        <location filename="../../messdevcfg.cpp" line="1368"/>
        <source>&amp;Open archive</source>
        <translation>&amp;Archiv öffnen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="332"/>
        <location filename="../../messdevcfg.cpp" line="614"/>
        <location filename="../../messdevcfg.cpp" line="1516"/>
        <source>No devices available</source>
        <translation>Keine Geräte verfügbar</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="387"/>
        <source>loading available system slots</source>
        <translation>Lade verfügbare System-Slots</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="408"/>
        <source>FATAL: can&apos;t start MESS executable within a reasonable time frame, giving up</source>
        <translation>FATAL: kann ausführbare MESS Datei nicht in einem angemessenen Zeitraum starten, gebe auf</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="409"/>
        <source>Failed to read slot info</source>
        <translation>Fehler beim Lesen der Slot-Infos</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="549"/>
        <source>done (loading available system slots, elapsed time = %1)</source>
        <translation>Fertig (Lade verfügbare System-Slots, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="639"/>
        <source>not used</source>
        <translation>nicht verwendet</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1067"/>
        <source>Choose default device directory for &apos;%1&apos;</source>
        <translation>Standard Geräte-Verzeichnis für &apos;%1&apos; auswählen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1368"/>
        <source>&amp;Close archive</source>
        <translation>&amp;Archiv schließen</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1533"/>
        <source>Choose a unique configuration name</source>
        <translation>Eindeutiger Name für Geräte-Konfiguration</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1533"/>
        <source>Unique configuration name:</source>
        <translation>Eindeutiger Name der Geräte-Konfiguration:</translation>
    </message>
    <message>
        <source>Configuration name:</source>
        <translation type="obsolete">Name der Konfiguration:</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1536"/>
        <source>Name conflict</source>
        <translation>Namenskonflikt</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1536"/>
        <source>A configuration named &apos;%1&apos; already exists.

Do you want to choose a different name?</source>
        <translation>Eine Konfiguration mit dem Namen &apos;%1&apos; existiert bereits.

Einen anderen Namen wählen?</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="obsolete">Konfiguration</translation>
    </message>
</context>
<context>
    <name>MESSDeviceFileDelegate</name>
    <message>
        <location filename="../../messdevcfg.cpp" line="53"/>
        <location filename="../../messdevcfg.cpp" line="73"/>
        <source>All files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="56"/>
        <location filename="../../messdevcfg.cpp" line="58"/>
        <source>Valid device files</source>
        <translation>Gültige Geräte-Dateien</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../qmc2main.ui" line="15"/>
        <location filename="../../macros.h" line="424"/>
        <location filename="../../macros.h" line="430"/>
        <source>M.A.M.E. Catalog / Launcher II</source>
        <translation>M.A.M.E. Catalog / Launcher II</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="147"/>
        <source>List of all supported games</source>
        <translation>Liste aller unterstützten Spiele</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="154"/>
        <location filename="../../qmc2main.cpp" line="1073"/>
        <source>Game / Attribute</source>
        <translation>Spiel / Attribut</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="526"/>
        <source>Creating category view, please wait...</source>
        <translation>Erzeuge Kategorie-Ansicht, bitte warten...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="661"/>
        <source>Creating version view, please wait...</source>
        <translation>Erzeuge Versions-Ansicht, bitte warten...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="680"/>
        <source>&amp;Search</source>
        <translation>Sucher&amp;gebnis</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="695"/>
        <location filename="../../qmc2main.ui" line="698"/>
        <source>Search result</source>
        <translation>Suchergebnis</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="744"/>
        <location filename="../../qmc2main.ui" line="747"/>
        <source>Favorite games</source>
        <translation>Favorisierte Spiele</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="774"/>
        <location filename="../../qmc2main.ui" line="777"/>
        <source>Games last played</source>
        <translation>Zuletzt gespielte Spiele</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1611"/>
        <location filename="../../qmc2main.ui" line="1614"/>
        <source>Frontend log</source>
        <translation>Frontend Log</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2207"/>
        <source>Remove finished</source>
        <translation>Beendete entfernen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2252"/>
        <source>&amp;Tools</source>
        <translation>Werk&amp;zeuge</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2243"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2466"/>
        <source>Check &amp;samples...</source>
        <translation>&amp;Samples prüfen...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2469"/>
        <location filename="../../qmc2main.ui" line="2472"/>
        <source>Check sample set</source>
        <translation>Sample-Sammlung überprüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2529"/>
        <source>&amp;Documentation...</source>
        <translation>&amp;Dokumentation...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2532"/>
        <location filename="../../qmc2main.ui" line="2535"/>
        <source>View online documentation</source>
        <translation>Online-Dokumentation ansehen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2553"/>
        <location filename="../../qmc2main.ui" line="2556"/>
        <source>About M.A.M.E. Catalog / Launcher II</source>
        <translation>Über M.A.M.E. Catalog / Launcher II</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2574"/>
        <location filename="../../qmc2main.cpp" line="2646"/>
        <source>About Qt</source>
        <translation>Über Qt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2592"/>
        <location filename="../../qmc2main.ui" line="2595"/>
        <source>Check ROM collection</source>
        <translation>ROM-Sammlung überprüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2610"/>
        <source>&amp;Options...</source>
        <translation>&amp;Optionen...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2613"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2634"/>
        <source>&amp;Reload</source>
        <translation>&amp;Neu laden</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3499"/>
        <source>Play (tagged)</source>
        <translation>Spielen (markierte)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3502"/>
        <location filename="../../qmc2main.ui" line="3505"/>
        <source>Play all tagged games</source>
        <translation>Alle markierten Spiele spielen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3517"/>
        <source>Play embedded (tagged)</source>
        <translation>Spielen (eingebettet, markierte)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3520"/>
        <location filename="../../qmc2main.ui" line="3523"/>
        <source>Play all tagged games (embedded)</source>
        <translation>All markierten Spiele eingebettet spielen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3535"/>
        <source>To favorites (tagged)</source>
        <translation>Zu Favoriten (markierte)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3538"/>
        <location filename="../../qmc2main.ui" line="3541"/>
        <source>Add all tagged games to favorites</source>
        <translation>Alle markierten Spiele zu den Favoriten hinzufügen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3544"/>
        <source>Ctrl+Shift+F</source>
        <translation>Ctrl+Shift+F</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3556"/>
        <source>ROM state (tagged)</source>
        <translation>ROM-Status (markierte)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3559"/>
        <location filename="../../qmc2main.ui" line="3562"/>
        <source>Check ROM states of all tagged sets</source>
        <translation>ROM-Status aller markierten Sets prüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3565"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3586"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2538"/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2622"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2643"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5056"/>
        <source>cleaning up</source>
        <translation>Räume auf</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="209"/>
        <location filename="../../qmc2main.ui" line="341"/>
        <location filename="../../qmc2main.ui" line="476"/>
        <location filename="../../qmc2main.ui" line="611"/>
        <location filename="../../qmc2main.cpp" line="1675"/>
        <location filename="../../qmc2main.cpp" line="1969"/>
        <location filename="../../qmc2main.cpp" line="1993"/>
        <location filename="../../qmc2main.cpp" line="3124"/>
        <location filename="../../qmc2main.cpp" line="3321"/>
        <location filename="../../qmc2main.cpp" line="3922"/>
        <location filename="../../qmc2main.cpp" line="4030"/>
        <location filename="../../qmc2main.cpp" line="4646"/>
        <location filename="../../qmc2main.cpp" line="4662"/>
        <location filename="../../qmc2main.cpp" line="5618"/>
        <location filename="../../qmc2main.cpp" line="5639"/>
        <location filename="../../qmc2main.cpp" line="7506"/>
        <location filename="../../qmc2main.cpp" line="7523"/>
        <location filename="../../qmc2main.cpp" line="7600"/>
        <location filename="../../qmc2main.cpp" line="7617"/>
        <source>Waiting for data...</source>
        <translation>Warte auf Daten...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="714"/>
        <source>Search for games</source>
        <translation>Nach Spielen suchen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2508"/>
        <location filename="../../qmc2main.cpp" line="745"/>
        <location filename="../../qmc2main.cpp" line="797"/>
        <location filename="../../qmc2main.cpp" line="849"/>
        <location filename="../../qmc2main.cpp" line="908"/>
        <source>&amp;Play</source>
        <translation>S&amp;pielen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2511"/>
        <location filename="../../qmc2main.ui" line="2514"/>
        <source>Play current game</source>
        <translation>Aktuelles Spiel starten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2517"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3577"/>
        <source>E&amp;xit / Stop</source>
        <translation>&amp;Beenden / Anhalten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2559"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2577"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3580"/>
        <location filename="../../qmc2main.ui" line="3583"/>
        <source>Exit program / Stop any active processing</source>
        <translation>Programm beenden / Aktive Verarbeitung stoppen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5422"/>
        <source>so long and thanks for all the fish</source>
        <translation>Macht&apos;s gut, und danke für den Fisch</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="644"/>
        <source>restoring main widget layout</source>
        <translation>Restauriere Hauptfenster-Layout</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5126"/>
        <source>saving main widget layout</source>
        <translation>Speichere Hauptfenster-Layout</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1811"/>
        <location filename="../../qmc2main.cpp" line="4524"/>
        <location filename="../../qmc2main.cpp" line="4569"/>
        <location filename="../../qmc2main.cpp" line="4993"/>
        <location filename="../../qmc2main.cpp" line="5008"/>
        <location filename="../../qmc2main.cpp" line="5039"/>
        <source>Confirm</source>
        <translation>Bestätigen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4994"/>
        <source>Your configuration changes have not been applied yet.
Really quit?</source>
        <translation>Ihre Konfigurationsänderungen wurden noch nicht übernommen.
Wirklich beenden?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2651"/>
        <location filename="../../qmc2main.ui" line="2654"/>
        <location filename="../../qmc2main.ui" line="2657"/>
        <source>Clear image cache</source>
        <translation>Bild-Zugriffsspeicher leeren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2660"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2025"/>
        <source>image cache cleared</source>
        <translation>Bild-Zugriffsspeicher geleert</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1779"/>
        <location filename="../../qmc2main.cpp" line="1801"/>
        <location filename="../../qmc2main.cpp" line="2103"/>
        <location filename="../../qmc2main.cpp" line="2142"/>
        <location filename="../../qmc2main.cpp" line="2179"/>
        <location filename="../../qmc2main.cpp" line="2211"/>
        <location filename="../../qmc2main.cpp" line="3913"/>
        <source>please wait for reload to finish and try again</source>
        <translation>Bitte warte bis die Spieleliste aktualisiert wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="723"/>
        <source>&amp;Terminate</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="728"/>
        <source>&amp;Kill</source>
        <translation>&amp;Töten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="722"/>
        <source>Terminate selected emulator(s) (sends TERM signal to emulator process(es))</source>
        <translation>Ausgewählte(n) Emulator(en) terminieren (sendet TERM Signal an betreffende(n) Emulator(en))</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="727"/>
        <source>Kill selected emulator(s) (sends KILL signal to emulator process(es))</source>
        <translation>Ausgewählte(n) Emulator(en) töten (sendet KILL Signal an betreffende(n) Emulator(en))</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="741"/>
        <location filename="../../qmc2main.cpp" line="793"/>
        <location filename="../../qmc2main.cpp" line="845"/>
        <location filename="../../qmc2main.cpp" line="904"/>
        <source>Play selected game</source>
        <translation>Ausgewähltes Spiel spielen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2677"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1741"/>
        <location filename="../../qmc2main.cpp" line="5111"/>
        <source>saving game selection</source>
        <translation>Speichere Spiel-Selektion</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2475"/>
        <source>Ctrl+2</source>
        <translation>Ctrl+2</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2496"/>
        <source>Ctrl+3</source>
        <translation>Ctrl+3</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2598"/>
        <source>Ctrl+1</source>
        <translation>Ctrl+1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2761"/>
        <source>Ctrl+4</source>
        <translation>Ctrl+4</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1777"/>
        <location filename="../../qmc2main.cpp" line="1799"/>
        <source>ROM verification already active</source>
        <translation>ROM-Überprüfung wird bereits durchgeführt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1698"/>
        <location filename="../../qmc2main.cpp" line="2107"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation>Bitte warte bis die ROM Verifikation abgeschlossen wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1812"/>
        <source>The ROM verification process may be very time-consuming.
It will overwrite existing cached data.

Do you really want to check all ROM states now?</source>
        <translation>Die ROM-Verifikation kann sehr viel Zeit in Anspruch nehmen.
Existierende Cache-Daten werden dabei überschrieben.

Willst Du wirklich jetzt den ROM-Status aller Spiele überprüfen?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2689"/>
        <location filename="../../qmc2main.cpp" line="767"/>
        <location filename="../../qmc2main.cpp" line="819"/>
        <location filename="../../qmc2main.cpp" line="930"/>
        <location filename="../../qmc2main.cpp" line="4180"/>
        <source>To &amp;favorites</source>
        <translation>Zu &amp;Favoriten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2692"/>
        <location filename="../../qmc2main.ui" line="2695"/>
        <location filename="../../qmc2main.cpp" line="763"/>
        <location filename="../../qmc2main.cpp" line="815"/>
        <location filename="../../qmc2main.cpp" line="926"/>
        <source>Add current game to favorites</source>
        <translation>Aktuelles Spiel zu Favoriten hinzufügen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="886"/>
        <source>Remove from favorites</source>
        <translation>Von Favoriten entfernen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="887"/>
        <location filename="../../qmc2main.cpp" line="955"/>
        <source>&amp;Remove</source>
        <translation>Entfe&amp;rnen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="891"/>
        <source>Clear all favorites</source>
        <translation>Alle Favoriten entfernen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="892"/>
        <location filename="../../qmc2main.cpp" line="960"/>
        <source>&amp;Clear</source>
        <translation>L&amp;eeren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="954"/>
        <source>Remove from played</source>
        <translation>Von Gespielten entfernen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="959"/>
        <source>Clear all played</source>
        <translation>Alle Gespielten entfernen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2698"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="896"/>
        <source>Save favorites now</source>
        <translation>Favoriten jetzt speichern</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="897"/>
        <location filename="../../qmc2main.cpp" line="965"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="964"/>
        <source>Save play-history now</source>
        <translation>Spiel-Historie jetzt speichern</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5009"/>
        <source>There are one or more emulators still running.
Should they be killed on exit?</source>
        <translation>Einer oder mehrere Emulatoren laufen noch.
Sollen diese beim Beenden geschlossen werden?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1408"/>
        <source>Fl&amp;yer</source>
        <translation>Fl&amp;yer</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="406"/>
        <location filename="../../qmc2main.cpp" line="3530"/>
        <location filename="../../qmc2main.cpp" line="3566"/>
        <location filename="../../qmc2main.cpp" line="3755"/>
        <location filename="../../qmc2main.cpp" line="3844"/>
        <location filename="../../qmc2main.cpp" line="5237"/>
        <location filename="../../qmc2main.cpp" line="5476"/>
        <location filename="../../qmc2main.cpp" line="5515"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1693"/>
        <location filename="../../qmc2main.cpp" line="1775"/>
        <location filename="../../qmc2main.cpp" line="1797"/>
        <source>please wait for ROM state filter to finish and try again</source>
        <translation>Bitte warte bis nach ROM Status gefiltert wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="286"/>
        <location filename="../../qmc2main.cpp" line="1113"/>
        <source>Game / Clones</source>
        <translation>Spiel / Klone</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="711"/>
        <source>Search for games (not case-sensitive)</source>
        <translation>Nach Spielen suchen (Groß-/Kleinschreibung wird nicht beachtet)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2336"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2719"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2740"/>
        <source>F6</source>
        <translation>F6</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2490"/>
        <location filename="../../qmc2main.ui" line="2493"/>
        <source>Check preview images</source>
        <translation>Vorschaubilder überprüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2755"/>
        <location filename="../../qmc2main.ui" line="2758"/>
        <source>Check flyer images</source>
        <translation>Flyer-Bilder überprüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2487"/>
        <source>Check &amp;previews...</source>
        <translation>&amp;Vorschaubilder prüfen...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2589"/>
        <source>Check &amp;ROMs...</source>
        <translation>&amp;ROMs prüfen...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2752"/>
        <source>Check &amp;flyers...</source>
        <translation>&amp;Flyer prüfen...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1703"/>
        <location filename="../../qmc2main.cpp" line="1781"/>
        <location filename="../../qmc2main.cpp" line="1803"/>
        <source>please wait for image check to finish and try again</source>
        <translation>Bitte warte bis die Bild-Überprüfung abgeschlossen wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2036"/>
        <source>icon cache cleared</source>
        <translation>Icon-Zugriffsspeicher geleert</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="164"/>
        <location filename="../../qmc2main.cpp" line="1077"/>
        <location filename="../../qmc2main.cpp" line="1084"/>
        <source>Icon / Value</source>
        <translation>Icon / Wert</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="296"/>
        <location filename="../../qmc2main.ui" line="431"/>
        <location filename="../../qmc2main.ui" line="566"/>
        <location filename="../../qmc2main.cpp" line="1117"/>
        <location filename="../../qmc2main.cpp" line="1124"/>
        <location filename="../../qmc2main.cpp" line="1157"/>
        <location filename="../../qmc2main.cpp" line="1182"/>
        <location filename="../../qmc2main.cpp" line="7809"/>
        <source>Icon</source>
        <translation>Icon</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2769"/>
        <location filename="../../qmc2main.ui" line="2772"/>
        <source>Clear icon cache</source>
        <translation>Icon-Zugriffsspeicher leeren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2787"/>
        <source>Check &amp;icons...</source>
        <translation>&amp;Icons prüfen...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2790"/>
        <location filename="../../qmc2main.ui" line="2793"/>
        <source>Check icon images</source>
        <translation>Icons überprüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="169"/>
        <location filename="../../qmc2main.ui" line="301"/>
        <location filename="../../qmc2main.ui" line="436"/>
        <location filename="../../qmc2main.ui" line="571"/>
        <location filename="../../qmc2main.cpp" line="1087"/>
        <location filename="../../qmc2main.cpp" line="1127"/>
        <location filename="../../qmc2main.cpp" line="1159"/>
        <location filename="../../qmc2main.cpp" line="1184"/>
        <source>Year</source>
        <translation>Jahr</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="174"/>
        <location filename="../../qmc2main.ui" line="306"/>
        <location filename="../../qmc2main.ui" line="441"/>
        <location filename="../../qmc2main.ui" line="576"/>
        <location filename="../../qmc2main.cpp" line="1089"/>
        <location filename="../../qmc2main.cpp" line="1129"/>
        <location filename="../../qmc2main.cpp" line="1161"/>
        <location filename="../../qmc2main.cpp" line="1186"/>
        <source>Manufacturer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2775"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2796"/>
        <source>Ctrl+5</source>
        <translation>Ctrl+5</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1708"/>
        <location filename="../../qmc2main.cpp" line="1783"/>
        <location filename="../../qmc2main.cpp" line="1805"/>
        <source>please wait for sample check to finish and try again</source>
        <translation>Bitte warte bis die Überprüfung der Samples abgeschlossen wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4987"/>
        <source>stopping current processing upon user request</source>
        <translation>Beende aktuelle Verarbeitung auf Wunsch des Benutzers</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="729"/>
        <source>Favo&amp;rites</source>
        <translation>Favo&amp;riten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2417"/>
        <source>Toolbar</source>
        <translation>Toolbar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2814"/>
        <location filename="../../qmc2main.ui" line="2817"/>
        <location filename="../../qmc2main.cpp" line="773"/>
        <location filename="../../qmc2main.cpp" line="825"/>
        <location filename="../../qmc2main.cpp" line="868"/>
        <location filename="../../qmc2main.cpp" line="936"/>
        <source>Check current game&apos;s ROM state</source>
        <translation>ROM-Status des aktuellen Spiels überprüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="777"/>
        <location filename="../../qmc2main.cpp" line="829"/>
        <location filename="../../qmc2main.cpp" line="872"/>
        <location filename="../../qmc2main.cpp" line="940"/>
        <source>Check &amp;ROM state</source>
        <translation>&amp;ROM-Status prüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2319"/>
        <source>&amp;Check</source>
        <translation>&amp;Prüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2808"/>
        <location filename="../../qmc2main.ui" line="2811"/>
        <source>ROM state</source>
        <translation>ROM-Status</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2820"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5278"/>
        <source>destroying preview</source>
        <translation>Zerstöre Vorschau</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5282"/>
        <source>destroying flyer</source>
        <translation>Zerstöre Flyer</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5306"/>
        <source>destroying about dialog</source>
        <translation>Zerstöre About-Dialog</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5325"/>
        <source>destroying image checker</source>
        <translation>Zerstöre Bild-Prüfer</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5330"/>
        <source>destroying sample checker</source>
        <translation>Zerstöre Sample-Prüfer</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5392"/>
        <source>destroying process manager</source>
        <translation>Zerstöre Prozess-Manager</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="786"/>
        <location filename="../../qmc2main.cpp" line="838"/>
        <location filename="../../qmc2main.cpp" line="881"/>
        <location filename="../../qmc2main.cpp" line="949"/>
        <source>&amp;Analyse ROM...</source>
        <translation>ROM &amp;analysieren...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2835"/>
        <source>Analyse ROM</source>
        <translation>ROM analysieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2844"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2880"/>
        <source>ROMAly&amp;zer...</source>
        <translation>ROMAly&amp;zer...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2883"/>
        <location filename="../../qmc2main.ui" line="2886"/>
        <source>Open ROMAlyzer dialog</source>
        <translation>ROMAlyzer Dialog öffnen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2889"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4763"/>
        <location filename="../../qmc2main.cpp" line="4822"/>
        <source>Choose export file</source>
        <translation>Export-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4763"/>
        <location filename="../../qmc2main.cpp" line="4790"/>
        <location filename="../../qmc2main.cpp" line="4822"/>
        <location filename="../../qmc2main.cpp" line="4858"/>
        <location filename="../../qmc2main.cpp" line="6383"/>
        <location filename="../../qmc2main.cpp" line="8113"/>
        <location filename="../../qmc2main.cpp" line="8155"/>
        <source>All files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3595"/>
        <source>Export to...</source>
        <translation>Exportieren nach...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3617"/>
        <location filename="../../qmc2main.cpp" line="3621"/>
        <source>&lt;inipath&gt;/%1.ini</source>
        <translation>&lt;inipath&gt;/%1.ini</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3618"/>
        <location filename="../../qmc2main.cpp" line="3622"/>
        <source>Select file...</source>
        <translation>Datei auswählen...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4790"/>
        <location filename="../../qmc2main.cpp" line="4858"/>
        <source>Choose import file</source>
        <translation>Import-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1823"/>
        <source>automatic ROM check triggered</source>
        <translation>Automatische ROM Überprüfung wird gestartet</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2850"/>
        <source>ERROR: no match found (?)</source>
        <translation>FEHLER: kein passender Eintrag gefunden (?)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5232"/>
        <source>destroying current game&apos;s emulator configuration</source>
        <translation>Zerstöre aktuelle Emulator-Konfiguration</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5244"/>
        <source>destroying global emulator options</source>
        <translation>Zerstöre globale Emulator-Konfiguration</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1489"/>
        <source>&amp;Configuration</source>
        <translation>&amp;Konfiguration</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1635"/>
        <source>Emulator &amp;log</source>
        <translation>Emulator &amp;Log</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1650"/>
        <location filename="../../qmc2main.ui" line="1653"/>
        <source>Emulator log</source>
        <translation>Emulator Log</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2616"/>
        <location filename="../../qmc2main.ui" line="2619"/>
        <source>Frontend setup and global emulator configuration</source>
        <translation>Frontend Einstellungen und globale Emulator-Konfiguration</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4820"/>
        <location filename="../../qmc2main.cpp" line="4855"/>
        <source>WARNING: invalid inipath</source>
        <translation>WARNUNG: ungültiger ini-Pfad</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5395"/>
        <source>killing %n running emulator(s) on exit</source>
        <translation>
            <numerusform>Töte %n Emulator beim Beenden</numerusform>
            <numerusform>Töte %n Emulatoren beim Beenden</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5398"/>
        <source>keeping %n running emulator(s) alive</source>
        <translation>
            <numerusform>Lasse %n laufenden Emulator am Leben</numerusform>
            <numerusform>Lasse %n laufende Emulatoren am Leben</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5335"/>
        <source>destroying ROMAlyzer</source>
        <translation>Zerstöre ROMAlyzer</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1713"/>
        <location filename="../../qmc2main.cpp" line="1785"/>
        <location filename="../../qmc2main.cpp" line="1807"/>
        <location filename="../../qmc2main.cpp" line="1997"/>
        <source>please wait for ROMAlyzer to finish the current analysis and try again</source>
        <translation>Bitte warte bis der ROMAlyzer die aktuelle Analyse abgeschlossen hat und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2967"/>
        <source>F11</source>
        <translation>F11</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="179"/>
        <location filename="../../qmc2main.ui" line="311"/>
        <location filename="../../qmc2main.ui" line="446"/>
        <location filename="../../qmc2main.ui" line="581"/>
        <location filename="../../qmc2main.cpp" line="1091"/>
        <location filename="../../qmc2main.cpp" line="1131"/>
        <location filename="../../qmc2main.cpp" line="1163"/>
        <location filename="../../qmc2main.cpp" line="1188"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4525"/>
        <source>Are you sure you want to clear the favorites list?</source>
        <translation>Bist Du sicher, dass Du alle Einträge der Spiel-Historie löschen möchtest?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4570"/>
        <source>Are you sure you want to clear the play history?</source>
        <translation>Bist Du sicher, dass Du alle Favoriten löschen möchtest?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3699"/>
        <location filename="../../qmc2main.cpp" line="3706"/>
        <location filename="../../qmc2main.cpp" line="3741"/>
        <location filename="../../qmc2main.cpp" line="3744"/>
        <source>No data available</source>
        <translation>Keine Daten verfügbar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1432"/>
        <location filename="../../qmc2main.ui" line="1435"/>
        <source>Detailed game information</source>
        <translation>Detaillierte Spiel Informationen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5359"/>
        <source>destroying game info DB</source>
        <translation>Zerstöre Spiel-Info DB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5809"/>
        <source>loading game info DB</source>
        <translation>Lade Spiel-Info DB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5918"/>
        <source>WARNING: missing &apos;$end&apos; in game info DB %1</source>
        <translation>WARNUNG: &apos;$end&apos; fehlt in Spiel-Info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5925"/>
        <source>WARNING: missing &apos;$bio&apos; in game info DB %1</source>
        <translation>WARNUNG: &apos;$bio&apos; fehlt in Spiel-Info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5932"/>
        <source>WARNING: missing &apos;$info&apos; in game info DB %1</source>
        <translation>WARNUNG: &apos;$info&apos; fehlt in Spiel-Info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5946"/>
        <source>WARNING: can&apos;t open game info DB %1</source>
        <translation>WARNUNG: kann Spiel-Info DB %1 nicht öffnen</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5955"/>
        <source>%n game info record(s) loaded</source>
        <translation>
            <numerusform>%n Spiel-Info Datensatz geladen</numerusform>
            <numerusform>%n Spiel-Info Datensätze geladen</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5954"/>
        <source>done (loading game info DB, elapsed time = %1)</source>
        <translation>Fertig (Lade Spiel-Info DB, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5377"/>
        <source>destroying emulator info DB</source>
        <translation>Zerstöre Emulator-Info DB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="530"/>
        <source>M&amp;achine list</source>
        <translation>M&amp;aschinenliste</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="467"/>
        <source>Toggle automatic pausing of embedded emulators (hold down for menu)</source>
        <translation>Automatisches Pausieren eingebetteter Emulatoren ein-/ausschalten (oder gedrückt halten um Menü anzuzeigen)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="476"/>
        <source>Scan the pause key used by the emulator</source>
        <translation>Taste zum Pausieren des Emulators scannen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="477"/>
        <source>Scan pause key...</source>
        <translation>Pause-Taste scannen...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="521"/>
        <source>Game</source>
        <translation>Spiel</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="540"/>
        <location filename="../../qmc2main.cpp" line="541"/>
        <source>Play all tagged machines</source>
        <translation>Alle markierten Maschinen spielen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="542"/>
        <location filename="../../qmc2main.cpp" line="543"/>
        <source>Clear machine list cache</source>
        <translation>Maschinen-Listen Cache leeren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="544"/>
        <location filename="../../qmc2main.cpp" line="545"/>
        <source>Forcedly clear (remove) the machine list cache</source>
        <translation>Maschinen-Listen Cache erzwungenermaßen leeren (löschen)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="546"/>
        <source>List of all supported machines</source>
        <translation>Liste aller unterstützten Maschinen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="550"/>
        <location filename="../../qmc2main.cpp" line="551"/>
        <source>Play all tagged machines (embedded)</source>
        <translation>Alle markierten Maschinen spielen (eingebettet)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="554"/>
        <source>Machine</source>
        <translation>Maschine</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="560"/>
        <location filename="../../qmc2main.cpp" line="561"/>
        <source>Add all tagged machines to favorites</source>
        <translation>Alle markierten Maschinen zu den Favoriten hinzufügen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="590"/>
        <location filename="../../qmc2main.cpp" line="591"/>
        <source>Loading machine list, please wait...</source>
        <translation>Lade Maschinenliste, bitte warten...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1216"/>
        <source>Enter search string</source>
        <translation>Such-Zeichenkette eingeben</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1392"/>
        <source>sorry, devices cannot run standalone</source>
        <translation>Sorry, Geräte können nicht allein stehend ausgeführt werden</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1544"/>
        <source>No devices available</source>
        <translation>Keine Geräte verfügbar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2092"/>
        <source>YouTube on-disk cache cleared (%1)</source>
        <translation>YouTube Festplatten-Cache geleert (%1)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2124"/>
        <source>ROM state cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation>ROM-Status Cache Datei &apos;%1&apos; auf Wunsch des Benutzers entfernt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2126"/>
        <source>WARNING: cannot remove the ROM state cache file &apos;%1&apos;, please check permissions</source>
        <translation>WARNUNG: kann ROM-Status Cache Datei &apos;%1&apos; nicht entfernen, bitte Zugriffsrechte prüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2131"/>
        <source>triggering an automatic ROM check on next reload</source>
        <translation>Automatische ROM Überprüfung wird nach dem nächsten Laden ausgeführt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2160"/>
        <source>game list cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation>Spielelisten Cache Datei &apos;%1&apos; auf Wunsch des Benutzers entfernt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2162"/>
        <source>WARNING: cannot remove the game list cache file &apos;%1&apos;, please check permissions</source>
        <translation>WARNUNG: kann Spielelisten Cache Datei &apos;%1&apos; nicht entfernen, bitte Zugriffsrechte prüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2165"/>
        <source>machine list cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation>Maschinen-Listen Cache Datei &apos;%1&apos; auf Wunsch des Benutzers entfernt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2167"/>
        <source>WARNING: cannot remove the machine list cache file &apos;%1&apos;, please check permissions</source>
        <translation>WARNUNG: kann Maschinen-Listen Cache Datei &apos;%1&apos; nicht entfernen, bitte Zugriffsrechte prüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2196"/>
        <source>XML cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation>XML Cache Datei &apos;%1&apos; auf Wunsch des Benutzers entfernt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2198"/>
        <source>WARNING: cannot remove the XML cache file &apos;%1&apos;, please check permissions</source>
        <translation>WARNUNG: kann XML Cache Datei &apos;%1&apos; nicht entfernen, bitte Zugriffsrechte prüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2228"/>
        <source>software list cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation>Software-Listen Cache Datei &apos;%1&apos; auf Wunsch des Benutzers entfernt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2230"/>
        <source>WARNING: cannot remove the software list cache file &apos;%1&apos;, please check permissions</source>
        <translation>WARNUNG: kann Software-Listen Cache Datei &apos;%1&apos; nicht entfernen, bitte Zugriffsrechte prüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4171"/>
        <location filename="../../qmc2main.cpp" line="4172"/>
        <source>Toggle embedder options (hold down for menu)</source>
        <translation>Embedder Optionen ein-/ausschalten (oder gedrückt halten um Menü anzuzeigen)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4179"/>
        <source>To favorites</source>
        <translation>Zu Favoriten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4185"/>
        <source>Terminate emulator</source>
        <translation>Emulator beenden</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4186"/>
        <source>&amp;Terminate emulator</source>
        <translation>Emulator &amp;beenden</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4190"/>
        <source>Kill emulator</source>
        <translation>Emulator töten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4191"/>
        <source>&amp;Kill emulator</source>
        <translation>Emulatoren &amp;töten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4219"/>
        <source>Couldn&apos;t find the window ID of one or more
emulator(s) within a reasonable timeframe.

Retry embedding?</source>
        <translation>Konnte die Fenster ID eines oder mehrerer
Emulatoren innerhalb einer angemessenen
Zeitspanne nicht finden.

Einbettung wiederholen?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4229"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4229"/>
        <source>Sorry, the emulator meanwhile died a sorrowful death :(.</source>
        <translation>Sorry, der Emulator ist in der Zwischenzeit von uns gegangen :(.</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5040"/>
        <source>There are one or more running downloads. Quit anyway?</source>
        <translation>Ein oder mehrere Downloads sind noch aktiv. Trotzdem beenden?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5059"/>
        <source>aborting running downloads</source>
        <translation>Breche aktive Downloads ab</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5071"/>
        <source>saving YouTube video info map</source>
        <translation>Speichere YouTube Video-Info Index</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5094"/>
        <source>done (saving YouTube video info map)</source>
        <translation>Fertig (Speichere YouTube Video-Info Index)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5096"/>
        <location filename="../../qmc2main.cpp" line="5098"/>
        <source>failed (saving YouTube video info map)</source>
        <translation>Fehlgeschlagen (Speichere YouTube Video-Info Index)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5213"/>
        <source>saving current game&apos;s favorite software</source>
        <translation>Speichere favorisierte Software des aktuellen Spiels</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5302"/>
        <source>destroying PCB</source>
        <translation>Zerstöre PCB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5310"/>
        <source>destroying MiniWebBrowser</source>
        <translation>Zerstöre MiniWebBrowser</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5315"/>
        <source>destroying MAWS lookup</source>
        <translation>Zerstöre MAWS Lookup</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5319"/>
        <source>destroying MAWS quick download setup</source>
        <translation>Zerstöre MAWS Schnell-Download Einstellung</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8652"/>
        <location filename="../../qmc2main.cpp" line="8686"/>
        <source>Play tagged - %p%</source>
        <translation>Markierte spielen - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8722"/>
        <source>Add favorites - %p%</source>
        <translation>Favoriten hinzufügen - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8755"/>
        <location filename="../../qmc2main.cpp" line="8783"/>
        <location filename="../../qmc2main.cpp" line="8823"/>
        <source>please wait for current activity to finish and try again (this batch-mode operation can only run exclusively)</source>
        <translation>Bitte warten bis die aktuelle Verarbeitung beendet ist (diese Batch-Mode Operation kann nur exklusiv ausgeführt werden)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8829"/>
        <source>ROM tool tagged - %p%</source>
        <translation>ROM Tool (markierte) - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8963"/>
        <source>Tag - %p%</source>
        <translation>Markierungen setzen - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9005"/>
        <source>Untag - %p%</source>
        <translation>Markierungen aufheben - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9047"/>
        <source>Invert tag - %p%</source>
        <translation>Markierungen umkehren - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5260"/>
        <source>destroying audio effects dialog</source>
        <translation>Zerstöre Audio Effekt Dialog</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5270"/>
        <source>destroying YouTube video widget</source>
        <translation>Zerstöre YouTube Video WIdget</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5418"/>
        <source>destroying network access manager</source>
        <translation>Zerstöre Netzwerk-Access-Manager</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5554"/>
        <source>loading style sheet &apos;%1&apos;</source>
        <translation>Lade Style Sheet &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5563"/>
        <source>FATAL: can&apos;t open style sheet file &apos;%1&apos;, please check</source>
        <translation>FATAL: kann Style Sheet Datei &apos;%1&apos; nicht öffnen, bitte prüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5566"/>
        <source>removing current style sheet</source>
        <translation>Entferne aktuelles Style Sheet</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5745"/>
        <source>loading YouTube video info map</source>
        <translation>Lade YouTube Video-Info Index</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5762"/>
        <source>YouTube index - %p%</source>
        <translation>YouTube Index - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5791"/>
        <source>done (loading YouTube video info map)</source>
        <translation>Fertig (Lade YouTube Video-Info Index)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5792"/>
        <source>%n video info record(s) loaded</source>
        <translation>
            <numerusform>%n Video-Info Datensatz geladen</numerusform>
            <numerusform>%n Video-Info Datensätze geladen</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5992"/>
        <source>loading emulator info DB</source>
        <translation>Lade Emulator-Info DB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6076"/>
        <source>WARNING: missing &apos;$end&apos; in emulator info DB %1</source>
        <translation>WARNUNG: &apos;$end&apos; fehlt in Emulator-Info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6079"/>
        <source>WARNING: missing &apos;$mame&apos; in emulator info DB %1</source>
        <translation>WARNUNG: &apos;$mame&apos; fehlt in Emulator-Info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6082"/>
        <source>WARNING: missing &apos;$info&apos; in emulator info DB %1</source>
        <translation>WARNUNG: &apos;$info&apos; fehlt in Emulator-Info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6092"/>
        <source>WARNING: can&apos;t open emulator info DB %1</source>
        <translation>WARNUNG: kann Emulator-Info DB %1 nicht öffnen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6095"/>
        <source>done (loading emulator info DB, elapsed time = %1)</source>
        <translation>Fertig (Lade Emulator-Info DB, benötigte Zeit = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="6096"/>
        <source>%n emulator info record(s) loaded</source>
        <translation>
            <numerusform>%n Emulator-Info Datensatz geladen</numerusform>
            <numerusform>%n Emulator-Info Datensätze geladen</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1468"/>
        <location filename="../../qmc2main.ui" line="1471"/>
        <source>Detailed emulator information</source>
        <translation>Detaillierte Emulator Informationen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1674"/>
        <source>E&amp;mulator control</source>
        <translation>E&amp;mulator Kontrolle</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5957"/>
        <source>invalidating game info DB</source>
        <translation>Invalidiere Spiel-Info DB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6098"/>
        <source>invalidating emulator info DB</source>
        <translation>Invalidiere Emulator-Info DB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2943"/>
        <source>F12</source>
        <translation>F12</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1399"/>
        <source>Pre&amp;view</source>
        <translation>&amp;Vorschau</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="159"/>
        <location filename="../../qmc2main.ui" line="291"/>
        <location filename="../../qmc2main.ui" line="426"/>
        <location filename="../../qmc2main.ui" line="561"/>
        <location filename="../../qmc2main.cpp" line="1075"/>
        <location filename="../../qmc2main.cpp" line="1082"/>
        <location filename="../../qmc2main.cpp" line="1115"/>
        <location filename="../../qmc2main.cpp" line="1122"/>
        <location filename="../../qmc2main.cpp" line="1155"/>
        <location filename="../../qmc2main.cpp" line="1180"/>
        <source>Tag</source>
        <translation>Markierung</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="184"/>
        <location filename="../../qmc2main.ui" line="316"/>
        <location filename="../../qmc2main.ui" line="451"/>
        <location filename="../../qmc2main.ui" line="586"/>
        <location filename="../../qmc2main.cpp" line="1093"/>
        <location filename="../../qmc2main.cpp" line="1133"/>
        <location filename="../../qmc2main.cpp" line="1165"/>
        <location filename="../../qmc2main.cpp" line="1190"/>
        <source>ROM types</source>
        <translation>ROM Typen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="189"/>
        <location filename="../../qmc2main.ui" line="321"/>
        <location filename="../../qmc2main.ui" line="456"/>
        <location filename="../../qmc2main.ui" line="591"/>
        <location filename="../../qmc2main.cpp" line="1095"/>
        <location filename="../../qmc2main.cpp" line="1135"/>
        <location filename="../../qmc2main.cpp" line="1167"/>
        <location filename="../../qmc2main.cpp" line="1192"/>
        <source>Players</source>
        <translation>Spieler</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="194"/>
        <location filename="../../qmc2main.ui" line="326"/>
        <location filename="../../qmc2main.ui" line="461"/>
        <location filename="../../qmc2main.ui" line="596"/>
        <location filename="../../qmc2main.cpp" line="1097"/>
        <location filename="../../qmc2main.cpp" line="1137"/>
        <location filename="../../qmc2main.cpp" line="1169"/>
        <location filename="../../qmc2main.cpp" line="1194"/>
        <source>Driver status</source>
        <translation>Treiberstatus</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="199"/>
        <location filename="../../qmc2main.ui" line="331"/>
        <location filename="../../qmc2main.ui" line="466"/>
        <location filename="../../qmc2main.ui" line="601"/>
        <location filename="../../qmc2main.cpp" line="1100"/>
        <location filename="../../qmc2main.cpp" line="1140"/>
        <location filename="../../qmc2main.cpp" line="1196"/>
        <source>Category</source>
        <translation>Kategorie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="204"/>
        <location filename="../../qmc2main.ui" line="336"/>
        <location filename="../../qmc2main.ui" line="471"/>
        <location filename="../../qmc2main.ui" line="606"/>
        <location filename="../../qmc2main.cpp" line="1103"/>
        <location filename="../../qmc2main.cpp" line="1143"/>
        <location filename="../../qmc2main.cpp" line="1171"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="259"/>
        <location filename="../../qmc2main.ui" line="391"/>
        <source>Loading game list, please wait...</source>
        <translation>Lade Spieleliste, bitte warten...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="414"/>
        <source>List of games viewed by category</source>
        <translation>Liste der Spiele angezeigt nach Kategorien</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="421"/>
        <location filename="../../qmc2main.cpp" line="1153"/>
        <source>Category / Game</source>
        <translation>Kategorie / Spiel</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="549"/>
        <source>List of games viewed by version they were added to the emulator</source>
        <translation>Liste der Spiele angezeigt nach der Emulator-Version bei der sie hinzugefügt wurden</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="556"/>
        <location filename="../../qmc2main.cpp" line="1178"/>
        <source>Version / Game</source>
        <translation>Version / Spiel</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="759"/>
        <source>Pl&amp;ayed</source>
        <translation>Ges&amp;pielt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="811"/>
        <source>Emulator</source>
        <translation>Emulator</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="847"/>
        <location filename="../../qmc2main.ui" line="850"/>
        <source>L:Listed - C:Correct - M:Mostly correct - I:Incorrect - N:Not found - U:Unknown - S:Search - T:Tagged</source>
        <translation>L:Liste - K:Korrekt - B:Beinahe korrekt - I:Inkorrekt - N:Nicht gefunden - U:Unbekannt - S:Suche - M:Markiert</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="891"/>
        <location filename="../../qmc2main.ui" line="894"/>
        <source>Indicator for current memory usage</source>
        <translation>Anzeige der aktuellen Speicherauslastung</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1498"/>
        <source>&amp;Devices</source>
        <translation>&amp;Geräte</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1525"/>
        <source>Mar&amp;quee</source>
        <translation>Mar&amp;quee</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1543"/>
        <source>MA&amp;WS</source>
        <translation>MA&amp;WS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1552"/>
        <source>&amp;PCB</source>
        <translation>&amp;PCB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1561"/>
        <source>Softwar&amp;e list</source>
        <translation>Softwar&amp;e Liste</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1570"/>
        <source>&amp;YouTube</source>
        <translation>&amp;YouTube</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1748"/>
        <source>MP&amp;3 player</source>
        <translation>MP&amp;3 Spieler</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1769"/>
        <location filename="../../qmc2main.ui" line="1772"/>
        <source>Playlist (move items by dragging &amp; dropping them)</source>
        <translation>Playliste (Einträge können per Drag &amp; Drop bewegt werden)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1790"/>
        <location filename="../../qmc2main.ui" line="1793"/>
        <location filename="../../qmc2main.ui" line="2979"/>
        <source>Previous track</source>
        <translation>Vorheriges Stück</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1804"/>
        <location filename="../../qmc2main.ui" line="1807"/>
        <location filename="../../qmc2main.ui" line="3000"/>
        <source>Next track</source>
        <translation>Nächstes Stück</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1818"/>
        <location filename="../../qmc2main.ui" line="1821"/>
        <location filename="../../qmc2main.ui" line="3021"/>
        <source>Fast backward</source>
        <translation>Schneller Rücklauf</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1841"/>
        <location filename="../../qmc2main.ui" line="1844"/>
        <location filename="../../qmc2main.ui" line="3039"/>
        <source>Fast forward</source>
        <translation>Schneller Vorlauf</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1968"/>
        <location filename="../../qmc2main.ui" line="1971"/>
        <source>Enter URL to add to playlist</source>
        <translation>URL zu Playliste hinzufügen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1999"/>
        <location filename="../../qmc2main.ui" line="2002"/>
        <source>Setup available audio effects</source>
        <translation>Verfügbare Audio Effekte einstellen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2013"/>
        <location filename="../../qmc2main.ui" line="2016"/>
        <source>Start playing automatically when QMC2 has started</source>
        <translation>Automatisch mit der Wiedergabe beginnen wenn QMC2 gestartet wurde</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2019"/>
        <source>Play on start</source>
        <translation>Beim Start spielen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2026"/>
        <location filename="../../qmc2main.ui" line="2029"/>
        <source>Select random tracks from playlist</source>
        <translation>Zufällig Stücke aus der Playliste auswählen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2032"/>
        <source>Shuffle</source>
        <translation>Zufällig</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2039"/>
        <location filename="../../qmc2main.ui" line="2042"/>
        <source>Automatically pause audio playback when at least one emulator is running</source>
        <translation>Audio Wiedergabe automatisch pausieren, wenn mindestens ein Emulator läuft</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2045"/>
        <source>Pause</source>
        <translation>Pausieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2061"/>
        <source>Fade in/out</source>
        <translation>Ein-/Ausblenden</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2097"/>
        <source>Dow&amp;nloads</source>
        <translation>Dow&amp;nloads</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2204"/>
        <source>Automatically remove successfully finished downloads from this list</source>
        <translation>Erfolgreich beendete Downloads automatisch aus dieser Liste entfernen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2315"/>
        <source>&amp;Game</source>
        <translation>&amp;Spiel</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2349"/>
        <source>&amp;Tag</source>
        <translation>&amp;Markierung</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2856"/>
        <source>Analyse ROM (tagged)...</source>
        <translation>ROM analysieren (markierte)...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2859"/>
        <source>Analyse ROM (tagged)</source>
        <translation>ROMs analysieren (markierte)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2862"/>
        <location filename="../../qmc2main.ui" line="2865"/>
        <source>Analyse all tagged sets with ROMAlyzer</source>
        <translation>Alle markierten Sets mit dem ROMAlyzer analysieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2985"/>
        <source>Ctrl+Alt+Left</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3006"/>
        <source>Ctrl+Alt+Right</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3027"/>
        <source>Ctrl+Alt+B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3045"/>
        <source>Ctrl+Alt+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3066"/>
        <source>Ctrl+Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3114"/>
        <source>Ctrl+Alt+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3271"/>
        <source>Check template map</source>
        <translation>Abbildungsvorlage überprüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3274"/>
        <location filename="../../qmc2main.ui" line="3277"/>
        <source>Check template map against the configuration options of the currently selected emulator</source>
        <translation>Abbildungsvorlage gegen die Konfigurationsoptionen des aktuell ausgewählten Emulators prüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3280"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3295"/>
        <location filename="../../qmc2main.ui" line="3298"/>
        <source>Play current game (embedded)</source>
        <translation>Ausgewähltes Spiel spielen (eingebettet)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3301"/>
        <source>Ctrl+Shift+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3313"/>
        <source>&amp;Demo mode...</source>
        <translation>&amp;Demo Modus...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3316"/>
        <location filename="../../qmc2main.ui" line="3319"/>
        <source>Open the demo mode dialog</source>
        <translation>Dialog für Demo Modus aufrufen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3598"/>
        <source>Set tag</source>
        <translation>Markierung setzen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3601"/>
        <location filename="../../qmc2main.ui" line="3604"/>
        <source>Set tag mark</source>
        <translation>Markierung entfernen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3607"/>
        <source>Ctrl+Shift+T</source>
        <translation>Ctrl+Shift+T</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3619"/>
        <source>Unset tag</source>
        <translation>Markierung aufheben</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3622"/>
        <location filename="../../qmc2main.ui" line="3625"/>
        <source>Unset tag mark</source>
        <translation>Aktuelle Markierung aufheben</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3628"/>
        <source>Ctrl+Shift+U</source>
        <translation>Ctrl+Shift+U</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3640"/>
        <source>Toggle tag</source>
        <translation>Markierung umkehren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3643"/>
        <location filename="../../qmc2main.ui" line="3646"/>
        <source>Toggle tag mark</source>
        <translation>Aktuelle Markierung umkehren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3649"/>
        <source>Ctrl+Shift+G</source>
        <translation>Ctrl+Shift+G</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3661"/>
        <source>Tag all</source>
        <translation>Alle Markierungen setzen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3664"/>
        <location filename="../../qmc2main.ui" line="3667"/>
        <source>Set tag mark for all sets</source>
        <translation>Markierungen für alle Sets setzen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3670"/>
        <source>Ctrl+Shift+L</source>
        <translation>Ctrl+Shift+L</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3682"/>
        <source>Untag all</source>
        <translation>Alle Markierungen aufheben</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3685"/>
        <location filename="../../qmc2main.ui" line="3688"/>
        <source>Unset all tag marks</source>
        <translation>Alle Markierungen aufheben</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3691"/>
        <source>Ctrl+Shift+N</source>
        <translation>Ctrl+Shift+N</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3703"/>
        <source>Invert all tags</source>
        <translation>Alle Markierungen umkehren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3706"/>
        <location filename="../../qmc2main.ui" line="3709"/>
        <source>Invert all tag marks</source>
        <translation>Alle Markierungen umkehren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3712"/>
        <source>Ctrl+Shift+I</source>
        <translation>Ctrl+Shift+I</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2868"/>
        <source>Ctrl+Shift+D</source>
        <translation>Ctrl+Shift+D</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3331"/>
        <source>By category</source>
        <translation>Nach Kategorie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3334"/>
        <location filename="../../qmc2main.ui" line="3337"/>
        <source>View games by category</source>
        <translation>Spiele nach Kategorien anzeigen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3340"/>
        <source>F7</source>
        <translation>F7</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3352"/>
        <source>By version</source>
        <translation>Nach Version</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3355"/>
        <location filename="../../qmc2main.ui" line="3358"/>
        <source>View games by version they were added to the emulator</source>
        <translation>Spiele nach der Emulator-Version anzeigen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3361"/>
        <source>F8</source>
        <translation>F8</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3373"/>
        <source>Run external ROM tool...</source>
        <translation>Externes ROM Werkzeug ausführen...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3376"/>
        <location filename="../../qmc2main.ui" line="3379"/>
        <source>Run tool to process ROM data externally</source>
        <translation>Externes Werkzeug zur Verarbeitung der ROM Daten ausführen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3382"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3394"/>
        <source>Run external ROM tool (tagged)...</source>
        <translation>Externes ROM Werkzeug ausführen (markierte)...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3397"/>
        <location filename="../../qmc2main.ui" line="3400"/>
        <source>Run tool to process ROM data externally for all tagged sets</source>
        <translation>Externes Werkzeug zur Verarbeitung der ROM Daten für alle markierten Sets ausführen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3403"/>
        <source>Ctrl+Shift+F9</source>
        <translation>Ctrl+Shift+F9</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3411"/>
        <location filename="../../qmc2main.ui" line="3414"/>
        <source>Clear YouTube cache</source>
        <translation>YouTube Cache leeren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3417"/>
        <source>Ctrl+Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3425"/>
        <source>Clear ROM state cache</source>
        <translation>ROM-Status Cache leeren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3428"/>
        <location filename="../../qmc2main.ui" line="3431"/>
        <source>Forcedly clear (remove) the ROM state cache</source>
        <translation>ROM-Status Cache erzwungenermaßen leeren (löschen)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3439"/>
        <source>Clear game list cache</source>
        <translation>Spielelisten-Cache leeren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3442"/>
        <location filename="../../qmc2main.ui" line="3445"/>
        <source>Forcedly clear (remove) the game list cache</source>
        <translation>Spielelisten-Cache erzwungenermaßen leeren (löschen)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3453"/>
        <source>Clear XML cache</source>
        <translation>XML Cache leeren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3456"/>
        <location filename="../../qmc2main.ui" line="3459"/>
        <source>Forcedly clear (remove) the XML cache</source>
        <translation>XML Cache erzwungenermaßen leeren (löschen)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3467"/>
        <source>Clear software list cache</source>
        <translation>Software-Listen Cache leeren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3470"/>
        <location filename="../../qmc2main.ui" line="3473"/>
        <source>Forcedly clear (remove) the software list cache</source>
        <translation>Software-Listen Cache erzwungenermaßen leeren (löschen)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3481"/>
        <source>Clear ALL emulator caches</source>
        <translation>ALLE Emulator Caches leeren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3484"/>
        <location filename="../../qmc2main.ui" line="3487"/>
        <source>Forcedly clear (remove) ALL emulator related caches</source>
        <translation>ALLE Emulator Caches erzwungenermaßen leeren (löschen)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1915"/>
        <location filename="../../qmc2main.ui" line="1918"/>
        <source>Progress indicator for current track</source>
        <translation>Fortschrittsanzeige für aktuelles Stück</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1924"/>
        <location filename="../../qmc2main.cpp" line="6560"/>
        <location filename="../../qmc2main.cpp" line="6570"/>
        <source>%vs (%ms total)</source>
        <translation>%vs (%ms gesamt)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3090"/>
        <source>Ctrl+Alt+#</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1864"/>
        <location filename="../../qmc2main.ui" line="1867"/>
        <location filename="../../qmc2main.ui" line="3060"/>
        <source>Stop track</source>
        <translation>Stück anhalten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1881"/>
        <location filename="../../qmc2main.ui" line="1884"/>
        <location filename="../../qmc2main.ui" line="3084"/>
        <source>Pause track</source>
        <translation>Stück pausieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1898"/>
        <location filename="../../qmc2main.ui" line="1901"/>
        <location filename="../../qmc2main.ui" line="3108"/>
        <source>Play track</source>
        <translation>Stück abspielen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1692"/>
        <location filename="../../qmc2main.ui" line="1695"/>
        <source>Emulator control panel</source>
        <translation>Emulator Kontrolle</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1705"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1730"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1715"/>
        <location filename="../../qmc2main.ui" line="2140"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1720"/>
        <source>LED0</source>
        <translation>LED0</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1725"/>
        <source>LED1</source>
        <translation>LED1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1735"/>
        <source>Command</source>
        <translation>Kommando</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6884"/>
        <location filename="../../qmc2main.cpp" line="6951"/>
        <location filename="../../qmc2main.cpp" line="6961"/>
        <source>running</source>
        <translation>rennt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6900"/>
        <source>stopped</source>
        <translation>beendet</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4157"/>
        <location filename="../../qmc2main.cpp" line="6945"/>
        <location filename="../../qmc2main.cpp" line="6959"/>
        <source>paused</source>
        <translation>pausiert</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="733"/>
        <location filename="../../qmc2main.cpp" line="4196"/>
        <source>Copy emulator command line to clipboard</source>
        <translation>Emulator Kommandozeile in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="734"/>
        <location filename="../../qmc2main.cpp" line="4197"/>
        <source>&amp;Copy command</source>
        <translation>&amp;Kommando kopieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4099"/>
        <source>emulator #%1 is already embedded</source>
        <translation>Emulator #%1 ist bereits eingebettet</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4128"/>
        <source>FATAL: can&apos;t start XWININFO within a reasonable time frame, giving up</source>
        <translation>FATAL: kann XWININFO nicht in einem angemessenen Zeitraum starten, gebe auf</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4148"/>
        <source>WARNING: multiple windows for emulator #%1 found, choosing window ID %2 for embedding</source>
        <translation>WARNUNG: mehrere Fenster für Emulator #%1 gefunden, wähle Fenster ID %2 für Einbettung</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4156"/>
        <source>embedding emulator #%1, window ID = %2</source>
        <translation>Emulator #%1 wird eingebettet, Fenster ID = %2</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6761"/>
        <source>WARNING: can&apos;t create SDLMAME output notifier FIFO, path = %1</source>
        <translation>WARNUNG: kann SDLMAME Ausgabe Benachrichtigungs-FIFO nicht erzeugen, Pfad = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6789"/>
        <source>SDLMAME output notifier FIFO created</source>
        <translation>SDLMAME Ausgabe Benachrichtigungs-FIFO erzeugt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6793"/>
        <location filename="../../qmc2main.cpp" line="6796"/>
        <source>WARNING: can&apos;t open SDLMAME output notifier FIFO for reading, path = %1</source>
        <translation>WARNUNG: kann SDLMAME Ausgabe Benachrichtigungs-FIFO nicht zum Lesen öffnen, Pfad = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6917"/>
        <location filename="../../qmc2main.cpp" line="6988"/>
        <source>unhandled MAME output notification: game = %1, class = %2, what = %3, state = %4</source>
        <translation>nicht behandelte MAME Ausgabe Benachrichtigung: Spiel = %1, Klasse = %2, Was = %3, Status = %4</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1710"/>
        <source>Game / Notifier</source>
        <translation>Spiel / Anzeiger</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2274"/>
        <source>&amp;Clean up</source>
        <translation>Au&amp;fräumen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2256"/>
        <source>&amp;Audio player</source>
        <translation>&amp;Audio Player</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2382"/>
        <source>&amp;Display</source>
        <translation>&amp;Anzeige</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2550"/>
        <source>&amp;About...</source>
        <translation>Ü&amp;ber...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2571"/>
        <source>About &amp;Qt...</source>
        <translation>Über &amp;Qt...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2668"/>
        <source>Recreate template map</source>
        <translation>Abbildungsvorlage neu erzeugen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2710"/>
        <source>Full detail</source>
        <translation>Alle Details</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2832"/>
        <source>Analyse ROM...</source>
        <translation>ROM analysieren...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2731"/>
        <source>Parent / clones</source>
        <translation>Original / Klone</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="94"/>
        <source>Parent / clone hierarchy (not filtered)</source>
        <translation>Original / Klon Hierarchie (ungefiltert)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="282"/>
        <source>Parent / clone hierarchy</source>
        <translation>Original / Klon Hierarchie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2734"/>
        <location filename="../../qmc2main.ui" line="2737"/>
        <source>View parent / clone hierarchy</source>
        <translation>Original / Klon Hierarchie anzeigen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2982"/>
        <source>Play previous track</source>
        <translation>Vorheriges Stück abspielen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3003"/>
        <source>Play next track</source>
        <translation>Nächstes Stück abspielen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3024"/>
        <source>Fast backward within track</source>
        <translation>Schneller Rücklauf innerhalb des Stücks</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3063"/>
        <source>Stop current track</source>
        <translation>Aktuelles Stück anhalten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3087"/>
        <source>Pause current track</source>
        <translation>Aktuelles Stück pausieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3111"/>
        <source>Play current track</source>
        <translation>Aktuelles Stück abspielen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3042"/>
        <source>Fast forward within track</source>
        <translation>Schneller Vorlauf innerhalb des Stücks</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6383"/>
        <source>Select one or more audio files</source>
        <translation>Eine oder mehrere Audio Dateien auswählen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1954"/>
        <location filename="../../qmc2main.ui" line="1957"/>
        <source>Browse for tracks to add to playlist</source>
        <translation>Stücke auswählen und der Playliste hinzufügen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1985"/>
        <location filename="../../qmc2main.ui" line="1988"/>
        <source>Remove selected tracks from playlist</source>
        <translation>Ausgewählte Stücke aus der Playliste entfernen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1933"/>
        <location filename="../../qmc2main.ui" line="1936"/>
        <location filename="../../qmc2main.ui" line="2071"/>
        <location filename="../../qmc2main.ui" line="2074"/>
        <source>Audio player volume</source>
        <translation>Lautstärke des  Audio Players</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2055"/>
        <location filename="../../qmc2main.ui" line="2058"/>
        <source>Fade in and out on pause / resume</source>
        <translation>Auotmatisch ein- und ausblenden beim Pausieren / Fortsetzen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6682"/>
        <source>audio player: track info: title = &apos;%1&apos;, artist = &apos;%2&apos;, album = &apos;%3&apos;, genre = &apos;%4&apos;</source>
        <translation>Audio Player: Track Info: Titel = &apos;%1&apos;, Künstler = &apos;%2&apos;, Album = &apos;%3&apos;, Genre = &apos;%4&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3125"/>
        <source>Raise volume</source>
        <translation>Lautstärke anheben</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3128"/>
        <source>Raise audio player volume</source>
        <translation>Lautstärke des Audio Players anheben</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3131"/>
        <source>Ctrl+Alt+PgUp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3139"/>
        <source>Lower volume</source>
        <translation>Lautstärke absenken</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3142"/>
        <source>Lower audio player volume</source>
        <translation>Lautstärke des Audio Players absenken</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3145"/>
        <source>Ctrl+Alt+PgDown</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3157"/>
        <source>&amp;Export ROM status...</source>
        <translation>ROM Status &amp;exportieren...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3160"/>
        <location filename="../../qmc2main.ui" line="3163"/>
        <source>Export ROM status</source>
        <translation>ROM Status exportieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3166"/>
        <source>Ctrl+E</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5340"/>
        <source>destroying ROM status exporter</source>
        <translation>Zerstöre ROM Status Exporter</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6773"/>
        <source>WARNING: can&apos;t create SDLMESS output notifier FIFO, path = %1</source>
        <translation>WARNUNG: kann SDLMESS Ausgabe Benachrichtigungs-FIFO nicht erzeugen, Pfad = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6800"/>
        <source>SDLMESS output notifier FIFO created</source>
        <translation>SDLMESS Ausgabe Benachrichtigungs-FIFO erzeugt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6804"/>
        <location filename="../../qmc2main.cpp" line="6807"/>
        <source>WARNING: can&apos;t open SDLMESS output notifier FIFO for reading, path = %1</source>
        <translation>WARNUNG: kann SDLMESS Ausgabe Benachrichtigungs-FIFO nicht zum Lesen öffnen, Pfad = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6919"/>
        <location filename="../../qmc2main.cpp" line="6990"/>
        <source>unhandled MESS output notification: game = %1, class = %2, what = %3, state = %4</source>
        <translation>nicht behandelte MESS Ausgabe Benachrichtigung: Spiel = %1, Klasse = %2, Was = %3, Status = %4</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="528"/>
        <location filename="../../qmc2main.cpp" line="1080"/>
        <source>Machine / Attribute</source>
        <translation>Maschine / Attribut</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="529"/>
        <location filename="../../qmc2main.cpp" line="1120"/>
        <source>Machine / Clones</source>
        <translation>Maschine / Klone</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="531"/>
        <source>Machine &amp;info</source>
        <translation>Machinen &amp;Info</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1417"/>
        <source>Game &amp;info</source>
        <translation>Spiel &amp;Info</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="55"/>
        <source>&amp;Game list</source>
        <translation>&amp;Spieleliste</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="534"/>
        <location filename="../../qmc2main.cpp" line="535"/>
        <source>Favorite machines</source>
        <translation>Favorisierte Maschinen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="536"/>
        <location filename="../../qmc2main.cpp" line="537"/>
        <source>Machines last played</source>
        <translation>Zuletzt gespielte Maschinen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="532"/>
        <location filename="../../qmc2main.cpp" line="533"/>
        <source>Detailed machine info</source>
        <translation>Detaillierte Maschinen Informationen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="556"/>
        <source>Machine / Notifier</source>
        <translation>Maschine / Anzeiger</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="538"/>
        <location filename="../../qmc2main.cpp" line="539"/>
        <source>Play current machine</source>
        <translation>Aktuelle Maschine starten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="558"/>
        <location filename="../../qmc2main.cpp" line="559"/>
        <location filename="../../qmc2main.cpp" line="765"/>
        <location filename="../../qmc2main.cpp" line="817"/>
        <location filename="../../qmc2main.cpp" line="928"/>
        <source>Add current machine to favorites</source>
        <translation>Aktuelle Maschine zu Favoriten hinzufügen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="562"/>
        <location filename="../../qmc2main.cpp" line="563"/>
        <source>Reload entire machine list</source>
        <translation>Gesamte Maschinenliste neu laden</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="564"/>
        <location filename="../../qmc2main.cpp" line="565"/>
        <source>View machine list with full detail</source>
        <translation>Maschinenliste mit vollen Details anzeigen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="570"/>
        <location filename="../../qmc2main.cpp" line="571"/>
        <location filename="../../qmc2main.cpp" line="775"/>
        <location filename="../../qmc2main.cpp" line="827"/>
        <location filename="../../qmc2main.cpp" line="870"/>
        <location filename="../../qmc2main.cpp" line="938"/>
        <source>Check current machine&apos;s ROM state</source>
        <translation>ROM-Status der aktuellen Maschine überprüfen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="572"/>
        <location filename="../../qmc2main.cpp" line="573"/>
        <source>Analyse current machine with ROMAlyzer</source>
        <translation>Aktuelle Maschine mit dem ROMAlyzer analysieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="574"/>
        <source>M&amp;achine</source>
        <translation>M&amp;aschine</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="575"/>
        <source>Machine list with full detail (filtered)</source>
        <translation>Maschinenliste mit vollen Details (gefiltert)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="576"/>
        <location filename="../../qmc2main.cpp" line="577"/>
        <source>Select between detailed machine list and parent / clone hierarchy</source>
        <translation>Zwischen detaillierter Maschinenliste und Maschine / Klon Hierarchie wechseln</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="78"/>
        <location filename="../../qmc2main.ui" line="81"/>
        <source>Switch between detailed game list and parent / clone hierarchy</source>
        <translation>Zwischen detaillierter Spieleliste und Spiel / Klon Hierarchie wechseln</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="85"/>
        <source>Game list with full detail (filtered)</source>
        <translation>Spieleliste mit vollen Details (gefiltert)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2637"/>
        <location filename="../../qmc2main.ui" line="2640"/>
        <source>Reload entire game list</source>
        <translation>Gesamte Spieleliste neu laden</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2671"/>
        <location filename="../../qmc2main.ui" line="2674"/>
        <source>Recreate template configuration map</source>
        <translation>Konfigurationsabbildungs-Vorlage neu erzeugen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2713"/>
        <location filename="../../qmc2main.ui" line="2716"/>
        <source>View game list with full detail</source>
        <translation>Spieleliste mit vollen Details anzeigen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2838"/>
        <location filename="../../qmc2main.ui" line="2841"/>
        <source>Analyse current game with ROMAlyzer</source>
        <translation>Aktuelles Spiel mit dem ROMAlyzer analysieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5234"/>
        <source>destroying current machine&apos;s emulator configuration</source>
        <translation>Zerstöre aktuelle Emulator-Konfiguration</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="578"/>
        <location filename="../../qmc2main.cpp" line="579"/>
        <location filename="../../qmc2main.cpp" line="584"/>
        <source>Machine status indicator</source>
        <translation>Maschinen Status Indikator</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1366"/>
        <location filename="../../qmc2main.ui" line="1369"/>
        <source>Game status indicator</source>
        <translation>Spiel Status Indikator</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="853"/>
        <source>&lt;b&gt;&lt;font color=black&gt;L:?&lt;/font&gt; &lt;font color=#00cc00&gt;C:?&lt;/font&gt; &lt;font color=#a2c743&gt;M:?&lt;/font&gt; &lt;font color=#f90000&gt;I:?&lt;/font&gt; &lt;font color=#7f7f7f&gt;N:?&lt;/font&gt; &lt;font color=#0000f9&gt;U:?&lt;/font&gt; &lt;font color=chocolate&gt;S:?&lt;/font&gt;&lt;/b&gt;</source>
        <translation>&lt;b&gt;&lt;font color=black&gt;L:?&lt;/font&gt; &lt;font color=#00cc00&gt;K:?&lt;/font&gt; &lt;font color=#a2c743&gt;B:?&lt;/font&gt; &lt;font color=#f90000&gt;I:?&lt;/font&gt; &lt;font color=#7f7f7f&gt;N:?&lt;/font&gt; &lt;font color=#0000f9&gt;U:?&lt;/font&gt; &lt;font color=chocolate&gt;S:?&lt;/font&gt;&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="585"/>
        <source>Show vertical machine status indicator in machine details</source>
        <translation>Zeige vertikalen Maschinen Status Indikator in den Maschinen-Details</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="586"/>
        <source>Show the machine status indicator only when the machine list is not visible due to the current layout</source>
        <translation>Zeige Maschinen Status Indikator nur an, wenn die Maschinenliste aufgrund des aktuellen Layouts nicht sichtbar ist</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="587"/>
        <source>Show machine name</source>
        <translation>Zeige Maschinennamen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="589"/>
        <source>Show machine&apos;s description only when the machine list is not visible due to the current layout</source>
        <translation>Zeige Maschinenbeschreibung nur an, wenn die Maschinenliste aufgrund des aktuellen Layouts nicht sichtbar ist</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5224"/>
        <source>saving current machine&apos;s device configurations</source>
        <translation>Speichere Geräte-Konfigurationen der aktuellen Maschine</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1562"/>
        <source>No devices</source>
        <translation>Keine Geräte</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1282"/>
        <source>&amp;Correct</source>
        <translation>&amp;Korrekt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1288"/>
        <source>&amp;Mostly correct</source>
        <translation>&amp;Beinahe korrekt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1294"/>
        <source>&amp;Incorrect</source>
        <translation>&amp;Inkorrekt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1300"/>
        <source>&amp;Not found</source>
        <translation>&amp;Nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1306"/>
        <source>&amp;Unknown</source>
        <translation>&amp;Unbekannt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="112"/>
        <location filename="../../qmc2main.ui" line="115"/>
        <source>Toggle individual ROM states</source>
        <translation>Indiviuelle ROM Stati ein-/ausblenden</translation>
    </message>
    <message>
        <location filename="../../macros.h" line="427"/>
        <location filename="../../macros.h" line="433"/>
        <location filename="../../qmc2main.cpp" line="527"/>
        <source>M.E.S.S. Catalog / Launcher II</source>
        <translation>M.E.S.S. Catalog / Launcher II</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3178"/>
        <source>QMC2 for SDLMAME</source>
        <translation>QMC2 für SDLMAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3181"/>
        <location filename="../../qmc2main.ui" line="3184"/>
        <source>Launch QMC2 for SDLMAME</source>
        <translation>QMC2 für SDLMAME starten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3199"/>
        <source>QMC2 for SDLMESS</source>
        <translation>QMC2 für SDLMESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3202"/>
        <location filename="../../qmc2main.ui" line="3205"/>
        <source>Launch QMC2 for SDLMESS</source>
        <translation>QMC2 für SDLMESS starten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3187"/>
        <source>Ctrl+Alt+1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3208"/>
        <source>Ctrl+Alt+2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2451"/>
        <location filename="../../qmc2main.cpp" line="2453"/>
        <location filename="../../qmc2main.cpp" line="2552"/>
        <location filename="../../qmc2main.cpp" line="2554"/>
        <source>variant &apos;%1&apos; launched</source>
        <translation>Variante &apos;%1&apos; gestartet</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2491"/>
        <location filename="../../qmc2main.cpp" line="2493"/>
        <location filename="../../qmc2main.cpp" line="2592"/>
        <location filename="../../qmc2main.cpp" line="2594"/>
        <source>WARNING: failed to launch variant &apos;%1&apos;</source>
        <translation>WARNUNG: Variante &apos;%1&apos; konnte nicht gestartet werden</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="582"/>
        <location filename="../../qmc2main.cpp" line="583"/>
        <source>Progress indicator for machine list processing</source>
        <translation>Fortschrittsanzeiger für Verarbeitungen der Maschinenliste</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="866"/>
        <location filename="../../qmc2main.ui" line="869"/>
        <source>Progress indicator for game list processing</source>
        <translation>Fortschrittsanzeiger für Verarbeitungen der Spieleliste</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2655"/>
        <location filename="../../qmc2main.cpp" line="2674"/>
        <source>WARNING: this feature is not yet working!</source>
        <translation>WARNUNG: dieses Feature arbeitet noch nicht richtig!</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5196"/>
        <source>destroying arcade view</source>
        <translation>Zerstöre Arcade View</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2386"/>
        <source>&amp;Arcade</source>
        <translation>&amp;Arcade</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2904"/>
        <location filename="../../qmc2main.cpp" line="1016"/>
        <source>&amp;Setup...</source>
        <translation>Ein&amp;stellen...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2907"/>
        <location filename="../../qmc2main.ui" line="2910"/>
        <source>Setup arcade mode</source>
        <translation>Acade Modus einstellen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2916"/>
        <source>Ctrl+Shift+A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5201"/>
        <source>destroying arcade setup dialog</source>
        <translation>Zerstöre Arcade Setup Dialog</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5250"/>
        <source>destroying game list</source>
        <translation>Zerstöre Spieleliste</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5252"/>
        <source>destroying machine list</source>
        <translation>Zerstöre Maschinenliste</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7645"/>
        <source>ArcadeView is not currently active, can&apos;t take screen shot</source>
        <translation>ArcadeView ist zur Zeit nicht aktiv, kann Screenshot nicht aufzeichnen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3219"/>
        <source>Show FPS</source>
        <translation>FPS anzeigen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3222"/>
        <location filename="../../qmc2main.ui" line="3225"/>
        <source>Toggle FPS display</source>
        <translation>FPS Anzeige ein-/ausschalten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3228"/>
        <source>Meta+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3240"/>
        <source>Screen shot</source>
        <translation>Screenshot</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3243"/>
        <source>Save a screen shot from the current arcade scene</source>
        <translation>Screenshot der aktuellen Arcade Szene aufzeichnen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3246"/>
        <source>Take screen shot from arcade scene</source>
        <translation>Screenshot der Arcade Szene</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3249"/>
        <source>Meta+F12</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2931"/>
        <source>&amp;Toggle arcade</source>
        <translation>&amp;Arcade ein/aus</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2934"/>
        <location filename="../../qmc2main.ui" line="2937"/>
        <source>Toggle arcade mode</source>
        <translation>Arcade Modus ein-/ausschalten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5361"/>
        <source>destroying machine info DB</source>
        <translation>Zerstöre Maschinen-Info DB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5811"/>
        <source>loading machine info DB</source>
        <translation>Lade Maschinen-Info DB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5920"/>
        <source>WARNING: missing &apos;$end&apos; in machine info DB %1</source>
        <translation>WARNUNG: &apos;$end&apos; fehlt in Maschinen-Info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5927"/>
        <source>WARNING: missing &apos;$bio&apos; in machine info DB %1</source>
        <translation>WARNUNG: &apos;$bio&apos; fehlt in Maschinen-Info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5934"/>
        <source>WARNING: missing &apos;$info&apos; in machine info DB %1</source>
        <translation>WARNUNG: &apos;$info&apos; fehlt in Maschinen-Info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5948"/>
        <source>WARNING: can&apos;t open machine info DB %1</source>
        <translation>WARNUNG: kann Maschinen-Info DB %1 nicht öffnen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5959"/>
        <source>done (loading machine info DB, elapsed time = %1)</source>
        <translation>Fertig (Lade Maschinen-Info DB, benötigte Zeit = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5960"/>
        <source>%n machine info record(s) loaded</source>
        <translation>
            <numerusform>%n Maschinen-Info Datensatz geladen</numerusform>
            <numerusform>%n Maschinen-Info Datensätze geladen</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5962"/>
        <source>invalidating machine info DB</source>
        <translation>Invalidiere Maschinen-Info DB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1596"/>
        <source>&amp;Front end log</source>
        <translation>&amp;Frontend Log</translation>
    </message>
    <message>
        <source>L:Listed - C:Correct - M:Mostly correct - I:Incorrect - N:Not found - U:Unknown - S:Search</source>
        <translation type="obsolete">L:Liste - K:Korrekt - B:Beinahe korrekt - I:Inkorrekt - N:Nicht gefunden - U:Unbekannt - S:Suche</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="443"/>
        <source>QMC2 for MAME</source>
        <translation>QMC2 für MAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="444"/>
        <location filename="../../qmc2main.cpp" line="445"/>
        <source>Launch QMC2 for MAME</source>
        <translation>QMC2 für MAME starten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="446"/>
        <source>QMC2 for MESS</source>
        <translation>QMC2 für MESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="447"/>
        <location filename="../../qmc2main.cpp" line="448"/>
        <source>Launch QMC2 for MESS</source>
        <translation>QMC2 für MESS starten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2958"/>
        <source>Toggle &amp;full screen</source>
        <translation>&amp;Vollbild-Modus umschalten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2961"/>
        <source>Toggle full screen</source>
        <translation>Vollbild-Modus umschalten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2964"/>
        <source>Toggle full screen / windowed mode</source>
        <translation>Vollbild- / Fenster-Modus umschalten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="743"/>
        <location filename="../../qmc2main.cpp" line="795"/>
        <location filename="../../qmc2main.cpp" line="847"/>
        <location filename="../../qmc2main.cpp" line="906"/>
        <source>Start selected machine</source>
        <translation>Selektierte Maschine starten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="782"/>
        <location filename="../../qmc2main.cpp" line="834"/>
        <location filename="../../qmc2main.cpp" line="877"/>
        <location filename="../../qmc2main.cpp" line="945"/>
        <source>Analyse current game&apos;s ROM set with ROMAlyzer</source>
        <translation>ROM-Set des aktuellen Spiels mit dem ROMAlyzer analysieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="784"/>
        <location filename="../../qmc2main.cpp" line="836"/>
        <location filename="../../qmc2main.cpp" line="879"/>
        <location filename="../../qmc2main.cpp" line="947"/>
        <source>Analyse current machine&apos;s ROM set with ROMAlyzer</source>
        <translation>ROM-Set der aktuellen Maschine mit dem ROMAlyzer analysieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="972"/>
        <location filename="../../qmc2main.cpp" line="994"/>
        <location filename="../../qmc2main.cpp" line="1022"/>
        <source>Set tab position north</source>
        <translation>Tabulator Position &apos;Norden&apos; setzen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="973"/>
        <location filename="../../qmc2main.cpp" line="995"/>
        <location filename="../../qmc2main.cpp" line="1023"/>
        <source>&amp;North</source>
        <translation>&amp;Norden</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="977"/>
        <location filename="../../qmc2main.cpp" line="999"/>
        <location filename="../../qmc2main.cpp" line="1027"/>
        <source>Set tab position south</source>
        <translation>Tabulator Position &apos;Süden&apos; setzen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="978"/>
        <location filename="../../qmc2main.cpp" line="1000"/>
        <location filename="../../qmc2main.cpp" line="1028"/>
        <source>&amp;South</source>
        <translation>&amp;Süden</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="982"/>
        <location filename="../../qmc2main.cpp" line="1004"/>
        <location filename="../../qmc2main.cpp" line="1032"/>
        <source>Set tab position west</source>
        <translation>Tabulator Position &apos;Westen&apos; setzen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="983"/>
        <location filename="../../qmc2main.cpp" line="1005"/>
        <location filename="../../qmc2main.cpp" line="1033"/>
        <source>&amp;West</source>
        <translation>&amp;Westen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="987"/>
        <location filename="../../qmc2main.cpp" line="1009"/>
        <location filename="../../qmc2main.cpp" line="1037"/>
        <source>Set tab position east</source>
        <translation>Tabulator Position &apos;Osten&apos; setzen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="988"/>
        <location filename="../../qmc2main.cpp" line="1010"/>
        <location filename="../../qmc2main.cpp" line="1038"/>
        <source>&amp;East</source>
        <translation>&amp;Osten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="588"/>
        <source>Show machine&apos;s description at the bottom of any images</source>
        <translation>Maschinenbeschreibung am unteren Rand jedes Bildes anzeigen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5286"/>
        <source>destroying cabinet</source>
        <translation>Zerstöre Gehäuse</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5290"/>
        <source>destroying controller</source>
        <translation>Zerstöre Controller</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1453"/>
        <source>Em&amp;ulator info</source>
        <translation>Em&amp;ulator Info</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1507"/>
        <source>Ca&amp;binet</source>
        <translation>Geh&amp;äuse</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1516"/>
        <source>C&amp;ontroller</source>
        <translation>C&amp;ontroller</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1534"/>
        <source>Titl&amp;e</source>
        <translation>Tit&amp;el</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5294"/>
        <source>destroying marquee</source>
        <translation>Zerstöre Marquee</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5298"/>
        <source>destroying title</source>
        <translation>Zerstöre Titel</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Wert</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1015"/>
        <source>Detail setup</source>
        <translation>Einstellung der Details</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5345"/>
        <source>destroying detail setup</source>
        <translation>Zerstöre Detail-Einstellung</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3502"/>
        <source>Fetching MAWS page for &apos;%1&apos;, please wait...</source>
        <translation>Lade MAWS Seite für &apos;%1&apos;, bitte warten...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3464"/>
        <source>MAWS page for &apos;%1&apos;</source>
        <translation>MAWS Seite für &apos;%1&apos;</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2046"/>
        <source>freed %n byte(s) in %1</source>
        <translation>
            <numerusform>%n Byte in %1 freigegeben</numerusform>
            <numerusform>%n Bytes in %1 freigegeben</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2046"/>
        <source>%n entry(s)</source>
        <translation>
            <numerusform>%n Eintrag</numerusform>
            <numerusform>%n Einträgen</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2048"/>
        <source>MAWS in-memory cache cleared (%1)</source>
        <translation>Speicher-residenter MAWS Cache geleert (%1)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3257"/>
        <location filename="../../qmc2main.ui" line="3260"/>
        <source>Clear MAWS cache</source>
        <translation>MAWS Cache leeren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3263"/>
        <source>Ctrl+M</source>
        <translation>Ctrl+M</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2064"/>
        <location filename="../../qmc2main.cpp" line="2091"/>
        <source>removed %n byte(s) in %1</source>
        <translation>
            <numerusform>%n Byte in %1 gelöscht</numerusform>
            <numerusform>%n Bytes in %1 gelöscht</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2064"/>
        <location filename="../../qmc2main.cpp" line="2091"/>
        <source>%n file(s)</source>
        <translation>
            <numerusform>%n Datei</numerusform>
            <numerusform>%n Dateien</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2065"/>
        <source>MAWS on-disk cache cleared (%1)</source>
        <translation>MAWS Festplatten-Cache geleert (%1)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2112"/>
        <location filename="../../qmc2main.ui" line="2115"/>
        <source>List of active/inactive downloads</source>
        <translation>Liste aktiver/inaktiver Downloads</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2145"/>
        <source>Progress</source>
        <translation>Fortschritt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2153"/>
        <location filename="../../qmc2main.ui" line="2156"/>
        <source>Clear finished / stopped downloads from list</source>
        <translation>Beendete / angehaltene Downloads aus der Liste löschen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2170"/>
        <location filename="../../qmc2main.ui" line="2173"/>
        <source>Reload selected downloads</source>
        <translation>Ausgewählte Downloads erneut laden</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2187"/>
        <location filename="../../qmc2main.ui" line="2190"/>
        <source>Stop selected downloads</source>
        <translation>Ausgewählte Downloads anhalten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8155"/>
        <source>Choose file to store download</source>
        <translation>Datei zum Speichern auswählen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7790"/>
        <source>Quick download links for MAWS data usable by QMC2</source>
        <translation>Schnell-Download Links für MAWS-Daten, die von QMC2 verwendet werden können</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7952"/>
        <source>Setup...</source>
        <translation>Einstellen...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7815"/>
        <source>Cabinet art</source>
        <translation>Gehäuse-Bilder</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="286"/>
        <location filename="../../qmc2main.cpp" line="300"/>
        <source>last message repeated %n time(s)</source>
        <translation>
            <numerusform>letzte Meldung %n-mal wiederholt</numerusform>
            <numerusform>letzte Meldung %n-mal wiederholt</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="488"/>
        <source>Toggle maximization of embedded emulator windows</source>
        <translation>Maximierung eingebetteter Emulatoren ein-/ausschalten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="548"/>
        <location filename="../../qmc2main.cpp" line="549"/>
        <source>Play current machine (embedded)</source>
        <translation>Ausgewählte Maschine spielen (eingebettet)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="592"/>
        <source>Search for machines (not case-sensitive)</source>
        <translation>Nach Machinen suchen (Groß-/Kleinschreibung wird nicht beachtet)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="593"/>
        <source>Search for machines</source>
        <translation>Nach Maschinen suchen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="715"/>
        <source>Embed emulator widget</source>
        <translation>Emulator Fenster einbetten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="716"/>
        <source>&amp;Embed</source>
        <translation>&amp;Einbetten</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="752"/>
        <location filename="../../qmc2main.cpp" line="804"/>
        <location filename="../../qmc2main.cpp" line="856"/>
        <location filename="../../qmc2main.cpp" line="915"/>
        <source>Play selected game (embedded)</source>
        <translation>Ausgewähltes Spiel spielen (eingebettet)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="754"/>
        <location filename="../../qmc2main.cpp" line="806"/>
        <location filename="../../qmc2main.cpp" line="858"/>
        <location filename="../../qmc2main.cpp" line="917"/>
        <source>Start selected machine (embedded)</source>
        <translation>Ausgewählte Maschine spielen (eingebettet)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3292"/>
        <location filename="../../qmc2main.cpp" line="756"/>
        <location filename="../../qmc2main.cpp" line="808"/>
        <location filename="../../qmc2main.cpp" line="860"/>
        <location filename="../../qmc2main.cpp" line="919"/>
        <source>Play &amp;embedded</source>
        <translation>Spielen (&amp;eingebettet)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1045"/>
        <location filename="../../qmc2main.cpp" line="1057"/>
        <source>Flip splitter orientation</source>
        <translation>Splitter Ausrichtung wechseln</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1046"/>
        <location filename="../../qmc2main.cpp" line="1058"/>
        <source>&amp;Flip splitter orientation</source>
        <translation>&amp;Splitter Ausrichtung wechseln</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1050"/>
        <source>Swap splitter&apos;s sub-layouts</source>
        <translation>Unter-Layouts des Splitters austauschen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1051"/>
        <source>&amp;Swap splitter&apos;s sub-layouts</source>
        <translation>&amp;Unter-Layouts des Splitters austauschen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1062"/>
        <source>Swap splitter&apos;s sub-widgets</source>
        <translation>Unter-Widgets des Splitters austauschen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1063"/>
        <source>&amp;Swap splitter&apos;s sub-widgets</source>
        <translation>&amp;Unter-Widgets des Splitters austauschen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1719"/>
        <source>game list reload is already active</source>
        <translation>Spieleliste wird bereits neu geladen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1721"/>
        <source>machine list reload is already active</source>
        <translation>Maschinenliste wird bereits neu geladen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3552"/>
        <source>Emulator for this game</source>
        <translation>Emulator für dieses Spiel</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3554"/>
        <source>Emulator for this machine</source>
        <translation>Emulator für diese Maschine</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3697"/>
        <location filename="../../qmc2main.cpp" line="3704"/>
        <source>&lt;p&gt;No data available&lt;/p&gt;</source>
        <translation>&lt;p&gt;Keine Daten verfügbar&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="789"/>
        <location filename="../../qmc2main.cpp" line="4155"/>
        <source>Embedded emulators</source>
        <translation>Eingebettete Emulatoren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4165"/>
        <source>Release emulator</source>
        <translation>Emulator freigeben</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4212"/>
        <source>WARNING: no matching window for emulator #%1 found</source>
        <translation>WARNUNG: es konnte kein Fenster für Emulator #%1 gefunden werden</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4218"/>
        <source>Embedding failed</source>
        <translation>Einbetten fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4243"/>
        <location filename="../../qmc2main.cpp" line="4244"/>
        <source>Scanning pause key</source>
        <translation>Scanne Pause-Taste</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5219"/>
        <source>saving current machine&apos;s favorite software</source>
        <translation>Speichere favorisierte Software der aktuellen Maschine</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5351"/>
        <source>destroying demo mode dialog</source>
        <translation>Zerstöre Dialog für Demo Modus</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5257"/>
        <source>disconnecting audio source from audio sink</source>
        <translation>Trenne Audio-Quelle von Audio-Ausgabe</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5843"/>
        <source>Game info - %p%</source>
        <translation>Spiel Info - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5845"/>
        <source>Machine info - %p%</source>
        <translation>Maschinen Info - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6021"/>
        <source>Emu info - %p%</source>
        <translation>Emu Info - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6395"/>
        <source>Add URL</source>
        <translation>URL hinzufügen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6395"/>
        <source>Enter valid MP3 stream URL:</source>
        <translation>Gültige MP3 Stream URL eingeben:</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6696"/>
        <source>Buffering %p%</source>
        <translation>Pufferung %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8067"/>
        <source>Cabinet</source>
        <translation>Gehäuse</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8069"/>
        <source>Controller</source>
        <translation>Controller</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8075"/>
        <source>PCB</source>
        <translation>PCB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8071"/>
        <source>Flyer</source>
        <translation>Flyer</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8073"/>
        <source>Marquee</source>
        <translation>Marquee</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7846"/>
        <source>No cabinet art</source>
        <translation>Keine Gehäuse-Bilder</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7858"/>
        <source>Previews</source>
        <translation>Vorschaubilder</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7904"/>
        <location filename="../../qmc2main.cpp" line="8077"/>
        <source>preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7889"/>
        <source>No previews</source>
        <translation>Keine Vorschaubilder</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7911"/>
        <source>Titles</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7944"/>
        <location filename="../../qmc2main.cpp" line="8079"/>
        <source>title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7929"/>
        <source>No titles</source>
        <translation>Keine Titelbilder</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8113"/>
        <source>Choose file to store the icon</source>
        <translation>Datei zum Speichern des Icons auswählen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8119"/>
        <source>icon image for &apos;%1&apos; stored as &apos;%2&apos;</source>
        <translation>Icon für &apos;%1&apos; unter &apos;%2&apos; gespeichert</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8126"/>
        <source>FATAL: icon image for &apos;%1&apos; couldn&apos;t be stored as &apos;%2&apos;</source>
        <translation>FATAL: konnte Icon für &apos;%1&apos; nicht unter &apos;%2&apos; speichern</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Physical memory:</source>
        <translation>Physischer Speicher:</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Total: %1 MB</source>
        <translation>Gesamt: %1 MB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Free: %1 MB</source>
        <translation>Frei: %1 MB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Used: %1 MB</source>
        <translation>Benutzt: %1 MB</translation>
    </message>
</context>
<context>
    <name>Marquee</name>
    <message>
        <location filename="../../marquee.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>In Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="56"/>
        <location filename="../../marquee.cpp" line="57"/>
        <source>Game marquee image</source>
        <translation>Marquee Abbildung</translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="59"/>
        <location filename="../../marquee.cpp" line="60"/>
        <source>Machine marquee image</source>
        <translation>Marquee Abbildung</translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="68"/>
        <location filename="../../marquee.cpp" line="72"/>
        <source>FATAL: can&apos;t open marquee file, please check access permissions for %1</source>
        <translation>FATAL: kann Marquee-Datei nicht öffnen; bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Warte auf Daten...</translation>
    </message>
</context>
<context>
    <name>MawsQuickDownloadSetup</name>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="14"/>
        <source>MAWS quick download setup</source>
        <translation>MAWS Schnell-Download Einstellung</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="395"/>
        <source>Previews</source>
        <translation>Vorschaubilder</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="424"/>
        <source>Path to store preview images</source>
        <translation>Pfad zum Speichern von Vorschaubildern</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="437"/>
        <source>Browse path to store preview images</source>
        <translation>Pfad zum Speichern von Vorschaubildern auswählen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="32"/>
        <source>Flyers</source>
        <translation>Flyer</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="20"/>
        <source>Icons and cabinet art</source>
        <translation>Icons und Gehäuse Bilder</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="61"/>
        <source>Path to store flyer images</source>
        <translation>Pfad zum Speichern von Flyer-Bildern</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="74"/>
        <source>Browse path to store flyer images</source>
        <translation>Pfad zum Speichern von Flyer-Bildern auswählen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="91"/>
        <source>Cabinets</source>
        <translation>Gehäuse</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="120"/>
        <source>Path to store cabinet images</source>
        <translation>Pfad zum Speichern von Gehäusebildern</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="133"/>
        <source>Browse path to store cabinet images</source>
        <translation>Pfad zum Speichern von Gehäusebildern auswählen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="150"/>
        <source>Controllers</source>
        <translation>Controller</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="179"/>
        <source>Path to store controller images</source>
        <translation>Pfad zum Speichern von Controller-Bildern</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="192"/>
        <source>Browse path to store controller images</source>
        <translation>Pfad zum Speichern von Controller-Bildern auswählen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="209"/>
        <source>Marquees</source>
        <translation>Marquees</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="238"/>
        <source>Path to store marquee images</source>
        <translation>Pfad zum Speichern von Marquee-Bildern</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="251"/>
        <source>Browse path to store marquee images</source>
        <translation>Pfad zum Speichern von Marquee-Bildern auswählen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="383"/>
        <source>Previews and titles</source>
        <translation>Vorschau- und Titel-Bilder</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="498"/>
        <source>Titles</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="527"/>
        <source>Path to store title images</source>
        <translation>Pfad zum Speichern von Titel-Bildern</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="540"/>
        <source>Browse path to store title images</source>
        <translation>Pfad zum Speichern von Titel-Bildern auswählen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="268"/>
        <source>PCBs</source>
        <translation>PCBs</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="297"/>
        <source>Path to store PCB images</source>
        <translation>Pfad zum Speichern von PCB-Bildern</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="310"/>
        <source>Browse path to store PCB images</source>
        <translation>Pfad zum Speichern von PCB-Bildern auswählen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="612"/>
        <source>Apply MAWS quick download setup and close dialog</source>
        <translation>MAWS Schnell-Download Einstellung übernehmen und Dialog schließen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="615"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="626"/>
        <source>Cancel MAWS quick download setup and close dialog</source>
        <translation>MAWS Schnell-Download Einstellung abbrechen und Dialog schließen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="629"/>
        <source>&amp;Cancel</source>
        <translation>Abbre&amp;chen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="408"/>
        <source>Automatically download preview images</source>
        <translation>Vorschaubilder automatisch herunterladen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="48"/>
        <location filename="../../mawsqdlsetup.ui" line="107"/>
        <location filename="../../mawsqdlsetup.ui" line="166"/>
        <location filename="../../mawsqdlsetup.ui" line="225"/>
        <location filename="../../mawsqdlsetup.ui" line="284"/>
        <location filename="../../mawsqdlsetup.ui" line="330"/>
        <location filename="../../mawsqdlsetup.ui" line="411"/>
        <location filename="../../mawsqdlsetup.ui" line="514"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="45"/>
        <source>Automatically download flyer images</source>
        <translation>Flyer-Bilder automatisch herunterladen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="104"/>
        <source>Automatically download cabinet images</source>
        <translation>Gehäuse-Bilder automatisch herunterladen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="163"/>
        <source>Automatically download controller images</source>
        <translation>Controller-Bilder automatisch herunterladen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="222"/>
        <source>Automatically download marquee images</source>
        <translation>Marquee-Bilder automatisch herunterladen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="511"/>
        <source>Automatically download title images</source>
        <translation>Titel-Bilder automatisch herunterladen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="281"/>
        <source>Automatically download PCB images</source>
        <translation>PCB-Bilder automatisch herunterladen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="373"/>
        <source>Icons</source>
        <translation>Icons</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="327"/>
        <source>Automatically download icon images</source>
        <translation>Icons automatisch herunterladen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="450"/>
        <location filename="../../mawsqdlsetup.ui" line="553"/>
        <source>Preferred collection</source>
        <translation>Bevorzugte Kollektion</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="463"/>
        <source>Select the preferred image collection for in-game previews (auto-download)</source>
        <translation>Bevorzugte Kollektion für Vorschaubilder auswählen (automatischer Download)</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="467"/>
        <location filename="../../mawsqdlsetup.ui" line="570"/>
        <location filename="../../mawsqdlsetup.cpp" line="97"/>
        <location filename="../../mawsqdlsetup.cpp" line="102"/>
        <source>AntoPISA progettoSNAPS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="472"/>
        <source>MAME World Snap Collection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="477"/>
        <location filename="../../mawsqdlsetup.ui" line="575"/>
        <source>CrashTest Snap Collection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="482"/>
        <source>Enaitz Jar Snaps</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="566"/>
        <source>Select the preferred image collection for titles (auto-download)</source>
        <translation>Bevorzugte Kollektion für Titel-Bilder auswählen (automatischer Download)</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="343"/>
        <source>Path to store icon images</source>
        <translation>Pfad zum Speichern von Icons</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="356"/>
        <source>Browse path to store icon images</source>
        <translation>Pfad zum Speichern von Icons auswählen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="113"/>
        <source>Choose icon directory</source>
        <translation>Icon Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="126"/>
        <source>Choose flyer directory</source>
        <translation>Flyer Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="139"/>
        <source>Choose cabinet directory</source>
        <translation>Gehäuse-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="152"/>
        <source>Choose controller directory</source>
        <translation>Controller-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="165"/>
        <source>Choose marquee directory</source>
        <translation>Marquee-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="178"/>
        <source>Choose PCB directory</source>
        <translation>PCB Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="191"/>
        <source>Choose preview directory</source>
        <translation>Vorschau-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="204"/>
        <source>Choose title directory</source>
        <translation>Titel-Verzeichnis auswählen</translation>
    </message>
</context>
<context>
    <name>MiniWebBrowser</name>
    <message>
        <location filename="../../miniwebbrowser.ui" line="15"/>
        <source>Mini Web Browser</source>
        <translation>Mini Web Browser</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="33"/>
        <location filename="../../miniwebbrowser.ui" line="36"/>
        <location filename="../../miniwebbrowser.cpp" line="96"/>
        <source>Go back</source>
        <translation>Gehe zurück</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="56"/>
        <location filename="../../miniwebbrowser.ui" line="59"/>
        <location filename="../../miniwebbrowser.cpp" line="98"/>
        <source>Go forward</source>
        <translation>Gehe vorwärts</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="79"/>
        <location filename="../../miniwebbrowser.ui" line="82"/>
        <source>Reload current URL</source>
        <translation>Aktuelle URL neu laden</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="102"/>
        <location filename="../../miniwebbrowser.ui" line="105"/>
        <source>Stop loading of current URL</source>
        <translation>Laden der aktuellen URL anhalten</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="145"/>
        <location filename="../../miniwebbrowser.ui" line="148"/>
        <source>Enter current URL</source>
        <translation>Aktuelle URL eingeben</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="170"/>
        <location filename="../../miniwebbrowser.ui" line="173"/>
        <source>Load URL</source>
        <translation>URL laden</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="205"/>
        <location filename="../../miniwebbrowser.ui" line="208"/>
        <source>Current progress loading URL</source>
        <translation>Aktueller Fortschritt beim Laden der URL</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="125"/>
        <location filename="../../miniwebbrowser.ui" line="128"/>
        <source>Go home (first page)</source>
        <translation>Zur ersten Seite zurückkehren</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="86"/>
        <source>Open link</source>
        <translation>Link öffnen</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="88"/>
        <source>Save link as...</source>
        <translation>Link speichern unter...</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="90"/>
        <source>Copy link</source>
        <translation>Link kopieren</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="92"/>
        <source>Save image as...</source>
        <translation>Bild speichern unter...</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="94"/>
        <source>Copy image</source>
        <translation>Bild kopieren</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="100"/>
        <source>Reload</source>
        <translation>Neu laden</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="102"/>
        <source>Stop</source>
        <translation>Anhalten</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="104"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="107"/>
        <source>Inspect</source>
        <translation>Inspizieren</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="477"/>
        <source>WARNING: invalid network reply and/or network error</source>
        <translation>WARNUNG: ungültige Netzwerk Antwort und/oder Netzwerk Fehler</translation>
    </message>
</context>
<context>
    <name>Options</name>
    <message>
        <location filename="../../options.cpp" line="968"/>
        <source>View games by category (not filtered)</source>
        <translation>Spiele nach Kategorie anzeigen (ungefiltert)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="970"/>
        <source>View games by emulator version (not filtered)</source>
        <translation>Spiele nach Emulator Version anzeigen (ungefiltert)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="15"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="41"/>
        <source>&amp;GUI</source>
        <translation>&amp;GUI</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="110"/>
        <source>DE (German)</source>
        <translation>DE (Deutsch)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="130"/>
        <source>US (English)</source>
        <translation>US (Englisch)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="161"/>
        <source>Restore layout</source>
        <translation>Layout wiederherstellen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1136"/>
        <source>Browse preview directory</source>
        <translation>Vorschau-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1088"/>
        <location filename="../../options.cpp" line="1900"/>
        <location filename="../../options.cpp" line="1957"/>
        <location filename="../../options.cpp" line="2847"/>
        <source>Preview directory</source>
        <translation>Vorschau-Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="822"/>
        <source>Browse temporary work file</source>
        <translation>Temporäre Arbeitsdatei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="796"/>
        <source>Temporary file</source>
        <translation>Temporäre Arbeitsdatei</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="125"/>
        <source>PT (Portuguese)</source>
        <translation>PT (Portugiesisch)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="638"/>
        <source>Open the detail setup dialog</source>
        <translation>Detail-Einstellungen vornehmen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="641"/>
        <source>Detail setup...</source>
        <translation>Detail-Einstellungen...</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="651"/>
        <source>Show memory  usage</source>
        <translation>Speicherauslastung anzeigen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="668"/>
        <source>Exit this QMC2 variant when launching another?</source>
        <translation>Diese QMC2-Variante beenden, wenn eine andere gestartet wird?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="671"/>
        <source>Exit on variant launch</source>
        <translation>Beenden beim Start einer Variante</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="678"/>
        <source>Log font</source>
        <translation>Schriftart in Logs</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="691"/>
        <source>Font used in logs (= application font if empty)</source>
        <translation>Schriftart, die in Logs verwendet wird (= Anwendungs-Schriftart, falls leer)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="707"/>
        <source>Browse font used in logs</source>
        <translation>Schriftart in Logs auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1806"/>
        <source>Switch between specifying a PCB directory or a ZIP-compressed PCB file</source>
        <translation>Zwischen der Angabe eines PCB-Verzeichnisses oder einer ZIP-komprimierten PCB-Datei umschalten</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1844"/>
        <source>PCB directory (read)</source>
        <translation>PCB Verzeichnis (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1857"/>
        <source>Browse PCB directory</source>
        <translation>PCB Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1881"/>
        <source>ZIP-compressed PCB file (read)</source>
        <translation>ZIP-komprimierte PCB-Datei (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1894"/>
        <source>Browse ZIP-compressed PCB file</source>
        <translation>ZIP-komprimierte PCB-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1041"/>
        <source>Enable the use of catver.ini -- get the newest version from http://www.progettoemma.net/?catlist</source>
        <translation>Verwendung von catver.ini aktivieren -- die neueste Version erhälst Du unter http://www.progettoemma.net/?catlist</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1044"/>
        <source>Use catver.ini</source>
        <translation>Benutze catver.ini</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1061"/>
        <source>Path to catver.ini (read)</source>
        <translation>Pfad zu catver.ini (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1074"/>
        <source>Browse path to catver.ini</source>
        <translation>Pfad zu catver.ini auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2357"/>
        <source>ROM types</source>
        <translation>ROM Typen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2646"/>
        <source>Hide primary game list while loading (recommended, much faster)</source>
        <translation>Primäre Spieleliste während des Ladens verbergen (empfohlen, sehr viel schneller)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2649"/>
        <source>Hide while loading</source>
        <translation>Beim Laden verbergen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2630"/>
        <source>Launch emulation directly when an item is activated in the search-, favorites- or played-lists (instead of jumping to the master list)</source>
        <translation>Emulation direkt starten, wenn ein Eintrag in einer der Unterlisten aktiviert wird (anstatt in die Hauptliste zu springen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2123"/>
        <source>Switch between specifying a software snap directory or a ZIP-compressed software snap file</source>
        <translation>Zwischen der Angabe eines Software Snap Verzeichnisses oder einer ZIP-komprimierten Software Snap Datei umschalten</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2126"/>
        <location filename="../../options.ui" line="2139"/>
        <location filename="../../options.cpp" line="1940"/>
        <location filename="../../options.cpp" line="1997"/>
        <location filename="../../options.cpp" line="2935"/>
        <source>SW snap folder</source>
        <translation>SW Snap Ordner</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1931"/>
        <source>Software snap-shot directory (read)</source>
        <translation>Software Snapshot Verzeichnis (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1944"/>
        <source>Browse software snap-shot directory</source>
        <translation>Software Snapshot Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1968"/>
        <source>ZIP-compressed software snap-shot file (read)</source>
        <translation>ZIP-komprimierte Software Snapshot Datei (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1981"/>
        <source>Browse ZIP-compressed software snap-shot file</source>
        <translation>ZIP-komprimierte Software Snapshot Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2362"/>
        <source>Players</source>
        <translation>Spieler</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2633"/>
        <source>Play on sub-list activation</source>
        <translation>Bei Unterlisten-Aktivierung starten</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2578"/>
        <source>Select the cursor position QMC2 uses when auto-scrolling to the current item (this setting applies to all views and lists!)</source>
        <translation>Auswahl der Cursor-Position, die QMC2 verwendet, wenn es automatisch zum aktuellen Eintrag scrollt (diese Einstellung gilt für alle Ansichten und Listen!)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2585"/>
        <source>Visible</source>
        <translation>Sichtbar</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2590"/>
        <source>Top</source>
        <translation>Oben</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2595"/>
        <source>Bottom</source>
        <translation>Unten</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2600"/>
        <source>Center</source>
        <translation>Zentrum</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2565"/>
        <source>Cursor position</source>
        <translation>Cursor Position</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="148"/>
        <source>Save game selection on exit and before reloading the game list</source>
        <translation>Ausgewähltes Spiel beim Beenden und vor dem Neuladen der Spieleliste speichern</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="168"/>
        <source>Restore saved game selection at start and after reloading the game list</source>
        <translation>Gespeicherte Spielauswahl beim Start und nach dem Neuladen der Spieleliste wieder herstellen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="748"/>
        <source>Use a unifed tool- and title-bar on Mac OS X</source>
        <translation>Gemeinsame Werkzeug- und Titel-Leiste unter Mac OS X verwenden</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="751"/>
        <source>Unify with title</source>
        <translation>Gemeinam mit Titel</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2367"/>
        <source>Driver status</source>
        <translation>Treiberstatus</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2515"/>
        <source>Display ROM status icons in master lists?</source>
        <translation>ROM Status Icons in den Hauptlisten anzeigen?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2518"/>
        <source>Show ROM status icons</source>
        <translation>ROM Status Icons anzeigen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2685"/>
        <source>Select the position where sofware snap-shots are displayed within software lists</source>
        <translation>Position zur Anzeige der Software-Snapshots innerhalb der Software-Liste auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2719"/>
        <source>Disable snaps</source>
        <translation>Keine Snaps</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2727"/>
        <source>Display software snap-shots when hovering the software list with the mouse cursor</source>
        <translation>Software-Snapshots anzeigen, wenn mit dem Maus-Zeiger darüber gefahren wird</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2730"/>
        <source>SW snaps on mouse hover</source>
        <translation>SW Snaps mit Maus-Zeiger</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3314"/>
        <source>Proxy / &amp;Tools</source>
        <translation>Proxy / &amp;Werkzeuge</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3488"/>
        <source>ROM tool</source>
        <translation>ROM Werkzeug</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3507"/>
        <source>External ROM tool (it&apos;s completely up to you...)</source>
        <translation>Externes ROM Werktzeug (es steht Dir völlig frei...)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3520"/>
        <source>Browse ROM tool</source>
        <translation>ROM Werkzeug auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3544"/>
        <source>ROM tool argument list (i. e. &quot;$ID$ $DESCRIPTION$&quot;)</source>
        <translation>Argumentliste für das ROM Werkzeug (z. B. &quot;$ID$ $DESCRIPTION$&quot;)</translation>
    </message>
    <message>
        <source>Working directory that&apos;s used when the ROM tool  is executed (if empty, QMC2&apos;s current working directory is used)</source>
        <translation type="obsolete">Arbeitsverzeichnis das zur Ausführung des ROM Werkzeugs verwendet wird (wenn leer, wird QMC2&apos;s aktuelles Arbeitsverzeichnis verwendet)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3577"/>
        <source>Browse working directory of the ROM tool</source>
        <translation>Arbeitsverzeichnis des ROM Werkzeugs auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3594"/>
        <source>Copy the tool&apos;s output to the front end log (keeping it for debugging)</source>
        <translation>Werkzeug Ausgaben auch im Frontend Log mitführen (zur Fehlersuche)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3597"/>
        <source>Copy tool output to front end log</source>
        <translation>Werkzeug Ausgaben in Frontend Log</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3604"/>
        <source>Automatically close the tool-executor dialog when the external process finished</source>
        <translation>Werzeug-Ausführungsdialog automatisch nach Beenden des externes Programmes schließen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3607"/>
        <source>Close dialog automatically</source>
        <translation>Dialog automatisch schließen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3752"/>
        <source>&amp;Global configuration</source>
        <translation>&amp;Globale Konfiguration</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3807"/>
        <source>Executable file</source>
        <translation>Ausführbare Datei</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3920"/>
        <source>XML game list cache</source>
        <translation>XML Spielelisten-Cache</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3957"/>
        <source>Game list cache</source>
        <translation>Spielelisten-Cache</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4120"/>
        <source>Software list cache</source>
        <translation>Softwarelisten-Cache</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4194"/>
        <source>MAME variant exe</source>
        <translation>MAME Variante</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4207"/>
        <source>Specify the exe file for the MAME variant (leave empty when all variants are installed in the same directory)</source>
        <translation>Exe-Datei für die MAME Variante angeben (leer lassen, wenn alle Varianten im selben Verzeichnis installiert wurden)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4220"/>
        <source>Browse MAME variant exe</source>
        <translation>Exe-Datei der MAME Variante auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4231"/>
        <source>MESS variant exe</source>
        <translation>MESS Variante</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4244"/>
        <source>Specify the exe file for the MESS variant (leave empty when all variants are installed in the same directory)</source>
        <translation>Exe-Datei für die MESS Variante angeben (leer lassen, wenn alle Varianten im selben Verzeichnis installiert wurden)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4257"/>
        <source>Browse MESS variant exe</source>
        <translation>Exe-Datei der MESS Variante auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3551"/>
        <location filename="../../options.ui" line="4268"/>
        <location filename="../../options.ui" line="4383"/>
        <location filename="../../options.ui" line="4528"/>
        <source>Working directory</source>
        <translation>Arbeitsverzeichnis</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4281"/>
        <location filename="../../options.ui" line="4541"/>
        <source>Working directory that&apos;s used when the emulator is executed (if empty, QMC2&apos;s current working directory is used)</source>
        <translation>Arbeitsverzeichnis das zur Ausführung des Emulators verwendet wird (wenn leer, wird QMC2&apos;s aktuelles Arbeitsverzeichnis verwendet)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4294"/>
        <location filename="../../options.ui" line="4554"/>
        <source>Browse working directory</source>
        <translation>Arbeitsverzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4157"/>
        <source>General software folder</source>
        <translation>Allg. Software-Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3564"/>
        <source>Working directory that&apos;s used when the ROM tool is executed (if empty, QMC2&apos;s current working directory is used)</source>
        <translation>Arbeitsverzeichnis das zur Ausführung des ROM Werkzeugs verwendet wird (wenn leer, wird QMC2&apos;s aktuelles Arbeitsverzeichnis verwendet)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4183"/>
        <source>Browse general software folder</source>
        <translation>Allgmeines Software-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4312"/>
        <source>Additional &amp;Emulators</source>
        <translation>Zusätzliche &amp;Emulatoren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4339"/>
        <source>Registered emulators -- you may select one of these in the game-specific emulator configuration</source>
        <translation>Registrierte Emulatoren -- diese lassen sich in den Spiel-spezifischen Emulator Einstellungen auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4373"/>
        <location filename="../../options.ui" line="4443"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4378"/>
        <location filename="../../options.ui" line="4457"/>
        <source>Executable</source>
        <translation>Ausführbare Datei</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4399"/>
        <source>Register emulator</source>
        <translation>Emulator registrieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4429"/>
        <source>Deregister emulator</source>
        <translation>Emulator deregistrieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4450"/>
        <source>Registered emulator&apos;s name</source>
        <translation>Name des registrierten Emulators</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4470"/>
        <source>Command to execute the emulator (path to the executable file)</source>
        <translation>Kommando zum Ausführen des Emulatzors (Pfad der ausführbaren Datei)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4504"/>
        <source>Arguments passed to the emulator -- use $ID$ as a placeholder for the unique game/machine ID (its short name)</source>
        <translation>Argumente, die an den Emulator übergeben werden -- verwende $ID$ als Platzhalter für die eindeutige ID des Spiels / der Maschine (Kurzname)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4514"/>
        <source>Replace emulator registration</source>
        <translation>Emulator Registrierung ersetzen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4582"/>
        <source>Apply settings</source>
        <translation>Einstellungen übernehmen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4585"/>
        <source>&amp;Apply</source>
        <translation>&amp;Anwenden</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4637"/>
        <source>Close and apply settings</source>
        <translation>Schließen und Einstellungen übernehmen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4640"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4647"/>
        <source>Close and discard changes</source>
        <translation>Schließen und Änderungen verwerfen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4650"/>
        <source>&amp;Cancel</source>
        <translation>Abbre&amp;chen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4596"/>
        <source>Restore currently applied settings</source>
        <translation>Aktuell gesicherte Einstellungen restaurieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4599"/>
        <source>&amp;Restore</source>
        <translation>&amp;Restaurieren</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="358"/>
        <source>WARNING: configuration is not writeable, please check access permissions for </source>
        <translation>WARNUNG: Konfiguration ist nicht beschreibbar, bitte überprüfe Zugriffsrechte für </translation>
    </message>
    <message>
        <location filename="../../options.ui" line="809"/>
        <source>Temporary work file (write)</source>
        <translation>Temporäre Arbeitsdatei (schreiben)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1123"/>
        <source>Preview directory (read)</source>
        <translation>Vorschau-Verzeichnis (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="970"/>
        <source>Browse frontend data directory</source>
        <translation>Vorschau-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="944"/>
        <source>Data directory</source>
        <translation>Daten-Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2283"/>
        <source>Choose temporary work file</source>
        <translation>Temporäre Arbeitsdatei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2283"/>
        <location filename="../../options.cpp" line="2407"/>
        <location filename="../../options.cpp" line="2419"/>
        <location filename="../../options.cpp" line="2432"/>
        <location filename="../../options.cpp" line="2444"/>
        <location filename="../../options.cpp" line="2495"/>
        <location filename="../../options.cpp" line="2507"/>
        <location filename="../../options.cpp" line="2519"/>
        <location filename="../../options.cpp" line="2531"/>
        <location filename="../../options.cpp" line="2543"/>
        <location filename="../../options.cpp" line="2567"/>
        <location filename="../../options.cpp" line="2579"/>
        <location filename="../../options.cpp" line="2591"/>
        <location filename="../../options.cpp" line="2605"/>
        <location filename="../../options.cpp" line="2646"/>
        <location filename="../../options.cpp" line="2672"/>
        <location filename="../../options.cpp" line="2698"/>
        <location filename="../../options.cpp" line="2710"/>
        <location filename="../../options.cpp" line="2723"/>
        <location filename="../../options.cpp" line="2944"/>
        <location filename="../../options.cpp" line="2956"/>
        <location filename="../../options.cpp" line="2968"/>
        <location filename="../../options.cpp" line="2980"/>
        <location filename="../../options.cpp" line="2992"/>
        <location filename="../../options.cpp" line="3004"/>
        <location filename="../../options.cpp" line="3016"/>
        <location filename="../../options.cpp" line="3028"/>
        <location filename="../../options.cpp" line="3054"/>
        <location filename="../../options.cpp" line="3266"/>
        <source>All files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="398"/>
        <source>Browse application font</source>
        <translation>Anwendungs-Schriftart auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="141"/>
        <source>Save layout</source>
        <translation>Layout speichern</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="382"/>
        <source>Application font (= system default if empty)</source>
        <translation>Anwendungs-Schriftart (= System-Standard, falls leer)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3909"/>
        <source>Browse options template file</source>
        <translation>Optionsvorlage-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3883"/>
        <source>Options template file</source>
        <translation>Optionsvorlage-Datei</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3896"/>
        <source>Options template file (read)</source>
        <translation>Optionsvorlage-Datei (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2295"/>
        <source>Choose preview directory</source>
        <translation>Vorschau-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2407"/>
        <source>Choose options template file</source>
        <translation>Optionsvorlage-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2684"/>
        <source>Choose data directory</source>
        <translation>Daten-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="171"/>
        <source>Restore game selection</source>
        <translation>Spiel-Selektion wiederherstellen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="151"/>
        <source>Save game selection</source>
        <translation>Spiel-Selektion speichern</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="158"/>
        <source>Restore saved window layout at start</source>
        <translation>Gespeichertes Fenster-Layout beim Start wiederherstellen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="138"/>
        <source>Save window layout at exit</source>
        <translation>Fenster-Layout beim Beenden sichern</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1206"/>
        <source>Confirm</source>
        <translation>Bestätigen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1212"/>
        <source>&amp;Local</source>
        <translation>&amp;Lokal</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1212"/>
        <source>&amp;Overwrite</source>
        <translation>&amp;Überschreiben</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1212"/>
        <source>Do&amp;n&apos;t apply</source>
        <translation>&amp;Nicht anwenden</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="178"/>
        <location filename="../../options.ui" line="188"/>
        <location filename="../../options.ui" line="198"/>
        <location filename="../../options.ui" line="208"/>
        <location filename="../../options.ui" line="356"/>
        <location filename="../../options.ui" line="366"/>
        <location filename="../../options.ui" line="422"/>
        <source>Scale image to fit frame size (otherwise use original size)</source>
        <translation>Bild automatisch an Rahmen-Größe anpassen (andernfalls Original-Größe benutzen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="181"/>
        <source>Scaled preview</source>
        <translation>Vorschaubilder skalieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="619"/>
        <source>Image cache size in MB</source>
        <translation>Größe des Bild-Zugriffsspeichers in MB</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="622"/>
        <source> MB</source>
        <translation> MB</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="106"/>
        <source>Application language</source>
        <translation>Anwendungs-Sprache</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="91"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="825"/>
        <source>image cache size set to %1 MB</source>
        <translation>Größe des Bild-Zugriffsspeichers auf %1 MB gesetzt</translation>
    </message>
    <message>
        <source>Restore saved game selection at start and after reloading the gamelist</source>
        <translation type="obsolete">Gespeicherte Spiel-Selektion beim Start und nach Aktualisierung der Spieleliste wiederherstellen</translation>
    </message>
    <message>
        <source>Save game selection at exit and before reloading the gamelist</source>
        <translation type="obsolete">Spiel-Selektion beim Beenden und vor Aktualisierung der Spieleliste sichern</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4020"/>
        <source>Browse ROM state cache file</source>
        <translation>ROM-Status Cache Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3994"/>
        <source>ROM state cache</source>
        <translation>ROM-Status Cache</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2605"/>
        <source>Choose ROM state cache file</source>
        <translation>ROM-Status Cache Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2429"/>
        <source>immediate</source>
        <translation>sofort</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2413"/>
        <source>Responsiveness</source>
        <translation>Änderungssensibilität</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="569"/>
        <source>Smooth scaling</source>
        <translation>Kantenglättung</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4007"/>
        <source>ROM state cache file (write)</source>
        <translation>ROM Status Cache Datei (schreiben)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="933"/>
        <source>Browse play history file</source>
        <translation>Spiel-Historien Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="896"/>
        <source>Browse game favorites file</source>
        <translation>Spiel-Favoriten Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="920"/>
        <source>Play history file (write)</source>
        <translation>Spiel-Historien Datei (schreiben)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="883"/>
        <source>Game favorites file (write)</source>
        <translation>Spiel-Favoriten Datei (schreiben)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="907"/>
        <source>Play history file</source>
        <translation>Spiel-Historien Datei</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="870"/>
        <source>Favorites file</source>
        <translation>Favoriten-Datei</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2567"/>
        <source>Choose game favorites file</source>
        <translation>Spiel-Favoriten Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2579"/>
        <source>Choose play history file</source>
        <translation>Spiel-Historien Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2375"/>
        <source>Sort order</source>
        <translation>Sortierreihenfolge</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2310"/>
        <source>Sort criteria</source>
        <translation>Sortier-Kriterium</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2388"/>
        <source>Select sort order</source>
        <translation>Sortierreihenfolge auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2392"/>
        <source>Ascending</source>
        <translation>Aufsteigend</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2401"/>
        <source>Descending</source>
        <translation>Absteigend</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2323"/>
        <source>Select sort criteria</source>
        <translation>Sortier-Kriterium auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2332"/>
        <source>ROM state</source>
        <translation>ROM-Status</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1438"/>
        <source>Waiting for data...</source>
        <translation>Warte auf Daten...</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1424"/>
        <location filename="../../options.cpp" line="1426"/>
        <source>ascending</source>
        <translation>aufsteigend</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="378"/>
        <location filename="../../options.cpp" line="385"/>
        <source>No style sheet</source>
        <translation>Kein Style Sheet</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1424"/>
        <location filename="../../options.cpp" line="1426"/>
        <source>descending</source>
        <translation>absteigend</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2327"/>
        <source>Game description</source>
        <translation>Spiel-Beschreibung</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="251"/>
        <source>Kill emulators on exit?</source>
        <translation>Emulatoren beim Beenden schließen?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="254"/>
        <source>Kill emulators</source>
        <translation>Emulatoren beenden</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2309"/>
        <source>Choose flyer directory</source>
        <translation>Flyer Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1239"/>
        <source>Browse flyer directory</source>
        <translation>Flyer Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1226"/>
        <source>Flyer directory (read)</source>
        <translation>Flyer Verzeichnis (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1191"/>
        <location filename="../../options.cpp" line="1905"/>
        <location filename="../../options.cpp" line="1962"/>
        <location filename="../../options.cpp" line="2858"/>
        <source>Flyer directory</source>
        <translation>Flyer Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="369"/>
        <source>Scaled flyer</source>
        <translation>Flyer-Bilder skalieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="566"/>
        <source>Smooth image scaling (nicer, but slower)</source>
        <translation>Sanfte Kantenglättung bei Bild-Skalierungen (schöner, aber langsamer)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3721"/>
        <source>&lt;font size=&quot;-1&quot;&gt;&lt;b&gt;WARNING: stored passwords are &lt;u&gt;weakly&lt;/u&gt; encrypted!&lt;/b&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&quot;-1&quot;&gt;&lt;b&gt;WARNUNG: gespeicherte Passwörter sind &lt;i&gt;mäßig&lt;/i&gt; verschlüsselt!&lt;/b&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4133"/>
        <source>Software list cache file (write)</source>
        <translation>Softwarelisten-Cache Datei (schreiben)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4146"/>
        <source>Browse software list cache file</source>
        <translation>Softwarelisten-Cache Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4170"/>
        <source>Directory used as the default software folder for the MESS device configurator (if a sub-folder named as the current machine exists, that folder will be selected instead)</source>
        <translation>Standard Software-Verzeichnis, das bei der MESS Geräte-Konfiguration verwendet wird (sofern dort ein Unterverzeichnis mit dem Namen der aktuell ausgewählten Maschine existiert, wird dieses stattdessen verwendet)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4610"/>
        <source>Reset to default settings (click &lt;i&gt;Restore&lt;/i&gt; to restore currently applied settings!)</source>
        <translation>Auf Standard-Einstellungen zurücksetzen (klicke auf &lt;i&gt;Restaurieren&lt;/i&gt; um die gespeicherte Konfiguration wiederherzustellen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4613"/>
        <source>&amp;Default</source>
        <translation>Stan&amp;dard</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2837"/>
        <location filename="../../options.cpp" line="386"/>
        <location filename="../../options.cpp" line="387"/>
        <location filename="../../options.cpp" line="3392"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2184"/>
        <source>ROM state filter</source>
        <translation>ROM Status Filter</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2199"/>
        <source>Show ROM state C (correct)?</source>
        <translation>Zeige ROM Status K (korrekt)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2222"/>
        <source>Show ROM state M (mostly correct)?</source>
        <translation>Zeige ROM Status B (beinahe korrekt)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2245"/>
        <source>Show ROM state I (incorrect)?</source>
        <translation>Zeige ROM Status I (inkorrekt)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2268"/>
        <source>Show ROM state N (not found)?</source>
        <translation>Zeige ROM Status N (nicht gefunden)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2291"/>
        <source>Show ROM state U (unknown)?</source>
        <translation>Zeige ROM Status U (unbekannt)?</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1900"/>
        <location filename="../../options.cpp" line="1957"/>
        <location filename="../../options.cpp" line="2847"/>
        <source>Preview file</source>
        <translation>Vorschau Datei</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1905"/>
        <location filename="../../options.cpp" line="1962"/>
        <location filename="../../options.cpp" line="2858"/>
        <source>Flyer file</source>
        <translation>Flyer Datei</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2944"/>
        <source>Choose ZIP-compressed preview file</source>
        <translation>ZIP-komprimierte Vorschau Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2956"/>
        <source>Choose ZIP-compressed flyer file</source>
        <translation>ZIP-komprimierte Flyer Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1188"/>
        <source>Switch between specifying a flyer directory or a ZIP-compressed flyer file</source>
        <translation>Zwischen der Angabe eines Flyer-Verzeichnisses oder einer ZIP-komprimierten Flyer-Datei umschalten</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1085"/>
        <source>Switch between specifying a preview directory or a ZIP-compressed preview file</source>
        <translation>Zwischen der Angabe eines Vorschau-Verzeichnisses oder einer ZIP-komprimierten Vorschau-Datei umschalten</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1160"/>
        <source>ZIP-compressed preview file (read)</source>
        <translation>ZIP-komprimierte Vorschau-Datei (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1173"/>
        <source>Browse ZIP-compressed preview file</source>
        <translation>ZIP-komprimierte Vorschau-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1263"/>
        <source>ZIP-compressed flyer file (read)</source>
        <translation>ZIP-komprimierte Flyer-Datei (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1276"/>
        <source>Browse ZIP-compressed flyer file</source>
        <translation>ZIP-komprimierte Flyer-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1609"/>
        <location filename="../../options.cpp" line="1613"/>
        <source>FATAL: can&apos;t open preview file, please check access permissions for %1</source>
        <translation>FATAL: kann Vorschau-Datei nicht öffnen; bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1627"/>
        <location filename="../../options.cpp" line="1631"/>
        <source>FATAL: can&apos;t open flyer file, please check access permissions for %1</source>
        <translation>FATAL: kann Flyer-Datei nicht öffnen; bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="957"/>
        <source>Frontend data directory (read)</source>
        <translation>Frontend Daten-Verzeichnis (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1752"/>
        <location filename="../../options.cpp" line="1756"/>
        <source>FATAL: can&apos;t open icon file, please check access permissions for %1</source>
        <translation>FATAL: kann Icon-Archiv nicht öffnen, bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1910"/>
        <location filename="../../options.cpp" line="1967"/>
        <location filename="../../options.cpp" line="2869"/>
        <source>Icon file</source>
        <translation>Icon Datei</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1294"/>
        <location filename="../../options.cpp" line="1910"/>
        <location filename="../../options.cpp" line="1967"/>
        <location filename="../../options.cpp" line="2869"/>
        <source>Icon directory</source>
        <translation>Icon Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2323"/>
        <source>Choose icon directory</source>
        <translation>Icon Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2968"/>
        <source>Choose ZIP-compressed icon file</source>
        <translation>ZIP-komprimierte Icon-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1291"/>
        <source>Switch between specifying an icon directory or a ZIP-compressed icon file</source>
        <translation>Zwischen der Angabe eines Icon-Verzeichnisses oder einer ZIP-komprimierten Icon-Datei umschalten</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1329"/>
        <source>Icon directory (read)</source>
        <translation>Icon Verzeichnis (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1342"/>
        <source>Browse icon directory</source>
        <translation>Icon Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1366"/>
        <source>ZIP-compressed icon file (read)</source>
        <translation>ZIP-komprimierte Icon-Datei (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1379"/>
        <source>Browse ZIP-compressed icon file</source>
        <translation>ZIP-komprimierte Icon-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2342"/>
        <source>Year</source>
        <translation>Jahr</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2347"/>
        <source>Manufacturer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2591"/>
        <source>Choose gamelist cache file</source>
        <translation>Cache Datei für Spieleliste auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3983"/>
        <source>Browse gamelist cache file</source>
        <translation>Cache Datei für Spieleliste auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3970"/>
        <source>Gamelist cache file (write)</source>
        <translation>Cache Datei für Spieleliste (schreiben)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="576"/>
        <source>Retry loading images which weren&apos;t found before?</source>
        <translation>Bilder, die zuvor nicht gefunden wurden, erneut laden?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="579"/>
        <source>Retry loading images</source>
        <translation>Bild laden wiederholen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="546"/>
        <source>GUI style</source>
        <translation>GUI Style</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="502"/>
        <source>Application font</source>
        <translation>Anwendungs-Schriftart</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="606"/>
        <source>Image cache size</source>
        <translation>Bild-Cache Größe</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="658"/>
        <source>Use standard or custom color palette?</source>
        <translation>Standard oder angepasste Farbpalette benutzen?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="661"/>
        <source>Standard color palette</source>
        <translation>Standard Farbpalette</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="767"/>
        <location filename="../../options.ui" line="3757"/>
        <source>F&amp;iles / Directories</source>
        <translation>Date&amp;ien / Verzeichnisse</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2744"/>
        <source>&amp;Shortcuts / Keys</source>
        <translation>Ta&amp;staturkürzel</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="266"/>
        <source>Check all ROM states</source>
        <translation>Alle ROM Stati prüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="267"/>
        <source>Check all sample sets</source>
        <translation>Alle Sample-Sets prüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="268"/>
        <source>Check preview images</source>
        <translation>Vorschaubilder überprüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="269"/>
        <source>Check flyer images</source>
        <translation>Flyer-Bilder überprüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="270"/>
        <source>Check icon images</source>
        <translation>Icons überprüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="271"/>
        <source>About QMC2</source>
        <translation>Über QMC2</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="273"/>
        <source>Analyze tagged sets</source>
        <translation>Markierte Sets analysieren</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="275"/>
        <source>Copy game to favorites</source>
        <translation>Nach Favoriten kopieren</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="276"/>
        <source>Copy tagged sets to favorites</source>
        <translation>Markierte Sets den Favoriten hinzufügen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="277"/>
        <source>Online documentation</source>
        <translation>Online Dokumentation</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="278"/>
        <source>Clear image cache</source>
        <translation>Bild-Zugriffsspeicher leeren</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="282"/>
        <source>Open options dialog</source>
        <translation>Options-Dialog öffnen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="287"/>
        <source>About Qt</source>
        <translation>Über Qt</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="289"/>
        <source>Check game&apos;s ROM state</source>
        <translation>ROM Status des aktuellen Spieles prüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="291"/>
        <source>Recreate template map</source>
        <translation>Abbildungsvorlage neu erzeugen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="293"/>
        <source>Stop processing / exit QMC2</source>
        <translation>Verarbeitung anhalten / QMC2 beenden</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="345"/>
        <source>Cursor down</source>
        <translation>Cursor runter</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="318"/>
        <source>Gamelist with full detail</source>
        <translation>Spieleliste mit vollen Details</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="348"/>
        <source>Cursor left</source>
        <translation>Cursor links</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="344"/>
        <source>Minus (-)</source>
        <translation>Minus (-)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="350"/>
        <source>Page down</source>
        <translation>Bild abwärts</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="351"/>
        <source>Page up</source>
        <translation>Bild aufwärts</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="343"/>
        <source>Plus (+)</source>
        <translation>Plus (+)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="353"/>
        <source>Cursor right</source>
        <translation>Cursor rechts</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="354"/>
        <source>Tabulator</source>
        <translation>Tabulator</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="355"/>
        <source>Cursor up</source>
        <translation>Cursor rauf</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2842"/>
        <source>Custom</source>
        <translation>Angepasst</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="352"/>
        <source>Enter key</source>
        <translation>Eingabetaste</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2793"/>
        <source>Reset key sequence to default</source>
        <translation>Tastaturkürzel auf Standard zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2776"/>
        <location filename="../../options.ui" line="2779"/>
        <source>Redefine key sequence</source>
        <translation>Tastaturkürzel neu festlegen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2813"/>
        <source>Active shortcut definitions; double-click to redefine key sequence</source>
        <translation>Aktive Tastaturkürzel; Doppel-Klick zur Festlegung eines neuen Tastaturkürzels</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="281"/>
        <source>Clear icon cache</source>
        <translation>Icon-Zugriffsspeicher leeren</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="288"/>
        <source>Reload gamelist</source>
        <translation>Spieleliste neu laden</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2617"/>
        <source>Auto-trigger ROM check</source>
        <translation>Automatische ROM-Prüfung</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2445"/>
        <source>Update delay</source>
        <translation>Verzögerung</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2464"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3238"/>
        <source>WARNING: shortcut map contains duplicates</source>
        <translation>WARNUNG: Tastaturkürzel-Definition enthält Duplikate</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3236"/>
        <source>shortcut map is clean</source>
        <translation>Tastaturkürzel-Definition ist in Ordnung</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2796"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3933"/>
        <source>Cache file for the output of mame -listxml / -lx (write)</source>
        <translation>Cache Datei für die Ausgabe von mame -listxml / -lx (schreiben)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2507"/>
        <source>Choose XML gamelist cache file</source>
        <translation>XML Spieldaten Cache Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3946"/>
        <source>Browse XML gamelist cache file</source>
        <translation>XML Spieldaten Cache Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="346"/>
        <source>End</source>
        <translation>Ende</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="347"/>
        <source>Escape</source>
        <translation>Escape</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="349"/>
        <source>Home</source>
        <translation>Pos1</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3343"/>
        <source>Zip tool</source>
        <translation>Zip Werkzeug</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3375"/>
        <source>Browse for zip tool</source>
        <translation>Zip Werkzeug auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2519"/>
        <source>Choose zip tool</source>
        <translation>Zip Werkzeug auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2531"/>
        <source>Choose file removal tool</source>
        <translation>Datei-Entfernungs Werkzeug auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3362"/>
        <source>External zip tool, i.e. &quot;zip&quot; (read and execute)</source>
        <translation>Externes Zip Werkzeug, z. B. &quot;zip&quot; (lesen und ausführen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3399"/>
        <source>Zip tool argument list to remove entries from the ZIP archive (i. e. &quot;$ARCHIVE$ -d $FILELIST$&quot;)</source>
        <translation>Argumentliste für Zip Werkzeug zum Löschen von Einträgen aus dem ZIP Archiv (z. B. &quot;$ARCHIVE$ -d $FILELIST$&quot;)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3409"/>
        <source>File removal tool</source>
        <translation>Datei-Enterfernungs Werkzeug</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3465"/>
        <source>File removal tool argument list (i. e. &quot;-f -v $FILELIST$&quot;)</source>
        <translation>Argumentliste für Datei-Entfernungs Werkzeug (z. B. &quot;-f -v $FILELIST$&quot;)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3428"/>
        <source>External file removal tool, i.e. &quot;rm&quot; (read and execute)</source>
        <translation>Externes Datei-Entfernungs Werkzeug, z. B. &quot;rm&quot; (lesen und ausführen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3386"/>
        <location filename="../../options.ui" line="3452"/>
        <location filename="../../options.ui" line="3531"/>
        <location filename="../../options.ui" line="4388"/>
        <location filename="../../options.ui" line="4497"/>
        <source>Arguments</source>
        <translation>Argumente</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3441"/>
        <source>Browse for file removal tool</source>
        <translation>Datei-Entfernungs Werkzeug auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="241"/>
        <source>Show short description of current processing in progress bar</source>
        <translation>Anzeige einer Kurz-Beschreibung der aktuellen Verarbeitung in Fortschritts-Anzeige</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2419"/>
        <location filename="../../options.cpp" line="3266"/>
        <source>Choose emulator executable file</source>
        <translation>Ausführbare Emulator Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2495"/>
        <source>Choose emulator log file</source>
        <translation>Emulator Log Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3742"/>
        <source>E&amp;mulator</source>
        <translation>E&amp;mulator</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3872"/>
        <source>Browse emulator log file</source>
        <translation>Emulator Log Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3859"/>
        <source>Emulator log file (write)</source>
        <translation>Emulator Log Datei (scheiben)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3846"/>
        <source>Emulator log file</source>
        <translation>Emulator Log Datei</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3835"/>
        <location filename="../../options.ui" line="4483"/>
        <source>Browse emulator executable file</source>
        <translation>Ausführbare Emulator Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3822"/>
        <source>Emulator executable file (read and execute)</source>
        <translation>Ausführbare Emulator Datei (lesen und ausführen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="120"/>
        <source>PL (Polish)</source>
        <translation>PL (Polnisch)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="272"/>
        <source>Analyze current game</source>
        <translation>Aktuelles Spiel analysieren</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="297"/>
        <source>Open ROMAlyzer dialog</source>
        <translation>ROMAlyzer Dialog öffnen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="115"/>
        <source>FR (French)</source>
        <translation>FR (Französisch)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2337"/>
        <source>Tag</source>
        <translation>Markierung</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2352"/>
        <source>Game name</source>
        <translation>Spielname</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2672"/>
        <source>SW snap position</source>
        <translation>SW Snap Position</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2689"/>
        <source>Above / Left</source>
        <translation>Drüber / Links</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2694"/>
        <source>Above / Center</source>
        <translation>Drüber / Zentrum</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2699"/>
        <source>Above / Right</source>
        <translation>Drüber / Rechts</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2704"/>
        <source>Below / Left</source>
        <translation>Drunter / Links</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2709"/>
        <source>Below / Center</source>
        <translation>Drunter / Zentrum</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2714"/>
        <source>Below / Right</source>
        <translation>Drunter / Rechts</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2857"/>
        <source>&amp;Joystick</source>
        <translation>&amp;Joystick</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2886"/>
        <source>Enable GUI control via joystick</source>
        <translation>GUI Steuerung via Joystick aktivieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2889"/>
        <source>Enable joystick control</source>
        <translation>Joystick-Steuerung aktivieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2905"/>
        <source>Rescan available joysticks</source>
        <translation>Verfügbare Joysticks suchen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2908"/>
        <source>Rescan joysticks</source>
        <translation>Joysticks suchen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2928"/>
        <source>Select joystick</source>
        <translation>Joystick auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2938"/>
        <source>List of available joysticks - select the one you want to use for GUI control</source>
        <translation>Liste der verfügbaren Joysticks - wähle denjenigen aus, den Du für die GUI Steuerung verwenden möchtest</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2942"/>
        <location filename="../../options.cpp" line="3419"/>
        <location filename="../../options.cpp" line="3442"/>
        <location filename="../../options.cpp" line="3495"/>
        <location filename="../../options.cpp" line="3582"/>
        <source>No joysticks found</source>
        <translation>Keine Joysticks gefunden</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2953"/>
        <source>Joystick information and settings</source>
        <translation>Joystick Informationen und Einstellungen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2970"/>
        <source>Number of joystick axes</source>
        <translation>Anzahl der Joystick Achsen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2973"/>
        <location filename="../../options.ui" line="2994"/>
        <location filename="../../options.ui" line="3015"/>
        <location filename="../../options.ui" line="3036"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2984"/>
        <source>Buttons:</source>
        <translation>Knöpfe:</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2991"/>
        <source>Number of joystick buttons</source>
        <translation>Anzahl der Joystick Knöpfe</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3005"/>
        <source>Hats:</source>
        <translation>Köpfe:</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3012"/>
        <source>Number of coolie hats</source>
        <translation>Anzahl der Coolie-Hats</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3026"/>
        <source>Trackballs:</source>
        <translation>Trackballs:</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3033"/>
        <source>Number of trackballs</source>
        <translation>Anzahl der Trackballs</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3152"/>
        <source>Calibrate joystick axes</source>
        <translation>Joystick Achsen kalibrieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3174"/>
        <source>Test all joystick functions</source>
        <translation>Alle Joystick Funktionen testen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2963"/>
        <source>Axes:</source>
        <translation>Achsen:</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3062"/>
        <source>Automatically repeat joystick functions after specified delay</source>
        <translation>Joystick Funktionen nach angegebener Zeitspanne automatisch wiederholen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3065"/>
        <source>Auto repeat after</source>
        <translation>Wiederholung nach</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3081"/>
        <source>Repeat all joystick functions after how many milliseconds?</source>
        <translation>Joystick Funktionen nach wie vielen Millisekunden wiederholen?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3084"/>
        <location filename="../../options.ui" line="3129"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3110"/>
        <source>Event timeout</source>
        <translation>Verzögerung</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3126"/>
        <source>Process joystick events after how many milliseconds?</source>
        <translation>Joystick Ereignisse nach wie vielen Millisekunden verarbeiten?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2832"/>
        <location filename="../../options.ui" line="3288"/>
        <source>Function / Key</source>
        <translation>Funktion / Taste</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3155"/>
        <source>Calibrate</source>
        <translation>Kalibrieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3177"/>
        <source>Test</source>
        <translation>Testen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3199"/>
        <source>Map</source>
        <translation>Abbilden</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3235"/>
        <source>Remap</source>
        <translation>Zuweisen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3252"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3269"/>
        <source>Active joystick mappings; double-click to remap joystick function</source>
        <translation>Aktive Joystick-Zuweisungen; doppelt klicken um neue Zuweisung festzulegen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3293"/>
        <source>Joystick function</source>
        <translation>Joystick Funktion</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3196"/>
        <source>Map joystick functions to GUI functions</source>
        <translation>Joystick Zuweisungen für GUI Funktionen festlegen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3232"/>
        <source>Remap a joystick function to the selected GUI function</source>
        <translation>Joystick Funktion für ausgewählte GUI Funtion neu zuweisen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3249"/>
        <source>Remove joystick mapping from selected GUI function</source>
        <translation>Joystick Zuweisung von ausgewählter GUI Funktion entfernen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1141"/>
        <source>WARNING: can&apos;t initialize joystick</source>
        <translation>WARNUNG: kann Joystick nicht initialisieren</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1940"/>
        <location filename="../../options.cpp" line="1997"/>
        <location filename="../../options.cpp" line="2935"/>
        <source>SW snap file</source>
        <translation>SW Snap Datei</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3016"/>
        <source>Choose ZIP-compressed title file</source>
        <translation>ZIP-komprimierte Titel-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3028"/>
        <source>Choose ZIP-compressed PCB file</source>
        <translation>ZIP-komprimierte PCB-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3040"/>
        <source>Choose software snap directory</source>
        <translation>Software Snapshot Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3054"/>
        <source>Choose ZIP-compressed software snap file</source>
        <translation>ZIP-komprimierte Software Snapshot Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3755"/>
        <source>joystick map is clean</source>
        <translation>Joystick-Abbilding ist in Ordnung</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3757"/>
        <source>WARNING: joystick map contains duplicates</source>
        <translation>WARNUNG: Joystick-Abbildung enthält Duplikate</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2698"/>
        <source>Choose game info DB</source>
        <translation>Spiel Info DB auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2050"/>
        <source>Game info DB</source>
        <translation>Spiel Info DB</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="987"/>
        <source>Game information database - MAME history.dat (read)</source>
        <translation>Spiel Informations Datenbank - MAME history.dat (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1000"/>
        <source>Browse game information database (MAME history.dat)</source>
        <translation>Spiel Informations Datenbank auswählen (MAME history.dat)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2067"/>
        <source>Use in-memory compression for game info DB (a bit slower, but consumes distinctly less memory; compression rate is usually about 1:16)</source>
        <translation>Daten-Kompression im Hauptspeicher für Spiel-Info DB aktivieren (ein wenig langsamer, aber reduziert den Speicherbedarf deutlich; die Kompressionsrate liegt üblicherweise bei etwa 1:16)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1357"/>
        <source>please restart QMC2 for some changes to take effect</source>
        <translation>Bitte QMC2 neu starten um einige der Änderungen wirksam werden zu lassen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="328"/>
        <location filename="../../options.ui" line="344"/>
        <source>Option requires a restart of QMC2 to take effect</source>
        <translation>Option erfordert einen Neustart von QMC2 um wirksam zu werden</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2017"/>
        <location filename="../../options.ui" line="2537"/>
        <location filename="../../options.ui" line="4052"/>
        <source>Option requires a reload of the gamelist to take effect</source>
        <translation>Option erfordert eine Aktualisierung der Spieleliste um wirksam zu werden</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1315"/>
        <source>invalidating game info DB</source>
        <translation>Invalidiere Spiel-Info DB</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2047"/>
        <source>Load game information database (MAME history.dat)</source>
        <translation>Spiel Informations Datenbank laden (MAME history.dat)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="347"/>
        <source>restart required</source>
        <translation>Neustart erforderlich</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2036"/>
        <location filename="../../options.ui" line="2556"/>
        <location filename="../../options.ui" line="4071"/>
        <source>reload required</source>
        <translation>Neu laden erforderlich</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2458"/>
        <source>Delay update of any game details (preview, flyer, info, configuration, ...) by how many milliseconds?</source>
        <translation>Aktualisierung der Spiel-Details (Vorschau, Flyer, Info, Konfiguration, ...) um wie viele Millisekunden verzögern?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2461"/>
        <source>none</source>
        <translation>keine</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1334"/>
        <source>invalidating emulator info DB</source>
        <translation>Invalidiere Emulator-Info DB</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2710"/>
        <source>Choose emulator info DB</source>
        <translation>Emulator Info DB auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2085"/>
        <source>Load emulator information database (mameinfo.dat)</source>
        <translation>Emulator Informations Datenbank laden (mameinfo.dat)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2105"/>
        <source>Use in-memory compression for emulator info DB (a bit slower, but consumes distinctly less memory; compression rate is usually about 1:16)</source>
        <translation>Daten-Kompression im Hauptspeicher für Emulator-Info DB aktivieren (ein wenig langsamer, aber reduziert den Speicherbedarf deutlich; die Kompressionsrate liegt üblicherweise bei etwa 1:16)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2088"/>
        <source>Emu info DB</source>
        <translation>Emu Info DB</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1017"/>
        <source>Emulator information database - mameinfo.dat (read)</source>
        <translation>Emulator Informations Datenbank - mameinfo.dat (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1030"/>
        <source>Browse emulator information database (mameinfo.dat)</source>
        <translation>Emulator Informations Datenbank auswählen (mameinfo.dat)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="448"/>
        <location filename="../../options.ui" line="483"/>
        <source>unlimited</source>
        <translation>unbegrenzt</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="451"/>
        <location filename="../../options.ui" line="486"/>
        <source> lines</source>
        <translation> Zeilen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="432"/>
        <source>Emulator log size</source>
        <translation>Emulator Log Größe</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="445"/>
        <source>Maximum number of lines to keep in emulator log browser</source>
        <translation>Maximale Anzahl von Zeilen, die im Emulator Log Browser gehalten werden</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="331"/>
        <source>Previous track (audio player)</source>
        <translation>Vorheriges Stück (Audio Player)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="332"/>
        <source>Next track (audio player)</source>
        <translation>Nächstes Stück (Audio Player)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="333"/>
        <source>Fast backward (audio player)</source>
        <translation>Schneller Rücklauf (Audio Player)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="334"/>
        <source>Fast forward (audio player)</source>
        <translation>Schneller Vorlauf (Audio Player)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="335"/>
        <source>Stop track (audio player)</source>
        <translation>Stück anhalten (Audio Player)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="336"/>
        <source>Pause track (audio player)</source>
        <translation>Stück pausieren (Audio Player)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="337"/>
        <source>Play track (audio player)</source>
        <translation>Stück abspielen (Audio Player)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="319"/>
        <source>Parent / clone hierarchy</source>
        <translation>Original / Klon Hierarchie</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="338"/>
        <source>Raise volume (audio player)</source>
        <translation>Laustärke anheben (Audio Player)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="339"/>
        <source>Lower volume (audio player)</source>
        <translation>Lautstärke absenken (Audio Player)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="274"/>
        <source>Export ROM Status</source>
        <translation>ROM Status exportieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="586"/>
        <source>Fall back to the parent&apos;s image if an indivual image is missing but there&apos;s one for the parent</source>
        <translation>Rückgriff auf Bild des Original-Sets, sofern ein einzelnes Bild fehlt, aber das Bild des Originals vorhanden ist</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="589"/>
        <source>Parent image fallback</source>
        <translation>Original-Bild Rückgriff</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="271"/>
        <source>Show vertical game status indicator in game details</source>
        <translation>Zeige vertikalen Spiel Status Indikator in den Spiel-Details</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="274"/>
        <source>Game status indicator</source>
        <translation>Spiel Status Indikator</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="284"/>
        <source>Show the game status indicator only when the game list is not visible due to the current layout</source>
        <translation>Zeige Spiel Status Indikator nur an, wenn die Spieleliste aufgrund des aktuellen Layouts nicht sichtbar ist</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="287"/>
        <location filename="../../options.ui" line="313"/>
        <source>Only when required</source>
        <translation>Nur wenn benötigt</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="297"/>
        <source>Show game name</source>
        <translation>Zeige Spielnamen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="310"/>
        <source>Show game&apos;s description only when the game list is not visible due to the current layout</source>
        <translation>Zeige Spielbeschreibung nur an, wenn die Spieleliste aufgrund des aktuellen Layouts nicht sichtbar ist</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="231"/>
        <source>Show the menu bar</source>
        <translation>Menüzeile anzeigen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="234"/>
        <source>Show menu bar</source>
        <translation>Zeige Menüzeile</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="218"/>
        <location filename="../../options.ui" line="221"/>
        <source>Show status bar</source>
        <translation>Zeige Statuszeile</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="729"/>
        <location filename="../../options.ui" line="732"/>
        <source>Show tool bar</source>
        <translation>Zeige Werzeugleiste</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="298"/>
        <source>Toggle ROM state C</source>
        <translation>ROM Status K ein-/ausblenden</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="299"/>
        <source>Toggle ROM state M</source>
        <translation>ROM Status B ein-/ausblenden</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="300"/>
        <source>Toggle ROM state I</source>
        <translation>ROM Status I ein-/ausblenden</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="301"/>
        <source>Toggle ROM state N</source>
        <translation>ROM Status N ein-/ausblenden</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="302"/>
        <source>Toggle ROM state U</source>
        <translation>ROM Status U ein-/ausblenden</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="261"/>
        <source>Check for other instances of this QMC2 variant on startup</source>
        <translation>Beim Start prüfen, ob eine andere Instanz dieser QMC2-Variante bereits ausgeführt wird</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="264"/>
        <source>Check single instance</source>
        <translation>Einzel-Instanz Prüfung</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="308"/>
        <source>Launch QMC2 for SDLMAME</source>
        <translation>QMC2 für SDLMAME starten</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="309"/>
        <source>Launch QMC2 for SDLMESS</source>
        <translation>QMC2 für SDLMESS starten</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="223"/>
        <source>Machine info DB</source>
        <translation>Maschinen Info DB</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="226"/>
        <source>Machine information database - MESS sysinfo.dat (read)</source>
        <translation>Maschinen Informations Datenbank - MESS sysinfo.dat (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="227"/>
        <source>Browse machine information database (MESS sysinfo.dat)</source>
        <translation>Maschinen Informations Datenbank auswählen (MESS sysinfo.dat)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="596"/>
        <source>Minimize application windows when launching another variant</source>
        <translation>Anwendungsfenster minimieren, wenn eine andere Variante gestartet wird</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="599"/>
        <source>Minimize on variant launch</source>
        <translation>Minimieren beim Start einer Variante</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="327"/>
        <source>Toggle arcade mode</source>
        <translation>Arcade Modus ein-/ausschalten</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="328"/>
        <source>Show FPS (arcade mode)</source>
        <translation>FPS anzeigen (Arcade Mode)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="329"/>
        <source>Take snapshot (arcade mode)</source>
        <translation>Screenshot aufzeichnen (Arcade Mode)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="279"/>
        <source>Setup arcade mode</source>
        <translation>Acade Modus einstellen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="229"/>
        <source>Machine description</source>
        <translation>Maschinenbeschreibung</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="230"/>
        <source>Machine name</source>
        <translation>Maschinenname</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="231"/>
        <source>Number of item insertions between machine list updates during reload (higher means faster, but makes the GUI less responsive)</source>
        <translation>Anzahl der Objekt-Einfügungen zwischen Updates der Maschinenliste beim Laden (höhere Werte sind schneller, beeinflussen jedoch die Ansprechbarkeit der GUI negativ)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="232"/>
        <source>Delay update of any machine details (preview, flyer, info, configuration, ...) by how many milliseconds?</source>
        <translation>Updates aller Maschinendetails (Vorschaubildr, Flyer, Infos, Konfiguration, ...) um wie viele Millisekunden verzögern?</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="233"/>
        <source>Sort machine list while reloading (slower)</source>
        <translation>Maschinenliste beim Laden sortieren (langsamer)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2662"/>
        <source>Sort game list while reloading (slower)</source>
        <translation>Spieleliste beim Laden sortieren (langsamer)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2426"/>
        <source>Number of item insertions between game list updates during reload (higher means faster, but makes the GUI less responsive)</source>
        <translation>Anzahl der Objekt-Einfügungen zwischen Updates der Spieleliste beim Laden (höhere Werte sind schneller, beeinflussen jedoch die Ansprechbarkeit der GUI negativ)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="25"/>
        <source>&amp;Front end</source>
        <translation>&amp;Frontend</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="559"/>
        <source>Application style (Default = use system&apos;s default style)</source>
        <translation>Anwendungs-Stil (Standard = verwende Standard-Stil des Systems)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2614"/>
        <source>Automatically trigger a ROM check if necessary</source>
        <translation>ROM Prüfung automatisch auslösen, wenn benötigt</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="305"/>
        <source>Launch QMC2 for MAME</source>
        <translation>QMC2 für MAME starten</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="306"/>
        <source>Launch QMC2 for MESS</source>
        <translation>QMC2 für MESS starten</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3349"/>
        <location filename="../../options.ui" line="3415"/>
        <location filename="../../options.ui" line="3494"/>
        <source>Command</source>
        <translation>Kommando</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2665"/>
        <source>Sort while loading</source>
        <translation>Beim Laden sortieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2486"/>
        <source>Launch emulation on double-click events (may be annoying)</source>
        <translation>Emulation bei Doppel-Klick Ereignissen starten (könnte nervig sein)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2489"/>
        <source>Double-click activation</source>
        <translation>Aktivierung bei Doppel-Klick</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1424"/>
        <source>sorting game list by %1 in %2 order</source>
        <translation>Sortiere Spieleliste nach %1 in %2er Reihenfolge</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1426"/>
        <source>sorting machine list by %1 in %2 order</source>
        <translation>Sortiere Maschinenliste nach %1 in %2er Reihenfolge</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1208"/>
        <source>An open game-specific emulator configuration has been detected.
Use local game-settings, overwrite with global settings or don&apos;t apply?</source>
        <translation>Eine geöffnete Spiel-spezifische Emulator-Konfiguration wurde entdeckt.
Lokale Spiel-Einstellungen verwenden, mit globalen Einstellungen überschreiben oder Änderungen nicht anwenden?</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1210"/>
        <source>An open machine-specific emulator configuration has been detected.
Use local machine-settings, overwrite with global settings or don&apos;t apply?</source>
        <translation>Eine geöffnete Maschinen-spezifische Emulator-Konfiguration wurde entdeckt.
Lokale Maschinen-Einstellungen verwenden, mit globalen Einstellungen überschreiben oder Änderungen nicht anwenden?</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="326"/>
        <source>Toggle full screen</source>
        <translation>Vollbild-Modus umschalten</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="201"/>
        <location filename="../../options.cpp" line="211"/>
        <source>Specify arguments...</source>
        <translation>Kommandozeilen-Optionen festlegen...</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="204"/>
        <location filename="../../options.cpp" line="214"/>
        <source>Reset to default (same path assumed)</source>
        <translation>Auf Standard zurücksetzen (gleicher Pfad)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="220"/>
        <source>Browse emulator information database (messinfo.dat)</source>
        <translation>Emulator Informations Datenbank auswählen (messinfo.dat)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="221"/>
        <source>Load emulator information database (messinfo.dat)</source>
        <translation>Emulator Informations Datenbank laden (messinfo.dat)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="222"/>
        <source>Emulator information database - messinfo.dat (read)</source>
        <translation>Emulator Informations Datenbank - messinfo.dat (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="224"/>
        <source>Load machine information database (MESS sysinfo.dat)</source>
        <translation>Maschinen Informations Datenbank laden (MESS sysinfo.dat)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="225"/>
        <source>Use in-memory compression for machine info DB (a bit slower, but consumes distinctly less memory; compression rate is usually about 1:16)</source>
        <translation>Daten-Kompression im Hauptspeicher für Maschinen-Info DB aktivieren (ein wenig langsamer, aber reduziert den Speicherbedarf deutlich; die Kompressionsrate liegt üblicherweise bei etwa 1:16)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="249"/>
        <location filename="../../options.cpp" line="250"/>
        <source>Option requires a reload of the entire machine list to take effect</source>
        <translation>Option erfordert eine Aktualisierung der Maschinenliste um wirksam zu werden</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="251"/>
        <source>Hide primary machine list while loading (recommended, much faster)</source>
        <translation>Primäre Maschinenliste während des Ladens verbergen (empfohlen, sehr viel schneller)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="252"/>
        <source>Registered emulators -- you may select one of these in the machine-specific emulator configuration</source>
        <translation>Registrierte Emulatoren -- diese lassen sich in den Maschinen-spezifischen Emulator Einstellungen auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="253"/>
        <source>Save machine selection</source>
        <translation>Speichere Maschinen-Selektion</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="254"/>
        <source>Save machine selection on exit and before reloading the machine list</source>
        <translation>Ausgewählte Maschine beim Beenden und vor dem Neuladen der Maschinenliste speichern</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="255"/>
        <source>Restore machine selection</source>
        <translation>Restauriere Maschinen-Selektion</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="256"/>
        <source>Restore saved machine selection at start and after reloading the machine list</source>
        <translation>Gespeicherte Maschinenauswahl beim Start und nach dem Neuladen der Maschinenliste wieder herstellen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="258"/>
        <location filename="../../options.cpp" line="964"/>
        <source>Category</source>
        <translation>Kategorie</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="259"/>
        <location filename="../../options.cpp" line="965"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>Demo mode</source>
        <translation type="obsolete">Demo Modus</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="283"/>
        <source>Play (independent)</source>
        <translation>Spielen (unabhängig)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="285"/>
        <source>Play (embedded)</source>
        <translation>Spielen (eingebettet)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="290"/>
        <source>Check states of tagged ROMs</source>
        <translation>ROM Status markierter Sets prüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="292"/>
        <source>Check template map</source>
        <translation>Abbildungsvorlage überprüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="295"/>
        <source>Clear YouTube cache</source>
        <translation>YouTube Cache leeren</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="312"/>
        <source>Tag current set</source>
        <translation>Aktuelles Sets markieren</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="313"/>
        <source>Untag current set</source>
        <translation>Markierung aufheben</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="314"/>
        <source>Toggle tag mark</source>
        <translation>Markierung umkehren</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="315"/>
        <source>Tag all sets</source>
        <translation>Alle Sets markieren</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="316"/>
        <source>Untag all sets</source>
        <translation>Alle Markierungen aufheben </translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="317"/>
        <source>Invert all tags</source>
        <translation>Alle Markierungen umkehren</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="321"/>
        <source>View games by category</source>
        <translation>Spiele nach Kategorien anzeigen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="322"/>
        <source>View games by version</source>
        <translation>Spiele nach Version anzeigen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="324"/>
        <source>Run external ROM tool</source>
        <translation>Externes ROM Werkzeug ausführen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="325"/>
        <source>Run ROM tool for tagged sets</source>
        <translation>Externes ROM Werkzeug für alle markierten Sets ausführen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="363"/>
        <location filename="../../options.cpp" line="370"/>
        <source>Reset to default font</source>
        <translation>Auf Standard-Schriftart zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="388"/>
        <location filename="../../options.cpp" line="389"/>
        <source>Search in the folder we were called from</source>
        <translation>Suche im Ordner aus dem wir aufgerufen wurden</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1317"/>
        <source>invalidating machine info DB</source>
        <translation>Invalidiere Maschinen-Info DB</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1351"/>
        <source>please reload game list for some changes to take effect</source>
        <translation>Bitte Spieleliste neu laden um einige der Änderungen wirksam werden zu lassen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1353"/>
        <source>please reload machine list for some changes to take effect</source>
        <translation>Bitte Maschinenliste neu laden um einige der Änderungen wirksam werden zu lassen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1367"/>
        <source>re-sort of game list impossible at this time, please wait for ROM verification to finish and try again</source>
        <translation>Sortierung der Spieleliste ist derzeit nicht möglich, bitte auf die Beendigung der ROM Verifikation warten und dann ggf. neu versuchen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1369"/>
        <source>re-sort of machine list impossible at this time, please wait for ROM verification to finish and try again</source>
        <translation>Sortierung der Maschinenliste ist derzeit nicht möglich, bitte auf die Beendigung der ROM Verifikation warten und dann ggf. neu versuchen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1645"/>
        <location filename="../../options.cpp" line="1649"/>
        <source>FATAL: can&apos;t open cabinet file, please check access permissions for %1</source>
        <translation>FATAL: kann Gehäuse-Datei nicht öffnen; bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1663"/>
        <location filename="../../options.cpp" line="1667"/>
        <source>FATAL: can&apos;t open controller file, please check access permissions for %1</source>
        <translation>FATAL: kann Controller-Datei nicht öffnen; bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1717"/>
        <location filename="../../options.cpp" line="1721"/>
        <source>FATAL: can&apos;t open PCB file, please check access permissions for %1</source>
        <translation>FATAL: kann PCB-Datei nicht öffnen, bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1735"/>
        <location filename="../../options.cpp" line="1739"/>
        <source>FATAL: can&apos;t open software snap-shot file, please check access permissions for %1</source>
        <translation>FATAL: kann Software Snapshot Datei nicht öffnen; bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1764"/>
        <source>triggering automatic reload of game list</source>
        <translation>Löse automatische Aktualisierung der Spieleliste aus</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1766"/>
        <source>triggering automatic reload of machine list</source>
        <translation>Löse automatische Aktualisierung der Maschinenliste aus</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1915"/>
        <location filename="../../options.cpp" line="1972"/>
        <location filename="../../options.cpp" line="2880"/>
        <source>Cabinet file</source>
        <translation>Gehäuse Datei</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1397"/>
        <location filename="../../options.cpp" line="1915"/>
        <location filename="../../options.cpp" line="1972"/>
        <location filename="../../options.cpp" line="2880"/>
        <source>Cabinet directory</source>
        <translation>Gehäuse-Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1920"/>
        <location filename="../../options.cpp" line="1977"/>
        <location filename="../../options.cpp" line="2891"/>
        <source>Controller file</source>
        <translation>Controller Datei</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1500"/>
        <location filename="../../options.cpp" line="1920"/>
        <location filename="../../options.cpp" line="1977"/>
        <location filename="../../options.cpp" line="2891"/>
        <source>Controller directory</source>
        <translation>Controller-Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1935"/>
        <location filename="../../options.cpp" line="1992"/>
        <location filename="../../options.cpp" line="2924"/>
        <source>PCB file</source>
        <translation>PCB Datei</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1809"/>
        <location filename="../../options.cpp" line="1935"/>
        <location filename="../../options.cpp" line="1992"/>
        <location filename="../../options.cpp" line="2924"/>
        <source>PCB directory</source>
        <translation>PCB Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2271"/>
        <source>Choose Qt style sheet file</source>
        <translation>Qt Style Sheet Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2271"/>
        <source>Qt Style Sheets (*.qss)</source>
        <translation>Qt Style Sheets (*.qss)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2337"/>
        <source>Choose cabinet directory</source>
        <translation>Gehäuse-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2351"/>
        <source>Choose controller directory</source>
        <translation>Controller-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2393"/>
        <source>Choose PCB directory</source>
        <translation>PCB Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2432"/>
        <source>Choose MAME variant&apos;s exe file</source>
        <translation>Exe-Datei der MAME Variante auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2444"/>
        <source>Choose MESS variant&apos;s exe file</source>
        <translation>Exe-Datei der MESS Variante auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2459"/>
        <source>MAME variant arguments</source>
        <translation>Kommandozeilen-Optionen der MAME Variante</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2460"/>
        <source>Specify command line arguments passed to the MAME variant
(empty means: &apos;pass the arguments we were called with&apos;):</source>
        <translation>Kommandozeilen-Optionen, die beim Aufruf der MAME Variante übergeben werden
(leer bedeutet: &apos;verwende dieselben Optionen, die uns übergeben wurden&apos;):</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2478"/>
        <source>MESS variant arguments</source>
        <translation>Kommandozeilen-Optionen der MESS Variante</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2479"/>
        <source>Specify command line arguments passed to the MESS variant
(empty means: &apos;pass the arguments we were called with&apos;):</source>
        <translation>Kommandozeilen-Optionen, die beim Aufruf der MESS Variante übergeben werden
(leer bedeutet: &apos;verwende dieselben Optionen, die uns übergeben wurden&apos;):</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2543"/>
        <source>Choose ROM tool</source>
        <translation>ROM Werkzeug auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2555"/>
        <location filename="../../options.cpp" line="2617"/>
        <location filename="../../options.cpp" line="3278"/>
        <source>Choose working directory</source>
        <translation>Arbeitsverzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2646"/>
        <source>Choose software list cache file</source>
        <translation>Softwarelisten-Cache Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2658"/>
        <source>Choose general software folder</source>
        <translation>Allgmeines Software-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2723"/>
        <source>Choose catver.ini file</source>
        <translation>Datei catver.ini auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2980"/>
        <source>Choose ZIP-compressed cabinet file</source>
        <translation>ZIP-komprimierte Gehäuse-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2992"/>
        <source>Choose ZIP-compressed controller file</source>
        <translation>ZIP-komprimierte Controller Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="294"/>
        <source>Show game&apos;s description at the bottom of any images</source>
        <translation>Spielbeschreibung am unteren Rand jedes Bildes anzeigen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2033"/>
        <location filename="../../options.ui" line="2553"/>
        <location filename="../../options.ui" line="4068"/>
        <source>Option requires a reload of the entire game list to take effect</source>
        <translation>Option erfordert eine Aktualisierung der Spieleliste um wirksam zu werden</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="191"/>
        <source>Scaled cabinet</source>
        <translation>Gehäusebilder skalieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="509"/>
        <source>Style sheet</source>
        <translation>Style Sheet</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="201"/>
        <source>Scaled controller</source>
        <translation>Controller-Bilder skalieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="244"/>
        <source>Show progress texts</source>
        <translation>Fortschrittstexte anzeigen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="425"/>
        <source>Scaled PCB</source>
        <translation>PCB-Bilder skalieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="522"/>
        <source>Qt style sheet file (*.qss, leave empty for no style sheet)</source>
        <translation>Qt Style Sheet datei (*.qss, leer bedeutet kein Style Sheet)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="535"/>
        <source>Browse Qt style sheet file</source>
        <translation>Qt Style Sheet Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="648"/>
        <source>Show indicator for current memory usage</source>
        <translation>Aktuelle Speicherauslastung anzeigen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1432"/>
        <source>Cabinet directory (read)</source>
        <translation>Gehäuse-Verzeichnis (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1445"/>
        <source>Browse cabinet directory</source>
        <translation>Gehäuse-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1469"/>
        <source>ZIP-compressed cabinet file (read)</source>
        <translation>ZIP-komprimierte Gehäuse-Datei (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1482"/>
        <source>Browse ZIP-compressed cabinet file</source>
        <translation>ZIP-komprimierte Gehäuse-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1394"/>
        <source>Switch between specifying a cabinet directory or a ZIP-compressed cabinet file</source>
        <translation>Zwischen der Angabe eines Gehäuse-Verzeichnisses oder einer ZIP-komprimierten Gehäuse-Datei umschalten</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1535"/>
        <source>Controller directory (read)</source>
        <translation>Controller-Verzeichnis (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1548"/>
        <source>Browse controller directory</source>
        <translation>Controller-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1572"/>
        <source>ZIP-compressed controller file (read)</source>
        <translation>ZIP-komprimierte Controller-Datei (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1585"/>
        <source>Browse ZIP-compressed controller file</source>
        <translation>ZIP-komprimierte Controller-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1497"/>
        <source>Switch between specifying a controller directory or a ZIP-compressed controller file</source>
        <translation>Zwischen der Angabe eines Controller-Verzeichnisses oder einer ZIP-komprimierten Controller-Datei umschalten</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1681"/>
        <location filename="../../options.cpp" line="1685"/>
        <source>FATAL: can&apos;t open marquee file, please check access permissions for %1</source>
        <translation>FATAL: kann Marquee-Datei nicht öffnen; bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1699"/>
        <location filename="../../options.cpp" line="1703"/>
        <source>FATAL: can&apos;t open title file, please check access permissions for %1</source>
        <translation>FATAL: kann Titel-Datei nicht öffnen; bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1925"/>
        <location filename="../../options.cpp" line="1982"/>
        <location filename="../../options.cpp" line="2902"/>
        <source>Marquee file</source>
        <translation>Marquee Datei</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1603"/>
        <location filename="../../options.cpp" line="1925"/>
        <location filename="../../options.cpp" line="1982"/>
        <location filename="../../options.cpp" line="2902"/>
        <source>Marquee directory</source>
        <translation>Marquee Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1930"/>
        <location filename="../../options.cpp" line="1987"/>
        <location filename="../../options.cpp" line="2913"/>
        <source>Title file</source>
        <translation>Titel Datei</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1706"/>
        <location filename="../../options.cpp" line="1930"/>
        <location filename="../../options.cpp" line="1987"/>
        <location filename="../../options.cpp" line="2913"/>
        <source>Title directory</source>
        <translation>Titel Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2365"/>
        <source>Choose marquee directory</source>
        <translation>Marquee-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2379"/>
        <source>Choose title directory</source>
        <translation>Titel-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3004"/>
        <source>Choose ZIP-compressed marquee file</source>
        <translation>ZIP-komprimierte Marquee Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="211"/>
        <source>Scaled marquee</source>
        <translation>Marqueebilder skalieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="359"/>
        <source>Scaled title</source>
        <translation>Titelbilder skalieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1638"/>
        <source>Marquee directory (read)</source>
        <translation>Marquee-Verzeichnis (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1651"/>
        <source>Browse marquee directory</source>
        <translation>Marquee-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1675"/>
        <source>ZIP-compressed marquee file (read)</source>
        <translation>ZIP-komprimierte Marquee-Datei (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1688"/>
        <source>Browse ZIP-compressed marquee file</source>
        <translation>ZIP-komprimierte Marquee Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1600"/>
        <source>Switch between specifying a marquee directory or a ZIP-compressed marquee file</source>
        <translation>Zwischen der Angabe eines Marquee-Verzeichnisses oder einer ZIP-komprimierten Marquee-Datei umschalten</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1741"/>
        <source>Title directory (read)</source>
        <translation>Titel Verzeichnis (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1754"/>
        <source>Browse title directory</source>
        <translation>Titel-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1778"/>
        <source>ZIP-compressed title file (read)</source>
        <translation>ZIP-komprimierte Titel-Datei (lesen)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1791"/>
        <source>Browse ZIP-compressed title file</source>
        <translation>ZIP-komprimierte Titel-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1703"/>
        <source>Switch between specifying a title directory or a ZIP-compressed title file</source>
        <translation>Zwischen der Angabe eines Titel-Verzeichnisses oder einer ZIP-komprimierten Titel-Datei umschalten</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="228"/>
        <source>Machine &amp;list</source>
        <translation>Maschinen&amp;liste</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2155"/>
        <source>Game &amp;list</source>
        <translation>Spiele&amp;liste</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="467"/>
        <source>Front end log size</source>
        <translation>Frontend Log Größe</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="480"/>
        <source>Maximum number of lines to keep in front end log browser</source>
        <translation>Maximale Anzahl von Zeilen, die im Frontend Log Browser gehalten werden</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="833"/>
        <source>Front end log file</source>
        <translation>Frontend Log Datei</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="846"/>
        <source>Front end log file (write)</source>
        <translation>Frontend Log Datei (scheiben)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="859"/>
        <source>Browse front end log file</source>
        <translation>Frontend Log Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2672"/>
        <source>Choose front end log file</source>
        <translation>Frontend Log Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2632"/>
        <source>Choose MAWS cache directory</source>
        <translation>MAWS Cache Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4080"/>
        <source>MAWS cache directory</source>
        <translation>MAWS Cache Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4093"/>
        <source>MAWS cache directory (write)</source>
        <translation>MAWS Cache Verzeichnis (schreiben)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4106"/>
        <source>Browse MAWS cache directory</source>
        <translation>MAWS Cache Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3620"/>
        <source>Use HTTP proxy</source>
        <translation>HTTP Proxy verwenden</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3632"/>
        <source>Host / IP</source>
        <translation>Host / IP</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3645"/>
        <source>Hostname or IP address of the HTTP proxy server</source>
        <translation>Hostname oder IP Adresse des HTTP Proxy Servers</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3652"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3665"/>
        <source>Port to access the HTTP proxy service</source>
        <translation>Port zum Zugriff auf den HTTP Proxy Dienst</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3678"/>
        <source>User ID</source>
        <translation>Benutzer ID</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3691"/>
        <source>User ID to access the HTTP proxy service (empty = no authentication)</source>
        <translation>Benutzer ID zum Zugriff auf den HTTP Proxy Dienst (leer = keine Authentifikation)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3698"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3617"/>
        <source>Enable / disable the use of an HTTP proxy on any web lookups</source>
        <translation>Verwendung eines HTTP Proxies beim Zugriff auf das Web aktivieren/deaktivieren</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3711"/>
        <source>Password to access the HTTP proxy service (empty = no authentication)</source>
        <translation>Passwort zum Zugriff auf den HTTP Proxy Dienst (leer = keine Authentifikation)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="280"/>
        <source>Clear MAWS cache</source>
        <translation>MAWS Cache leeren</translation>
    </message>
</context>
<context>
    <name>PCB</name>
    <message>
        <location filename="../../pcb.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>In Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="56"/>
        <location filename="../../pcb.cpp" line="57"/>
        <source>Game PCB image</source>
        <translation>Spiel PCB-Bild</translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="59"/>
        <location filename="../../pcb.cpp" line="60"/>
        <source>Machine PCB image</source>
        <translation>Machinen PCB-Bild</translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="68"/>
        <location filename="../../pcb.cpp" line="72"/>
        <source>FATAL: can&apos;t open PCB file, please check access permissions for %1</source>
        <translation>FATAL: kann PCB-Datei nicht öffnen, bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Warte auf Daten...</translation>
    </message>
</context>
<context>
    <name>Preview</name>
    <message>
        <location filename="../../preview.cpp" line="102"/>
        <source>Waiting for data...</source>
        <translation>Warte auf Daten...</translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="58"/>
        <location filename="../../preview.cpp" line="59"/>
        <source>Game preview image</source>
        <translation>Spiel-Vorschaubild</translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="51"/>
        <source>Copy to clipboard</source>
        <translation>In Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="70"/>
        <location filename="../../preview.cpp" line="74"/>
        <source>FATAL: can&apos;t open preview file, please check access permissions for %1</source>
        <translation>FATAL: kann Vorschau-Datei nicht öffnen; bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="61"/>
        <location filename="../../preview.cpp" line="62"/>
        <source>Machine preview image</source>
        <translation>Machinen-Vorschaubild</translation>
    </message>
</context>
<context>
    <name>ProcessManager</name>
    <message>
        <location filename="../../procmgr.cpp" line="131"/>
        <source>terminating emulator #%1, PID = %2</source>
        <translation>Beende Emulator #%1, PID = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="154"/>
        <source>killing emulator #%1, PID = %2</source>
        <translation>Töte Emulator #%1, PID = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="99"/>
        <location filename="../../procmgr.cpp" line="101"/>
        <source>starting emulator #%1, command = %2</source>
        <translation>Starte Emulator #%1, Kommando = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="236"/>
        <source>emulator #%1 finished, exit code = %2, exit status = %3, remaining emulators = %4</source>
        <translation>Emulator #%1 beendet, Rückgabewert = %2, Beendigungs-Status = %3, verbliebene Emulatoren = %4</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="236"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="236"/>
        <source>crashed</source>
        <translation>abgestürzt</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="281"/>
        <source>emulator #%1 started, PID = %2, running emulators = %3</source>
        <translation>Emulator #%1 gestartet, PID = %2, aktive Emulatoren = %3</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="320"/>
        <source>FATAL: failed to start emulator #%1</source>
        <translation>FATAL: Emulator #%1 konnte nicht gestartet werden</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="325"/>
        <source>WARNING: emulator #%1 crashed</source>
        <translation>WARNUNG: Emulator #%1 ist abgestürzt</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="329"/>
        <source>WARNING: failed to write to emulator #%1</source>
        <translation>WARNUNG: Fehler beim Schreiben auf Eingabe-Kanal von Emulator #%1</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="333"/>
        <source>WARNING: failed to read from emulator #%1</source>
        <translation>WARNUNG: Fehler beim Lesen von Ausgabe-Kanal des Emulators #%1</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="337"/>
        <source>WARNING: unhandled error for emulator #%1, error code = %2</source>
        <translation>WARNUNG: nicht verarbeiteter Fehler von Emulator #%1, Fehler-Code = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="355"/>
        <source>no error</source>
        <translation>kein Fehler</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="356"/>
        <source>failed validity checks</source>
        <translation>Validierungen fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="357"/>
        <source>missing files</source>
        <translation>Dateien fehlen</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="358"/>
        <source>fatal error</source>
        <translation>fataler Fehler</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="359"/>
        <source>device initialization error</source>
        <translation>Geräte-Initialisierung fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="361"/>
        <source>game doesn&apos;t exist</source>
        <translation>Spiel existiert nicht</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="363"/>
        <source>machine doesn&apos;t exist</source>
        <translation>Maschine existiert nicht</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="365"/>
        <source>invalid configuration</source>
        <translation>ungültige Konfiguration</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="366"/>
        <source>identified all non-ROM files</source>
        <translation>alle nicht-ROM Dateien identifiziert</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="367"/>
        <source>identified some files but not all</source>
        <translation>einige Dateien identifiziert, aber nicht alle</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="368"/>
        <source>identified no files</source>
        <translation>keine Dateien identifiziert</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="369"/>
        <source>unknown error</source>
        <translation>unbekannter Fehler</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="185"/>
        <source>stdout[#%1]:</source>
        <translation>stdout[#%1]:</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="203"/>
        <source>stderr[#%1]:</source>
        <translation>stderr[#%1]:</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="145"/>
        <source>WARNING: ProcessManager::terminate(ushort index = %1): trying to terminate a null process</source>
        <translation>WARNUNG: ProcessManager::terminate(ushort index = %1): es wurde versucht, einen Null-Prozess zu terminieren</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="61"/>
        <source>WARNING: ProcessManager::start(): the specified working directory &apos;%1&apos; is not a directory -- ignored</source>
        <translation>WARNUNG: ProcessManager::start(): Das angegebene Arbeitsverzeichnis &apos;%1&apos; ist kein Verzeichnis -- ignoriert</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="64"/>
        <source>WARNING: ProcessManager::start(): the specified working directory &apos;%1&apos; does not exist -- ignored</source>
        <translation>WARNUNG: ProcessManager::start(): Das angegebene Arbeitsverzeichnis &apos;%1&apos; existiert nicht -- ignoriert</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="168"/>
        <source>WARNING: ProcessManager::kill(ushort index = %1): trying to kill a null process</source>
        <translation>WARNUNG: ProcessManager::kill(ushort index = %1): es wurde versucht, einen Null-Prozess zu töten</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="233"/>
        <source>WARNING: ProcessManager::finished(...): trying to remove a null item</source>
        <translation>WARNUNG: ProcessManager::finished(...): es wurde versucht, ein Null-Item zu löschen</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../gamelist.cpp" line="2086"/>
        <location filename="../../options.cpp" line="1409"/>
        <source>players</source>
        <translation>Spieler</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2093"/>
        <location filename="../../options.cpp" line="1416"/>
        <source>category</source>
        <translation>Kategorie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2096"/>
        <location filename="../../options.cpp" line="1419"/>
        <source>version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2083"/>
        <location filename="../../options.cpp" line="1406"/>
        <source>ROM types</source>
        <translation>ROM Typen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9440"/>
        <location filename="../../qmc2main.cpp" line="9442"/>
        <source>M.A.M.E. Catalog / Launcher II v</source>
        <translation>M.A.M.E. Catalog / Launcher II v</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="802"/>
        <location filename="../../options.cpp" line="1845"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2059"/>
        <location filename="../../options.cpp" line="1381"/>
        <source>game description</source>
        <translation>Spiel-Beschreibung</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2065"/>
        <location filename="../../options.cpp" line="1387"/>
        <source>ROM state</source>
        <translation>ROM-Status</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2068"/>
        <location filename="../../options.cpp" line="1390"/>
        <source>tag</source>
        <translation>Markierung</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2071"/>
        <location filename="../../options.cpp" line="1393"/>
        <source>year</source>
        <translation>Jahr</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2074"/>
        <location filename="../../options.cpp" line="1396"/>
        <source>manufacturer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2089"/>
        <location filename="../../options.cpp" line="1412"/>
        <source>driver status</source>
        <translation>Treiberstatus</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3012"/>
        <source>correct</source>
        <translation>korrekt</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3047"/>
        <source>incorrect</source>
        <translation>inkorrekt</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3082"/>
        <source>mostly correct</source>
        <translation>beinahe korrekt</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3117"/>
        <source>not found</source>
        <translation>nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3152"/>
        <location filename="../../gamelist.cpp" line="3190"/>
        <location filename="../../romalyzer.cpp" line="2843"/>
        <location filename="../../romalyzer.cpp" line="2876"/>
        <location filename="../../romalyzer.cpp" line="2888"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9491"/>
        <source>Export to...</source>
        <translation>Exportieren nach...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3603"/>
        <location filename="../../qmc2main.cpp" line="9499"/>
        <source>Import from...</source>
        <translation>Importieren von...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9517"/>
        <location filename="../../qmc2main.cpp" line="9525"/>
        <source>&lt;inipath&gt;/mame.ini</source>
        <translation>&lt;inipath&gt;/mame.ini</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9521"/>
        <location filename="../../qmc2main.cpp" line="9529"/>
        <source>Select file...</source>
        <translation>Datei auswählen...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3597"/>
        <location filename="../../qmc2main.cpp" line="3598"/>
        <source>Export game-specific MAME configuration</source>
        <translation>Spiel-spezifische MAME Konfiguration exportieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3605"/>
        <location filename="../../qmc2main.cpp" line="3606"/>
        <source>Import game-specific MAME configuration</source>
        <translation>Spiel-spezifische MAME Konfiguration importieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9446"/>
        <source>SVN r%1</source>
        <translation>SVN r%1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9493"/>
        <location filename="../../qmc2main.cpp" line="9494"/>
        <source>Export global MAME configuration</source>
        <translation>Globale MAME Konfiguration exportieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9501"/>
        <location filename="../../qmc2main.cpp" line="9502"/>
        <source>Import global MAME configuration</source>
        <translation>Globale MAME Konfiguration importieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9477"/>
        <source>processing global emulator configuration</source>
        <translation>Verarbeite globale Emulator-Konfiguration</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2827"/>
        <source>ROM</source>
        <translation>ROM</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="757"/>
        <location filename="../../romalyzer.cpp" line="2827"/>
        <source>CHD</source>
        <translation>CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2831"/>
        <location filename="../../romalyzer.cpp" line="2873"/>
        <location filename="../../romalyzer.cpp" line="2893"/>
        <source>good</source>
        <translation>gut</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="758"/>
        <location filename="../../romalyzer.cpp" line="2835"/>
        <location filename="../../romalyzer.cpp" line="2882"/>
        <source>no dump</source>
        <translation>kein Dump</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2839"/>
        <location filename="../../romalyzer.cpp" line="2885"/>
        <source>bad dump</source>
        <translation>Schlechter Dump</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2879"/>
        <source>no / bad dump</source>
        <translation>kein / schlechter Dump</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2078"/>
        <location filename="../../options.cpp" line="1400"/>
        <source>game name</source>
        <translation>Spielname</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9464"/>
        <source>OpenGL features enabled</source>
        <translation>OpenGL Features aktiviert</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9496"/>
        <location filename="../../qmc2main.cpp" line="9497"/>
        <source>Export global MESS configuration</source>
        <translation>Globale MESS Konfiguration exportieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9504"/>
        <location filename="../../qmc2main.cpp" line="9505"/>
        <source>Import global MESS configuration</source>
        <translation>Globale MESS Konfiguration importieren</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2061"/>
        <location filename="../../options.cpp" line="1383"/>
        <source>machine description</source>
        <translation>Maschinen-Beschreibung</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2080"/>
        <location filename="../../options.cpp" line="1402"/>
        <source>machine name</source>
        <translation>Maschinen Name</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9473"/>
        <source>SDL joystick support enabled - using SDL v%1.%2.%3</source>
        <translation>SDL Joystick Unterstützung aktiviert - verwende SDL v%1.%2.%3</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9519"/>
        <location filename="../../qmc2main.cpp" line="9527"/>
        <source>&lt;inipath&gt;/mess.ini</source>
        <translation>&lt;inipath&gt;/mess.ini</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3600"/>
        <location filename="../../qmc2main.cpp" line="3601"/>
        <source>Export machine-specific MESS configuration</source>
        <translation>Maschinen-spezifische MESS Konfiguration exportieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3608"/>
        <location filename="../../qmc2main.cpp" line="3609"/>
        <source>Import machine-specific MESS configuration</source>
        <translation>Maschinen-spezifische MESS Konfiguration importieren</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9438"/>
        <source>M.E.S.S. Catalog / Launcher II v</source>
        <translation>M.E.S.S. Catalog / Launcher II v</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9468"/>
        <source>Phonon features enabled - using Phonon v%1</source>
        <translation>Phonon Features aktiviert - verwende Phonon v%1</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1619"/>
        <source>video player: XML error: fatal error on line %1, column %2: %3</source>
        <translation>Videospieler: XML-Fehler: fataler Fehler in Zeile %1, Spalte %2: %3</translation>
    </message>
</context>
<context>
    <name>ROMAlyzer</name>
    <message>
        <location filename="../../romalyzer.cpp" line="2511"/>
        <source>checksum wizard: FATAL: can&apos;t open ZIP archive &apos;%1&apos; for reading</source>
        <translation>Prüfsummen-Assistent: FATAL: kann ZIP Archiv &apos;%1&apos; nicht zum Lesen öffnen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2534"/>
        <source>Repairing set &apos;%1&apos; - %2</source>
        <translation>Repariere Set &apos;%1&apos; - %2</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2535"/>
        <source>checksum wizard: repairing %1 file &apos;%2&apos; in &apos;%3&apos; from repro template</source>
        <translation>Prüfsummen-Assistent: repariere %1 Datei &apos;%2&apos; in &apos;%3&apos; aus Repro-Vorlage</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2620"/>
        <source>checksum wizard: FATAL: can&apos;t open file &apos;%1&apos; in ZIP archive &apos;%2&apos; for writing</source>
        <translation>Prüfsummen-Assistent: FATAL: kann Datei &apos;%1&apos; in ZIP Archiv &apos;%2&apos; nicht zum Schreiben öffnen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2626"/>
        <source>Fixed by QMC2 v%1 (%2)</source>
        <translation>Korrigiert durch QMC2 v%1 (%2)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2632"/>
        <source>checksum wizard: FATAL: can&apos;t open ZIP archive &apos;%1&apos; for writing</source>
        <translation>Prüfsummen-Assistent: FATAL: kann ZIP Archiv &apos;%1&apos; nicht zum Schreiben öffnen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2647"/>
        <source>repaired</source>
        <translation>repariert</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2649"/>
        <source>checksum wizard: successfully repaired %1 file &apos;%2&apos; in &apos;%3&apos; from repro template</source>
        <translation>Prüfsummen-Assistent: %1 Datei &apos;%2&apos; in &apos;%3&apos; wurde erfolgreich repariert</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2766"/>
        <source>Choose local DB output path</source>
        <translation>Lokalen DB Ausgabe-Pfad wählen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2728"/>
        <source>Connection check -- succeeded!</source>
        <translation>Verbindung prüfen -- erfolgreich!</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2738"/>
        <source>Connection check -- failed!</source>
        <translation>Verbindung prüfen -- fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2729"/>
        <source>database connection check successful</source>
        <translation>Datenbankverbindung erfolgreich geprüft</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1754"/>
        <source>Choose CHD manager executable file</source>
        <translation>Ausführbare CHD Manager Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1754"/>
        <source>All files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1766"/>
        <source>Choose temporary working directory</source>
        <translation>Temporäres Arbeitsverzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1809"/>
        <source>CHD manager: external process started</source>
        <translation>CHD Manager: externer Prozess gestartet</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1820"/>
        <location filename="../../romalyzer.cpp" line="2005"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1822"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1823"/>
        <source>crashed</source>
        <translation>abgestürzt</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1825"/>
        <source>CHD manager: external process finished (exit code = %1, exit status = %2)</source>
        <translation>CHD Manager: externer Prozess beendet (Exit-Code = %1, Exit-Status = %2)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1841"/>
        <source>CHD manager: stdout: %1</source>
        <translation>CHD Manager: stdout: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1863"/>
        <source>CHD manager: stderr: %1</source>
        <translation>CHD Manager: stderr: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1890"/>
        <source>CHD manager: failed to start</source>
        <translation>CHD Manager: Starten fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1894"/>
        <source>CHD manager: crashed</source>
        <translation>CHD Manager: abgestürzt</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1898"/>
        <source>CHD manager: write error</source>
        <translation>CHD Manager: Schreibfehler</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1902"/>
        <source>CHD manager: read error</source>
        <translation>CHD Manager: Lesefehler</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1906"/>
        <source>CHD manager: unknown error %1</source>
        <translation>CHD Manager: unbekannter Fehler %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="15"/>
        <source>ROMAlyzer</source>
        <translation>ROMAlyzer</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="42"/>
        <location filename="../../romalyzer.cpp" line="1039"/>
        <source>&amp;Analyze</source>
        <translation>&amp;Analysieren</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="82"/>
        <source>Analysis report</source>
        <translation>Analyse-Bericht</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="127"/>
        <location filename="../../romalyzer.ui" line="1346"/>
        <source>SHA1</source>
        <translation>SHA1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="132"/>
        <source>MD5</source>
        <translation>MD5</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="599"/>
        <source>Enable ROM database support (repository access may be slow)</source>
        <translation>ROM Datenbank unterstützen (Zugriff möglicherweise langsam)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="698"/>
        <source>Server / IP</source>
        <translation>Server / IP</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="685"/>
        <source>Name or IP address of the database server</source>
        <translation>Name oder IP Adresse des Datenbankservers</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="614"/>
        <source>Driver</source>
        <translation>Treiber</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="627"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="643"/>
        <source>default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="711"/>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="724"/>
        <source>Username used to access the database</source>
        <translation>Benutzername der beim Datenbank-Zugriff verwendet wird</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="798"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="824"/>
        <source>Automatically download missing / bad files from the database (if they are available in the repository)</source>
        <translation>Fehlende / defekte Dateien automatisch von der Datenbank herunterladen (sofern sie im Repository enthalten sind)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="827"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="840"/>
        <source>Automatically upload good files to the database (if they are missing in the repository)</source>
        <translation>Korrekte Dateien automatisch zur Datenbank hochladen (sofern sie im Repository fehlen)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="662"/>
        <source>SQL driver to use</source>
        <translation>Zu verwendender SQL Treiber</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="289"/>
        <source>Temporary directory used by the CHD manager (make sure it has enough room to store the biggest CHDs)</source>
        <translation>Temporäres Arbeitsverzeichnis des CHD Managers (stelle sicher, dass genug Platz vorhanden ist, um die größten CHDs zwischen zu speichern)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="302"/>
        <source>Browse temporary directory used by the CHD manager</source>
        <translation>Temporäres Arbeitsverzeichnis des CHD Managers auswählen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="366"/>
        <source>General analysis flags and limits</source>
        <translation>Allgemeine Analyse-Einstellungen und Obergrenzen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="472"/>
        <source>&lt;b&gt;Limits:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Grenzen:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="479"/>
        <source>File size</source>
        <translation>Dateigröße</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="492"/>
        <source>Maximum size (in MB) of files to be loaded, files are skipped when they are bigger than that (0 = no limit)</source>
        <translation>Maximale Größe (in MB) zu ladender Dateien, größere Dateien werden übersprungen (0 = keine Beschränkung)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="514"/>
        <source>Log size</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="549"/>
        <source>Reports</source>
        <translation>Berichte</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="562"/>
        <source>Maximum number of reported sets held in memory (0 = no limit)</source>
        <translation>Maximale Anzahl im Hauptspeicher gehaltener Reports (0 = keine Beschränkung)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="666"/>
        <source>MySQL</source>
        <translation>MySQL</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="671"/>
        <source>SQLite</source>
        <translation>SQLite</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="737"/>
        <source>Check the connection to the database</source>
        <translation>Verbindung zur Datenbank prüfen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="859"/>
        <source>Overwrite existing data in the database</source>
        <translation>Existierende Daten in der Datenbank überschreiben</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="862"/>
        <source>Overwrite</source>
        <translation>Überschreiben</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="875"/>
        <source>Output path</source>
        <translation>Ausgabepfad</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="888"/>
        <source>Local output directory where downloaded ROMs &amp; CHDs will be created (WARNING: existing files will be overwritten!)</source>
        <translation>Lokaler Ausgabepfad, in dem heruntergeladene ROMs &amp; CHDs erzeugt werden (WARNUNG: existierende Dateien werden überschrieben!)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="901"/>
        <source>Browse local output directory</source>
        <translation>Lokales Ausgabe Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="915"/>
        <source>Enable set rewriter</source>
        <translation>Neuerzeugung der Sets aktivieren</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="946"/>
        <source>Output path for the set rewriter (WARNING: existing files will be overwritten!) -- you should NEVER use one of your primary ROM paths here!!!</source>
        <translation>Ausgabepfad für neu geschriebene Sets (WARNUNG: existierende Dateien werden überschrieben!) -- Du solltest hier NIEMALS eines Deiner primären ROM Verzeichnisse verwenden!!!</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1108"/>
        <source>Browse output path for the set rewriter</source>
        <translation>Ausgabepfad für neu erzeugte Sets auswählen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="972"/>
        <source>Rewrite sets while analyzing them (otherwise sets will only be rewritten on demand / through the context menu)</source>
        <translation>Sets während der Analyse neu schreiben (andernfalls werden Sets nur nach Aufforderung / durch Auswahl im Kontext-Menü neu geschrieben)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="933"/>
        <source>Output directory</source>
        <translation>Ausgabeverzeichnis</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="959"/>
        <source>General settings</source>
        <translation>Allg. Einstellungen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="988"/>
        <source>Create sets that do not need parent sets (otherwise create merged sets, which is recommended)</source>
        <translation>Erzeuge Sets, die von keinem anderen Set abhängen (andernfalls werden zusammengefügte Sets erzeugt, was die empfohlene Einstellung ist) </translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="998"/>
        <source>Rewrite sets only when they are &apos;good&apos; (otherwise, &apos;bad&apos; sets will be included)</source>
        <translation>Sets nur dann neu schreiben, wenn sie &apos;gut&apos; sind (andernfalls werden &apos;schlechte&apos; Sets ebenfalls neu geschrieben)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1001"/>
        <source>Good sets only</source>
        <translation>Nur gute Sets</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1014"/>
        <source>Reproduction type</source>
        <translation>Reproduktionstyp</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1029"/>
        <source>Produce ZIP archived sets (recommended)</source>
        <translation>Sets in ZIP Archiven erzeugen (empfohlen)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1045"/>
        <source>Level </source>
        <translation>Stufe </translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1069"/>
        <source>Unique CRCs</source>
        <translation>Einmalige CRCs</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1125"/>
        <source>Additional ROM path</source>
        <translation>Zusätzlicher ROM Pfad</translation>
    </message>
    <message>
        <source>Specify an additional source ROM path used only by the set rewriter</source>
        <translation type="obsolete">Angabe eines zusätzlichen Quell ROM Pfades, der nur bei der Set Neuerzeugung verwendet wird</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1151"/>
        <source>Browse additional ROM path</source>
        <translation>Zusätzlichen ROM Pfadi auswählen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1166"/>
        <source>Checksum wizard</source>
        <translation>Prüfsummen-Assistent</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1194"/>
        <source>Search</source>
        <translation>Suchen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1264"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1269"/>
        <source>Filename</source>
        <translation>Dateiname</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1274"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1284"/>
        <source>Path</source>
        <translation>Pfad</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1225"/>
        <source>Repair selected &apos;bad&apos; sets using the file from the first selected &apos;good&apos; set (at least 1 good and 1 bad set must be selected)</source>
        <translation>Ausgewählte &apos;schlechte&apos; Sets mit Hilfe des ersten ausgewählten &apos;guten&apos; Sets reparieren (mindestens 1 gutes und 1 schlechtes Set müssen selektiert sein)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1228"/>
        <source>Repair bad sets</source>
        <translation>Schlechte Sets reparieren</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1208"/>
        <source>Analyze all selected sets in order to qualify them</source>
        <translation>Alle ausgewählten Sets für Qualifikation überprüfen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1138"/>
        <source>Specify an additional source ROM path used when the set rewriter is active</source>
        <translation>Angabe eines zusätzlichen Quell ROM Pfades (wird verwendet, wenn die Set Neuerzeugung aktiviert wurde)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1178"/>
        <source>Checksum to be searched</source>
        <translation>Zu suchende Prüfsumme</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1191"/>
        <source>Search for the checksum now</source>
        <translation>Nach der Prüfsumme jetzt suchen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1211"/>
        <source>Analyze selected sets</source>
        <translation>Ausgewählte Sets prüfen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1239"/>
        <source>Search results for the current checksum</source>
        <translation>Suchergebnis für die aktuelle Prüfsumme</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1294"/>
        <source>Level of automation</source>
        <translation>Automatisierungsstufe</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1307"/>
        <source>Choose the level of automated wizard operations</source>
        <translation>Automatisierungsstufe der Wizard Operationen auswählen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1311"/>
        <source>Do nothing automatically</source>
        <translation>Nichts automatisch durchführen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1316"/>
        <source>Automatically select matches</source>
        <translation>Automatische Selektion der Treffer</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1321"/>
        <source>Automatically select matches and analyze sets</source>
        <translation>Automatische Selektion der Treffer und Set-Analyse</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1326"/>
        <source>Automatically select matches, analyze sets and repair bad ones</source>
        <translation>Automatische Selektion der Treffer, Set-Analyse und Reparatur der defekten Sets</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1342"/>
        <source>Select the checksum type</source>
        <translation>Prüfsummen-Typ auswählen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1368"/>
        <source>Close ROMAlyzer</source>
        <translation>ROMAlyzer schließen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1371"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="29"/>
        <source>Shortname of game to be analyzed - wildcards allowed, use space as delimiter for multiple games</source>
        <translation>Kurzname des zu analysierenden Spiels - Wildcards sind erlaubt, Leerzeichen trennen mehrfache Angaben</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="372"/>
        <source>If set, analysis output is appended (otherwise the report is cleared before the analysis)</source>
        <translation>Wenn gesetzt, wird die Ausgabe der Analyse angehängt (andernfalls wird der Bericht vor der Analyse gelöscht)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="785"/>
        <source>Database</source>
        <translation>Datenbank</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="772"/>
        <source>Name of the database on the server</source>
        <translation>Name der Datenbank auf dem Server</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="743"/>
        <location filename="../../romalyzer.cpp" line="2755"/>
        <source>Connection check</source>
        <translation>Verbindung prüfen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1388"/>
        <source>Current ROMAlyzer status</source>
        <translation>Aktueller ROMAlyzer Status</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1391"/>
        <location filename="../../romalyzer.cpp" line="598"/>
        <location filename="../../romalyzer.cpp" line="1046"/>
        <location filename="../../romalyzer.cpp" line="2014"/>
        <location filename="../../romalyzer.cpp" line="2659"/>
        <source>Idle</source>
        <translation>Leerlauf</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1407"/>
        <source>Analysis progress indicator</source>
        <translation>Fortschrittsanzeige für Analyse</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="102"/>
        <location filename="../../romalyzer.ui" line="1279"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="73"/>
        <source>Report</source>
        <translation>Report</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="141"/>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="147"/>
        <source>Analysis log</source>
        <translation>Analyse Log</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="210"/>
        <source>please wait for reload to finish and try again</source>
        <translation>Bitte warte bis die Spieleliste aktualisiert wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="548"/>
        <source>&amp;Stop</source>
        <translation>&amp;Anhalten</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="213"/>
        <source>stopping analysis</source>
        <translation>Beende Analyse</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="217"/>
        <source>starting analysis</source>
        <translation>Beginne Analyse</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="556"/>
        <source>analysis started</source>
        <translation>Analyse gestarted</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="558"/>
        <source>determining list of games to analyze</source>
        <translation>Ermittle Liste der zu analysierenden Spiele</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="578"/>
        <source>Searching games</source>
        <translation>Suche Spiele</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1049"/>
        <source>analysis ended</source>
        <translation>Analyse beendet</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="606"/>
        <source>done (determining list of games to analyze)</source>
        <translation>Fertig (Ermittle Liste der zu analysierenden Spiele)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="607"/>
        <source>%n game(s) to analyze</source>
        <translation>
            <numerusform>%n Spiel zu analysieren</numerusform>
            <numerusform>%n Spiele zu analysieren</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="657"/>
        <source>analyzing &apos;%1&apos;</source>
        <translation>Analysiere &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="659"/>
        <source>Analyzing &apos;%1&apos;</source>
        <translation>Analysiere &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1029"/>
        <source>done (analyzing &apos;%1&apos;)</source>
        <translation>Fertig (Analysiere &apos;%1&apos;)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="1031"/>
        <source>%n game(s) left</source>
        <translation>
            <numerusform>%n Spiel verbleibend</numerusform>
            <numerusform>%n Spiele verbleibend</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="670"/>
        <source>parsing XML data for &apos;%1&apos;</source>
        <translation>Verarbeite XML Daten für &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="677"/>
        <source>done (parsing XML data for &apos;%1&apos;)</source>
        <translation>Fertig (Verarbeite XML Daten für &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="679"/>
        <source>error (parsing XML data for &apos;%1&apos;)</source>
        <translation>Fehler (Verarbeite XML Daten für &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="92"/>
        <source>Game / File</source>
        <translation>Spiel / Datei</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="97"/>
        <source>Merge</source>
        <translation>Merge</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="107"/>
        <source>Emu status</source>
        <translation>Emu Status</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="211"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="375"/>
        <source>Append to report</source>
        <translation>An Bericht anhängen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="112"/>
        <source>File status</source>
        <translation>Datei Status</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="122"/>
        <location filename="../../romalyzer.ui" line="1351"/>
        <source>CRC</source>
        <translation>CRC</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="117"/>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="688"/>
        <source>checking %n file(s) for &apos;%1&apos;</source>
        <translation>
            <numerusform>Prüfe %n Datei für &apos;%1&apos;</numerusform>
            <numerusform>Prüfe %n Dateien für &apos;%1&apos;</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="740"/>
        <location filename="../../romalyzer.cpp" line="834"/>
        <location filename="../../romalyzer.cpp" line="911"/>
        <location filename="../../romalyzer.cpp" line="1997"/>
        <location filename="../../romalyzer.cpp" line="2475"/>
        <location filename="../../romalyzer.cpp" line="2538"/>
        <source>ROM</source>
        <translation>ROM</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="749"/>
        <location filename="../../romalyzer.cpp" line="801"/>
        <location filename="../../romalyzer.cpp" line="915"/>
        <location filename="../../romalyzer.cpp" line="949"/>
        <location filename="../../romalyzer.cpp" line="956"/>
        <source>not found</source>
        <translation>nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="828"/>
        <source>Checksums</source>
        <translation>Prüfsummen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="845"/>
        <source>SIZE </source>
        <translation>GR </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="860"/>
        <source>CRC </source>
        <translation>CRC </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="872"/>
        <source>SHA1 </source>
        <translation>SHA1 </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="897"/>
        <source>MD5 </source>
        <translation>MD5 </translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="941"/>
        <source>interrupted (checking %n file(s) for &apos;%1&apos;)</source>
        <translation>
            <numerusform>Unterbrochen (Prüfe %n Datei für &apos;%1&apos;)</numerusform>
            <numerusform>Unterbrochen (Prüfe %n Dateien für &apos;%1&apos;)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="964"/>
        <source>good / not found</source>
        <translation>gut / nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="883"/>
        <location filename="../../romalyzer.cpp" line="953"/>
        <location filename="../../romalyzer.cpp" line="976"/>
        <location filename="../../romalyzer.cpp" line="2260"/>
        <location filename="../../romalyzer.cpp" line="2452"/>
        <source>good</source>
        <translation>gut</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="126"/>
        <source>Search checksum</source>
        <translation>Prüfsumme suchen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="135"/>
        <source>Rewrite set</source>
        <translation>Set neu schreiben</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="793"/>
        <location filename="../../romalyzer.cpp" line="814"/>
        <location filename="../../romalyzer.cpp" line="883"/>
        <location filename="../../romalyzer.cpp" line="996"/>
        <location filename="../../romalyzer.cpp" line="2261"/>
        <location filename="../../romalyzer.cpp" line="2450"/>
        <source>bad</source>
        <translation>schlecht</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="834"/>
        <source>WARNING: %1 file &apos;%2&apos; loaded from &apos;%3&apos; may be obsolete, should be merged from parent set &apos;%4&apos;</source>
        <translation>WARNUNG: %1 Datei &apos;%2&apos; geladen von &apos;%3&apos; ist möglicherweise überflüssig, sollte vom Eltern-Set &apos;%4&apos; genommen werden</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="1016"/>
        <source>done (checking %n file(s) for &apos;%1&apos;)</source>
        <translation>
            <numerusform>Fertig (Prüfe %n Datei für &apos;%1&apos;)</numerusform>
            <numerusform>Fertig (Prüfe %n Dateien für &apos;%1&apos;)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1182"/>
        <location filename="../../romalyzer.cpp" line="1206"/>
        <source>  logical size: %1 (%2 B)</source>
        <translation>  Logische Größe: %1 (%2 B)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1243"/>
        <source>Verify - %p%</source>
        <translation>Prüfen - %p%</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1245"/>
        <source>CHD manager: verifying and fixing CHD</source>
        <translation>CHD Manager: prüfe und korrigiere CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1248"/>
        <source>CHD manager: verifying CHD</source>
        <translation>CHD Manager: prüfe CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1258"/>
        <source>Update - %p%</source>
        <translation>Aktualisiere - %p%</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1259"/>
        <source>CHD manager: updating CHD (v%1 -&gt; v%2)</source>
        <translation>CHD Manager: aktualisiere CHD (v%1 -&gt; v%2)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1264"/>
        <location filename="../../romalyzer.cpp" line="1276"/>
        <source>CHD manager: using header checksums for CHD verification</source>
        <translation>CHD Manager: verwende Header Prüfsummen zur CHD Verifikation</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1284"/>
        <source>CHD manager: no header checksums available for CHD verification</source>
        <translation>CHD Manager: keine Header Prüfsummen für CHD Verifikation verfügbar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1312"/>
        <source>CHD manager: terminating external process</source>
        <translation>CHD Manager: terminiere externen Prozess</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1333"/>
        <location filename="../../romalyzer.cpp" line="1335"/>
        <source>CHD manager: CHD file integrity is good</source>
        <translation>CHD Manager: CHD Datei-Integrität ist gut</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1337"/>
        <source>CHD manager: WARNING: CHD file integrity is bad</source>
        <translation>CHD Manager: CHD Datei-Integrität ist schlecht</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1341"/>
        <location filename="../../romalyzer.cpp" line="1353"/>
        <source>CHD manager: using CHD v%1 header checksums for CHD verification</source>
        <translation>CHD Manager: verwende CHD v%1 Header Prüfsummen zur CHD Verifikation</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1361"/>
        <source>CHD manager: WARNING: no header checksums available for CHD verification</source>
        <translation>CHD Manager: WARNUNG: keine Header Prüfsummen für CHD Verifikation verfügbar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1368"/>
        <source>CHD manager: replacing CHD</source>
        <translation>CHD Manager: ersetze CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1370"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1376"/>
        <source>CHD manager: CHD replaced</source>
        <translation>CHD Manager: CHD ersetzt</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1379"/>
        <source>CHD manager: FATAL: failed to replace CHD -- updated CHD preserved as &apos;%1&apos;, please copy it to &apos;%2&apos; manually!</source>
        <translation>CHD Manager: FATAL: Ersetzen der CHD Datei fehlgeschlagen -- erhalte aktualisierte CHD Datei &apos;%1&apos;, bitte manuell nach &apos;%2&apos; kopieren!</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1383"/>
        <source>CHD manager: cleaning up</source>
        <translation>CHD Manager: räume auf</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1395"/>
        <location filename="../../romalyzer.cpp" line="1407"/>
        <source>using CHD v%1 header checksums for CHD verification</source>
        <translation>verwende CHD v%1 Header Prüfsummen zur CHD Verifikation</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1415"/>
        <source>WARNING: no header checksums available for CHD verification</source>
        <translation>WARNUNG: keine Header Prüfsummen für CHD Verifikation verfügbar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1461"/>
        <location filename="../../romalyzer.cpp" line="1592"/>
        <source>WARNING: found &apos;%1&apos; but can&apos;t read from it - check permission</source>
        <translation>WARNUNG: &apos;%1&apos; wurde gefunden, kann aber nicht zum Lesen geöffnet werden - Berechtigungen prüfen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1499"/>
        <source>WARNING: unable to identify &apos;%1&apos; from &apos;%2&apos; by CRC &apos;%3&apos;</source>
        <translation>WARNUNG: kann &apos;%1&apos; aus &apos;%2&apos; nicht per CRC &apos;%3&apos; identifizieren</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1496"/>
        <source>WARNING: unable to identify &apos;%1&apos; from &apos;%2&apos; by CRC (no dump exists / CRC unknown)</source>
        <translation>WARNUNG: kann &apos;%1&apos; aus &apos;%2&apos; nicht per CRC (kein existierender Dump / CRC unbekannt)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="142"/>
        <source>Copy to clipboard</source>
        <translation>In Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="822"/>
        <location filename="../../romalyzer.cpp" line="917"/>
        <source>no dump</source>
        <translation>kein Dump</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="968"/>
        <source>good / no dump / skipped</source>
        <translation>gut / kein Dump / übersprungen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="970"/>
        <source>good / no dump</source>
        <translation>gut / kein Dump</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="988"/>
        <source>bad / no dump / skipped</source>
        <translation>schlecht / kein Dump / übersprungen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="990"/>
        <source>bad / no dump</source>
        <translation>schlecht / kein Dump</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1519"/>
        <source>loading &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos; as &apos;%4&apos;%5</source>
        <translation>Lade &apos;%1&apos; mit CRC &apos;%2&apos; von &apos;%3&apos; als &apos;%4&apos;%5</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1586"/>
        <source>WARNING: unable to decompress &apos;%1&apos; from &apos;%2&apos; - check file integrity</source>
        <translation>WARNUNG: kann &apos;%1&apos; nicht aus &apos;%2&apos; entpacken - Datei Integrität prüfen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1590"/>
        <source>WARNING: found &apos;%1&apos; but can&apos;t open it for decompression - check file integrity</source>
        <translation>WARNUNG: &apos;%1&apos; wurde gefunden, kann aber nicht zur Dekompression geöffnet werden - Datei Integrität prüfen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1780"/>
        <source>Choose output directory</source>
        <translation>Ausgabe-Verzeichnis wählen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1794"/>
        <source>Choose additional ROM path</source>
        <translation>Zusätzlichen ROM Pfadi auswählen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1940"/>
        <source>Checksum search</source>
        <translation>Suche nach Prüfsummen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2079"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path is not a directory</source>
        <translation>Set Neuerzeugung: WARNUNG: kann Set &apos;%1&apos; nicht neu schreiben, der angegebene Ausgabepfad ist kein Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2083"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path is not writable</source>
        <translation>Set Neuerzeugung: WARNUNG: kann Set &apos;%1&apos; nicht neu schreiben, der angegebene Ausgabepfad ist nicht beschreibbar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2088"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path does not exist</source>
        <translation>Set Neuerzeugung: WARNUNG: kann Set &apos;%1&apos; nicht neu schreiben, der angegebene Ausgabepfad existiert nicht</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2092"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path is empty</source>
        <translation>Set Neuerzeugung: WARNUNG: kann Set &apos;%1&apos; nicht neu schreiben, es wurde kein Ausgabepfad angegeben</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2106"/>
        <source>space-efficient</source>
        <translation>platzsparendes</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2107"/>
        <source>self-contained</source>
        <translation>in sich abgeschlossenes</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2109"/>
        <source>set rewriter: rewriting %1 set &apos;%2&apos; to &apos;%3&apos;</source>
        <translation>Set Neuerzeugung: erzeuge ein %1 Set für &apos;%2&apos; in &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2125"/>
        <source>set rewriter: skipping &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos; as &apos;%4&apos;</source>
        <translation>Set Neuerzeugung: überspringe &apos;%1&apos; mit CRC &apos;%2&apos; von &apos;%3&apos; als &apos;%4&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2136"/>
        <source>set rewriter: FATAL: can&apos;t load &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos;, aborting</source>
        <translation>Set Neuerzeugung: FATAL: kann &apos;%1&apos; mit CRC &apos;%2&apos; nicht von &apos;%3&apos; laden, breche ab</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2139"/>
        <source>set rewriter: WARNING: can&apos;t load &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos;, ignoring this file</source>
        <translation>Set Neuerzeugung: WARNUNG: kann &apos;%1&apos; mit CRC &apos;%2&apos; nicht von &apos;%3&apos; laden, ignoriere diese Datei</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2166"/>
        <source>set rewriter: writing new %1 set &apos;%2&apos; in &apos;%3&apos;</source>
        <translation>Set Neuerzeugung: schreibe neues %1 Set für &apos;%2&apos; nach &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2167"/>
        <source>Writing &apos;%1&apos; - %2</source>
        <translation>Schreibe &apos;%1&apos; - %2</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2169"/>
        <source>set rewriter: new %1 set &apos;%2&apos; in &apos;%3&apos; successfully created</source>
        <translation>Set Neuerzeugung: neues %1 Set für &apos;%2&apos; erfolgreich in &apos;%3&apos; erzeugt</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2171"/>
        <source>set rewriter: FATAL: failed to create new %1 set &apos;%2&apos; in &apos;%3&apos;</source>
        <translation>Set Neuerzeugung: FATAL: die Erzeugung des neuen %1 Sets für &apos;%2&apos; in &apos;%3&apos; ist fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2181"/>
        <source>set rewriter: done (rewriting %1 set &apos;%2&apos; to &apos;%3&apos;)</source>
        <translation>Set Neuerzeugung: fertig (erzeuge ein %1 Set für &apos;%2&apos; in &apos;%3&apos;)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="2464"/>
        <source>checksum wizard: repairing %n bad set(s)</source>
        <translation>
            <numerusform>Prüfsummen-Assistent: repariere %n schlechtes Set</numerusform>
            <numerusform>Prüfsummen-Assistent: repariere %n schlechte Sets</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2470"/>
        <source>checksum wizard: using %1 file &apos;%2&apos; from &apos;%3&apos; as repro template</source>
        <translation>Prüfsummen-Assistent: verwende %1 Datei &apos;%2&apos; von &apos;%3&apos; als Repro-Vorlage</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2493"/>
        <source>checksum wizard: successfully identified &apos;%1&apos; from &apos;%2&apos; by CRC, filename in ZIP archive is &apos;%3&apos;</source>
        <translation>Prüfsummen-Assistent: &apos;%1&apos; aus &apos;%2&apos; erfolgreich mittels CRC identifiziert, Dateiname im ZIP Archiv ist &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2502"/>
        <source>checksum wizard: template data loaded, uncompressed size = %1</source>
        <translation>Prüfsummen-Assistent: Vorlagedaten geladen, unkomprimierte Größe = %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2506"/>
        <source>checksum wizard: FATAL: unable to identify &apos;%1&apos; from &apos;%2&apos; by CRC</source>
        <translation>Prüfsummen-Assistent: FATAL: kann &apos;%1&apos; aus &apos;%2&apos; nicht mittels CRC identifizieren</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2516"/>
        <location filename="../../romalyzer.cpp" line="2637"/>
        <source>checksum wizard: sorry, no support for regular files yet</source>
        <translation>Prüfsummen-Assistent: sorry, herkömmliche Dateien werden noch nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2521"/>
        <location filename="../../romalyzer.cpp" line="2642"/>
        <source>checksum wizard: sorry, no support for CHD files yet</source>
        <translation>Prüfsummen-Assistent: sorry, CHD Dateien werden noch nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2547"/>
        <source>checksum wizard: target ZIP exists, loading complete data and structure</source>
        <translation>Prüfsummen-Assistent: das ZIP Ziel existiert bereits, lade Daten und Struktur vollständig</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2549"/>
        <source>checksum wizard: target ZIP successfully loaded</source>
        <translation>Prüfsummen-Assistent: ZIP Ziel erfolgreich geladen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2551"/>
        <source>checksum wizard: an entry with the CRC &apos;%1&apos; already exists, recreating the ZIP from scratch to replace the bad file</source>
        <translation>Prüfsummen-Assistent: ein Eintrag mit dem CRC &apos;%1&apos; existiert bereits, erzeuge das ZIP Ziel komplett neu um das schlechte File auszutauschen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2554"/>
        <source>checksum wizard: backup file &apos;%1&apos; successfully created</source>
        <translation>Prüfsummen-Assistent: Backup-Datei &apos;%1&apos; erfolgreich erzeugt</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2558"/>
        <source>checksum wizard: FATAL: failed to create backup file &apos;%1&apos;, aborting</source>
        <translation>Prüfsummen-Assistent: FATAL: die Backup-Datei &apos;%1&apos; konnte nicht angelegt werden, breche ab</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2562"/>
        <source>checksum wizard: no entry with the CRC &apos;%1&apos; was found, adding the missing file to the existing ZIP</source>
        <translation>Prüfsummen-Assistent: es wurde kein Eintrag mit dem CRC &apos;%1&apos; gefunden, füge die fehlende Datei dem ZIP Ziel hinzu</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2565"/>
        <source>checksum wizard: FATAL: failed to load target ZIP, aborting</source>
        <translation>Prüfsummen-Assistent: FATAL: ZIP Ziel kann nicht geladen werden, breche ab</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2570"/>
        <source>checksum wizard: the target ZIP does not exist, creating a new ZIP with just the missing file</source>
        <translation>Prüfsummen-Assistent: das ZIP Ziel existiert nicht, erzeuge ein neues ZIP, das nur die fehlende Datei enthält</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2431"/>
        <location filename="../../romalyzer.cpp" line="2628"/>
        <source>Created by QMC2 v%1 (%2)</source>
        <translation>Erzeugt durch QMC2 v%1 (%2)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2102"/>
        <source>Reading &apos;%1&apos; - %2</source>
        <translation>Lese &apos;%1&apos; - %2</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2129"/>
        <source>set rewriter: loading &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos; as &apos;%4&apos;</source>
        <translation>Set Neuerzeugung: lade &apos;%1&apos; mit CRC &apos;%2&apos; von &apos;%3&apos; als &apos;%4&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2156"/>
        <source>set rewriter: removing redundant file &apos;%1&apos; with CRC &apos;%2&apos; from output data</source>
        <translation>Set Neuerzeugung: entferne redundante Datei &apos;%1&apos; mit CRC &apos;%2&apos; von den Ausgabedaten</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2175"/>
        <source>set rewriter: INFORMATION: no output data available, thus not rewriting set &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>Set Neuerzeugung: INFORMATION: es sind keine Ausgabedaten verfügbar, daher wird das Set für &apos;%1&apos; nicht nach &apos;%2&apos; geschrieben</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2405"/>
        <source>set rewriter: deflating &apos;%1&apos; (uncompressed size: %2)</source>
        <translation>Set Neuerzeugung: komprimiere &apos;%1&apos; (Größe: %2)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2425"/>
        <source>set rewriter: WARNING: failed to deflate &apos;%1&apos;</source>
        <translation>Set Neuerzeugung: WARNUNG: konnte &apos;%1&apos; nicht schreiben</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2651"/>
        <source>repair failed</source>
        <translation>Reparatur fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2653"/>
        <source>checksum wizard: FATAL: failed to repair %1 file &apos;%2&apos; in &apos;%3&apos; from repro template</source>
        <translation>Prüfsummen-Assistent: FATAL: %1 Datei &apos;%2&apos; in &apos;%3&apos; konnte nicht repariert werden</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2663"/>
        <source>checksum wizard: FATAL: can&apos;t find any good set</source>
        <translation>Prüfsummen-Assistent: FATAL: kann keine guten Sets finden</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="2665"/>
        <source>checksum wizard: done (repairing %n bad set(s))</source>
        <translation>
            <numerusform>Prüfsummen-Assistent: fertig (repariere %n schlechtes Set)</numerusform>
            <numerusform>Prüfsummen-Assistent: fertig (repariere %n schlechte Sets)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2739"/>
        <source>database connection check failed -- errorNumber = %1, errorText = &apos;%2&apos;</source>
        <translation>Datenbankverbindung konnte nicht hergestellt werden -- Fehlernummer = %1, Fehlertext = &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="395"/>
        <source>Calculate CRC-32 checksum</source>
        <translation>CRC-32 Prüfsumme berechnen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="398"/>
        <source>Calculate CRC</source>
        <translation>CRC berechnen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="382"/>
        <source>Automatically scroll to the currently analyzed game</source>
        <translation>Automatisch zum aktuell analysierten Spiel scrollen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="385"/>
        <source>Auto scroll</source>
        <translation>Auto Scrollen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="444"/>
        <source>Calculate SHA1 hash</source>
        <translation>SHA1 Prüfsumme berechnen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="447"/>
        <source>Calculate SHA1</source>
        <translation>SHA1 berechnen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="421"/>
        <source>Automatically expand file info</source>
        <translation>Automatisch Datei Information expandieren</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="424"/>
        <source>Expand file info</source>
        <translation>Datei Info expandieren</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="411"/>
        <source>Calculate MD5</source>
        <translation>MD5 berechnen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="434"/>
        <source>Automatically expand checksums</source>
        <translation>Automatisch Prüfsummen expandieren</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="437"/>
        <source>Expand checksums</source>
        <translation>Prüfsummen expandieren</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="408"/>
        <source>Calculate MD5 hash</source>
        <translation>MD5 Prüfsumme berechnen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="984"/>
        <source>bad / not found</source>
        <translation>schlecht / nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1458"/>
        <source>WARNING: found &apos;%1&apos; but can&apos;t read from it although permissions seem okay - check file integrity</source>
        <translation>WARNUNG: &apos;%1&apos; wurde gefunden, kann aber nicht zum Lesen geöffnet werden, obwohl die Zugrifssrechte okay sind - Datei Integrität prüfen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="231"/>
        <source>pausing analysis</source>
        <translation>Pausiere Analyse</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="234"/>
        <source>resuming analysis</source>
        <translation>Nehme Analyse wieder auf</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="56"/>
        <location filename="../../romalyzer.cpp" line="235"/>
        <location filename="../../romalyzer.cpp" line="551"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="619"/>
        <source>analysis paused</source>
        <translation>Analyse pausiert</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="620"/>
        <source>&amp;Resume</source>
        <translation>&amp;Weiter</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="623"/>
        <source>Paused</source>
        <translation>Pausiert</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="53"/>
        <source>Pause / resume active analysis</source>
        <translation>Pausieren / Wiederaufnehmen der aktiven Analyse</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1141"/>
        <source>loading &apos;%1&apos;%2</source>
        <translation>Lade &apos;%1&apos;%2</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1141"/>
        <location filename="../../romalyzer.cpp" line="1519"/>
        <source> (merged)</source>
        <translation> (vererbt)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1426"/>
        <source>File I/O progress indicator</source>
        <translation>Fortschrittsanzeige für Datei Ein-/Ausgabe</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="720"/>
        <location filename="../../romalyzer.cpp" line="772"/>
        <location filename="../../romalyzer.cpp" line="775"/>
        <location filename="../../romalyzer.cpp" line="913"/>
        <source>skipped</source>
        <translation>übersprungen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="962"/>
        <source>good / not found / skipped</source>
        <translation>gut / nicht gefunden / übersprungen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="974"/>
        <source>good / skipped</source>
        <translation>gut / übersprungen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="982"/>
        <source>bad / not found / skipped</source>
        <translation>schlecht / nicht gefunden / übersprungen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="994"/>
        <source>bad / skipped</source>
        <translation>schlecht / übersprungen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="498"/>
        <location filename="../../romalyzer.cpp" line="1711"/>
        <location filename="../../romalyzer.cpp" line="1725"/>
        <source> MB</source>
        <translation> MB</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>none</source>
        <translation>keine</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="84"/>
        <source>Automatically scroll to the currently analyzed machine</source>
        <translation>Automatisch zur aktuell analysierten Maschine scrollen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="85"/>
        <source>Shortname of machine to be analyzed - wildcards allowed, use space as delimiter for multiple machines</source>
        <translation>Kurzname der zu analysierenden Machine - Wildcards sind erlaubt, Leerzeichen trennen mehrfache Angaben</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>zlib</source>
        <translation>zlib</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>zlib+</source>
        <translation>zlib+</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>A/V codec</source>
        <translation>A/V Codec</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="640"/>
        <source>report limit reached, removing %n set(s) from the report</source>
        <translation>
            <numerusform>Berichtgrenze erreicht, entferne das älteste Set aus dem Bericht</numerusform>
            <numerusform>Berichtgrenze erreicht, entferne %n Sets aus dem Bericht</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="778"/>
        <source>error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="911"/>
        <source>WARNING: %1 file &apos;%2&apos; loaded from &apos;%3&apos; has incorrect / unexpected checksums</source>
        <translation>WARNUNG: %1 Datei &apos;%2&apos; geladen von &apos;%3&apos; hat falsche / unerwartete Prüfsummen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1164"/>
        <source>CHD header information:</source>
        <translation>CHD Header Information:</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1168"/>
        <source>  version: %1</source>
        <translation>  Version: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1169"/>
        <location filename="../../romalyzer.cpp" line="1377"/>
        <source>CHD v%1</source>
        <translation>CHD v%1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1174"/>
        <location filename="../../romalyzer.cpp" line="1198"/>
        <source>  compression: %1</source>
        <translation>  Kompression: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1178"/>
        <location filename="../../romalyzer.cpp" line="1202"/>
        <source>  number of total hunks: %1</source>
        <translation>  Gesamtzahl Abschnitte: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1180"/>
        <location filename="../../romalyzer.cpp" line="1204"/>
        <source>  number of bytes per hunk: %1</source>
        <translation>  Bytes pro Abschnitt: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1184"/>
        <source>  MD5 checksum: %1</source>
        <translation>  MD5 Prüfsumme: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1186"/>
        <location filename="../../romalyzer.cpp" line="1208"/>
        <source>  SHA1 checksum: %1</source>
        <translation>  SHA1 Prüfsumme: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1219"/>
        <source>only CHD v3 and v4 headers supported -- rest of header information skipped</source>
        <translation>Nur CHD v3 und v4 Header unterstützt -- der Rest der Header Informationen wird übersprungen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1423"/>
        <source>WARNING: can&apos;t read CHD header information</source>
        <translation>WARNUNG: kann CHD Header Information nicht lesen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1570"/>
        <source>WARNING: the CRC for &apos;%1&apos; from &apos;%2&apos; is unknown to the emulator, the set rewriter will use the recalculated CRC &apos;%3&apos; to qualify the file</source>
        <translation>WARNUNG: der CRC für &apos;%1&apos; von &apos;%2&apos; ist dem Emulator unbekannt, für die Set-Neuerzeugung wird der berechnete CRC &apos;%3&apos; verwendet</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1572"/>
        <source>WARNING: unable to determine the CRC for &apos;%1&apos; from &apos;%2&apos;, the set rewriter will NOT store this file in the new set</source>
        <translation>WARNUNG: der CRC für &apos;%1&apos; von &apos;%2&apos; konnte nicht ermittelt werden, diese Datei wird bei der Set-Neuerzeugung NICHT mit abgespeichert</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1708"/>
        <location filename="../../romalyzer.cpp" line="1722"/>
        <source> KB</source>
        <translation> KB</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1714"/>
        <location filename="../../romalyzer.cpp" line="1728"/>
        <source> GB</source>
        <translation> GB</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1717"/>
        <source> TB</source>
        <translation> TB</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="179"/>
        <source>&amp;Forward</source>
        <translation>&amp;Vorwärts</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="196"/>
        <source>&amp;Backward</source>
        <translation>&amp;Rückwärts</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="460"/>
        <source>Select game</source>
        <translation>Spiel selektieren</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="169"/>
        <source>Search string</source>
        <translation>Such-Zeichenkette</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="176"/>
        <source>Search string forward</source>
        <translation>Zeichenkette vorwärts suchen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="193"/>
        <source>Search string backward</source>
        <translation>Zeichenkette rückwärts suchen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="230"/>
        <source>Enable CHD manager (may be slow)</source>
        <translation>CHD Manager aktivieren (langsam)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="239"/>
        <source>CHD manager (chdman)</source>
        <translation>CHD Manager (chdman)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="252"/>
        <source>CHD manager executable file (read and execute)</source>
        <translation>Ausführbare CHD Manager Datei (lesen und ausführen)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="265"/>
        <source>Browse CHD manager executable file</source>
        <translation>Ausführbare CHD Manager Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="276"/>
        <source>Temporary working directory</source>
        <translation>Temporäres Arbeitsverzeichnis</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="315"/>
        <source>Verify CHDs through &apos;chdman -verify&apos;</source>
        <translation>CHDs überprüfen mittels &apos;chdman -verify&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="318"/>
        <source>Verify CHDs</source>
        <translation>CHDs prüfen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="328"/>
        <source>Also try to fix CHDs using &apos;chdman -verifyfix&apos;</source>
        <translation>Gleichzeitig versuchen CHDs zu korrigieren mittels &apos;chdman -verifyfix&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="331"/>
        <source>Fix CHDs</source>
        <translation>CHDs korrigieren</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="338"/>
        <source>Try to update CHDs if their header indicates an older version (&apos;chdman -update&apos;)</source>
        <translation>CHDs aktualisieren falls der Header anzeigt, dass die Version veraltet ist (&apos;chdman -update&apos;)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="341"/>
        <source>Update CHDs</source>
        <translation>CHDs aktualisieren</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="527"/>
        <source>Maximum number of lines in log (0 = no limit)</source>
        <translation>Maximale Anzahl von Zeilen im Log (0 = keine Beschränkung)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="533"/>
        <source> lines</source>
        <translation> Zeilen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="640"/>
        <source>Database server port (0 = default)</source>
        <translation>Port des Datenbank-Servers (0 = Standard)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="756"/>
        <source>Password used to access the database (WARNING: stored passwords are &lt;u&gt;weakly&lt;/u&gt; encrypted!)</source>
        <translation>Passwort für den Datenbank-Zugriff (WARNUNG: gespeicherte Passwörter sind nur &lt;i&gt;mäßig&lt;/i&gt; verschlüsselt!)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="811"/>
        <source>Mode</source>
        <translation>Modus</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="843"/>
        <source>Upload</source>
        <translation>Upload</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="975"/>
        <source>Rewrite while analyzing</source>
        <translation>Während Analyse</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="991"/>
        <source>Self-contained</source>
        <translation>Unabhängige Sets</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1032"/>
        <source>ZIPs</source>
        <translation>ZIPs</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1042"/>
        <source>Select the ZIP compression level (0 = lowest / fastest, 9 = highest / slowest)</source>
        <translation>ZIP Kompressionsstufe auswählen (0 = niedrigste / schnellste, 9 = höchste / langsamste)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1066"/>
        <source>When a set contains multiple files with the same CRC, should the produced ZIP include all files individually or just the first one (which is actually sufficient)?</source>
        <translation>Wenn ein Set mehrere Dateien mit demselben CRC enthält, soll das erzeugte ZIP dann alle einzelnen Dateien enthalten oder nur die erste (was eigentlich ausreichend ist)?</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1079"/>
        <source>Produce sets in individual sub-directories (not recommended -- and not supported yet!)</source>
        <translation>Sets in individuellen Unterverzeichnisen erzeugen (nicht empfohlen -- und derzeit auch nicht unterstützt!)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1082"/>
        <source>Directories</source>
        <translation>Verzeichnisse</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="495"/>
        <location filename="../../romalyzer.ui" line="530"/>
        <location filename="../../romalyzer.ui" line="565"/>
        <source>unlimited</source>
        <translation>unbegrenzt</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1166"/>
        <source>  tag: %1</source>
        <translation>  Markierung: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1189"/>
        <source>  parent CHD&apos;s MD5 checksum: %1</source>
        <translation>  MD5 Prüfsumme des Eltern CHDs: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1191"/>
        <location filename="../../romalyzer.cpp" line="1211"/>
        <source>  parent CHD&apos;s SHA1 checksum: %1</source>
        <translation>  SHA1 Prüfsumme des Eltern CHDs: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>  flags: %1, %2</source>
        <translation>  Flags: %1, %2</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>has parent</source>
        <translation>Hat Eltern CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>no parent</source>
        <translation>Kein Eltern CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>allows writes</source>
        <translation>Schreiben erlaubt</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>read only</source>
        <translation>Nur lesen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1464"/>
        <source>WARNING: CHD file &apos;%1&apos; not found</source>
        <translation>WARNUNG: CHD Datei &apos;%1&apos; nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="834"/>
        <location filename="../../romalyzer.cpp" line="911"/>
        <location filename="../../romalyzer.cpp" line="1106"/>
        <location filename="../../romalyzer.cpp" line="1994"/>
        <source>CHD</source>
        <translation>CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="82"/>
        <source>Select machine</source>
        <translation>Maschine selektieren</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="83"/>
        <source>Select machine in machine list if selected in analysis report?</source>
        <translation>Maschine in Machinenliste auswählen, wenn im Analyse-Report selektiert?</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="457"/>
        <source>Select game in game list if selected in analysis report?</source>
        <translation>Spiel in Spieleliste auswählen, wenn in Analyse-Report selektiert?</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="81"/>
        <source>Machine / File</source>
        <translation>Maschine / Datei</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="560"/>
        <source>determining list of machines to analyze</source>
        <translation>Ermittle Liste der zu analysierenden Maschinen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="580"/>
        <source>Searching machines</source>
        <translation>Suche Maschinen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="609"/>
        <source>done (determining list of machines to analyze)</source>
        <translation>Fertig (Ermittle Liste der zu analysierenden Maschinen)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="610"/>
        <source>%n machine(s) to analyze</source>
        <translation>
            <numerusform>%n Maschine zu analysieren</numerusform>
            <numerusform>%n Maschinen zu analysieren</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="1033"/>
        <source>%n machine(s) left</source>
        <translation>
            <numerusform>%n Maschine verbleibend</numerusform>
            <numerusform>%n Maschinen verbleibend</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1049"/>
        <source>elapsed time = %1</source>
        <translation>benötigte Zeit  = %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="39"/>
        <source>Start / stop analysis</source>
        <translation>Analyse starten / stoppen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1214"/>
        <source>  raw SHA1 checksum: %1</source>
        <translation>SHA1 Prüfsumme der Rohdaten: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1132"/>
        <source>size of &apos;%1&apos; is greater than allowed maximum -- skipped</source>
        <translation>Die Datei &apos;%1&apos; ist größer als das erlaubte Maximum - übersprungen</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1510"/>
        <source>size of &apos;%1&apos; from &apos;%2&apos; is greater than allowed maximum -- skipped</source>
        <translation>Die Datei &apos;%1&apos; aus &apos;%2&apos; ist größer als das erlaubte Maximum - übersprungen</translation>
    </message>
</context>
<context>
    <name>ROMStatusExporter</name>
    <message>
        <location filename="../../romstatusexport.ui" line="15"/>
        <source>ROM status export</source>
        <translation>ROM Status Export</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="23"/>
        <location filename="../../romstatusexport.ui" line="36"/>
        <source>Select output format</source>
        <translation>Ausgabe Format wählen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="40"/>
        <source>ASCII</source>
        <translation>ASCII</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="45"/>
        <source>CSV</source>
        <translation>CSV</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="339"/>
        <source>Browse ASCII export file</source>
        <translation>ASCII Export Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="342"/>
        <source>ASCII file</source>
        <translation>ASCII Datei</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="362"/>
        <source>ASCII export file</source>
        <translation>ASCII Export Datei</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="369"/>
        <source>Column width</source>
        <translation>Spaltenbreite</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="385"/>
        <source>Maximum column width for ASCII export (0 = unlimited)</source>
        <translation>Maximale Spaltenbreite für ASCII Export (0 = unbegrenzt)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="388"/>
        <source>unlimited</source>
        <translation>unbegrenzt</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="444"/>
        <source>Browse CSV export file</source>
        <translation>CSV Export Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="447"/>
        <source>CSV file</source>
        <translation>CSV Datei</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="467"/>
        <source>CSV export file</source>
        <translation>CSV Export Datei</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="474"/>
        <source>Separator</source>
        <translation>Trennzeichen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="515"/>
        <source>Field separator for CSV export</source>
        <translation>Feld-Trennzeichen für CSV Export</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="518"/>
        <source>;</source>
        <translation>;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="531"/>
        <source>Delimiter</source>
        <translation>Begrenzungzeichen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="559"/>
        <source>Text delimiter for CSV export</source>
        <translation>Text-Begrenzungszeichen für CSV Export</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="562"/>
        <source>&quot;</source>
        <translation>&quot;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="81"/>
        <source>Export ROM state C (correct)?</source>
        <translation>Exportiere ROM Status K (korrekt)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="98"/>
        <source>Export ROM state M (mostly correct)?</source>
        <translation>Exportiere ROM Status B (beinahe korrekt)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="115"/>
        <source>Export ROM state I (incorrect)?</source>
        <translation>Exportiere ROM Status I (inkorrekt)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="132"/>
        <source>Export ROM state N (not found)?</source>
        <translation>Exportiere ROM Status N (nicht gefunden)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="149"/>
        <source>Export ROM state U (unknown)?</source>
        <translation>Exportiere ROM Status U (unbekannt)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="269"/>
        <source>Include some header information in export</source>
        <translation>Einige Kopfzeilen dem Export hinzufügen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="272"/>
        <source>Include header</source>
        <translation>Inkl. Kopfzeilen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="282"/>
        <source>Include statistical overview of the ROM state in export</source>
        <translation>Statistische Übersicht des aktuellen ROM Status dem Export hinzufügen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="285"/>
        <source>Include ROM statistics</source>
        <translation>Inkl. ROM Statistik</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="728"/>
        <source>Close ROM status export</source>
        <translation>ROM Status Report schließen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="731"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="714"/>
        <source>Export now!</source>
        <translation>Jetzt exportieren!</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="742"/>
        <source>Export progress indicator</source>
        <translation>Fortschrittsanzeiger für Export</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="748"/>
        <source>%p%</source>
        <translation>%p%</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="717"/>
        <source>&amp;Export</source>
        <translation>&amp;Exportieren</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="72"/>
        <source>Exported ROM states</source>
        <translation>Exportierte ROM Statis</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="50"/>
        <source>HTML</source>
        <translation>HTML</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="295"/>
        <source>Export to the system clipboard instead of a file</source>
        <translation>Export in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="298"/>
        <source>Export to clipboard</source>
        <translation>Zwischenablage</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="308"/>
        <source>Overwrite existing files without asking what to do</source>
        <translation>Existierende Dateien ohne zu fragen überschreiben</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="311"/>
        <source>Overwrite blindly</source>
        <translation>Überschreiben</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="618"/>
        <source>Browse HTML export file</source>
        <translation>HTML Export Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="621"/>
        <source>HTML file</source>
        <translation>HTML Datei</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="641"/>
        <source>HTML export file</source>
        <translation>HTML Export Datei</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="181"/>
        <source>Sort criteria</source>
        <translation>Sortier-Kriterium</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="194"/>
        <source>Select sort criteria</source>
        <translation>Sortier-Kriterium auswählen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="198"/>
        <source>Game description</source>
        <translation>Spiel-Beschreibung</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="203"/>
        <source>ROM state</source>
        <translation>ROM-Status</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="208"/>
        <location filename="../../romstatusexport.cpp" line="225"/>
        <location filename="../../romstatusexport.cpp" line="334"/>
        <location filename="../../romstatusexport.cpp" line="555"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Year</source>
        <translation>Jahr</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="213"/>
        <location filename="../../romstatusexport.cpp" line="224"/>
        <location filename="../../romstatusexport.cpp" line="335"/>
        <location filename="../../romstatusexport.cpp" line="556"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Manufacturer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="218"/>
        <source>Game name</source>
        <translation>Spielname</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="231"/>
        <source>Sort order</source>
        <translation>Sortierreihenfolge</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="244"/>
        <source>Select sort order</source>
        <translation>Sortierreihenfolge auswählen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="248"/>
        <source>Ascending</source>
        <translation>Aufsteigend</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="257"/>
        <source>Descending</source>
        <translation>Absteigend</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="907"/>
        <source>Choose ASCII export file</source>
        <translation>ASCII Export Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="907"/>
        <location filename="../../romstatusexport.cpp" line="919"/>
        <location filename="../../romstatusexport.cpp" line="931"/>
        <source>All files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="919"/>
        <source>Choose CSV export file</source>
        <translation>CSV Export Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="931"/>
        <source>Choose HTML export file</source>
        <translation>HTML Export Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="134"/>
        <location filename="../../romstatusexport.cpp" line="440"/>
        <location filename="../../romstatusexport.cpp" line="658"/>
        <source>Confirm</source>
        <translation>Bestätigen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="135"/>
        <location filename="../../romstatusexport.cpp" line="441"/>
        <location filename="../../romstatusexport.cpp" line="659"/>
        <source>Overwrite existing file?</source>
        <translation>Existierende Datei überschreiben?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="150"/>
        <source>exporting ROM status in ASCII format to &apos;%1&apos;</source>
        <translation>Exportiere ROM Status im ASCII Format nach &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="152"/>
        <source>WARNING: can&apos;t open ASCII export file &apos;%1&apos; for writing, please check permissions</source>
        <translation>WARNUNG: kann ASCII Export Datei &apos;%1&apos; nicht zum Schreiben öffnen, bitte Zugriffsrechte prüfen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="158"/>
        <source>exporting ROM status in ASCII format to clipboard</source>
        <translation>Exportiere ROM Status im ASCII Format zur Zwischenablage</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="196"/>
        <location filename="../../romstatusexport.cpp" line="271"/>
        <location filename="../../romstatusexport.cpp" line="385"/>
        <location filename="../../romstatusexport.cpp" line="488"/>
        <location filename="../../romstatusexport.cpp" line="622"/>
        <location filename="../../romstatusexport.cpp" line="711"/>
        <location filename="../../romstatusexport.cpp" line="873"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="169"/>
        <location filename="../../romstatusexport.cpp" line="198"/>
        <location filename="../../romstatusexport.cpp" line="490"/>
        <location filename="../../romstatusexport.cpp" line="717"/>
        <source>Emulator</source>
        <translation>Emulator</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="170"/>
        <location filename="../../romstatusexport.cpp" line="199"/>
        <location filename="../../romstatusexport.cpp" line="491"/>
        <location filename="../../romstatusexport.cpp" line="719"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="171"/>
        <location filename="../../romstatusexport.cpp" line="200"/>
        <location filename="../../romstatusexport.cpp" line="492"/>
        <location filename="../../romstatusexport.cpp" line="721"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
    <message>
        <source>Total games</source>
        <translation type="obsolete">Spiele gesamt</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="173"/>
        <location filename="../../romstatusexport.cpp" line="208"/>
        <location filename="../../romstatusexport.cpp" line="499"/>
        <location filename="../../romstatusexport.cpp" line="735"/>
        <source>Correct</source>
        <translation>Korrekt</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="174"/>
        <location filename="../../romstatusexport.cpp" line="209"/>
        <location filename="../../romstatusexport.cpp" line="500"/>
        <location filename="../../romstatusexport.cpp" line="737"/>
        <source>Mostly correct</source>
        <translation>Beinahe korrekt</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="175"/>
        <location filename="../../romstatusexport.cpp" line="210"/>
        <location filename="../../romstatusexport.cpp" line="501"/>
        <location filename="../../romstatusexport.cpp" line="739"/>
        <source>Incorrect</source>
        <translation>Inkorrekt</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="176"/>
        <location filename="../../romstatusexport.cpp" line="211"/>
        <location filename="../../romstatusexport.cpp" line="502"/>
        <location filename="../../romstatusexport.cpp" line="741"/>
        <source>Not found</source>
        <translation>Nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="177"/>
        <location filename="../../romstatusexport.cpp" line="212"/>
        <location filename="../../romstatusexport.cpp" line="503"/>
        <location filename="../../romstatusexport.cpp" line="743"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="188"/>
        <location filename="../../romstatusexport.cpp" line="480"/>
        <location filename="../../romstatusexport.cpp" line="703"/>
        <source>SDLMAME</source>
        <translation>SDLMAME</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="190"/>
        <location filename="../../romstatusexport.cpp" line="482"/>
        <location filename="../../romstatusexport.cpp" line="705"/>
        <source>SDLMESS</source>
        <translation>SDLMESS</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="192"/>
        <location filename="../../romstatusexport.cpp" line="484"/>
        <location filename="../../romstatusexport.cpp" line="707"/>
        <source>MAME</source>
        <translation>MAME</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="194"/>
        <location filename="../../romstatusexport.cpp" line="486"/>
        <location filename="../../romstatusexport.cpp" line="709"/>
        <source>MESS</source>
        <translation>MESS</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="223"/>
        <location filename="../../romstatusexport.cpp" line="226"/>
        <location filename="../../romstatusexport.cpp" line="336"/>
        <location filename="../../romstatusexport.cpp" line="557"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>ROM types</source>
        <translation>ROM Typen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="419"/>
        <source>done (exporting ROM status in ASCII format to clipboard)</source>
        <translation>Fertig (Exportiere ROM Status im ASCII Format zur Zwischenablage)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="422"/>
        <source>done (exporting ROM status in ASCII format to &apos;%1&apos;)</source>
        <translation>Fertig (Exportiere ROM Status im ASCII Format nach &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="944"/>
        <source>gamelist is not ready, please wait</source>
        <translation>Spieleliste ist nicht bereit, bitte warten</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="222"/>
        <location filename="../../romstatusexport.cpp" line="331"/>
        <location filename="../../romstatusexport.cpp" line="552"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="223"/>
        <location filename="../../romstatusexport.cpp" line="332"/>
        <location filename="../../romstatusexport.cpp" line="553"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="221"/>
        <location filename="../../romstatusexport.cpp" line="333"/>
        <location filename="../../romstatusexport.cpp" line="554"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="242"/>
        <location filename="../../romstatusexport.cpp" line="368"/>
        <location filename="../../romstatusexport.cpp" line="577"/>
        <location filename="../../romstatusexport.cpp" line="820"/>
        <source>correct</source>
        <translation>korrekt</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="249"/>
        <location filename="../../romstatusexport.cpp" line="372"/>
        <location filename="../../romstatusexport.cpp" line="588"/>
        <location filename="../../romstatusexport.cpp" line="833"/>
        <source>mostly correct</source>
        <translation>beinahe korrekt</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="256"/>
        <location filename="../../romstatusexport.cpp" line="376"/>
        <location filename="../../romstatusexport.cpp" line="599"/>
        <location filename="../../romstatusexport.cpp" line="846"/>
        <source>incorrect</source>
        <translation>inkorrekt</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="263"/>
        <location filename="../../romstatusexport.cpp" line="380"/>
        <location filename="../../romstatusexport.cpp" line="610"/>
        <location filename="../../romstatusexport.cpp" line="859"/>
        <source>not found</source>
        <translation>nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="219"/>
        <source>sorting, filtering and analyzing export data</source>
        <translation>Sortiere, filtere und analysiere Export-Daten</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="172"/>
        <location filename="../../romstatusexport.cpp" line="207"/>
        <location filename="../../romstatusexport.cpp" line="498"/>
        <location filename="../../romstatusexport.cpp" line="733"/>
        <source>Total sets</source>
        <translation>Sets gesamt</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="328"/>
        <source>done (sorting, filtering and analyzing export data)</source>
        <translation>Fertig (Sortiere, filtere und analysiere Export-Daten)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="329"/>
        <location filename="../../romstatusexport.cpp" line="550"/>
        <location filename="../../romstatusexport.cpp" line="794"/>
        <source>writing export data</source>
        <translation>Schreibe Export-Daten</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="415"/>
        <location filename="../../romstatusexport.cpp" line="633"/>
        <location filename="../../romstatusexport.cpp" line="887"/>
        <source>done (writing export data)</source>
        <translation>Fertig (Schreibe Export-Daten)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="456"/>
        <source>exporting ROM status in CSV format to &apos;%1&apos;</source>
        <translation>Exportiere ROM Status im CSV Format nach &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="458"/>
        <source>WARNING: can&apos;t open CSV export file &apos;%1&apos; for writing, please check permissions</source>
        <translation>WARNUNG: kann CSV Export Datei &apos;%1&apos; nicht zum Schreiben öffnen, bitte Zugriffsrechte prüfen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="464"/>
        <source>exporting ROM status in CSV format to clipboard</source>
        <translation>Exportiere ROM Status im CSV Format zur Zwischenablage</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="509"/>
        <location filename="../../romstatusexport.cpp" line="753"/>
        <source>sorting and filtering export data</source>
        <translation>Sortiere und filtere Export-Daten</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="637"/>
        <source>done (exporting ROM status in CSV format to clipboard)</source>
        <translation>Fertig (Exportiere ROM Status im CSV Format zur Zwischenablage)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="640"/>
        <source>done (exporting ROM status in CSV format to &apos;%1&apos;)</source>
        <translation>Fertig (Exportiere ROM Status im CSV Format nach &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="185"/>
        <location filename="../../romstatusexport.cpp" line="186"/>
        <location filename="../../romstatusexport.cpp" line="478"/>
        <location filename="../../romstatusexport.cpp" line="697"/>
        <location filename="../../romstatusexport.cpp" line="713"/>
        <source>ROM Status Export - created by QMC2 %1</source>
        <translation>ROM Status Export - erzeugt durch QMC2 %1</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="674"/>
        <source>exporting ROM status in HTML format to &apos;%1&apos;</source>
        <translation>Exportiere ROM Status im HTNL Format nach &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="676"/>
        <source>WARNING: can&apos;t open HTML export file &apos;%1&apos; for writing, please check permissions</source>
        <translation>WARNUNG: kann HTML Export Datei &apos;%1&apos; nicht zum Schreiben öffnen, bitte Zugriffsrechte prüfen</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="682"/>
        <source>exporting ROM status in HTML format to clipboard</source>
        <translation>Exportiere ROM Status im HTML Format zur Zwischenablage</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="891"/>
        <source>done (exporting ROM status in HTML format to clipboard)</source>
        <translation>Fertig (Exportiere ROM Status im HTML Format zur Zwischenablage)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="894"/>
        <source>done (exporting ROM status in HTML format to &apos;%1&apos;)</source>
        <translation>Fertig (Exportiere ROM Status im HTML Format nach &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="648"/>
        <source>Border width</source>
        <translation>Rahmenbreite</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="664"/>
        <source>Border line width for tables (0 = no border)</source>
        <translation>Linienbreite des Rahmens von Tabellen (0 = kein Rahmen)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="205"/>
        <location filename="../../romstatusexport.cpp" line="206"/>
        <location filename="../../romstatusexport.cpp" line="497"/>
        <location filename="../../romstatusexport.cpp" line="729"/>
        <source>Overall ROM Status</source>
        <translation>Gesamt ROM Status</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="215"/>
        <location filename="../../romstatusexport.cpp" line="216"/>
        <location filename="../../romstatusexport.cpp" line="506"/>
        <location filename="../../romstatusexport.cpp" line="749"/>
        <source>Detailed ROM Status</source>
        <translation>Detaillierter ROM Status</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="549"/>
        <location filename="../../romstatusexport.cpp" line="793"/>
        <source>done (sorting and filtering export data)</source>
        <translation>Fertig (Sortiere und filtere Export-Daten)</translation>
    </message>
    <message>
        <source>Total machines</source>
        <translation type="obsolete">Maschinen gesamt</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="34"/>
        <source>Machine description</source>
        <translation>Maschinenbeschreibung</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="35"/>
        <source>Machine name</source>
        <translation>Maschinenname</translation>
    </message>
</context>
<context>
    <name>SampleChecker</name>
    <message>
        <location filename="../../sampcheck.ui" line="15"/>
        <source>Check samples</source>
        <translation>Samples überprüfen</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="71"/>
        <source>Check samples / stop check</source>
        <translation>Samples überprüfen / Prüfung anhalten</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="74"/>
        <location filename="../../sampcheck.cpp" line="232"/>
        <source>&amp;Check samples</source>
        <translation>Samples &amp;prüfen</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="120"/>
        <source>Close sample check dialog</source>
        <translation>Sample-Prüfungs Dialog schließen</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="123"/>
        <source>C&amp;lose</source>
        <translation>Sch&amp;ließen</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="29"/>
        <location filename="../../sampcheck.cpp" line="134"/>
        <source>Good: 0</source>
        <translation>Gut: 0</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="42"/>
        <location filename="../../sampcheck.cpp" line="136"/>
        <source>Bad: 0</source>
        <translation>Schlecht: 0</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="132"/>
        <source>verifying samples</source>
        <translation>Prüfe Samples</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="55"/>
        <location filename="../../sampcheck.cpp" line="138"/>
        <source>Obsolete: 0</source>
        <translation>Überflüssig: 0</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="140"/>
        <source>check pass 1: sample status</source>
        <translation>Prüf-Durchgang 1: Sample-Status</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="194"/>
        <source>check pass 2: obsolete sample sets</source>
        <translation>Prüf-Durchgang 2: überflüssige Sample-Sets</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="216"/>
        <source>Obsolete: %1</source>
        <translation>Überflüssig: %1</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="226"/>
        <source>done (verifying samples, elapsed time = %1)</source>
        <translation>Fertig (Prüfe Samples, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="227"/>
        <source>%1 good, %2 bad (or missing), %3 obsolete</source>
        <translation>%1 gut, %2 schlecht (oder fehlend), %3 überflüssig</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="267"/>
        <location filename="../../sampcheck.cpp" line="318"/>
        <source>Good: %1</source>
        <translation>Gut: %1</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="273"/>
        <location filename="../../sampcheck.cpp" line="324"/>
        <source>Bad: %1</source>
        <translation>Schlecht: %1</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="361"/>
        <source>please wait for reload to finish and try again</source>
        <translation>Bitte warte bis die Spieleliste aktualisiert wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="366"/>
        <source>please wait for ROM state filter to finish and try again</source>
        <translation>Bitte warte bis nach ROM Status gefiltert wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="371"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation>Bitte warte bis die ROM Verifikation abgeschlossen wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="376"/>
        <source>please wait for image check to finish and try again</source>
        <translation>Bitte warte bis die Bild-Überprüfung abgeschlossen wurde und versuche es dann erneut</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="381"/>
        <source>stopping sample check upon user request</source>
        <translation>Beende Sample-Prüfung auf Wunsch des Benutzers</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="386"/>
        <source>&amp;Stop check</source>
        <translation>&amp;Anhalten</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="94"/>
        <source>Select game in gamelist when selecting a sample set?</source>
        <translation>Spiel in Spieleliste auswählen, wenn ein Sample selektiert wird?</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="97"/>
        <source>Select &amp;game</source>
        <translation>Spiel &amp;auswählen</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="84"/>
        <source>Remove obsolete sample sets</source>
        <translation>Überflüssige Samples-Sets entfernen</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="87"/>
        <source>&amp;Remove obsolete</source>
        <translation>Überflüssige &amp;entfernen</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="178"/>
        <source>WARNING: emulator audit call didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation>WARNUNG: Emulator-Aufruf zur Auditierung wurde nicht ordentlich beendet -- exitCode = %1, exitStatus = %2</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="178"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="178"/>
        <source>crashed</source>
        <translation>abgestürzt</translation>
    </message>
</context>
<context>
    <name>SnapshotViewer</name>
    <message>
        <location filename="../../embedderopt.cpp" line="156"/>
        <source>Snapshot viewer</source>
        <translation>Schnappschuss Anzeige</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="165"/>
        <source>Use as preview</source>
        <translation>Als Vorschaubild verwenden</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="171"/>
        <source>Use as title</source>
        <translation>Als Titelbild verwenden</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="180"/>
        <source>Save as...</source>
        <translation>Speichern unter...</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="186"/>
        <source>Copy to clipboard</source>
        <translation>In Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="291"/>
        <source>Choose PNG file to store image</source>
        <translation>PNG Datei zum Speichern des Bildes auswählen</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="291"/>
        <source>PNG images (*.png)</source>
        <translation>PNG Dateien (*.png)</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="296"/>
        <source>FATAL: couldn&apos;t save snapshot image to &apos;%1&apos;</source>
        <translation>FATAL: konnte Schnappschuss für &apos;%1&apos; nicht speichern</translation>
    </message>
</context>
<context>
    <name>SoftwareList</name>
    <message>
        <location filename="../../softwarelist.ui" line="18"/>
        <source>Software list</source>
        <translation>Software Liste</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="35"/>
        <location filename="../../softwarelist.ui" line="38"/>
        <source>Reload all information</source>
        <translation>Alle Informationen neu laden</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="55"/>
        <location filename="../../softwarelist.ui" line="58"/>
        <source>Select a pre-defined device configuration to be added to the software setup</source>
        <translation>Wähle eine vordefinierte Geräte-Konfiguration, um sie dem Software-Setup hinzuzufügen</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="62"/>
        <location filename="../../softwarelist.cpp" line="877"/>
        <location filename="../../softwarelist.cpp" line="1377"/>
        <source>No additional devices</source>
        <translation>Keine zusätzlichen Geräte</translation>
    </message>
    <message>
        <source>Add the currently selected software and device setup to the favorites list</source>
        <translation type="obsolete">Die aktuell ausgewählte Software- und Geräte-Konfiguration den Favoriten hinzufügen</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="109"/>
        <location filename="../../softwarelist.ui" line="112"/>
        <source>Remove the currently selected favorite software configuration</source>
        <translation>Aktuell ausgewählte favorisierte Software-Konfiguration entfernen</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="129"/>
        <location filename="../../softwarelist.ui" line="132"/>
        <source>Play the selected software configuration</source>
        <translation>Ausgewählte Software-Konfiguration starten</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="149"/>
        <location filename="../../softwarelist.ui" line="152"/>
        <source>Play the selected software configuration (embedded)</source>
        <translation>Ausgewählte Software-Konfiguration starten (eingebettet)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="184"/>
        <source>Known software</source>
        <translation>Bekannte Software</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="187"/>
        <source>Complete list of known software for the current system</source>
        <translation>Vollständige Liste bekannter Software für das aktuelle System</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="275"/>
        <source>View / manage your favorite software list for the current system</source>
        <translation>Favoriten für das aktuelle System verwalten</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="368"/>
        <source>Search within the list of known software for the current system</source>
        <translation>Suche innerhalb der Liste bekannter Software für das aktuelle System</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="202"/>
        <location filename="../../softwarelist.ui" line="205"/>
        <source>List of known software</source>
        <translation>Liste bekannter Software</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="89"/>
        <location filename="../../softwarelist.ui" line="92"/>
        <source>Add the currently selected software and device setup to the favorites list (or overwrite existing favorite)</source>
        <translation>Die aktuell ausgewählte Software- und Geräte-Konfiguration den Favoriten hinzufügen (oder existierenden Favoriten überschreiben)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="221"/>
        <location filename="../../softwarelist.ui" line="309"/>
        <location filename="../../softwarelist.ui" line="421"/>
        <location filename="../../softwarelist.cpp" line="128"/>
        <location filename="../../softwarelist.cpp" line="147"/>
        <location filename="../../softwarelist.cpp" line="168"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="226"/>
        <location filename="../../softwarelist.ui" line="314"/>
        <location filename="../../softwarelist.ui" line="426"/>
        <location filename="../../softwarelist.cpp" line="130"/>
        <location filename="../../softwarelist.cpp" line="149"/>
        <location filename="../../softwarelist.cpp" line="170"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="231"/>
        <location filename="../../softwarelist.ui" line="319"/>
        <location filename="../../softwarelist.ui" line="431"/>
        <location filename="../../softwarelist.cpp" line="132"/>
        <location filename="../../softwarelist.cpp" line="151"/>
        <location filename="../../softwarelist.cpp" line="172"/>
        <source>Publisher</source>
        <translation>Herausgeber</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="236"/>
        <location filename="../../softwarelist.ui" line="324"/>
        <location filename="../../softwarelist.ui" line="436"/>
        <location filename="../../softwarelist.cpp" line="134"/>
        <location filename="../../softwarelist.cpp" line="153"/>
        <location filename="../../softwarelist.cpp" line="174"/>
        <source>Year</source>
        <translation>Jahr</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="241"/>
        <location filename="../../softwarelist.ui" line="329"/>
        <location filename="../../softwarelist.ui" line="441"/>
        <location filename="../../softwarelist.cpp" line="136"/>
        <location filename="../../softwarelist.cpp" line="155"/>
        <location filename="../../softwarelist.cpp" line="176"/>
        <source>Part</source>
        <translation>Teil</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="246"/>
        <location filename="../../softwarelist.ui" line="334"/>
        <location filename="../../softwarelist.ui" line="446"/>
        <location filename="../../softwarelist.cpp" line="138"/>
        <location filename="../../softwarelist.cpp" line="157"/>
        <location filename="../../softwarelist.cpp" line="178"/>
        <source>Interface</source>
        <translation>Schnittstelle</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="251"/>
        <location filename="../../softwarelist.ui" line="339"/>
        <location filename="../../softwarelist.ui" line="451"/>
        <location filename="../../softwarelist.cpp" line="140"/>
        <location filename="../../softwarelist.cpp" line="159"/>
        <location filename="../../softwarelist.cpp" line="180"/>
        <source>List</source>
        <translation>Liste</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="272"/>
        <source>Favorites</source>
        <translation>Favoriten</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="290"/>
        <location filename="../../softwarelist.ui" line="293"/>
        <source>Favorite software configurations</source>
        <translation>Favorisierte Software-Konfigurationen</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="344"/>
        <location filename="../../softwarelist.cpp" line="161"/>
        <source>Device configuration</source>
        <translation>Geräte-Konfiguration</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="365"/>
        <source>Search</source>
        <translation>Suchen</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="383"/>
        <location filename="../../softwarelist.ui" line="386"/>
        <source>Search for known software (not case-sensitive)</source>
        <translation>Suche nach bekannter Software (Groß-/Kleinschreibung wird nicht beachtet)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="402"/>
        <location filename="../../softwarelist.ui" line="405"/>
        <source>Search results</source>
        <translation>Suchergebnis</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="475"/>
        <source>Loading software-lists, please wait...</source>
        <translation>Lade Software-Listen, bitte warten...</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="54"/>
        <source>Add the currently selected software to the favorites list</source>
        <translation>Aktuell ausgewählte Software-Konfiguration den Favoriten hinzufügen</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="61"/>
        <source>Enter search string</source>
        <translation>Such-Zeichenkette eingeben</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="88"/>
        <source>Play selected software</source>
        <translation>Ausgewählte Software spielen</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="89"/>
        <source>&amp;Play</source>
        <translation>S&amp;pielen</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="94"/>
        <source>Play selected software (embedded)</source>
        <translation>Ausgewählte Software spielen (eingebettet)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="95"/>
        <source>Play &amp;embedded</source>
        <translation>Spielen (&amp;eingebettet)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="101"/>
        <source>Add to favorite software list</source>
        <translation>Zu Software-Favoriten hinzufügen</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="102"/>
        <source>&amp;Add to favorites</source>
        <translation>&amp;Zu Favoriten hinzufügen</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="106"/>
        <source>Remove from favorite software list</source>
        <translation>Von Software-Favoriten entfernen</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="107"/>
        <source>&amp;Remove from favorites</source>
        <translation>&amp;Von Favoriten entfernen</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="221"/>
        <source>WARNING: software list &apos;%1&apos; not found</source>
        <translation>WARNUNG: Software-Liste &apos;%1&apos; nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="335"/>
        <source>Known software (%1)</source>
        <translation>Bekannte Software (%1)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="336"/>
        <source>Favorites (%1)</source>
        <translation>Favoriten (%1)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="337"/>
        <source>Search (%1)</source>
        <translation>Suchen (%1)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="351"/>
        <location filename="../../softwarelist.cpp" line="545"/>
        <source>Known software (no data available)</source>
        <translation>Bekannte Software (keine Daten verfügbar)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="352"/>
        <location filename="../../softwarelist.cpp" line="546"/>
        <source>Favorites (no data available)</source>
        <translation>Favoriten (keine Daten verfügbar)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="353"/>
        <location filename="../../softwarelist.cpp" line="547"/>
        <source>Search (no data available)</source>
        <translation>Suchen (keine Daten verfügbar)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="419"/>
        <source>loading XML software list data from cache</source>
        <translation>Lade XML Softwarelisten-Daten aus dem Cache</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="424"/>
        <source>SWL cache - %p%</source>
        <translation>SWL Cache - %p%</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="452"/>
        <source>done (loading XML software list data from cache, elapsed time = %1)</source>
        <translation>Fertig (Lade XML Softwarelisten-Daten aus dem Cache, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <source>done (loading XML software list data from cache, elapsed time = %1</source>
        <translation type="obsolete">Fertig (Lade XML Softwarelisten-Daten aus dem Cache, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="460"/>
        <source>ERROR: the file name for the MAME software list cache is empty -- please correct this and reload the game list afterwards</source>
        <translation>FEHLER: Der Dateiname für den MAME Softwarelisten-Cache ist leer -- bitte korrigieren und anschließend Spieleliste neu laden</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="462"/>
        <source>ERROR: the file name for the MESS software list cache is empty -- please correct this and reload the machine list afterwards</source>
        <translation>FEHLER: Der Dateiname für den MESS Softwarelisten Cache ist leer -- bitte korrigieren und anschließend Maschinenliste neu laden</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="478"/>
        <source>loading XML software list data and (re)creating cache</source>
        <translation>Lade XML Softwarelisten-Daten und erzeuge Cache neu</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="481"/>
        <source>SWL data - %p%</source>
        <translation>SWL Daten - %p%</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="487"/>
        <source>ERROR: can&apos;t open the MAME software list cache for writing, path = %1 -- please check/correct access permissions and reload the game list afterwards</source>
        <translation>FEHLER: Kann den MAME Softwarelisten-Cache nicht zum Schreiben öffnen, Pfad = %1 -- bitte Zugriffsrechte prüfen/korrigieren und anschließend die Spieleliste neu laden</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="489"/>
        <source>ERROR: can&apos;t open the MESS software list cache for writing, path = %1 -- please check/correct access permissions and reload the machine list afterwards</source>
        <translation>FEHLER: Kann den MESS Softwarelisten Cache nicht zum Schreiben öffnen, Pfad = %1 -- bitte Zugriffsrechte prüfen/korrigieren und anschließend die Maschinenliste neu laden</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="579"/>
        <source>FATAL: error while parsing XML data for software list &apos;%1&apos;</source>
        <translation>FATAL: Fehler beim Parsen der XML Daten für Software-Liste &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="759"/>
        <source>WARNING: the external process called to load the MAME software lists didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation>WARNUNG: Der externe Prozess, der zum Laden der MAME Softwarelisten gestartet wurde, konnte nicht ordentlich beendet werden -- exitCode = %1, exitStatus = %2</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="759"/>
        <location filename="../../softwarelist.cpp" line="761"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="759"/>
        <location filename="../../softwarelist.cpp" line="761"/>
        <source>crashed</source>
        <translation>abgestürzt</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="761"/>
        <source>WARNING: the external process called to load the MESS software lists didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation>WARNUNG: Der externe Prozess, der zum Laden der MESS Softwarelisten gestartet wurde, konnte nicht ordentlich beendet werden -- exitCode = %1, exitStatus = %2</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="765"/>
        <source>done (loading XML software list data and (re)creating cache, elapsed time = %1)</source>
        <translation>Fertig (Lade XML Softwarelisten-Daten und erzeuge Cache neu, benötigte Zeit = %1)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="818"/>
        <source>WARNING: the currently selected MAME emulator doesn&apos;t support software lists</source>
        <translation>WARNUNG: der zur Zeit ausgewählte MAME Emulator unterstützt keine Software-Listen</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="820"/>
        <source>WARNING: the currently selected MESS emulator doesn&apos;t support software lists</source>
        <translation>WARNUNG: der zur Zeit ausgewählte MESS Emulator unterstützt keine Software-Listen</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="837"/>
        <source>WARNING: the external process called to load the MAME software lists caused an error -- processError = %1</source>
        <translation>WARNUNG: Der externe Prozess, der zum Laden der MAME Softwarelisten gestartet wurde, verursachte einen Fehler -- processError = %1</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="839"/>
        <source>WARNING: the external process called to load the MESS software lists caused an error -- processError = %1</source>
        <translation>WARNUNG: Der externe Prozess, der zum Laden der MESS Softwarelisten gestartet wurde, verursachte einen Fehler -- processError = %1</translation>
    </message>
</context>
<context>
    <name>SoftwareSnap</name>
    <message>
        <location filename="../../softwarelist.cpp" line="1585"/>
        <source>Snapshot viewer</source>
        <translation>Schnappschuss Anzeige</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="1598"/>
        <source>Copy to clipboard</source>
        <translation>In Zwischenablage kopieren</translation>
    </message>
</context>
<context>
    <name>Title</name>
    <message>
        <location filename="../../title.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>In Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="56"/>
        <location filename="../../title.cpp" line="57"/>
        <source>Game title image</source>
        <translation>Spiel Titel-Bild</translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="59"/>
        <location filename="../../title.cpp" line="60"/>
        <source>Machine title image</source>
        <translation>Machinen Titel-Bild</translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="68"/>
        <location filename="../../title.cpp" line="72"/>
        <source>FATAL: can&apos;t open title file, please check access permissions for %1</source>
        <translation>FATAL: kann Titel-Datei nicht öffnen; bitte Zugriffsrechte für %1 überprüfen</translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Warte auf Daten...</translation>
    </message>
</context>
<context>
    <name>ToolExecutor</name>
    <message>
        <location filename="../../toolexec.ui" line="15"/>
        <source>Executing tool</source>
        <translation>Führe Werkzeug aus</translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="29"/>
        <source>Command</source>
        <translation>Kommando</translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="42"/>
        <source>Executed command</source>
        <translation>Ausgeführtes Kommando</translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="91"/>
        <source>Close tool execution dialog</source>
        <translation>Werzeug Ausführungs-Dialog schließen</translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="94"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="54"/>
        <source>Output from tool</source>
        <translation>Ausgabe des Tools</translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="60"/>
        <location filename="../../toolexec.cpp" line="62"/>
        <source>### tool started, output below ###</source>
        <translation>###  Werkzeug gestartet, Ausgabe folgt ###</translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="62"/>
        <location filename="../../toolexec.cpp" line="81"/>
        <location filename="../../toolexec.cpp" line="128"/>
        <source>tool control: </source>
        <translation>Werkzeug Steuerung: </translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="79"/>
        <location filename="../../toolexec.cpp" line="81"/>
        <source>### tool finished, exit code = %1, exit status = %2 ###</source>
        <translation>###  Werkzeug beendet, Rückgabewert = %1, Rückkehrstatus = %2 ###</translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="100"/>
        <location filename="../../toolexec.cpp" line="116"/>
        <source>tool output: </source>
        <translation>Werkzeug Ausgabe: </translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="100"/>
        <source>stdout: %1</source>
        <translation>stdout: %1</translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="116"/>
        <source>stderr: %1</source>
        <translation>stderr: %1</translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="126"/>
        <location filename="../../toolexec.cpp" line="128"/>
        <source>### tool error, process error = %1 ###</source>
        <translation>### Werkzeug Fehler, Prozessfehler = %1 ###</translation>
    </message>
</context>
<context>
    <name>VideoItemWidget</name>
    <message>
        <location filename="../../videoitemwidget.cpp" line="173"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="178"/>
        <location filename="../../videoitemwidget.cpp" line="180"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="178"/>
        <source>Open author URL with the default browser</source>
        <translation>Home-URL des Autors mit Standard-Browser öffnen</translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="186"/>
        <source>Open video URL with the default browser</source>
        <translation>Video URL mit Standard-Browser öffnen</translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="186"/>
        <location filename="../../videoitemwidget.cpp" line="188"/>
        <source>Video:</source>
        <translation>Video:</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../../welcome.cpp" line="94"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="94"/>
        <source>The specified file isn&apos;t executable!</source>
        <translation>Die angegebene Datei ist nicht ausführbar!</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="109"/>
        <source>All files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="221"/>
        <source>It appears that another instance of %1 is already running.
However, this can also be the leftover of a previous crash.

Exit now, accept once or ignore completely?</source>
        <translation>Es hat den Anschein, dass bereits eine Instanz von &apos;%1&apos; läuft.
Oder es gab einen Absturz bei einer vorhergehenden Sitzung.

Jetzt beenden, einmalig akzeptieren oder vollständig ignorieren?</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="15"/>
        <source>Welcome to QMC2</source>
        <translation>Willkommen zu QMC2</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="528"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Welcome to QMC2!&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This appears to be your first start of QMC2 because no valid configuration was found. In order to use QMC2 as a front end for an emulator, you must specify the path to the emulator&apos;s executable file below.&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The paths below the executable file are optional, but you should specify as many of them as you can right now to avoid problems or confusion later (of course, you can change the paths in the emulator&apos;s global configuration at any time later).&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;It&apos;s strongly recommended that you specify the ROM path you are going to use at least!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Willkommen bei QMC2!&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Wie es scheint ist dies Dein erster Start von QMC2, denn es wurde keine gültige Konfiguration gefunden. Um QMC2 als Frontend für einen Emulator einsetzen zu können, musst der Pfad zur ausführbaren Emulator-Datei unten angegeben werden.&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Die sich darunter befindenden Pfadangaben sind zwar optional, aber um spätere Probleme oder Verwirrungen zu vermeiden, solltest Du so viele von ihnen wie Du kannst jetzt schon angeben (natürlich lassen sich diese Pfade später in den globalen Emulator Einstellungen zu jeder Zeit ändern).&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Es wird dringend empfohlen, dass Du zumindest das ROM Verzeichnis angibst, um es im Anschluss gleich verwenden zu können!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="651"/>
        <source>Hash path</source>
        <translation>Hash Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="664"/>
        <source>Path to hash files</source>
        <translation>Pfad zu Hash Dateien</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="671"/>
        <source>Browse hash path</source>
        <translation>Hash Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="709"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="716"/>
        <source>&amp;Cancel</source>
        <translation>Abbre&amp;chen</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="121"/>
        <source>Choose ROM path</source>
        <translation>ROM Pfad auswählen</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="582"/>
        <source>Browse ROM path</source>
        <translation>ROM Pfad auswählen</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="134"/>
        <source>Choose sample path</source>
        <translation>Sample Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="606"/>
        <source>Browse sample path</source>
        <translation>Sample Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="109"/>
        <source>Choose emulator executable file</source>
        <translation>Ausführbare Emulator Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="551"/>
        <source>Emulator executable file</source>
        <translation>Ausführbare Emulator Datei</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="558"/>
        <source>Browse emulator executable file</source>
        <translation>Ausführbare Emulator Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="33"/>
        <source>SDLMAME</source>
        <translation>SDLMAME</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="35"/>
        <source>SDLMESS</source>
        <translation>SDLMESS</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="40"/>
        <source>MAME</source>
        <translation>MAME</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="42"/>
        <source>MESS</source>
        <translation>MESS</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="47"/>
        <source>Unsupported emulator</source>
        <translation>Emulator nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="49"/>
        <source>%1 executable file</source>
        <translation>Ausführbare Datei (%1)</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="147"/>
        <source>Choose hash path</source>
        <translation>Hash Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="220"/>
        <source>Single-instance check</source>
        <translation>Einzel-Instanz Prüfung</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="222"/>
        <source>&amp;Exit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="222"/>
        <source>&amp;Once</source>
        <translation>&amp;Einmal</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="222"/>
        <source>&amp;Ignore</source>
        <translation>&amp;Ignorieren</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="575"/>
        <source>Path to ROM images</source>
        <translation>Pfad zu ROM Dateien</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="599"/>
        <source>Path to samples</source>
        <translation>Pfad zu Sample Dateien</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="630"/>
        <source>Emulator executable</source>
        <translation>Programmpfad</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="637"/>
        <source>ROM path</source>
        <translation>ROM Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="644"/>
        <source>Sample path</source>
        <translation>Sample Verzeichnis</translation>
    </message>
</context>
<context>
    <name>YouTubeVideoPlayer</name>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="40"/>
        <source>Attached videos</source>
        <translation>Zugeordnete Videos</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="74"/>
        <source>Start playing / select next video automatically</source>
        <translation>Starte das Abspielen / wähle nächstes Video automatisch</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="77"/>
        <source>Play-O-Matic</source>
        <translation>Spiel-O-Matic</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="93"/>
        <source>Mode:</source>
        <translation>Modus:</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="112"/>
        <source>Choose the video selection mode</source>
        <translation>Video-Selektionsmodus auswählen</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="116"/>
        <source>sequential</source>
        <translation>sequenziell</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="121"/>
        <source>random</source>
        <translation>zufällig</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="138"/>
        <source>Allow videos to be repeated (otherwise stop after last video)</source>
        <translation>Video-Wiederholungen zulassen (andernfalls wird das Abspielen nach dem letzten Video angehalten)</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="141"/>
        <source>Allow repeat</source>
        <translation>Wdh. erlaubt</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="174"/>
        <source>Video player</source>
        <translation>Videospieler</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="196"/>
        <source>Select the preferred video format (automatically falls back to the next available format)</source>
        <translation>Bevorzugtes Video-Format auswählen (ggf. wird automatisch auf das nächste verfügbare Format zurückgegriffen)</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="203"/>
        <location filename="../../youtubevideoplayer.cpp" line="79"/>
        <source>FLV 240P</source>
        <translation>FLV 240P</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="212"/>
        <location filename="../../youtubevideoplayer.cpp" line="80"/>
        <source>FLV 360P</source>
        <translation>FLV 360P</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="221"/>
        <location filename="../../youtubevideoplayer.cpp" line="81"/>
        <source>MP4 360P</source>
        <translation>MP4 360P</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="230"/>
        <location filename="../../youtubevideoplayer.cpp" line="82"/>
        <source>FLV 480P</source>
        <translation>FLV 480P</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="239"/>
        <location filename="../../youtubevideoplayer.cpp" line="83"/>
        <source>MP4 720P</source>
        <translation>MP4 720P</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="248"/>
        <location filename="../../youtubevideoplayer.cpp" line="84"/>
        <source>MP4 1080P</source>
        <translation>MP4 1080P</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="257"/>
        <location filename="../../youtubevideoplayer.cpp" line="85"/>
        <source>MP4 3072P</source>
        <translation>MP4 3072P</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="275"/>
        <location filename="../../youtubevideoplayer.cpp" line="146"/>
        <source>Start / pause / resume video playback</source>
        <translation>Video Wiedergabe starten / pausieren / fortsetzen</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="315"/>
        <source>Remaining playing time</source>
        <translation>Verbliebene Abspielzeit</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="334"/>
        <source>Current buffer fill level</source>
        <translation>Puffer-Füllstand</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="363"/>
        <source>Search videos</source>
        <translation>Nach Videos suchen</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="378"/>
        <source>Search pattern -- use the &apos;hint&apos; button to get a suggestion</source>
        <translation>Such-Zeichenkette -- benutze den &apos;Hinweis&apos;-Button für einen Vorschlag</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="391"/>
        <source>Search YouTube videos using the specified search pattern</source>
        <translation>Nach YouTube Videos suchen</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="435"/>
        <source>Maximum number of results per search request</source>
        <translation>Maximale Anzahl von Resultaten pro Suchanfrage</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="466"/>
        <source>Start index for the search request</source>
        <translation>Start-Index für die Suchanfrage</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="491"/>
        <source>SI:</source>
        <translation>SI:</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="498"/>
        <source>R:</source>
        <translation>R:</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="418"/>
        <source>Suggest a search pattern (hold down for menu)</source>
        <translation>Suchzeichenkette vorschlagen (gedrückt halten für Menü)</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="61"/>
        <source>Mute / unmute audio output</source>
        <translation>Audio-Ausgabe stummschalten / aktivieren</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="96"/>
        <source>Video progress</source>
        <translation>Video-Fortschrittsanzeige</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="105"/>
        <location filename="../../youtubevideoplayer.cpp" line="724"/>
        <location filename="../../youtubevideoplayer.cpp" line="748"/>
        <location filename="../../youtubevideoplayer.cpp" line="767"/>
        <location filename="../../youtubevideoplayer.cpp" line="814"/>
        <source>Current buffer fill level: %1%</source>
        <translation>Puffer-Füllstand: %1%</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="112"/>
        <location filename="../../youtubevideoplayer.cpp" line="179"/>
        <source>Play this video</source>
        <translation>Dieses Video abspielen</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="119"/>
        <location filename="../../youtubevideoplayer.cpp" line="161"/>
        <location filename="../../youtubevideoplayer.cpp" line="190"/>
        <source>Copy video URL</source>
        <translation>Video-URL kopieren</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="125"/>
        <location filename="../../youtubevideoplayer.cpp" line="166"/>
        <location filename="../../youtubevideoplayer.cpp" line="195"/>
        <source>Copy author URL</source>
        <translation>Home-URL des Autors kopieren</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="131"/>
        <source>Paste video URL</source>
        <translation>Video-URL einfügen</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="138"/>
        <source>Remove selected videos</source>
        <translation>Ausgewählte Videos entfernen</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="154"/>
        <location filename="../../youtubevideoplayer.cpp" line="1246"/>
        <source>Full screen (return with toggle-key)</source>
        <translation>Vollbild (Rückkehr mit Umschalt-Taste)</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="172"/>
        <location filename="../../youtubevideoplayer.cpp" line="184"/>
        <source>Attach this video</source>
        <translation>Dieses Video zuordnen</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="202"/>
        <source>Auto-suggest a search pattern?</source>
        <translation>Suche-Zeichenkette automatisch vorschlagen?</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="203"/>
        <source>Auto-suggest</source>
        <translation>Autom. vorschlagen</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="208"/>
        <source>Enter string to be appended</source>
        <translation>Angefügte Zeichenkette eingeben</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="209"/>
        <source>Append...</source>
        <translation>Anhängen...</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="220"/>
        <source>Enter search string</source>
        <translation>Such-Zeichenkette eingeben</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="324"/>
        <source>Appended string</source>
        <translation>Angehängte Zeichenkette</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="325"/>
        <source>Enter the string to be appended when suggesting a pattern:</source>
        <translation>Zeichenkette, die dem Vorschlag immer angehängt werden soll:</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="587"/>
        <source>video player: a video with the ID &apos;%1&apos; is already attached, ignored</source>
        <translation>Videospieler: ein Video mit der ID &apos;%1&apos; ist bereits zugeordnet, ignoriert</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="797"/>
        <source>video player: playback error: %1</source>
        <translation>Videospieler: Wiedergabe-Fehler: %1</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="946"/>
        <source>video player: video info error: ID = &apos;%1&apos;, status = &apos;%2&apos;, errorCode = &apos;%3&apos;, errorText = &apos;%4&apos;</source>
        <translation>Videospieler: Video-Info Fehler: ID= &apos;%1&apos;, Status = &apos;%2&apos;, Fehler-Code = &apos;%3&apos;, Fehler-Text = &apos;%4&apos;</translation>
    </message>
    <message>
        <source>, ID = %1</source>
        <translation type="obsolete">, ID = %1</translation>
    </message>
    <message>
        <source>video player: video info error: status = &apos;%1&apos;, errorCode = &apos;%2&apos;, errorText = &apos;%3&apos;</source>
        <translation type="obsolete">Videospieler: Video-Info Fehler: Status = &apos;%1&apos;, Fehler-Code = &apos;%2&apos;, Fehler-Text = &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1015"/>
        <source>video player: video info error: timeout occurred</source>
        <translation>Videospieler: Video-Info Fehler: Zeitüberschreitung der Anfrage</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1051"/>
        <source>video player: video info error: %1</source>
        <translation>Videospieler: Video-Info Fehler: %1</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1244"/>
        <source>Full screen (press %1 to return)</source>
        <translation>Vollbild (Rückkehr mit %1)</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1419"/>
        <source>video player: video image info error: %1</source>
        <translation>Videospieler: Videobild-Info Fehler: %1</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1450"/>
        <source>video player: search request error: %1</source>
        <translation>Videospieler: Suchanfrage-Fehler: %1</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1478"/>
        <source>video player: search error: can&apos;t parse XML data</source>
        <translation>Videospieler: Suchanfrage-Fehler: kann XML Daten nicht parsen</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1511"/>
        <source>video player: can&apos;t determine the video ID from the reply URL &apos;%1&apos; -- please inform developers</source>
        <translation>Videospieler: kann die Video ID nicht aus der Antwort URL &apos;%1&apos; ermitteln -- bitte Entwickler informieren</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1517"/>
        <source>video player: can&apos;t associate the returned image for video ID &apos;%1&apos; -- please inform developers</source>
        <translation>Videospieler: kann die erhaltenen Bilddaten nicht mit der Video ID &apos;%1&apos; assoziieren -- bitte Entwickler informieren</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1539"/>
        <source>video player: can&apos;t save the image for video ID &apos;%1&apos; to the YouTube cache directory &apos;%2&apos; -- please check permissions</source>
        <translation>Videospieler: kann das Bild für Video ID &apos;%1&apos; nicht im YouTube Cache Verzeichnis &apos;%2&apos; abspeichern -- bitte Berechtigungen prüfen</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1541"/>
        <source>video player: can&apos;t save the image for video ID &apos;%1&apos;, the YouTube cache directory &apos;%2&apos; does not exist -- please correct</source>
        <translation>Videospieler: kann das Bild für Video ID &apos;%1&apos; nicht abspeichern, da das YouTube Cache Verzeichnis &apos;%2&apos; nicht existiert -- bitte korrigieren</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1543"/>
        <source>video player: image download failed for video ID &apos;%1&apos;, retrieved image is not valid</source>
        <translation>Videospieler: der Download des Bildes für Video ID &apos;%1&apos; ist fehlgeschlagen, da die erhaltenen Bilddaten nicht gültig sind</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1545"/>
        <source>video player: image download failed for video ID &apos;%1&apos;, error text = &apos;%2&apos;</source>
        <translation>Videospieler: der Download des Bildes für Video ID &apos;%1&apos; ist fehlgeschlagen, Fehlertext = &apos;%2&apos;</translation>
    </message>
</context>
</TS>
