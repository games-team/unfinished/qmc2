<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>About</name>
    <message>
        <location filename="../../about.cpp" line="120"/>
        <source>Version </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="122"/>
        <source>SVN r%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="124"/>
        <source>built for</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="136"/>
        <source>Copyright</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="136"/>
        <source>Germany</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="140"/>
        <source>Project homepage:</source>
        <translation>Project homepage:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="141"/>
        <source>Development site:</source>
        <translation>Development site:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="142"/>
        <source>QMC2 development mailing list:</source>
        <translation>QMC2 development mailing list:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="143"/>
        <source>List subscription:</source>
        <translation>List subscription:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="144"/>
        <source>Bug tracking system:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="149"/>
        <source>Build OS:</source>
        <translation>Build OS:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="156"/>
        <source>Emulator version:</source>
        <translation>Emulator version:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <source>Qt version:</source>
        <translation>Qt version:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <location filename="../../about.cpp" line="160"/>
        <source>Compile-time:</source>
        <translation>Compile-time:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <location filename="../../about.cpp" line="160"/>
        <location filename="../../about.cpp" line="163"/>
        <source>Run-time:</source>
        <translation>Run-time:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <source>Build key:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="163"/>
        <source>Phonon backend / supported MIME types:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="175"/>
        <source>Environment variables:</source>
        <translation>Environment variables:</translation>
    </message>
    <message>
        <location filename="../../about.ui" line="15"/>
        <source>About QMC2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.ui" line="515"/>
        <source>Project details</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="../../about.ui" line="527"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;QMC2 - M.A.M.E. Catalog / Launcher II&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Qt 4 based UNIX multi-emulator frontend&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Version X.Y[.bZ], built for SDLMAME&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright © 2006 - 2008 R. Reucher, Germany&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.ui" line="549"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Project homepage:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;http://www.mameworld.net/mamecat&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Development site:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;http://sourceforge.net/projects/qmc2&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;QMC2 development mailing list:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;qmc2-devel@lists.sourceforge.net (subscribers only)&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;List subscription:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;https://lists.sourceforge.net/lists/listinfo/qmc2-devel&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.ui" line="579"/>
        <source>System information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Total: %1 MB</source>
        <translation>Total: %1 MB</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Free: %1 MB</source>
        <translation>Free: %1 MB</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Used: %1 MB</source>
        <translation>Used: %1 MB</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Physical memory:</source>
        <translation>Physical memory:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="174"/>
        <source>Number of CPUs:</source>
        <translation>Number of CPUs:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="174"/>
        <source>unknown</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Template information:</source>
        <translation>Template information:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="72"/>
        <source>Windows 7 or Windows Server 2008 R2 (Windows 6.1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Emulator:</source>
        <translation>Emulator:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Format:</source>
        <translation>Format:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="160"/>
        <source>SDL version:</source>
        <translation>SDL version:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="59"/>
        <source>Mac OS X 10.3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="60"/>
        <source>Mac OS X 10.4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="61"/>
        <source>Mac OS X 10.5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="62"/>
        <source>Mac OS X 10.6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="63"/>
        <source>Mac (unkown)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="67"/>
        <source>Windows NT (Windows 4.0)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="68"/>
        <source>Windows 2000 (Windows 5.0)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="69"/>
        <source>Windows XP (Windows 5.1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="70"/>
        <source>Windows Server 2003, Windows Server 2003 R2, Windows Home Server or Windows XP Professional x64 Edition (Windows 5.2)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="71"/>
        <source>Windows Vista or Windows Server 2008 (Windows 6.0)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="73"/>
        <source>Windows (unknown)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="142"/>
        <source>subscription required</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="152"/>
        <location filename="../../about.cpp" line="154"/>
        <source>Running OS:</source>
        <translation>Running OS:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="119"/>
        <source>Qt 4 based multi-platform/multi-emulator front end</source>
        <translation>Qt 4 based multi-platform/multi-emulator front end</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="163"/>
        <source>Phonon version:</source>
        <translation>Phonon version:</translation>
    </message>
</context>
<context>
    <name>ArcadeScene</name>
    <message>
        <location filename="../../arcade/arcadescene.cpp" line="36"/>
        <source>FPS: --</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescene.cpp" line="258"/>
        <location filename="../../arcade/arcadescene.cpp" line="260"/>
        <source>FPS: %1</source>
        <translation>FPS: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescene.cpp" line="303"/>
        <location filename="../../arcade/arcadescene.cpp" line="306"/>
        <source>Paused</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ArcadeScreenshotSaverThread</name>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="48"/>
        <source>ArcadeScreenshotSaverThread: Started</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="66"/>
        <source>ArcadeScreenshotSaverThread: Saving screen shot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="80"/>
        <source>ArcadeScreenshotSaverThread: Screen shot successfully saved as &apos;%1&apos;</source>
        <translation>ArcadeScreenshotSaverThread: Screen shot successfully saved as &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="82"/>
        <source>ArcadeScreenshotSaverThread: Failed to save screen shot as &apos;%1&apos;</source>
        <translation>ArcadeScreenshotSaverThread: Failed to save screen shot as &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="92"/>
        <source>ArcadeScreenshotSaverThread: Ended</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="71"/>
        <source>ArcadeScreenshotSaverThread: Failed to create screen shot directory &apos;%1&apos; - aborting screen shot creation</source>
        <translation>ArcadeScreenshotSaverThread: Failed to create screen shot directory &apos;%1&apos; - aborting screen shot creation</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="83"/>
        <source>Saving screen shot</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ArcadeSetupDialog</name>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="15"/>
        <source>Arcade setup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="25"/>
        <source>Graphics mode settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="58"/>
        <source>General</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="648"/>
        <source>Arcade font (= system default if empty)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="658"/>
        <source>Browse arcade font</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="295"/>
        <source>Snapshot directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="304"/>
        <source>Directory to store snapshots (write)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="311"/>
        <source>Browse snapshot directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="72"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="171"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="786"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1007"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1228"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1449"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1670"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1891"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2112"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2333"/>
        <source>X:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="95"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="194"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="812"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1033"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1254"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1475"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1696"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1917"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2138"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2359"/>
        <source>Y:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="216"/>
        <source>Display arcade scene in full screen mode or windowed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="219"/>
        <source>Full screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="229"/>
        <source>Use window resolution in full screen mode (for slow systems)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="232"/>
        <source>Use window resolution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="158"/>
        <source>Aspect ratio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="239"/>
        <source>Show frames per second counter in the lower left corner</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="242"/>
        <source>Show FPS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="276"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="590"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="634"/>
        <source>Keep aspect ratio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="683"/>
        <source>Virtual resolution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="711"/>
        <source>Virtual width of scene</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="744"/>
        <source>Virtual height of scene</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="253"/>
        <source>Enable anti aliasing on primitive drawing</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="256"/>
        <source>Primitive AA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="263"/>
        <source>Scale items smoothly</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="266"/>
        <source>Smooth item scaling</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="../../arcade/arcadesetupdialog.ui" line="2540"/>
        <source>°</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="329"/>
        <source>OpenGL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="343"/>
        <source>Enable direct rendering</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="346"/>
        <source>Direct rendering</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="353"/>
        <source>Enable enhanced OpenGL anti aliasing</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="356"/>
        <source>Anti aliasing</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="363"/>
        <source>Synchronize buffer swaps with screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="366"/>
        <source>Sync to screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="373"/>
        <source>Enable double buffering (avoids flicker)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="376"/>
        <source>Double buffering</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="383"/>
        <source>Enable depth buffering (Z-buffer)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="386"/>
        <source>Depth buffering</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="393"/>
        <source>Use RGBA color mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="396"/>
        <source>RGBA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="406"/>
        <source>Alpha channel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="413"/>
        <source>Enable multi sampling</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="416"/>
        <source>Multi sampling</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="423"/>
        <source>Enable OpenGL overlays</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="426"/>
        <source>Overlays</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="433"/>
        <source>Enable accumulator buffer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="436"/>
        <source>Accumulator buffer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="443"/>
        <source>Enable stencil buffer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="446"/>
        <source>Stencil buffer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="453"/>
        <source>Enable stereo buffer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="456"/>
        <source>Stereo buffer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="486"/>
        <source>Scene layout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="492"/>
        <source>Layout name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="505"/>
        <source>Select the layout you want to edit / use</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2303"/>
        <source>Control display of MAWS lookup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2306"/>
        <source>MAWS lookup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2584"/>
        <source>Apply settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2587"/>
        <source>&amp;Apply</source>
        <translation>&amp;Apply</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2601"/>
        <source>Restore currently applied settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2604"/>
        <source>&amp;Restore</source>
        <translation>&amp;Restore</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2618"/>
        <source>Reset to default settings (click &lt;i&gt;Restore&lt;/i&gt; to restore currently applied settings!)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2621"/>
        <source>&amp;Default</source>
        <translation>&amp;Default</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2645"/>
        <source>Close and apply settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2648"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2655"/>
        <source>Close and discard changes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2658"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="273"/>
        <source>Keep aspect ratio when resizing scene window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="403"/>
        <source>Use alpha channel information for transparency</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2537"/>
        <source>Scene rotation angle in degrees</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="518"/>
        <source>Items, placements and parameters</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="556"/>
        <source>Background image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="600"/>
        <source>Foreground image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="759"/>
        <source>Game list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="771"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="992"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1213"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1434"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1655"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1876"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2097"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2318"/>
        <source>Geometry</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="796"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1017"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1238"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1459"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1680"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1901"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2122"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2343"/>
        <source>X coordinate of item position</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="822"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1043"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1264"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1485"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1706"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1927"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2148"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2369"/>
        <source>Y coordinate of item position</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="118"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="698"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="838"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1059"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1280"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1501"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1722"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1943"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2164"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2385"/>
        <source>W:</source>
        <translation>W:</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="848"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1069"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1290"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1511"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1732"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1953"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2174"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2395"/>
        <source>Item width</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="141"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="731"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="864"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1085"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1306"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1527"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1748"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1969"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2190"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2411"/>
        <source>H:</source>
        <translation>H:</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="874"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1095"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1316"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1537"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1758"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1979"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2200"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2421"/>
        <source>Item height</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="886"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1107"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1328"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1549"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1770"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1991"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2212"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2433"/>
        <source>Use background for this item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="889"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1110"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1331"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1552"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1773"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1994"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2215"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2436"/>
        <source>Background</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="904"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1125"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1346"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1567"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1788"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2009"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2230"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2451"/>
        <source>Select background color</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="918"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1139"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1360"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1581"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1802"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2023"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2244"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2465"/>
        <source>T:</source>
        <translation>T:</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="925"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1146"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1367"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1588"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1809"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2030"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2251"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2472"/>
        <source>Select background transparency</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="928"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1149"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1370"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1591"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1812"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2033"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2254"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2475"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="938"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1159"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1380"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1601"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1822"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2043"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2264"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2485"/>
        <source>Use texture bitmap for background</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="941"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1162"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1383"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1604"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1825"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2046"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2267"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2488"/>
        <source>Texture</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="948"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1169"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1390"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1611"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1832"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2053"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2274"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2495"/>
        <source>Texture bitmap for background</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="961"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1182"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1403"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1624"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1845"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2066"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2287"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2508"/>
        <source>Browse texture bitmap for background</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="980"/>
        <source>Preview image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1201"/>
        <source>Flyer image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="641"/>
        <source>Arcade font</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="669"/>
        <source>Select font color</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="672"/>
        <source>Font color</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.cpp" line="25"/>
        <source>Machine list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.cpp" line="26"/>
        <source>Control display of machine list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="553"/>
        <source>Use a background image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="563"/>
        <source>Background image file (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="576"/>
        <source>Browse background image file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="587"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="631"/>
        <source>Keep image&apos;s aspect ratio when scaling</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="597"/>
        <source>Use a foreground image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="607"/>
        <source>Foreground image file (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="620"/>
        <source>Browse foreground image file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="756"/>
        <source>Control display of game list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="977"/>
        <source>Control display of preview image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1198"/>
        <source>Control display of flyer image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="79"/>
        <source>X coordinate of scene window position</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="102"/>
        <source>Y coordinate of scene window position</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="125"/>
        <source>Width of scene window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="148"/>
        <source>Height of scene window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="178"/>
        <source>X portion of scene aspect ratio (should be equal to screen&apos;s aspect ratio)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="201"/>
        <source>Y portion of scene aspect ratio (should be equal to screen&apos;s aspect ratio)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2530"/>
        <source>Scene rotation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="285"/>
        <source>Center arcade window on screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="288"/>
        <source>Center window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1419"/>
        <source>Control display of cabinet image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1422"/>
        <source>Cabinet image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1640"/>
        <source>Control display of controller image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1643"/>
        <source>Controller image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1861"/>
        <source>Control display of marquee image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1864"/>
        <source>Marquee image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2082"/>
        <source>Control display of title image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2085"/>
        <source>Title image</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ArcadeView</name>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="53"/>
        <source>QMC2 - ArcadeView</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="88"/>
        <source>ArcadeView: Cleaning up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="120"/>
        <source>ArcadeView: Switching to windowed mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="130"/>
        <source>ArcadeView: Resolution switching is not yet supported</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="146"/>
        <source>ArcadeView: Setting window size to %1x%2</source>
        <translation>ArcadeView: Setting window size to %1x%2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="156"/>
        <source>ArcadeView: Setting window position to %1, %2</source>
        <translation>ArcadeView: Setting window position to %1, %2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="170"/>
        <source>ArcadeView: This system does not appear to support OpenGL -- reverting to non-OpenGL / software renderer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="175"/>
        <source>ArcadeView: Using OpenGL renderer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="179"/>
        <source>ArcadeView: This system does not appear to support vertical syncing -- disabling SyncToScreen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="182"/>
        <source>ArcadeView: OpenGL: SyncToScreen: %1</source>
        <translation>ArcadeView: OpenGL: SyncToScreen: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="182"/>
        <location filename="../../arcade/arcadeview.cpp" line="184"/>
        <location filename="../../arcade/arcadeview.cpp" line="186"/>
        <location filename="../../arcade/arcadeview.cpp" line="188"/>
        <location filename="../../arcade/arcadeview.cpp" line="190"/>
        <location filename="../../arcade/arcadeview.cpp" line="192"/>
        <location filename="../../arcade/arcadeview.cpp" line="194"/>
        <location filename="../../arcade/arcadeview.cpp" line="196"/>
        <location filename="../../arcade/arcadeview.cpp" line="198"/>
        <location filename="../../arcade/arcadeview.cpp" line="204"/>
        <location filename="../../arcade/arcadeview.cpp" line="210"/>
        <location filename="../../arcade/arcadeview.cpp" line="212"/>
        <source>on</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="182"/>
        <location filename="../../arcade/arcadeview.cpp" line="184"/>
        <location filename="../../arcade/arcadeview.cpp" line="186"/>
        <location filename="../../arcade/arcadeview.cpp" line="188"/>
        <location filename="../../arcade/arcadeview.cpp" line="190"/>
        <location filename="../../arcade/arcadeview.cpp" line="192"/>
        <location filename="../../arcade/arcadeview.cpp" line="194"/>
        <location filename="../../arcade/arcadeview.cpp" line="196"/>
        <location filename="../../arcade/arcadeview.cpp" line="198"/>
        <location filename="../../arcade/arcadeview.cpp" line="204"/>
        <location filename="../../arcade/arcadeview.cpp" line="210"/>
        <location filename="../../arcade/arcadeview.cpp" line="212"/>
        <source>off</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="184"/>
        <source>ArcadeView: OpenGL: DoubleBuffer: %1</source>
        <translation>ArcadeView: OpenGL: DoubleBuffer: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="186"/>
        <source>ArcadeView: OpenGL: DepthBuffer: %1</source>
        <translation>ArcadeView: OpenGL: DepthBuffer: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="188"/>
        <source>ArcadeView: OpenGL: RGBA: %1</source>
        <translation>ArcadeView: OpenGL: RGBA: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="190"/>
        <source>ArcadeView: OpenGL: AlphaChannel: %1</source>
        <translation>ArcadeView: OpenGL: AlphaChannel: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="192"/>
        <source>ArcadeView: OpenGL: AccumulatorBuffer: %1</source>
        <translation>ArcadeView: OpenGL: AccumulatorBuffer: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="194"/>
        <source>ArcadeView: OpenGL: StencilBuffer: %1</source>
        <translation>ArcadeView: OpenGL: StencilBuffer: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="196"/>
        <source>ArcadeView: OpenGL: Stereo: %1</source>
        <translation>ArcadeView: OpenGL: Stereo: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="198"/>
        <source>ArcadeView: OpenGL: DirectRendering: %1</source>
        <translation>ArcadeView: OpenGL: DirectRendering: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="201"/>
        <source>ArcadeView: This system does not appear to support OpenGL overlays -- disabling OpenGL overlays</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="204"/>
        <source>ArcadeView: OpenGL: Overlay: %1</source>
        <translation>ArcadeView: OpenGL: Overlay: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="207"/>
        <source>ArcadeView: This system does not appear to support OpenGL multi sampling -- disabling MultiSample</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="210"/>
        <source>ArcadeView: OpenGL: MultiSample: %1</source>
        <translation>ArcadeView: OpenGL: MultiSample: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="212"/>
        <source>ArcadeView: OpenGL: AntiAliasing: %1</source>
        <translation>ArcadeView: OpenGL: AntiAliasing: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="222"/>
        <source>ArcadeView: Using software renderer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="227"/>
        <source>ArcadeView: X11: Screen number: %1</source>
        <translation>ArcadeView: X11: Screen number: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="228"/>
        <source>ArcadeView: X11: Color depth: %1</source>
        <translation>ArcadeView: X11: Color depth: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="229"/>
        <source>ArcadeView: X11: DPI-X: %1</source>
        <translation>ArcadeView: X11: DPI-X: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="230"/>
        <source>ArcadeView: X11: DPI-Y: %1</source>
        <translation>ArcadeView: X11: DPI-Y: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="231"/>
        <source>ArcadeView: X11: Compositing manager: %1</source>
        <translation>ArcadeView: X11: Compositing manager: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="231"/>
        <source>running</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="231"/>
        <source>not running</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="235"/>
        <source>ArcadeView: Screen geometry: %1x%2</source>
        <translation>ArcadeView: Screen geometry: %1x%2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="236"/>
        <source>ArcadeView: Virtual resolution: %1x%2</source>
        <translation>ArcadeView: Virtual resolution: %1x%2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="237"/>
        <source>ArcadeView: Selected aspect ratio: %1:%2</source>
        <translation>ArcadeView: Selected aspect ratio: %1:%2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="241"/>
        <source>ArcadeView: Virtual resolution doesn&apos;t fit aspect ratio -- scene coordinates may be stretched or compressed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="242"/>
        <source>ArcadeView: Aspect ratio will be maintained</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="242"/>
        <source>ArcadeView: Aspect ratio will not be maintained</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="243"/>
        <source>ArcadeView: FPS counter display %1</source>
        <translation>ArcadeView: FPS counter display %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="243"/>
        <location filename="../../arcade/arcadeview.cpp" line="246"/>
        <source>activated</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="243"/>
        <location filename="../../arcade/arcadeview.cpp" line="246"/>
        <source>deactivated</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="246"/>
        <source>ArcadeView: Primitive antialiasing %1</source>
        <translation>ArcadeView: Primitive antialiasing %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="250"/>
        <source>ArcadeView: Centering window on screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="254"/>
        <source>ArcadeView: Restoring saved window position</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="300"/>
        <source>ArcadeView: Adjusting window size to %1x%2 to maintain the aspect ratio</source>
        <translation>ArcadeView: Adjusting window size to %1x%2 to maintain the aspect ratio</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="281"/>
        <source>ArcadeView: Rendering screen shot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="290"/>
        <source>Saving screen shot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="238"/>
        <source>ArcadeView: Scene rotation angle: %1 degrees</source>
        <translation>ArcadeView: Scene rotation angle: %1 degrees</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="127"/>
        <source>ArcadeView: Switching to full screen mode</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AudioEffectDialog</name>
    <message>
        <location filename="../../audioeffects.cpp" line="35"/>
        <location filename="../../audioeffects.cpp" line="194"/>
        <location filename="../../audioeffects.cpp" line="199"/>
        <source>Enable effect &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="52"/>
        <source>Setup effect &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="186"/>
        <location filename="../../audioeffects.cpp" line="207"/>
        <source>Disable effect &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="195"/>
        <source>WARNING: audio player: can&apos;t insert effect &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="208"/>
        <source>WARNING: audio player: can&apos;t remove effect &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="14"/>
        <source>Audio effects</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="36"/>
        <source>Close audio effects dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="39"/>
        <source>Close</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="59"/>
        <location filename="../../audioeffects.ui" line="62"/>
        <source>List of available audio effects</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="81"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="86"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="91"/>
        <source>Enable</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="96"/>
        <source>Setup</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Cabinet</name>
    <message>
        <location filename="../../cabinet.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="56"/>
        <location filename="../../cabinet.cpp" line="57"/>
        <source>Game cabinet image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="59"/>
        <location filename="../../cabinet.cpp" line="60"/>
        <source>Machine cabinet image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="68"/>
        <location filename="../../cabinet.cpp" line="72"/>
        <source>FATAL: can&apos;t open cabinet file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open cabinet file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Waiting for data...</translation>
    </message>
</context>
<context>
    <name>Controller</name>
    <message>
        <location filename="../../controller.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="56"/>
        <location filename="../../controller.cpp" line="57"/>
        <source>Game controller image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="59"/>
        <location filename="../../controller.cpp" line="60"/>
        <source>Machine controller image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="68"/>
        <location filename="../../controller.cpp" line="72"/>
        <source>FATAL: can&apos;t open controller file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open controller file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Waiting for data...</translation>
    </message>
</context>
<context>
    <name>DemoModeDialog</name>
    <message>
        <location filename="../../demomode.cpp" line="105"/>
        <source>demo mode stopped</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="263"/>
        <location filename="../../demomode.cpp" line="107"/>
        <source>Run &amp;demo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="260"/>
        <location filename="../../demomode.cpp" line="108"/>
        <source>Run demo now</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="124"/>
        <source>please wait for reload to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="128"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../demomode.cpp" line="170"/>
        <source>demo mode started -- %n game(s) selected by filter</source>
        <translation>
            <numerusform>demo mode started -- %n game selected by filter</numerusform>
            <numerusform>demo mode started -- %n games selected by filter</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="172"/>
        <source>demo mode cannot start -- no games selected by filter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="176"/>
        <source>Stop &amp;demo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="177"/>
        <source>Stop demo now</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="240"/>
        <source>starting emulation in demo mode for &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="14"/>
        <source>Demo mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="26"/>
        <source>ROM state filter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="44"/>
        <source>Select ROM state C (correct)?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="67"/>
        <source>Select ROM state M (mostly correct)?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="90"/>
        <source>Select ROM state I (incorrect)?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="110"/>
        <source>Select ROM state N (not found)?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="130"/>
        <source>Select ROM state U (unknown)?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="152"/>
        <source>Seconds to run</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="159"/>
        <source>Number of seconds to run an emulator in demo mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="213"/>
        <source>Use only tagged games</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="216"/>
        <source>Tagged</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="289"/>
        <source>Pause (seconds)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="296"/>
        <source>Number of seconds to pause between emulator runs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="174"/>
        <source>Start emulators in full screen mode (otherwise use windowed mode)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="177"/>
        <source>Full screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="190"/>
        <source>Maximize emulators when in windowed mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="193"/>
        <source>Maximized</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="203"/>
        <source>Embed windowed emulators</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="206"/>
        <source>Embedded</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="231"/>
        <source>Close this dialog (and stop running demo)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="234"/>
        <source>&amp;Close</source>
        <translation>&amp;Close</translation>
    </message>
</context>
<context>
    <name>DetailSetup</name>
    <message>
        <location filename="../../detailsetup.ui" line="15"/>
        <source>Detail setup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="201"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="36"/>
        <source>List of available details</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="91"/>
        <source>List of active details and their order</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="68"/>
        <source>Activate selected details</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="106"/>
        <source>Deactivate selected details</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="173"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="21"/>
        <location filename="../../detailsetup.cpp" line="98"/>
        <source>Pre&amp;view</source>
        <translation>Pre&amp;view</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="22"/>
        <source>Game preview image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="24"/>
        <location filename="../../detailsetup.cpp" line="101"/>
        <source>Fl&amp;yer</source>
        <translation>Fl&amp;yer</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="25"/>
        <source>Game flyer image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="27"/>
        <source>Game &amp;info</source>
        <translation>Game &amp;info</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="28"/>
        <source>Game information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="30"/>
        <location filename="../../detailsetup.cpp" line="107"/>
        <source>Em&amp;ulator info</source>
        <translation>Em&amp;ulator info</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="31"/>
        <location filename="../../detailsetup.cpp" line="108"/>
        <source>Emulator information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="33"/>
        <location filename="../../detailsetup.cpp" line="110"/>
        <source>&amp;Configuration</source>
        <translation>&amp;Configuration</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="34"/>
        <location filename="../../detailsetup.cpp" line="111"/>
        <source>Emulator configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="36"/>
        <location filename="../../detailsetup.cpp" line="119"/>
        <source>Ca&amp;binet</source>
        <translation>Ca&amp;binet</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="37"/>
        <source>Arcade cabinet image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="39"/>
        <source>C&amp;ontroller</source>
        <translation>C&amp;ontroller</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="40"/>
        <source>Control panel image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="42"/>
        <source>Mar&amp;quee</source>
        <translation>Mar&amp;quee</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="43"/>
        <source>Marquee image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="45"/>
        <source>Titl&amp;e</source>
        <translation>Titl&amp;e</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="46"/>
        <source>Title screen image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="48"/>
        <source>MA&amp;WS</source>
        <translation>MA&amp;WS</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="51"/>
        <location filename="../../detailsetup.cpp" line="116"/>
        <source>&amp;PCB</source>
        <translation>&amp;PCB</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="52"/>
        <location filename="../../detailsetup.cpp" line="117"/>
        <source>PCB image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="58"/>
        <location filename="../../detailsetup.cpp" line="126"/>
        <source>&amp;YouTube</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="59"/>
        <location filename="../../detailsetup.cpp" line="127"/>
        <source>YouTube videos</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="99"/>
        <source>Machine preview image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="102"/>
        <source>Machine flyer image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="104"/>
        <source>Machine &amp;info</source>
        <translation>Machine &amp;info</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="105"/>
        <source>Machine information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="113"/>
        <source>De&amp;vices</source>
        <translation>De&amp;vices</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="114"/>
        <source>Device configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="54"/>
        <location filename="../../detailsetup.cpp" line="122"/>
        <source>Softwar&amp;e list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="55"/>
        <location filename="../../detailsetup.cpp" line="123"/>
        <source>Software list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="367"/>
        <source>MAWS configuration (1/2)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="379"/>
        <source>Enable MAWS quick download?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="30"/>
        <source>Available details</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="85"/>
        <source>Active details</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="120"/>
        <source>Move selected detail up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="137"/>
        <source>Move selected detail down</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="170"/>
        <source>Apply detail setup and close dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="198"/>
        <source>Cancel detail setup and close dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="184"/>
        <source>Apply detail setup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="187"/>
        <source>&amp;Apply</source>
        <translation>&amp;Apply</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="49"/>
        <source>MAWS page (web lookup)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="120"/>
        <source>Machine cabinet image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="377"/>
        <location filename="../../detailsetup.cpp" line="382"/>
        <source>Yes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="377"/>
        <source>No</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="379"/>
        <source>MAWS configuration (2/2)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="402"/>
        <source>Choose the YouTube cache directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="414"/>
        <source>FATAL: can&apos;t create new YouTube cache directory, path = %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="444"/>
        <source>INFO: the configuration tab can&apos;t be removed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="51"/>
        <source>Configure current detail</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="54"/>
        <source>Configure...</source>
        <translation>Configure...</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="368"/>
        <source>MAWS URL pattern (use %1 as placeholder for game ID):</source>
        <translation>MAWS URL pattern (use %1 as placeholder for game ID):</translation>
    </message>
</context>
<context>
    <name>DirectoryEditWidget</name>
    <message>
        <location filename="../../direditwidget.cpp" line="44"/>
        <source>Choose directory</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DocBrowser</name>
    <message>
        <location filename="../../docbrowser.ui" line="15"/>
        <location filename="../../docbrowser.cpp" line="86"/>
        <location filename="../../docbrowser.cpp" line="91"/>
        <location filename="../../docbrowser.cpp" line="93"/>
        <location filename="../../docbrowser.cpp" line="96"/>
        <source>MiniWebBrowser</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Embedder</name>
    <message>
        <location filename="../../embedder.cpp" line="92"/>
        <source>emulator #%1 released, window ID = 0x%2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="103"/>
        <source>emulator #%1 embedded, window ID = 0x%2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="128"/>
        <source>emulator #%1 closed, window ID = 0x%2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="148"/>
        <source>WARNING: embedder: unknown error, window ID = 0x%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="143"/>
        <source>WARNING: embedder: invalid window ID = 0x%1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EmbedderOptions</name>
    <message>
        <location filename="../../embedderopt.ui" line="14"/>
        <source>Embedder options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="31"/>
        <source>Snapshots</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="37"/>
        <source>Take a snapshot of the current window content -- hold to take snapshots repeatedly (every 100ms)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="40"/>
        <source>Take snapshot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="98"/>
        <source>Clear snapshots</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="101"/>
        <source>Clear</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="115"/>
        <source>Scale snapshots to the native resolution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="118"/>
        <source>Native resolution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="130"/>
        <source>Movies</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EmulatorOptionDelegate</name>
    <message>
        <location filename="../../emuopt.cpp" line="159"/>
        <source>All files (*)</source>
        <translation>All files (*)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="164"/>
        <location filename="../../emuopt.cpp" line="176"/>
        <source>Browse: </source>
        <translation>Browse: </translation>
    </message>
</context>
<context>
    <name>EmulatorOptions</name>
    <message>
        <location filename="../../emuopt.cpp" line="393"/>
        <location filename="../../emuopt.cpp" line="845"/>
        <location filename="../../emuopt.cpp" line="911"/>
        <location filename="../../emuopt.cpp" line="959"/>
        <location filename="../../emuopt.cpp" line="960"/>
        <location filename="../../emuopt.cpp" line="961"/>
        <location filename="../../emuopt.cpp" line="1053"/>
        <location filename="../../emuopt.cpp" line="1055"/>
        <location filename="../../emuopt.cpp" line="1057"/>
        <source>unknown</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="399"/>
        <source>Game specific emulator configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="405"/>
        <source>Global emulator configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="413"/>
        <source>Option / Attribute</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="414"/>
        <source>Value</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="858"/>
        <source>true</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="860"/>
        <source>false</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="806"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="809"/>
        <source>bool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="813"/>
        <source>int</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="817"/>
        <source>float</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="821"/>
        <source>float2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="825"/>
        <source>float3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="837"/>
        <source>choice</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="841"/>
        <source>string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="850"/>
        <source>Short name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="855"/>
        <source>Default</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="870"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="954"/>
        <source>creating template configuration map</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="991"/>
        <source>FATAL: XML error reading template: &apos;%1&apos; in file &apos;%2&apos; at line %3, column %4</source>
        <translation>FATAL: XML error reading template: &apos;%1&apos; in file &apos;%2&apos; at line %3, column %4</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1044"/>
        <source>template info: emulator = %1, version = %2, format = %3</source>
        <translation>template info: emulator = %1, version = %2, format = %3</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1051"/>
        <source>FATAL: can&apos;t open options template file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1054"/>
        <source>WARNING: couldn&apos;t determine emulator type of template</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1056"/>
        <source>WARNING: couldn&apos;t determine template version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1058"/>
        <source>WARNING: couldn&apos;t determine template format</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1068"/>
        <source>please wait for reload to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1077"/>
        <source>checking template configuration map against selected emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1109"/>
        <source>FATAL: can&apos;t start MAME executable within a reasonable time frame, giving up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1111"/>
        <source>FATAL: can&apos;t start MESS executable within a reasonable time frame, giving up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1167"/>
        <source>FATAL: can&apos;t create temporary file, please check emulator executable and permissions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1184"/>
        <location filename="../../emuopt.cpp" line="1193"/>
        <location filename="../../emuopt.cpp" line="1202"/>
        <location filename="../../emuopt.cpp" line="1211"/>
        <location filename="../../emuopt.cpp" line="1223"/>
        <location filename="../../emuopt.cpp" line="1240"/>
        <source>emulator uses a different default value for option &apos;%1&apos; (&apos;%2&apos; vs. &apos;%3&apos;); assumed option type is &apos;%4&apos;</source>
        <translation>emulator uses a different default value for option &apos;%1&apos; (&apos;%2&apos; vs. &apos;%3&apos;); assumed option type is &apos;%4&apos;</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1247"/>
        <source>template option &apos;%1&apos; is unknown to the emulator</source>
        <translation>template option &apos;%1&apos; is unknown to the emulator</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1257"/>
        <source>emulator option &apos;%1&apos; with default value &apos;%2&apos; is unknown to the template</source>
        <translation>emulator option &apos;%1&apos; with default value &apos;%2&apos; is unknown to the template</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1261"/>
        <source>done (checking template configuration map against selected emulator)</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../emuopt.cpp" line="1262"/>
        <source>check results: %n difference(s)</source>
        <translation>
            <numerusform>check results: %n difference</numerusform>
            <numerusform>check results: %n differences</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1370"/>
        <source>WARNING: ini-export: no writable ini-paths found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1374"/>
        <location filename="../../emuopt.cpp" line="1534"/>
        <source>Path selection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1375"/>
        <source>Multiple ini-paths detected. Select path(s) to export to:</source>
        <translation>Multiple ini-paths detected. Select path(s) to export to:</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1394"/>
        <source>WARNING: ini-export: no path selected (or invalid inipath)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1406"/>
        <location filename="../../emuopt.cpp" line="1566"/>
        <source>Waiting for data...</source>
        <translation>Waiting for data...</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1422"/>
        <source>FATAL: can&apos;t open export file for writing, path = %1</source>
        <translation>FATAL: can&apos;t open export file for writing, path = %1</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1428"/>
        <source>exporting %1 MAME configuration to %2</source>
        <translation>exporting %1 MAME configuration to %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1428"/>
        <location filename="../../emuopt.cpp" line="1430"/>
        <location filename="../../emuopt.cpp" line="1481"/>
        <location filename="../../emuopt.cpp" line="1483"/>
        <location filename="../../emuopt.cpp" line="1589"/>
        <location filename="../../emuopt.cpp" line="1591"/>
        <location filename="../../emuopt.cpp" line="1690"/>
        <location filename="../../emuopt.cpp" line="1692"/>
        <source>global</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1428"/>
        <location filename="../../emuopt.cpp" line="1481"/>
        <location filename="../../emuopt.cpp" line="1589"/>
        <location filename="../../emuopt.cpp" line="1690"/>
        <source>game-specific</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1430"/>
        <source>exporting %1 MESS configuration to %2</source>
        <translation>exporting %1 MESS configuration to %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1481"/>
        <source>done (exporting %1 MAME configuration to %2, elapsed time = %3)</source>
        <translation>done (exporting %1 MAME configuration to %2, elapsed time = %3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1483"/>
        <source>done (exporting %1 MESS configuration to %2, elapsed time = %3)</source>
        <translation>done (exporting %1 MESS configuration to %2, elapsed time = %3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1530"/>
        <source>WARNING: ini-import: no readable ini-paths found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1535"/>
        <source>Multiple ini-paths detected. Select path(s) to import from:</source>
        <translation>Multiple ini-paths detected. Select path(s) to import from:</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1554"/>
        <source>WARNING: ini-import: no path selected (or invalid inipath)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1583"/>
        <source>FATAL: can&apos;t open import file for reading, path = %1</source>
        <translation>FATAL: can&apos;t open import file for reading, path = %1</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1589"/>
        <source>importing %1 MAME configuration from %2</source>
        <translation>importing %1 MAME configuration from %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1591"/>
        <source>importing %1 MESS configuration from %2</source>
        <translation>importing %1 MESS configuration from %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1680"/>
        <source>WARNING: unknown option &apos;%1&apos; at line %2 (%3) ignored</source>
        <translation>WARNING: unknown option &apos;%1&apos; at line %2 (%3) ignored</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1683"/>
        <source>WARNING: invalid syntax at line %1 (%2) ignored</source>
        <translation>WARNING: invalid syntax at line %1 (%2) ignored</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1690"/>
        <source>done (importing %1 MAME configuration from %2, elapsed time = %3)</source>
        <translation>done (importing %1 MAME configuration from %2, elapsed time = %3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1692"/>
        <source>done (importing %1 MESS configuration from %2, elapsed time = %3)</source>
        <translation>done (importing %1 MESS configuration from %2, elapsed time = %3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1430"/>
        <location filename="../../emuopt.cpp" line="1483"/>
        <location filename="../../emuopt.cpp" line="1591"/>
        <location filename="../../emuopt.cpp" line="1692"/>
        <source>machine-specific</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="401"/>
        <source>Machine specific emulator configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="829"/>
        <source>file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="833"/>
        <source>directory</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FileEditWidget</name>
    <message>
        <location filename="../../fileeditwidget.cpp" line="49"/>
        <source>Choose file</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FileSystemModel</name>
    <message>
        <location filename="../../filesystemmodel.h" line="274"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="274"/>
        <source>Size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="470"/>
        <location filename="../../filesystemmodel.h" line="484"/>
        <source> KB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="473"/>
        <location filename="../../filesystemmodel.h" line="487"/>
        <source> MB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="476"/>
        <location filename="../../filesystemmodel.h" line="490"/>
        <source> GB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="479"/>
        <source> TB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="274"/>
        <source>Date modified</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Flyer</name>
    <message>
        <location filename="../../flyer.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="56"/>
        <location filename="../../flyer.cpp" line="57"/>
        <source>Game flyer image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="68"/>
        <location filename="../../flyer.cpp" line="72"/>
        <source>FATAL: can&apos;t open flyer file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open flyer file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Waiting for data...</translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="59"/>
        <location filename="../../flyer.cpp" line="60"/>
        <source>Machine flyer image</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Gamelist</name>
    <message>
        <location filename="../../gamelist.cpp" line="129"/>
        <location filename="../../gamelist.cpp" line="152"/>
        <location filename="../../gamelist.cpp" line="510"/>
        <location filename="../../gamelist.cpp" line="514"/>
        <location filename="../../gamelist.cpp" line="515"/>
        <location filename="../../gamelist.cpp" line="525"/>
        <location filename="../../gamelist.cpp" line="529"/>
        <location filename="../../gamelist.cpp" line="530"/>
        <location filename="../../gamelist.cpp" line="535"/>
        <location filename="../../gamelist.cpp" line="536"/>
        <location filename="../../gamelist.cpp" line="602"/>
        <location filename="../../gamelist.cpp" line="605"/>
        <source>unknown</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>good</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>bad</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>preliminary</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>supported</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>unsupported</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>imperfect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>yes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>no</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>baddump</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>nodump</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <source>vertical</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <source>horizontal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <source>raster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <location filename="../../gamelist.cpp" line="1492"/>
        <location filename="../../gamelist.cpp" line="1755"/>
        <source>Unknown</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>On</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>Off</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>audio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>Unused</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>original</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="159"/>
        <source>compatible</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="165"/>
        <location filename="../../gamelist.cpp" line="169"/>
        <source>FATAL: can&apos;t open icon file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open icon file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="387"/>
        <location filename="../../gamelist.cpp" line="389"/>
        <location filename="../../gamelist.cpp" line="392"/>
        <location filename="../../gamelist.cpp" line="394"/>
        <location filename="../../gamelist.cpp" line="1575"/>
        <location filename="../../gamelist.cpp" line="1838"/>
        <location filename="../../gamelist.cpp" line="2115"/>
        <location filename="../../gamelist.cpp" line="2951"/>
        <source>Waiting for data...</source>
        <translation>Waiting for data...</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="509"/>
        <source>FATAL: selected executable file is not MAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="534"/>
        <location filename="../../gamelist.cpp" line="598"/>
        <source>FATAL: can&apos;t create temporary file, please check emulator executable and permissions</source>
        <translation></translation>
    </message>
    <message>
        <source>done (determining emulator version and supported games, elapsed time = %1)</source>
        <translation type="obsolete">done (determining emulator version and supported games, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="609"/>
        <source>FATAL: couldn&apos;t determine emulator version, type identification string is &apos;%1&apos; -- please inform developers if you&apos;re sure that this is a valid MAME binary</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="611"/>
        <source>FATAL: couldn&apos;t determine emulator version, type identification string is &apos;%1&apos; -- please inform developers if you&apos;re sure that this is a valid MESS binary</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="668"/>
        <source>XML cache - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="781"/>
        <source>XML data - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="845"/>
        <source>verifying ROM status for &apos;%1&apos;</source>
        <translation>verifying ROM status for &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="887"/>
        <source>verifying ROM status for all games</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="989"/>
        <source>retrieving game information for &apos;%1&apos;</source>
        <translation>retrieving game information for &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1006"/>
        <source>WARNING: couldn&apos;t find game information for &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1008"/>
        <source>WARNING: couldn&apos;t find machine information for &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Source file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Clone of</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>ROM of</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Sample of</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Is device?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1036"/>
        <source>Year</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1043"/>
        <source>Manufacturer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1048"/>
        <location filename="../../gamelist.cpp" line="1475"/>
        <location filename="../../gamelist.cpp" line="1739"/>
        <source>ROM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>BIOS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>Size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>CRC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>SHA1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Merge</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Region</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>Offset</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1161"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <location filename="../../gamelist.cpp" line="1206"/>
        <source>Status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1058"/>
        <source>Device reference</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1063"/>
        <source>Chip</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1068"/>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1068"/>
        <source>Clock</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Width</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Height</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Refresh</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1082"/>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Sound</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1086"/>
        <source>Channels</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1091"/>
        <source>Input</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Service</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Tilt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Players</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Buttons</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Coins</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1114"/>
        <source>DIP switch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1121"/>
        <source>DIP value</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1126"/>
        <location filename="../../gamelist.cpp" line="1150"/>
        <location filename="../../gamelist.cpp" line="1171"/>
        <location filename="../../gamelist.cpp" line="1196"/>
        <location filename="../../gamelist.cpp" line="1223"/>
        <location filename="../../gamelist.cpp" line="1273"/>
        <source>Default</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1133"/>
        <source>Configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1138"/>
        <source>Mask</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1145"/>
        <source>Setting</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1150"/>
        <source>Value</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1157"/>
        <source>Driver</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Emulation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Color</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Graphic</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Cocktail</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Protection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Save state</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Palette size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1166"/>
        <source>BIOS set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1171"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1176"/>
        <source>Sample</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1181"/>
        <source>Disk</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>MD5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Index</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1191"/>
        <source>Adjuster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1201"/>
        <source>Software list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1211"/>
        <source>Category</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1218"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1235"/>
        <source>Interface</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1310"/>
        <source>WARNING: can&apos;t open ROM state cache, please check ROMs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1313"/>
        <source>loading ROM state from cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1315"/>
        <source>ROM states - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1337"/>
        <source>done (loading ROM state from cache, elapsed time = %1)</source>
        <translation>done (loading ROM state from cache, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1481"/>
        <location filename="../../gamelist.cpp" line="1482"/>
        <location filename="../../gamelist.cpp" line="1745"/>
        <location filename="../../gamelist.cpp" line="1746"/>
        <source>N/A</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2166"/>
        <source>%n game(s)</source>
        <translation>
            <numerusform>%n game</numerusform>
            <numerusform>%n games</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2166"/>
        <location filename="../../gamelist.cpp" line="2169"/>
        <source> and %n device(s) loaded</source>
        <translation>
            <numerusform> and %n device loaded</numerusform>
            <numerusform> and %n devices loaded</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2166"/>
        <location filename="../../gamelist.cpp" line="2169"/>
        <source>, %n BIOS set(s)</source>
        <translation>
            <numerusform>, %n BIOS set</numerusform>
            <numerusform>, %n BIOS sets</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2169"/>
        <source>%n machine(s)</source>
        <translation>
            <numerusform>%n machine</numerusform>
            <numerusform>%n machines</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2193"/>
        <location filename="../../gamelist.cpp" line="2923"/>
        <source>ROM state info: L:%1 C:%2 M:%3 I:%4 N:%5 U:%6</source>
        <translation>ROM state info: L:%1 C:%2 M:%3 I:%4 N:%5 U:%6</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2205"/>
        <source>WARNING: ROM state cache is incomplete or not up to date, triggering an automatic ROM check</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2208"/>
        <source>WARNING: ROM state cache is incomplete or not up to date, please re-check ROMs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1391"/>
        <location filename="../../gamelist.cpp" line="1400"/>
        <source>INFORMATION: the game list cache will now be updated due to a new format</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="421"/>
        <source>determining emulator version and supported sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="596"/>
        <source>done (determining emulator version and supported sets, elapsed time = %1)</source>
        <translation>done (determining emulator version and supported sets, elapsed time = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="621"/>
        <source>%n supported set(s)</source>
        <translation>
            <numerusform>%n supported set</numerusform>
            <numerusform>%n supported sets</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="623"/>
        <source>FATAL: couldn&apos;t determine the number of supported sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1393"/>
        <location filename="../../gamelist.cpp" line="1402"/>
        <source>INFORMATION: the machine list cache will now be updated due to a new format</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1417"/>
        <location filename="../../gamelist.cpp" line="1632"/>
        <source>Game data - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1473"/>
        <location filename="../../gamelist.cpp" line="1737"/>
        <source>ROM, CHD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1477"/>
        <location filename="../../gamelist.cpp" line="1741"/>
        <source>CHD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1493"/>
        <location filename="../../gamelist.cpp" line="1756"/>
        <location filename="../../gamelist.cpp" line="3549"/>
        <location filename="../../gamelist.cpp" line="3652"/>
        <source>?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2101"/>
        <location filename="../../gamelist.cpp" line="2103"/>
        <location filename="../../gamelist.cpp" line="2937"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>ascending</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2101"/>
        <location filename="../../gamelist.cpp" line="2103"/>
        <location filename="../../gamelist.cpp" line="2937"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>descending</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2135"/>
        <location filename="../../gamelist.cpp" line="2151"/>
        <source>restoring game selection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2228"/>
        <source>ROM state filter already active</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2233"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2238"/>
        <source>please wait for reload to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2247"/>
        <source>applying ROM state filter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2250"/>
        <source>State filter - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2304"/>
        <source>done (applying ROM state filter, elapsed time = %1)</source>
        <translation>done (applying ROM state filter, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2331"/>
        <source>loading favorites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2355"/>
        <source>done (loading favorites)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2368"/>
        <source>saving favorites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2384"/>
        <location filename="../../gamelist.cpp" line="2386"/>
        <source>FATAL: can&apos;t open favorites file for writing, path = %1</source>
        <translation>FATAL: can&apos;t open favorites file for writing, path = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2390"/>
        <source>done (saving favorites)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2399"/>
        <source>loading play history</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2422"/>
        <source>done (loading play history)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2435"/>
        <source>saving play history</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2451"/>
        <location filename="../../gamelist.cpp" line="2453"/>
        <source>FATAL: can&apos;t open play history file for writing, path = %1</source>
        <translation>FATAL: can&apos;t open play history file for writing, path = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2457"/>
        <source>done (saving play history)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2468"/>
        <source>L:</source>
        <translation>L:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2469"/>
        <source>C:</source>
        <translation>C:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2470"/>
        <source>M:</source>
        <translation>M:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2471"/>
        <source>I:</source>
        <translation>I:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2472"/>
        <source>N:</source>
        <translation>N:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2473"/>
        <source>U:</source>
        <translation>U:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2474"/>
        <source>S:</source>
        <translation>S:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="879"/>
        <location filename="../../gamelist.cpp" line="901"/>
        <source>ERROR: can&apos;t open ROM state cache for writing, path = %1</source>
        <translation>ERROR: can&apos;t open ROM state cache for writing, path = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="908"/>
        <source>ROM check - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2475"/>
        <source>T:</source>
        <translation>T:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2915"/>
        <source>done (verifying ROM status for &apos;%1&apos;, elapsed time = %2)</source>
        <translation>done (verifying ROM status for &apos;%1&apos;, elapsed time = %2)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2918"/>
        <source>done (verifying ROM status for all games, elapsed time = %1)</source>
        <translation>done (verifying ROM status for all games, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2937"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>ROM state</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3203"/>
        <source>ROM status for &apos;%1&apos; is &apos;%2&apos;</source>
        <translation>ROM status for &apos;%1&apos; is &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3293"/>
        <source>pre-caching icons from ZIP archive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3349"/>
        <source>done (pre-caching icons from ZIP archive, elapsed time = %1)</source>
        <translation>done (pre-caching icons from ZIP archive, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3363"/>
        <source>pre-caching icons from directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3416"/>
        <source>done (pre-caching icons from directory, elapsed time = %1)</source>
        <translation>done (pre-caching icons from directory, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3445"/>
        <source>loading catver.ini</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3451"/>
        <source>Catver.ini - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3489"/>
        <source>ERROR: can&apos;t open &apos;%1&apos; for reading -- no catver.ini data available</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3498"/>
        <source>done (loading catver.ini, elapsed time = %1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3499"/>
        <source>%1 category / %2 version records loaded</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3532"/>
        <source>Category view - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3635"/>
        <source>Version view - %p%</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <source>%n supported game(s)</source>
        <translation type="obsolete">
            <numerusform>%n supported game</numerusform>
            <numerusform>%n supported games</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="1338"/>
        <source>%n cached ROM state(s) loaded</source>
        <translation>
            <numerusform>%n cached ROM state loaded</numerusform>
            <numerusform>%n cached ROM states loaded</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n game(s) loaded</source>
        <translation type="obsolete">
            <numerusform>%n game loaded</numerusform>
            <numerusform>%n games loaded</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="3350"/>
        <location filename="../../gamelist.cpp" line="3417"/>
        <source>%n icon(s) loaded</source>
        <translation>
            <numerusform>%n icon loaded</numerusform>
            <numerusform>%n icons loaded</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3299"/>
        <location filename="../../gamelist.cpp" line="3378"/>
        <source>Icon cache - %p%</source>
        <translation>Icon cache - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>unused</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>cpu</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>vector</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>lcd</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="524"/>
        <source>FATAL: selected executable file is not MESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Is BIOS?</source>
        <translation>Is BIOS?</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Runnable</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1073"/>
        <source>Display</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Rotate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Flip-X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Pixel clock</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>H-Total</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>H-Bend</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>HB-Start</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>V-Total</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>V-Bend</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>VB-Start</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1068"/>
        <location filename="../../gamelist.cpp" line="1138"/>
        <location filename="../../gamelist.cpp" line="1235"/>
        <source>Tag</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1102"/>
        <source>Control</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Minimum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Maximum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Sensitivity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Key Delta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Reverse</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>joy4way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>joy8way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>trackball</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>joy2way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>doublejoy8way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>dial</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>paddle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>pedal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>stick</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>vjoy2way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>lightgun</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>doublejoy4way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>vdoublejoy2way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>doublejoy2way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>printer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>cdrom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>cartridge</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>cassette</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>quickload</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>floppydisk</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>serial</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>snapshot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1230"/>
        <source>Device</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1242"/>
        <source>Instance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1247"/>
        <source>Brief name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1254"/>
        <source>Extension</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1235"/>
        <source>Mandatory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="471"/>
        <location filename="../../gamelist.cpp" line="567"/>
        <source>FATAL: can&apos;t start MAME executable within a reasonable time frame, giving up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="473"/>
        <location filename="../../gamelist.cpp" line="569"/>
        <source>FATAL: can&apos;t start MESS executable within a reasonable time frame, giving up</source>
        <translation></translation>
    </message>
    <message>
        <source>done (determining emulator version and supported machines, elapsed time = %1)</source>
        <translation type="obsolete">done (determining emulator version and supported machines, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="603"/>
        <source>emulator info: type = %1, version = %2</source>
        <translation>emulator info: type = %1, version = %2</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="606"/>
        <source>FATAL: couldn&apos;t determine emulator type and version</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <source>%n supported machine(s)</source>
        <translation type="obsolete">
            <numerusform>%n supported machine</numerusform>
            <numerusform>%n supported machines</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="663"/>
        <source>loading XML game list data from cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="665"/>
        <source>loading XML machine list data from cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="706"/>
        <source>done (loading XML game list data from cache, elapsed time = %1)</source>
        <translation>done (loading XML game list data from cache, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="708"/>
        <source>WARNING: XML game list cache is incomplete, invalidating XML game list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="710"/>
        <source>done (loading XML machine list data from cache, elapsed time = %1)</source>
        <translation>done (loading XML machine list data from cache, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="712"/>
        <source>WARNING: XML machine list cache is incomplete, invalidating XML machine list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="776"/>
        <source>loading XML game list data and (re)creating cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="778"/>
        <source>loading XML machine list data and (re)creating cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="798"/>
        <source>WARNING: can&apos;t open XML game list cache for writing, please check permissions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="800"/>
        <source>WARNING: can&apos;t open XML machine list cache for writing, please check permissions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="889"/>
        <source>verifying ROM status for all machines</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="991"/>
        <source>retrieving machine information for &apos;%1&apos;</source>
        <translation>retrieving machine information for &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Optional</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1347"/>
        <source>processing game list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1349"/>
        <source>processing machine list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1384"/>
        <source>WARNING: couldn&apos;t determine emulator version of game list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1386"/>
        <source>WARNING: couldn&apos;t determine emulator version of machine list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1415"/>
        <source>loading game data from game list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1609"/>
        <source>done (loading game data from game list cache, elapsed time = %1)</source>
        <translation>done (loading game data from game list cache, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1630"/>
        <source>parsing game data and (re)creating game list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1634"/>
        <source>parsing machine data and (re)creating machine list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1643"/>
        <source>ERROR: can&apos;t open game list cache for writing, path = %1</source>
        <translation>ERROR: can&apos;t open game list cache for writing, path = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1645"/>
        <source>ERROR: can&apos;t open machine list cache for writing, path = %1</source>
        <translation>ERROR: can&apos;t open machine list cache for writing, path = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2101"/>
        <location filename="../../gamelist.cpp" line="2937"/>
        <source>sorting game list by %1 in %2 order</source>
        <translation>sorting game list by %1 in %2 order</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2103"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>sorting machine list by %1 in %2 order</source>
        <translation>sorting machine list by %1 in %2 order</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2137"/>
        <location filename="../../gamelist.cpp" line="2153"/>
        <source>restoring machine selection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2165"/>
        <source>done (processing game list, elapsed time = %1)</source>
        <translation>done (processing game list, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2168"/>
        <source>done (processing machine list, elapsed time = %1)</source>
        <translation>done (processing machine list, elapsed time = %1)</translation>
    </message>
    <message numerus="yes">
        <source>%n machine(s) loaded</source>
        <translation type="obsolete">
            <numerusform>%n machine loaded</numerusform>
            <numerusform>%n machines loaded</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2175"/>
        <source>WARNING: game list not fully parsed, invalidating game list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2178"/>
        <source>WARNING: machine list not fully parsed, invalidating machine list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2184"/>
        <source>WARNING: game list cache is out of date, invalidating game list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2187"/>
        <source>WARNING: machine list cache is out of date, invalidating machine list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2317"/>
        <source>saving game list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2318"/>
        <source>done (saving game list)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2320"/>
        <source>saving machine list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2321"/>
        <source>done (saving machine list)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2504"/>
        <source>done (loading XML game list data and (re)creating cache, elapsed time = %1)</source>
        <translation>done (loading XML game list data and (re)creating cache, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2506"/>
        <source>done (loading XML machine list data and (re)creating cache, elapsed time = %1)</source>
        <translation>done (loading XML machine list data and (re)creating cache, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2920"/>
        <source>done (verifying ROM status for all machines, elapsed time = %1)</source>
        <translation>done (verifying ROM status for all machines, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1419"/>
        <source>loading machine data from machine list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1611"/>
        <source>done (loading machine data from machine list cache, elapsed time = %1)</source>
        <translation>done (loading machine data from machine list cache, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1421"/>
        <location filename="../../gamelist.cpp" line="1636"/>
        <source>Machine data - %p%</source>
        <translation>Machine data - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1261"/>
        <source>RAM options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1265"/>
        <source>Option</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2738"/>
        <location filename="../../gamelist.cpp" line="2874"/>
        <location filename="../../gamelist.cpp" line="3188"/>
        <source>WARNING: can&apos;t find item map entry for &apos;%1&apos; - ROM state cannot be determined</source>
        <translation>WARNING: can&apos;t find item map entry for &apos;%1&apos; - ROM state cannot be determined</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2499"/>
        <location filename="../../gamelist.cpp" line="2682"/>
        <source>WARNING: emulator audit call didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation>WARNING: emulator audit call didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2499"/>
        <location filename="../../gamelist.cpp" line="2682"/>
        <source>normal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2499"/>
        <location filename="../../gamelist.cpp" line="2682"/>
        <source>crashed</source>
        <translation></translation>
    </message>
    <message>
        <source>ROM state info: C:%1 M:%2 I:%3 N:%4 U:%5</source>
        <translation type="obsolete">ROM state info: C:%1 M:%2 I:%3 N:%4 U:%5</translation>
    </message>
</context>
<context>
    <name>ImageChecker</name>
    <message>
        <location filename="../../imgcheck.cpp" line="104"/>
        <source>checking previews from ZIP archive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="106"/>
        <source>checking previews from directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="109"/>
        <source>Preview check - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="123"/>
        <location filename="../../imgcheck.cpp" line="124"/>
        <location filename="../../imgcheck.cpp" line="125"/>
        <location filename="../../imgcheck.cpp" line="447"/>
        <location filename="../../imgcheck.cpp" line="448"/>
        <location filename="../../imgcheck.cpp" line="449"/>
        <location filename="../../imgcheck.cpp" line="773"/>
        <location filename="../../imgcheck.cpp" line="774"/>
        <location filename="../../imgcheck.cpp" line="775"/>
        <source>&amp;Stop check</source>
        <translation>&amp;Stop check</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="70"/>
        <location filename="../../imgcheck.ui" line="243"/>
        <location filename="../../imgcheck.ui" line="323"/>
        <location filename="../../imgcheck.cpp" line="137"/>
        <location filename="../../imgcheck.cpp" line="461"/>
        <location filename="../../imgcheck.cpp" line="787"/>
        <source>Found: 0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="83"/>
        <location filename="../../imgcheck.ui" line="230"/>
        <location filename="../../imgcheck.ui" line="336"/>
        <location filename="../../imgcheck.cpp" line="139"/>
        <location filename="../../imgcheck.cpp" line="463"/>
        <location filename="../../imgcheck.cpp" line="789"/>
        <source>Missing: 0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="96"/>
        <location filename="../../imgcheck.ui" line="217"/>
        <location filename="../../imgcheck.ui" line="349"/>
        <location filename="../../imgcheck.cpp" line="141"/>
        <location filename="../../imgcheck.cpp" line="465"/>
        <location filename="../../imgcheck.cpp" line="791"/>
        <source>Obsolete: 0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="146"/>
        <source>check pass 1: found and missing previews</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="154"/>
        <location filename="../../imgcheck.cpp" line="176"/>
        <location filename="../../imgcheck.cpp" line="478"/>
        <location filename="../../imgcheck.cpp" line="500"/>
        <location filename="../../imgcheck.cpp" line="804"/>
        <location filename="../../imgcheck.cpp" line="826"/>
        <source>Found: %1</source>
        <translation>Found: %1</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="156"/>
        <location filename="../../imgcheck.cpp" line="179"/>
        <location filename="../../imgcheck.cpp" line="480"/>
        <location filename="../../imgcheck.cpp" line="503"/>
        <location filename="../../imgcheck.cpp" line="806"/>
        <location filename="../../imgcheck.cpp" line="829"/>
        <source>Missing: %1</source>
        <translation>Missing: %1</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="187"/>
        <location filename="../../imgcheck.cpp" line="511"/>
        <location filename="../../imgcheck.cpp" line="837"/>
        <source>check pass 2: obsolete files: reading ZIP directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="219"/>
        <location filename="../../imgcheck.cpp" line="232"/>
        <location filename="../../imgcheck.cpp" line="252"/>
        <location filename="../../imgcheck.cpp" line="261"/>
        <location filename="../../imgcheck.cpp" line="543"/>
        <location filename="../../imgcheck.cpp" line="556"/>
        <location filename="../../imgcheck.cpp" line="576"/>
        <location filename="../../imgcheck.cpp" line="585"/>
        <location filename="../../imgcheck.cpp" line="870"/>
        <location filename="../../imgcheck.cpp" line="883"/>
        <location filename="../../imgcheck.cpp" line="903"/>
        <location filename="../../imgcheck.cpp" line="912"/>
        <source>Obsolete: %1</source>
        <translation>Obsolete: %1</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="236"/>
        <location filename="../../imgcheck.cpp" line="560"/>
        <location filename="../../imgcheck.cpp" line="887"/>
        <source>check pass 2: obsolete files: reading directory structure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="278"/>
        <source>done (checking previews from ZIP archive, elapsed time = %1)</source>
        <translation>done (checking previews from ZIP archive, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="280"/>
        <source>done (checking previews from directory, elapsed time = %1)</source>
        <translation>done (checking previews from directory, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="281"/>
        <location filename="../../imgcheck.cpp" line="605"/>
        <location filename="../../imgcheck.cpp" line="932"/>
        <source>%1 found, %2 missing, %3 obsolete</source>
        <translation>%1 found, %2 missing, %3 obsolete</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="115"/>
        <location filename="../../imgcheck.cpp" line="287"/>
        <location filename="../../imgcheck.cpp" line="611"/>
        <location filename="../../imgcheck.cpp" line="938"/>
        <source>&amp;Check previews</source>
        <translation>&amp;Check previews</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="253"/>
        <location filename="../../imgcheck.cpp" line="288"/>
        <location filename="../../imgcheck.cpp" line="612"/>
        <location filename="../../imgcheck.cpp" line="939"/>
        <source>&amp;Check flyers</source>
        <translation>&amp;Check flyers</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="368"/>
        <location filename="../../imgcheck.cpp" line="289"/>
        <location filename="../../imgcheck.cpp" line="613"/>
        <location filename="../../imgcheck.cpp" line="940"/>
        <source>&amp;Check icons</source>
        <translation>&amp;Check icons</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="428"/>
        <source>checking flyers from ZIP archive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="430"/>
        <source>checking flyers from directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="433"/>
        <source>Flyer check - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="470"/>
        <source>check pass 1: found and missing flyers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="602"/>
        <source>done (checking flyers from ZIP archive, elapsed time = %1)</source>
        <translation>done (checking flyers from ZIP archive, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="604"/>
        <source>done (checking flyers from directory, elapsed time = %1)</source>
        <translation>done (checking flyers from directory, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="754"/>
        <source>checking icons from ZIP archive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="756"/>
        <source>checking icons from directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="759"/>
        <source>Icon check - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="796"/>
        <source>check pass 1: found and missing icons</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="929"/>
        <source>done (checking icons from ZIP archive, elapsed time = %1)</source>
        <translation>done (checking icons from ZIP archive, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="931"/>
        <source>done (checking icons from directory, elapsed time = %1)</source>
        <translation>done (checking icons from directory, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1072"/>
        <source>please wait for reload to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1082"/>
        <source>please wait for ROM state filter to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1087"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1092"/>
        <source>please wait for sample check to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1097"/>
        <source>stopping image check upon user request</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="15"/>
        <source>Check images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="36"/>
        <source>Close image check dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="39"/>
        <source>C&amp;lose</source>
        <translation>C&amp;lose</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="56"/>
        <source>&amp;Previews</source>
        <translation>&amp;Previews</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="112"/>
        <source>Check preview images / stop check</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="164"/>
        <source>Remove obsolete preview images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="167"/>
        <location filename="../../imgcheck.ui" line="266"/>
        <location filename="../../imgcheck.ui" line="381"/>
        <source>&amp;Remove obsolete</source>
        <translation>&amp;Remove obsolete</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="125"/>
        <location filename="../../imgcheck.ui" line="279"/>
        <location filename="../../imgcheck.ui" line="391"/>
        <source>Select &amp;game</source>
        <translation>Select &amp;game</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="181"/>
        <source>&amp;Flyers</source>
        <translation>&amp;Flyers</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="263"/>
        <source>Remove obsolete flyer images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="250"/>
        <source>Check flyer images / stop check</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="309"/>
        <source>&amp;Icons</source>
        <translation>&amp;Icons</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="401"/>
        <source>Clear cache before checking icons?</source>
        <translation>Clear cache before checking icons?</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="404"/>
        <source>C&amp;lear cache</source>
        <translation>C&amp;lear cache</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="378"/>
        <source>Remove obsolete icon images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="365"/>
        <source>Check icon images / stop check</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1077"/>
        <source>please wait for ROMAlyzer to finish the current analysis and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="70"/>
        <location filename="../../imgcheck.cpp" line="72"/>
        <source>Select machine</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="71"/>
        <location filename="../../imgcheck.cpp" line="73"/>
        <source>Select machine in machine list if selected in check lists?</source>
        <translation>Select machine in machine list if selected in check lists?</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="122"/>
        <location filename="../../imgcheck.ui" line="276"/>
        <location filename="../../imgcheck.ui" line="388"/>
        <source>Select game in game list if selected in check lists?</source>
        <translation>Select game in game list if selected in check lists?</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="117"/>
        <location filename="../../imgcheck.cpp" line="441"/>
        <location filename="../../imgcheck.cpp" line="767"/>
        <source>WARNING: game list not fully loaded, check results may be misleading</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="119"/>
        <location filename="../../imgcheck.cpp" line="443"/>
        <location filename="../../imgcheck.cpp" line="769"/>
        <source>WARNING: machine list not fully loaded, check results may be misleading</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ItemDownloader</name>
    <message>
        <location filename="../../downloaditem.cpp" line="98"/>
        <source>FATAL: can&apos;t open &apos;%1&apos; for writing</source>
        <translation>FATAL: can&apos;t open &apos;%1&apos; for writing</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="112"/>
        <location filename="../../downloaditem.cpp" line="190"/>
        <location filename="../../downloaditem.cpp" line="207"/>
        <location filename="../../downloaditem.cpp" line="238"/>
        <source>Source URL: %1</source>
        <translation>Source URL: %1</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="113"/>
        <location filename="../../downloaditem.cpp" line="191"/>
        <location filename="../../downloaditem.cpp" line="208"/>
        <location filename="../../downloaditem.cpp" line="239"/>
        <source>Local path: %2</source>
        <translation>Local path: %2</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="114"/>
        <location filename="../../downloaditem.cpp" line="192"/>
        <location filename="../../downloaditem.cpp" line="209"/>
        <location filename="../../downloaditem.cpp" line="240"/>
        <source>Status: %1</source>
        <translation>Status: %1</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="114"/>
        <source>initializing download</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="115"/>
        <location filename="../../downloaditem.cpp" line="193"/>
        <location filename="../../downloaditem.cpp" line="210"/>
        <location filename="../../downloaditem.cpp" line="241"/>
        <source>Total size: %1</source>
        <translation>Total size: %1</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="115"/>
        <source>unknown</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="116"/>
        <location filename="../../downloaditem.cpp" line="194"/>
        <location filename="../../downloaditem.cpp" line="211"/>
        <location filename="../../downloaditem.cpp" line="242"/>
        <source>Downloaded: %1 (%2%)</source>
        <translation>Downloaded: %1 (%2%)</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="117"/>
        <source>download started: URL = %1, local path = %2, reply ID = %3</source>
        <translation>download started: URL = %1, local path = %2, reply ID = %3</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="175"/>
        <source>Error #%1: </source>
        <translation>Error #%1: </translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="181"/>
        <source>download aborted: reason = %1, URL = %2, local path = %3, reply ID = %4</source>
        <translation>download aborted: reason = %1, URL = %2, local path = %3, reply ID = %4</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="192"/>
        <source>download aborted</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="209"/>
        <source>downloading</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="235"/>
        <source>download finished: URL = %1, local path = %2, reply ID = %3</source>
        <translation>download finished: URL = %1, local path = %2, reply ID = %3</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="240"/>
        <source>download finished</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ItemSelector</name>
    <message>
        <location filename="../../itemselect.ui" line="15"/>
        <source>Item selection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="21"/>
        <source>Select item(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="54"/>
        <source>Confirm selection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="57"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="67"/>
        <location filename="../../itemselect.ui" line="70"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Joystick</name>
    <message>
        <location filename="../../joystick.cpp" line="67"/>
        <source>ERROR: couldn&apos;t open SDL joystick #%1</source>
        <translation>ERROR: couldn&apos;t open SDL joystick #%1</translation>
    </message>
    <message>
        <location filename="../../joystick.cpp" line="23"/>
        <source>ERROR: couldn&apos;t initialize SDL joystick support</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>JoystickCalibrationWidget</name>
    <message>
        <location filename="../../options.cpp" line="3794"/>
        <source>Axis %1:</source>
        <translation>Axis %1:</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3803"/>
        <source>Current value of axis %1</source>
        <translation>Current value of axis %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3807"/>
        <source>DZ:</source>
        <translation>DZ:</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3809"/>
        <source>Deadzone of axis %1</source>
        <translation>Deadzone of axis %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3815"/>
        <source>S:</source>
        <translation>S:</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3817"/>
        <source>Sensitivity of axis %1</source>
        <translation>Sensitivity of axis %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3795"/>
        <source>Reset calibration of axis %1</source>
        <translation>Reset calibration of axis %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3790"/>
        <source>Enable/disable axis %1</source>
        <translation>Enable/disable axis %1</translation>
    </message>
</context>
<context>
    <name>JoystickFunctionScanner</name>
    <message>
        <location filename="../../joyfuncscan.ui" line="15"/>
        <location filename="../../joyfuncscan.ui" line="72"/>
        <location filename="../../joyfuncscan.cpp" line="24"/>
        <location filename="../../joyfuncscan.cpp" line="25"/>
        <source>Scanning joystick function</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="27"/>
        <source>&lt;&lt;&lt;&gt;&gt;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="43"/>
        <source>Accept joystick function</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="46"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="56"/>
        <source>Cancel remapping of joystick function</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="59"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>JoystickTestWidget</name>
    <message>
        <location filename="../../options.cpp" line="4038"/>
        <source>A%1: %v</source>
        <translation>A%1: %v</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4039"/>
        <source>Current value of axis %1</source>
        <translation>Current value of axis %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4049"/>
        <source>B%1</source>
        <translation>B%1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4050"/>
        <source>Current state of button %1</source>
        <translation>Current state of button %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4060"/>
        <source>H%1: 0</source>
        <translation>H%1: 0</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4061"/>
        <source>Current value of hat %1</source>
        <translation>Current value of hat %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4071"/>
        <source>T%1 DX: 0</source>
        <translation>T%1 DX: 0</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4072"/>
        <source>Current X-delta of trackball %1</source>
        <translation>Current X-delta of trackball %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4082"/>
        <source>T%1 DY: 0</source>
        <translation>T%1 DY: 0</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4083"/>
        <source>Current Y-delta of trackball %1</source>
        <translation>Current Y-delta of trackball %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4171"/>
        <source>H%1: %2</source>
        <translation>H%1: %2</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4205"/>
        <source>T%1 DX: %2</source>
        <translation>T%1 DX: %2</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4206"/>
        <source>T%1 DY: %2</source>
        <translation>T%1 DY: %2</translation>
    </message>
</context>
<context>
    <name>KeySequenceScanner</name>
    <message>
        <location filename="../../keyseqscan.cpp" line="21"/>
        <location filename="../../keyseqscan.cpp" line="22"/>
        <source>Scanning special key</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="15"/>
        <location filename="../../keyseqscan.ui" line="72"/>
        <location filename="../../keyseqscan.cpp" line="24"/>
        <location filename="../../keyseqscan.cpp" line="25"/>
        <source>Scanning shortcut</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="27"/>
        <source>&lt;&lt;&lt;&gt;&gt;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="43"/>
        <source>Accept key sequence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="46"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="56"/>
        <source>Cancel redefinition of key sequence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="59"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MESSDeviceConfigurator</name>
    <message>
        <location filename="../../messdevcfg.ui" line="15"/>
        <source>MESS device configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="37"/>
        <location filename="../../messdevcfg.cpp" line="1124"/>
        <location filename="../../messdevcfg.cpp" line="1129"/>
        <source>Active device configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="73"/>
        <location filename="../../messdevcfg.ui" line="76"/>
        <source>Device configuration menu</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="99"/>
        <location filename="../../messdevcfg.ui" line="102"/>
        <source>Name of device configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="184"/>
        <location filename="../../messdevcfg.ui" line="187"/>
        <source>Save current device configuration to list of available configurations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="208"/>
        <source>Device mappings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="229"/>
        <location filename="../../messdevcfg.ui" line="232"/>
        <source>Device setup of current configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="250"/>
        <source>Brief name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="279"/>
        <source>Slot options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="300"/>
        <location filename="../../messdevcfg.ui" line="303"/>
        <source>Available slot options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="316"/>
        <source>Slot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="321"/>
        <source>Option</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="330"/>
        <source>File chooser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="369"/>
        <location filename="../../messdevcfg.ui" line="372"/>
        <source>Save selected instance / file as a new device configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="392"/>
        <location filename="../../messdevcfg.ui" line="395"/>
        <source>Select the device instance the file is mapped to</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="411"/>
        <location filename="../../messdevcfg.ui" line="414"/>
        <source>Automatically select the first matching device instance when selecting a file with a valid extension</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="434"/>
        <location filename="../../messdevcfg.ui" line="437"/>
        <source>Filter files by allowed extensions for the current device instance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="457"/>
        <location filename="../../messdevcfg.ui" line="460"/>
        <source>Process ZIP contents on item activation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="480"/>
        <location filename="../../messdevcfg.ui" line="483"/>
        <source>Enter search string (case-insensitive)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="499"/>
        <location filename="../../messdevcfg.ui" line="502"/>
        <source>Clear search string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="519"/>
        <location filename="../../messdevcfg.ui" line="522"/>
        <source>Number of files scanned</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="544"/>
        <location filename="../../messdevcfg.ui" line="547"/>
        <source>Reload directory contents</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="564"/>
        <location filename="../../messdevcfg.ui" line="567"/>
        <source>Play the selected configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="584"/>
        <location filename="../../messdevcfg.ui" line="587"/>
        <source>Play the selected configuration (embedded)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="653"/>
        <source>Available device configurations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="665"/>
        <location filename="../../messdevcfg.ui" line="668"/>
        <source>List of available device configurations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="677"/>
        <location filename="../../messdevcfg.cpp" line="718"/>
        <location filename="../../messdevcfg.cpp" line="879"/>
        <location filename="../../messdevcfg.cpp" line="997"/>
        <source>No devices</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="245"/>
        <source>Device instance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="255"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="260"/>
        <source>Tag</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="265"/>
        <source>Extensions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="270"/>
        <source>File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="115"/>
        <location filename="../../messdevcfg.ui" line="118"/>
        <source>Create a new device configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="161"/>
        <location filename="../../messdevcfg.ui" line="164"/>
        <source>Remove current device configuration from list of available configurations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="138"/>
        <location filename="../../messdevcfg.ui" line="141"/>
        <source>Clone current device configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="754"/>
        <location filename="../../messdevcfg.cpp" line="756"/>
        <source>%1. copy of </source>
        <translation>%1. copy of </translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="962"/>
        <location filename="../../messdevcfg.cpp" line="1531"/>
        <source>%1. variant of </source>
        <translation>%1. variant of </translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="234"/>
        <location filename="../../messdevcfg.cpp" line="276"/>
        <source>Play selected game</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="146"/>
        <location filename="../../messdevcfg.cpp" line="385"/>
        <source>Reading slot info, please wait...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="149"/>
        <source>Enter configuration name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="166"/>
        <source>Enter search string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="235"/>
        <location filename="../../messdevcfg.cpp" line="277"/>
        <source>&amp;Play</source>
        <translation>&amp;Play</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="225"/>
        <source>Select default device directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="226"/>
        <source>&amp;Default device directory for &apos;%1&apos;...</source>
        <translation>&amp;Default device directory for &apos;%1&apos;...</translation>
    </message>
    <message>
        <source>&amp;Generate configurations for &apos;%1&apos;...</source>
        <translation type="obsolete">&amp;Generate configurations for &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="240"/>
        <location filename="../../messdevcfg.cpp" line="282"/>
        <source>Play selected game (embedded)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="241"/>
        <location filename="../../messdevcfg.cpp" line="283"/>
        <source>Play &amp;embedded</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="247"/>
        <source>Remove configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="248"/>
        <source>&amp;Remove configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="256"/>
        <source>Select a file to be mapped to this device instance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="257"/>
        <source>Select file...</source>
        <translation>Select file...</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="268"/>
        <source>Use as default directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="290"/>
        <location filename="../../messdevcfg.cpp" line="1368"/>
        <source>&amp;Open archive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="332"/>
        <location filename="../../messdevcfg.cpp" line="614"/>
        <location filename="../../messdevcfg.cpp" line="1516"/>
        <source>No devices available</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="387"/>
        <source>loading available system slots</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="408"/>
        <source>FATAL: can&apos;t start MESS executable within a reasonable time frame, giving up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="409"/>
        <source>Failed to read slot info</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="549"/>
        <source>done (loading available system slots, elapsed time = %1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="639"/>
        <source>not used</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1067"/>
        <source>Choose default device directory for &apos;%1&apos;</source>
        <translation>Choose default device directory for &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1368"/>
        <source>&amp;Close archive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1533"/>
        <source>Choose a unique configuration name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1533"/>
        <source>Unique configuration name:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1536"/>
        <source>Name conflict</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1536"/>
        <source>A configuration named &apos;%1&apos; already exists.

Do you want to choose a different name?</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MESSDeviceFileDelegate</name>
    <message>
        <location filename="../../messdevcfg.cpp" line="53"/>
        <location filename="../../messdevcfg.cpp" line="73"/>
        <source>All files</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="56"/>
        <location filename="../../messdevcfg.cpp" line="58"/>
        <source>Valid device files</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../qmc2main.cpp" line="406"/>
        <location filename="../../qmc2main.cpp" line="3530"/>
        <location filename="../../qmc2main.cpp" line="3566"/>
        <location filename="../../qmc2main.cpp" line="3755"/>
        <location filename="../../qmc2main.cpp" line="3844"/>
        <location filename="../../qmc2main.cpp" line="5237"/>
        <location filename="../../qmc2main.cpp" line="5476"/>
        <location filename="../../qmc2main.cpp" line="5515"/>
        <source>Default</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="488"/>
        <source>Toggle maximization of embedded emulator windows</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="530"/>
        <source>M&amp;achine list</source>
        <translation>M&amp;achine list</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="548"/>
        <location filename="../../qmc2main.cpp" line="549"/>
        <source>Play current machine (embedded)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="590"/>
        <location filename="../../qmc2main.cpp" line="591"/>
        <source>Loading machine list, please wait...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="592"/>
        <source>Search for machines (not case-sensitive)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="593"/>
        <source>Search for machines</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="644"/>
        <source>restoring main widget layout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="715"/>
        <source>Embed emulator widget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="716"/>
        <source>&amp;Embed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="722"/>
        <source>Terminate selected emulator(s) (sends TERM signal to emulator process(es))</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="723"/>
        <source>&amp;Terminate</source>
        <translation>&amp;Terminate</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="727"/>
        <source>Kill selected emulator(s) (sends KILL signal to emulator process(es))</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="728"/>
        <source>&amp;Kill</source>
        <translation>&amp;Kill</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="741"/>
        <location filename="../../qmc2main.cpp" line="793"/>
        <location filename="../../qmc2main.cpp" line="845"/>
        <location filename="../../qmc2main.cpp" line="904"/>
        <source>Play selected game</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2508"/>
        <location filename="../../qmc2main.cpp" line="745"/>
        <location filename="../../qmc2main.cpp" line="797"/>
        <location filename="../../qmc2main.cpp" line="849"/>
        <location filename="../../qmc2main.cpp" line="908"/>
        <source>&amp;Play</source>
        <translation>&amp;Play</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="752"/>
        <location filename="../../qmc2main.cpp" line="804"/>
        <location filename="../../qmc2main.cpp" line="856"/>
        <location filename="../../qmc2main.cpp" line="915"/>
        <source>Play selected game (embedded)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="754"/>
        <location filename="../../qmc2main.cpp" line="806"/>
        <location filename="../../qmc2main.cpp" line="858"/>
        <location filename="../../qmc2main.cpp" line="917"/>
        <source>Start selected machine (embedded)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3292"/>
        <location filename="../../qmc2main.cpp" line="756"/>
        <location filename="../../qmc2main.cpp" line="808"/>
        <location filename="../../qmc2main.cpp" line="860"/>
        <location filename="../../qmc2main.cpp" line="919"/>
        <source>Play &amp;embedded</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2692"/>
        <location filename="../../qmc2main.ui" line="2695"/>
        <location filename="../../qmc2main.cpp" line="763"/>
        <location filename="../../qmc2main.cpp" line="815"/>
        <location filename="../../qmc2main.cpp" line="926"/>
        <source>Add current game to favorites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2689"/>
        <location filename="../../qmc2main.cpp" line="767"/>
        <location filename="../../qmc2main.cpp" line="819"/>
        <location filename="../../qmc2main.cpp" line="930"/>
        <location filename="../../qmc2main.cpp" line="4180"/>
        <source>To &amp;favorites</source>
        <translation>To &amp;favorites</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2814"/>
        <location filename="../../qmc2main.ui" line="2817"/>
        <location filename="../../qmc2main.cpp" line="773"/>
        <location filename="../../qmc2main.cpp" line="825"/>
        <location filename="../../qmc2main.cpp" line="868"/>
        <location filename="../../qmc2main.cpp" line="936"/>
        <source>Check current game&apos;s ROM state</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="777"/>
        <location filename="../../qmc2main.cpp" line="829"/>
        <location filename="../../qmc2main.cpp" line="872"/>
        <location filename="../../qmc2main.cpp" line="940"/>
        <source>Check &amp;ROM state</source>
        <translation>Check &amp;ROM state</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="886"/>
        <source>Remove from favorites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="887"/>
        <location filename="../../qmc2main.cpp" line="955"/>
        <source>&amp;Remove</source>
        <translation>&amp;Remove</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="891"/>
        <source>Clear all favorites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="892"/>
        <location filename="../../qmc2main.cpp" line="960"/>
        <source>&amp;Clear</source>
        <translation>&amp;Clear</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="896"/>
        <source>Save favorites now</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="897"/>
        <location filename="../../qmc2main.cpp" line="965"/>
        <source>&amp;Save</source>
        <translation>&amp;Save</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="954"/>
        <source>Remove from played</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="959"/>
        <source>Clear all played</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="964"/>
        <source>Save play-history now</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1045"/>
        <location filename="../../qmc2main.cpp" line="1057"/>
        <source>Flip splitter orientation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1046"/>
        <location filename="../../qmc2main.cpp" line="1058"/>
        <source>&amp;Flip splitter orientation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1050"/>
        <source>Swap splitter&apos;s sub-layouts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1051"/>
        <source>&amp;Swap splitter&apos;s sub-layouts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1062"/>
        <source>Swap splitter&apos;s sub-widgets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1063"/>
        <source>&amp;Swap splitter&apos;s sub-widgets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="209"/>
        <location filename="../../qmc2main.ui" line="341"/>
        <location filename="../../qmc2main.ui" line="476"/>
        <location filename="../../qmc2main.ui" line="611"/>
        <location filename="../../qmc2main.cpp" line="1675"/>
        <location filename="../../qmc2main.cpp" line="1969"/>
        <location filename="../../qmc2main.cpp" line="1993"/>
        <location filename="../../qmc2main.cpp" line="3124"/>
        <location filename="../../qmc2main.cpp" line="3321"/>
        <location filename="../../qmc2main.cpp" line="3922"/>
        <location filename="../../qmc2main.cpp" line="4030"/>
        <location filename="../../qmc2main.cpp" line="4646"/>
        <location filename="../../qmc2main.cpp" line="4662"/>
        <location filename="../../qmc2main.cpp" line="5618"/>
        <location filename="../../qmc2main.cpp" line="5639"/>
        <location filename="../../qmc2main.cpp" line="7506"/>
        <location filename="../../qmc2main.cpp" line="7523"/>
        <location filename="../../qmc2main.cpp" line="7600"/>
        <location filename="../../qmc2main.cpp" line="7617"/>
        <source>Waiting for data...</source>
        <translation>Waiting for data...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1693"/>
        <location filename="../../qmc2main.cpp" line="1775"/>
        <location filename="../../qmc2main.cpp" line="1797"/>
        <source>please wait for ROM state filter to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1698"/>
        <location filename="../../qmc2main.cpp" line="2107"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1703"/>
        <location filename="../../qmc2main.cpp" line="1781"/>
        <location filename="../../qmc2main.cpp" line="1803"/>
        <source>please wait for image check to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1708"/>
        <location filename="../../qmc2main.cpp" line="1783"/>
        <location filename="../../qmc2main.cpp" line="1805"/>
        <source>please wait for sample check to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1741"/>
        <location filename="../../qmc2main.cpp" line="5111"/>
        <source>saving game selection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1777"/>
        <location filename="../../qmc2main.cpp" line="1799"/>
        <source>ROM verification already active</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1779"/>
        <location filename="../../qmc2main.cpp" line="1801"/>
        <location filename="../../qmc2main.cpp" line="2103"/>
        <location filename="../../qmc2main.cpp" line="2142"/>
        <location filename="../../qmc2main.cpp" line="2179"/>
        <location filename="../../qmc2main.cpp" line="2211"/>
        <location filename="../../qmc2main.cpp" line="3913"/>
        <source>please wait for reload to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1811"/>
        <location filename="../../qmc2main.cpp" line="4524"/>
        <location filename="../../qmc2main.cpp" line="4569"/>
        <location filename="../../qmc2main.cpp" line="4993"/>
        <location filename="../../qmc2main.cpp" line="5008"/>
        <location filename="../../qmc2main.cpp" line="5039"/>
        <source>Confirm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1812"/>
        <source>The ROM verification process may be very time-consuming.
It will overwrite existing cached data.

Do you really want to check all ROM states now?</source>
        <translation>The ROM verification process may be very time-consuming.
It will overwrite existing cached data.

Do you really want to check all ROM states now?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1823"/>
        <source>automatic ROM check triggered</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2025"/>
        <source>image cache cleared</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2036"/>
        <source>icon cache cleared</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2574"/>
        <location filename="../../qmc2main.cpp" line="2646"/>
        <source>About Qt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2850"/>
        <source>ERROR: no match found (?)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3552"/>
        <source>Emulator for this game</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3554"/>
        <source>Emulator for this machine</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3595"/>
        <source>Export to...</source>
        <translation>Export to...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3617"/>
        <location filename="../../qmc2main.cpp" line="3621"/>
        <source>&lt;inipath&gt;/%1.ini</source>
        <translation>&lt;inipath&gt;/%1.ini</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3618"/>
        <location filename="../../qmc2main.cpp" line="3622"/>
        <source>Select file...</source>
        <translation>Select file...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3697"/>
        <location filename="../../qmc2main.cpp" line="3704"/>
        <source>&lt;p&gt;No data available&lt;/p&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="789"/>
        <location filename="../../qmc2main.cpp" line="4155"/>
        <source>Embedded emulators</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4165"/>
        <source>Release emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4212"/>
        <source>WARNING: no matching window for emulator #%1 found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4218"/>
        <source>Embedding failed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4243"/>
        <location filename="../../qmc2main.cpp" line="4244"/>
        <source>Scanning pause key</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4763"/>
        <location filename="../../qmc2main.cpp" line="4822"/>
        <source>Choose export file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4763"/>
        <location filename="../../qmc2main.cpp" line="4790"/>
        <location filename="../../qmc2main.cpp" line="4822"/>
        <location filename="../../qmc2main.cpp" line="4858"/>
        <location filename="../../qmc2main.cpp" line="6383"/>
        <location filename="../../qmc2main.cpp" line="8113"/>
        <location filename="../../qmc2main.cpp" line="8155"/>
        <source>All files (*)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4790"/>
        <location filename="../../qmc2main.cpp" line="4858"/>
        <source>Choose import file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4820"/>
        <location filename="../../qmc2main.cpp" line="4855"/>
        <source>WARNING: invalid inipath</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4987"/>
        <source>stopping current processing upon user request</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4994"/>
        <source>Your configuration changes have not been applied yet.
Really quit?</source>
        <translation>Your configuration changes have not been applied yet.
Really quit?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5009"/>
        <source>There are one or more emulators still running.
Should they be killed on exit?</source>
        <translation>There are one or more emulators still running.
Should they be killed on exit?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5040"/>
        <source>There are one or more running downloads. Quit anyway?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5056"/>
        <source>cleaning up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5059"/>
        <source>aborting running downloads</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5071"/>
        <source>saving YouTube video info map</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5094"/>
        <source>done (saving YouTube video info map)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5096"/>
        <location filename="../../qmc2main.cpp" line="5098"/>
        <source>failed (saving YouTube video info map)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5126"/>
        <source>saving main widget layout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5213"/>
        <source>saving current game&apos;s favorite software</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5219"/>
        <source>saving current machine&apos;s favorite software</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5232"/>
        <source>destroying current game&apos;s emulator configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5244"/>
        <source>destroying global emulator options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5278"/>
        <source>destroying preview</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5282"/>
        <source>destroying flyer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5302"/>
        <source>destroying PCB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5306"/>
        <source>destroying about dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5319"/>
        <source>destroying MAWS quick download setup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5325"/>
        <source>destroying image checker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5330"/>
        <source>destroying sample checker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5351"/>
        <source>destroying demo mode dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5257"/>
        <source>disconnecting audio source from audio sink</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5270"/>
        <source>destroying YouTube video widget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5392"/>
        <source>destroying process manager</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5395"/>
        <source>killing %n running emulator(s) on exit</source>
        <translation>
            <numerusform>killing %n running emulator on exit</numerusform>
            <numerusform>killing %n running emulators on exit</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5398"/>
        <source>keeping %n running emulator(s) alive</source>
        <translation>
            <numerusform>keeping %n running emulator alive</numerusform>
            <numerusform>keeping %n running emulators alive</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5418"/>
        <source>destroying network access manager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5422"/>
        <source>so long and thanks for all the fish</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="15"/>
        <location filename="../../macros.h" line="424"/>
        <location filename="../../macros.h" line="430"/>
        <source>M.A.M.E. Catalog / Launcher II</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="711"/>
        <source>Search for games (not case-sensitive)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="714"/>
        <source>Search for games</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="147"/>
        <source>List of all supported games</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="154"/>
        <location filename="../../qmc2main.cpp" line="1073"/>
        <source>Game / Attribute</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="159"/>
        <location filename="../../qmc2main.ui" line="291"/>
        <location filename="../../qmc2main.ui" line="426"/>
        <location filename="../../qmc2main.ui" line="561"/>
        <location filename="../../qmc2main.cpp" line="1075"/>
        <location filename="../../qmc2main.cpp" line="1082"/>
        <location filename="../../qmc2main.cpp" line="1115"/>
        <location filename="../../qmc2main.cpp" line="1122"/>
        <location filename="../../qmc2main.cpp" line="1155"/>
        <location filename="../../qmc2main.cpp" line="1180"/>
        <source>Tag</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="164"/>
        <location filename="../../qmc2main.cpp" line="1077"/>
        <location filename="../../qmc2main.cpp" line="1084"/>
        <source>Icon / Value</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="169"/>
        <location filename="../../qmc2main.ui" line="301"/>
        <location filename="../../qmc2main.ui" line="436"/>
        <location filename="../../qmc2main.ui" line="571"/>
        <location filename="../../qmc2main.cpp" line="1087"/>
        <location filename="../../qmc2main.cpp" line="1127"/>
        <location filename="../../qmc2main.cpp" line="1159"/>
        <location filename="../../qmc2main.cpp" line="1184"/>
        <source>Year</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="174"/>
        <location filename="../../qmc2main.ui" line="306"/>
        <location filename="../../qmc2main.ui" line="441"/>
        <location filename="../../qmc2main.ui" line="576"/>
        <location filename="../../qmc2main.cpp" line="1089"/>
        <location filename="../../qmc2main.cpp" line="1129"/>
        <location filename="../../qmc2main.cpp" line="1161"/>
        <location filename="../../qmc2main.cpp" line="1186"/>
        <source>Manufacturer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="194"/>
        <location filename="../../qmc2main.ui" line="326"/>
        <location filename="../../qmc2main.ui" line="461"/>
        <location filename="../../qmc2main.ui" line="596"/>
        <location filename="../../qmc2main.cpp" line="1097"/>
        <location filename="../../qmc2main.cpp" line="1137"/>
        <location filename="../../qmc2main.cpp" line="1169"/>
        <location filename="../../qmc2main.cpp" line="1194"/>
        <source>Driver status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="286"/>
        <location filename="../../qmc2main.cpp" line="1113"/>
        <source>Game / Clones</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="296"/>
        <location filename="../../qmc2main.ui" line="431"/>
        <location filename="../../qmc2main.ui" line="566"/>
        <location filename="../../qmc2main.cpp" line="1117"/>
        <location filename="../../qmc2main.cpp" line="1124"/>
        <location filename="../../qmc2main.cpp" line="1157"/>
        <location filename="../../qmc2main.cpp" line="1182"/>
        <location filename="../../qmc2main.cpp" line="7809"/>
        <source>Icon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="680"/>
        <source>&amp;Search</source>
        <translation>&amp;Search</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="695"/>
        <location filename="../../qmc2main.ui" line="698"/>
        <source>Search result</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="729"/>
        <source>Favo&amp;rites</source>
        <translation>Favo&amp;rites</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="744"/>
        <location filename="../../qmc2main.ui" line="747"/>
        <source>Favorite games</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="774"/>
        <location filename="../../qmc2main.ui" line="777"/>
        <source>Games last played</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="811"/>
        <source>Emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="847"/>
        <location filename="../../qmc2main.ui" line="850"/>
        <source>L:Listed - C:Correct - M:Mostly correct - I:Incorrect - N:Not found - U:Unknown - S:Search - T:Tagged</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="891"/>
        <location filename="../../qmc2main.ui" line="894"/>
        <source>Indicator for current memory usage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1408"/>
        <source>Fl&amp;yer</source>
        <translation>Fl&amp;yer</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1489"/>
        <source>&amp;Configuration</source>
        <translation>&amp;Configuration</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1498"/>
        <source>&amp;Devices</source>
        <translation>&amp;Devices</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1525"/>
        <source>Mar&amp;quee</source>
        <translation>Mar&amp;quee</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1543"/>
        <source>MA&amp;WS</source>
        <translation>MA&amp;WS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1552"/>
        <source>&amp;PCB</source>
        <translation>&amp;PCB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1561"/>
        <source>Softwar&amp;e list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1570"/>
        <source>&amp;YouTube</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1611"/>
        <location filename="../../qmc2main.ui" line="1614"/>
        <source>Frontend log</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1635"/>
        <source>Emulator &amp;log</source>
        <translation>Emulator &amp;log</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1650"/>
        <location filename="../../qmc2main.ui" line="1653"/>
        <source>Emulator log</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1748"/>
        <source>MP&amp;3 player</source>
        <translation>MP&amp;3 player</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1769"/>
        <location filename="../../qmc2main.ui" line="1772"/>
        <source>Playlist (move items by dragging &amp; dropping them)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1968"/>
        <location filename="../../qmc2main.ui" line="1971"/>
        <source>Enter URL to add to playlist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1999"/>
        <location filename="../../qmc2main.ui" line="2002"/>
        <source>Setup available audio effects</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2097"/>
        <source>Dow&amp;nloads</source>
        <translation>Dow&amp;nloads</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2204"/>
        <source>Automatically remove successfully finished downloads from this list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2243"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2252"/>
        <source>&amp;Tools</source>
        <translation>&amp;Tools</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2319"/>
        <source>&amp;Check</source>
        <translation>&amp;Check</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2336"/>
        <source>&amp;View</source>
        <translation>&amp;View</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2349"/>
        <source>&amp;Tag</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2417"/>
        <source>Toolbar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2466"/>
        <source>Check &amp;samples...</source>
        <translation>Check &amp;samples...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2469"/>
        <location filename="../../qmc2main.ui" line="2472"/>
        <source>Check sample set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2475"/>
        <source>Ctrl+2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2487"/>
        <source>Check &amp;previews...</source>
        <translation>Check &amp;previews...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2490"/>
        <location filename="../../qmc2main.ui" line="2493"/>
        <source>Check preview images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2496"/>
        <source>Ctrl+3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2511"/>
        <location filename="../../qmc2main.ui" line="2514"/>
        <source>Play current game</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2517"/>
        <source>Ctrl+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2856"/>
        <source>Analyse ROM (tagged)...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2859"/>
        <source>Analyse ROM (tagged)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2862"/>
        <location filename="../../qmc2main.ui" line="2865"/>
        <source>Analyse all tagged sets with ROMAlyzer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3394"/>
        <source>Run external ROM tool (tagged)...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3397"/>
        <location filename="../../qmc2main.ui" line="3400"/>
        <source>Run tool to process ROM data externally for all tagged sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3403"/>
        <source>Ctrl+Shift+F9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3499"/>
        <source>Play (tagged)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3502"/>
        <location filename="../../qmc2main.ui" line="3505"/>
        <source>Play all tagged games</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3517"/>
        <source>Play embedded (tagged)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3520"/>
        <location filename="../../qmc2main.ui" line="3523"/>
        <source>Play all tagged games (embedded)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3535"/>
        <source>To favorites (tagged)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3538"/>
        <location filename="../../qmc2main.ui" line="3541"/>
        <source>Add all tagged games to favorites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3544"/>
        <source>Ctrl+Shift+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3556"/>
        <source>ROM state (tagged)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3559"/>
        <location filename="../../qmc2main.ui" line="3562"/>
        <source>Check ROM states of all tagged sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3565"/>
        <source>Ctrl+Shift+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3577"/>
        <source>E&amp;xit / Stop</source>
        <translation>E&amp;xit / Stop</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3580"/>
        <location filename="../../qmc2main.ui" line="3583"/>
        <source>Exit program / Stop any active processing</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3586"/>
        <source>Ctrl+X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3598"/>
        <source>Set tag</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3601"/>
        <location filename="../../qmc2main.ui" line="3604"/>
        <source>Set tag mark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3607"/>
        <source>Ctrl+Shift+T</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3619"/>
        <source>Unset tag</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3622"/>
        <location filename="../../qmc2main.ui" line="3625"/>
        <source>Unset tag mark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3628"/>
        <source>Ctrl+Shift+U</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3640"/>
        <source>Toggle tag</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3643"/>
        <location filename="../../qmc2main.ui" line="3646"/>
        <source>Toggle tag mark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3649"/>
        <source>Ctrl+Shift+G</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3661"/>
        <source>Tag all</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3664"/>
        <location filename="../../qmc2main.ui" line="3667"/>
        <source>Set tag mark for all sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3670"/>
        <source>Ctrl+Shift+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3682"/>
        <source>Untag all</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3685"/>
        <location filename="../../qmc2main.ui" line="3688"/>
        <source>Unset all tag marks</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3691"/>
        <source>Ctrl+Shift+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3703"/>
        <source>Invert all tags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3706"/>
        <location filename="../../qmc2main.ui" line="3709"/>
        <source>Invert all tag marks</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3712"/>
        <source>Ctrl+Shift+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2529"/>
        <source>&amp;Documentation...</source>
        <translation>&amp;Documentation...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2532"/>
        <location filename="../../qmc2main.ui" line="2535"/>
        <source>View online documentation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2538"/>
        <source>Ctrl+H</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2553"/>
        <location filename="../../qmc2main.ui" line="2556"/>
        <source>About M.A.M.E. Catalog / Launcher II</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2559"/>
        <source>Ctrl+A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2577"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2589"/>
        <source>Check &amp;ROMs...</source>
        <translation>Check &amp;ROMs...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2592"/>
        <location filename="../../qmc2main.ui" line="2595"/>
        <source>Check ROM collection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2598"/>
        <source>Ctrl+1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2610"/>
        <source>&amp;Options...</source>
        <translation>&amp;Options...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2613"/>
        <source>Options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2616"/>
        <location filename="../../qmc2main.ui" line="2619"/>
        <source>Frontend setup and global emulator configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2622"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2634"/>
        <source>&amp;Reload</source>
        <translation>&amp;Reload</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2643"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2651"/>
        <location filename="../../qmc2main.ui" line="2654"/>
        <location filename="../../qmc2main.ui" line="2657"/>
        <source>Clear image cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2660"/>
        <source>Ctrl+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2677"/>
        <source>Ctrl+T</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2698"/>
        <source>Ctrl+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2719"/>
        <source>F5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2740"/>
        <source>F6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2752"/>
        <source>Check &amp;flyers...</source>
        <translation>Check &amp;flyers...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2755"/>
        <location filename="../../qmc2main.ui" line="2758"/>
        <source>Check flyer images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2761"/>
        <source>Ctrl+4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2769"/>
        <location filename="../../qmc2main.ui" line="2772"/>
        <source>Clear icon cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2775"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2787"/>
        <source>Check &amp;icons...</source>
        <translation>Check &amp;icons...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2790"/>
        <location filename="../../qmc2main.ui" line="2793"/>
        <source>Check icon images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2796"/>
        <source>Ctrl+5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2808"/>
        <location filename="../../qmc2main.ui" line="2811"/>
        <source>ROM state</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2820"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3271"/>
        <source>Check template map</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3274"/>
        <location filename="../../qmc2main.ui" line="3277"/>
        <source>Check template map against the configuration options of the currently selected emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3280"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3295"/>
        <location filename="../../qmc2main.ui" line="3298"/>
        <source>Play current game (embedded)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3301"/>
        <source>Ctrl+Shift+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3313"/>
        <source>&amp;Demo mode...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3316"/>
        <location filename="../../qmc2main.ui" line="3319"/>
        <source>Open the demo mode dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2868"/>
        <source>Ctrl+Shift+D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3331"/>
        <source>By category</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3334"/>
        <location filename="../../qmc2main.ui" line="3337"/>
        <source>View games by category</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3340"/>
        <source>F7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3352"/>
        <source>By version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3355"/>
        <location filename="../../qmc2main.ui" line="3358"/>
        <source>View games by version they were added to the emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3361"/>
        <source>F8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3373"/>
        <source>Run external ROM tool...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3376"/>
        <location filename="../../qmc2main.ui" line="3379"/>
        <source>Run tool to process ROM data externally</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3382"/>
        <source>F9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3411"/>
        <location filename="../../qmc2main.ui" line="3414"/>
        <source>Clear YouTube cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3417"/>
        <source>Ctrl+Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3425"/>
        <source>Clear ROM state cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3428"/>
        <location filename="../../qmc2main.ui" line="3431"/>
        <source>Forcedly clear (remove) the ROM state cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3439"/>
        <source>Clear game list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3442"/>
        <location filename="../../qmc2main.ui" line="3445"/>
        <source>Forcedly clear (remove) the game list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3453"/>
        <source>Clear XML cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3456"/>
        <location filename="../../qmc2main.ui" line="3459"/>
        <source>Forcedly clear (remove) the XML cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3467"/>
        <source>Clear software list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3470"/>
        <location filename="../../qmc2main.ui" line="3473"/>
        <source>Forcedly clear (remove) the software list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3481"/>
        <source>Clear ALL emulator caches</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3484"/>
        <location filename="../../qmc2main.ui" line="3487"/>
        <source>Forcedly clear (remove) ALL emulator related caches</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="786"/>
        <location filename="../../qmc2main.cpp" line="838"/>
        <location filename="../../qmc2main.cpp" line="881"/>
        <location filename="../../qmc2main.cpp" line="949"/>
        <source>&amp;Analyse ROM...</source>
        <translation>&amp;Analyse ROM...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2835"/>
        <source>Analyse ROM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2844"/>
        <source>Ctrl+D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2880"/>
        <source>ROMAly&amp;zer...</source>
        <translation>ROMAly&amp;zer...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2883"/>
        <location filename="../../qmc2main.ui" line="2886"/>
        <source>Open ROMAlyzer dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2889"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5335"/>
        <source>destroying ROMAlyzer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1713"/>
        <location filename="../../qmc2main.cpp" line="1785"/>
        <location filename="../../qmc2main.cpp" line="1807"/>
        <location filename="../../qmc2main.cpp" line="1997"/>
        <source>please wait for ROMAlyzer to finish the current analysis and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2967"/>
        <source>F11</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="179"/>
        <location filename="../../qmc2main.ui" line="311"/>
        <location filename="../../qmc2main.ui" line="446"/>
        <location filename="../../qmc2main.ui" line="581"/>
        <location filename="../../qmc2main.cpp" line="1091"/>
        <location filename="../../qmc2main.cpp" line="1131"/>
        <location filename="../../qmc2main.cpp" line="1163"/>
        <location filename="../../qmc2main.cpp" line="1188"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4525"/>
        <source>Are you sure you want to clear the favorites list?</source>
        <translation>Are you sure you want to clear the favorites list?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4570"/>
        <source>Are you sure you want to clear the play history?</source>
        <translation>Are you sure you want to clear the play history?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3699"/>
        <location filename="../../qmc2main.cpp" line="3706"/>
        <location filename="../../qmc2main.cpp" line="3741"/>
        <location filename="../../qmc2main.cpp" line="3744"/>
        <source>No data available</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1432"/>
        <location filename="../../qmc2main.ui" line="1435"/>
        <source>Detailed game information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5359"/>
        <source>destroying game info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5809"/>
        <source>loading game info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5918"/>
        <source>WARNING: missing &apos;$end&apos; in game info DB %1</source>
        <translation>WARNING: missing &apos;$end&apos; in game info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5925"/>
        <source>WARNING: missing &apos;$bio&apos; in game info DB %1</source>
        <translation>WARNING: missing &apos;$bio&apos; in game info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5932"/>
        <source>WARNING: missing &apos;$info&apos; in game info DB %1</source>
        <translation>WARNING: missing &apos;$info&apos; in game info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5946"/>
        <source>WARNING: can&apos;t open game info DB %1</source>
        <translation>WARNING: can&apos;t open game info DB %1</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5955"/>
        <source>%n game info record(s) loaded</source>
        <translation>
            <numerusform>%n game info record loaded</numerusform>
            <numerusform>%n game info records loaded</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5954"/>
        <source>done (loading game info DB, elapsed time = %1)</source>
        <translation>done (loading game info DB, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4128"/>
        <source>FATAL: can&apos;t start XWININFO within a reasonable time frame, giving up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5377"/>
        <source>destroying emulator info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5992"/>
        <source>loading emulator info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6076"/>
        <source>WARNING: missing &apos;$end&apos; in emulator info DB %1</source>
        <translation>WARNING: missing &apos;$end&apos; in emulator info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6079"/>
        <source>WARNING: missing &apos;$mame&apos; in emulator info DB %1</source>
        <translation>WARNING: missing &apos;$mame&apos; in emulator info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6082"/>
        <source>WARNING: missing &apos;$info&apos; in emulator info DB %1</source>
        <translation>WARNING: missing &apos;$info&apos; in emulator info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6092"/>
        <source>WARNING: can&apos;t open emulator info DB %1</source>
        <translation>WARNING: can&apos;t open emulator info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6095"/>
        <source>done (loading emulator info DB, elapsed time = %1)</source>
        <translation>done (loading emulator info DB, elapsed time = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="6096"/>
        <source>%n emulator info record(s) loaded</source>
        <translation>
            <numerusform>%n emulator info record loaded</numerusform>
            <numerusform>%n emulator info records loaded</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1468"/>
        <location filename="../../qmc2main.ui" line="1471"/>
        <source>Detailed emulator information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1674"/>
        <source>E&amp;mulator control</source>
        <translation>E&amp;mulator control</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5957"/>
        <source>invalidating game info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6098"/>
        <source>invalidating emulator info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2943"/>
        <source>F12</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1399"/>
        <source>Pre&amp;view</source>
        <translation>Pre&amp;view</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="184"/>
        <location filename="../../qmc2main.ui" line="316"/>
        <location filename="../../qmc2main.ui" line="451"/>
        <location filename="../../qmc2main.ui" line="586"/>
        <location filename="../../qmc2main.cpp" line="1093"/>
        <location filename="../../qmc2main.cpp" line="1133"/>
        <location filename="../../qmc2main.cpp" line="1165"/>
        <location filename="../../qmc2main.cpp" line="1190"/>
        <source>ROM types</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="189"/>
        <location filename="../../qmc2main.ui" line="321"/>
        <location filename="../../qmc2main.ui" line="456"/>
        <location filename="../../qmc2main.ui" line="591"/>
        <location filename="../../qmc2main.cpp" line="1095"/>
        <location filename="../../qmc2main.cpp" line="1135"/>
        <location filename="../../qmc2main.cpp" line="1167"/>
        <location filename="../../qmc2main.cpp" line="1192"/>
        <source>Players</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="199"/>
        <location filename="../../qmc2main.ui" line="331"/>
        <location filename="../../qmc2main.ui" line="466"/>
        <location filename="../../qmc2main.ui" line="601"/>
        <location filename="../../qmc2main.cpp" line="1100"/>
        <location filename="../../qmc2main.cpp" line="1140"/>
        <location filename="../../qmc2main.cpp" line="1196"/>
        <source>Category</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="204"/>
        <location filename="../../qmc2main.ui" line="336"/>
        <location filename="../../qmc2main.ui" line="471"/>
        <location filename="../../qmc2main.ui" line="606"/>
        <location filename="../../qmc2main.cpp" line="1103"/>
        <location filename="../../qmc2main.cpp" line="1143"/>
        <location filename="../../qmc2main.cpp" line="1171"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="259"/>
        <location filename="../../qmc2main.ui" line="391"/>
        <source>Loading game list, please wait...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="414"/>
        <source>List of games viewed by category</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="421"/>
        <location filename="../../qmc2main.cpp" line="1153"/>
        <source>Category / Game</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="549"/>
        <source>List of games viewed by version they were added to the emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="556"/>
        <location filename="../../qmc2main.cpp" line="1178"/>
        <source>Version / Game</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="759"/>
        <source>Pl&amp;ayed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1790"/>
        <location filename="../../qmc2main.ui" line="1793"/>
        <location filename="../../qmc2main.ui" line="2979"/>
        <source>Previous track</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1804"/>
        <location filename="../../qmc2main.ui" line="1807"/>
        <location filename="../../qmc2main.ui" line="3000"/>
        <source>Next track</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1818"/>
        <location filename="../../qmc2main.ui" line="1821"/>
        <location filename="../../qmc2main.ui" line="3021"/>
        <source>Fast backward</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1841"/>
        <location filename="../../qmc2main.ui" line="1844"/>
        <location filename="../../qmc2main.ui" line="3039"/>
        <source>Fast forward</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2013"/>
        <location filename="../../qmc2main.ui" line="2016"/>
        <source>Start playing automatically when QMC2 has started</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2019"/>
        <source>Play on start</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2026"/>
        <location filename="../../qmc2main.ui" line="2029"/>
        <source>Select random tracks from playlist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2032"/>
        <source>Shuffle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2039"/>
        <location filename="../../qmc2main.ui" line="2042"/>
        <source>Automatically pause audio playback when at least one emulator is running</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2045"/>
        <source>Pause</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2061"/>
        <source>Fade in/out</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2207"/>
        <source>Remove finished</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2315"/>
        <source>&amp;Game</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2382"/>
        <source>&amp;Display</source>
        <translation>&amp;Display</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2985"/>
        <source>Ctrl+Alt+Left</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3006"/>
        <source>Ctrl+Alt+Right</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3027"/>
        <source>Ctrl+Alt+B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3045"/>
        <source>Ctrl+Alt+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3066"/>
        <source>Ctrl+Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3114"/>
        <source>Ctrl+Alt+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1915"/>
        <location filename="../../qmc2main.ui" line="1918"/>
        <source>Progress indicator for current track</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1924"/>
        <location filename="../../qmc2main.cpp" line="6560"/>
        <location filename="../../qmc2main.cpp" line="6570"/>
        <source>%vs (%ms total)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3090"/>
        <source>Ctrl+Alt+#</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1864"/>
        <location filename="../../qmc2main.ui" line="1867"/>
        <location filename="../../qmc2main.ui" line="3060"/>
        <source>Stop track</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1881"/>
        <location filename="../../qmc2main.ui" line="1884"/>
        <location filename="../../qmc2main.ui" line="3084"/>
        <source>Pause track</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1898"/>
        <location filename="../../qmc2main.ui" line="1901"/>
        <location filename="../../qmc2main.ui" line="3108"/>
        <source>Play track</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1692"/>
        <location filename="../../qmc2main.ui" line="1695"/>
        <source>Emulator control panel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1705"/>
        <source>#</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1730"/>
        <source>PID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1715"/>
        <location filename="../../qmc2main.ui" line="2140"/>
        <source>Status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1720"/>
        <source>LED0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1725"/>
        <source>LED1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1735"/>
        <source>Command</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6884"/>
        <location filename="../../qmc2main.cpp" line="6951"/>
        <location filename="../../qmc2main.cpp" line="6961"/>
        <source>running</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6900"/>
        <source>stopped</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4157"/>
        <location filename="../../qmc2main.cpp" line="6945"/>
        <location filename="../../qmc2main.cpp" line="6959"/>
        <source>paused</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="733"/>
        <location filename="../../qmc2main.cpp" line="4196"/>
        <source>Copy emulator command line to clipboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="734"/>
        <location filename="../../qmc2main.cpp" line="4197"/>
        <source>&amp;Copy command</source>
        <translation>&amp;Copy command</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6761"/>
        <source>WARNING: can&apos;t create SDLMAME output notifier FIFO, path = %1</source>
        <translation>WARNING: can&apos;t create SDLMAME output notifier FIFO, path = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6789"/>
        <source>SDLMAME output notifier FIFO created</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6793"/>
        <location filename="../../qmc2main.cpp" line="6796"/>
        <source>WARNING: can&apos;t open SDLMAME output notifier FIFO for reading, path = %1</source>
        <translation>WARNING: can&apos;t open SDLMAME output notifier FIFO for reading, path = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6917"/>
        <location filename="../../qmc2main.cpp" line="6988"/>
        <source>unhandled MAME output notification: game = %1, class = %2, what = %3, state = %4</source>
        <translation>unhandled MAME output notification: game = %1, class = %2, what = %3, state = %4</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1710"/>
        <source>Game / Notifier</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2274"/>
        <source>&amp;Clean up</source>
        <translation>&amp;Clean up</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2256"/>
        <source>&amp;Audio player</source>
        <translation>&amp;Audio player</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2550"/>
        <source>&amp;About...</source>
        <translation>&amp;About...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2571"/>
        <source>About &amp;Qt...</source>
        <translation>About &amp;Qt...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2668"/>
        <source>Recreate template map</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2710"/>
        <source>Full detail</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2832"/>
        <source>Analyse ROM...</source>
        <translation>Analyse ROM...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2731"/>
        <source>Parent / clones</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="94"/>
        <source>Parent / clone hierarchy (not filtered)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="282"/>
        <source>Parent / clone hierarchy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="526"/>
        <source>Creating category view, please wait...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="661"/>
        <source>Creating version view, please wait...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2734"/>
        <location filename="../../qmc2main.ui" line="2737"/>
        <source>View parent / clone hierarchy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2982"/>
        <source>Play previous track</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3003"/>
        <source>Play next track</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3024"/>
        <source>Fast backward within track</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3063"/>
        <source>Stop current track</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3087"/>
        <source>Pause current track</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3111"/>
        <source>Play current track</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3042"/>
        <source>Fast forward within track</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6383"/>
        <source>Select one or more audio files</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1954"/>
        <location filename="../../qmc2main.ui" line="1957"/>
        <source>Browse for tracks to add to playlist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1985"/>
        <location filename="../../qmc2main.ui" line="1988"/>
        <source>Remove selected tracks from playlist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1933"/>
        <location filename="../../qmc2main.ui" line="1936"/>
        <location filename="../../qmc2main.ui" line="2071"/>
        <location filename="../../qmc2main.ui" line="2074"/>
        <source>Audio player volume</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2055"/>
        <location filename="../../qmc2main.ui" line="2058"/>
        <source>Fade in and out on pause / resume</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6682"/>
        <source>audio player: track info: title = &apos;%1&apos;, artist = &apos;%2&apos;, album = &apos;%3&apos;, genre = &apos;%4&apos;</source>
        <translation>audio player: track info: title = &apos;%1&apos;, artist = &apos;%2&apos;, album = &apos;%3&apos;, genre = &apos;%4&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3125"/>
        <source>Raise volume</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3128"/>
        <source>Raise audio player volume</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3131"/>
        <source>Ctrl+Alt+PgUp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3139"/>
        <source>Lower volume</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3142"/>
        <source>Lower audio player volume</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3145"/>
        <source>Ctrl+Alt+PgDown</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3157"/>
        <source>&amp;Export ROM status...</source>
        <translation>&amp;Export ROM status...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3160"/>
        <location filename="../../qmc2main.ui" line="3163"/>
        <source>Export ROM status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3166"/>
        <source>Ctrl+E</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5340"/>
        <source>destroying ROM status exporter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6773"/>
        <source>WARNING: can&apos;t create SDLMESS output notifier FIFO, path = %1</source>
        <translation>WARNING: can&apos;t create SDLMESS output notifier FIFO, path = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6800"/>
        <source>SDLMESS output notifier FIFO created</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6804"/>
        <location filename="../../qmc2main.cpp" line="6807"/>
        <source>WARNING: can&apos;t open SDLMESS output notifier FIFO for reading, path = %1</source>
        <translation>WARNING: can&apos;t open SDLMESS output notifier FIFO for reading, path = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6919"/>
        <location filename="../../qmc2main.cpp" line="6990"/>
        <source>unhandled MESS output notification: game = %1, class = %2, what = %3, state = %4</source>
        <translation>unhandled MESS output notification: game = %1, class = %2, what = %3, state = %4</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="528"/>
        <location filename="../../qmc2main.cpp" line="1080"/>
        <source>Machine / Attribute</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="529"/>
        <location filename="../../qmc2main.cpp" line="1120"/>
        <source>Machine / Clones</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="531"/>
        <source>Machine &amp;info</source>
        <translation>Machine &amp;info</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1417"/>
        <source>Game &amp;info</source>
        <translation>Game &amp;info</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="55"/>
        <source>&amp;Game list</source>
        <translation>&amp;Game list</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="534"/>
        <location filename="../../qmc2main.cpp" line="535"/>
        <source>Favorite machines</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="536"/>
        <location filename="../../qmc2main.cpp" line="537"/>
        <source>Machines last played</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="532"/>
        <location filename="../../qmc2main.cpp" line="533"/>
        <source>Detailed machine info</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="556"/>
        <source>Machine / Notifier</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="538"/>
        <location filename="../../qmc2main.cpp" line="539"/>
        <source>Play current machine</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="558"/>
        <location filename="../../qmc2main.cpp" line="559"/>
        <location filename="../../qmc2main.cpp" line="765"/>
        <location filename="../../qmc2main.cpp" line="817"/>
        <location filename="../../qmc2main.cpp" line="928"/>
        <source>Add current machine to favorites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="562"/>
        <location filename="../../qmc2main.cpp" line="563"/>
        <source>Reload entire machine list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="564"/>
        <location filename="../../qmc2main.cpp" line="565"/>
        <source>View machine list with full detail</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="570"/>
        <location filename="../../qmc2main.cpp" line="571"/>
        <location filename="../../qmc2main.cpp" line="775"/>
        <location filename="../../qmc2main.cpp" line="827"/>
        <location filename="../../qmc2main.cpp" line="870"/>
        <location filename="../../qmc2main.cpp" line="938"/>
        <source>Check current machine&apos;s ROM state</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="572"/>
        <location filename="../../qmc2main.cpp" line="573"/>
        <source>Analyse current machine with ROMAlyzer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="574"/>
        <source>M&amp;achine</source>
        <translation>M&amp;achine</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="575"/>
        <source>Machine list with full detail (filtered)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="576"/>
        <location filename="../../qmc2main.cpp" line="577"/>
        <source>Select between detailed machine list and parent / clone hierarchy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="78"/>
        <location filename="../../qmc2main.ui" line="81"/>
        <source>Switch between detailed game list and parent / clone hierarchy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="85"/>
        <source>Game list with full detail (filtered)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2637"/>
        <location filename="../../qmc2main.ui" line="2640"/>
        <source>Reload entire game list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2671"/>
        <location filename="../../qmc2main.ui" line="2674"/>
        <source>Recreate template configuration map</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2713"/>
        <location filename="../../qmc2main.ui" line="2716"/>
        <source>View game list with full detail</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2838"/>
        <location filename="../../qmc2main.ui" line="2841"/>
        <source>Analyse current game with ROMAlyzer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5234"/>
        <source>destroying current machine&apos;s emulator configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="578"/>
        <location filename="../../qmc2main.cpp" line="579"/>
        <location filename="../../qmc2main.cpp" line="584"/>
        <source>Machine status indicator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1366"/>
        <location filename="../../qmc2main.ui" line="1369"/>
        <source>Game status indicator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="853"/>
        <source>&lt;b&gt;&lt;font color=black&gt;L:?&lt;/font&gt; &lt;font color=#00cc00&gt;C:?&lt;/font&gt; &lt;font color=#a2c743&gt;M:?&lt;/font&gt; &lt;font color=#f90000&gt;I:?&lt;/font&gt; &lt;font color=#7f7f7f&gt;N:?&lt;/font&gt; &lt;font color=#0000f9&gt;U:?&lt;/font&gt; &lt;font color=chocolate&gt;S:?&lt;/font&gt;&lt;/b&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="585"/>
        <source>Show vertical machine status indicator in machine details</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="586"/>
        <source>Show the machine status indicator only when the machine list is not visible due to the current layout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="587"/>
        <source>Show machine name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="589"/>
        <source>Show machine&apos;s description only when the machine list is not visible due to the current layout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5224"/>
        <source>saving current machine&apos;s device configurations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1562"/>
        <source>No devices</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1282"/>
        <source>&amp;Correct</source>
        <translation>&amp;Correct</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1288"/>
        <source>&amp;Mostly correct</source>
        <translation>&amp;Mostly correct</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1294"/>
        <source>&amp;Incorrect</source>
        <translation>&amp;Incorrect</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1300"/>
        <source>&amp;Not found</source>
        <translation>&amp;Not found</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1306"/>
        <source>&amp;Unknown</source>
        <translation>&amp;Unknown</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="112"/>
        <location filename="../../qmc2main.ui" line="115"/>
        <source>Toggle individual ROM states</source>
        <translation>Toggle individual ROM states</translation>
    </message>
    <message>
        <location filename="../../macros.h" line="427"/>
        <location filename="../../macros.h" line="433"/>
        <location filename="../../qmc2main.cpp" line="527"/>
        <source>M.E.S.S. Catalog / Launcher II</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3178"/>
        <source>QMC2 for SDLMAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3181"/>
        <location filename="../../qmc2main.ui" line="3184"/>
        <source>Launch QMC2 for SDLMAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3199"/>
        <source>QMC2 for SDLMESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3202"/>
        <location filename="../../qmc2main.ui" line="3205"/>
        <source>Launch QMC2 for SDLMESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3187"/>
        <source>Ctrl+Alt+1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3208"/>
        <source>Ctrl+Alt+2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2451"/>
        <location filename="../../qmc2main.cpp" line="2453"/>
        <location filename="../../qmc2main.cpp" line="2552"/>
        <location filename="../../qmc2main.cpp" line="2554"/>
        <source>variant &apos;%1&apos; launched</source>
        <translation>variant &apos;%1&apos; launched</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2491"/>
        <location filename="../../qmc2main.cpp" line="2493"/>
        <location filename="../../qmc2main.cpp" line="2592"/>
        <location filename="../../qmc2main.cpp" line="2594"/>
        <source>WARNING: failed to launch variant &apos;%1&apos;</source>
        <translation>WARNING: failed to launch variant &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="582"/>
        <location filename="../../qmc2main.cpp" line="583"/>
        <source>Progress indicator for machine list processing</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="866"/>
        <location filename="../../qmc2main.ui" line="869"/>
        <source>Progress indicator for game list processing</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2655"/>
        <location filename="../../qmc2main.cpp" line="2674"/>
        <source>WARNING: this feature is not yet working!</source>
        <translation>WARNING: this feature is not yet working!</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5196"/>
        <source>destroying arcade view</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2386"/>
        <source>&amp;Arcade</source>
        <translation>&amp;Arcade</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2904"/>
        <location filename="../../qmc2main.cpp" line="1016"/>
        <source>&amp;Setup...</source>
        <translation>&amp;Setup...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2907"/>
        <location filename="../../qmc2main.ui" line="2910"/>
        <source>Setup arcade mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2916"/>
        <source>Ctrl+Shift+A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5201"/>
        <source>destroying arcade setup dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5250"/>
        <source>destroying game list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5252"/>
        <source>destroying machine list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7645"/>
        <source>ArcadeView is not currently active, can&apos;t take screen shot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3219"/>
        <source>Show FPS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3222"/>
        <location filename="../../qmc2main.ui" line="3225"/>
        <source>Toggle FPS display</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3228"/>
        <source>Meta+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3240"/>
        <source>Screen shot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3243"/>
        <source>Save a screen shot from the current arcade scene</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3246"/>
        <source>Take screen shot from arcade scene</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3249"/>
        <source>Meta+F12</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2931"/>
        <source>&amp;Toggle arcade</source>
        <translation>&amp;Toggle arcade</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2934"/>
        <location filename="../../qmc2main.ui" line="2937"/>
        <source>Toggle arcade mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5361"/>
        <source>destroying machine info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5811"/>
        <source>loading machine info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5920"/>
        <source>WARNING: missing &apos;$end&apos; in machine info DB %1</source>
        <translation>WARNING: missing &apos;$end&apos; in machine info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5927"/>
        <source>WARNING: missing &apos;$bio&apos; in machine info DB %1</source>
        <translation>WARNING: missing &apos;$bio&apos; in machine info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5934"/>
        <source>WARNING: missing &apos;$info&apos; in machine info DB %1</source>
        <translation>WARNING: missing &apos;$info&apos; in machine info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5948"/>
        <source>WARNING: can&apos;t open machine info DB %1</source>
        <translation>WARNING: can&apos;t open machine info DB %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5959"/>
        <source>done (loading machine info DB, elapsed time = %1)</source>
        <translation>done (loading machine info DB, elapsed time = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5960"/>
        <source>%n machine info record(s) loaded</source>
        <translation>
            <numerusform>%n machine info record loaded</numerusform>
            <numerusform>%n machine info records loaded</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5962"/>
        <source>invalidating machine info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1596"/>
        <source>&amp;Front end log</source>
        <translation>&amp;Front end log</translation>
    </message>
    <message>
        <source>L:Listed - C:Correct - M:Mostly correct - I:Incorrect - N:Not found - U:Unknown - S:Search</source>
        <translation type="obsolete">L:Listed - C:Correct - M:Mostly correct - I:Incorrect - N:Not found - U:Unknown - S:Search</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="443"/>
        <source>QMC2 for MAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="444"/>
        <location filename="../../qmc2main.cpp" line="445"/>
        <source>Launch QMC2 for MAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="446"/>
        <source>QMC2 for MESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="447"/>
        <location filename="../../qmc2main.cpp" line="448"/>
        <source>Launch QMC2 for MESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2958"/>
        <source>Toggle &amp;full screen</source>
        <translation>Toggle &amp;full screen</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2961"/>
        <source>Toggle full screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2964"/>
        <source>Toggle full screen / windowed mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="743"/>
        <location filename="../../qmc2main.cpp" line="795"/>
        <location filename="../../qmc2main.cpp" line="847"/>
        <location filename="../../qmc2main.cpp" line="906"/>
        <source>Start selected machine</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="782"/>
        <location filename="../../qmc2main.cpp" line="834"/>
        <location filename="../../qmc2main.cpp" line="877"/>
        <location filename="../../qmc2main.cpp" line="945"/>
        <source>Analyse current game&apos;s ROM set with ROMAlyzer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="784"/>
        <location filename="../../qmc2main.cpp" line="836"/>
        <location filename="../../qmc2main.cpp" line="879"/>
        <location filename="../../qmc2main.cpp" line="947"/>
        <source>Analyse current machine&apos;s ROM set with ROMAlyzer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="972"/>
        <location filename="../../qmc2main.cpp" line="994"/>
        <location filename="../../qmc2main.cpp" line="1022"/>
        <source>Set tab position north</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="973"/>
        <location filename="../../qmc2main.cpp" line="995"/>
        <location filename="../../qmc2main.cpp" line="1023"/>
        <source>&amp;North</source>
        <translation>&amp;North</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="977"/>
        <location filename="../../qmc2main.cpp" line="999"/>
        <location filename="../../qmc2main.cpp" line="1027"/>
        <source>Set tab position south</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="978"/>
        <location filename="../../qmc2main.cpp" line="1000"/>
        <location filename="../../qmc2main.cpp" line="1028"/>
        <source>&amp;South</source>
        <translation>&amp;South</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="982"/>
        <location filename="../../qmc2main.cpp" line="1004"/>
        <location filename="../../qmc2main.cpp" line="1032"/>
        <source>Set tab position west</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="983"/>
        <location filename="../../qmc2main.cpp" line="1005"/>
        <location filename="../../qmc2main.cpp" line="1033"/>
        <source>&amp;West</source>
        <translation>&amp;West</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="987"/>
        <location filename="../../qmc2main.cpp" line="1009"/>
        <location filename="../../qmc2main.cpp" line="1037"/>
        <source>Set tab position east</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="988"/>
        <location filename="../../qmc2main.cpp" line="1010"/>
        <location filename="../../qmc2main.cpp" line="1038"/>
        <source>&amp;East</source>
        <translation>&amp;East</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="588"/>
        <source>Show machine&apos;s description at the bottom of any images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5286"/>
        <source>destroying cabinet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5290"/>
        <source>destroying controller</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1453"/>
        <source>Em&amp;ulator info</source>
        <translation>Em&amp;ulator info</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1507"/>
        <source>Ca&amp;binet</source>
        <translation>Ca&amp;binet</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1516"/>
        <source>C&amp;ontroller</source>
        <translation>C&amp;ontroller</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1534"/>
        <source>Titl&amp;e</source>
        <translation>Titl&amp;e</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5294"/>
        <source>destroying marquee</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5298"/>
        <source>destroying title</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1015"/>
        <source>Detail setup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5345"/>
        <source>destroying detail setup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3502"/>
        <source>Fetching MAWS page for &apos;%1&apos;, please wait...</source>
        <translation>Fetching MAWS page for &apos;%1&apos;, please wait...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3464"/>
        <source>MAWS page for &apos;%1&apos;</source>
        <translation>MAWS page for &apos;%1&apos;</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2046"/>
        <source>freed %n byte(s) in %1</source>
        <translation>
            <numerusform>freed %n byte in %1</numerusform>
            <numerusform>freed %n bytes in %1</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2046"/>
        <source>%n entry(s)</source>
        <translation>
            <numerusform>%n entry</numerusform>
            <numerusform>%n entries</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2048"/>
        <source>MAWS in-memory cache cleared (%1)</source>
        <translation>MAWS in-memory cache cleared (%1)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3257"/>
        <location filename="../../qmc2main.ui" line="3260"/>
        <source>Clear MAWS cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3263"/>
        <source>Ctrl+M</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2064"/>
        <location filename="../../qmc2main.cpp" line="2091"/>
        <source>removed %n byte(s) in %1</source>
        <translation>
            <numerusform>removed %n byte in %1</numerusform>
            <numerusform>removed %n bytes in %1</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2064"/>
        <location filename="../../qmc2main.cpp" line="2091"/>
        <source>%n file(s)</source>
        <translation>
            <numerusform>%n file</numerusform>
            <numerusform>%n files</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2065"/>
        <source>MAWS on-disk cache cleared (%1)</source>
        <translation>MAWS on-disk cache cleared (%1)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2112"/>
        <location filename="../../qmc2main.ui" line="2115"/>
        <source>List of active/inactive downloads</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2145"/>
        <source>Progress</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2153"/>
        <location filename="../../qmc2main.ui" line="2156"/>
        <source>Clear finished / stopped downloads from list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2170"/>
        <location filename="../../qmc2main.ui" line="2173"/>
        <source>Reload selected downloads</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2187"/>
        <location filename="../../qmc2main.ui" line="2190"/>
        <source>Stop selected downloads</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5310"/>
        <source>destroying MiniWebBrowser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5315"/>
        <source>destroying MAWS lookup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8155"/>
        <source>Choose file to store download</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5554"/>
        <source>loading style sheet &apos;%1&apos;</source>
        <translation>loading style sheet &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5563"/>
        <source>FATAL: can&apos;t open style sheet file &apos;%1&apos;, please check</source>
        <translation>FATAL: can&apos;t open style sheet file &apos;%1&apos;, please check</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5566"/>
        <source>removing current style sheet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7790"/>
        <source>Quick download links for MAWS data usable by QMC2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7952"/>
        <source>Setup...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7815"/>
        <source>Cabinet art</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="286"/>
        <location filename="../../qmc2main.cpp" line="300"/>
        <source>last message repeated %n time(s)</source>
        <translation>
            <numerusform>last message repeated %n time</numerusform>
            <numerusform>last message repeated %n times</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="467"/>
        <source>Toggle automatic pausing of embedded emulators (hold down for menu)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="476"/>
        <source>Scan the pause key used by the emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="477"/>
        <source>Scan pause key...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="521"/>
        <source>Game</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="540"/>
        <location filename="../../qmc2main.cpp" line="541"/>
        <source>Play all tagged machines</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="542"/>
        <location filename="../../qmc2main.cpp" line="543"/>
        <source>Clear machine list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="544"/>
        <location filename="../../qmc2main.cpp" line="545"/>
        <source>Forcedly clear (remove) the machine list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="546"/>
        <source>List of all supported machines</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="550"/>
        <location filename="../../qmc2main.cpp" line="551"/>
        <source>Play all tagged machines (embedded)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="554"/>
        <source>Machine</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="560"/>
        <location filename="../../qmc2main.cpp" line="561"/>
        <source>Add all tagged machines to favorites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1216"/>
        <source>Enter search string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1392"/>
        <source>sorry, devices cannot run standalone</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1544"/>
        <source>No devices available</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1719"/>
        <source>game list reload is already active</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1721"/>
        <source>machine list reload is already active</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2092"/>
        <source>YouTube on-disk cache cleared (%1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2124"/>
        <source>ROM state cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2126"/>
        <source>WARNING: cannot remove the ROM state cache file &apos;%1&apos;, please check permissions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2131"/>
        <source>triggering an automatic ROM check on next reload</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2160"/>
        <source>game list cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2162"/>
        <source>WARNING: cannot remove the game list cache file &apos;%1&apos;, please check permissions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2165"/>
        <source>machine list cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2167"/>
        <source>WARNING: cannot remove the machine list cache file &apos;%1&apos;, please check permissions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2196"/>
        <source>XML cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2198"/>
        <source>WARNING: cannot remove the XML cache file &apos;%1&apos;, please check permissions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2228"/>
        <source>software list cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2230"/>
        <source>WARNING: cannot remove the software list cache file &apos;%1&apos;, please check permissions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4171"/>
        <location filename="../../qmc2main.cpp" line="4172"/>
        <source>Toggle embedder options (hold down for menu)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4179"/>
        <source>To favorites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4185"/>
        <source>Terminate emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4186"/>
        <source>&amp;Terminate emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4190"/>
        <source>Kill emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4191"/>
        <source>&amp;Kill emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4219"/>
        <source>Couldn&apos;t find the window ID of one or more
emulator(s) within a reasonable timeframe.

Retry embedding?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4229"/>
        <source>Information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4229"/>
        <source>Sorry, the emulator meanwhile died a sorrowful death :(.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5260"/>
        <source>destroying audio effects dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5745"/>
        <source>loading YouTube video info map</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5762"/>
        <source>YouTube index - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5791"/>
        <source>done (loading YouTube video info map)</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5792"/>
        <source>%n video info record(s) loaded</source>
        <translation>
            <numerusform>%n video info record loaded</numerusform>
            <numerusform>%n video info records loaded</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5843"/>
        <source>Game info - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5845"/>
        <source>Machine info - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6021"/>
        <source>Emu info - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6395"/>
        <source>Add URL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6395"/>
        <source>Enter valid MP3 stream URL:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6696"/>
        <source>Buffering %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8067"/>
        <source>Cabinet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8069"/>
        <source>Controller</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8075"/>
        <source>PCB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8652"/>
        <location filename="../../qmc2main.cpp" line="8686"/>
        <source>Play tagged - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8722"/>
        <source>Add favorites - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8755"/>
        <location filename="../../qmc2main.cpp" line="8783"/>
        <location filename="../../qmc2main.cpp" line="8823"/>
        <source>please wait for current activity to finish and try again (this batch-mode operation can only run exclusively)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8829"/>
        <source>ROM tool tagged - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8963"/>
        <source>Tag - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9005"/>
        <source>Untag - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9047"/>
        <source>Invert tag - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8071"/>
        <source>Flyer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8073"/>
        <source>Marquee</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7846"/>
        <source>No cabinet art</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7858"/>
        <source>Previews</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7904"/>
        <location filename="../../qmc2main.cpp" line="8077"/>
        <source>preview</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7889"/>
        <source>No previews</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7911"/>
        <source>Titles</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7944"/>
        <location filename="../../qmc2main.cpp" line="8079"/>
        <source>title</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7929"/>
        <source>No titles</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8113"/>
        <source>Choose file to store the icon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8119"/>
        <source>icon image for &apos;%1&apos; stored as &apos;%2&apos;</source>
        <translation>icon image for &apos;%1&apos; stored as &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8126"/>
        <source>FATAL: icon image for &apos;%1&apos; couldn&apos;t be stored as &apos;%2&apos;</source>
        <translation>FATAL: icon image for &apos;%1&apos; couldn&apos;t be stored as &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Physical memory:</source>
        <translation>Physical memory:</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Total: %1 MB</source>
        <translation>Total: %1 MB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Free: %1 MB</source>
        <translation>Free: %1 MB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Used: %1 MB</source>
        <translation>Used: %1 MB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4099"/>
        <source>emulator #%1 is already embedded</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4148"/>
        <source>WARNING: multiple windows for emulator #%1 found, choosing window ID %2 for embedding</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4156"/>
        <source>embedding emulator #%1, window ID = %2</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Marquee</name>
    <message>
        <location filename="../../marquee.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="56"/>
        <location filename="../../marquee.cpp" line="57"/>
        <source>Game marquee image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="59"/>
        <location filename="../../marquee.cpp" line="60"/>
        <source>Machine marquee image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="68"/>
        <location filename="../../marquee.cpp" line="72"/>
        <source>FATAL: can&apos;t open marquee file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open marquee file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Waiting for data...</translation>
    </message>
</context>
<context>
    <name>MawsQuickDownloadSetup</name>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="14"/>
        <source>MAWS quick download setup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="395"/>
        <source>Previews</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="424"/>
        <source>Path to store preview images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="437"/>
        <source>Browse path to store preview images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="32"/>
        <source>Flyers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="20"/>
        <source>Icons and cabinet art</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="61"/>
        <source>Path to store flyer images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="74"/>
        <source>Browse path to store flyer images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="91"/>
        <source>Cabinets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="120"/>
        <source>Path to store cabinet images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="133"/>
        <source>Browse path to store cabinet images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="150"/>
        <source>Controllers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="179"/>
        <source>Path to store controller images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="192"/>
        <source>Browse path to store controller images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="209"/>
        <source>Marquees</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="238"/>
        <source>Path to store marquee images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="251"/>
        <source>Browse path to store marquee images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="383"/>
        <source>Previews and titles</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="498"/>
        <source>Titles</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="527"/>
        <source>Path to store title images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="540"/>
        <source>Browse path to store title images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="268"/>
        <source>PCBs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="297"/>
        <source>Path to store PCB images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="310"/>
        <source>Browse path to store PCB images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="612"/>
        <source>Apply MAWS quick download setup and close dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="615"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="626"/>
        <source>Cancel MAWS quick download setup and close dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="629"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="408"/>
        <source>Automatically download preview images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="48"/>
        <location filename="../../mawsqdlsetup.ui" line="107"/>
        <location filename="../../mawsqdlsetup.ui" line="166"/>
        <location filename="../../mawsqdlsetup.ui" line="225"/>
        <location filename="../../mawsqdlsetup.ui" line="284"/>
        <location filename="../../mawsqdlsetup.ui" line="330"/>
        <location filename="../../mawsqdlsetup.ui" line="411"/>
        <location filename="../../mawsqdlsetup.ui" line="514"/>
        <source>Auto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="45"/>
        <source>Automatically download flyer images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="104"/>
        <source>Automatically download cabinet images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="163"/>
        <source>Automatically download controller images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="222"/>
        <source>Automatically download marquee images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="511"/>
        <source>Automatically download title images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="281"/>
        <source>Automatically download PCB images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="373"/>
        <source>Icons</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="327"/>
        <source>Automatically download icon images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="450"/>
        <location filename="../../mawsqdlsetup.ui" line="553"/>
        <source>Preferred collection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="463"/>
        <source>Select the preferred image collection for in-game previews (auto-download)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="467"/>
        <location filename="../../mawsqdlsetup.ui" line="570"/>
        <location filename="../../mawsqdlsetup.cpp" line="97"/>
        <location filename="../../mawsqdlsetup.cpp" line="102"/>
        <source>AntoPISA progettoSNAPS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="472"/>
        <source>MAME World Snap Collection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="477"/>
        <location filename="../../mawsqdlsetup.ui" line="575"/>
        <source>CrashTest Snap Collection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="482"/>
        <source>Enaitz Jar Snaps</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="566"/>
        <source>Select the preferred image collection for titles (auto-download)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="343"/>
        <source>Path to store icon images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="356"/>
        <source>Browse path to store icon images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="113"/>
        <source>Choose icon directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="126"/>
        <source>Choose flyer directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="139"/>
        <source>Choose cabinet directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="152"/>
        <source>Choose controller directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="165"/>
        <source>Choose marquee directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="178"/>
        <source>Choose PCB directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="191"/>
        <source>Choose preview directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="204"/>
        <source>Choose title directory</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MiniWebBrowser</name>
    <message>
        <location filename="../../miniwebbrowser.ui" line="15"/>
        <source>Mini Web Browser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="33"/>
        <location filename="../../miniwebbrowser.ui" line="36"/>
        <location filename="../../miniwebbrowser.cpp" line="96"/>
        <source>Go back</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="56"/>
        <location filename="../../miniwebbrowser.ui" line="59"/>
        <location filename="../../miniwebbrowser.cpp" line="98"/>
        <source>Go forward</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="79"/>
        <location filename="../../miniwebbrowser.ui" line="82"/>
        <source>Reload current URL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="102"/>
        <location filename="../../miniwebbrowser.ui" line="105"/>
        <source>Stop loading of current URL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="145"/>
        <location filename="../../miniwebbrowser.ui" line="148"/>
        <source>Enter current URL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="170"/>
        <location filename="../../miniwebbrowser.ui" line="173"/>
        <source>Load URL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="205"/>
        <location filename="../../miniwebbrowser.ui" line="208"/>
        <source>Current progress loading URL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="125"/>
        <location filename="../../miniwebbrowser.ui" line="128"/>
        <source>Go home (first page)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="86"/>
        <source>Open link</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="88"/>
        <source>Save link as...</source>
        <translation>Save link as...</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="90"/>
        <source>Copy link</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="92"/>
        <source>Save image as...</source>
        <translation>Save image as...</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="94"/>
        <source>Copy image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="100"/>
        <source>Reload</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="102"/>
        <source>Stop</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="104"/>
        <source>Copy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="107"/>
        <source>Inspect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="477"/>
        <source>WARNING: invalid network reply and/or network error</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Options</name>
    <message>
        <location filename="../../options.cpp" line="266"/>
        <source>Check all ROM states</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="267"/>
        <source>Check all sample sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="268"/>
        <source>Check preview images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="269"/>
        <source>Check flyer images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="270"/>
        <source>Check icon images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="271"/>
        <source>About QMC2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="275"/>
        <source>Copy game to favorites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="277"/>
        <source>Online documentation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="278"/>
        <source>Clear image cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="281"/>
        <source>Clear icon cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="282"/>
        <source>Open options dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="287"/>
        <source>About Qt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="288"/>
        <source>Reload gamelist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="289"/>
        <source>Check game&apos;s ROM state</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="290"/>
        <source>Check states of tagged ROMs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="291"/>
        <source>Recreate template map</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="292"/>
        <source>Check template map</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="293"/>
        <source>Stop processing / exit QMC2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="295"/>
        <source>Clear YouTube cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="312"/>
        <source>Tag current set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="313"/>
        <source>Untag current set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="314"/>
        <source>Toggle tag mark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="315"/>
        <source>Tag all sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="316"/>
        <source>Untag all sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="317"/>
        <source>Invert all tags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="318"/>
        <source>Gamelist with full detail</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="321"/>
        <source>View games by category</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="322"/>
        <source>View games by version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="324"/>
        <source>Run external ROM tool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="325"/>
        <source>Run ROM tool for tagged sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="343"/>
        <source>Plus (+)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="344"/>
        <source>Minus (-)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="345"/>
        <source>Cursor down</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="346"/>
        <source>End</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="347"/>
        <source>Escape</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="348"/>
        <source>Cursor left</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="349"/>
        <source>Home</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="350"/>
        <source>Page down</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="351"/>
        <source>Page up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="352"/>
        <source>Enter key</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="353"/>
        <source>Cursor right</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="354"/>
        <source>Tabulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="355"/>
        <source>Cursor up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="358"/>
        <source>WARNING: configuration is not writeable, please check access permissions for </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="363"/>
        <location filename="../../options.cpp" line="370"/>
        <source>Reset to default font</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="388"/>
        <location filename="../../options.cpp" line="389"/>
        <source>Search in the folder we were called from</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="825"/>
        <source>image cache size set to %1 MB</source>
        <translation>image cache size set to %1 MB</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1206"/>
        <source>Confirm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1212"/>
        <source>&amp;Local</source>
        <translation>&amp;Local</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1212"/>
        <source>&amp;Overwrite</source>
        <translation>&amp;Overwrite</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1212"/>
        <source>Do&amp;n&apos;t apply</source>
        <translation>Do&amp;n&apos;t apply</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1317"/>
        <source>invalidating machine info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1351"/>
        <source>please reload game list for some changes to take effect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1353"/>
        <source>please reload machine list for some changes to take effect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1369"/>
        <source>re-sort of machine list impossible at this time, please wait for ROM verification to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1424"/>
        <location filename="../../options.cpp" line="1426"/>
        <source>ascending</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1424"/>
        <location filename="../../options.cpp" line="1426"/>
        <source>descending</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1438"/>
        <source>Waiting for data...</source>
        <translation>Waiting for data...</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1609"/>
        <location filename="../../options.cpp" line="1613"/>
        <source>FATAL: can&apos;t open preview file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open preview file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1627"/>
        <location filename="../../options.cpp" line="1631"/>
        <source>FATAL: can&apos;t open flyer file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open flyer file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1717"/>
        <location filename="../../options.cpp" line="1721"/>
        <source>FATAL: can&apos;t open PCB file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open PCB file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1735"/>
        <location filename="../../options.cpp" line="1739"/>
        <source>FATAL: can&apos;t open software snap-shot file, please check access permissions for %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1752"/>
        <location filename="../../options.cpp" line="1756"/>
        <source>FATAL: can&apos;t open icon file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open icon file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1940"/>
        <location filename="../../options.cpp" line="1997"/>
        <location filename="../../options.cpp" line="2935"/>
        <source>SW snap file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2432"/>
        <source>Choose MAME variant&apos;s exe file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2444"/>
        <source>Choose MESS variant&apos;s exe file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2459"/>
        <source>MAME variant arguments</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2460"/>
        <source>Specify command line arguments passed to the MAME variant
(empty means: &apos;pass the arguments we were called with&apos;):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2478"/>
        <source>MESS variant arguments</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2479"/>
        <source>Specify command line arguments passed to the MESS variant
(empty means: &apos;pass the arguments we were called with&apos;):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2543"/>
        <source>Choose ROM tool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2555"/>
        <location filename="../../options.cpp" line="2617"/>
        <location filename="../../options.cpp" line="3278"/>
        <source>Choose working directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2646"/>
        <source>Choose software list cache file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2658"/>
        <source>Choose general software folder</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2723"/>
        <source>Choose catver.ini file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2837"/>
        <location filename="../../options.cpp" line="386"/>
        <location filename="../../options.cpp" line="387"/>
        <location filename="../../options.cpp" line="3392"/>
        <source>Default</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1900"/>
        <location filename="../../options.cpp" line="1957"/>
        <location filename="../../options.cpp" line="2847"/>
        <source>Preview file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="252"/>
        <source>Registered emulators -- you may select one of these in the machine-specific emulator configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="201"/>
        <location filename="../../options.cpp" line="211"/>
        <source>Specify arguments...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="204"/>
        <location filename="../../options.cpp" line="214"/>
        <source>Reset to default (same path assumed)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="220"/>
        <source>Browse emulator information database (messinfo.dat)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="221"/>
        <source>Load emulator information database (messinfo.dat)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="222"/>
        <source>Emulator information database - messinfo.dat (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="253"/>
        <source>Save machine selection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="254"/>
        <source>Save machine selection on exit and before reloading the machine list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="255"/>
        <source>Restore machine selection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="256"/>
        <source>Restore saved machine selection at start and after reloading the machine list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="258"/>
        <location filename="../../options.cpp" line="964"/>
        <source>Category</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="259"/>
        <location filename="../../options.cpp" line="965"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="273"/>
        <source>Analyze tagged sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="276"/>
        <source>Copy tagged sets to favorites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="283"/>
        <source>Play (independent)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="285"/>
        <source>Play (embedded)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="378"/>
        <location filename="../../options.cpp" line="385"/>
        <source>No style sheet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="968"/>
        <source>View games by category (not filtered)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="970"/>
        <source>View games by emulator version (not filtered)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1367"/>
        <source>re-sort of game list impossible at this time, please wait for ROM verification to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1764"/>
        <source>triggering automatic reload of game list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1766"/>
        <source>triggering automatic reload of machine list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1088"/>
        <location filename="../../options.cpp" line="1900"/>
        <location filename="../../options.cpp" line="1957"/>
        <location filename="../../options.cpp" line="2847"/>
        <source>Preview directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1905"/>
        <location filename="../../options.cpp" line="1962"/>
        <location filename="../../options.cpp" line="2858"/>
        <source>Flyer file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1191"/>
        <location filename="../../options.cpp" line="1905"/>
        <location filename="../../options.cpp" line="1962"/>
        <location filename="../../options.cpp" line="2858"/>
        <source>Flyer directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1910"/>
        <location filename="../../options.cpp" line="1967"/>
        <location filename="../../options.cpp" line="2869"/>
        <source>Icon file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1294"/>
        <location filename="../../options.cpp" line="1910"/>
        <location filename="../../options.cpp" line="1967"/>
        <location filename="../../options.cpp" line="2869"/>
        <source>Icon directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1935"/>
        <location filename="../../options.cpp" line="1992"/>
        <location filename="../../options.cpp" line="2924"/>
        <source>PCB file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1809"/>
        <location filename="../../options.cpp" line="1935"/>
        <location filename="../../options.cpp" line="1992"/>
        <location filename="../../options.cpp" line="2924"/>
        <source>PCB directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2271"/>
        <source>Choose Qt style sheet file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2271"/>
        <source>Qt Style Sheets (*.qss)</source>
        <translation>Qt Style Sheets (*.qss)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2283"/>
        <source>Choose temporary work file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2283"/>
        <location filename="../../options.cpp" line="2407"/>
        <location filename="../../options.cpp" line="2419"/>
        <location filename="../../options.cpp" line="2432"/>
        <location filename="../../options.cpp" line="2444"/>
        <location filename="../../options.cpp" line="2495"/>
        <location filename="../../options.cpp" line="2507"/>
        <location filename="../../options.cpp" line="2519"/>
        <location filename="../../options.cpp" line="2531"/>
        <location filename="../../options.cpp" line="2543"/>
        <location filename="../../options.cpp" line="2567"/>
        <location filename="../../options.cpp" line="2579"/>
        <location filename="../../options.cpp" line="2591"/>
        <location filename="../../options.cpp" line="2605"/>
        <location filename="../../options.cpp" line="2646"/>
        <location filename="../../options.cpp" line="2672"/>
        <location filename="../../options.cpp" line="2698"/>
        <location filename="../../options.cpp" line="2710"/>
        <location filename="../../options.cpp" line="2723"/>
        <location filename="../../options.cpp" line="2944"/>
        <location filename="../../options.cpp" line="2956"/>
        <location filename="../../options.cpp" line="2968"/>
        <location filename="../../options.cpp" line="2980"/>
        <location filename="../../options.cpp" line="2992"/>
        <location filename="../../options.cpp" line="3004"/>
        <location filename="../../options.cpp" line="3016"/>
        <location filename="../../options.cpp" line="3028"/>
        <location filename="../../options.cpp" line="3054"/>
        <location filename="../../options.cpp" line="3266"/>
        <source>All files (*)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2295"/>
        <source>Choose preview directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2309"/>
        <source>Choose flyer directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2323"/>
        <source>Choose icon directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2393"/>
        <source>Choose PCB directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2407"/>
        <source>Choose options template file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2419"/>
        <location filename="../../options.cpp" line="3266"/>
        <source>Choose emulator executable file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2495"/>
        <source>Choose emulator log file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2507"/>
        <source>Choose XML gamelist cache file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2519"/>
        <source>Choose zip tool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2531"/>
        <source>Choose file removal tool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2567"/>
        <source>Choose game favorites file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2579"/>
        <source>Choose play history file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2591"/>
        <source>Choose gamelist cache file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2605"/>
        <source>Choose ROM state cache file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2684"/>
        <source>Choose data directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2944"/>
        <source>Choose ZIP-compressed preview file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2956"/>
        <source>Choose ZIP-compressed flyer file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2968"/>
        <source>Choose ZIP-compressed icon file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3016"/>
        <source>Choose ZIP-compressed title file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3028"/>
        <source>Choose ZIP-compressed PCB file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3040"/>
        <source>Choose software snap directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3054"/>
        <source>Choose ZIP-compressed software snap file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3236"/>
        <source>shortcut map is clean</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3238"/>
        <source>WARNING: shortcut map contains duplicates</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="15"/>
        <source>Options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="41"/>
        <source>&amp;GUI</source>
        <translation>&amp;GUI</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="106"/>
        <source>Application language</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="110"/>
        <source>DE (German)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="130"/>
        <source>US (English)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="91"/>
        <source>Language</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="619"/>
        <source>Image cache size in MB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="622"/>
        <source> MB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="606"/>
        <source>Image cache size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="502"/>
        <source>Application font</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="241"/>
        <source>Show short description of current processing in progress bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="566"/>
        <source>Smooth image scaling (nicer, but slower)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="569"/>
        <source>Smooth scaling</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="576"/>
        <source>Retry loading images which weren&apos;t found before?</source>
        <translation>Retry loading images which weren&apos;t found before?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="579"/>
        <source>Retry loading images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="151"/>
        <source>Save game selection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="178"/>
        <location filename="../../options.ui" line="188"/>
        <location filename="../../options.ui" line="198"/>
        <location filename="../../options.ui" line="208"/>
        <location filename="../../options.ui" line="356"/>
        <location filename="../../options.ui" line="366"/>
        <location filename="../../options.ui" line="422"/>
        <source>Scale image to fit frame size (otherwise use original size)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="181"/>
        <source>Scaled preview</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="251"/>
        <source>Kill emulators on exit?</source>
        <translation>Kill emulators on exit?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="254"/>
        <source>Kill emulators</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="138"/>
        <source>Save window layout at exit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="141"/>
        <source>Save layout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="658"/>
        <source>Use standard or custom color palette?</source>
        <translation>Use standard or custom color palette?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="661"/>
        <source>Standard color palette</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="369"/>
        <source>Scaled flyer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="158"/>
        <source>Restore saved window layout at start</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="161"/>
        <source>Restore layout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="171"/>
        <source>Restore game selection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="546"/>
        <source>GUI style</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="382"/>
        <source>Application font (= system default if empty)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="398"/>
        <source>Browse application font</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="767"/>
        <location filename="../../options.ui" line="3757"/>
        <source>F&amp;iles / Directories</source>
        <translation>F&amp;iles / Directories</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="970"/>
        <source>Browse frontend data directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="957"/>
        <source>Frontend data directory (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="944"/>
        <source>Data directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1291"/>
        <source>Switch between specifying an icon directory or a ZIP-compressed icon file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1188"/>
        <source>Switch between specifying a flyer directory or a ZIP-compressed flyer file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1085"/>
        <source>Switch between specifying a preview directory or a ZIP-compressed preview file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3983"/>
        <source>Browse gamelist cache file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3970"/>
        <source>Gamelist cache file (write)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4020"/>
        <source>Browse ROM state cache file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4007"/>
        <source>ROM state cache file (write)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3994"/>
        <source>ROM state cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="920"/>
        <source>Play history file (write)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="796"/>
        <source>Temporary file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="870"/>
        <source>Favorites file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="907"/>
        <source>Play history file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="933"/>
        <source>Browse play history file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="883"/>
        <source>Game favorites file (write)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="896"/>
        <source>Browse game favorites file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="809"/>
        <source>Temporary work file (write)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="822"/>
        <source>Browse temporary work file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1329"/>
        <source>Icon directory (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1342"/>
        <source>Browse icon directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1366"/>
        <source>ZIP-compressed icon file (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1379"/>
        <source>Browse ZIP-compressed icon file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1226"/>
        <source>Flyer directory (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1239"/>
        <source>Browse flyer directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1263"/>
        <source>ZIP-compressed flyer file (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1276"/>
        <source>Browse ZIP-compressed flyer file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1123"/>
        <source>Preview directory (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1136"/>
        <source>Browse preview directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1160"/>
        <source>ZIP-compressed preview file (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1173"/>
        <source>Browse ZIP-compressed preview file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2617"/>
        <source>Auto-trigger ROM check</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2445"/>
        <source>Update delay</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2429"/>
        <source>immediate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2464"/>
        <source> ms</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2413"/>
        <source>Responsiveness</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2388"/>
        <source>Select sort order</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2392"/>
        <source>Ascending</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2401"/>
        <source>Descending</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2323"/>
        <source>Select sort criteria</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2327"/>
        <source>Game description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2332"/>
        <source>ROM state</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2342"/>
        <source>Year</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2347"/>
        <source>Manufacturer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2375"/>
        <source>Sort order</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2310"/>
        <source>Sort criteria</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2184"/>
        <source>ROM state filter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2199"/>
        <source>Show ROM state C (correct)?</source>
        <translation>Show ROM state C (correct)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2222"/>
        <source>Show ROM state M (mostly correct)?</source>
        <translation>Show ROM state M (mostly correct)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2245"/>
        <source>Show ROM state I (incorrect)?</source>
        <translation>Show ROM state I (incorrect)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2268"/>
        <source>Show ROM state N (not found)?</source>
        <translation>Show ROM state N (not found)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2291"/>
        <source>Show ROM state U (unknown)?</source>
        <translation>Show ROM state U (unknown)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2744"/>
        <source>&amp;Shortcuts / Keys</source>
        <translation>&amp;Shortcuts / Keys</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2793"/>
        <source>Reset key sequence to default</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2796"/>
        <source>Reset</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2776"/>
        <location filename="../../options.ui" line="2779"/>
        <source>Redefine key sequence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2813"/>
        <source>Active shortcut definitions; double-click to redefine key sequence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2842"/>
        <source>Custom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3343"/>
        <source>Zip tool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3375"/>
        <source>Browse for zip tool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3399"/>
        <source>Zip tool argument list to remove entries from the ZIP archive (i. e. &quot;$ARCHIVE$ -d $FILELIST$&quot;)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3409"/>
        <source>File removal tool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3465"/>
        <source>File removal tool argument list (i. e. &quot;-f -v $FILELIST$&quot;)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3386"/>
        <location filename="../../options.ui" line="3452"/>
        <location filename="../../options.ui" line="3531"/>
        <location filename="../../options.ui" line="4388"/>
        <location filename="../../options.ui" line="4497"/>
        <source>Arguments</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3441"/>
        <source>Browse for file removal tool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3721"/>
        <source>&lt;font size=&quot;-1&quot;&gt;&lt;b&gt;WARNING: stored passwords are &lt;u&gt;weakly&lt;/u&gt; encrypted!&lt;/b&gt;&lt;/font&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3742"/>
        <source>E&amp;mulator</source>
        <translation>E&amp;mulator</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3752"/>
        <source>&amp;Global configuration</source>
        <translation>&amp;Global configuration</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3946"/>
        <source>Browse XML gamelist cache file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3933"/>
        <source>Cache file for the output of mame -listxml / -lx (write)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3872"/>
        <source>Browse emulator log file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3859"/>
        <source>Emulator log file (write)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3846"/>
        <source>Emulator log file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3835"/>
        <location filename="../../options.ui" line="4483"/>
        <source>Browse emulator executable file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3807"/>
        <source>Executable file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3909"/>
        <source>Browse options template file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3896"/>
        <source>Options template file (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="125"/>
        <source>PT (Portuguese)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="244"/>
        <source>Show progress texts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="425"/>
        <source>Scaled PCB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="509"/>
        <source>Style sheet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="522"/>
        <source>Qt style sheet file (*.qss, leave empty for no style sheet)</source>
        <translation>Qt style sheet file (*.qss, leave empty for no style sheet)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="535"/>
        <source>Browse Qt style sheet file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="638"/>
        <source>Open the detail setup dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="641"/>
        <source>Detail setup...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="651"/>
        <source>Show memory  usage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="668"/>
        <source>Exit this QMC2 variant when launching another?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="671"/>
        <source>Exit on variant launch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="678"/>
        <source>Log font</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="691"/>
        <source>Font used in logs (= application font if empty)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="707"/>
        <source>Browse font used in logs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1806"/>
        <source>Switch between specifying a PCB directory or a ZIP-compressed PCB file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1844"/>
        <source>PCB directory (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1857"/>
        <source>Browse PCB directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1881"/>
        <source>ZIP-compressed PCB file (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1894"/>
        <source>Browse ZIP-compressed PCB file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1041"/>
        <source>Enable the use of catver.ini -- get the newest version from http://www.progettoemma.net/?catlist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1044"/>
        <source>Use catver.ini</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1061"/>
        <source>Path to catver.ini (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1074"/>
        <source>Browse path to catver.ini</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2357"/>
        <source>ROM types</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2646"/>
        <source>Hide primary game list while loading (recommended, much faster)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2649"/>
        <source>Hide while loading</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2672"/>
        <source>SW snap position</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2689"/>
        <source>Above / Left</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2694"/>
        <source>Above / Center</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2699"/>
        <source>Above / Right</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2704"/>
        <source>Below / Left</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2709"/>
        <source>Below / Center</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2714"/>
        <source>Below / Right</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4133"/>
        <source>Software list cache file (write)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4146"/>
        <source>Browse software list cache file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4170"/>
        <source>Directory used as the default software folder for the MESS device configurator (if a sub-folder named as the current machine exists, that folder will be selected instead)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2630"/>
        <source>Launch emulation directly when an item is activated in the search-, favorites- or played-lists (instead of jumping to the master list)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2123"/>
        <source>Switch between specifying a software snap directory or a ZIP-compressed software snap file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2126"/>
        <location filename="../../options.ui" line="2139"/>
        <location filename="../../options.cpp" line="1940"/>
        <location filename="../../options.cpp" line="1997"/>
        <location filename="../../options.cpp" line="2935"/>
        <source>SW snap folder</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1931"/>
        <source>Software snap-shot directory (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1944"/>
        <source>Browse software snap-shot directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1968"/>
        <source>ZIP-compressed software snap-shot file (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1981"/>
        <source>Browse ZIP-compressed software snap-shot file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2362"/>
        <source>Players</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2633"/>
        <source>Play on sub-list activation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2578"/>
        <source>Select the cursor position QMC2 uses when auto-scrolling to the current item (this setting applies to all views and lists!)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2585"/>
        <source>Visible</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2590"/>
        <source>Top</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2595"/>
        <source>Bottom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2600"/>
        <source>Center</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2565"/>
        <source>Cursor position</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="148"/>
        <source>Save game selection on exit and before reloading the game list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="168"/>
        <source>Restore saved game selection at start and after reloading the game list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="748"/>
        <source>Use a unifed tool- and title-bar on Mac OS X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="751"/>
        <source>Unify with title</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2337"/>
        <source>Tag</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2367"/>
        <source>Driver status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2515"/>
        <source>Display ROM status icons in master lists?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2518"/>
        <source>Show ROM status icons</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2685"/>
        <source>Select the position where sofware snap-shots are displayed within software lists</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2719"/>
        <source>Disable snaps</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2727"/>
        <source>Display software snap-shots when hovering the software list with the mouse cursor</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2730"/>
        <source>SW snaps on mouse hover</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3314"/>
        <source>Proxy / &amp;Tools</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3488"/>
        <source>ROM tool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3507"/>
        <source>External ROM tool (it&apos;s completely up to you...)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3520"/>
        <source>Browse ROM tool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3544"/>
        <source>ROM tool argument list (i. e. &quot;$ID$ $DESCRIPTION$&quot;)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3577"/>
        <source>Browse working directory of the ROM tool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3594"/>
        <source>Copy the tool&apos;s output to the front end log (keeping it for debugging)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3597"/>
        <source>Copy tool output to front end log</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3604"/>
        <source>Automatically close the tool-executor dialog when the external process finished</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3607"/>
        <source>Close dialog automatically</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3883"/>
        <source>Options template file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3920"/>
        <source>XML game list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3957"/>
        <source>Game list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4120"/>
        <source>Software list cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4194"/>
        <source>MAME variant exe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4207"/>
        <source>Specify the exe file for the MAME variant (leave empty when all variants are installed in the same directory)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4220"/>
        <source>Browse MAME variant exe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4231"/>
        <source>MESS variant exe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4244"/>
        <source>Specify the exe file for the MESS variant (leave empty when all variants are installed in the same directory)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4257"/>
        <source>Browse MESS variant exe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3551"/>
        <location filename="../../options.ui" line="4268"/>
        <location filename="../../options.ui" line="4383"/>
        <location filename="../../options.ui" line="4528"/>
        <source>Working directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4281"/>
        <location filename="../../options.ui" line="4541"/>
        <source>Working directory that&apos;s used when the emulator is executed (if empty, QMC2&apos;s current working directory is used)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4294"/>
        <location filename="../../options.ui" line="4554"/>
        <source>Browse working directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4157"/>
        <source>General software folder</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3564"/>
        <source>Working directory that&apos;s used when the ROM tool is executed (if empty, QMC2&apos;s current working directory is used)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4183"/>
        <source>Browse general software folder</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4312"/>
        <source>Additional &amp;Emulators</source>
        <translation>Additional &amp;Emulators</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4339"/>
        <source>Registered emulators -- you may select one of these in the game-specific emulator configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4373"/>
        <location filename="../../options.ui" line="4443"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4378"/>
        <location filename="../../options.ui" line="4457"/>
        <source>Executable</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4399"/>
        <source>Register emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4429"/>
        <source>Deregister emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4450"/>
        <source>Registered emulator&apos;s name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4470"/>
        <source>Command to execute the emulator (path to the executable file)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4504"/>
        <source>Arguments passed to the emulator -- use $ID$ as a placeholder for the unique game/machine ID (its short name)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4514"/>
        <source>Replace emulator registration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4582"/>
        <source>Apply settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4585"/>
        <source>&amp;Apply</source>
        <translation>&amp;Apply</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4596"/>
        <source>Restore currently applied settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4599"/>
        <source>&amp;Restore</source>
        <translation>&amp;Restore</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4610"/>
        <source>Reset to default settings (click &lt;i&gt;Restore&lt;/i&gt; to restore currently applied settings!)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4613"/>
        <source>&amp;Default</source>
        <translation>&amp;Default</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4637"/>
        <source>Close and apply settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4640"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4647"/>
        <source>Close and discard changes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4650"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="120"/>
        <source>PL (Polish)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3362"/>
        <source>External zip tool, i.e. &quot;zip&quot; (read and execute)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3428"/>
        <source>External file removal tool, i.e. &quot;rm&quot; (read and execute)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3822"/>
        <source>Emulator executable file (read and execute)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="272"/>
        <source>Analyze current game</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="297"/>
        <source>Open ROMAlyzer dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="115"/>
        <source>FR (French)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2352"/>
        <source>Game name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2857"/>
        <source>&amp;Joystick</source>
        <translation>&amp;Joystick</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2886"/>
        <source>Enable GUI control via joystick</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2889"/>
        <source>Enable joystick control</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2905"/>
        <source>Rescan available joysticks</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2908"/>
        <source>Rescan joysticks</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2928"/>
        <source>Select joystick</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2938"/>
        <source>List of available joysticks - select the one you want to use for GUI control</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2942"/>
        <location filename="../../options.cpp" line="3419"/>
        <location filename="../../options.cpp" line="3442"/>
        <location filename="../../options.cpp" line="3495"/>
        <location filename="../../options.cpp" line="3582"/>
        <source>No joysticks found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2953"/>
        <source>Joystick information and settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2970"/>
        <source>Number of joystick axes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2973"/>
        <location filename="../../options.ui" line="2994"/>
        <location filename="../../options.ui" line="3015"/>
        <location filename="../../options.ui" line="3036"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2984"/>
        <source>Buttons:</source>
        <translation>Buttons:</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2991"/>
        <source>Number of joystick buttons</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3005"/>
        <source>Hats:</source>
        <translation>Hats:</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3012"/>
        <source>Number of coolie hats</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3026"/>
        <source>Trackballs:</source>
        <translation>Trackballs:</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3033"/>
        <source>Number of trackballs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3152"/>
        <source>Calibrate joystick axes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3174"/>
        <source>Test all joystick functions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2963"/>
        <source>Axes:</source>
        <translation>Axes:</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3062"/>
        <source>Automatically repeat joystick functions after specified delay</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3065"/>
        <source>Auto repeat after</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3081"/>
        <source>Repeat all joystick functions after how many milliseconds?</source>
        <translation>Repeat all joystick functions after how many milliseconds?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3084"/>
        <location filename="../../options.ui" line="3129"/>
        <source>ms</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3110"/>
        <source>Event timeout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3126"/>
        <source>Process joystick events after how many milliseconds?</source>
        <translation>Process joystick events after how many milliseconds?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2832"/>
        <location filename="../../options.ui" line="3288"/>
        <source>Function / Key</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3155"/>
        <source>Calibrate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3177"/>
        <source>Test</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3199"/>
        <source>Map</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3235"/>
        <source>Remap</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3252"/>
        <source>Remove</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3269"/>
        <source>Active joystick mappings; double-click to remap joystick function</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3293"/>
        <source>Joystick function</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3196"/>
        <source>Map joystick functions to GUI functions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3232"/>
        <source>Remap a joystick function to the selected GUI function</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3249"/>
        <source>Remove joystick mapping from selected GUI function</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1141"/>
        <source>WARNING: can&apos;t initialize joystick</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3755"/>
        <source>joystick map is clean</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3757"/>
        <source>WARNING: joystick map contains duplicates</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2698"/>
        <source>Choose game info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2050"/>
        <source>Game info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="987"/>
        <source>Game information database - MAME history.dat (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1000"/>
        <source>Browse game information database (MAME history.dat)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2067"/>
        <source>Use in-memory compression for game info DB (a bit slower, but consumes distinctly less memory; compression rate is usually about 1:16)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1357"/>
        <source>please restart QMC2 for some changes to take effect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="328"/>
        <location filename="../../options.ui" line="344"/>
        <source>Option requires a restart of QMC2 to take effect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2017"/>
        <location filename="../../options.ui" line="2537"/>
        <location filename="../../options.ui" line="4052"/>
        <source>Option requires a reload of the gamelist to take effect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1315"/>
        <source>invalidating game info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2047"/>
        <source>Load game information database (MAME history.dat)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="347"/>
        <source>restart required</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2036"/>
        <location filename="../../options.ui" line="2556"/>
        <location filename="../../options.ui" line="4071"/>
        <source>reload required</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2458"/>
        <source>Delay update of any game details (preview, flyer, info, configuration, ...) by how many milliseconds?</source>
        <translation>Delay update of any game details (preview, flyer, info, configuration, ...) by how many milliseconds?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2461"/>
        <source>none</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1334"/>
        <source>invalidating emulator info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2710"/>
        <source>Choose emulator info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2085"/>
        <source>Load emulator information database (mameinfo.dat)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2105"/>
        <source>Use in-memory compression for emulator info DB (a bit slower, but consumes distinctly less memory; compression rate is usually about 1:16)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2088"/>
        <source>Emu info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1017"/>
        <source>Emulator information database - mameinfo.dat (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1030"/>
        <source>Browse emulator information database (mameinfo.dat)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="448"/>
        <location filename="../../options.ui" line="483"/>
        <source>unlimited</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="451"/>
        <location filename="../../options.ui" line="486"/>
        <source> lines</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="432"/>
        <source>Emulator log size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="445"/>
        <source>Maximum number of lines to keep in emulator log browser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="331"/>
        <source>Previous track (audio player)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="332"/>
        <source>Next track (audio player)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="333"/>
        <source>Fast backward (audio player)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="334"/>
        <source>Fast forward (audio player)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="335"/>
        <source>Stop track (audio player)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="336"/>
        <source>Pause track (audio player)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="337"/>
        <source>Play track (audio player)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="319"/>
        <source>Parent / clone hierarchy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="338"/>
        <source>Raise volume (audio player)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="339"/>
        <source>Lower volume (audio player)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="274"/>
        <source>Export ROM Status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="586"/>
        <source>Fall back to the parent&apos;s image if an indivual image is missing but there&apos;s one for the parent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="589"/>
        <source>Parent image fallback</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="271"/>
        <source>Show vertical game status indicator in game details</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="274"/>
        <source>Game status indicator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="284"/>
        <source>Show the game status indicator only when the game list is not visible due to the current layout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="287"/>
        <location filename="../../options.ui" line="313"/>
        <source>Only when required</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="297"/>
        <source>Show game name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="310"/>
        <source>Show game&apos;s description only when the game list is not visible due to the current layout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="231"/>
        <source>Show the menu bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="234"/>
        <source>Show menu bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="218"/>
        <location filename="../../options.ui" line="221"/>
        <source>Show status bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="729"/>
        <location filename="../../options.ui" line="732"/>
        <source>Show tool bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="298"/>
        <source>Toggle ROM state C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="299"/>
        <source>Toggle ROM state M</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="300"/>
        <source>Toggle ROM state I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="301"/>
        <source>Toggle ROM state N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="302"/>
        <source>Toggle ROM state U</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="261"/>
        <source>Check for other instances of this QMC2 variant on startup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="264"/>
        <source>Check single instance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="308"/>
        <source>Launch QMC2 for SDLMAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="309"/>
        <source>Launch QMC2 for SDLMESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="223"/>
        <source>Machine info DB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="226"/>
        <source>Machine information database - MESS sysinfo.dat (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="227"/>
        <source>Browse machine information database (MESS sysinfo.dat)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="596"/>
        <source>Minimize application windows when launching another variant</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="599"/>
        <source>Minimize on variant launch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="327"/>
        <source>Toggle arcade mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="328"/>
        <source>Show FPS (arcade mode)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="329"/>
        <source>Take snapshot (arcade mode)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="279"/>
        <source>Setup arcade mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="229"/>
        <source>Machine description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="230"/>
        <source>Machine name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="231"/>
        <source>Number of item insertions between machine list updates during reload (higher means faster, but makes the GUI less responsive)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="232"/>
        <source>Delay update of any machine details (preview, flyer, info, configuration, ...) by how many milliseconds?</source>
        <translation>Delay update of any machine details (preview, flyer, info, configuration, ...) by how many milliseconds?</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="233"/>
        <source>Sort machine list while reloading (slower)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2662"/>
        <source>Sort game list while reloading (slower)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2426"/>
        <source>Number of item insertions between game list updates during reload (higher means faster, but makes the GUI less responsive)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="25"/>
        <source>&amp;Front end</source>
        <translation>&amp;Front end</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="559"/>
        <source>Application style (Default = use system&apos;s default style)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2614"/>
        <source>Automatically trigger a ROM check if necessary</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="305"/>
        <source>Launch QMC2 for MAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="306"/>
        <source>Launch QMC2 for MESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3349"/>
        <location filename="../../options.ui" line="3415"/>
        <location filename="../../options.ui" line="3494"/>
        <source>Command</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2665"/>
        <source>Sort while loading</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2486"/>
        <source>Launch emulation on double-click events (may be annoying)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2489"/>
        <source>Double-click activation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1424"/>
        <source>sorting game list by %1 in %2 order</source>
        <translation>sorting game list by %1 in %2 order</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1426"/>
        <source>sorting machine list by %1 in %2 order</source>
        <translation>sorting machine list by %1 in %2 order</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1208"/>
        <source>An open game-specific emulator configuration has been detected.
Use local game-settings, overwrite with global settings or don&apos;t apply?</source>
        <translation>An open game-specific emulator configuration has been detected.￼
Use local game-settings, overwrite with global settings or don&apos;t apply?</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1210"/>
        <source>An open machine-specific emulator configuration has been detected.
Use local machine-settings, overwrite with global settings or don&apos;t apply?</source>
        <translation>An open machine-specific emulator configuration has been detected.￼
Use local machine-settings, overwrite with global settings or don&apos;t apply?</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="326"/>
        <source>Toggle full screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="224"/>
        <source>Load machine information database (MESS sysinfo.dat)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="225"/>
        <source>Use in-memory compression for machine info DB (a bit slower, but consumes distinctly less memory; compression rate is usually about 1:16)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="249"/>
        <location filename="../../options.cpp" line="250"/>
        <source>Option requires a reload of the entire machine list to take effect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="251"/>
        <source>Hide primary machine list while loading (recommended, much faster)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1645"/>
        <location filename="../../options.cpp" line="1649"/>
        <source>FATAL: can&apos;t open cabinet file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open cabinet file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1663"/>
        <location filename="../../options.cpp" line="1667"/>
        <source>FATAL: can&apos;t open controller file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open controller file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1915"/>
        <location filename="../../options.cpp" line="1972"/>
        <location filename="../../options.cpp" line="2880"/>
        <source>Cabinet file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1397"/>
        <location filename="../../options.cpp" line="1915"/>
        <location filename="../../options.cpp" line="1972"/>
        <location filename="../../options.cpp" line="2880"/>
        <source>Cabinet directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1920"/>
        <location filename="../../options.cpp" line="1977"/>
        <location filename="../../options.cpp" line="2891"/>
        <source>Controller file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1500"/>
        <location filename="../../options.cpp" line="1920"/>
        <location filename="../../options.cpp" line="1977"/>
        <location filename="../../options.cpp" line="2891"/>
        <source>Controller directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2337"/>
        <source>Choose cabinet directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2351"/>
        <source>Choose controller directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2980"/>
        <source>Choose ZIP-compressed cabinet file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2992"/>
        <source>Choose ZIP-compressed controller file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="294"/>
        <source>Show game&apos;s description at the bottom of any images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2033"/>
        <location filename="../../options.ui" line="2553"/>
        <location filename="../../options.ui" line="4068"/>
        <source>Option requires a reload of the entire game list to take effect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="191"/>
        <source>Scaled cabinet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="201"/>
        <source>Scaled controller</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="648"/>
        <source>Show indicator for current memory usage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1432"/>
        <source>Cabinet directory (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1445"/>
        <source>Browse cabinet directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1469"/>
        <source>ZIP-compressed cabinet file (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1482"/>
        <source>Browse ZIP-compressed cabinet file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1394"/>
        <source>Switch between specifying a cabinet directory or a ZIP-compressed cabinet file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1535"/>
        <source>Controller directory (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1548"/>
        <source>Browse controller directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1572"/>
        <source>ZIP-compressed controller file (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1585"/>
        <source>Browse ZIP-compressed controller file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1497"/>
        <source>Switch between specifying a controller directory or a ZIP-compressed controller file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1681"/>
        <location filename="../../options.cpp" line="1685"/>
        <source>FATAL: can&apos;t open marquee file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open marquee file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1699"/>
        <location filename="../../options.cpp" line="1703"/>
        <source>FATAL: can&apos;t open title file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open title file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1925"/>
        <location filename="../../options.cpp" line="1982"/>
        <location filename="../../options.cpp" line="2902"/>
        <source>Marquee file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1603"/>
        <location filename="../../options.cpp" line="1925"/>
        <location filename="../../options.cpp" line="1982"/>
        <location filename="../../options.cpp" line="2902"/>
        <source>Marquee directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1930"/>
        <location filename="../../options.cpp" line="1987"/>
        <location filename="../../options.cpp" line="2913"/>
        <source>Title file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1706"/>
        <location filename="../../options.cpp" line="1930"/>
        <location filename="../../options.cpp" line="1987"/>
        <location filename="../../options.cpp" line="2913"/>
        <source>Title directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2365"/>
        <source>Choose marquee directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2379"/>
        <source>Choose title directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3004"/>
        <source>Choose ZIP-compressed marquee file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="211"/>
        <source>Scaled marquee</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="359"/>
        <source>Scaled title</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1638"/>
        <source>Marquee directory (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1651"/>
        <source>Browse marquee directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1675"/>
        <source>ZIP-compressed marquee file (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1688"/>
        <source>Browse ZIP-compressed marquee file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1600"/>
        <source>Switch between specifying a marquee directory or a ZIP-compressed marquee file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1741"/>
        <source>Title directory (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1754"/>
        <source>Browse title directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1778"/>
        <source>ZIP-compressed title file (read)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1791"/>
        <source>Browse ZIP-compressed title file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1703"/>
        <source>Switch between specifying a title directory or a ZIP-compressed title file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="228"/>
        <source>Machine &amp;list</source>
        <translation>Machine &amp;list</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2155"/>
        <source>Game &amp;list</source>
        <translation>Game &amp;list</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="467"/>
        <source>Front end log size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="480"/>
        <source>Maximum number of lines to keep in front end log browser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="833"/>
        <source>Front end log file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="846"/>
        <source>Front end log file (write)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="859"/>
        <source>Browse front end log file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2672"/>
        <source>Choose front end log file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2632"/>
        <source>Choose MAWS cache directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4080"/>
        <source>MAWS cache directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4093"/>
        <source>MAWS cache directory (write)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4106"/>
        <source>Browse MAWS cache directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3620"/>
        <source>Use HTTP proxy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3632"/>
        <source>Host / IP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3645"/>
        <source>Hostname or IP address of the HTTP proxy server</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3652"/>
        <source>Port</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3665"/>
        <source>Port to access the HTTP proxy service</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3678"/>
        <source>User ID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3691"/>
        <source>User ID to access the HTTP proxy service (empty = no authentication)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3698"/>
        <source>Password</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3617"/>
        <source>Enable / disable the use of an HTTP proxy on any web lookups</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3711"/>
        <source>Password to access the HTTP proxy service (empty = no authentication)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="280"/>
        <source>Clear MAWS cache</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PCB</name>
    <message>
        <location filename="../../pcb.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="56"/>
        <location filename="../../pcb.cpp" line="57"/>
        <source>Game PCB image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="59"/>
        <location filename="../../pcb.cpp" line="60"/>
        <source>Machine PCB image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="68"/>
        <location filename="../../pcb.cpp" line="72"/>
        <source>FATAL: can&apos;t open PCB file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open PCB file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Waiting for data...</translation>
    </message>
</context>
<context>
    <name>Preview</name>
    <message>
        <location filename="../../preview.cpp" line="51"/>
        <source>Copy to clipboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="58"/>
        <location filename="../../preview.cpp" line="59"/>
        <source>Game preview image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="70"/>
        <location filename="../../preview.cpp" line="74"/>
        <source>FATAL: can&apos;t open preview file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open preview file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="102"/>
        <source>Waiting for data...</source>
        <translation>Waiting for data...</translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="61"/>
        <location filename="../../preview.cpp" line="62"/>
        <source>Machine preview image</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ProcessManager</name>
    <message>
        <location filename="../../procmgr.cpp" line="99"/>
        <location filename="../../procmgr.cpp" line="101"/>
        <source>starting emulator #%1, command = %2</source>
        <translation>starting emulator #%1, command = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="131"/>
        <source>terminating emulator #%1, PID = %2</source>
        <translation>terminating emulator #%1, PID = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="154"/>
        <source>killing emulator #%1, PID = %2</source>
        <translation>killing emulator #%1, PID = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="236"/>
        <source>emulator #%1 finished, exit code = %2, exit status = %3, remaining emulators = %4</source>
        <translation>emulator #%1 finished, exit code = %2, exit status = %3, remaining emulators = %4</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="236"/>
        <source>normal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="236"/>
        <source>crashed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="281"/>
        <source>emulator #%1 started, PID = %2, running emulators = %3</source>
        <translation>emulator #%1 started, PID = %2, running emulators = %3</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="320"/>
        <source>FATAL: failed to start emulator #%1</source>
        <translation>FATAL: failed to start emulator #%1</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="325"/>
        <source>WARNING: emulator #%1 crashed</source>
        <translation>WARNING: emulator #%1 crashed</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="329"/>
        <source>WARNING: failed to write to emulator #%1</source>
        <translation>WARNING: failed to write to emulator #%1</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="333"/>
        <source>WARNING: failed to read from emulator #%1</source>
        <translation>WARNING: failed to read from emulator #%1</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="337"/>
        <source>WARNING: unhandled error for emulator #%1, error code = %2</source>
        <translation>WARNING: unhandled error for emulator #%1, error code = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="355"/>
        <source>no error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="356"/>
        <source>failed validity checks</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="357"/>
        <source>missing files</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="358"/>
        <source>fatal error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="359"/>
        <source>device initialization error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="361"/>
        <source>game doesn&apos;t exist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="363"/>
        <source>machine doesn&apos;t exist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="365"/>
        <source>invalid configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="366"/>
        <source>identified all non-ROM files</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="367"/>
        <source>identified some files but not all</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="368"/>
        <source>identified no files</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="369"/>
        <source>unknown error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="185"/>
        <source>stdout[#%1]:</source>
        <translation>stdout[#%1]:</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="61"/>
        <source>WARNING: ProcessManager::start(): the specified working directory &apos;%1&apos; is not a directory -- ignored</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="64"/>
        <source>WARNING: ProcessManager::start(): the specified working directory &apos;%1&apos; does not exist -- ignored</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="203"/>
        <source>stderr[#%1]:</source>
        <translation>stderr[#%1]:</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="145"/>
        <source>WARNING: ProcessManager::terminate(ushort index = %1): trying to terminate a null process</source>
        <translation>WARNING: ProcessManager::terminate(ushort index = %1): trying to terminate a null process</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="168"/>
        <source>WARNING: ProcessManager::kill(ushort index = %1): trying to kill a null process</source>
        <translation>WARNING: ProcessManager::kill(ushort index = %1): trying to kill a null process</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="233"/>
        <source>WARNING: ProcessManager::finished(...): trying to remove a null item</source>
        <translation>WARNING: ProcessManager::finished(...): trying to remove a null item</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../gamelist.cpp" line="2086"/>
        <location filename="../../options.cpp" line="1409"/>
        <source>players</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2093"/>
        <location filename="../../options.cpp" line="1416"/>
        <source>category</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2096"/>
        <location filename="../../options.cpp" line="1419"/>
        <source>version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2083"/>
        <location filename="../../options.cpp" line="1406"/>
        <source>ROM types</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2059"/>
        <location filename="../../options.cpp" line="1381"/>
        <source>game description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2065"/>
        <location filename="../../options.cpp" line="1387"/>
        <source>ROM state</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2068"/>
        <location filename="../../options.cpp" line="1390"/>
        <source>tag</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2071"/>
        <location filename="../../options.cpp" line="1393"/>
        <source>year</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2074"/>
        <location filename="../../options.cpp" line="1396"/>
        <source>manufacturer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2089"/>
        <location filename="../../options.cpp" line="1412"/>
        <source>driver status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3012"/>
        <source>correct</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3047"/>
        <source>incorrect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3082"/>
        <source>mostly correct</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3117"/>
        <source>not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3152"/>
        <location filename="../../gamelist.cpp" line="3190"/>
        <location filename="../../romalyzer.cpp" line="2843"/>
        <location filename="../../romalyzer.cpp" line="2876"/>
        <location filename="../../romalyzer.cpp" line="2888"/>
        <source>unknown</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="802"/>
        <location filename="../../options.cpp" line="1845"/>
        <source>Default</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3597"/>
        <location filename="../../qmc2main.cpp" line="3598"/>
        <source>Export game-specific MAME configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3603"/>
        <location filename="../../qmc2main.cpp" line="9499"/>
        <source>Import from...</source>
        <translation>Import from...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3605"/>
        <location filename="../../qmc2main.cpp" line="3606"/>
        <source>Import game-specific MAME configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9440"/>
        <location filename="../../qmc2main.cpp" line="9442"/>
        <source>M.A.M.E. Catalog / Launcher II v</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9446"/>
        <source>SVN r%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9477"/>
        <source>processing global emulator configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9491"/>
        <source>Export to...</source>
        <translation>Export to...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9493"/>
        <location filename="../../qmc2main.cpp" line="9494"/>
        <source>Export global MAME configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9501"/>
        <location filename="../../qmc2main.cpp" line="9502"/>
        <source>Import global MAME configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9517"/>
        <location filename="../../qmc2main.cpp" line="9525"/>
        <source>&lt;inipath&gt;/mame.ini</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9521"/>
        <location filename="../../qmc2main.cpp" line="9529"/>
        <source>Select file...</source>
        <translation>Select file...</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2827"/>
        <source>ROM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="757"/>
        <location filename="../../romalyzer.cpp" line="2827"/>
        <source>CHD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2831"/>
        <location filename="../../romalyzer.cpp" line="2873"/>
        <location filename="../../romalyzer.cpp" line="2893"/>
        <source>good</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="758"/>
        <location filename="../../romalyzer.cpp" line="2835"/>
        <location filename="../../romalyzer.cpp" line="2882"/>
        <source>no dump</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2839"/>
        <location filename="../../romalyzer.cpp" line="2885"/>
        <source>bad dump</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2879"/>
        <source>no / bad dump</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2078"/>
        <location filename="../../options.cpp" line="1400"/>
        <source>game name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9464"/>
        <source>OpenGL features enabled</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9496"/>
        <location filename="../../qmc2main.cpp" line="9497"/>
        <source>Export global MESS configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9504"/>
        <location filename="../../qmc2main.cpp" line="9505"/>
        <source>Import global MESS configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2061"/>
        <location filename="../../options.cpp" line="1383"/>
        <source>machine description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2080"/>
        <location filename="../../options.cpp" line="1402"/>
        <source>machine name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9473"/>
        <source>SDL joystick support enabled - using SDL v%1.%2.%3</source>
        <translation>SDL joystick support enabled - using SDL v%1.%2.%3</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9519"/>
        <location filename="../../qmc2main.cpp" line="9527"/>
        <source>&lt;inipath&gt;/mess.ini</source>
        <translation>&lt;inipath&gt;/mess.ini</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3600"/>
        <location filename="../../qmc2main.cpp" line="3601"/>
        <source>Export machine-specific MESS configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3608"/>
        <location filename="../../qmc2main.cpp" line="3609"/>
        <source>Import machine-specific MESS configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9438"/>
        <source>M.E.S.S. Catalog / Launcher II v</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9468"/>
        <source>Phonon features enabled - using Phonon v%1</source>
        <translation>Phonon features enabled - using Phonon v%1</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1619"/>
        <source>video player: XML error: fatal error on line %1, column %2: %3</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ROMAlyzer</name>
    <message>
        <location filename="../../romalyzer.cpp" line="2511"/>
        <source>checksum wizard: FATAL: can&apos;t open ZIP archive &apos;%1&apos; for reading</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2534"/>
        <source>Repairing set &apos;%1&apos; - %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2535"/>
        <source>checksum wizard: repairing %1 file &apos;%2&apos; in &apos;%3&apos; from repro template</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2620"/>
        <source>checksum wizard: FATAL: can&apos;t open file &apos;%1&apos; in ZIP archive &apos;%2&apos; for writing</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2626"/>
        <source>Fixed by QMC2 v%1 (%2)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2632"/>
        <source>checksum wizard: FATAL: can&apos;t open ZIP archive &apos;%1&apos; for writing</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2647"/>
        <source>repaired</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2649"/>
        <source>checksum wizard: successfully repaired %1 file &apos;%2&apos; in &apos;%3&apos; from repro template</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2766"/>
        <source>Choose local DB output path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2728"/>
        <source>Connection check -- succeeded!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2738"/>
        <source>Connection check -- failed!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2729"/>
        <source>database connection check successful</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1754"/>
        <source>Choose CHD manager executable file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1754"/>
        <source>All files (*)</source>
        <translation>All files (*)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1766"/>
        <source>Choose temporary working directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1809"/>
        <source>CHD manager: external process started</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1820"/>
        <location filename="../../romalyzer.cpp" line="2005"/>
        <source>unknown</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1822"/>
        <source>normal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1823"/>
        <source>crashed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1825"/>
        <source>CHD manager: external process finished (exit code = %1, exit status = %2)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1841"/>
        <source>CHD manager: stdout: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1863"/>
        <source>CHD manager: stderr: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1890"/>
        <source>CHD manager: failed to start</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1894"/>
        <source>CHD manager: crashed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1898"/>
        <source>CHD manager: write error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1902"/>
        <source>CHD manager: read error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1906"/>
        <source>CHD manager: unknown error %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="15"/>
        <source>ROMAlyzer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="42"/>
        <location filename="../../romalyzer.cpp" line="1039"/>
        <source>&amp;Analyze</source>
        <translation>&amp;Analyze</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="82"/>
        <source>Analysis report</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="127"/>
        <location filename="../../romalyzer.ui" line="1346"/>
        <source>SHA1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="132"/>
        <source>MD5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="599"/>
        <source>Enable ROM database support (repository access may be slow)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="698"/>
        <source>Server / IP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="685"/>
        <source>Name or IP address of the database server</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="614"/>
        <source>Driver</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="627"/>
        <source>Port</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="643"/>
        <source>default</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="711"/>
        <source>Username</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="724"/>
        <source>Username used to access the database</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="798"/>
        <source>Password</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="824"/>
        <source>Automatically download missing / bad files from the database (if they are available in the repository)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="827"/>
        <source>Download</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="840"/>
        <source>Automatically upload good files to the database (if they are missing in the repository)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="662"/>
        <source>SQL driver to use</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="289"/>
        <source>Temporary directory used by the CHD manager (make sure it has enough room to store the biggest CHDs)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="302"/>
        <source>Browse temporary directory used by the CHD manager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="366"/>
        <source>General analysis flags and limits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="472"/>
        <source>&lt;b&gt;Limits:&lt;/b&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="479"/>
        <source>File size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="492"/>
        <source>Maximum size (in MB) of files to be loaded, files are skipped when they are bigger than that (0 = no limit)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="514"/>
        <source>Log size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="549"/>
        <source>Reports</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="562"/>
        <source>Maximum number of reported sets held in memory (0 = no limit)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="666"/>
        <source>MySQL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="671"/>
        <source>SQLite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="737"/>
        <source>Check the connection to the database</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="859"/>
        <source>Overwrite existing data in the database</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="862"/>
        <source>Overwrite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="875"/>
        <source>Output path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="888"/>
        <source>Local output directory where downloaded ROMs &amp; CHDs will be created (WARNING: existing files will be overwritten!)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="901"/>
        <source>Browse local output directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="915"/>
        <source>Enable set rewriter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="946"/>
        <source>Output path for the set rewriter (WARNING: existing files will be overwritten!) -- you should NEVER use one of your primary ROM paths here!!!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1108"/>
        <source>Browse output path for the set rewriter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="972"/>
        <source>Rewrite sets while analyzing them (otherwise sets will only be rewritten on demand / through the context menu)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="933"/>
        <source>Output directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="959"/>
        <source>General settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="988"/>
        <source>Create sets that do not need parent sets (otherwise create merged sets, which is recommended)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="998"/>
        <source>Rewrite sets only when they are &apos;good&apos; (otherwise, &apos;bad&apos; sets will be included)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1001"/>
        <source>Good sets only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1014"/>
        <source>Reproduction type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1029"/>
        <source>Produce ZIP archived sets (recommended)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1045"/>
        <source>Level </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1069"/>
        <source>Unique CRCs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1125"/>
        <source>Additional ROM path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1151"/>
        <source>Browse additional ROM path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1166"/>
        <source>Checksum wizard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1194"/>
        <source>Search</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1264"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1269"/>
        <source>Filename</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1274"/>
        <source>Status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1284"/>
        <source>Path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1225"/>
        <source>Repair selected &apos;bad&apos; sets using the file from the first selected &apos;good&apos; set (at least 1 good and 1 bad set must be selected)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1228"/>
        <source>Repair bad sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1208"/>
        <source>Analyze all selected sets in order to qualify them</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1138"/>
        <source>Specify an additional source ROM path used when the set rewriter is active</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1178"/>
        <source>Checksum to be searched</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1191"/>
        <source>Search for the checksum now</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1211"/>
        <source>Analyze selected sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1239"/>
        <source>Search results for the current checksum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1294"/>
        <source>Level of automation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1307"/>
        <source>Choose the level of automated wizard operations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1311"/>
        <source>Do nothing automatically</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1316"/>
        <source>Automatically select matches</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1321"/>
        <source>Automatically select matches and analyze sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1326"/>
        <source>Automatically select matches, analyze sets and repair bad ones</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1342"/>
        <source>Select the checksum type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1368"/>
        <source>Close ROMAlyzer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1371"/>
        <source>&amp;Close</source>
        <translation>&amp;Close</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="29"/>
        <source>Shortname of game to be analyzed - wildcards allowed, use space as delimiter for multiple games</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="372"/>
        <source>If set, analysis output is appended (otherwise the report is cleared before the analysis)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="785"/>
        <source>Database</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="772"/>
        <source>Name of the database on the server</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="743"/>
        <location filename="../../romalyzer.cpp" line="2755"/>
        <source>Connection check</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1388"/>
        <source>Current ROMAlyzer status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1391"/>
        <location filename="../../romalyzer.cpp" line="598"/>
        <location filename="../../romalyzer.cpp" line="1046"/>
        <location filename="../../romalyzer.cpp" line="2014"/>
        <location filename="../../romalyzer.cpp" line="2659"/>
        <source>Idle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1407"/>
        <source>Analysis progress indicator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="102"/>
        <location filename="../../romalyzer.ui" line="1279"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="73"/>
        <source>Report</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="141"/>
        <source>Log</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="147"/>
        <source>Analysis log</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="210"/>
        <source>please wait for reload to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="548"/>
        <source>&amp;Stop</source>
        <translation>&amp;Stop</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="213"/>
        <source>stopping analysis</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="217"/>
        <source>starting analysis</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="556"/>
        <source>analysis started</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="558"/>
        <source>determining list of games to analyze</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="578"/>
        <source>Searching games</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1049"/>
        <source>analysis ended</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="606"/>
        <source>done (determining list of games to analyze)</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="607"/>
        <source>%n game(s) to analyze</source>
        <translation>
            <numerusform>%n game to analyze</numerusform>
            <numerusform>%n games to analyze</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="657"/>
        <source>analyzing &apos;%1&apos;</source>
        <translation>analyzing &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="659"/>
        <source>Analyzing &apos;%1&apos;</source>
        <translation>Analyzing &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1029"/>
        <source>done (analyzing &apos;%1&apos;)</source>
        <translation>done (analyzing &apos;%1&apos;)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="1031"/>
        <source>%n game(s) left</source>
        <translation>
            <numerusform>%n game left</numerusform>
            <numerusform>%n games left</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="670"/>
        <source>parsing XML data for &apos;%1&apos;</source>
        <translation>parsing XML data for &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="677"/>
        <source>done (parsing XML data for &apos;%1&apos;)</source>
        <translation>done (parsing XML data for &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="679"/>
        <source>error (parsing XML data for &apos;%1&apos;)</source>
        <translation>error (parsing XML data for &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="92"/>
        <source>Game / File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="97"/>
        <source>Merge</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="107"/>
        <source>Emu status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="211"/>
        <source>Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="375"/>
        <source>Append to report</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="112"/>
        <source>File status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="122"/>
        <location filename="../../romalyzer.ui" line="1351"/>
        <source>CRC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="117"/>
        <source>Size</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="688"/>
        <source>checking %n file(s) for &apos;%1&apos;</source>
        <translation>
            <numerusform>checking %n file for &apos;%1&apos;</numerusform>
            <numerusform>checking %n files for &apos;%1&apos;</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="740"/>
        <location filename="../../romalyzer.cpp" line="834"/>
        <location filename="../../romalyzer.cpp" line="911"/>
        <location filename="../../romalyzer.cpp" line="1997"/>
        <location filename="../../romalyzer.cpp" line="2475"/>
        <location filename="../../romalyzer.cpp" line="2538"/>
        <source>ROM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="749"/>
        <location filename="../../romalyzer.cpp" line="801"/>
        <location filename="../../romalyzer.cpp" line="915"/>
        <location filename="../../romalyzer.cpp" line="949"/>
        <location filename="../../romalyzer.cpp" line="956"/>
        <source>not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="828"/>
        <source>Checksums</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="845"/>
        <source>SIZE </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="860"/>
        <source>CRC </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="872"/>
        <source>SHA1 </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="897"/>
        <source>MD5 </source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="941"/>
        <source>interrupted (checking %n file(s) for &apos;%1&apos;)</source>
        <translation>
            <numerusform>interrupted (checking %n file for &apos;%1&apos;)</numerusform>
            <numerusform>interrupted (checking %n files for &apos;%1&apos;)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="964"/>
        <source>good / not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="883"/>
        <location filename="../../romalyzer.cpp" line="953"/>
        <location filename="../../romalyzer.cpp" line="976"/>
        <location filename="../../romalyzer.cpp" line="2260"/>
        <location filename="../../romalyzer.cpp" line="2452"/>
        <source>good</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="126"/>
        <source>Search checksum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="135"/>
        <source>Rewrite set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="793"/>
        <location filename="../../romalyzer.cpp" line="814"/>
        <location filename="../../romalyzer.cpp" line="883"/>
        <location filename="../../romalyzer.cpp" line="996"/>
        <location filename="../../romalyzer.cpp" line="2261"/>
        <location filename="../../romalyzer.cpp" line="2450"/>
        <source>bad</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="834"/>
        <source>WARNING: %1 file &apos;%2&apos; loaded from &apos;%3&apos; may be obsolete, should be merged from parent set &apos;%4&apos;</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="1016"/>
        <source>done (checking %n file(s) for &apos;%1&apos;)</source>
        <translation>
            <numerusform>done (checking %n file for &apos;%1&apos;)</numerusform>
            <numerusform>done (checking %n files for &apos;%1&apos;)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1182"/>
        <location filename="../../romalyzer.cpp" line="1206"/>
        <source>  logical size: %1 (%2 B)</source>
        <translation>  logical size: %1 (%2 B)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1243"/>
        <source>Verify - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1245"/>
        <source>CHD manager: verifying and fixing CHD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1248"/>
        <source>CHD manager: verifying CHD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1258"/>
        <source>Update - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1259"/>
        <source>CHD manager: updating CHD (v%1 -&gt; v%2)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1264"/>
        <location filename="../../romalyzer.cpp" line="1276"/>
        <source>CHD manager: using header checksums for CHD verification</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1284"/>
        <source>CHD manager: no header checksums available for CHD verification</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1312"/>
        <source>CHD manager: terminating external process</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1333"/>
        <location filename="../../romalyzer.cpp" line="1335"/>
        <source>CHD manager: CHD file integrity is good</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1337"/>
        <source>CHD manager: WARNING: CHD file integrity is bad</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1341"/>
        <location filename="../../romalyzer.cpp" line="1353"/>
        <source>CHD manager: using CHD v%1 header checksums for CHD verification</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1361"/>
        <source>CHD manager: WARNING: no header checksums available for CHD verification</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1368"/>
        <source>CHD manager: replacing CHD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1370"/>
        <source>Copy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1376"/>
        <source>CHD manager: CHD replaced</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1379"/>
        <source>CHD manager: FATAL: failed to replace CHD -- updated CHD preserved as &apos;%1&apos;, please copy it to &apos;%2&apos; manually!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1383"/>
        <source>CHD manager: cleaning up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1395"/>
        <location filename="../../romalyzer.cpp" line="1407"/>
        <source>using CHD v%1 header checksums for CHD verification</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1415"/>
        <source>WARNING: no header checksums available for CHD verification</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1461"/>
        <location filename="../../romalyzer.cpp" line="1592"/>
        <source>WARNING: found &apos;%1&apos; but can&apos;t read from it - check permission</source>
        <translation>WARNING: found &apos;%1&apos; but can&apos;t read from it - check permission</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1499"/>
        <source>WARNING: unable to identify &apos;%1&apos; from &apos;%2&apos; by CRC &apos;%3&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1496"/>
        <source>WARNING: unable to identify &apos;%1&apos; from &apos;%2&apos; by CRC (no dump exists / CRC unknown)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="142"/>
        <source>Copy to clipboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="822"/>
        <location filename="../../romalyzer.cpp" line="917"/>
        <source>no dump</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="968"/>
        <source>good / no dump / skipped</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="970"/>
        <source>good / no dump</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="988"/>
        <source>bad / no dump / skipped</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="990"/>
        <source>bad / no dump</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1519"/>
        <source>loading &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos; as &apos;%4&apos;%5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1586"/>
        <source>WARNING: unable to decompress &apos;%1&apos; from &apos;%2&apos; - check file integrity</source>
        <translation>WARNING: unable to decompress &apos;%1&apos; from &apos;%2&apos; - check file integrity</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1590"/>
        <source>WARNING: found &apos;%1&apos; but can&apos;t open it for decompression - check file integrity</source>
        <translation>WARNING: found &apos;%1&apos; but can&apos;t open it for decompression - check file integrity</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1780"/>
        <source>Choose output directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1794"/>
        <source>Choose additional ROM path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1940"/>
        <source>Checksum search</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2079"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path is not a directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2083"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path is not writable</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2088"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path does not exist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2092"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path is empty</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2106"/>
        <source>space-efficient</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2107"/>
        <source>self-contained</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2109"/>
        <source>set rewriter: rewriting %1 set &apos;%2&apos; to &apos;%3&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2125"/>
        <source>set rewriter: skipping &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos; as &apos;%4&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2136"/>
        <source>set rewriter: FATAL: can&apos;t load &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos;, aborting</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2139"/>
        <source>set rewriter: WARNING: can&apos;t load &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos;, ignoring this file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2166"/>
        <source>set rewriter: writing new %1 set &apos;%2&apos; in &apos;%3&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2167"/>
        <source>Writing &apos;%1&apos; - %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2169"/>
        <source>set rewriter: new %1 set &apos;%2&apos; in &apos;%3&apos; successfully created</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2171"/>
        <source>set rewriter: FATAL: failed to create new %1 set &apos;%2&apos; in &apos;%3&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2181"/>
        <source>set rewriter: done (rewriting %1 set &apos;%2&apos; to &apos;%3&apos;)</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="2464"/>
        <source>checksum wizard: repairing %n bad set(s)</source>
        <translation>
            <numerusform>checksum wizard: repairing %n bad set</numerusform>
            <numerusform>checksum wizard: repairing %n bad sets</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2470"/>
        <source>checksum wizard: using %1 file &apos;%2&apos; from &apos;%3&apos; as repro template</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2493"/>
        <source>checksum wizard: successfully identified &apos;%1&apos; from &apos;%2&apos; by CRC, filename in ZIP archive is &apos;%3&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2502"/>
        <source>checksum wizard: template data loaded, uncompressed size = %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2506"/>
        <source>checksum wizard: FATAL: unable to identify &apos;%1&apos; from &apos;%2&apos; by CRC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2516"/>
        <location filename="../../romalyzer.cpp" line="2637"/>
        <source>checksum wizard: sorry, no support for regular files yet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2521"/>
        <location filename="../../romalyzer.cpp" line="2642"/>
        <source>checksum wizard: sorry, no support for CHD files yet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2547"/>
        <source>checksum wizard: target ZIP exists, loading complete data and structure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2549"/>
        <source>checksum wizard: target ZIP successfully loaded</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2551"/>
        <source>checksum wizard: an entry with the CRC &apos;%1&apos; already exists, recreating the ZIP from scratch to replace the bad file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2554"/>
        <source>checksum wizard: backup file &apos;%1&apos; successfully created</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2558"/>
        <source>checksum wizard: FATAL: failed to create backup file &apos;%1&apos;, aborting</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2562"/>
        <source>checksum wizard: no entry with the CRC &apos;%1&apos; was found, adding the missing file to the existing ZIP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2565"/>
        <source>checksum wizard: FATAL: failed to load target ZIP, aborting</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2570"/>
        <source>checksum wizard: the target ZIP does not exist, creating a new ZIP with just the missing file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2431"/>
        <location filename="../../romalyzer.cpp" line="2628"/>
        <source>Created by QMC2 v%1 (%2)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2102"/>
        <source>Reading &apos;%1&apos; - %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2129"/>
        <source>set rewriter: loading &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos; as &apos;%4&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2156"/>
        <source>set rewriter: removing redundant file &apos;%1&apos; with CRC &apos;%2&apos; from output data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2175"/>
        <source>set rewriter: INFORMATION: no output data available, thus not rewriting set &apos;%1&apos; to &apos;%2&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2405"/>
        <source>set rewriter: deflating &apos;%1&apos; (uncompressed size: %2)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2425"/>
        <source>set rewriter: WARNING: failed to deflate &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2651"/>
        <source>repair failed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2653"/>
        <source>checksum wizard: FATAL: failed to repair %1 file &apos;%2&apos; in &apos;%3&apos; from repro template</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2663"/>
        <source>checksum wizard: FATAL: can&apos;t find any good set</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="2665"/>
        <source>checksum wizard: done (repairing %n bad set(s))</source>
        <translation>
            <numerusform>checksum wizard: done (repairing %n bad set)</numerusform>
            <numerusform>checksum wizard: done (repairing %n bad sets)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2739"/>
        <source>database connection check failed -- errorNumber = %1, errorText = &apos;%2&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="395"/>
        <source>Calculate CRC-32 checksum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="398"/>
        <source>Calculate CRC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="382"/>
        <source>Automatically scroll to the currently analyzed game</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="385"/>
        <source>Auto scroll</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="444"/>
        <source>Calculate SHA1 hash</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="447"/>
        <source>Calculate SHA1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="421"/>
        <source>Automatically expand file info</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="424"/>
        <source>Expand file info</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="411"/>
        <source>Calculate MD5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="434"/>
        <source>Automatically expand checksums</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="437"/>
        <source>Expand checksums</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="408"/>
        <source>Calculate MD5 hash</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="984"/>
        <source>bad / not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1458"/>
        <source>WARNING: found &apos;%1&apos; but can&apos;t read from it although permissions seem okay - check file integrity</source>
        <translation>WARNING: found &apos;%1&apos; but can&apos;t read from it although permissions seem okay - check file integrity</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="231"/>
        <source>pausing analysis</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="234"/>
        <source>resuming analysis</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="56"/>
        <location filename="../../romalyzer.cpp" line="235"/>
        <location filename="../../romalyzer.cpp" line="551"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="619"/>
        <source>analysis paused</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="620"/>
        <source>&amp;Resume</source>
        <translation>&amp;Resume</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="623"/>
        <source>Paused</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="53"/>
        <source>Pause / resume active analysis</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1141"/>
        <source>loading &apos;%1&apos;%2</source>
        <translation>loading &apos;%1&apos;%2</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1141"/>
        <location filename="../../romalyzer.cpp" line="1519"/>
        <source> (merged)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1426"/>
        <source>File I/O progress indicator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="720"/>
        <location filename="../../romalyzer.cpp" line="772"/>
        <location filename="../../romalyzer.cpp" line="775"/>
        <location filename="../../romalyzer.cpp" line="913"/>
        <source>skipped</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="962"/>
        <source>good / not found / skipped</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="974"/>
        <source>good / skipped</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="982"/>
        <source>bad / not found / skipped</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="994"/>
        <source>bad / skipped</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="498"/>
        <location filename="../../romalyzer.cpp" line="1711"/>
        <location filename="../../romalyzer.cpp" line="1725"/>
        <source> MB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>none</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="84"/>
        <source>Automatically scroll to the currently analyzed machine</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="85"/>
        <source>Shortname of machine to be analyzed - wildcards allowed, use space as delimiter for multiple machines</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>zlib</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>zlib+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>A/V codec</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="640"/>
        <source>report limit reached, removing %n set(s) from the report</source>
        <translation>
            <numerusform>report limit reached, removing the oldest set from the report</numerusform>
            <numerusform>report limit reached, removing %n sets from the report</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="778"/>
        <source>error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="911"/>
        <source>WARNING: %1 file &apos;%2&apos; loaded from &apos;%3&apos; has incorrect / unexpected checksums</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1164"/>
        <source>CHD header information:</source>
        <translation>CHD header information:</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1168"/>
        <source>  version: %1</source>
        <translation>  version: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1169"/>
        <location filename="../../romalyzer.cpp" line="1377"/>
        <source>CHD v%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1174"/>
        <location filename="../../romalyzer.cpp" line="1198"/>
        <source>  compression: %1</source>
        <translation>  compression: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1178"/>
        <location filename="../../romalyzer.cpp" line="1202"/>
        <source>  number of total hunks: %1</source>
        <translation>  number of total hunks: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1180"/>
        <location filename="../../romalyzer.cpp" line="1204"/>
        <source>  number of bytes per hunk: %1</source>
        <translation>  number of bytes per hunk: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1184"/>
        <source>  MD5 checksum: %1</source>
        <translation>  MD5 checksum: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1186"/>
        <location filename="../../romalyzer.cpp" line="1208"/>
        <source>  SHA1 checksum: %1</source>
        <translation>  SHA1 checksum: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1219"/>
        <source>only CHD v3 and v4 headers supported -- rest of header information skipped</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1423"/>
        <source>WARNING: can&apos;t read CHD header information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1570"/>
        <source>WARNING: the CRC for &apos;%1&apos; from &apos;%2&apos; is unknown to the emulator, the set rewriter will use the recalculated CRC &apos;%3&apos; to qualify the file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1572"/>
        <source>WARNING: unable to determine the CRC for &apos;%1&apos; from &apos;%2&apos;, the set rewriter will NOT store this file in the new set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1708"/>
        <location filename="../../romalyzer.cpp" line="1722"/>
        <source> KB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1714"/>
        <location filename="../../romalyzer.cpp" line="1728"/>
        <source> GB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1717"/>
        <source> TB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="179"/>
        <source>&amp;Forward</source>
        <translation>&amp;Forward</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="196"/>
        <source>&amp;Backward</source>
        <translation>&amp;Backward</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="460"/>
        <source>Select game</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="169"/>
        <source>Search string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="176"/>
        <source>Search string forward</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="193"/>
        <source>Search string backward</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="230"/>
        <source>Enable CHD manager (may be slow)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="239"/>
        <source>CHD manager (chdman)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="252"/>
        <source>CHD manager executable file (read and execute)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="265"/>
        <source>Browse CHD manager executable file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="276"/>
        <source>Temporary working directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="315"/>
        <source>Verify CHDs through &apos;chdman -verify&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="318"/>
        <source>Verify CHDs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="328"/>
        <source>Also try to fix CHDs using &apos;chdman -verifyfix&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="331"/>
        <source>Fix CHDs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="338"/>
        <source>Try to update CHDs if their header indicates an older version (&apos;chdman -update&apos;)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="341"/>
        <source>Update CHDs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="527"/>
        <source>Maximum number of lines in log (0 = no limit)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="533"/>
        <source> lines</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="640"/>
        <source>Database server port (0 = default)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="756"/>
        <source>Password used to access the database (WARNING: stored passwords are &lt;u&gt;weakly&lt;/u&gt; encrypted!)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="811"/>
        <source>Mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="843"/>
        <source>Upload</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="975"/>
        <source>Rewrite while analyzing</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="991"/>
        <source>Self-contained</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1032"/>
        <source>ZIPs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1042"/>
        <source>Select the ZIP compression level (0 = lowest / fastest, 9 = highest / slowest)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1066"/>
        <source>When a set contains multiple files with the same CRC, should the produced ZIP include all files individually or just the first one (which is actually sufficient)?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1079"/>
        <source>Produce sets in individual sub-directories (not recommended -- and not supported yet!)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1082"/>
        <source>Directories</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="495"/>
        <location filename="../../romalyzer.ui" line="530"/>
        <location filename="../../romalyzer.ui" line="565"/>
        <source>unlimited</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1166"/>
        <source>  tag: %1</source>
        <translation>  tag: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1189"/>
        <source>  parent CHD&apos;s MD5 checksum: %1</source>
        <translation>  parent CHD&apos;s MD5 checksum: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1191"/>
        <location filename="../../romalyzer.cpp" line="1211"/>
        <source>  parent CHD&apos;s SHA1 checksum: %1</source>
        <translation>  parent CHD&apos;s SHA1 checksum: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>  flags: %1, %2</source>
        <translation>  flags: %1, %2</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>has parent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>no parent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>allows writes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>read only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1464"/>
        <source>WARNING: CHD file &apos;%1&apos; not found</source>
        <translation>WARNING: CHD file &apos;%1&apos; not found</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="834"/>
        <location filename="../../romalyzer.cpp" line="911"/>
        <location filename="../../romalyzer.cpp" line="1106"/>
        <location filename="../../romalyzer.cpp" line="1994"/>
        <source>CHD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="82"/>
        <source>Select machine</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="83"/>
        <source>Select machine in machine list if selected in analysis report?</source>
        <translation>Select machine in machine list if selected in analysis report?</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="457"/>
        <source>Select game in game list if selected in analysis report?</source>
        <translation>Select game in game list if selected in analysis report?</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="81"/>
        <source>Machine / File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="560"/>
        <source>determining list of machines to analyze</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="580"/>
        <source>Searching machines</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="609"/>
        <source>done (determining list of machines to analyze)</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="610"/>
        <source>%n machine(s) to analyze</source>
        <translation>
            <numerusform>%n machine to analyze</numerusform>
            <numerusform>%n machines to analyze</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="1033"/>
        <source>%n machine(s) left</source>
        <translation>
            <numerusform>%n machine left</numerusform>
            <numerusform>%n machines left</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1049"/>
        <source>elapsed time = %1</source>
        <translation>elapsed time = %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="39"/>
        <source>Start / stop analysis</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1214"/>
        <source>  raw SHA1 checksum: %1</source>
        <translation>  raw SHA1 checksum: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1132"/>
        <source>size of &apos;%1&apos; is greater than allowed maximum -- skipped</source>
        <translation>size of &apos;%1&apos; is greater than allowed maximum -- skipped</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1510"/>
        <source>size of &apos;%1&apos; from &apos;%2&apos; is greater than allowed maximum -- skipped</source>
        <translation>size of &apos;%1&apos; from &apos;%2&apos; is greater than allowed maximum -- skipped</translation>
    </message>
</context>
<context>
    <name>ROMStatusExporter</name>
    <message>
        <location filename="../../romstatusexport.ui" line="15"/>
        <source>ROM status export</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="23"/>
        <location filename="../../romstatusexport.ui" line="36"/>
        <source>Select output format</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="40"/>
        <source>ASCII</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="45"/>
        <source>CSV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="339"/>
        <source>Browse ASCII export file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="342"/>
        <source>ASCII file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="362"/>
        <source>ASCII export file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="369"/>
        <source>Column width</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="385"/>
        <source>Maximum column width for ASCII export (0 = unlimited)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="388"/>
        <source>unlimited</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="444"/>
        <source>Browse CSV export file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="447"/>
        <source>CSV file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="467"/>
        <source>CSV export file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="474"/>
        <source>Separator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="515"/>
        <source>Field separator for CSV export</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="518"/>
        <source>;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="531"/>
        <source>Delimiter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="559"/>
        <source>Text delimiter for CSV export</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="562"/>
        <source>&quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="81"/>
        <source>Export ROM state C (correct)?</source>
        <translation>Export ROM state C (correct)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="98"/>
        <source>Export ROM state M (mostly correct)?</source>
        <translation>Export ROM state M (mostly correct)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="115"/>
        <source>Export ROM state I (incorrect)?</source>
        <translation>Export ROM state I (incorrect)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="132"/>
        <source>Export ROM state N (not found)?</source>
        <translation>Export ROM state N (not found)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="149"/>
        <source>Export ROM state U (unknown)?</source>
        <translation>Export ROM state U (unknown)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="269"/>
        <source>Include some header information in export</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="272"/>
        <source>Include header</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="282"/>
        <source>Include statistical overview of the ROM state in export</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="285"/>
        <source>Include ROM statistics</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="728"/>
        <source>Close ROM status export</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="731"/>
        <source>&amp;Close</source>
        <translation>&amp;Close</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="714"/>
        <source>Export now!</source>
        <translation>Export now!</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="742"/>
        <source>Export progress indicator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="748"/>
        <source>%p%</source>
        <translation>%p%</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="717"/>
        <source>&amp;Export</source>
        <translation>&amp;Export</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="72"/>
        <source>Exported ROM states</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="50"/>
        <source>HTML</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="295"/>
        <source>Export to the system clipboard instead of a file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="298"/>
        <source>Export to clipboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="308"/>
        <source>Overwrite existing files without asking what to do</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="311"/>
        <source>Overwrite blindly</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="618"/>
        <source>Browse HTML export file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="621"/>
        <source>HTML file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="641"/>
        <source>HTML export file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="181"/>
        <source>Sort criteria</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="194"/>
        <source>Select sort criteria</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="198"/>
        <source>Game description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="203"/>
        <source>ROM state</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="208"/>
        <location filename="../../romstatusexport.cpp" line="225"/>
        <location filename="../../romstatusexport.cpp" line="334"/>
        <location filename="../../romstatusexport.cpp" line="555"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Year</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="213"/>
        <location filename="../../romstatusexport.cpp" line="224"/>
        <location filename="../../romstatusexport.cpp" line="335"/>
        <location filename="../../romstatusexport.cpp" line="556"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Manufacturer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="218"/>
        <source>Game name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="231"/>
        <source>Sort order</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="244"/>
        <source>Select sort order</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="248"/>
        <source>Ascending</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="257"/>
        <source>Descending</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="907"/>
        <source>Choose ASCII export file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="907"/>
        <location filename="../../romstatusexport.cpp" line="919"/>
        <location filename="../../romstatusexport.cpp" line="931"/>
        <source>All files (*)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="919"/>
        <source>Choose CSV export file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="931"/>
        <source>Choose HTML export file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="134"/>
        <location filename="../../romstatusexport.cpp" line="440"/>
        <location filename="../../romstatusexport.cpp" line="658"/>
        <source>Confirm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="135"/>
        <location filename="../../romstatusexport.cpp" line="441"/>
        <location filename="../../romstatusexport.cpp" line="659"/>
        <source>Overwrite existing file?</source>
        <translation>Overwrite existing file?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="150"/>
        <source>exporting ROM status in ASCII format to &apos;%1&apos;</source>
        <translation>exporting ROM status in ASCII format to &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="152"/>
        <source>WARNING: can&apos;t open ASCII export file &apos;%1&apos; for writing, please check permissions</source>
        <translation>WARNING: can&apos;t open ASCII export file &apos;%1&apos; for writing, please check permissions</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="158"/>
        <source>exporting ROM status in ASCII format to clipboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="196"/>
        <location filename="../../romstatusexport.cpp" line="271"/>
        <location filename="../../romstatusexport.cpp" line="385"/>
        <location filename="../../romstatusexport.cpp" line="488"/>
        <location filename="../../romstatusexport.cpp" line="622"/>
        <location filename="../../romstatusexport.cpp" line="711"/>
        <location filename="../../romstatusexport.cpp" line="873"/>
        <source>unknown</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="169"/>
        <location filename="../../romstatusexport.cpp" line="198"/>
        <location filename="../../romstatusexport.cpp" line="490"/>
        <location filename="../../romstatusexport.cpp" line="717"/>
        <source>Emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="170"/>
        <location filename="../../romstatusexport.cpp" line="199"/>
        <location filename="../../romstatusexport.cpp" line="491"/>
        <location filename="../../romstatusexport.cpp" line="719"/>
        <source>Date</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="171"/>
        <location filename="../../romstatusexport.cpp" line="200"/>
        <location filename="../../romstatusexport.cpp" line="492"/>
        <location filename="../../romstatusexport.cpp" line="721"/>
        <source>Time</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="173"/>
        <location filename="../../romstatusexport.cpp" line="208"/>
        <location filename="../../romstatusexport.cpp" line="499"/>
        <location filename="../../romstatusexport.cpp" line="735"/>
        <source>Correct</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="174"/>
        <location filename="../../romstatusexport.cpp" line="209"/>
        <location filename="../../romstatusexport.cpp" line="500"/>
        <location filename="../../romstatusexport.cpp" line="737"/>
        <source>Mostly correct</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="175"/>
        <location filename="../../romstatusexport.cpp" line="210"/>
        <location filename="../../romstatusexport.cpp" line="501"/>
        <location filename="../../romstatusexport.cpp" line="739"/>
        <source>Incorrect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="176"/>
        <location filename="../../romstatusexport.cpp" line="211"/>
        <location filename="../../romstatusexport.cpp" line="502"/>
        <location filename="../../romstatusexport.cpp" line="741"/>
        <source>Not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="177"/>
        <location filename="../../romstatusexport.cpp" line="212"/>
        <location filename="../../romstatusexport.cpp" line="503"/>
        <location filename="../../romstatusexport.cpp" line="743"/>
        <source>Unknown</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="188"/>
        <location filename="../../romstatusexport.cpp" line="480"/>
        <location filename="../../romstatusexport.cpp" line="703"/>
        <source>SDLMAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="190"/>
        <location filename="../../romstatusexport.cpp" line="482"/>
        <location filename="../../romstatusexport.cpp" line="705"/>
        <source>SDLMESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="192"/>
        <location filename="../../romstatusexport.cpp" line="484"/>
        <location filename="../../romstatusexport.cpp" line="707"/>
        <source>MAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="194"/>
        <location filename="../../romstatusexport.cpp" line="486"/>
        <location filename="../../romstatusexport.cpp" line="709"/>
        <source>MESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="223"/>
        <location filename="../../romstatusexport.cpp" line="226"/>
        <location filename="../../romstatusexport.cpp" line="336"/>
        <location filename="../../romstatusexport.cpp" line="557"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>ROM types</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="419"/>
        <source>done (exporting ROM status in ASCII format to clipboard)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="422"/>
        <source>done (exporting ROM status in ASCII format to &apos;%1&apos;)</source>
        <translation>done (exporting ROM status in ASCII format to &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="944"/>
        <source>gamelist is not ready, please wait</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="222"/>
        <location filename="../../romstatusexport.cpp" line="331"/>
        <location filename="../../romstatusexport.cpp" line="552"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="223"/>
        <location filename="../../romstatusexport.cpp" line="332"/>
        <location filename="../../romstatusexport.cpp" line="553"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="221"/>
        <location filename="../../romstatusexport.cpp" line="333"/>
        <location filename="../../romstatusexport.cpp" line="554"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="242"/>
        <location filename="../../romstatusexport.cpp" line="368"/>
        <location filename="../../romstatusexport.cpp" line="577"/>
        <location filename="../../romstatusexport.cpp" line="820"/>
        <source>correct</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="249"/>
        <location filename="../../romstatusexport.cpp" line="372"/>
        <location filename="../../romstatusexport.cpp" line="588"/>
        <location filename="../../romstatusexport.cpp" line="833"/>
        <source>mostly correct</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="256"/>
        <location filename="../../romstatusexport.cpp" line="376"/>
        <location filename="../../romstatusexport.cpp" line="599"/>
        <location filename="../../romstatusexport.cpp" line="846"/>
        <source>incorrect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="263"/>
        <location filename="../../romstatusexport.cpp" line="380"/>
        <location filename="../../romstatusexport.cpp" line="610"/>
        <location filename="../../romstatusexport.cpp" line="859"/>
        <source>not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="219"/>
        <source>sorting, filtering and analyzing export data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="172"/>
        <location filename="../../romstatusexport.cpp" line="207"/>
        <location filename="../../romstatusexport.cpp" line="498"/>
        <location filename="../../romstatusexport.cpp" line="733"/>
        <source>Total sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="328"/>
        <source>done (sorting, filtering and analyzing export data)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="329"/>
        <location filename="../../romstatusexport.cpp" line="550"/>
        <location filename="../../romstatusexport.cpp" line="794"/>
        <source>writing export data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="415"/>
        <location filename="../../romstatusexport.cpp" line="633"/>
        <location filename="../../romstatusexport.cpp" line="887"/>
        <source>done (writing export data)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="456"/>
        <source>exporting ROM status in CSV format to &apos;%1&apos;</source>
        <translation>exporting ROM status in CSV format to &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="458"/>
        <source>WARNING: can&apos;t open CSV export file &apos;%1&apos; for writing, please check permissions</source>
        <translation>WARNING: can&apos;t open CSV export file &apos;%1&apos; for writing, please check permissions</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="464"/>
        <source>exporting ROM status in CSV format to clipboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="509"/>
        <location filename="../../romstatusexport.cpp" line="753"/>
        <source>sorting and filtering export data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="637"/>
        <source>done (exporting ROM status in CSV format to clipboard)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="640"/>
        <source>done (exporting ROM status in CSV format to &apos;%1&apos;)</source>
        <translation>done (exporting ROM status in CSV format to &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="185"/>
        <location filename="../../romstatusexport.cpp" line="186"/>
        <location filename="../../romstatusexport.cpp" line="478"/>
        <location filename="../../romstatusexport.cpp" line="697"/>
        <location filename="../../romstatusexport.cpp" line="713"/>
        <source>ROM Status Export - created by QMC2 %1</source>
        <translation>ROM Status Export - created by QMC2 %1</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="674"/>
        <source>exporting ROM status in HTML format to &apos;%1&apos;</source>
        <translation>exporting ROM status in HTML format to &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="676"/>
        <source>WARNING: can&apos;t open HTML export file &apos;%1&apos; for writing, please check permissions</source>
        <translation>WARNING: can&apos;t open HTML export file &apos;%1&apos; for writing, please check permissions</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="682"/>
        <source>exporting ROM status in HTML format to clipboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="891"/>
        <source>done (exporting ROM status in HTML format to clipboard)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="894"/>
        <source>done (exporting ROM status in HTML format to &apos;%1&apos;)</source>
        <translation>done (exporting ROM status in HTML format to &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="648"/>
        <source>Border width</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="664"/>
        <source>Border line width for tables (0 = no border)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="205"/>
        <location filename="../../romstatusexport.cpp" line="206"/>
        <location filename="../../romstatusexport.cpp" line="497"/>
        <location filename="../../romstatusexport.cpp" line="729"/>
        <source>Overall ROM Status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="215"/>
        <location filename="../../romstatusexport.cpp" line="216"/>
        <location filename="../../romstatusexport.cpp" line="506"/>
        <location filename="../../romstatusexport.cpp" line="749"/>
        <source>Detailed ROM Status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="549"/>
        <location filename="../../romstatusexport.cpp" line="793"/>
        <source>done (sorting and filtering export data)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="34"/>
        <source>Machine description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="35"/>
        <source>Machine name</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SampleChecker</name>
    <message>
        <location filename="../../sampcheck.cpp" line="132"/>
        <source>verifying samples</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="29"/>
        <location filename="../../sampcheck.cpp" line="134"/>
        <source>Good: 0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="42"/>
        <location filename="../../sampcheck.cpp" line="136"/>
        <source>Bad: 0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="55"/>
        <location filename="../../sampcheck.cpp" line="138"/>
        <source>Obsolete: 0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="140"/>
        <source>check pass 1: sample status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="194"/>
        <source>check pass 2: obsolete sample sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="216"/>
        <source>Obsolete: %1</source>
        <translation>Obsolete: %1</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="226"/>
        <source>done (verifying samples, elapsed time = %1)</source>
        <translation>done (verifying samples, elapsed time = %1)</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="227"/>
        <source>%1 good, %2 bad (or missing), %3 obsolete</source>
        <translation>%1 good, %2 bad (or missing), %3 obsolete</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="74"/>
        <location filename="../../sampcheck.cpp" line="232"/>
        <source>&amp;Check samples</source>
        <translation>&amp;Check samples</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="267"/>
        <location filename="../../sampcheck.cpp" line="318"/>
        <source>Good: %1</source>
        <translation>Good: %1</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="273"/>
        <location filename="../../sampcheck.cpp" line="324"/>
        <source>Bad: %1</source>
        <translation>Bad: %1</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="361"/>
        <source>please wait for reload to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="366"/>
        <source>please wait for ROM state filter to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="371"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="376"/>
        <source>please wait for image check to finish and try again</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="381"/>
        <source>stopping sample check upon user request</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="386"/>
        <source>&amp;Stop check</source>
        <translation>&amp;Stop check</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="15"/>
        <source>Check samples</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="94"/>
        <source>Select game in gamelist when selecting a sample set?</source>
        <translation>Select game in gamelist when selecting a sample set?</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="97"/>
        <source>Select &amp;game</source>
        <translation>Select &amp;game</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="120"/>
        <source>Close sample check dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="123"/>
        <source>C&amp;lose</source>
        <translation>C&amp;lose</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="84"/>
        <source>Remove obsolete sample sets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="87"/>
        <source>&amp;Remove obsolete</source>
        <translation>&amp;Remove obsolete</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="71"/>
        <source>Check samples / stop check</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="178"/>
        <source>WARNING: emulator audit call didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation>WARNING: emulator audit call didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="178"/>
        <source>normal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="178"/>
        <source>crashed</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SnapshotViewer</name>
    <message>
        <location filename="../../embedderopt.cpp" line="156"/>
        <source>Snapshot viewer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="165"/>
        <source>Use as preview</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="171"/>
        <source>Use as title</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="180"/>
        <source>Save as...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="186"/>
        <source>Copy to clipboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="291"/>
        <source>Choose PNG file to store image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="291"/>
        <source>PNG images (*.png)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="296"/>
        <source>FATAL: couldn&apos;t save snapshot image to &apos;%1&apos;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SoftwareList</name>
    <message>
        <location filename="../../softwarelist.ui" line="18"/>
        <source>Software list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="35"/>
        <location filename="../../softwarelist.ui" line="38"/>
        <source>Reload all information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="55"/>
        <location filename="../../softwarelist.ui" line="58"/>
        <source>Select a pre-defined device configuration to be added to the software setup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="62"/>
        <location filename="../../softwarelist.cpp" line="877"/>
        <location filename="../../softwarelist.cpp" line="1377"/>
        <source>No additional devices</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="109"/>
        <location filename="../../softwarelist.ui" line="112"/>
        <source>Remove the currently selected favorite software configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="129"/>
        <location filename="../../softwarelist.ui" line="132"/>
        <source>Play the selected software configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="149"/>
        <location filename="../../softwarelist.ui" line="152"/>
        <source>Play the selected software configuration (embedded)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="184"/>
        <source>Known software</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="187"/>
        <source>Complete list of known software for the current system</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="275"/>
        <source>View / manage your favorite software list for the current system</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="368"/>
        <source>Search within the list of known software for the current system</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="202"/>
        <location filename="../../softwarelist.ui" line="205"/>
        <source>List of known software</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="89"/>
        <location filename="../../softwarelist.ui" line="92"/>
        <source>Add the currently selected software and device setup to the favorites list (or overwrite existing favorite)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="221"/>
        <location filename="../../softwarelist.ui" line="309"/>
        <location filename="../../softwarelist.ui" line="421"/>
        <location filename="../../softwarelist.cpp" line="128"/>
        <location filename="../../softwarelist.cpp" line="147"/>
        <location filename="../../softwarelist.cpp" line="168"/>
        <source>Title</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="226"/>
        <location filename="../../softwarelist.ui" line="314"/>
        <location filename="../../softwarelist.ui" line="426"/>
        <location filename="../../softwarelist.cpp" line="130"/>
        <location filename="../../softwarelist.cpp" line="149"/>
        <location filename="../../softwarelist.cpp" line="170"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="231"/>
        <location filename="../../softwarelist.ui" line="319"/>
        <location filename="../../softwarelist.ui" line="431"/>
        <location filename="../../softwarelist.cpp" line="132"/>
        <location filename="../../softwarelist.cpp" line="151"/>
        <location filename="../../softwarelist.cpp" line="172"/>
        <source>Publisher</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="236"/>
        <location filename="../../softwarelist.ui" line="324"/>
        <location filename="../../softwarelist.ui" line="436"/>
        <location filename="../../softwarelist.cpp" line="134"/>
        <location filename="../../softwarelist.cpp" line="153"/>
        <location filename="../../softwarelist.cpp" line="174"/>
        <source>Year</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="241"/>
        <location filename="../../softwarelist.ui" line="329"/>
        <location filename="../../softwarelist.ui" line="441"/>
        <location filename="../../softwarelist.cpp" line="136"/>
        <location filename="../../softwarelist.cpp" line="155"/>
        <location filename="../../softwarelist.cpp" line="176"/>
        <source>Part</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="246"/>
        <location filename="../../softwarelist.ui" line="334"/>
        <location filename="../../softwarelist.ui" line="446"/>
        <location filename="../../softwarelist.cpp" line="138"/>
        <location filename="../../softwarelist.cpp" line="157"/>
        <location filename="../../softwarelist.cpp" line="178"/>
        <source>Interface</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="251"/>
        <location filename="../../softwarelist.ui" line="339"/>
        <location filename="../../softwarelist.ui" line="451"/>
        <location filename="../../softwarelist.cpp" line="140"/>
        <location filename="../../softwarelist.cpp" line="159"/>
        <location filename="../../softwarelist.cpp" line="180"/>
        <source>List</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="272"/>
        <source>Favorites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="290"/>
        <location filename="../../softwarelist.ui" line="293"/>
        <source>Favorite software configurations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="344"/>
        <location filename="../../softwarelist.cpp" line="161"/>
        <source>Device configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="365"/>
        <source>Search</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="383"/>
        <location filename="../../softwarelist.ui" line="386"/>
        <source>Search for known software (not case-sensitive)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="402"/>
        <location filename="../../softwarelist.ui" line="405"/>
        <source>Search results</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="475"/>
        <source>Loading software-lists, please wait...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="54"/>
        <source>Add the currently selected software to the favorites list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="61"/>
        <source>Enter search string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="88"/>
        <source>Play selected software</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="89"/>
        <source>&amp;Play</source>
        <translation>&amp;Play</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="94"/>
        <source>Play selected software (embedded)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="95"/>
        <source>Play &amp;embedded</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="101"/>
        <source>Add to favorite software list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="102"/>
        <source>&amp;Add to favorites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="106"/>
        <source>Remove from favorite software list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="107"/>
        <source>&amp;Remove from favorites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="221"/>
        <source>WARNING: software list &apos;%1&apos; not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="335"/>
        <source>Known software (%1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="336"/>
        <source>Favorites (%1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="337"/>
        <source>Search (%1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="351"/>
        <location filename="../../softwarelist.cpp" line="545"/>
        <source>Known software (no data available)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="352"/>
        <location filename="../../softwarelist.cpp" line="546"/>
        <source>Favorites (no data available)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="353"/>
        <location filename="../../softwarelist.cpp" line="547"/>
        <source>Search (no data available)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="419"/>
        <source>loading XML software list data from cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="424"/>
        <source>SWL cache - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="452"/>
        <source>done (loading XML software list data from cache, elapsed time = %1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="460"/>
        <source>ERROR: the file name for the MAME software list cache is empty -- please correct this and reload the game list afterwards</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="462"/>
        <source>ERROR: the file name for the MESS software list cache is empty -- please correct this and reload the machine list afterwards</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="478"/>
        <source>loading XML software list data and (re)creating cache</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="481"/>
        <source>SWL data - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="487"/>
        <source>ERROR: can&apos;t open the MAME software list cache for writing, path = %1 -- please check/correct access permissions and reload the game list afterwards</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="489"/>
        <source>ERROR: can&apos;t open the MESS software list cache for writing, path = %1 -- please check/correct access permissions and reload the machine list afterwards</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="579"/>
        <source>FATAL: error while parsing XML data for software list &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="759"/>
        <source>WARNING: the external process called to load the MAME software lists didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="759"/>
        <location filename="../../softwarelist.cpp" line="761"/>
        <source>normal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="759"/>
        <location filename="../../softwarelist.cpp" line="761"/>
        <source>crashed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="761"/>
        <source>WARNING: the external process called to load the MESS software lists didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="765"/>
        <source>done (loading XML software list data and (re)creating cache, elapsed time = %1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="818"/>
        <source>WARNING: the currently selected MAME emulator doesn&apos;t support software lists</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="820"/>
        <source>WARNING: the currently selected MESS emulator doesn&apos;t support software lists</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="837"/>
        <source>WARNING: the external process called to load the MAME software lists caused an error -- processError = %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="839"/>
        <source>WARNING: the external process called to load the MESS software lists caused an error -- processError = %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SoftwareSnap</name>
    <message>
        <location filename="../../softwarelist.cpp" line="1585"/>
        <source>Snapshot viewer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="1598"/>
        <source>Copy to clipboard</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Title</name>
    <message>
        <location filename="../../title.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="56"/>
        <location filename="../../title.cpp" line="57"/>
        <source>Game title image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="59"/>
        <location filename="../../title.cpp" line="60"/>
        <source>Machine title image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="68"/>
        <location filename="../../title.cpp" line="72"/>
        <source>FATAL: can&apos;t open title file, please check access permissions for %1</source>
        <translation>FATAL: can&apos;t open title file, please check access permissions for %1</translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Waiting for data...</translation>
    </message>
</context>
<context>
    <name>ToolExecutor</name>
    <message>
        <location filename="../../toolexec.cpp" line="60"/>
        <location filename="../../toolexec.cpp" line="62"/>
        <source>### tool started, output below ###</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="62"/>
        <location filename="../../toolexec.cpp" line="81"/>
        <location filename="../../toolexec.cpp" line="128"/>
        <source>tool control: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="79"/>
        <location filename="../../toolexec.cpp" line="81"/>
        <source>### tool finished, exit code = %1, exit status = %2 ###</source>
        <translation>### tool finished, exit code = %1, exit status = %2 ###</translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="100"/>
        <location filename="../../toolexec.cpp" line="116"/>
        <source>tool output: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="100"/>
        <source>stdout: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="116"/>
        <source>stderr: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="126"/>
        <location filename="../../toolexec.cpp" line="128"/>
        <source>### tool error, process error = %1 ###</source>
        <translation>### tool error, process error = %1 ###</translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="15"/>
        <source>Executing tool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="29"/>
        <source>Command</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="42"/>
        <source>Executed command</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="91"/>
        <source>Close tool execution dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="94"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="54"/>
        <source>Output from tool</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>VideoItemWidget</name>
    <message>
        <location filename="../../videoitemwidget.cpp" line="173"/>
        <source>Title:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="178"/>
        <location filename="../../videoitemwidget.cpp" line="180"/>
        <source>Author:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="178"/>
        <source>Open author URL with the default browser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="186"/>
        <source>Open video URL with the default browser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="186"/>
        <location filename="../../videoitemwidget.cpp" line="188"/>
        <source>Video:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../../welcome.cpp" line="33"/>
        <source>SDLMAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="35"/>
        <source>SDLMESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="40"/>
        <source>MAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="42"/>
        <source>MESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="94"/>
        <source>Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="94"/>
        <source>The specified file isn&apos;t executable!</source>
        <translation>The specified file isn&apos;t executable!</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="109"/>
        <source>Choose emulator executable file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="109"/>
        <source>All files (*)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="121"/>
        <source>Choose ROM path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="134"/>
        <source>Choose sample path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="147"/>
        <source>Choose hash path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="221"/>
        <source>It appears that another instance of %1 is already running.
However, this can also be the leftover of a previous crash.

Exit now, accept once or ignore completely?</source>
        <translation>It appears that another instance of %1 is already running.
However, this can also be the leftover of a previous crash.

Exit now, accept once or ignore completely?</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="15"/>
        <source>Welcome to QMC2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="606"/>
        <source>Browse sample path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="582"/>
        <source>Browse ROM path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="575"/>
        <source>Path to ROM images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="599"/>
        <source>Path to samples</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="630"/>
        <source>Emulator executable</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="637"/>
        <source>ROM path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="644"/>
        <source>Sample path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="651"/>
        <source>Hash path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="664"/>
        <source>Path to hash files</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="671"/>
        <source>Browse hash path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="716"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="709"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="528"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Welcome to QMC2!&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This appears to be your first start of QMC2 because no valid configuration was found. In order to use QMC2 as a front end for an emulator, you must specify the path to the emulator&apos;s executable file below.&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The paths below the executable file are optional, but you should specify as many of them as you can right now to avoid problems or confusion later (of course, you can change the paths in the emulator&apos;s global configuration at any time later).&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;It&apos;s strongly recommended that you specify the ROM path you are going to use at least!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="551"/>
        <source>Emulator executable file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="558"/>
        <source>Browse emulator executable file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="47"/>
        <source>Unsupported emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="49"/>
        <source>%1 executable file</source>
        <translation>%1 executable file</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="220"/>
        <source>Single-instance check</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="222"/>
        <source>&amp;Exit</source>
        <translation>&amp;Exit</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="222"/>
        <source>&amp;Once</source>
        <translation>&amp;Once</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="222"/>
        <source>&amp;Ignore</source>
        <translation>&amp;Ignore</translation>
    </message>
</context>
<context>
    <name>YouTubeVideoPlayer</name>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="40"/>
        <source>Attached videos</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="74"/>
        <source>Start playing / select next video automatically</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="77"/>
        <source>Play-O-Matic</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="93"/>
        <source>Mode:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="112"/>
        <source>Choose the video selection mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="116"/>
        <source>sequential</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="121"/>
        <source>random</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="138"/>
        <source>Allow videos to be repeated (otherwise stop after last video)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="141"/>
        <source>Allow repeat</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="174"/>
        <source>Video player</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="196"/>
        <source>Select the preferred video format (automatically falls back to the next available format)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="203"/>
        <location filename="../../youtubevideoplayer.cpp" line="79"/>
        <source>FLV 240P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="212"/>
        <location filename="../../youtubevideoplayer.cpp" line="80"/>
        <source>FLV 360P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="221"/>
        <location filename="../../youtubevideoplayer.cpp" line="81"/>
        <source>MP4 360P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="230"/>
        <location filename="../../youtubevideoplayer.cpp" line="82"/>
        <source>FLV 480P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="239"/>
        <location filename="../../youtubevideoplayer.cpp" line="83"/>
        <source>MP4 720P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="248"/>
        <location filename="../../youtubevideoplayer.cpp" line="84"/>
        <source>MP4 1080P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="257"/>
        <location filename="../../youtubevideoplayer.cpp" line="85"/>
        <source>MP4 3072P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="275"/>
        <location filename="../../youtubevideoplayer.cpp" line="146"/>
        <source>Start / pause / resume video playback</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="315"/>
        <source>Remaining playing time</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="334"/>
        <source>Current buffer fill level</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="363"/>
        <source>Search videos</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="378"/>
        <source>Search pattern -- use the &apos;hint&apos; button to get a suggestion</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="391"/>
        <source>Search YouTube videos using the specified search pattern</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="435"/>
        <source>Maximum number of results per search request</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="466"/>
        <source>Start index for the search request</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="491"/>
        <source>SI:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="498"/>
        <source>R:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="418"/>
        <source>Suggest a search pattern (hold down for menu)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="61"/>
        <source>Mute / unmute audio output</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="96"/>
        <source>Video progress</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="105"/>
        <location filename="../../youtubevideoplayer.cpp" line="724"/>
        <location filename="../../youtubevideoplayer.cpp" line="748"/>
        <location filename="../../youtubevideoplayer.cpp" line="767"/>
        <location filename="../../youtubevideoplayer.cpp" line="814"/>
        <source>Current buffer fill level: %1%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="112"/>
        <location filename="../../youtubevideoplayer.cpp" line="179"/>
        <source>Play this video</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="119"/>
        <location filename="../../youtubevideoplayer.cpp" line="161"/>
        <location filename="../../youtubevideoplayer.cpp" line="190"/>
        <source>Copy video URL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="125"/>
        <location filename="../../youtubevideoplayer.cpp" line="166"/>
        <location filename="../../youtubevideoplayer.cpp" line="195"/>
        <source>Copy author URL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="131"/>
        <source>Paste video URL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="138"/>
        <source>Remove selected videos</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="154"/>
        <location filename="../../youtubevideoplayer.cpp" line="1246"/>
        <source>Full screen (return with toggle-key)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="172"/>
        <location filename="../../youtubevideoplayer.cpp" line="184"/>
        <source>Attach this video</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="202"/>
        <source>Auto-suggest a search pattern?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="203"/>
        <source>Auto-suggest</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="208"/>
        <source>Enter string to be appended</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="209"/>
        <source>Append...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="220"/>
        <source>Enter search string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="324"/>
        <source>Appended string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="325"/>
        <source>Enter the string to be appended when suggesting a pattern:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="587"/>
        <source>video player: a video with the ID &apos;%1&apos; is already attached, ignored</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="797"/>
        <source>video player: playback error: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="946"/>
        <source>video player: video info error: ID = &apos;%1&apos;, status = &apos;%2&apos;, errorCode = &apos;%3&apos;, errorText = &apos;%4&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1015"/>
        <source>video player: video info error: timeout occurred</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1051"/>
        <source>video player: video info error: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1244"/>
        <source>Full screen (press %1 to return)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1419"/>
        <source>video player: video image info error: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1450"/>
        <source>video player: search request error: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1478"/>
        <source>video player: search error: can&apos;t parse XML data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1511"/>
        <source>video player: can&apos;t determine the video ID from the reply URL &apos;%1&apos; -- please inform developers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1517"/>
        <source>video player: can&apos;t associate the returned image for video ID &apos;%1&apos; -- please inform developers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1539"/>
        <source>video player: can&apos;t save the image for video ID &apos;%1&apos; to the YouTube cache directory &apos;%2&apos; -- please check permissions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1541"/>
        <source>video player: can&apos;t save the image for video ID &apos;%1&apos;, the YouTube cache directory &apos;%2&apos; does not exist -- please correct</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1543"/>
        <source>video player: image download failed for video ID &apos;%1&apos;, retrieved image is not valid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1545"/>
        <source>video player: image download failed for video ID &apos;%1&apos;, error text = &apos;%2&apos;</source>
        <translation></translation>
    </message>
</context>
</TS>
