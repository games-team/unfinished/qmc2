<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl_PL">
<context>
    <name>About</name>
    <message>
        <location filename="../../about.ui" line="15"/>
        <source>About QMC2</source>
        <translation>O QMC2</translation>
    </message>
    <message>
        <location filename="../../about.ui" line="515"/>
        <source>Project details</source>
        <translation>Szczegóły projektu</translation>
    </message>
    <message utf8="true">
        <location filename="../../about.ui" line="527"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;QMC2 - M.A.M.E. Catalog / Launcher II&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Qt 4 based UNIX multi-emulator frontend&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Version X.Y[.bZ], built for SDLMAME&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright © 2006 - 2008 R. Reucher, Germany&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;QMC2 - M.A.M.E. Catalog / Launcher II&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Oparty na Qt 4 interfejs graficzny dla wielu emulatorów dla UNIX-a&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Wersja X.Y[.bZ], zbudowana dla SDLMAME&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Prawa autorskie © 2006 - 2008 R. Reucher, Germany&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../about.ui" line="549"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Project homepage:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;http://www.mameworld.net/mamecat&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Development site:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;http://sourceforge.net/projects/qmc2&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;QMC2 development mailing list:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;qmc2-devel@lists.sourceforge.net (subscribers only)&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;List subscription:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;https://lists.sourceforge.net/lists/listinfo/qmc2-devel&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Strona domowa projektu:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;http://www.mameworld.net/mamecat&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Strona deweloperska:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;http://sourceforge.net/projects/qmc2&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Deweloperska lista dyskusyjna QMC2:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;qmc2-devel@lists.sourceforge.net (tylko członkowie)&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Zapis na listę:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;https://lists.sourceforge.net/lists/listinfo/qmc2-devel&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../about.ui" line="579"/>
        <source>System information</source>
        <translation>Informacje o systemie</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="120"/>
        <source>Version </source>
        <translation>Wersja </translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="122"/>
        <source>SVN r%1</source>
        <translation>SVN wersja %1</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="124"/>
        <source>built for</source>
        <translation>zbudowana dla</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="136"/>
        <source>Copyright</source>
        <translation>Prawa autorskie</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="136"/>
        <source>Germany</source>
        <translation>Niemcy</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="140"/>
        <source>Project homepage:</source>
        <translation>Strona domowa projektu:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="141"/>
        <source>Development site:</source>
        <translation>Strona deweloperska:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="142"/>
        <source>QMC2 development mailing list:</source>
        <translation>Deweloperska lista dyskusyjna QMC2:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="143"/>
        <source>List subscription:</source>
        <translation>Zapis na listę:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="144"/>
        <source>Bug tracking system:</source>
        <translation>System śledzenia błędów:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="149"/>
        <source>Build OS:</source>
        <translation>Zbudowano na:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="156"/>
        <source>Emulator version:</source>
        <translation>Wersja emulatora:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <source>Qt version:</source>
        <translation>Wersja Qt:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <location filename="../../about.cpp" line="160"/>
        <source>Compile-time:</source>
        <translation>Podczas kompilacji:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <location filename="../../about.cpp" line="160"/>
        <location filename="../../about.cpp" line="163"/>
        <source>Run-time:</source>
        <translation>Podczas uruchomienia:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <source>Build key:</source>
        <translation>Klucz builda:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="163"/>
        <source>Phonon backend / supported MIME types:</source>
        <translation>Backend phonona / obsługiwane typy MIME:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="175"/>
        <source>Environment variables:</source>
        <translation>Zmienne środowiskowe:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Total: %1 MB</source>
        <translation>Całkowita: %1 MB</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="72"/>
        <source>Windows 7 or Windows Server 2008 R2 (Windows 6.1)</source>
        <translation>Windows 7 albo Windows Server 2008 R2 (Windows 6.1)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Free: %1 MB</source>
        <translation>Wolna: %1 MB</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Used: %1 MB</source>
        <translation>W użyciu: %1 MB</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Physical memory:</source>
        <translation>Pamięć fizyczna:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="174"/>
        <source>Number of CPUs:</source>
        <translation>Liczba procesorów:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="174"/>
        <source>unknown</source>
        <translation>nieznany</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Template information:</source>
        <translation>Informacje o szablonie:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Emulator:</source>
        <translation>Emulator:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Version:</source>
        <translation>Wersja:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Format:</source>
        <translation>Format:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="160"/>
        <source>SDL version:</source>
        <translation>Wersja SDL:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="59"/>
        <source>Mac OS X 10.3</source>
        <translation>Mac OS X 10.3</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="60"/>
        <source>Mac OS X 10.4</source>
        <translation>Mac OS X 10.4</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="61"/>
        <source>Mac OS X 10.5</source>
        <translation>Mac OS X 10.5</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="62"/>
        <source>Mac OS X 10.6</source>
        <translation>Mac OS X 10.6</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="63"/>
        <source>Mac (unkown)</source>
        <translation>Mac (nieznany)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="67"/>
        <source>Windows NT (Windows 4.0)</source>
        <translation>Windows NT (Windows 4.0)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="68"/>
        <source>Windows 2000 (Windows 5.0)</source>
        <translation>Windows 2000 (Windows 5.0)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="69"/>
        <source>Windows XP (Windows 5.1)</source>
        <translation>Windows XP (Windows 5.1)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="70"/>
        <source>Windows Server 2003, Windows Server 2003 R2, Windows Home Server or Windows XP Professional x64 Edition (Windows 5.2)</source>
        <translation>Windows Server 2003, Windows Server 2003 R2, Windows Home Server albo Windows XP Professional x64 Edition (Windows 5.2)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="71"/>
        <source>Windows Vista or Windows Server 2008 (Windows 6.0)</source>
        <translation>Windows Vista albo Windows Server 2008 (Windows 6.0)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="73"/>
        <source>Windows (unknown)</source>
        <translation>Windows (nieznany)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="142"/>
        <source>subscription required</source>
        <translation>wymagana subskrypcja</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="152"/>
        <location filename="../../about.cpp" line="154"/>
        <source>Running OS:</source>
        <translation>Uruchomiony system operacyjny:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="119"/>
        <source>Qt 4 based multi-platform/multi-emulator front end</source>
        <translation>Oparty na Qt 4 wieloplatfowy interfejs graficzny dla wielu emulatorów</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="163"/>
        <source>Phonon version:</source>
        <translation>Wersja Phonona:</translation>
    </message>
</context>
<context>
    <name>ArcadeScene</name>
    <message>
        <location filename="../../arcade/arcadescene.cpp" line="36"/>
        <source>FPS: --</source>
        <translation>FPS: --</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescene.cpp" line="258"/>
        <location filename="../../arcade/arcadescene.cpp" line="260"/>
        <source>FPS: %1</source>
        <translation>FPS: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescene.cpp" line="303"/>
        <location filename="../../arcade/arcadescene.cpp" line="306"/>
        <source>Paused</source>
        <translation>Wstrzymano</translation>
    </message>
</context>
<context>
    <name>ArcadeScreenshotSaverThread</name>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="48"/>
        <source>ArcadeScreenshotSaverThread: Started</source>
        <translation>ArcadeScreenshotSaverThread: Uruchomiono</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="66"/>
        <source>ArcadeScreenshotSaverThread: Saving screen shot</source>
        <translation>ArcadeScreenshotSaverThread: Zapisywanie zrzutu ekranu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="80"/>
        <source>ArcadeScreenshotSaverThread: Screen shot successfully saved as &apos;%1&apos;</source>
        <translation>ArcadeScreenshotSaverThread: Zrzut ekranu zapisany poprawnie jako &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="82"/>
        <source>ArcadeScreenshotSaverThread: Failed to save screen shot as &apos;%1&apos;</source>
        <translation>ArcadeScreenshotSaverThread: Nie udało się zapisać zrzutu ekranu jako &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="92"/>
        <source>ArcadeScreenshotSaverThread: Ended</source>
        <translation>ArcadeScreenshotSaverThread: Zakończono</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="71"/>
        <source>ArcadeScreenshotSaverThread: Failed to create screen shot directory &apos;%1&apos; - aborting screen shot creation</source>
        <translation>ArcadeScreenshotSaverThread: Nie udało się utworzyć katalogu zrzutów ekranu &apos;%1&apos; - przerywanie tworzenia zrzutu ekranu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="83"/>
        <source>Saving screen shot</source>
        <translation>Zapisywanie zrzutu ekranu</translation>
    </message>
</context>
<context>
    <name>ArcadeSetupDialog</name>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="15"/>
        <source>Arcade setup</source>
        <translation>Ustawienia salonu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="25"/>
        <source>Graphics mode settings</source>
        <translation>Ustawienia trybu grafiki</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="58"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="648"/>
        <source>Arcade font (= system default if empty)</source>
        <translation>Czcionka salonu (= domyślna systemowa jeśli puste)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="658"/>
        <source>Browse arcade font</source>
        <translation>Wskaż czcionkę salonu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="295"/>
        <source>Snapshot directory</source>
        <translation>Katalog zrzutów ekranu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="304"/>
        <source>Directory to store snapshots (write)</source>
        <translation>Katalog do zapisywania zrzutów ekranu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="311"/>
        <source>Browse snapshot directory</source>
        <translation>Wskaż katalog zrzutów ekranu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="72"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="171"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="786"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1007"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1228"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1449"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1670"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1891"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2112"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2333"/>
        <source>X:</source>
        <translation>X:</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="95"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="194"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="812"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1033"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1254"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1475"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1696"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1917"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2138"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2359"/>
        <source>Y:</source>
        <translation>Y:</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="216"/>
        <source>Display arcade scene in full screen mode or windowed</source>
        <translation>Wyświetlaj salon w trybie pełnoekranowym lub w oknie</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="219"/>
        <source>Full screen</source>
        <translation>Pełny ekran</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="229"/>
        <source>Use window resolution in full screen mode (for slow systems)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="232"/>
        <source>Use window resolution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="158"/>
        <source>Aspect ratio</source>
        <translation>Proporcja</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="239"/>
        <source>Show frames per second counter in the lower left corner</source>
        <translation>Pokaż licznik klatek na sekundę w lewym dolnym rogu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="242"/>
        <source>Show FPS</source>
        <translation>Pokaż FPS</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="276"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="590"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="634"/>
        <source>Keep aspect ratio</source>
        <translation>Utrzymuj proporcję</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="683"/>
        <source>Virtual resolution</source>
        <translation>Wirtualna rozdzielczość</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="711"/>
        <source>Virtual width of scene</source>
        <translation>Wirtualna szerokość salonu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="744"/>
        <source>Virtual height of scene</source>
        <translation>Wirtualna wysokość salonu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="253"/>
        <source>Enable anti aliasing on primitive drawing</source>
        <translation>Włącz antyaliasing rysowania prymitywów</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="256"/>
        <source>Primitive AA</source>
        <translation>AA prymitywów</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="263"/>
        <source>Scale items smoothly</source>
        <translation>Skaluj elementy gładko</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="266"/>
        <source>Smooth item scaling</source>
        <translation>Wygładzone skalowanie elementów</translation>
    </message>
    <message utf8="true">
        <location filename="../../arcade/arcadesetupdialog.ui" line="2540"/>
        <source>°</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="329"/>
        <source>OpenGL</source>
        <translation>OpenGL</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="343"/>
        <source>Enable direct rendering</source>
        <translation>Włącz renderowanie bezpośrednie</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="346"/>
        <source>Direct rendering</source>
        <translation>Renderowanie bezpośrednie</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="353"/>
        <source>Enable enhanced OpenGL anti aliasing</source>
        <translation>Włącz ulepszony antyaliasing OpenGL</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="356"/>
        <source>Anti aliasing</source>
        <translation>Antyaliasing</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="363"/>
        <source>Synchronize buffer swaps with screen</source>
        <translation>Synchronizacja aktualizacji bufora z ekranem</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="366"/>
        <source>Sync to screen</source>
        <translation>Synchronizuj z ekranem</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="373"/>
        <source>Enable double buffering (avoids flicker)</source>
        <translation>Włącz podwójne buforowanie (zapobiega migotaniu)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="376"/>
        <source>Double buffering</source>
        <translation>Podwójne buforowanie</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="383"/>
        <source>Enable depth buffering (Z-buffer)</source>
        <translation>Włącz buforowanie głębokości (bufor Z)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="386"/>
        <source>Depth buffering</source>
        <translation>Buforowanie głębokości</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="393"/>
        <source>Use RGBA color mode</source>
        <translation>Używaj trybu kolorów RGBA</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="396"/>
        <source>RGBA</source>
        <translation>RGBA</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="406"/>
        <source>Alpha channel</source>
        <translation>Kanał alfa</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="413"/>
        <source>Enable multi sampling</source>
        <translation>Włącz multisampling</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="416"/>
        <source>Multi sampling</source>
        <translation>Multisampling</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="423"/>
        <source>Enable OpenGL overlays</source>
        <translation>Włącz nakładki OpenGL</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="426"/>
        <source>Overlays</source>
        <translation>Nakładki</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="433"/>
        <source>Enable accumulator buffer</source>
        <translation>Włącz bufor akumulatora</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="436"/>
        <source>Accumulator buffer</source>
        <translation>Bufor akumulatora</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="443"/>
        <source>Enable stencil buffer</source>
        <translation>Włącz bufor szablonowy</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="446"/>
        <source>Stencil buffer</source>
        <translation>Bufor szablonowy</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="453"/>
        <source>Enable stereo buffer</source>
        <translation>Włącz bufor stereo</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="456"/>
        <source>Stereo buffer</source>
        <translation>Bufor stereo</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="486"/>
        <source>Scene layout</source>
        <translation>Układ salonu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="492"/>
        <source>Layout name</source>
        <translation>Nazwa układu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="505"/>
        <source>Select the layout you want to edit / use</source>
        <translation>Wybierz układ, który chcesz edytować / używać</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2303"/>
        <source>Control display of MAWS lookup</source>
        <translation>Obraz kontrolny danych MAWS</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2306"/>
        <source>MAWS lookup</source>
        <translation>Dane MAWS</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2584"/>
        <source>Apply settings</source>
        <translation>Zastosuj ustawienia</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2587"/>
        <source>&amp;Apply</source>
        <translation>&amp;Zastosuj</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2601"/>
        <source>Restore currently applied settings</source>
        <translation>Przywróć bieżąco zastosowane ustawienia</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2604"/>
        <source>&amp;Restore</source>
        <translation>&amp;Przywróć</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2618"/>
        <source>Reset to default settings (click &lt;i&gt;Restore&lt;/i&gt; to restore currently applied settings!)</source>
        <translation>Zresetuj do ustawień domyślnych (kliknij &lt;i&gt;Przywróć&lt;/i&gt; aby przywrócić bieżąco zastosowane ustawienia!)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2621"/>
        <source>&amp;Default</source>
        <translation>&amp;Domyślne</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2645"/>
        <source>Close and apply settings</source>
        <translation>Zamknij i zastosuj ustawienia</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2648"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2655"/>
        <source>Close and discard changes</source>
        <translation>Zamknij i porzuć zmiany</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2658"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="273"/>
        <source>Keep aspect ratio when resizing scene window</source>
        <translation>Zachowaj proporcje podczas zmiany rozmiaru okna salonu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="403"/>
        <source>Use alpha channel information for transparency</source>
        <translation>Użyj informacji kanału alfa dla przezroczystości</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2537"/>
        <source>Scene rotation angle in degrees</source>
        <translation>Kąt obrotu salonu w stopniach</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="518"/>
        <source>Items, placements and parameters</source>
        <translation>Elementy, lokalizacje i parametry</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="556"/>
        <source>Background image</source>
        <translation>Obraz tła</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="600"/>
        <source>Foreground image</source>
        <translation>Obraz pierwszoplanowy</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="759"/>
        <source>Game list</source>
        <translation>Lista gier</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="771"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="992"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1213"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1434"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1655"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1876"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2097"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2318"/>
        <source>Geometry</source>
        <translation>Geometria</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="796"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1017"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1238"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1459"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1680"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1901"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2122"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2343"/>
        <source>X coordinate of item position</source>
        <translation>Współrzędna X pozycji elementu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="822"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1043"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1264"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1485"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1706"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1927"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2148"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2369"/>
        <source>Y coordinate of item position</source>
        <translation>Współrzędna Y pozycji elementu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="118"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="698"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="838"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1059"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1280"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1501"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1722"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1943"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2164"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2385"/>
        <source>W:</source>
        <translation>Sz:</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="848"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1069"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1290"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1511"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1732"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1953"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2174"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2395"/>
        <source>Item width</source>
        <translation>Szerokość elementu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="141"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="731"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="864"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1085"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1306"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1527"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1748"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1969"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2190"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2411"/>
        <source>H:</source>
        <translation>W:</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="874"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1095"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1316"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1537"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1758"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1979"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2200"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2421"/>
        <source>Item height</source>
        <translation>Wysokość elementu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="886"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1107"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1328"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1549"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1770"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1991"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2212"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2433"/>
        <source>Use background for this item</source>
        <translation>Użyj tła dla tego elementu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="889"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1110"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1331"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1552"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1773"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1994"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2215"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2436"/>
        <source>Background</source>
        <translation>Tło</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="904"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1125"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1346"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1567"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1788"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2009"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2230"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2451"/>
        <source>Select background color</source>
        <translation>Wybierz kolor tła</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="918"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1139"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1360"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1581"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1802"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2023"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2244"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2465"/>
        <source>T:</source>
        <translation>P:</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="925"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1146"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1367"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1588"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1809"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2030"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2251"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2472"/>
        <source>Select background transparency</source>
        <translation>Wybierz przezroczystość tła</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="928"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1149"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1370"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1591"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1812"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2033"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2254"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2475"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="938"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1159"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1380"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1601"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1822"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2043"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2264"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2485"/>
        <source>Use texture bitmap for background</source>
        <translation>Użyj tekstury jako tła</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="941"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1162"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1383"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1604"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1825"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2046"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2267"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2488"/>
        <source>Texture</source>
        <translation>Tekstura</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="948"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1169"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1390"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1611"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1832"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2053"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2274"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2495"/>
        <source>Texture bitmap for background</source>
        <translation>Tekstura tła</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="961"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1182"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1403"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1624"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1845"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2066"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2287"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2508"/>
        <source>Browse texture bitmap for background</source>
        <translation>Wskaż teksturę tła</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="980"/>
        <source>Preview image</source>
        <translation>Obraz podglądu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1201"/>
        <source>Flyer image</source>
        <translation>Obraz ulotki</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="641"/>
        <source>Arcade font</source>
        <translation>Czcionka salonu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="669"/>
        <source>Select font color</source>
        <translation>Wybierz kolor czcionki</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="672"/>
        <source>Font color</source>
        <translation>Kolor czcionki</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.cpp" line="25"/>
        <source>Machine list</source>
        <translation>Lista maszyn</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.cpp" line="26"/>
        <source>Control display of machine list</source>
        <translation>Obraz kontrolny listy maszyn</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="553"/>
        <source>Use a background image</source>
        <translation>Używaj obrazu tła</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="563"/>
        <source>Background image file (read)</source>
        <translation>Plik obrazu tła (odczyt)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="576"/>
        <source>Browse background image file</source>
        <translation>Wskaż plik obrazu tła</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="587"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="631"/>
        <source>Keep image&apos;s aspect ratio when scaling</source>
        <translation>Zachowaj proporcję obrazu podczas skalowania</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="597"/>
        <source>Use a foreground image</source>
        <translation>Używaj obrazu pierwszoplanowego</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="607"/>
        <source>Foreground image file (read)</source>
        <translation>Plik obrazu pierwszoplanowego (odczyt)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="620"/>
        <source>Browse foreground image file</source>
        <translation>Wskaż plik obrazu pierwszoplanowego</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="756"/>
        <source>Control display of game list</source>
        <translation>Obraz kontrolny listy gier</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="977"/>
        <source>Control display of preview image</source>
        <translation>Obraz kontrolny podglądu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1198"/>
        <source>Control display of flyer image</source>
        <translation>Obraz kontrolny ulotki</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="79"/>
        <source>X coordinate of scene window position</source>
        <translation>Odcięta pozycji okna salonu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="102"/>
        <source>Y coordinate of scene window position</source>
        <translation>Rzędna pozycji okna salonu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="125"/>
        <source>Width of scene window</source>
        <translation>Szerokość okna salonu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="148"/>
        <source>Height of scene window</source>
        <translation>Wysokość okna salonu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="178"/>
        <source>X portion of scene aspect ratio (should be equal to screen&apos;s aspect ratio)</source>
        <translation>Pozioma część proporcji salonu (powinna być równa proporcji ekranu)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="201"/>
        <source>Y portion of scene aspect ratio (should be equal to screen&apos;s aspect ratio)</source>
        <translation>Pionowa część proporcji salonu (powinna być równa proporcji ekranu)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2530"/>
        <source>Scene rotation</source>
        <translation>Obrót salonu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="285"/>
        <source>Center arcade window on screen</source>
        <translation>Wyśrodkuj okno salonu na ekranie</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="288"/>
        <source>Center window</source>
        <translation>Wyśrodkuj okno</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1419"/>
        <source>Control display of cabinet image</source>
        <translation>Obraz kontrolny automatu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1422"/>
        <source>Cabinet image</source>
        <translation>Obraz automatu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1640"/>
        <source>Control display of controller image</source>
        <translation>Obraz kontrolny kontrolera</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1643"/>
        <source>Controller image</source>
        <translation>Obraz kontrolera</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1861"/>
        <source>Control display of marquee image</source>
        <translation>Obraz kontrolny planszy tytułowej</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1864"/>
        <source>Marquee image</source>
        <translation>Obraz planszy tytułowej</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2082"/>
        <source>Control display of title image</source>
        <translation>Obraz kontrolny ekranu tytułowego</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2085"/>
        <source>Title image</source>
        <translation>Obraz ekranu tytułowego</translation>
    </message>
</context>
<context>
    <name>ArcadeView</name>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="53"/>
        <source>QMC2 - ArcadeView</source>
        <translation>QMC2 - ArcadeView</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="88"/>
        <source>ArcadeView: Cleaning up</source>
        <translation>ArcadeView: Czyszczenie</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="120"/>
        <source>ArcadeView: Switching to windowed mode</source>
        <translation>ArcadeView: Przełączanie do trybu okna</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="130"/>
        <source>ArcadeView: Resolution switching is not yet supported</source>
        <translation>ArcadeView: Przełączanie rozdzielczości nie jest jeszcze obsługiwane</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="146"/>
        <source>ArcadeView: Setting window size to %1x%2</source>
        <translation>ArcadeView: Ustawianie rozmiaru okna na %1x%2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="156"/>
        <source>ArcadeView: Setting window position to %1, %2</source>
        <translation>ArcadeView: Ustawianie pozycji okna na %1, %2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="170"/>
        <source>ArcadeView: This system does not appear to support OpenGL -- reverting to non-OpenGL / software renderer</source>
        <translation>ArcadeView: Ten system nie wydaje się obsługiwać OpenGL -- powracanie do renderera nie-OpenGL / programowego</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="175"/>
        <source>ArcadeView: Using OpenGL renderer</source>
        <translation>ArcadeView: Używanie renderera OpenGL</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="179"/>
        <source>ArcadeView: This system does not appear to support vertical syncing -- disabling SyncToScreen</source>
        <translation>ArcadeView: Ten system nie wydaje się obsługiwać synchronizacji pionowej -- wyłączanie SyncToScreen</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="182"/>
        <source>ArcadeView: OpenGL: SyncToScreen: %1</source>
        <translation>ArcadeView: OpenGL: SyncToScreen: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="182"/>
        <location filename="../../arcade/arcadeview.cpp" line="184"/>
        <location filename="../../arcade/arcadeview.cpp" line="186"/>
        <location filename="../../arcade/arcadeview.cpp" line="188"/>
        <location filename="../../arcade/arcadeview.cpp" line="190"/>
        <location filename="../../arcade/arcadeview.cpp" line="192"/>
        <location filename="../../arcade/arcadeview.cpp" line="194"/>
        <location filename="../../arcade/arcadeview.cpp" line="196"/>
        <location filename="../../arcade/arcadeview.cpp" line="198"/>
        <location filename="../../arcade/arcadeview.cpp" line="204"/>
        <location filename="../../arcade/arcadeview.cpp" line="210"/>
        <location filename="../../arcade/arcadeview.cpp" line="212"/>
        <source>on</source>
        <translation>wł</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="182"/>
        <location filename="../../arcade/arcadeview.cpp" line="184"/>
        <location filename="../../arcade/arcadeview.cpp" line="186"/>
        <location filename="../../arcade/arcadeview.cpp" line="188"/>
        <location filename="../../arcade/arcadeview.cpp" line="190"/>
        <location filename="../../arcade/arcadeview.cpp" line="192"/>
        <location filename="../../arcade/arcadeview.cpp" line="194"/>
        <location filename="../../arcade/arcadeview.cpp" line="196"/>
        <location filename="../../arcade/arcadeview.cpp" line="198"/>
        <location filename="../../arcade/arcadeview.cpp" line="204"/>
        <location filename="../../arcade/arcadeview.cpp" line="210"/>
        <location filename="../../arcade/arcadeview.cpp" line="212"/>
        <source>off</source>
        <translation>wył</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="184"/>
        <source>ArcadeView: OpenGL: DoubleBuffer: %1</source>
        <translation>ArcadeView: OpenGL: DoubleBuffer: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="186"/>
        <source>ArcadeView: OpenGL: DepthBuffer: %1</source>
        <translation>ArcadeView: OpenGL: DepthBuffer: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="188"/>
        <source>ArcadeView: OpenGL: RGBA: %1</source>
        <translation>ArcadeView: OpenGL: RGBA: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="190"/>
        <source>ArcadeView: OpenGL: AlphaChannel: %1</source>
        <translation>ArcadeView: OpenGL: AlphaChannel: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="192"/>
        <source>ArcadeView: OpenGL: AccumulatorBuffer: %1</source>
        <translation>ArcadeView: OpenGL: AccumulatorBuffer: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="194"/>
        <source>ArcadeView: OpenGL: StencilBuffer: %1</source>
        <translation>ArcadeView: OpenGL: StencilBuffer: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="196"/>
        <source>ArcadeView: OpenGL: Stereo: %1</source>
        <translation>ArcadeView: OpenGL: Stereo: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="198"/>
        <source>ArcadeView: OpenGL: DirectRendering: %1</source>
        <translation>ArcadeView: OpenGL: DirectRendering: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="201"/>
        <source>ArcadeView: This system does not appear to support OpenGL overlays -- disabling OpenGL overlays</source>
        <translation>ArcadeView: Ten system nie wydaje się obsługiwać nakładek OpenGL -- wyłączanie nakładek OpenGL</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="204"/>
        <source>ArcadeView: OpenGL: Overlay: %1</source>
        <translation>ArcadeView: OpenGL: Overlay: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="207"/>
        <source>ArcadeView: This system does not appear to support OpenGL multi sampling -- disabling MultiSample</source>
        <translation>ArcadeView: Ten system nie wydaje się obsługiwać multisamplingu -- wyłączanie multisamplingu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="210"/>
        <source>ArcadeView: OpenGL: MultiSample: %1</source>
        <translation>ArcadeView: OpenGL: MultiSample: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="212"/>
        <source>ArcadeView: OpenGL: AntiAliasing: %1</source>
        <translation>ArcadeView: OpenGL: AntiAliasing: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="222"/>
        <source>ArcadeView: Using software renderer</source>
        <translation>ArcadeView: Używanie renderera programowego</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="227"/>
        <source>ArcadeView: X11: Screen number: %1</source>
        <translation>ArcadeView: X11: Numer ekranu: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="228"/>
        <source>ArcadeView: X11: Color depth: %1</source>
        <translation>ArcadeView: X11: Głębia kolorów: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="229"/>
        <source>ArcadeView: X11: DPI-X: %1</source>
        <translation>ArcadeView: X11: DPI-X: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="230"/>
        <source>ArcadeView: X11: DPI-Y: %1</source>
        <translation>ArcadeView: X11: DPI-Y: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="231"/>
        <source>ArcadeView: X11: Compositing manager: %1</source>
        <translation>ArcadeView: X11: Menedżer składania: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="231"/>
        <source>running</source>
        <translation>uruchomiony</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="231"/>
        <source>not running</source>
        <translation>nieuruchomiony</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="235"/>
        <source>ArcadeView: Screen geometry: %1x%2</source>
        <translation>ArcadeView: Geometria ekranu: %1x%2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="236"/>
        <source>ArcadeView: Virtual resolution: %1x%2</source>
        <translation>ArcadeView: Wirtualna rozdzielczość: %1x%2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="237"/>
        <source>ArcadeView: Selected aspect ratio: %1:%2</source>
        <translation>ArcadeView: Wybrana proporcja: %1:%2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="241"/>
        <source>ArcadeView: Virtual resolution doesn&apos;t fit aspect ratio -- scene coordinates may be stretched or compressed</source>
        <translation>ArcadeView: Wirtualna rozdzielczość nie zgadza się z proporcjami ekranu -- współrzędne salonu mogą być rozciągniętę bądź ściśnięte</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="242"/>
        <source>ArcadeView: Aspect ratio will be maintained</source>
        <translation>ArcadeView: Proporcja będzie utrzymana</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="242"/>
        <source>ArcadeView: Aspect ratio will not be maintained</source>
        <translation>ArcadeView: Proporcja nie będzie utrzymana</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="243"/>
        <source>ArcadeView: FPS counter display %1</source>
        <translation>ArcadeView: Wyświetlanie licznika FPS %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="243"/>
        <location filename="../../arcade/arcadeview.cpp" line="246"/>
        <source>activated</source>
        <translation>włączone</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="243"/>
        <location filename="../../arcade/arcadeview.cpp" line="246"/>
        <source>deactivated</source>
        <translation>wyłączone</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="246"/>
        <source>ArcadeView: Primitive antialiasing %1</source>
        <translation>ArcadeView: Antyaliasing prymitywów %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="250"/>
        <source>ArcadeView: Centering window on screen</source>
        <translation>ArcadeView: Wyśrodkowywanie okna na ekranie</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="254"/>
        <source>ArcadeView: Restoring saved window position</source>
        <translation>ArcadeView: Przywracannie zapisanej pozycji okna</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="300"/>
        <source>ArcadeView: Adjusting window size to %1x%2 to maintain the aspect ratio</source>
        <translation>ArcadeView: Poprawianie rozmiaru okna do %1x%2 w celu zachowania proporcji</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="281"/>
        <source>ArcadeView: Rendering screen shot</source>
        <translation>ArcadeView: Renderowanie zrzutu ekranu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="290"/>
        <source>Saving screen shot</source>
        <translation>Zapisywanie zrzutu ekranu</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="238"/>
        <source>ArcadeView: Scene rotation angle: %1 degrees</source>
        <translation>ArcadeView: Kąt obrotu salonu: %1 stopni</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="127"/>
        <source>ArcadeView: Switching to full screen mode</source>
        <translation>ArcadeView: Przełączanie do trybu pełnoekranowego</translation>
    </message>
</context>
<context>
    <name>AudioEffectDialog</name>
    <message>
        <location filename="../../audioeffects.cpp" line="35"/>
        <location filename="../../audioeffects.cpp" line="194"/>
        <location filename="../../audioeffects.cpp" line="199"/>
        <source>Enable effect &apos;%1&apos;</source>
        <translation>Włącz efekt &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="52"/>
        <source>Setup effect &apos;%1&apos;</source>
        <translation>Konfiguruj efekt &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="186"/>
        <location filename="../../audioeffects.cpp" line="207"/>
        <source>Disable effect &apos;%1&apos;</source>
        <translation>Wyłącz efekt &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="195"/>
        <source>WARNING: audio player: can&apos;t insert effect &apos;%1&apos;</source>
        <translation>UWAGA: odtwarzacz dźwięku nie może dodać efektu &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="208"/>
        <source>WARNING: audio player: can&apos;t remove effect &apos;%1&apos;</source>
        <translation>UWAGA: odtwarzacz dźwięku nie może usunąć efektu &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="14"/>
        <source>Audio effects</source>
        <translation>Efekty dźwięku</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="36"/>
        <source>Close audio effects dialog</source>
        <translation>Zamknij okno efektów dźwięku</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="39"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="59"/>
        <location filename="../../audioeffects.ui" line="62"/>
        <source>List of available audio effects</source>
        <translation>Lista dostępnych efektów dźwięku</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="81"/>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="86"/>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="91"/>
        <source>Enable</source>
        <translation>Włącz</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="96"/>
        <source>Setup</source>
        <translation>Ustawienia</translation>
    </message>
</context>
<context>
    <name>Cabinet</name>
    <message>
        <location filename="../../cabinet.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>Kopiuj do schowka</translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="56"/>
        <location filename="../../cabinet.cpp" line="57"/>
        <source>Game cabinet image</source>
        <translation>Obraz automatu gry</translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="59"/>
        <location filename="../../cabinet.cpp" line="60"/>
        <source>Machine cabinet image</source>
        <translation>Obraz maszyny</translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="68"/>
        <location filename="../../cabinet.cpp" line="72"/>
        <source>FATAL: can&apos;t open cabinet file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku obrazu automatu, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Oczekiwanie na dane...</translation>
    </message>
</context>
<context>
    <name>Controller</name>
    <message>
        <location filename="../../controller.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>Kopiuj do schowka</translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="56"/>
        <location filename="../../controller.cpp" line="57"/>
        <source>Game controller image</source>
        <translation>Obraz kontrolera gry</translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="59"/>
        <location filename="../../controller.cpp" line="60"/>
        <source>Machine controller image</source>
        <translation>Obraz kontrolera maszyny</translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="68"/>
        <location filename="../../controller.cpp" line="72"/>
        <source>FATAL: can&apos;t open controller file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku kontrolera, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Oczekiwanie na dane...</translation>
    </message>
</context>
<context>
    <name>DemoModeDialog</name>
    <message>
        <location filename="../../demomode.cpp" line="105"/>
        <source>demo mode stopped</source>
        <translation>tryb demonstracyjny zatrzymany</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="263"/>
        <location filename="../../demomode.cpp" line="107"/>
        <source>Run &amp;demo</source>
        <translation>Uruchom &amp;demonstrację</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="260"/>
        <location filename="../../demomode.cpp" line="108"/>
        <source>Run demo now</source>
        <translation>Uruchom demonstrację teraz</translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="124"/>
        <source>please wait for reload to finish and try again</source>
        <translation>proszę poczekać za zakończenie przeładowywania i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="128"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation>proszę poczekać za zakończenie weryfikacji ROM-u i spróbować ponownie</translation>
    </message>
    <message numerus="yes">
        <location filename="../../demomode.cpp" line="170"/>
        <source>demo mode started -- %n game(s) selected by filter</source>
        <translation>
            <numerusform>tryb demonstracyjny uruchomiono -- %n gra wybrana za pomocą filtra</numerusform>
            <numerusform>tryb demonstracyjny uruchomiono -- %n gry wybrane za pomocą filtra</numerusform>
            <numerusform>tryb demonstracyjny uruchomiono -- %n gier wybranych za pomocą filtra</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="172"/>
        <source>demo mode cannot start -- no games selected by filter</source>
        <translation>tryb demonstracyjny nie może zostać uruchomiony -- żadna gra nie została wybrana przez filtr</translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="176"/>
        <source>Stop &amp;demo</source>
        <translation>Zatrzymaj &amp;demonstrację</translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="177"/>
        <source>Stop demo now</source>
        <translation>Zatrzymaj demonstrację teraz</translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="240"/>
        <source>starting emulation in demo mode for &apos;%1&apos;</source>
        <translation>uruchamianie emulacji w trybie demonstracyjnym dla &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="14"/>
        <source>Demo mode</source>
        <translation>Tryb demonstracyjny</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="26"/>
        <source>ROM state filter</source>
        <translation>Filtr stanu ROM-ów</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="44"/>
        <source>Select ROM state C (correct)?</source>
        <translation>Wybrać stan ROM-ów P (poprawny)?</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="67"/>
        <source>Select ROM state M (mostly correct)?</source>
        <translation>Wybrać stan ROM-ów Wp (w większości poprawny)?</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="90"/>
        <source>Select ROM state I (incorrect)?</source>
        <translation>Wybrać stan ROM-ów Np (niepoprawny)?</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="110"/>
        <source>Select ROM state N (not found)?</source>
        <translation>Wybrać stan ROM-ów Nz (nieznaleziony)?</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="130"/>
        <source>Select ROM state U (unknown)?</source>
        <translation>Wybrać stan ROM-ów Nn (nieznany)?</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="152"/>
        <source>Seconds to run</source>
        <translation>Sekundy uruchomienia</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="159"/>
        <source>Number of seconds to run an emulator in demo mode</source>
        <translation>Liczba sekund, przez którą emulator ma być uruchomiony w trybie demonstracyjnym</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="213"/>
        <source>Use only tagged games</source>
        <translation>Używanie jedynie zaznaczonych gier</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="216"/>
        <source>Tagged</source>
        <translation>Zaznaczone</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="289"/>
        <source>Pause (seconds)</source>
        <translation>Pauza (sekundy)</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="296"/>
        <source>Number of seconds to pause between emulator runs</source>
        <translation>Liczba sekund oczekiwania pomiędzy kolejnymi uruchomieniami emulatora</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="174"/>
        <source>Start emulators in full screen mode (otherwise use windowed mode)</source>
        <translation>Uruchamiaj emulatory w trybie pełnoekranowym (w przeciwnym wypadku używaj okna)</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="177"/>
        <source>Full screen</source>
        <translation>Pełny ekran</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="190"/>
        <source>Maximize emulators when in windowed mode</source>
        <translation>Maksymalizuj okna emulatora</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="193"/>
        <source>Maximized</source>
        <translation>Maksymalizacja</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="203"/>
        <source>Embed windowed emulators</source>
        <translation>Osadzaj okna emulatora</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="206"/>
        <source>Embedded</source>
        <translation>Osadzone</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="231"/>
        <source>Close this dialog (and stop running demo)</source>
        <translation>Zamknij to okno dialogowe (i zakończ pokazywanie demonstracji)</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="234"/>
        <source>&amp;Close</source>
        <translation>&amp;Zamknij</translation>
    </message>
</context>
<context>
    <name>DetailSetup</name>
    <message>
        <location filename="../../detailsetup.ui" line="15"/>
        <source>Detail setup</source>
        <translation>Ustawienia szczegółów</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="201"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="36"/>
        <source>List of available details</source>
        <translation>Lista dostępnych szczegółów</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="91"/>
        <source>List of active details and their order</source>
        <translation>Lista dostępnych szczegółów i ich porządek</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="68"/>
        <source>Activate selected details</source>
        <translation>Aktywuj wybrane szczegóły</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="106"/>
        <source>Deactivate selected details</source>
        <translation>Deaktywuj wybrane szczegóły</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="173"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="21"/>
        <location filename="../../detailsetup.cpp" line="98"/>
        <source>Pre&amp;view</source>
        <translation>&amp;Podgląd</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="22"/>
        <source>Game preview image</source>
        <translation>Obraz podglądu gry</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="24"/>
        <location filename="../../detailsetup.cpp" line="101"/>
        <source>Fl&amp;yer</source>
        <translation>&amp;Ulotka</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="25"/>
        <source>Game flyer image</source>
        <translation>Obraz ulotki gry</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="27"/>
        <source>Game &amp;info</source>
        <translation>&amp;Informacje o grze</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="28"/>
        <source>Game information</source>
        <translation>Informacje o grze</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="30"/>
        <location filename="../../detailsetup.cpp" line="107"/>
        <source>Em&amp;ulator info</source>
        <translation>Informacje o &amp;emulatorze</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="31"/>
        <location filename="../../detailsetup.cpp" line="108"/>
        <source>Emulator information</source>
        <translation>Informacje o emulatorze</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="33"/>
        <location filename="../../detailsetup.cpp" line="110"/>
        <source>&amp;Configuration</source>
        <translation>&amp;Konfiguracja</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="34"/>
        <location filename="../../detailsetup.cpp" line="111"/>
        <source>Emulator configuration</source>
        <translation>Konfiguracja emulatora</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="36"/>
        <location filename="../../detailsetup.cpp" line="119"/>
        <source>Ca&amp;binet</source>
        <translation>Auto&amp;mat</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="37"/>
        <source>Arcade cabinet image</source>
        <translation>Obraz automatu</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="39"/>
        <source>C&amp;ontroller</source>
        <translation>K&amp;ontroler</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="40"/>
        <source>Control panel image</source>
        <translation>Obraz panelu kontrolnego</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="42"/>
        <source>Mar&amp;quee</source>
        <translation>P&amp;lansza tytułowa</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="43"/>
        <source>Marquee image</source>
        <translation>Obraz planszy tytułowej</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="45"/>
        <source>Titl&amp;e</source>
        <translation>Ekran &amp;tytułowy</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="46"/>
        <source>Title screen image</source>
        <translation>Obraz ekranu tytułowego</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="48"/>
        <source>MA&amp;WS</source>
        <translation>MA&amp;WS</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="51"/>
        <location filename="../../detailsetup.cpp" line="116"/>
        <source>&amp;PCB</source>
        <translation>Pł&amp;ytka</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="52"/>
        <location filename="../../detailsetup.cpp" line="117"/>
        <source>PCB image</source>
        <translation>Obraz płytki drukowanej</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="58"/>
        <location filename="../../detailsetup.cpp" line="126"/>
        <source>&amp;YouTube</source>
        <translation>&amp;YouTube</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="59"/>
        <location filename="../../detailsetup.cpp" line="127"/>
        <source>YouTube videos</source>
        <translation>Filmy YouTube</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="99"/>
        <source>Machine preview image</source>
        <translation>Obraz podglądu maszyny</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="102"/>
        <source>Machine flyer image</source>
        <translation>Obraz ulotki maszyny</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="104"/>
        <source>Machine &amp;info</source>
        <translation>&amp;Informacje o maszynie</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="105"/>
        <source>Machine information</source>
        <translation>Informacje o maszynie</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="113"/>
        <source>De&amp;vices</source>
        <translation>U&amp;rządzenia</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="114"/>
        <source>Device configuration</source>
        <translation>Konfiguracja urządzeń</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="54"/>
        <location filename="../../detailsetup.cpp" line="122"/>
        <source>Softwar&amp;e list</source>
        <translation>Lista &amp;oprogramowania</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="55"/>
        <location filename="../../detailsetup.cpp" line="123"/>
        <source>Software list</source>
        <translation>Lista oprogramowania</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="367"/>
        <source>MAWS configuration (1/2)</source>
        <translation>Konfiguracja MAWS (1/2)</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="379"/>
        <source>Enable MAWS quick download?</source>
        <translation>Włączyć szybkie pobieranie MAWS?</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="30"/>
        <source>Available details</source>
        <translation>Dostępne szczegóły</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="85"/>
        <source>Active details</source>
        <translation>Aktywne szczegóły</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="120"/>
        <source>Move selected detail up</source>
        <translation>Przesuń wybrany szczegół w górę</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="137"/>
        <source>Move selected detail down</source>
        <translation>Przesuń wybrany szczegół w dół</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="170"/>
        <source>Apply detail setup and close dialog</source>
        <translation>Zastosuj ustawienia szczegółów i zamknij okno</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="198"/>
        <source>Cancel detail setup and close dialog</source>
        <translation>Anuluj ustawienia szczegółów i zamknij okno</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="184"/>
        <source>Apply detail setup</source>
        <translation>Zastosuj ustawienia szczegółów</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="187"/>
        <source>&amp;Apply</source>
        <translation>&amp;Zastosuj</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="49"/>
        <source>MAWS page (web lookup)</source>
        <translation>Strona MAWS (odwołanie do sieci)</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="120"/>
        <source>Machine cabinet image</source>
        <translation>Obraz maszyny</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="377"/>
        <location filename="../../detailsetup.cpp" line="382"/>
        <source>Yes</source>
        <translation>Tak</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="377"/>
        <source>No</source>
        <translation>Nie</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="379"/>
        <source>MAWS configuration (2/2)</source>
        <translation>Konfiguracja MAWS (2/2)</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="402"/>
        <source>Choose the YouTube cache directory</source>
        <translation>Wybierz katalog bufora YouTube</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="414"/>
        <source>FATAL: can&apos;t create new YouTube cache directory, path = %1</source>
        <translation>FATALNIE: nie można stworzyć katalogu bufora YouTube, ścieżka = %1</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="444"/>
        <source>INFO: the configuration tab can&apos;t be removed</source>
        <translation>INFORMACJA: karta konfiguracji nie może zostać usunięta</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="51"/>
        <source>Configure current detail</source>
        <translation>Konfiguracja wybranego szczegółu</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="54"/>
        <source>Configure...</source>
        <translation>Konfiguracja...</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="368"/>
        <source>MAWS URL pattern (use %1 as placeholder for game ID):</source>
        <translation>Wzór adresu URL MAWS (użyj %1 w miejsce identyfikatora gry):</translation>
    </message>
</context>
<context>
    <name>DirectoryEditWidget</name>
    <message>
        <location filename="../../direditwidget.cpp" line="44"/>
        <source>Choose directory</source>
        <translation>Wybierz katalog</translation>
    </message>
</context>
<context>
    <name>DocBrowser</name>
    <message>
        <location filename="../../docbrowser.ui" line="15"/>
        <location filename="../../docbrowser.cpp" line="86"/>
        <location filename="../../docbrowser.cpp" line="91"/>
        <location filename="../../docbrowser.cpp" line="93"/>
        <location filename="../../docbrowser.cpp" line="96"/>
        <source>MiniWebBrowser</source>
        <translation>MiniWebBrowser</translation>
    </message>
</context>
<context>
    <name>Embedder</name>
    <message>
        <location filename="../../embedder.cpp" line="92"/>
        <source>emulator #%1 released, window ID = 0x%2</source>
        <translation>emulator nr %1 uwolniony, ID okna = 0x%2</translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="103"/>
        <source>emulator #%1 embedded, window ID = 0x%2</source>
        <translation>emulator nr %1 osadzony, ID okna = 0x%2</translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="128"/>
        <source>emulator #%1 closed, window ID = 0x%2</source>
        <translation>emulator nr %1 zamknięty, ID okna = 0x%2</translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="148"/>
        <source>WARNING: embedder: unknown error, window ID = 0x%1</source>
        <translation>UWAGA: osadzacz: nieznany błąd, ID okna = 0x%1</translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="143"/>
        <source>WARNING: embedder: invalid window ID = 0x%1</source>
        <translation>UWAGA: osadzacz: niepoprawny ID okna = 0x%1</translation>
    </message>
</context>
<context>
    <name>EmbedderOptions</name>
    <message>
        <location filename="../../embedderopt.ui" line="14"/>
        <source>Embedder options</source>
        <translation>Opcje osadzacza</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="31"/>
        <source>Snapshots</source>
        <translation>Zrzuty ekranu</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="37"/>
        <source>Take a snapshot of the current window content -- hold to take snapshots repeatedly (every 100ms)</source>
        <translation>Pobierz zrzut obecnej zawartości okna -- przytrzymaj aby pobierać zrzuty wielokrotnie (co 100ms)</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="40"/>
        <source>Take snapshot</source>
        <translation>Pobierz zrzut ekranu</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="98"/>
        <source>Clear snapshots</source>
        <translation>Wyczyść zrzuty ekranu</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="101"/>
        <source>Clear</source>
        <translation>Wyczyść</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="115"/>
        <source>Scale snapshots to the native resolution</source>
        <translation>Skaluj zrzuty ekranu do rozdzielczości natywnej</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="118"/>
        <source>Native resolution</source>
        <translation>Rozdzielczość natywna</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="130"/>
        <source>Movies</source>
        <translation>Filmy</translation>
    </message>
</context>
<context>
    <name>EmulatorOptionDelegate</name>
    <message>
        <location filename="../../emuopt.cpp" line="159"/>
        <source>All files (*)</source>
        <translation>Wszystkie pliki (*)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="164"/>
        <location filename="../../emuopt.cpp" line="176"/>
        <source>Browse: </source>
        <translation>Przeglądaj:</translation>
    </message>
</context>
<context>
    <name>EmulatorOptions</name>
    <message>
        <location filename="../../emuopt.cpp" line="393"/>
        <location filename="../../emuopt.cpp" line="845"/>
        <location filename="../../emuopt.cpp" line="911"/>
        <location filename="../../emuopt.cpp" line="959"/>
        <location filename="../../emuopt.cpp" line="960"/>
        <location filename="../../emuopt.cpp" line="961"/>
        <location filename="../../emuopt.cpp" line="1053"/>
        <location filename="../../emuopt.cpp" line="1055"/>
        <location filename="../../emuopt.cpp" line="1057"/>
        <source>unknown</source>
        <translation>nieznany</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="399"/>
        <source>Game specific emulator configuration</source>
        <translation>Specyficzna dla gry konfiguracja emulatora</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="405"/>
        <source>Global emulator configuration</source>
        <translation>Globalna konfiguracja emulatora</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="413"/>
        <source>Option / Attribute</source>
        <translation>Opcja / Atrybut</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="414"/>
        <source>Value</source>
        <translation>Wartość</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="858"/>
        <source>true</source>
        <translation>prawda</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="860"/>
        <source>false</source>
        <translation>fałsz</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="806"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="809"/>
        <source>bool</source>
        <translation>logiczny</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="813"/>
        <source>int</source>
        <translation>całkowita</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="817"/>
        <source>float</source>
        <translation>zmiennoprzecinkowa</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="821"/>
        <source>float2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="825"/>
        <source>float3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="837"/>
        <source>choice</source>
        <translation>wybór</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="841"/>
        <source>string</source>
        <translation>ciąg</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="850"/>
        <source>Short name</source>
        <translation>Krótka nazwa</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="855"/>
        <source>Default</source>
        <translation>Domyślny</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="870"/>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="954"/>
        <source>creating template configuration map</source>
        <translation>tworzenie szablonowej mapy konfiguracji</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="991"/>
        <source>FATAL: XML error reading template: &apos;%1&apos; in file &apos;%2&apos; at line %3, column %4</source>
        <translation>FATALNIE: błąd XML podczas czytania szablonu: &apos;%1&apos; w pliku &apos;%2&apos; w linii %3, kolumna %4</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1044"/>
        <source>template info: emulator = %1, version = %2, format = %3</source>
        <translation>informacje o szablonie: emulator = %1, wersja = %2, format = %3</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1051"/>
        <source>FATAL: can&apos;t open options template file</source>
        <translation>FATALNIE: nie można otworzyć pliku szablonu opcji</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1054"/>
        <source>WARNING: couldn&apos;t determine emulator type of template</source>
        <translation>UWAGA: nie udało się określić typu emulatora szablonu</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1056"/>
        <source>WARNING: couldn&apos;t determine template version</source>
        <translation>UWAGA: nie udało się określić wersji szablonu</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1058"/>
        <source>WARNING: couldn&apos;t determine template format</source>
        <translation>UWAGA: nie udało się określić formatu szablonu</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1068"/>
        <source>please wait for reload to finish and try again</source>
        <translation>proszę poczekać za zakończenie przeładowywania i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1077"/>
        <source>checking template configuration map against selected emulator</source>
        <translation>sprawdzanie mapy konfiguracji szablonu z wybranym emulatorem</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1109"/>
        <source>FATAL: can&apos;t start MAME executable within a reasonable time frame, giving up</source>
        <translation>FATALNIE: nie udało się uruchomienie MAME w sensownym czasie, poddanie</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1111"/>
        <source>FATAL: can&apos;t start MESS executable within a reasonable time frame, giving up</source>
        <translation>FATALNIE: nie udało się uruchomienie MESS w sensownym czasie, poddanie</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1167"/>
        <source>FATAL: can&apos;t create temporary file, please check emulator executable and permissions</source>
        <translation>FATALNIE: nie można stworzyć pliku tymczasowego, proszę sprawdzić plik wykonywalny emulatora oraz uprawnienia</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1184"/>
        <location filename="../../emuopt.cpp" line="1193"/>
        <location filename="../../emuopt.cpp" line="1202"/>
        <location filename="../../emuopt.cpp" line="1211"/>
        <location filename="../../emuopt.cpp" line="1223"/>
        <location filename="../../emuopt.cpp" line="1240"/>
        <source>emulator uses a different default value for option &apos;%1&apos; (&apos;%2&apos; vs. &apos;%3&apos;); assumed option type is &apos;%4&apos;</source>
        <translation>emulator używa innej wartości domyślnej dla opcji &apos;%1&apos; (&apos;%2&apos; zamiast &apos;%3&apos;); przypuszczalny typ opcji to &apos;%4&apos;</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1247"/>
        <source>template option &apos;%1&apos; is unknown to the emulator</source>
        <translation>opcja szablonu &apos;%1&apos; nie jest znana emulatorowi</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1257"/>
        <source>emulator option &apos;%1&apos; with default value &apos;%2&apos; is unknown to the template</source>
        <translation>opcja emulatora &apos;%1&apos; o domyślnej wartości &apos;%2&apos; nie jest znana szablonowi</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1261"/>
        <source>done (checking template configuration map against selected emulator)</source>
        <translation>ukończono (sprawdzanie mapy konfiguracji szablonu z wybranym emulatorem)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../emuopt.cpp" line="1262"/>
        <source>check results: %n difference(s)</source>
        <translation>
            <numerusform>wyniki sprawdzenia: %n różnica</numerusform>
            <numerusform>wyniki sprawdzenia: %n różnice</numerusform>
            <numerusform>wyniki sprawdzenia: %n różnic</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1370"/>
        <source>WARNING: ini-export: no writable ini-paths found</source>
        <translation>UWAGA: eksport ini: nie znaleniono zapisywalnych ścieżek ini</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1374"/>
        <location filename="../../emuopt.cpp" line="1534"/>
        <source>Path selection</source>
        <translation>Wybór ścieżki</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1375"/>
        <source>Multiple ini-paths detected. Select path(s) to export to:</source>
        <translation>Wykryto wiele ścieżek ini. Wybierz ścieżki, do których eksportować:</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1394"/>
        <source>WARNING: ini-export: no path selected (or invalid inipath)</source>
        <translation>UWAGA: eksport ini: nie wybrano ścieżki (lub błędna ścieżka ini)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1406"/>
        <location filename="../../emuopt.cpp" line="1566"/>
        <source>Waiting for data...</source>
        <translation>Oczekiwanie na dane...</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1422"/>
        <source>FATAL: can&apos;t open export file for writing, path = %1</source>
        <translation>FATALNIE: nie można otworzyć pliku eksportu do zapisu, ścieżka = %1</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1428"/>
        <source>exporting %1 MAME configuration to %2</source>
        <translation>eksportowanie konfiguracji MAME %1 do %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1428"/>
        <location filename="../../emuopt.cpp" line="1430"/>
        <location filename="../../emuopt.cpp" line="1481"/>
        <location filename="../../emuopt.cpp" line="1483"/>
        <location filename="../../emuopt.cpp" line="1589"/>
        <location filename="../../emuopt.cpp" line="1591"/>
        <location filename="../../emuopt.cpp" line="1690"/>
        <location filename="../../emuopt.cpp" line="1692"/>
        <source>global</source>
        <translation>globalne</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1428"/>
        <location filename="../../emuopt.cpp" line="1481"/>
        <location filename="../../emuopt.cpp" line="1589"/>
        <location filename="../../emuopt.cpp" line="1690"/>
        <source>game-specific</source>
        <translation>specyficzna dla gry</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1430"/>
        <source>exporting %1 MESS configuration to %2</source>
        <translation>eksportowanie konfiguracji MESS %1 do %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1481"/>
        <source>done (exporting %1 MAME configuration to %2, elapsed time = %3)</source>
        <translation>ukończono (eksportowanie konfiguracji MAME %1 do %2, miniony czas = %3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1483"/>
        <source>done (exporting %1 MESS configuration to %2, elapsed time = %3)</source>
        <translation>ukończono (eksportowanie konfiguracji MESS %1 do %2, miniony czas = %3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1530"/>
        <source>WARNING: ini-import: no readable ini-paths found</source>
        <translation>UWAGA: import ini: nie znaleziono odczytywalnych ścieżek ini</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1535"/>
        <source>Multiple ini-paths detected. Select path(s) to import from:</source>
        <translation>Wykryto wiele ścieżek ini. Wybierz ścieżki, z których importować:</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1554"/>
        <source>WARNING: ini-import: no path selected (or invalid inipath)</source>
        <translation>UWAGA: import ini: nie wybrano ścieżki (lub błędna ścieżka ini)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1583"/>
        <source>FATAL: can&apos;t open import file for reading, path = %1</source>
        <translation>FATALNIE: nie można otworzyć pliku importu do odczytu, ścieżka = %1</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1589"/>
        <source>importing %1 MAME configuration from %2</source>
        <translation>importowanie konfiguracji MAME %1 z %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1591"/>
        <source>importing %1 MESS configuration from %2</source>
        <translation>importowanie konfiguracji MESS %1 z %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1680"/>
        <source>WARNING: unknown option &apos;%1&apos; at line %2 (%3) ignored</source>
        <translation>UWAGA: zignorowano nieznaną opcję &apos;%1&apos; w linii %2 (%3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1683"/>
        <source>WARNING: invalid syntax at line %1 (%2) ignored</source>
        <translation>UWAGA: zignorowano nieprawidłową składnię w linii %1 (%2)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1690"/>
        <source>done (importing %1 MAME configuration from %2, elapsed time = %3)</source>
        <translation>ukończono (importowanie konfiguracji MAME %1 z %2, miniony czas = %3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1692"/>
        <source>done (importing %1 MESS configuration from %2, elapsed time = %3)</source>
        <translation>ukończono (importowanie konfiguracji MESS %1 z %2, miniony czas = %3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1430"/>
        <location filename="../../emuopt.cpp" line="1483"/>
        <location filename="../../emuopt.cpp" line="1591"/>
        <location filename="../../emuopt.cpp" line="1692"/>
        <source>machine-specific</source>
        <translation>specyficzna dla maszyny</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="401"/>
        <source>Machine specific emulator configuration</source>
        <translation>Specyficzna dla maszyny konfiguracja emulatora</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="829"/>
        <source>file</source>
        <translation>plik</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="833"/>
        <source>directory</source>
        <translation>katalog</translation>
    </message>
</context>
<context>
    <name>FileEditWidget</name>
    <message>
        <location filename="../../fileeditwidget.cpp" line="49"/>
        <source>Choose file</source>
        <translation>Wybierz plik</translation>
    </message>
</context>
<context>
    <name>FileSystemModel</name>
    <message>
        <location filename="../../filesystemmodel.h" line="274"/>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="274"/>
        <source>Size</source>
        <translation>Rozmiar</translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="470"/>
        <location filename="../../filesystemmodel.h" line="484"/>
        <source> KB</source>
        <translation> KB</translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="473"/>
        <location filename="../../filesystemmodel.h" line="487"/>
        <source> MB</source>
        <translation> MB</translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="476"/>
        <location filename="../../filesystemmodel.h" line="490"/>
        <source> GB</source>
        <translation> GB</translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="479"/>
        <source> TB</source>
        <translation> TB</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Typ</translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="274"/>
        <source>Date modified</source>
        <translation>Data modyfikacji</translation>
    </message>
</context>
<context>
    <name>Flyer</name>
    <message>
        <location filename="../../flyer.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>Kopiuj do schowka</translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="56"/>
        <location filename="../../flyer.cpp" line="57"/>
        <source>Game flyer image</source>
        <translation>Obraz ulotki gry</translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="68"/>
        <location filename="../../flyer.cpp" line="72"/>
        <source>FATAL: can&apos;t open flyer file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku ulotki, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Oczekiwanie na dane...</translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="59"/>
        <location filename="../../flyer.cpp" line="60"/>
        <source>Machine flyer image</source>
        <translation>Obraz ulotki maszyny</translation>
    </message>
</context>
<context>
    <name>Gamelist</name>
    <message>
        <location filename="../../gamelist.cpp" line="129"/>
        <location filename="../../gamelist.cpp" line="152"/>
        <location filename="../../gamelist.cpp" line="510"/>
        <location filename="../../gamelist.cpp" line="514"/>
        <location filename="../../gamelist.cpp" line="515"/>
        <location filename="../../gamelist.cpp" line="525"/>
        <location filename="../../gamelist.cpp" line="529"/>
        <location filename="../../gamelist.cpp" line="530"/>
        <location filename="../../gamelist.cpp" line="535"/>
        <location filename="../../gamelist.cpp" line="536"/>
        <location filename="../../gamelist.cpp" line="602"/>
        <location filename="../../gamelist.cpp" line="605"/>
        <source>unknown</source>
        <translation>nieznany</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>good</source>
        <translation>dobry</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>bad</source>
        <translation>zły</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>preliminary</source>
        <translation>wstępny</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>supported</source>
        <translation>obsługiwana</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>unsupported</source>
        <translation>nieobsługiwana</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>imperfect</source>
        <translation>niedoskonały</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>yes</source>
        <translation>tak</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>no</source>
        <translation>nie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>baddump</source>
        <translation>zły zrzut</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>nodump</source>
        <translation>brak zrzutu</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <source>vertical</source>
        <translation>pionowa</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <source>horizontal</source>
        <translation>pozioma</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <source>raster</source>
        <translation>rastrowy</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <location filename="../../gamelist.cpp" line="1492"/>
        <location filename="../../gamelist.cpp" line="1755"/>
        <source>Unknown</source>
        <translation>Nieznany</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>On</source>
        <translation>Włączony</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>Off</source>
        <translation>Wyłączony</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>audio</source>
        <translation>dźwięk</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>Unused</source>
        <translation>Nieużywany</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>original</source>
        <translation>oryginalne</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="159"/>
        <source>compatible</source>
        <translation>kompatybilne</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="165"/>
        <location filename="../../gamelist.cpp" line="169"/>
        <source>FATAL: can&apos;t open icon file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku ikony, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="387"/>
        <location filename="../../gamelist.cpp" line="389"/>
        <location filename="../../gamelist.cpp" line="392"/>
        <location filename="../../gamelist.cpp" line="394"/>
        <location filename="../../gamelist.cpp" line="1575"/>
        <location filename="../../gamelist.cpp" line="1838"/>
        <location filename="../../gamelist.cpp" line="2115"/>
        <location filename="../../gamelist.cpp" line="2951"/>
        <source>Waiting for data...</source>
        <translation>Oczekiwanie na dane...</translation>
    </message>
    <message>
        <source>determining emulator version and supported games</source>
        <translation type="obsolete">określanie wersji emulatora i obsługiwanych gier</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="509"/>
        <source>FATAL: selected executable file is not MAME</source>
        <translation>FATALNIE: wybrany plik wykonywalny to nie MAME</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="534"/>
        <location filename="../../gamelist.cpp" line="598"/>
        <source>FATAL: can&apos;t create temporary file, please check emulator executable and permissions</source>
        <translation>FATALNIE: nie można stworzyć pliku tymczasowego, proszę sprawdzić plik wykonywalny emulatora oraz uprawnienia</translation>
    </message>
    <message>
        <source>done (determining emulator version and supported games, elapsed time = %1)</source>
        <translation type="obsolete">ukończono (określanie wersji emulatora i obsługiwanych gier, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="609"/>
        <source>FATAL: couldn&apos;t determine emulator version, type identification string is &apos;%1&apos; -- please inform developers if you&apos;re sure that this is a valid MAME binary</source>
        <translation>FATALNIE: nie udało się ustalić wersji emulatora, łańcuch identyfikacji typu to &apos;%1&apos; -- proszę poinformować programistów w przypadku pewności, że jest to prawidłowy plik binarny MAME</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="611"/>
        <source>FATAL: couldn&apos;t determine emulator version, type identification string is &apos;%1&apos; -- please inform developers if you&apos;re sure that this is a valid MESS binary</source>
        <translation>FATALNIE: nie udało się ustalić wersji emulatora, łańcuch identyfikacji typu to &apos;%1&apos; -- proszę poinformować programistów w przypadku pewności, że jest to prawidłowy plik binarny MESS</translation>
    </message>
    <message>
        <source>FATAL: couldn&apos;t determine supported games</source>
        <translation type="obsolete">FATALNIE: nie udało się ustalić obsługiwanych gier</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="668"/>
        <source>XML cache - %p%</source>
        <translation>bufor XML - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="781"/>
        <source>XML data - %p%</source>
        <translation>dane XML - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="845"/>
        <source>verifying ROM status for &apos;%1&apos;</source>
        <translation>sprawdzanie stanu ROM-ów dla &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="887"/>
        <source>verifying ROM status for all games</source>
        <translation>sprawdzanie stanu ROM-ów dla wszystkich gier</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="989"/>
        <source>retrieving game information for &apos;%1&apos;</source>
        <translation>uzyskiwanie informacji o grze dla &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1006"/>
        <source>WARNING: couldn&apos;t find game information for &apos;%1&apos;</source>
        <translation>UWAGA: nie udało się znaleźć informacji o grze &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1008"/>
        <source>WARNING: couldn&apos;t find machine information for &apos;%1&apos;</source>
        <translation>UWAGA: nie udało się znaleźć informacji o maszynie &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Source file</source>
        <translation>Plik źródłowy</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Clone of</source>
        <translation>Klon</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>ROM of</source>
        <translation>ROM</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Sample of</source>
        <translation>Sampel</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Is device?</source>
        <translation>Jest urządzeniem?</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1036"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1043"/>
        <source>Manufacturer</source>
        <translation>Producent</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1048"/>
        <location filename="../../gamelist.cpp" line="1475"/>
        <location filename="../../gamelist.cpp" line="1739"/>
        <source>ROM</source>
        <translation>ROM</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>BIOS</source>
        <translation>BIOS</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>Size</source>
        <translation>Rozmiar</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>CRC</source>
        <translation>CRC</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>SHA1</source>
        <translation>SHA1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Merge</source>
        <translation>Scalanie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Region</source>
        <translation>Region</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>Offset</source>
        <translation>Przesunięcie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1161"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <location filename="../../gamelist.cpp" line="1206"/>
        <source>Status</source>
        <translation>Stan</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1058"/>
        <source>Device reference</source>
        <translation>Referencja urządzenia</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1063"/>
        <source>Chip</source>
        <translation>Chip</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1068"/>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1068"/>
        <source>Clock</source>
        <translation>Taktowanie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Width</source>
        <translation>Szerokość</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Height</source>
        <translation>Wysokość</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Refresh</source>
        <translation>Odświeżanie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1082"/>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Sound</source>
        <translation>Dźwięk</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1086"/>
        <source>Channels</source>
        <translation>Kanały</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1091"/>
        <source>Input</source>
        <translation>Wejście</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Service</source>
        <translation>Serwis</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Tilt</source>
        <translation>Nachylenie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Players</source>
        <translation>Gracze</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Buttons</source>
        <translation>Przyciski</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Coins</source>
        <translation>Monety</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1114"/>
        <source>DIP switch</source>
        <translation>przełącznik DIP</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1121"/>
        <source>DIP value</source>
        <translation>wartość DIP</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1126"/>
        <location filename="../../gamelist.cpp" line="1150"/>
        <location filename="../../gamelist.cpp" line="1171"/>
        <location filename="../../gamelist.cpp" line="1196"/>
        <location filename="../../gamelist.cpp" line="1223"/>
        <location filename="../../gamelist.cpp" line="1273"/>
        <source>Default</source>
        <translation>Domyślny</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1133"/>
        <source>Configuration</source>
        <translation>Konfiguracja</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1138"/>
        <source>Mask</source>
        <translation>Maska</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1145"/>
        <source>Setting</source>
        <translation>Ustawienie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1150"/>
        <source>Value</source>
        <translation>Wartość</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1157"/>
        <source>Driver</source>
        <translation>Sterownik</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Emulation</source>
        <translation>Emulacja</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Color</source>
        <translation>Kolor</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Graphic</source>
        <translation>Grafika</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Cocktail</source>
        <translation>Koktajl</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Protection</source>
        <translation>Ochrona</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Save state</source>
        <translation>Zapis stanu</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Palette size</source>
        <translation>Rozmiar palety</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1166"/>
        <source>BIOS set</source>
        <translation>Zestaw BIOS</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1171"/>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1176"/>
        <source>Sample</source>
        <translation>Sampel</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1181"/>
        <source>Disk</source>
        <translation>Dysk</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>MD5</source>
        <translation>MD5</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Index</source>
        <translation>Indeks</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1191"/>
        <source>Adjuster</source>
        <translation>Regulator</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1201"/>
        <source>Software list</source>
        <translation>Lista oprogramowania</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1211"/>
        <source>Category</source>
        <translation>Kategoria</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1218"/>
        <source>Item</source>
        <translation>Element</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1235"/>
        <source>Interface</source>
        <translation>Interfejs</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1310"/>
        <source>WARNING: can&apos;t open ROM state cache, please check ROMs</source>
        <translation>UWAGA: nie można otworzyć bufora stanów ROM-ów, proszę sprawdzić ROM-y</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1313"/>
        <source>loading ROM state from cache</source>
        <translation>wczytywanie stanów ROM-ów z bufora</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1315"/>
        <source>ROM states - %p%</source>
        <translation>Stany romów - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1337"/>
        <source>done (loading ROM state from cache, elapsed time = %1)</source>
        <translation>ukończono (wczytywanie stanów ROM-ów z bufora, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1481"/>
        <location filename="../../gamelist.cpp" line="1482"/>
        <location filename="../../gamelist.cpp" line="1745"/>
        <location filename="../../gamelist.cpp" line="1746"/>
        <source>N/A</source>
        <translation>niedostępne</translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2166"/>
        <source>%n game(s)</source>
        <translation>
            <numerusform>wczytano %n grę</numerusform>
            <numerusform>wczytano %n gry</numerusform>
            <numerusform>wczytano %n gier</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2166"/>
        <location filename="../../gamelist.cpp" line="2169"/>
        <source> and %n device(s) loaded</source>
        <translation>
            <numerusform>oraz %n urządzenie</numerusform>
            <numerusform>oraz %n urządzenia</numerusform>
            <numerusform>oraz %n urządzeń</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2166"/>
        <location filename="../../gamelist.cpp" line="2169"/>
        <source>, %n BIOS set(s)</source>
        <translation>
            <numerusform>, %n zestaw BIOS</numerusform>
            <numerusform>, %n zestawy BIOS</numerusform>
            <numerusform>, %n zestawów BIOS</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2169"/>
        <source>%n machine(s)</source>
        <translation>
            <numerusform>wczytano %n maszynę</numerusform>
            <numerusform>wczytano %n maszyny</numerusform>
            <numerusform>wczytano %n maszyn</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2193"/>
        <location filename="../../gamelist.cpp" line="2923"/>
        <source>ROM state info: L:%1 C:%2 M:%3 I:%4 N:%5 U:%6</source>
        <translation>Informacje o stanie ROM-ów: W: %1 P:%2 Wp:%3 Np:%4 Nz:%5 Nn:%6</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2205"/>
        <source>WARNING: ROM state cache is incomplete or not up to date, triggering an automatic ROM check</source>
        <translation>UWAGA: bufor stanów ROM-ów jest niekompletny lub nieaktualny, uruchamianie automatycznego sprawdzenia ROM-ów</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2208"/>
        <source>WARNING: ROM state cache is incomplete or not up to date, please re-check ROMs</source>
        <translation>UWAGA: bufor stanów ROM-ów jest niekompletny lub nieaktualny, proszę ponownie sprawdzić ROM-y</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1391"/>
        <location filename="../../gamelist.cpp" line="1400"/>
        <source>INFORMATION: the game list cache will now be updated due to a new format</source>
        <translation>INFORMACJA: bufor listy gier zostanie teraz zaktualizowany ze względu na nowy format</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="421"/>
        <source>determining emulator version and supported sets</source>
        <translation>określanie wersji emulatora i obsługiwanych zestawów</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="596"/>
        <source>done (determining emulator version and supported sets, elapsed time = %1)</source>
        <translation>ukończono (określanie wersji emulatora i obsługiwanych zestawów, miniony czas = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="621"/>
        <source>%n supported set(s)</source>
        <translation>
            <numerusform>%n obsługiwany zestaw</numerusform>
            <numerusform>%n obsługiwane zestawy</numerusform>
            <numerusform>%n obsługiwanych zestawów</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="623"/>
        <source>FATAL: couldn&apos;t determine the number of supported sets</source>
        <translation>FATALNIE: nie udało się ustalić liczby obsługiwanych zestawów</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1393"/>
        <location filename="../../gamelist.cpp" line="1402"/>
        <source>INFORMATION: the machine list cache will now be updated due to a new format</source>
        <translation>INFORMACJA: bufor listy maszyn zostanie teraz zaktualizowany ze względu na nowy format</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1417"/>
        <location filename="../../gamelist.cpp" line="1632"/>
        <source>Game data - %p%</source>
        <translation>Dane gier - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1473"/>
        <location filename="../../gamelist.cpp" line="1737"/>
        <source>ROM, CHD</source>
        <translation>ROM, CHD</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1477"/>
        <location filename="../../gamelist.cpp" line="1741"/>
        <source>CHD</source>
        <translation>CHD</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1493"/>
        <location filename="../../gamelist.cpp" line="1756"/>
        <location filename="../../gamelist.cpp" line="3549"/>
        <location filename="../../gamelist.cpp" line="3652"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2101"/>
        <location filename="../../gamelist.cpp" line="2103"/>
        <location filename="../../gamelist.cpp" line="2937"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>ascending</source>
        <translation>rosnącym</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2101"/>
        <location filename="../../gamelist.cpp" line="2103"/>
        <location filename="../../gamelist.cpp" line="2937"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>descending</source>
        <translation>malejącym</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2135"/>
        <location filename="../../gamelist.cpp" line="2151"/>
        <source>restoring game selection</source>
        <translation>przywracanie wyboru gry</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2228"/>
        <source>ROM state filter already active</source>
        <translation>filtr stanów ROM-ów jest już aktywny</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2233"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation>proszę poczekać za zakończenie weryfikacji ROM-ów i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2238"/>
        <source>please wait for reload to finish and try again</source>
        <translation>proszę poczekać za zakończenie przeładowywania i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2247"/>
        <source>applying ROM state filter</source>
        <translation>zastosowywanie filtra stanów ROM-ów</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2250"/>
        <source>State filter - %p%</source>
        <translation>Filtr stanów - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2304"/>
        <source>done (applying ROM state filter, elapsed time = %1)</source>
        <translation>ukończono (zastosowywanie filtra stanów ROM-ów, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2331"/>
        <source>loading favorites</source>
        <translation>wczytywanie ulubionych</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2355"/>
        <source>done (loading favorites)</source>
        <translation>ukończono (wczytywanie ulubionych)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2368"/>
        <source>saving favorites</source>
        <translation>zapisywanie ulubionych</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2384"/>
        <location filename="../../gamelist.cpp" line="2386"/>
        <source>FATAL: can&apos;t open favorites file for writing, path = %1</source>
        <translation>FATALNIE: nie można otworzyć pliku ulubionych do zapisu, ścieżka = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2390"/>
        <source>done (saving favorites)</source>
        <translation>ukończono (zapisywanie ulubionych)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2399"/>
        <source>loading play history</source>
        <translation>wczytywanie historii gier</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2422"/>
        <source>done (loading play history)</source>
        <translation>ukończono (wczytywanie historii gier)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2435"/>
        <source>saving play history</source>
        <translation>zapisywanie historii gier</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2451"/>
        <location filename="../../gamelist.cpp" line="2453"/>
        <source>FATAL: can&apos;t open play history file for writing, path = %1</source>
        <translation>FATALNIE: nie można otworzyć pliku historii do zapisu, ścieżka = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2457"/>
        <source>done (saving play history)</source>
        <translation>ukończono (zapisywanie historii gier)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2468"/>
        <source>L:</source>
        <translation>W:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2469"/>
        <source>C:</source>
        <translation>P:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2470"/>
        <source>M:</source>
        <translation>Wp:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2471"/>
        <source>I:</source>
        <translation>Np:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2472"/>
        <source>N:</source>
        <translation>Nz:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2473"/>
        <source>U:</source>
        <translation>Nn:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2474"/>
        <source>S:</source>
        <translation>S:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="879"/>
        <location filename="../../gamelist.cpp" line="901"/>
        <source>ERROR: can&apos;t open ROM state cache for writing, path = %1</source>
        <translation>BŁĄD: nie można otworzyć bufora stanów ROM-ów do zapisu, ścieżka = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="908"/>
        <source>ROM check - %p%</source>
        <translation>Sprawdzanie ROM-ów - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2475"/>
        <source>T:</source>
        <translation>Z:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2915"/>
        <source>done (verifying ROM status for &apos;%1&apos;, elapsed time = %2)</source>
        <translation>ukończono (weryfikowanie stanu ROM-ów dla &apos;%1&apos;, miniony czas = %2)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2918"/>
        <source>done (verifying ROM status for all games, elapsed time = %1)</source>
        <translation>ukończono (weryfikowanie stanu ROM-ów dla wszystkich gier, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2937"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>ROM state</source>
        <translation>Stan ROM-u</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3203"/>
        <source>ROM status for &apos;%1&apos; is &apos;%2&apos;</source>
        <translation>Stan ROM-u dla &apos;%1&apos; to &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3293"/>
        <source>pre-caching icons from ZIP archive</source>
        <translation>pre-buforowanie ikon z archiwum ZIP</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3349"/>
        <source>done (pre-caching icons from ZIP archive, elapsed time = %1)</source>
        <translation>ukończono (pre-buforowanie ikon z archiwum ZIP, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3363"/>
        <source>pre-caching icons from directory</source>
        <translation>pre-buforowanie ikon z katalogu</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3416"/>
        <source>done (pre-caching icons from directory, elapsed time = %1)</source>
        <translation>ukończono (pre-buforowanie ikon z katalogu, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3445"/>
        <source>loading catver.ini</source>
        <translation>wczytywanie catver.ini</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3451"/>
        <source>Catver.ini - %p%</source>
        <translation>Catver.ini - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3489"/>
        <source>ERROR: can&apos;t open &apos;%1&apos; for reading -- no catver.ini data available</source>
        <translation>UWAGA: nie można otworzyć &apos;%1&apos; do odczytu -- dane catver.ini niedostępne</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3498"/>
        <source>done (loading catver.ini, elapsed time = %1)</source>
        <translation>ukończono (wczytywanie catver.ini, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3499"/>
        <source>%1 category / %2 version records loaded</source>
        <translation>wczytano %1 rekordów kategorii / %2 wersji</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3532"/>
        <source>Category view - %p%</source>
        <translation>Widok kategorii - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3635"/>
        <source>Version view - %p%</source>
        <translation>Widok wersji - %p%</translation>
    </message>
    <message numerus="yes">
        <source>%n supported game(s)</source>
        <translation type="obsolete">
            <numerusform>%n obsługiwana gra</numerusform>
            <numerusform>%n obsługiwane gry</numerusform>
            <numerusform>%n obsługiwanych gier</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="1338"/>
        <source>%n cached ROM state(s) loaded</source>
        <translation>
            <numerusform>%n buforowany stan ROM-ów załadowany</numerusform>
            <numerusform>%n buforowane stany ROM-ów załadowane</numerusform>
            <numerusform>%n buforowanych stanów ROM-ów załadowanych</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n game(s) loaded</source>
        <translation type="obsolete">
            <numerusform>%n gra załadowana</numerusform>
            <numerusform>%n gry załadowane</numerusform>
            <numerusform>%n gier załadowanych</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="3350"/>
        <location filename="../../gamelist.cpp" line="3417"/>
        <source>%n icon(s) loaded</source>
        <translation>
            <numerusform>%n ikona załadowana</numerusform>
            <numerusform>%n ikony załadowane</numerusform>
            <numerusform>%n ikon załadowanych</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3299"/>
        <location filename="../../gamelist.cpp" line="3378"/>
        <source>Icon cache - %p%</source>
        <translation>Bufor ikon - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>unused</source>
        <translation>nieużywany</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>cpu</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>vector</source>
        <translation>Wektorowy</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>lcd</source>
        <translation>LCD</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="524"/>
        <source>FATAL: selected executable file is not MESS</source>
        <translation>FATALNIE: wybrany plik wykonywalny to nie MESS</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Is BIOS?</source>
        <translation>Jest BIOS-em?</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Runnable</source>
        <translation>Uruchamialny</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1073"/>
        <source>Display</source>
        <translation>Ekran</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Rotate</source>
        <translation>Obrót</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Flip-X</source>
        <translation>Odbicie w poziomie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Pixel clock</source>
        <translation>Zegar pikseli</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>H-Total</source>
        <translation>Poz. całkowite</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>H-Bend</source>
        <translation>Odchylanie poz. </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>HB-Start</source>
        <translation>Początek odch. poz.</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>V-Total</source>
        <translation>Pion. całkowite</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>V-Bend</source>
        <translation>Odchylanie pion.</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>VB-Start</source>
        <translation>Początek odch. pion.</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1068"/>
        <location filename="../../gamelist.cpp" line="1138"/>
        <location filename="../../gamelist.cpp" line="1235"/>
        <source>Tag</source>
        <translation>Zaznaczenie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1102"/>
        <source>Control</source>
        <translation>Kontrola</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Sensitivity</source>
        <translation>Czułość</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Key Delta</source>
        <translation>Delta przycisków</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Reverse</source>
        <translation>Odwrócenie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>joy4way</source>
        <translation>Dżojstik czterokierunkowy</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>joy8way</source>
        <translation>Dżojstik ośmiokierunkowy</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>trackball</source>
        <translation>Trackball</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>joy2way</source>
        <translation>Dżojstik dwukierunkowy</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>doublejoy8way</source>
        <translation>Podwójny dżojstik ośmiokierunkowy</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>dial</source>
        <translation>Tarcza</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>paddle</source>
        <translation>Wiosło</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>pedal</source>
        <translation>Pedał</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>stick</source>
        <translation>Drążek</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>vjoy2way</source>
        <translation>Pionowy dżojstik dwukierunkowy</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>lightgun</source>
        <translation>Pistolet świetlny</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>doublejoy4way</source>
        <translation>Podwójny dżojstik czterokierunkowy</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>vdoublejoy2way</source>
        <translation>Pionowy podwójny dżojstik dwukierunkowy</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>doublejoy2way</source>
        <translation>Podwójny dżojstik dwukierunkowy</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>printer</source>
        <translation>Drukarka</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>cdrom</source>
        <translation>CD-ROM</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>cartridge</source>
        <translation>Kartridż</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>cassette</source>
        <translation>Kaseta</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>quickload</source>
        <translation>Szybki zapis</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>floppydisk</source>
        <translation>Dyskietka</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>serial</source>
        <translation>Port szeregowy</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>snapshot</source>
        <translation>Zrzut</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1230"/>
        <source>Device</source>
        <translation>Urządzenie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1242"/>
        <source>Instance</source>
        <translation>Instancja</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1247"/>
        <source>Brief name</source>
        <translation>Skrócona nazwa</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1254"/>
        <source>Extension</source>
        <translation>Rozszerzenie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1235"/>
        <source>Mandatory</source>
        <translation>Obowiązkowy</translation>
    </message>
    <message>
        <source>determining emulator version and supported machines</source>
        <translation type="obsolete">określanie wersji emulatora i obsługiwanych maszyn</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="471"/>
        <location filename="../../gamelist.cpp" line="567"/>
        <source>FATAL: can&apos;t start MAME executable within a reasonable time frame, giving up</source>
        <translation>FATALNIE: nie udało się uruchomienie MAME w sensownym czasie, poddanie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="473"/>
        <location filename="../../gamelist.cpp" line="569"/>
        <source>FATAL: can&apos;t start MESS executable within a reasonable time frame, giving up</source>
        <translation>FATALNIE: nie udało się uruchomienie MESS w sensownym czasie, poddanie</translation>
    </message>
    <message>
        <source>done (determining emulator version and supported machines, elapsed time = %1)</source>
        <translation type="obsolete">ukończono (określanie wersji emulatora i obsługiwanych maszyn, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="603"/>
        <source>emulator info: type = %1, version = %2</source>
        <translation>informacje o emulatorze: typ = %1, wersja = %2</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="606"/>
        <source>FATAL: couldn&apos;t determine emulator type and version</source>
        <translation>FATALNIE: nie udało się ustalić typu i wersji emulatora</translation>
    </message>
    <message numerus="yes">
        <source>%n supported machine(s)</source>
        <translation type="obsolete">
            <numerusform>%n obsługiwana maszyna</numerusform>
            <numerusform>%n obsługiwane maszyny</numerusform>
            <numerusform>%n obsługiwanych maszyn</numerusform>
        </translation>
    </message>
    <message>
        <source>FATAL: couldn&apos;t determine supported machines</source>
        <translation type="obsolete">FATALNIE: nie udało ustalić się obsługiwanych maszyn</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="663"/>
        <source>loading XML game list data from cache</source>
        <translation>wczytywanie listy gier XML z bufora</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="665"/>
        <source>loading XML machine list data from cache</source>
        <translation>wczytywanielisty maszyn XML z bufora</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="706"/>
        <source>done (loading XML game list data from cache, elapsed time = %1)</source>
        <translation>ukończono (wczytywanie listy gier XML z bufora, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="708"/>
        <source>WARNING: XML game list cache is incomplete, invalidating XML game list cache</source>
        <translation>UWAGA: bufor listy gier XML jest niekompletny, unieważnianie bufora listy gier XML</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="710"/>
        <source>done (loading XML machine list data from cache, elapsed time = %1)</source>
        <translation>ukończono (wczytywanie listy maszyn XML z bufora, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="712"/>
        <source>WARNING: XML machine list cache is incomplete, invalidating XML machine list cache</source>
        <translation>UWAGA: bufor listy maszyn XML jest niekompletny, unieważnianie bufora listy maszyn XML</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="776"/>
        <source>loading XML game list data and (re)creating cache</source>
        <translation>wczytywanie danych listy gier XML i tworzenie (odtwarzanie) bufora</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="778"/>
        <source>loading XML machine list data and (re)creating cache</source>
        <translation>wczytywanie danych listy maszyn XML i tworzenie (odtwarzanie) bufora</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="798"/>
        <source>WARNING: can&apos;t open XML game list cache for writing, please check permissions</source>
        <translation>UWAGA: nie można otworzyć bufora listy gier XML do zapisu, proszę sprawdzić uprawnienia</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="800"/>
        <source>WARNING: can&apos;t open XML machine list cache for writing, please check permissions</source>
        <translation>UWAGA: nie można otworzyć bufora maszyn gier XML do zapisu, proszę sprawdzić uprawnienia</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="889"/>
        <source>verifying ROM status for all machines</source>
        <translation>sprawdzanie stanu ROM-ów dla wszystkich maszyn</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="991"/>
        <source>retrieving machine information for &apos;%1&apos;</source>
        <translation>uzyskiwanie informacji o maszynie dla &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Optional</source>
        <translation>opcjonalne</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1347"/>
        <source>processing game list</source>
        <translation>przetwarzanie listy gier</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1349"/>
        <source>processing machine list</source>
        <translation>przetwarzanie listy maszyn</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1384"/>
        <source>WARNING: couldn&apos;t determine emulator version of game list cache</source>
        <translation>UWAGA: nie udało się określić wersji emulatora dla bufora listy gier</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1386"/>
        <source>WARNING: couldn&apos;t determine emulator version of machine list cache</source>
        <translation>UWAGA: nie udało się określić wersji emulatora dla bufora listy maszyn</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1415"/>
        <source>loading game data from game list cache</source>
        <translation>wczytywanie danych gier z bufora listy gier</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1609"/>
        <source>done (loading game data from game list cache, elapsed time = %1)</source>
        <translation>ukończono (wczytywanie danych gier z bufora listy gier, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1630"/>
        <source>parsing game data and (re)creating game list cache</source>
        <translation>parsowanie danych listy gier XML i tworzenie (odtwarzanie) bufora</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1634"/>
        <source>parsing machine data and (re)creating machine list cache</source>
        <translation>parsowanie danych listy maszyn XML i tworzenie (odtwarzanie) bufora</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1643"/>
        <source>ERROR: can&apos;t open game list cache for writing, path = %1</source>
        <translation>BŁĄD: nie można otworzyć bufora listy gier do zapisu, ścieżka = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1645"/>
        <source>ERROR: can&apos;t open machine list cache for writing, path = %1</source>
        <translation>BŁĄD: nie można otworzyć bufora listy maszyn do zapisu, ścieżka = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2101"/>
        <location filename="../../gamelist.cpp" line="2937"/>
        <source>sorting game list by %1 in %2 order</source>
        <translation>sortowanie listy gier według %1 w porządku %2</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2103"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>sorting machine list by %1 in %2 order</source>
        <translation>sortowanie listy maszyn według %1 w porządku %2</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2137"/>
        <location filename="../../gamelist.cpp" line="2153"/>
        <source>restoring machine selection</source>
        <translation>przywracanie wyboru maszyny</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2165"/>
        <source>done (processing game list, elapsed time = %1)</source>
        <translation>ukończono (przetwarzanie listy gier, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2168"/>
        <source>done (processing machine list, elapsed time = %1)</source>
        <translation>ukończono (przetwarzanie listy maszyn, miniony czas = %1)</translation>
    </message>
    <message numerus="yes">
        <source>%n machine(s) loaded</source>
        <translation type="obsolete">
            <numerusform>%n maszyna załadowana</numerusform>
            <numerusform>%n maszyny załadowane</numerusform>
            <numerusform>%n maszyn załadowanych</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2175"/>
        <source>WARNING: game list not fully parsed, invalidating game list cache</source>
        <translation>UWAGA: lista gier nie jest w pełni sparsowana, unieważnianie bufora listy gier</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2178"/>
        <source>WARNING: machine list not fully parsed, invalidating machine list cache</source>
        <translation>UWAGA: lista maszyn nie jest w pełni sparsowana, unieważnianie bufora listy maszyn</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2184"/>
        <source>WARNING: game list cache is out of date, invalidating game list cache</source>
        <translation>UWAGA: bufor listy gier jest nieaktualny, unieważnianie bufora listy gier</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2187"/>
        <source>WARNING: machine list cache is out of date, invalidating machine list cache</source>
        <translation>UWAGA: bufor listy maszyn jest nieaktualny, unieważnianie bufora listy maszyn</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2317"/>
        <source>saving game list</source>
        <translation>zapisywanie listy gier</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2318"/>
        <source>done (saving game list)</source>
        <translation>ukończono (zapisywanie listy gier)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2320"/>
        <source>saving machine list</source>
        <translation>zapisywanie listy maszyn</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2321"/>
        <source>done (saving machine list)</source>
        <translation>ukończono (zapisywanie listy maszyn)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2504"/>
        <source>done (loading XML game list data and (re)creating cache, elapsed time = %1)</source>
        <translation>ukończono (wczytywanie listy gier XML i tworzenie (odtwarzanie) bufora, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2506"/>
        <source>done (loading XML machine list data and (re)creating cache, elapsed time = %1)</source>
        <translation>ukończono (wczytywanie listy maszyn XML i tworzenie (odtwarzanie) bufora, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2920"/>
        <source>done (verifying ROM status for all machines, elapsed time = %1)</source>
        <translation>ukończono (weryfikowanie stanu ROM-ów dla wszystkich maszyn, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1419"/>
        <source>loading machine data from machine list cache</source>
        <translation>wczytywanie danych maszyn z bufora listy maszyn</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1611"/>
        <source>done (loading machine data from machine list cache, elapsed time = %1)</source>
        <translation>ukończono (wczytywanie danych maszyn z bufora listy maszyn, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1421"/>
        <location filename="../../gamelist.cpp" line="1636"/>
        <source>Machine data - %p%</source>
        <translation>Dane maszyn - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1261"/>
        <source>RAM options</source>
        <translation>Opcje RAM-u</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1265"/>
        <source>Option</source>
        <translation>Opcja</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2738"/>
        <location filename="../../gamelist.cpp" line="2874"/>
        <location filename="../../gamelist.cpp" line="3188"/>
        <source>WARNING: can&apos;t find item map entry for &apos;%1&apos; - ROM state cannot be determined</source>
        <translation>UWAGA: nie można znaleźć wpisu o mapie elementów dla &apos;%1&apos; - stan ROM-u nieustalony</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2499"/>
        <location filename="../../gamelist.cpp" line="2682"/>
        <source>WARNING: emulator audit call didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation>UWAGA: wywołanie audytu emulatora nie zakończyło się czysto -- exitCode = %1, exitStatus = %2</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2499"/>
        <location filename="../../gamelist.cpp" line="2682"/>
        <source>normal</source>
        <translation>normalny</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2499"/>
        <location filename="../../gamelist.cpp" line="2682"/>
        <source>crashed</source>
        <translation>przestał działać</translation>
    </message>
    <message>
        <source>ROM state info: C:%1 M:%2 I:%3 N:%4 U:%5</source>
        <translation type="obsolete">Informacje o stanie ROM-ów: P:%1 Wp:%2 Np:%3 Nz:%4 Nn:%5</translation>
    </message>
</context>
<context>
    <name>ImageChecker</name>
    <message>
        <location filename="../../imgcheck.ui" line="15"/>
        <source>Check images</source>
        <translation>Sprawdź obrazy</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="36"/>
        <source>Close image check dialog</source>
        <translation>Zamknij okno dialogowe sprawdzania obrazów</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="39"/>
        <source>C&amp;lose</source>
        <translation>&amp;Zamknij</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="56"/>
        <source>&amp;Previews</source>
        <translation>&amp;Podglądy</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="70"/>
        <location filename="../../imgcheck.ui" line="243"/>
        <location filename="../../imgcheck.ui" line="323"/>
        <location filename="../../imgcheck.cpp" line="137"/>
        <location filename="../../imgcheck.cpp" line="461"/>
        <location filename="../../imgcheck.cpp" line="787"/>
        <source>Found: 0</source>
        <translation>Znaleziono: 0</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="83"/>
        <location filename="../../imgcheck.ui" line="230"/>
        <location filename="../../imgcheck.ui" line="336"/>
        <location filename="../../imgcheck.cpp" line="139"/>
        <location filename="../../imgcheck.cpp" line="463"/>
        <location filename="../../imgcheck.cpp" line="789"/>
        <source>Missing: 0</source>
        <translation>Brakujących: 0</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="96"/>
        <location filename="../../imgcheck.ui" line="217"/>
        <location filename="../../imgcheck.ui" line="349"/>
        <location filename="../../imgcheck.cpp" line="141"/>
        <location filename="../../imgcheck.cpp" line="465"/>
        <location filename="../../imgcheck.cpp" line="791"/>
        <source>Obsolete: 0</source>
        <translation>Przestarzałych: 0</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="112"/>
        <source>Check preview images / stop check</source>
        <translation>Sprawdź obrazy podglądu / zatrzymaj sprawdzanie</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="115"/>
        <location filename="../../imgcheck.cpp" line="287"/>
        <location filename="../../imgcheck.cpp" line="611"/>
        <location filename="../../imgcheck.cpp" line="938"/>
        <source>&amp;Check previews</source>
        <translation>&amp;Sprawdź podglądy</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="164"/>
        <source>Remove obsolete preview images</source>
        <translation>Usuń przestarzałe obrazy podglądów</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="167"/>
        <location filename="../../imgcheck.ui" line="266"/>
        <location filename="../../imgcheck.ui" line="381"/>
        <source>&amp;Remove obsolete</source>
        <translation>&amp;Usuń przestarzałe</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="125"/>
        <location filename="../../imgcheck.ui" line="279"/>
        <location filename="../../imgcheck.ui" line="391"/>
        <source>Select &amp;game</source>
        <translation>Wybierz &amp;grę</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="181"/>
        <source>&amp;Flyers</source>
        <translation>&amp;Ulotki</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="263"/>
        <source>Remove obsolete flyer images</source>
        <translation>Usuń przestarzałe obrazy ulotek</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="250"/>
        <source>Check flyer images / stop check</source>
        <translation>Sprawdź obrazy ulotek / zatrzymaj sprawdzanie</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="253"/>
        <location filename="../../imgcheck.cpp" line="288"/>
        <location filename="../../imgcheck.cpp" line="612"/>
        <location filename="../../imgcheck.cpp" line="939"/>
        <source>&amp;Check flyers</source>
        <translation>&amp;Sprawdź ulotki</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="309"/>
        <source>&amp;Icons</source>
        <translation>&amp;Ikony</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="401"/>
        <source>Clear cache before checking icons?</source>
        <translation>Wyczyścić bufor przed sprawdzeniem ikon?</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="404"/>
        <source>C&amp;lear cache</source>
        <translation>&amp;Wyczyść bufor</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="378"/>
        <source>Remove obsolete icon images</source>
        <translation>Usuń przestarzałe obrazy ikon</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="365"/>
        <source>Check icon images / stop check</source>
        <translation>Sprawdź obrazy ikon / zatrzymaj sprawdzanie</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="368"/>
        <location filename="../../imgcheck.cpp" line="289"/>
        <location filename="../../imgcheck.cpp" line="613"/>
        <location filename="../../imgcheck.cpp" line="940"/>
        <source>&amp;Check icons</source>
        <translation>&amp;Sprawdź ikony</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="104"/>
        <source>checking previews from ZIP archive</source>
        <translation>sprawdzanie podglądów z archiwum ZIP</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="106"/>
        <source>checking previews from directory</source>
        <translation>sprawdzanie podglądów z katalogu</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="109"/>
        <source>Preview check - %p%</source>
        <translation>Sprawdzanie podglądów - %p%</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="123"/>
        <location filename="../../imgcheck.cpp" line="124"/>
        <location filename="../../imgcheck.cpp" line="125"/>
        <location filename="../../imgcheck.cpp" line="447"/>
        <location filename="../../imgcheck.cpp" line="448"/>
        <location filename="../../imgcheck.cpp" line="449"/>
        <location filename="../../imgcheck.cpp" line="773"/>
        <location filename="../../imgcheck.cpp" line="774"/>
        <location filename="../../imgcheck.cpp" line="775"/>
        <source>&amp;Stop check</source>
        <translation>&amp;Zatrzymaj sprawdzanie</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="146"/>
        <source>check pass 1: found and missing previews</source>
        <translation>1. przebieg sprawdzający: znalezione i brakujące podglądy</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="154"/>
        <location filename="../../imgcheck.cpp" line="176"/>
        <location filename="../../imgcheck.cpp" line="478"/>
        <location filename="../../imgcheck.cpp" line="500"/>
        <location filename="../../imgcheck.cpp" line="804"/>
        <location filename="../../imgcheck.cpp" line="826"/>
        <source>Found: %1</source>
        <translation>Znalezionych: %1</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="156"/>
        <location filename="../../imgcheck.cpp" line="179"/>
        <location filename="../../imgcheck.cpp" line="480"/>
        <location filename="../../imgcheck.cpp" line="503"/>
        <location filename="../../imgcheck.cpp" line="806"/>
        <location filename="../../imgcheck.cpp" line="829"/>
        <source>Missing: %1</source>
        <translation>Brakujących: %1</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="187"/>
        <location filename="../../imgcheck.cpp" line="511"/>
        <location filename="../../imgcheck.cpp" line="837"/>
        <source>check pass 2: obsolete files: reading ZIP directory</source>
        <translation>2. przebieg sprawdzający: czytanie katalogu ZIP</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="219"/>
        <location filename="../../imgcheck.cpp" line="232"/>
        <location filename="../../imgcheck.cpp" line="252"/>
        <location filename="../../imgcheck.cpp" line="261"/>
        <location filename="../../imgcheck.cpp" line="543"/>
        <location filename="../../imgcheck.cpp" line="556"/>
        <location filename="../../imgcheck.cpp" line="576"/>
        <location filename="../../imgcheck.cpp" line="585"/>
        <location filename="../../imgcheck.cpp" line="870"/>
        <location filename="../../imgcheck.cpp" line="883"/>
        <location filename="../../imgcheck.cpp" line="903"/>
        <location filename="../../imgcheck.cpp" line="912"/>
        <source>Obsolete: %1</source>
        <translation>Przestarzałych: %1</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="236"/>
        <location filename="../../imgcheck.cpp" line="560"/>
        <location filename="../../imgcheck.cpp" line="887"/>
        <source>check pass 2: obsolete files: reading directory structure</source>
        <translation>2. przebieg sprawdzający: przestarzałe pliki: czytanie struktury katalogu</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="278"/>
        <source>done (checking previews from ZIP archive, elapsed time = %1)</source>
        <translation>ukończono (sprawdzanie podglądów z archiwum ZIP, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="280"/>
        <source>done (checking previews from directory, elapsed time = %1)</source>
        <translation>ukończono (sprawdzanie podglądów z katalogu, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="281"/>
        <location filename="../../imgcheck.cpp" line="605"/>
        <location filename="../../imgcheck.cpp" line="932"/>
        <source>%1 found, %2 missing, %3 obsolete</source>
        <translation>%1 znalezionych, %2 brakujących, %3 przestarzałych</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="428"/>
        <source>checking flyers from ZIP archive</source>
        <translation>sprawdzanie ulotek z archiwum ZIP</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="430"/>
        <source>checking flyers from directory</source>
        <translation>sprawdzanie ulotek z katalogu</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="433"/>
        <source>Flyer check - %p%</source>
        <translation>Sprawdzanie ulotek - %p%</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="470"/>
        <source>check pass 1: found and missing flyers</source>
        <translation>1. przebieg sprawdzający: znalezione i brakujące ulotki</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="602"/>
        <source>done (checking flyers from ZIP archive, elapsed time = %1)</source>
        <translation>ukończono (sprawdzanie ulotek z archiwum ZIP, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="604"/>
        <source>done (checking flyers from directory, elapsed time = %1)</source>
        <translation>ukończono (sprawdzanie ulotek z katalogu, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="754"/>
        <source>checking icons from ZIP archive</source>
        <translation>sprawdzanie ikon z archiwum ZIP</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="756"/>
        <source>checking icons from directory</source>
        <translation>sprawdzanie ikon z katalogu</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="759"/>
        <source>Icon check - %p%</source>
        <translation>Sprawdzanie ikon - %p%</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="796"/>
        <source>check pass 1: found and missing icons</source>
        <translation>1. przebieg sprawdzający: znalezione i brakujące ikony</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="929"/>
        <source>done (checking icons from ZIP archive, elapsed time = %1)</source>
        <translation>ukończono (sprawdzanie ikon z archiwum ZIP, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="931"/>
        <source>done (checking icons from directory, elapsed time = %1)</source>
        <translation>ukończono (sprawdzanie ikon z katalogu, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1072"/>
        <source>please wait for reload to finish and try again</source>
        <translation>proszę poczekać za zakończenie przeładowywania i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1082"/>
        <source>please wait for ROM state filter to finish and try again</source>
        <translation>proszę poczekać za zakończenie filtra stanu ROM-ów i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1087"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation>proszę poczekać za zakończenie weryfikacji ROM-ów i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1092"/>
        <source>please wait for sample check to finish and try again</source>
        <translation>proszę poczekać za zakończenie sprawdzania sampli i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1097"/>
        <source>stopping image check upon user request</source>
        <translation>zatrzymywanie sprawdzania obrazów na żądanie użytkownika</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1077"/>
        <source>please wait for ROMAlyzer to finish the current analysis and try again</source>
        <translation>proszę poczekać, aż ROMAlyzer skończy bieżącą analizę i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="70"/>
        <location filename="../../imgcheck.cpp" line="72"/>
        <source>Select machine</source>
        <translation>Wybierz maszynę</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="71"/>
        <location filename="../../imgcheck.cpp" line="73"/>
        <source>Select machine in machine list if selected in check lists?</source>
        <translation>Wybrać maszynę na liście maszyn jeśli wybrana na liście?</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="122"/>
        <location filename="../../imgcheck.ui" line="276"/>
        <location filename="../../imgcheck.ui" line="388"/>
        <source>Select game in game list if selected in check lists?</source>
        <translation>Wybrać grę na liście gier jeśli wybrana na liście?</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="117"/>
        <location filename="../../imgcheck.cpp" line="441"/>
        <location filename="../../imgcheck.cpp" line="767"/>
        <source>WARNING: game list not fully loaded, check results may be misleading</source>
        <translation>UWAGA: lista gier nie jest w pełni załadowana, wyniki sprawdzania mogą być mylące</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="119"/>
        <location filename="../../imgcheck.cpp" line="443"/>
        <location filename="../../imgcheck.cpp" line="769"/>
        <source>WARNING: machine list not fully loaded, check results may be misleading</source>
        <translation>UWAGA: lista maszyn nie jest w pełni załadowana, wyniki sprawdzania mogą być mylące</translation>
    </message>
</context>
<context>
    <name>ItemDownloader</name>
    <message>
        <location filename="../../downloaditem.cpp" line="98"/>
        <source>FATAL: can&apos;t open &apos;%1&apos; for writing</source>
        <translation>FATALNIE: nie można &apos;%1&apos; do zapisu</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="112"/>
        <location filename="../../downloaditem.cpp" line="190"/>
        <location filename="../../downloaditem.cpp" line="207"/>
        <location filename="../../downloaditem.cpp" line="238"/>
        <source>Source URL: %1</source>
        <translation>Adres źródłowy: %1</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="113"/>
        <location filename="../../downloaditem.cpp" line="191"/>
        <location filename="../../downloaditem.cpp" line="208"/>
        <location filename="../../downloaditem.cpp" line="239"/>
        <source>Local path: %2</source>
        <translation>Lokalna ścieżka: %2</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="114"/>
        <location filename="../../downloaditem.cpp" line="192"/>
        <location filename="../../downloaditem.cpp" line="209"/>
        <location filename="../../downloaditem.cpp" line="240"/>
        <source>Status: %1</source>
        <translation>Stan: %1</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="114"/>
        <source>initializing download</source>
        <translation>inicjalizacja pobierania</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="115"/>
        <location filename="../../downloaditem.cpp" line="193"/>
        <location filename="../../downloaditem.cpp" line="210"/>
        <location filename="../../downloaditem.cpp" line="241"/>
        <source>Total size: %1</source>
        <translation>Całkowity rozmiar: %1</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="115"/>
        <source>unknown</source>
        <translation>nieznany</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="116"/>
        <location filename="../../downloaditem.cpp" line="194"/>
        <location filename="../../downloaditem.cpp" line="211"/>
        <location filename="../../downloaditem.cpp" line="242"/>
        <source>Downloaded: %1 (%2%)</source>
        <translation>Pobrano: %1 (%2%)</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="117"/>
        <source>download started: URL = %1, local path = %2, reply ID = %3</source>
        <translation>rozpoczęto pobieranie: adres URL = %1, lokalna ścieżka = %2, ID odpowiedzi = %3</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="175"/>
        <source>Error #%1: </source>
        <translation>Błąd numer %1:</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="181"/>
        <source>download aborted: reason = %1, URL = %2, local path = %3, reply ID = %4</source>
        <translation>przerwano pobieranie: powód = %1, adres URL = %2, lokalna ścieżka = %3, ID odpowiedzi = %4</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="192"/>
        <source>download aborted</source>
        <translation>przerwano pobieranie</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="209"/>
        <source>downloading</source>
        <translation>pobieranie</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="235"/>
        <source>download finished: URL = %1, local path = %2, reply ID = %3</source>
        <translation>ukończono pobieranie: adres URL = %1, lokalna ścieżka = %2, ID odpowiedzi = %3</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="240"/>
        <source>download finished</source>
        <translation>ukończono pobieranie</translation>
    </message>
</context>
<context>
    <name>ItemSelector</name>
    <message>
        <location filename="../../itemselect.ui" line="15"/>
        <source>Item selection</source>
        <translation>Wybór elementów</translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="21"/>
        <source>Select item(s)</source>
        <translation>Wybierz element(y)</translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="54"/>
        <source>Confirm selection</source>
        <translation>Potwierdź wybór</translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="57"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="67"/>
        <location filename="../../itemselect.ui" line="70"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>Joystick</name>
    <message>
        <location filename="../../joystick.cpp" line="67"/>
        <source>ERROR: couldn&apos;t open SDL joystick #%1</source>
        <translation>BŁĄD: nie udało się otworzyć dżojstika SDL nr %1</translation>
    </message>
    <message>
        <location filename="../../joystick.cpp" line="23"/>
        <source>ERROR: couldn&apos;t initialize SDL joystick support</source>
        <translation>BŁĄD: nie udało się zainicjować wsparcia dla dżojstika SDL</translation>
    </message>
</context>
<context>
    <name>JoystickCalibrationWidget</name>
    <message>
        <location filename="../../options.cpp" line="3794"/>
        <source>Axis %1:</source>
        <translation>Oś %1:</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3803"/>
        <source>Current value of axis %1</source>
        <translation>Bieżąca wartość osi %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3807"/>
        <source>DZ:</source>
        <translation>MS:</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3809"/>
        <source>Deadzone of axis %1</source>
        <translation>Martwa strefa osi %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3815"/>
        <source>S:</source>
        <translation>Cz:</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3817"/>
        <source>Sensitivity of axis %1</source>
        <translation>Czułość osi %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3795"/>
        <source>Reset calibration of axis %1</source>
        <translation>Zresetuj kalibrację osi %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3790"/>
        <source>Enable/disable axis %1</source>
        <translation>Włącz/wyłącz oś %1</translation>
    </message>
</context>
<context>
    <name>JoystickFunctionScanner</name>
    <message>
        <location filename="../../joyfuncscan.ui" line="15"/>
        <location filename="../../joyfuncscan.ui" line="72"/>
        <location filename="../../joyfuncscan.cpp" line="24"/>
        <location filename="../../joyfuncscan.cpp" line="25"/>
        <source>Scanning joystick function</source>
        <translation>Skanowanie funkcji dżojstika</translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="27"/>
        <source>&lt;&lt;&lt;&gt;&gt;&gt;</source>
        <translation>&lt;&lt;&lt;&gt;&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="43"/>
        <source>Accept joystick function</source>
        <translation>Akceptuj funkcję dżojstika</translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="46"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="56"/>
        <source>Cancel remapping of joystick function</source>
        <translation>Anuluj ponowne przypisywanie funkcji dżojstika</translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="59"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>JoystickTestWidget</name>
    <message>
        <location filename="../../options.cpp" line="4038"/>
        <source>A%1: %v</source>
        <translation>O%1: %v</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4039"/>
        <source>Current value of axis %1</source>
        <translation>Bieżąca wartość osi %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4049"/>
        <source>B%1</source>
        <translation>P%1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4050"/>
        <source>Current state of button %1</source>
        <translation>Bieżący stan przycisku %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4060"/>
        <source>H%1: 0</source>
        <translation>K%1: 0</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4061"/>
        <source>Current value of hat %1</source>
        <translation>Bieżąca wartość kapturka %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4071"/>
        <source>T%1 DX: 0</source>
        <translation>T%1 OX: 0</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4072"/>
        <source>Current X-delta of trackball %1</source>
        <translation>Bieżące odchylenie X trackballa %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4082"/>
        <source>T%1 DY: 0</source>
        <translation>T%1 OY: 0</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4083"/>
        <source>Current Y-delta of trackball %1</source>
        <translation>Bieżące odchylenie Y trackballa %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4171"/>
        <source>H%1: %2</source>
        <translation>K%1: %2</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4205"/>
        <source>T%1 DX: %2</source>
        <translation>T%1 OX: %2</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4206"/>
        <source>T%1 DY: %2</source>
        <translation>T%1 OY: %2</translation>
    </message>
</context>
<context>
    <name>KeySequenceScanner</name>
    <message>
        <location filename="../../keyseqscan.ui" line="15"/>
        <location filename="../../keyseqscan.ui" line="72"/>
        <location filename="../../keyseqscan.cpp" line="24"/>
        <location filename="../../keyseqscan.cpp" line="25"/>
        <source>Scanning shortcut</source>
        <translation>Skanowanie skrótu</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="27"/>
        <source>&lt;&lt;&lt;&gt;&gt;&gt;</source>
        <translation>&lt;&lt;&lt;&gt;&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="43"/>
        <source>Accept key sequence</source>
        <translation>Zaakceptuj sekwencję klawiszy</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="46"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="56"/>
        <source>Cancel redefinition of key sequence</source>
        <translation>Anuluj redefiniowanie sekwencji klawiszy</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="59"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.cpp" line="21"/>
        <location filename="../../keyseqscan.cpp" line="22"/>
        <source>Scanning special key</source>
        <translation>Skanowanie klawisza specjalnego</translation>
    </message>
</context>
<context>
    <name>MESSDeviceConfigurator</name>
    <message>
        <location filename="../../messdevcfg.ui" line="15"/>
        <source>MESS device configuration</source>
        <translation>Konfiguracja urządzeń MESS</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="37"/>
        <location filename="../../messdevcfg.cpp" line="1124"/>
        <location filename="../../messdevcfg.cpp" line="1129"/>
        <source>Active device configuration</source>
        <translation>Aktywna konfiguracja urządzeń</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="73"/>
        <location filename="../../messdevcfg.ui" line="76"/>
        <source>Device configuration menu</source>
        <translation>Menu konfiguracji urządzeń</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="99"/>
        <location filename="../../messdevcfg.ui" line="102"/>
        <source>Name of device configuration</source>
        <translation>Nazwa konfiguracji urządzeń</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="184"/>
        <location filename="../../messdevcfg.ui" line="187"/>
        <source>Save current device configuration to list of available configurations</source>
        <translation>Zapisz bieżącą konfigurację urządzeń na listę dostępnych konfiguracji</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="208"/>
        <source>Device mappings</source>
        <translation>Mapowania urządzeń</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="229"/>
        <location filename="../../messdevcfg.ui" line="232"/>
        <source>Device setup of current configuration</source>
        <translation>Ustawienia urządzeń bieżącej konfiguracji</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="250"/>
        <source>Brief name</source>
        <translation>Skrócona nazwa</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="279"/>
        <source>Slot options</source>
        <translation>Opcje slotów</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="300"/>
        <location filename="../../messdevcfg.ui" line="303"/>
        <source>Available slot options</source>
        <translation>Dostępne opcje slotów</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="316"/>
        <source>Slot</source>
        <translation>Slot</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="321"/>
        <source>Option</source>
        <translation>Opcja</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="330"/>
        <source>File chooser</source>
        <translation>Selektor plików</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="369"/>
        <location filename="../../messdevcfg.ui" line="372"/>
        <source>Save selected instance / file as a new device configuration</source>
        <translation>Zapisz wybraną instancję / plik jako nową konfigurację urządzeń</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="392"/>
        <location filename="../../messdevcfg.ui" line="395"/>
        <source>Select the device instance the file is mapped to</source>
        <translation>Wybierz instancję urządzenia, do której przypisany jest plik</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="411"/>
        <location filename="../../messdevcfg.ui" line="414"/>
        <source>Automatically select the first matching device instance when selecting a file with a valid extension</source>
        <translation>Automatycznie wybierz pierwszą pasującą instancję urządzenia podczas wyboru pliku o prawidłowym rozszerzeniu</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="434"/>
        <location filename="../../messdevcfg.ui" line="437"/>
        <source>Filter files by allowed extensions for the current device instance</source>
        <translation>Filtruj pliki na podstawie rozszerzeń dozwolonych dla obecnej instancji urządzenia</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="457"/>
        <location filename="../../messdevcfg.ui" line="460"/>
        <source>Process ZIP contents on item activation</source>
        <translation>Przetwarzaj zawartość plików ZIP podczas aktywacji elementu</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="480"/>
        <location filename="../../messdevcfg.ui" line="483"/>
        <source>Enter search string (case-insensitive)</source>
        <translation>Proszę wpisać szukany ciąg (bez uwzględniania wielkości liter)</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="499"/>
        <location filename="../../messdevcfg.ui" line="502"/>
        <source>Clear search string</source>
        <translation>Wyczyść szukany ciąg</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="519"/>
        <location filename="../../messdevcfg.ui" line="522"/>
        <source>Number of files scanned</source>
        <translation>Liczba przeskanowanych plików</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="544"/>
        <location filename="../../messdevcfg.ui" line="547"/>
        <source>Reload directory contents</source>
        <translation>Odśwież zawartość katalogu</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="564"/>
        <location filename="../../messdevcfg.ui" line="567"/>
        <source>Play the selected configuration</source>
        <translation>Uruchom używając wybranej konfiguracji</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="584"/>
        <location filename="../../messdevcfg.ui" line="587"/>
        <source>Play the selected configuration (embedded)</source>
        <translation>Uruchom używając wybranej konfiguracji (tryb osadzony)</translation>
    </message>
    <message>
        <source>Choose directory</source>
        <translation type="obsolete">Wybierz katalog</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="653"/>
        <source>Available device configurations</source>
        <translation>Dostępne konfiguracje urządzeń</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="665"/>
        <location filename="../../messdevcfg.ui" line="668"/>
        <source>List of available device configurations</source>
        <translation>Lista dostępnych konfiguracji urządzeń</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="677"/>
        <location filename="../../messdevcfg.cpp" line="718"/>
        <location filename="../../messdevcfg.cpp" line="879"/>
        <location filename="../../messdevcfg.cpp" line="997"/>
        <source>No devices</source>
        <translation>Brak urządzeń</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="245"/>
        <source>Device instance</source>
        <translation>Instancja urządzenia</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="255"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="260"/>
        <source>Tag</source>
        <translation>Zaznaczenie</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="265"/>
        <source>Extensions</source>
        <translation>Rozszerzenia</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="270"/>
        <source>File</source>
        <translation>Plik</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="115"/>
        <location filename="../../messdevcfg.ui" line="118"/>
        <source>Create a new device configuration</source>
        <translation>Utwórz nową konfigurację urządzeń</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="161"/>
        <location filename="../../messdevcfg.ui" line="164"/>
        <source>Remove current device configuration from list of available configurations</source>
        <translation>Usuń bieżącą konfigurację urządzeń z listy dostępnych konfiguracji</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="138"/>
        <location filename="../../messdevcfg.ui" line="141"/>
        <source>Clone current device configuration</source>
        <translation>Klonuj bieżącą konfigurację urządzeń</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="754"/>
        <location filename="../../messdevcfg.cpp" line="756"/>
        <source>%1. copy of </source>
        <translation>%1. kopia</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="962"/>
        <location filename="../../messdevcfg.cpp" line="1531"/>
        <source>%1. variant of </source>
        <translation>%1 wariant</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="234"/>
        <location filename="../../messdevcfg.cpp" line="276"/>
        <source>Play selected game</source>
        <translation>Graj w wybraną grę</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="146"/>
        <location filename="../../messdevcfg.cpp" line="385"/>
        <source>Reading slot info, please wait...</source>
        <translation>Wczytywanie informacji o slotach, proszę czekać...</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="149"/>
        <source>Enter configuration name</source>
        <translation>Proszę podać nazwę konfiguracji</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="166"/>
        <source>Enter search string</source>
        <translation>Proszę podać szukany ciąg</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="235"/>
        <location filename="../../messdevcfg.cpp" line="277"/>
        <source>&amp;Play</source>
        <translation>&amp;Graj</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="225"/>
        <source>Select default device directory</source>
        <translation>Wybierz domyślny katalog urządzeń</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="226"/>
        <source>&amp;Default device directory for &apos;%1&apos;...</source>
        <translation>&amp;Domyślny katalog urządzeń dla &apos;%1&apos;...</translation>
    </message>
    <message>
        <source>Generate device configurations</source>
        <translation type="obsolete">Utwórz konfiguracje urządzeń</translation>
    </message>
    <message>
        <source>&amp;Generate configurations for &apos;%1&apos;...</source>
        <translation type="obsolete">&amp;Utwórz konfiguracje dla &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="240"/>
        <location filename="../../messdevcfg.cpp" line="282"/>
        <source>Play selected game (embedded)</source>
        <translation>Graj w wybraną grę (osadzaj)</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="241"/>
        <location filename="../../messdevcfg.cpp" line="283"/>
        <source>Play &amp;embedded</source>
        <translation>Graj osad&amp;zając</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="247"/>
        <source>Remove configuration</source>
        <translation>Usuń konfigurację</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="248"/>
        <source>&amp;Remove configuration</source>
        <translation>&amp;Usuń konfigurację</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="256"/>
        <source>Select a file to be mapped to this device instance</source>
        <translation>Wybierz plik który ma być mapowany do tej instancji urządzenia</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="257"/>
        <source>Select file...</source>
        <translation>Wybierz plik...</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="268"/>
        <source>Use as default directory</source>
        <translation>Używaj jako katalog domyślny</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="290"/>
        <location filename="../../messdevcfg.cpp" line="1368"/>
        <source>&amp;Open archive</source>
        <translation>&amp;Otwórz archiwum</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="332"/>
        <location filename="../../messdevcfg.cpp" line="614"/>
        <location filename="../../messdevcfg.cpp" line="1516"/>
        <source>No devices available</source>
        <translation>Brak dostępnych urządzeń</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="387"/>
        <source>loading available system slots</source>
        <translation>wczytywanie dostępnych slotów systemu</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="408"/>
        <source>FATAL: can&apos;t start MESS executable within a reasonable time frame, giving up</source>
        <translation>FATALNIE: nie udało się uruchomienie MESS w sensownym czasie, poddanie</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="409"/>
        <source>Failed to read slot info</source>
        <translation>Nie udało się wczytać informacji o slotach</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="549"/>
        <source>done (loading available system slots, elapsed time = %1)</source>
        <translation>ukończono (wczytywanie dostępnych slotów systemu, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="639"/>
        <source>not used</source>
        <translation>nieużywany</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1067"/>
        <source>Choose default device directory for &apos;%1&apos;</source>
        <translation>Wybierz domyślny katalog urządzeń &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1368"/>
        <source>&amp;Close archive</source>
        <translation>Za&amp;mknij archiwum</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1533"/>
        <source>Choose a unique configuration name</source>
        <translation>Proszę wybrać unikalną nazwę konfiguracji</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1533"/>
        <source>Unique configuration name:</source>
        <translation>Unikalna nazwa konfiguracji:</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1536"/>
        <source>Name conflict</source>
        <translation>Konflikt nazw</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1536"/>
        <source>A configuration named &apos;%1&apos; already exists.

Do you want to choose a different name?</source>
        <translation>Konfiguracja o nazwie &apos;%1&apos; już istnieje.

Czy wybrać inną nazwę?</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="obsolete">Konfiguracja</translation>
    </message>
</context>
<context>
    <name>MESSDeviceFileDelegate</name>
    <message>
        <location filename="../../messdevcfg.cpp" line="53"/>
        <location filename="../../messdevcfg.cpp" line="73"/>
        <source>All files</source>
        <translation>Wszystkie pliki</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="56"/>
        <location filename="../../messdevcfg.cpp" line="58"/>
        <source>Valid device files</source>
        <translation>Poprawne pliki urządzeń</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../qmc2main.ui" line="15"/>
        <location filename="../../macros.h" line="424"/>
        <location filename="../../macros.h" line="430"/>
        <source>M.A.M.E. Catalog / Launcher II</source>
        <translation>M.A.M.E. Catalog / Launcher II</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="711"/>
        <source>Search for games (not case-sensitive)</source>
        <translation>Szukaj gier (bez uwzględniania wielkości liter)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="714"/>
        <source>Search for games</source>
        <translation>Szukaj gier</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="147"/>
        <source>List of all supported games</source>
        <translation>Lista wszystkich obsługiwanych gier</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="154"/>
        <location filename="../../qmc2main.cpp" line="1073"/>
        <source>Game / Attribute</source>
        <translation>Gra / Atrybut</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="159"/>
        <location filename="../../qmc2main.ui" line="291"/>
        <location filename="../../qmc2main.ui" line="426"/>
        <location filename="../../qmc2main.ui" line="561"/>
        <location filename="../../qmc2main.cpp" line="1075"/>
        <location filename="../../qmc2main.cpp" line="1082"/>
        <location filename="../../qmc2main.cpp" line="1115"/>
        <location filename="../../qmc2main.cpp" line="1122"/>
        <location filename="../../qmc2main.cpp" line="1155"/>
        <location filename="../../qmc2main.cpp" line="1180"/>
        <source>Tag</source>
        <translation>Zaznaczenie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="164"/>
        <location filename="../../qmc2main.cpp" line="1077"/>
        <location filename="../../qmc2main.cpp" line="1084"/>
        <source>Icon / Value</source>
        <translation>Ikona / Wartość</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="169"/>
        <location filename="../../qmc2main.ui" line="301"/>
        <location filename="../../qmc2main.ui" line="436"/>
        <location filename="../../qmc2main.ui" line="571"/>
        <location filename="../../qmc2main.cpp" line="1087"/>
        <location filename="../../qmc2main.cpp" line="1127"/>
        <location filename="../../qmc2main.cpp" line="1159"/>
        <location filename="../../qmc2main.cpp" line="1184"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="174"/>
        <location filename="../../qmc2main.ui" line="306"/>
        <location filename="../../qmc2main.ui" line="441"/>
        <location filename="../../qmc2main.ui" line="576"/>
        <location filename="../../qmc2main.cpp" line="1089"/>
        <location filename="../../qmc2main.cpp" line="1129"/>
        <location filename="../../qmc2main.cpp" line="1161"/>
        <location filename="../../qmc2main.cpp" line="1186"/>
        <source>Manufacturer</source>
        <translation>Producent</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="194"/>
        <location filename="../../qmc2main.ui" line="326"/>
        <location filename="../../qmc2main.ui" line="461"/>
        <location filename="../../qmc2main.ui" line="596"/>
        <location filename="../../qmc2main.cpp" line="1097"/>
        <location filename="../../qmc2main.cpp" line="1137"/>
        <location filename="../../qmc2main.cpp" line="1169"/>
        <location filename="../../qmc2main.cpp" line="1194"/>
        <source>Driver status</source>
        <translation>Stan sterownika</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="209"/>
        <location filename="../../qmc2main.ui" line="341"/>
        <location filename="../../qmc2main.ui" line="476"/>
        <location filename="../../qmc2main.ui" line="611"/>
        <location filename="../../qmc2main.cpp" line="1675"/>
        <location filename="../../qmc2main.cpp" line="1969"/>
        <location filename="../../qmc2main.cpp" line="1993"/>
        <location filename="../../qmc2main.cpp" line="3124"/>
        <location filename="../../qmc2main.cpp" line="3321"/>
        <location filename="../../qmc2main.cpp" line="3922"/>
        <location filename="../../qmc2main.cpp" line="4030"/>
        <location filename="../../qmc2main.cpp" line="4646"/>
        <location filename="../../qmc2main.cpp" line="4662"/>
        <location filename="../../qmc2main.cpp" line="5618"/>
        <location filename="../../qmc2main.cpp" line="5639"/>
        <location filename="../../qmc2main.cpp" line="7506"/>
        <location filename="../../qmc2main.cpp" line="7523"/>
        <location filename="../../qmc2main.cpp" line="7600"/>
        <location filename="../../qmc2main.cpp" line="7617"/>
        <source>Waiting for data...</source>
        <translation>Oczekiwanie na dane...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="286"/>
        <location filename="../../qmc2main.cpp" line="1113"/>
        <source>Game / Clones</source>
        <translation>Gra / Klony</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="296"/>
        <location filename="../../qmc2main.ui" line="431"/>
        <location filename="../../qmc2main.ui" line="566"/>
        <location filename="../../qmc2main.cpp" line="1117"/>
        <location filename="../../qmc2main.cpp" line="1124"/>
        <location filename="../../qmc2main.cpp" line="1157"/>
        <location filename="../../qmc2main.cpp" line="1182"/>
        <location filename="../../qmc2main.cpp" line="7809"/>
        <source>Icon</source>
        <translation>Ikona</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="680"/>
        <source>&amp;Search</source>
        <translation>&amp;Szukaj</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="695"/>
        <location filename="../../qmc2main.ui" line="698"/>
        <source>Search result</source>
        <translation>Wyniki szukania</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="729"/>
        <source>Favo&amp;rites</source>
        <translation>Ulu&amp;bione</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="744"/>
        <location filename="../../qmc2main.ui" line="747"/>
        <source>Favorite games</source>
        <translation>Ulubione gry</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="774"/>
        <location filename="../../qmc2main.ui" line="777"/>
        <source>Games last played</source>
        <translation>Ostatnio uruchamiane gry</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="811"/>
        <source>Emulator</source>
        <translation>Emulator</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="847"/>
        <location filename="../../qmc2main.ui" line="850"/>
        <source>L:Listed - C:Correct - M:Mostly correct - I:Incorrect - N:Not found - U:Unknown - S:Search - T:Tagged</source>
        <translation>W:Wymieniony - P:Poprawny - Wp:W większości poprawny - Np:Niepoprawny - Nz:Nieznaleziony - Nn:Nieznany - S:Szukany - Z: Zaznaczony</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="891"/>
        <location filename="../../qmc2main.ui" line="894"/>
        <source>Indicator for current memory usage</source>
        <translation>Wskaźnik bieżącego zużycia pamięci</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1408"/>
        <source>Fl&amp;yer</source>
        <translation>&amp;Ulotka</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1489"/>
        <source>&amp;Configuration</source>
        <translation>&amp;Konfiguracja</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1498"/>
        <source>&amp;Devices</source>
        <translation>U&amp;rządzenia</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1525"/>
        <source>Mar&amp;quee</source>
        <translation>P&amp;lansza tytułowa</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1543"/>
        <source>MA&amp;WS</source>
        <translation>MA&amp;WS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1552"/>
        <source>&amp;PCB</source>
        <translation>Pł&amp;ytka</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1561"/>
        <source>Softwar&amp;e list</source>
        <translation>Lista &amp;oprogramowania</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1570"/>
        <source>&amp;YouTube</source>
        <translation>&amp;YouTube</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1611"/>
        <location filename="../../qmc2main.ui" line="1614"/>
        <source>Frontend log</source>
        <translation>Dziennik interfejsu</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1635"/>
        <source>Emulator &amp;log</source>
        <translation>Dziennik &amp;emulatora</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1650"/>
        <location filename="../../qmc2main.ui" line="1653"/>
        <source>Emulator log</source>
        <translation>Dziennik emulatora</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1748"/>
        <source>MP&amp;3 player</source>
        <translation>Odtwarzacz MP&amp;3</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1769"/>
        <location filename="../../qmc2main.ui" line="1772"/>
        <source>Playlist (move items by dragging &amp; dropping them)</source>
        <translation>Lista odtwarzania (elementy można przesuwać metodą przeciągnij i upuść)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1968"/>
        <location filename="../../qmc2main.ui" line="1971"/>
        <source>Enter URL to add to playlist</source>
        <translation>Wprowadź adres URL do dodania do listy odtwarzania</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1999"/>
        <location filename="../../qmc2main.ui" line="2002"/>
        <source>Setup available audio effects</source>
        <translation>Ustaw dostępne efekty dźwięku</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2097"/>
        <source>Dow&amp;nloads</source>
        <translation>P&amp;obierania</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2204"/>
        <source>Automatically remove successfully finished downloads from this list</source>
        <translation>Usuwaj pobierania zakończone sukcesem automatycznie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2243"/>
        <source>&amp;Help</source>
        <translation>Pomo&amp;c</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2252"/>
        <source>&amp;Tools</source>
        <translation>Narzę&amp;dzia</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2319"/>
        <source>&amp;Check</source>
        <translation>&amp;Sprawdź</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2336"/>
        <source>&amp;View</source>
        <translation>&amp;Widok</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2349"/>
        <source>&amp;Tag</source>
        <translation>Zaz&amp;naczenie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2382"/>
        <source>&amp;Display</source>
        <translation>Ekra&amp;n</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2417"/>
        <source>Toolbar</source>
        <translation>Pasek narzędzi</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2466"/>
        <source>Check &amp;samples...</source>
        <translation>Sprawdź &amp;sample...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2469"/>
        <location filename="../../qmc2main.ui" line="2472"/>
        <source>Check sample set</source>
        <translation>Sprawdź zestaw sampli</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2475"/>
        <source>Ctrl+2</source>
        <translation>Ctrl+2</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2487"/>
        <source>Check &amp;previews...</source>
        <translation>Sprawdź &amp;podglądy...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2490"/>
        <location filename="../../qmc2main.ui" line="2493"/>
        <source>Check preview images</source>
        <translation>Sprawdź obrazy podglądów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2496"/>
        <source>Ctrl+3</source>
        <translation>Ctrl+3</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2856"/>
        <source>Analyse ROM (tagged)...</source>
        <translation>Analizuj ROM (zaznaczone)...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2859"/>
        <source>Analyse ROM (tagged)</source>
        <translation>Analizuj ROM (zaznaczone)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2862"/>
        <location filename="../../qmc2main.ui" line="2865"/>
        <source>Analyse all tagged sets with ROMAlyzer</source>
        <translation>Analizuj wszystkie zaznaczone zestawy za pomocą ROMAlyzera</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3271"/>
        <source>Check template map</source>
        <translation>Sprawdź mapę szablonu</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3274"/>
        <location filename="../../qmc2main.ui" line="3277"/>
        <source>Check template map against the configuration options of the currently selected emulator</source>
        <translation>Sprawdź mapę szablonu z opcjami konfiguracji obecnie wybranego emulatora</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3280"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3295"/>
        <location filename="../../qmc2main.ui" line="3298"/>
        <source>Play current game (embedded)</source>
        <translation>Graj w obecną grę (osadzając)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3301"/>
        <source>Ctrl+Shift+P</source>
        <translation>Ctrl+Shift+P</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3313"/>
        <source>&amp;Demo mode...</source>
        <translation>Tryb de&amp;monstracyjny...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3316"/>
        <location filename="../../qmc2main.ui" line="3319"/>
        <source>Open the demo mode dialog</source>
        <translation>Otwórz okno dialogowe trybu demonstracyjnego</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3598"/>
        <source>Set tag</source>
        <translation>Zaznacz</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3601"/>
        <location filename="../../qmc2main.ui" line="3604"/>
        <source>Set tag mark</source>
        <translation>Ustaw zaznaczenie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3607"/>
        <source>Ctrl+Shift+T</source>
        <translation>Ctrl+Shift+T</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3619"/>
        <source>Unset tag</source>
        <translation>Odznacz</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3622"/>
        <location filename="../../qmc2main.ui" line="3625"/>
        <source>Unset tag mark</source>
        <translation>Usuń zaznaczenie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3628"/>
        <source>Ctrl+Shift+U</source>
        <translation>Ctrl+Shift+U</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3640"/>
        <source>Toggle tag</source>
        <translation>Odwróć zaznaczenie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3643"/>
        <location filename="../../qmc2main.ui" line="3646"/>
        <source>Toggle tag mark</source>
        <translation>Odwróć zaznaczenie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3649"/>
        <source>Ctrl+Shift+G</source>
        <translation>Ctrl+Shift+G</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3661"/>
        <source>Tag all</source>
        <translation>Zaznacz wszystkie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3664"/>
        <location filename="../../qmc2main.ui" line="3667"/>
        <source>Set tag mark for all sets</source>
        <translation>Ustaw zaznaczenie wszystkim zestawom</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3670"/>
        <source>Ctrl+Shift+L</source>
        <translation>Ctrl+Shift+L</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3682"/>
        <source>Untag all</source>
        <translation>Odznacz wszystkie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3685"/>
        <location filename="../../qmc2main.ui" line="3688"/>
        <source>Unset all tag marks</source>
        <translation>Usuń wszystkie zaznaczenia</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3691"/>
        <source>Ctrl+Shift+N</source>
        <translation>Ctrl+Shift+N</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3703"/>
        <source>Invert all tags</source>
        <translation>Odwróć wszystkie zaznaczenia</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3706"/>
        <location filename="../../qmc2main.ui" line="3709"/>
        <source>Invert all tag marks</source>
        <translation>Odwróć wszystkie zaznaczenia</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3712"/>
        <source>Ctrl+Shift+I</source>
        <translation>Ctrl+Shift+I</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2868"/>
        <source>Ctrl+Shift+D</source>
        <translation>Ctrl+Shift+D</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3331"/>
        <source>By category</source>
        <translation>Według kategorii</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3334"/>
        <location filename="../../qmc2main.ui" line="3337"/>
        <source>View games by category</source>
        <translation>Wyświetlaj gry według kategorii</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3340"/>
        <source>F7</source>
        <translation>F7</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3352"/>
        <source>By version</source>
        <translation>Według wersji</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3355"/>
        <location filename="../../qmc2main.ui" line="3358"/>
        <source>View games by version they were added to the emulator</source>
        <translation>Wyświetlaj gry według wersji, w której zostały dodane do emulatora</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3361"/>
        <source>F8</source>
        <translation>F8</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3373"/>
        <source>Run external ROM tool...</source>
        <translation>Uruchom zewnętrzne narzędzie do obsługi ROM-ów...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3376"/>
        <location filename="../../qmc2main.ui" line="3379"/>
        <source>Run tool to process ROM data externally</source>
        <translation>Uruchom narzędzie, aby przetworzyć dane ROM-ów zewnętrznie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3382"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3394"/>
        <source>Run external ROM tool (tagged)...</source>
        <translation>Uruchom zewnętrzne narzędzie do obsługi ROM-ów (zaznaczone)...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3397"/>
        <location filename="../../qmc2main.ui" line="3400"/>
        <source>Run tool to process ROM data externally for all tagged sets</source>
        <translation>Uruchom narzędzie, aby przetworzyć dane ROM-ów zewnętrznie dla wszystkich zaznaczonych zestawów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3403"/>
        <source>Ctrl+Shift+F9</source>
        <translation>Ctrl+Shift+F9</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3411"/>
        <location filename="../../qmc2main.ui" line="3414"/>
        <source>Clear YouTube cache</source>
        <translation>Wyczyść bufor YouTube</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3417"/>
        <source>Ctrl+Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3425"/>
        <source>Clear ROM state cache</source>
        <translation>Wyczyść bufor stanów ROM-ów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3428"/>
        <location filename="../../qmc2main.ui" line="3431"/>
        <source>Forcedly clear (remove) the ROM state cache</source>
        <translation>Wymuś wyczyszczenie (usunięcie) bufora stanów ROM-ów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3439"/>
        <source>Clear game list cache</source>
        <translation>Wyczyść bufor listy gier</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3442"/>
        <location filename="../../qmc2main.ui" line="3445"/>
        <source>Forcedly clear (remove) the game list cache</source>
        <translation>Wymuś wyczyszczenie (usunięcie) bufora listy gier</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3453"/>
        <source>Clear XML cache</source>
        <translation>Wyczyść bufor XML</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3456"/>
        <location filename="../../qmc2main.ui" line="3459"/>
        <source>Forcedly clear (remove) the XML cache</source>
        <translation>Wymuś wyczyszczenie (usunięcie) bufora XML</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3467"/>
        <source>Clear software list cache</source>
        <translation>Wyczyść bufor listy oprogramowania</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3470"/>
        <location filename="../../qmc2main.ui" line="3473"/>
        <source>Forcedly clear (remove) the software list cache</source>
        <translation>Wymuś wyczyszczenie (usunięcie) bufora listy oprogramowania</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3481"/>
        <source>Clear ALL emulator caches</source>
        <translation>Wyczyść WSZYSTKIE bufory emulatorów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3484"/>
        <location filename="../../qmc2main.ui" line="3487"/>
        <source>Forcedly clear (remove) ALL emulator related caches</source>
        <translation>Wymuś wyczyszczenie (usunięcie) WSZYSTKICH buforów emulatorów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2508"/>
        <location filename="../../qmc2main.cpp" line="745"/>
        <location filename="../../qmc2main.cpp" line="797"/>
        <location filename="../../qmc2main.cpp" line="849"/>
        <location filename="../../qmc2main.cpp" line="908"/>
        <source>&amp;Play</source>
        <translation>&amp;Graj</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2511"/>
        <location filename="../../qmc2main.ui" line="2514"/>
        <source>Play current game</source>
        <translation>Graj w bieżącą grę</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2517"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3499"/>
        <source>Play (tagged)</source>
        <translation>Graj (zaznaczone)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3502"/>
        <location filename="../../qmc2main.ui" line="3505"/>
        <source>Play all tagged games</source>
        <translation>Graj we wszystkie zaznaczone gry</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3517"/>
        <source>Play embedded (tagged)</source>
        <translation>Graj osadzając (zaznaczone)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3520"/>
        <location filename="../../qmc2main.ui" line="3523"/>
        <source>Play all tagged games (embedded)</source>
        <translation>Graj osadzając we wszystkie zaznaczone gry</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3535"/>
        <source>To favorites (tagged)</source>
        <translation>Do ulubionych (zaznaczone)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3538"/>
        <location filename="../../qmc2main.ui" line="3541"/>
        <source>Add all tagged games to favorites</source>
        <translation>Dodaj wszystkie zaznaczone gry do ulubionych</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3544"/>
        <source>Ctrl+Shift+F</source>
        <translation>Ctrl+Shift+F</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3556"/>
        <source>ROM state (tagged)</source>
        <translation>Stan ROM-ów (zaznaczone)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3559"/>
        <location filename="../../qmc2main.ui" line="3562"/>
        <source>Check ROM states of all tagged sets</source>
        <translation>Sprawdź stany ROM-ów wszystkich zaznaczonych zestawów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3565"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3577"/>
        <source>E&amp;xit / Stop</source>
        <translation>W&amp;yjdź / Zatrzymaj</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3580"/>
        <location filename="../../qmc2main.ui" line="3583"/>
        <source>Exit program / Stop any active processing</source>
        <translation>Wyjdź z programu / zatrzymaj jakiekolwiek bieżące przetwarzanie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3586"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2529"/>
        <source>&amp;Documentation...</source>
        <translation>&amp;Dokumentacja...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2532"/>
        <location filename="../../qmc2main.ui" line="2535"/>
        <source>View online documentation</source>
        <translation>Obejrzyj dokumentację on-line</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2538"/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2553"/>
        <location filename="../../qmc2main.ui" line="2556"/>
        <source>About M.A.M.E. Catalog / Launcher II</source>
        <translation>O M.A.M.E. Catalog / Launcher II</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2559"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2574"/>
        <location filename="../../qmc2main.cpp" line="2646"/>
        <source>About Qt</source>
        <translation>O Qt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2577"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2589"/>
        <source>Check &amp;ROMs...</source>
        <translation>Sprawdź &amp;ROM-y...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2592"/>
        <location filename="../../qmc2main.ui" line="2595"/>
        <source>Check ROM collection</source>
        <translation>Sprawdź kolekcję ROM-ów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2598"/>
        <source>Ctrl+1</source>
        <translation>Ctrl+1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2610"/>
        <source>&amp;Options...</source>
        <translation>&amp;Opcje...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2613"/>
        <source>Options</source>
        <translation>Opcje</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2616"/>
        <location filename="../../qmc2main.ui" line="2619"/>
        <source>Frontend setup and global emulator configuration</source>
        <translation>Ustawienia interfejsu i globalna konfiguracja emulatora</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2622"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2634"/>
        <source>&amp;Reload</source>
        <translation>&amp;Przeładuj</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2643"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2651"/>
        <location filename="../../qmc2main.ui" line="2654"/>
        <location filename="../../qmc2main.ui" line="2657"/>
        <source>Clear image cache</source>
        <translation>Wyczyść bufor obrazów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2660"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2677"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2689"/>
        <location filename="../../qmc2main.cpp" line="767"/>
        <location filename="../../qmc2main.cpp" line="819"/>
        <location filename="../../qmc2main.cpp" line="930"/>
        <location filename="../../qmc2main.cpp" line="4180"/>
        <source>To &amp;favorites</source>
        <translation>Do ulu&amp;bionych</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2692"/>
        <location filename="../../qmc2main.ui" line="2695"/>
        <location filename="../../qmc2main.cpp" line="763"/>
        <location filename="../../qmc2main.cpp" line="815"/>
        <location filename="../../qmc2main.cpp" line="926"/>
        <source>Add current game to favorites</source>
        <translation>Dodaj bieżącą grę do ulubionych</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2698"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2719"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2740"/>
        <source>F6</source>
        <translation>F6</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2752"/>
        <source>Check &amp;flyers...</source>
        <translation>Sprawdź &amp;ulotki...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2755"/>
        <location filename="../../qmc2main.ui" line="2758"/>
        <source>Check flyer images</source>
        <translation>Sprawdź obrazy ulotek</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2761"/>
        <source>Ctrl+4</source>
        <translation>Ctrl+4</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2769"/>
        <location filename="../../qmc2main.ui" line="2772"/>
        <source>Clear icon cache</source>
        <translation>Wyczyść bufor ikon</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2775"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2787"/>
        <source>Check &amp;icons...</source>
        <translation>Sprawdź &amp;ikony...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2790"/>
        <location filename="../../qmc2main.ui" line="2793"/>
        <source>Check icon images</source>
        <translation>Sprawdź obrazy ikon</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2796"/>
        <source>Ctrl+5</source>
        <translation>Ctrl+5</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2808"/>
        <location filename="../../qmc2main.ui" line="2811"/>
        <source>ROM state</source>
        <translation>Stan ROM-u</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2814"/>
        <location filename="../../qmc2main.ui" line="2817"/>
        <location filename="../../qmc2main.cpp" line="773"/>
        <location filename="../../qmc2main.cpp" line="825"/>
        <location filename="../../qmc2main.cpp" line="868"/>
        <location filename="../../qmc2main.cpp" line="936"/>
        <source>Check current game&apos;s ROM state</source>
        <translation>Sprawdź stan ROM-u bieżącej gry</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2820"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="786"/>
        <location filename="../../qmc2main.cpp" line="838"/>
        <location filename="../../qmc2main.cpp" line="881"/>
        <location filename="../../qmc2main.cpp" line="949"/>
        <source>&amp;Analyse ROM...</source>
        <translation>&amp;Analizuj ROM...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2835"/>
        <source>Analyse ROM</source>
        <translation>Analizuj ROM</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2844"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2880"/>
        <source>ROMAly&amp;zer...</source>
        <translation>ROMAly&amp;zer...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2883"/>
        <location filename="../../qmc2main.ui" line="2886"/>
        <source>Open ROMAlyzer dialog</source>
        <translation>Otwórz okno dialogowe ROMAlyzera</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2889"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="406"/>
        <location filename="../../qmc2main.cpp" line="3530"/>
        <location filename="../../qmc2main.cpp" line="3566"/>
        <location filename="../../qmc2main.cpp" line="3755"/>
        <location filename="../../qmc2main.cpp" line="3844"/>
        <location filename="../../qmc2main.cpp" line="5237"/>
        <location filename="../../qmc2main.cpp" line="5476"/>
        <location filename="../../qmc2main.cpp" line="5515"/>
        <source>Default</source>
        <translation>Domyślny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="488"/>
        <source>Toggle maximization of embedded emulator windows</source>
        <translation>Przełącz maksymalizację osadzonych okien emulatora</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="530"/>
        <source>M&amp;achine list</source>
        <translation>&amp;Lista maszyn</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="548"/>
        <location filename="../../qmc2main.cpp" line="549"/>
        <source>Play current machine (embedded)</source>
        <translation>Graj na obecnej maszynie (osadzając)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="590"/>
        <location filename="../../qmc2main.cpp" line="591"/>
        <source>Loading machine list, please wait...</source>
        <translation>Wczytywanie listy maszyn, proszę czekać...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="592"/>
        <source>Search for machines (not case-sensitive)</source>
        <translation>Szukaj maszyn (bez uwzględniania wielkości liter)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="593"/>
        <source>Search for machines</source>
        <translation>Szukaj maszyn</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="644"/>
        <source>restoring main widget layout</source>
        <translation>przywracanie układu widżeta głównego</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="715"/>
        <source>Embed emulator widget</source>
        <translation>Osadzaj widżet emulatora</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="716"/>
        <source>&amp;Embed</source>
        <translation>Osad&amp;zaj</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="722"/>
        <source>Terminate selected emulator(s) (sends TERM signal to emulator process(es))</source>
        <translation>Zakończ wybrane emulatory (wysyła sygnał TERM do procesów emulatorów)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="723"/>
        <source>&amp;Terminate</source>
        <translation>&amp;Zakończ</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="727"/>
        <source>Kill selected emulator(s) (sends KILL signal to emulator process(es))</source>
        <translation>Zabij wybrane emulatory (wysyła sygnał KILL do procesów emulatorów)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="728"/>
        <source>&amp;Kill</source>
        <translation>Za&amp;bij</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="741"/>
        <location filename="../../qmc2main.cpp" line="793"/>
        <location filename="../../qmc2main.cpp" line="845"/>
        <location filename="../../qmc2main.cpp" line="904"/>
        <source>Play selected game</source>
        <translation>Graj w wybraną grę</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="752"/>
        <location filename="../../qmc2main.cpp" line="804"/>
        <location filename="../../qmc2main.cpp" line="856"/>
        <location filename="../../qmc2main.cpp" line="915"/>
        <source>Play selected game (embedded)</source>
        <translation>Graj w wybraną grę (osadzając)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="754"/>
        <location filename="../../qmc2main.cpp" line="806"/>
        <location filename="../../qmc2main.cpp" line="858"/>
        <location filename="../../qmc2main.cpp" line="917"/>
        <source>Start selected machine (embedded)</source>
        <translation>Uruchom wybraną maszynę (osadzając)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3292"/>
        <location filename="../../qmc2main.cpp" line="756"/>
        <location filename="../../qmc2main.cpp" line="808"/>
        <location filename="../../qmc2main.cpp" line="860"/>
        <location filename="../../qmc2main.cpp" line="919"/>
        <source>Play &amp;embedded</source>
        <translation>Graj osad&amp;zając</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="777"/>
        <location filename="../../qmc2main.cpp" line="829"/>
        <location filename="../../qmc2main.cpp" line="872"/>
        <location filename="../../qmc2main.cpp" line="940"/>
        <source>Check &amp;ROM state</source>
        <translation>Sprawdź stan &amp;ROM-u</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="886"/>
        <source>Remove from favorites</source>
        <translation>Usuń z ulubionych</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="887"/>
        <location filename="../../qmc2main.cpp" line="955"/>
        <source>&amp;Remove</source>
        <translation>&amp;Usuń</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="891"/>
        <source>Clear all favorites</source>
        <translation>Wyczyść wszystkie ulubione</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="892"/>
        <location filename="../../qmc2main.cpp" line="960"/>
        <source>&amp;Clear</source>
        <translation>&amp;Wyczyść</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="896"/>
        <source>Save favorites now</source>
        <translation>Zapisz ulubione teraz</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="897"/>
        <location filename="../../qmc2main.cpp" line="965"/>
        <source>&amp;Save</source>
        <translation>&amp;Zapisz</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="954"/>
        <source>Remove from played</source>
        <translation>Usuń z granych</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="959"/>
        <source>Clear all played</source>
        <translation>Wyczyść wszystkie grane</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="964"/>
        <source>Save play-history now</source>
        <translation>Zapisz historię gier teraz</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1045"/>
        <location filename="../../qmc2main.cpp" line="1057"/>
        <source>Flip splitter orientation</source>
        <translation>Przestaw orientację</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1046"/>
        <location filename="../../qmc2main.cpp" line="1058"/>
        <source>&amp;Flip splitter orientation</source>
        <translation>Przestaw &amp;orientację</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1050"/>
        <source>Swap splitter&apos;s sub-layouts</source>
        <translation>Zamień podukłady</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1051"/>
        <source>&amp;Swap splitter&apos;s sub-layouts</source>
        <translation>Zamień &amp;podukłady</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1062"/>
        <source>Swap splitter&apos;s sub-widgets</source>
        <translation>Zamień podwidżety</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1063"/>
        <source>&amp;Swap splitter&apos;s sub-widgets</source>
        <translation>Zamień &amp;podwidżety</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1216"/>
        <source>Enter search string</source>
        <translation>Wpisz szukany ciąg</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1392"/>
        <source>sorry, devices cannot run standalone</source>
        <translation>Przepraszamy, urządzenia nie mogą pracować samodzielnie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1544"/>
        <source>No devices available</source>
        <translation>Brak dostępnych urządzeń</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1693"/>
        <location filename="../../qmc2main.cpp" line="1775"/>
        <location filename="../../qmc2main.cpp" line="1797"/>
        <source>please wait for ROM state filter to finish and try again</source>
        <translation>proszę poczekać za zakończenie filtra stanu ROM-ów i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1698"/>
        <location filename="../../qmc2main.cpp" line="2107"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation>proszę poczekać za zakończenie weryfikacji ROM-ów i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1703"/>
        <location filename="../../qmc2main.cpp" line="1781"/>
        <location filename="../../qmc2main.cpp" line="1803"/>
        <source>please wait for image check to finish and try again</source>
        <translation>proszę poczekać za zakończenie sprawdzania obrazów i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1708"/>
        <location filename="../../qmc2main.cpp" line="1783"/>
        <location filename="../../qmc2main.cpp" line="1805"/>
        <source>please wait for sample check to finish and try again</source>
        <translation>proszę poczekać za zakończenie sprawdzania sampli i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1741"/>
        <location filename="../../qmc2main.cpp" line="5111"/>
        <source>saving game selection</source>
        <translation>zapisywanie wyboru gry</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1777"/>
        <location filename="../../qmc2main.cpp" line="1799"/>
        <source>ROM verification already active</source>
        <translation>weryfikacja ROM-ów jest już aktywna</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1779"/>
        <location filename="../../qmc2main.cpp" line="1801"/>
        <location filename="../../qmc2main.cpp" line="2103"/>
        <location filename="../../qmc2main.cpp" line="2142"/>
        <location filename="../../qmc2main.cpp" line="2179"/>
        <location filename="../../qmc2main.cpp" line="2211"/>
        <location filename="../../qmc2main.cpp" line="3913"/>
        <source>please wait for reload to finish and try again</source>
        <translation>proszę poczekać za zakończenie przeładowywania i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1811"/>
        <location filename="../../qmc2main.cpp" line="4524"/>
        <location filename="../../qmc2main.cpp" line="4569"/>
        <location filename="../../qmc2main.cpp" line="4993"/>
        <location filename="../../qmc2main.cpp" line="5008"/>
        <location filename="../../qmc2main.cpp" line="5039"/>
        <source>Confirm</source>
        <translation>Potwierdź</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1812"/>
        <source>The ROM verification process may be very time-consuming.
It will overwrite existing cached data.

Do you really want to check all ROM states now?</source>
        <translation>Proces weryfikacji ROM-ów może zająć bardzo dużo czasu.
Nadpisze on istniejące zbuforowane dane .

Czy naprawdę chcesz sprawdzić teraz stany wszystkich ROM-ów?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1823"/>
        <source>automatic ROM check triggered</source>
        <translation>uruchomiono automatyczne sprawdzanie ROM-ów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2025"/>
        <source>image cache cleared</source>
        <translation>wyczyszczono bufor obrazów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2036"/>
        <source>icon cache cleared</source>
        <translation>wyczyszczono bufor ikon</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2092"/>
        <source>YouTube on-disk cache cleared (%1)</source>
        <translation>Wyczyszczono dyskowy bufor YouTube (%1)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2124"/>
        <source>ROM state cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation>Usunięcie pliku bufora stanów ROM-ów &apos;%1&apos; zostało wymuszone przez użytkownika</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2126"/>
        <source>WARNING: cannot remove the ROM state cache file &apos;%1&apos;, please check permissions</source>
        <translation>UWAGA: nie można usunąć pliku bufora stanów ROM-ów &apos;%1&apos;, proszę sprawdzić uprawnienia</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2131"/>
        <source>triggering an automatic ROM check on next reload</source>
        <translation>uruchamianie automatycznego sprawdzenia romów podczas następnego przeładowania</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2160"/>
        <source>game list cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation>Usunięcie pliku bufora listy gier &apos;%1&apos; zostało wymuszone przez użytkownika</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2162"/>
        <source>WARNING: cannot remove the game list cache file &apos;%1&apos;, please check permissions</source>
        <translation>UWAGA: nie można usunąć pliku bufora listy gier &apos;%1&apos;, proszę sprawdzić uprawnienia</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2165"/>
        <source>machine list cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation>Usunięcie pliku bufora listy maszyn &apos;%1&apos; zostało wymuszone przez użytkownika</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2167"/>
        <source>WARNING: cannot remove the machine list cache file &apos;%1&apos;, please check permissions</source>
        <translation>UWAGA: nie można usunąć pliku bufora listy maszyn &apos;%1&apos;, proszę sprawdzić uprawnienia</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2196"/>
        <source>XML cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation>Usunięcie pliku bufora XML &apos;%1&apos; zostało wymuszone przez użytkownika</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2198"/>
        <source>WARNING: cannot remove the XML cache file &apos;%1&apos;, please check permissions</source>
        <translation>UWAGA: nie można usunąć pliku bufora XML &apos;%1&apos;, proszę sprawdzić uprawnienia</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2228"/>
        <source>software list cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation>Usunięcie pliku bufora listy oprogramowania &apos;%1&apos; zostało wymuszone przez użytkownika</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2230"/>
        <source>WARNING: cannot remove the software list cache file &apos;%1&apos;, please check permissions</source>
        <translation>UWAGA: nie można usunąć pliku bufora listy oprogramowania &apos;%1&apos;, proszę sprawdzić uprawnienia</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2850"/>
        <source>ERROR: no match found (?)</source>
        <translation>BŁĄD: nie znaleziono dopasowania (?)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3552"/>
        <source>Emulator for this game</source>
        <translation>Emulator dla tej gry</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3554"/>
        <source>Emulator for this machine</source>
        <translation>Emulator dla tej maszyny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3595"/>
        <source>Export to...</source>
        <translation>Eksportuj do...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3617"/>
        <location filename="../../qmc2main.cpp" line="3621"/>
        <source>&lt;inipath&gt;/%1.ini</source>
        <translation>&lt;ścieżka ini&gt;/%1.ini</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3618"/>
        <location filename="../../qmc2main.cpp" line="3622"/>
        <source>Select file...</source>
        <translation>Wybierz plik...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3697"/>
        <location filename="../../qmc2main.cpp" line="3704"/>
        <source>&lt;p&gt;No data available&lt;/p&gt;</source>
        <translation>&lt;p&gt;Brak danych&lt;p&gt;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8652"/>
        <location filename="../../qmc2main.cpp" line="8686"/>
        <source>Play tagged - %p%</source>
        <translation>Graj w zaznaczone - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8722"/>
        <source>Add favorites - %p%</source>
        <translation>Dodaj do ulubionych - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8755"/>
        <location filename="../../qmc2main.cpp" line="8783"/>
        <location filename="../../qmc2main.cpp" line="8823"/>
        <source>please wait for current activity to finish and try again (this batch-mode operation can only run exclusively)</source>
        <translation>proszę poczekać na zakończenie bieżącej aktywności i spróbować ponownie (ta operacja wsadowa może działać jedynie samodzielnie)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8829"/>
        <source>ROM tool tagged - %p%</source>
        <translation>Narzędzie ROM-ów (zaznaczone) - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8963"/>
        <source>Tag - %p%</source>
        <translation>Ustawianie zaznaczenia - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9005"/>
        <source>Untag - %p%</source>
        <translation>Usuwanie zaznaczenia - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9047"/>
        <source>Invert tag - %p%</source>
        <translation>Odwracanie zaznaczenia - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="789"/>
        <location filename="../../qmc2main.cpp" line="4155"/>
        <source>Embedded emulators</source>
        <translation>Osadzone emulatory</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4165"/>
        <source>Release emulator</source>
        <translation>Uwolnij emulator</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4212"/>
        <source>WARNING: no matching window for emulator #%1 found</source>
        <translation>UWAGA: nie znaleziono okna odpowiadającego emulatorowi #%1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4218"/>
        <source>Embedding failed</source>
        <translation>Osadzanie nie powiodło się</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4243"/>
        <location filename="../../qmc2main.cpp" line="4244"/>
        <source>Scanning pause key</source>
        <translation>Skanowanie klawisza pauzy</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4763"/>
        <location filename="../../qmc2main.cpp" line="4822"/>
        <source>Choose export file</source>
        <translation>Wybierz plik eksportu</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4763"/>
        <location filename="../../qmc2main.cpp" line="4790"/>
        <location filename="../../qmc2main.cpp" line="4822"/>
        <location filename="../../qmc2main.cpp" line="4858"/>
        <location filename="../../qmc2main.cpp" line="6383"/>
        <location filename="../../qmc2main.cpp" line="8113"/>
        <location filename="../../qmc2main.cpp" line="8155"/>
        <source>All files (*)</source>
        <translation>Wszystkie pliki (*)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4790"/>
        <location filename="../../qmc2main.cpp" line="4858"/>
        <source>Choose import file</source>
        <translation>Wybierz plik importu</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4820"/>
        <location filename="../../qmc2main.cpp" line="4855"/>
        <source>WARNING: invalid inipath</source>
        <translation>UWAGA: nieprawidłowa ścieżka ini</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4987"/>
        <source>stopping current processing upon user request</source>
        <translation>zatrzymywanie bieżącego przetwarzania na żądanie użytkownika</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4994"/>
        <source>Your configuration changes have not been applied yet.
Really quit?</source>
        <translation>Twoje zmiany konfiguracji nie zostały jeszcze zastosowane.
Na pewno zakończyć?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5009"/>
        <source>There are one or more emulators still running.
Should they be killed on exit?</source>
        <translation>Pozostało jeden lub więcej uruchomionych emulatorów.
Czy mają być zabite przy wyjściu?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5040"/>
        <source>There are one or more running downloads. Quit anyway?</source>
        <translation>Jedno lub więcej pobierań wciąż jest aktywnych. Zakończyć mimo wszystko?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5056"/>
        <source>cleaning up</source>
        <translation>czyszczenie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5059"/>
        <source>aborting running downloads</source>
        <translation>przerywanie aktywnych pobierań</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5071"/>
        <source>saving YouTube video info map</source>
        <translation>zapisywanie mapy informacji o filmie YouTube</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5094"/>
        <source>done (saving YouTube video info map)</source>
        <translation>ukończono (zapisywanie mapy informacji o filmie YouTube)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5096"/>
        <location filename="../../qmc2main.cpp" line="5098"/>
        <source>failed (saving YouTube video info map)</source>
        <translation>nieudane (zapisywanie mapy informacji o filmie YouTube)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5126"/>
        <source>saving main widget layout</source>
        <translation>zapisywanie układu widżeta głównego</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5213"/>
        <source>saving current game&apos;s favorite software</source>
        <translation>zapisywanie ulubionego oprogramowania bieżącej gry</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5219"/>
        <source>saving current machine&apos;s favorite software</source>
        <translation>zapisywanie ulubionego oprogramowania bieżącej maszyny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5232"/>
        <source>destroying current game&apos;s emulator configuration</source>
        <translation>niszczenie konfiguracji emulatora bieżącej gry</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5244"/>
        <source>destroying global emulator options</source>
        <translation>niszczenie globalnych opcji emulatora</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5278"/>
        <source>destroying preview</source>
        <translation>niszczenie podglądu</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5282"/>
        <source>destroying flyer</source>
        <translation>niszczenie ulotki</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5302"/>
        <source>destroying PCB</source>
        <translation>niszczenie płytki</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5306"/>
        <source>destroying about dialog</source>
        <translation>niszczenie okna dialogowego o programie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5319"/>
        <source>destroying MAWS quick download setup</source>
        <translation>niszczenie ustawień szybkiego pobierania MAWS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5325"/>
        <source>destroying image checker</source>
        <translation>niszczenie sprawdzacza obrazów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5330"/>
        <source>destroying sample checker</source>
        <translation>niszczenie sprawdzacza sampli</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5351"/>
        <source>destroying demo mode dialog</source>
        <translation>niszczenie okna dialogowego trybu demonstracyjnego</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5257"/>
        <source>disconnecting audio source from audio sink</source>
        <translation>odzłączanie źródła dźwięku od wyśjścia</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5270"/>
        <source>destroying YouTube video widget</source>
        <translation>niszczenie widżetu filmu YouTube</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5392"/>
        <source>destroying process manager</source>
        <translation>niszczenie zarządcy procesów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5418"/>
        <source>destroying network access manager</source>
        <translation>niszczenie zarządcy dostępu do sieci</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5422"/>
        <source>so long and thanks for all the fish</source>
        <translation>cześć, i dzięki za ryby</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5395"/>
        <source>killing %n running emulator(s) on exit</source>
        <translation>
            <numerusform>zabijanie %n uruchomionego emulatora przy wyjściu</numerusform>
            <numerusform>zabijanie %n uruchomionych emulatorów przy wyjściu</numerusform>
            <numerusform>zabijanie %n uruchomionych emulatorów przy wyjściu</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5398"/>
        <source>keeping %n running emulator(s) alive</source>
        <translation>
            <numerusform>pozostawianie %n uruchomionego emulatora przy życiu</numerusform>
            <numerusform>pozostawianie %n uruchomionych emulatorów przy życiu</numerusform>
            <numerusform>pozostawianie %n uruchomionych emulatorów przy życiu</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5335"/>
        <source>destroying ROMAlyzer</source>
        <translation>niszczenie ROMAlyzera</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1713"/>
        <location filename="../../qmc2main.cpp" line="1785"/>
        <location filename="../../qmc2main.cpp" line="1807"/>
        <location filename="../../qmc2main.cpp" line="1997"/>
        <source>please wait for ROMAlyzer to finish the current analysis and try again</source>
        <translation>proszę poczekać, aż ROMAlyzer skończy bieżącą analizę i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2967"/>
        <source>F11</source>
        <translation>F11</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="179"/>
        <location filename="../../qmc2main.ui" line="311"/>
        <location filename="../../qmc2main.ui" line="446"/>
        <location filename="../../qmc2main.ui" line="581"/>
        <location filename="../../qmc2main.cpp" line="1091"/>
        <location filename="../../qmc2main.cpp" line="1131"/>
        <location filename="../../qmc2main.cpp" line="1163"/>
        <location filename="../../qmc2main.cpp" line="1188"/>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4525"/>
        <source>Are you sure you want to clear the favorites list?</source>
        <translation>Czy na pewno chcesz wyczyścić listę ulubionych?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4570"/>
        <source>Are you sure you want to clear the play history?</source>
        <translation>Czy na pewno chcesz wyczyścić historię gier?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3699"/>
        <location filename="../../qmc2main.cpp" line="3706"/>
        <location filename="../../qmc2main.cpp" line="3741"/>
        <location filename="../../qmc2main.cpp" line="3744"/>
        <source>No data available</source>
        <translation>Brak dostępnych danych</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1432"/>
        <location filename="../../qmc2main.ui" line="1435"/>
        <source>Detailed game information</source>
        <translation>Szczegółowe informacje o grze</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5359"/>
        <source>destroying game info DB</source>
        <translation>niszczenie bazy danych informacji o grach</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5809"/>
        <source>loading game info DB</source>
        <translation>wczytywanie bazy danych informacji o grach</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5918"/>
        <source>WARNING: missing &apos;$end&apos; in game info DB %1</source>
        <translation>UWAGA: brakujący &apos;$end&apos; w bazie danych informacji o grach %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5925"/>
        <source>WARNING: missing &apos;$bio&apos; in game info DB %1</source>
        <translation>UWAGA: brakujący &apos;$bio&apos; w bazie danych informacji o grach %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5932"/>
        <source>WARNING: missing &apos;$info&apos; in game info DB %1</source>
        <translation>UWAGA: brakujący &apos;$info&apos; w bazie danych informacji o grach %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5946"/>
        <source>WARNING: can&apos;t open game info DB %1</source>
        <translation>UWAGA: nie można otworzyć bazy danych informacji o grach %1</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5955"/>
        <source>%n game info record(s) loaded</source>
        <translation>
            <numerusform>załadowano %n rekord informacji o grach</numerusform>
            <numerusform>załadowano %n rekordy informacji o grach</numerusform>
            <numerusform>załadowano %n rekordów informacji o grach</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5954"/>
        <source>done (loading game info DB, elapsed time = %1)</source>
        <translation>ukończono (wczytywanie bazy danych informacji o grach, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4128"/>
        <source>FATAL: can&apos;t start XWININFO within a reasonable time frame, giving up</source>
        <translation>FATALNIE: nie udało się uruchomienie XWININFO w sensownym czasie, poddanie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5377"/>
        <source>destroying emulator info DB</source>
        <translation>niszczenie bazy danych informacji o emulatorze</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5992"/>
        <source>loading emulator info DB</source>
        <translation>wczytywanie bazy danych informacji o emulatorze</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6076"/>
        <source>WARNING: missing &apos;$end&apos; in emulator info DB %1</source>
        <translation>UWAGA: brakujący &apos;$end&apos; w bazie danych informacji o emulatorze %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6079"/>
        <source>WARNING: missing &apos;$mame&apos; in emulator info DB %1</source>
        <translation>UWAGA: brakujący &apos;$mame&apos; w bazie danych informacji o emulatorze %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6082"/>
        <source>WARNING: missing &apos;$info&apos; in emulator info DB %1</source>
        <translation>UWAGA: brakujący &apos;$info&apos; w bazie danych informacji o emulatorze %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6092"/>
        <source>WARNING: can&apos;t open emulator info DB %1</source>
        <translation>UWAGA: nie można otworzyć bazy danych informacji o emulatorze %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6095"/>
        <source>done (loading emulator info DB, elapsed time = %1)</source>
        <translation>ukończono (wczytywanie bazy danych informacji o emulatorze, miniony czas = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="6096"/>
        <source>%n emulator info record(s) loaded</source>
        <translation>
            <numerusform>załadowano %n rekord informacji o emulatorze</numerusform>
            <numerusform>załadowano %n rekordy informacji o emulatorze</numerusform>
            <numerusform>załadowano %n rekordów informacji o emulatorze</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1468"/>
        <location filename="../../qmc2main.ui" line="1471"/>
        <source>Detailed emulator information</source>
        <translation>Szczegółowe informacje o emulatorze</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1674"/>
        <source>E&amp;mulator control</source>
        <translation>Kontrola emu&amp;latora</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5957"/>
        <source>invalidating game info DB</source>
        <translation>unieważnianie bazy danych informacji o grach</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6098"/>
        <source>invalidating emulator info DB</source>
        <translation>unieważnianie bazy danych informacji o emulatorze</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2943"/>
        <source>F12</source>
        <translation>F12</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1399"/>
        <source>Pre&amp;view</source>
        <translation>&amp;Podgląd</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="184"/>
        <location filename="../../qmc2main.ui" line="316"/>
        <location filename="../../qmc2main.ui" line="451"/>
        <location filename="../../qmc2main.ui" line="586"/>
        <location filename="../../qmc2main.cpp" line="1093"/>
        <location filename="../../qmc2main.cpp" line="1133"/>
        <location filename="../../qmc2main.cpp" line="1165"/>
        <location filename="../../qmc2main.cpp" line="1190"/>
        <source>ROM types</source>
        <translation>Rodzaje ROM-ów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="189"/>
        <location filename="../../qmc2main.ui" line="321"/>
        <location filename="../../qmc2main.ui" line="456"/>
        <location filename="../../qmc2main.ui" line="591"/>
        <location filename="../../qmc2main.cpp" line="1095"/>
        <location filename="../../qmc2main.cpp" line="1135"/>
        <location filename="../../qmc2main.cpp" line="1167"/>
        <location filename="../../qmc2main.cpp" line="1192"/>
        <source>Players</source>
        <translation>Gracze</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="199"/>
        <location filename="../../qmc2main.ui" line="331"/>
        <location filename="../../qmc2main.ui" line="466"/>
        <location filename="../../qmc2main.ui" line="601"/>
        <location filename="../../qmc2main.cpp" line="1100"/>
        <location filename="../../qmc2main.cpp" line="1140"/>
        <location filename="../../qmc2main.cpp" line="1196"/>
        <source>Category</source>
        <translation>Kategoria</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="204"/>
        <location filename="../../qmc2main.ui" line="336"/>
        <location filename="../../qmc2main.ui" line="471"/>
        <location filename="../../qmc2main.ui" line="606"/>
        <location filename="../../qmc2main.cpp" line="1103"/>
        <location filename="../../qmc2main.cpp" line="1143"/>
        <location filename="../../qmc2main.cpp" line="1171"/>
        <source>Version</source>
        <translation>Wersja</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="259"/>
        <location filename="../../qmc2main.ui" line="391"/>
        <source>Loading game list, please wait...</source>
        <translation>Ładowanie listy gier, proszę czekać...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="414"/>
        <source>List of games viewed by category</source>
        <translation>Lista gier wyświetlana według kategorii</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="421"/>
        <location filename="../../qmc2main.cpp" line="1153"/>
        <source>Category / Game</source>
        <translation>Kategoria / Gra</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="549"/>
        <source>List of games viewed by version they were added to the emulator</source>
        <translation>Lista gier wyświetlana według wersji, w której zostały dodane do emulatora</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="556"/>
        <location filename="../../qmc2main.cpp" line="1178"/>
        <source>Version / Game</source>
        <translation>Wersja / Gra</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="759"/>
        <source>Pl&amp;ayed</source>
        <translation>&amp;Grane</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1790"/>
        <location filename="../../qmc2main.ui" line="1793"/>
        <location filename="../../qmc2main.ui" line="2979"/>
        <source>Previous track</source>
        <translation>Poprzednia ścieżka</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1804"/>
        <location filename="../../qmc2main.ui" line="1807"/>
        <location filename="../../qmc2main.ui" line="3000"/>
        <source>Next track</source>
        <translation>Następna ścieżka</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1818"/>
        <location filename="../../qmc2main.ui" line="1821"/>
        <location filename="../../qmc2main.ui" line="3021"/>
        <source>Fast backward</source>
        <translation>Przewijanie wstecz</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1841"/>
        <location filename="../../qmc2main.ui" line="1844"/>
        <location filename="../../qmc2main.ui" line="3039"/>
        <source>Fast forward</source>
        <translation>Przewijanie do przodu</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2013"/>
        <location filename="../../qmc2main.ui" line="2016"/>
        <source>Start playing automatically when QMC2 has started</source>
        <translation>Rozpocznij odtwarzanie automatycznie przy starcie QMC2</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2019"/>
        <source>Play on start</source>
        <translation>Odtwarzaj przy starcie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2026"/>
        <location filename="../../qmc2main.ui" line="2029"/>
        <source>Select random tracks from playlist</source>
        <translation>Wybierz losowe ścieżki z listy odtwarzania</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2032"/>
        <source>Shuffle</source>
        <translation>Wymieszaj</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2039"/>
        <location filename="../../qmc2main.ui" line="2042"/>
        <source>Automatically pause audio playback when at least one emulator is running</source>
        <translation>Automatycznie zatrzymaj odtwarzanie dźwięku kiedy chociaż jeden emulator jest uruchomiony</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2045"/>
        <source>Pause</source>
        <translation>Wstrzymaj</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2061"/>
        <source>Fade in/out</source>
        <translation>Pogłaśnianie / wyciszenie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2207"/>
        <source>Remove finished</source>
        <translation>Usuwanie skończonych</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2315"/>
        <source>&amp;Game</source>
        <translation>Gr&amp;a</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2985"/>
        <source>Ctrl+Alt+Left</source>
        <translation>Ctrl+Alt+Lewo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3006"/>
        <source>Ctrl+Alt+Right</source>
        <translation>Ctrl+Alt+Prawo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3027"/>
        <source>Ctrl+Alt+B</source>
        <translation>Ctrl+Alt+B</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3045"/>
        <source>Ctrl+Alt+F</source>
        <translation>Ctrl+Alt+F</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3066"/>
        <source>Ctrl+Alt+S</source>
        <translation>Ctrl+Alt+S</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3114"/>
        <source>Ctrl+Alt+P</source>
        <translation>Ctrl+Alt+P</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1915"/>
        <location filename="../../qmc2main.ui" line="1918"/>
        <source>Progress indicator for current track</source>
        <translation>Wskaźnik postępu dla bieżącej ścieżki</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1924"/>
        <location filename="../../qmc2main.cpp" line="6560"/>
        <location filename="../../qmc2main.cpp" line="6570"/>
        <source>%vs (%ms total)</source>
        <translation>%vs (%ms w sumie)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3090"/>
        <source>Ctrl+Alt+#</source>
        <translation>Ctrl+Alt+#</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1864"/>
        <location filename="../../qmc2main.ui" line="1867"/>
        <location filename="../../qmc2main.ui" line="3060"/>
        <source>Stop track</source>
        <translation>Zatrzymaj ścieżkę</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1881"/>
        <location filename="../../qmc2main.ui" line="1884"/>
        <location filename="../../qmc2main.ui" line="3084"/>
        <source>Pause track</source>
        <translation>Wstrzymaj ścieżkę</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1898"/>
        <location filename="../../qmc2main.ui" line="1901"/>
        <location filename="../../qmc2main.ui" line="3108"/>
        <source>Play track</source>
        <translation>Odtwarzaj ścieżkę</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1692"/>
        <location filename="../../qmc2main.ui" line="1695"/>
        <source>Emulator control panel</source>
        <translation>Panel kontrolny emulatora</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1705"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1730"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1715"/>
        <location filename="../../qmc2main.ui" line="2140"/>
        <source>Status</source>
        <translation>Stan</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1720"/>
        <source>LED0</source>
        <translation>LED0</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1725"/>
        <source>LED1</source>
        <translation>LED1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1735"/>
        <source>Command</source>
        <translation>Polecenie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6884"/>
        <location filename="../../qmc2main.cpp" line="6951"/>
        <location filename="../../qmc2main.cpp" line="6961"/>
        <source>running</source>
        <translation>uruchomiony</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6900"/>
        <source>stopped</source>
        <translation>zatrzymany</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4157"/>
        <location filename="../../qmc2main.cpp" line="6945"/>
        <location filename="../../qmc2main.cpp" line="6959"/>
        <source>paused</source>
        <translation>wstrzymany</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="733"/>
        <location filename="../../qmc2main.cpp" line="4196"/>
        <source>Copy emulator command line to clipboard</source>
        <translation>Skopiuj wiersz poleceń emulatora do schowka</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="734"/>
        <location filename="../../qmc2main.cpp" line="4197"/>
        <source>&amp;Copy command</source>
        <translation>S&amp;kopiuj polecenie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6761"/>
        <source>WARNING: can&apos;t create SDLMAME output notifier FIFO, path = %1</source>
        <translation>UWAGA: nie można otworzyć FIFO powiadamiacza wyjścia SDLMAME, ścieżka = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6789"/>
        <source>SDLMAME output notifier FIFO created</source>
        <translation>Utworzono FIFO powiadamiacza wyjścia SDLMAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6793"/>
        <location filename="../../qmc2main.cpp" line="6796"/>
        <source>WARNING: can&apos;t open SDLMAME output notifier FIFO for reading, path = %1</source>
        <translation>UWAGA: nie można otworzyć FIFO powiadamiacza wyjścia SDLMAME do odczytu, ścieżka = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6917"/>
        <location filename="../../qmc2main.cpp" line="6988"/>
        <source>unhandled MAME output notification: game = %1, class = %2, what = %3, state = %4</source>
        <translation>nieobsługiwane powiadomienie wyjścia MAME: gra = %1, klasa = %2, co = %3, stan = %4</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1710"/>
        <source>Game / Notifier</source>
        <translation>Gra / Powiadomienie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2274"/>
        <source>&amp;Clean up</source>
        <translation>&amp;Wyczyść</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2256"/>
        <source>&amp;Audio player</source>
        <translation>Odtwarzacz &amp;dźwięku</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2550"/>
        <source>&amp;About...</source>
        <translation>&amp;O programie...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2571"/>
        <source>About &amp;Qt...</source>
        <translation>O &amp;Qt...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2668"/>
        <source>Recreate template map</source>
        <translation>Odtwórz mapę szablonu</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2710"/>
        <source>Full detail</source>
        <translation>Wszystkie szczegóły</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2832"/>
        <source>Analyse ROM...</source>
        <translation>Analizuj ROM...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2731"/>
        <source>Parent / clones</source>
        <translation>Macierzyste / klony</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="94"/>
        <source>Parent / clone hierarchy (not filtered)</source>
        <translation>Hierarchia macierzysty / klon (niefiltrowana)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="282"/>
        <source>Parent / clone hierarchy</source>
        <translation>Hierarchia macierzysty / klon</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="526"/>
        <source>Creating category view, please wait...</source>
        <translation>Tworzenie widoku kategorii, proszę czekać...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="661"/>
        <source>Creating version view, please wait...</source>
        <translation>Tworzenie widoku wersji, proszę czekać...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2734"/>
        <location filename="../../qmc2main.ui" line="2737"/>
        <source>View parent / clone hierarchy</source>
        <translation>Zobacz hierarchię macierzysty / klon</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2982"/>
        <source>Play previous track</source>
        <translation>Odtwarzaj poprzednią ścieżkę</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3003"/>
        <source>Play next track</source>
        <translation>Odtwarzaj następną ścieżkę</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3024"/>
        <source>Fast backward within track</source>
        <translation>Przewijaj do tyłu w ramach ścieżki</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3063"/>
        <source>Stop current track</source>
        <translation>Zatrzymaj bieżącą ścieżkę</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3087"/>
        <source>Pause current track</source>
        <translation>Wstrzymaj bieżącą ścieżkę</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3111"/>
        <source>Play current track</source>
        <translation>Odtwarzaj bieżącą ścieżkę</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3042"/>
        <source>Fast forward within track</source>
        <translation>Przewijaj do przodu w ramach ścieżki</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6383"/>
        <source>Select one or more audio files</source>
        <translation>Wybierz jeden lub więcej plików dźwiękowych</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1954"/>
        <location filename="../../qmc2main.ui" line="1957"/>
        <source>Browse for tracks to add to playlist</source>
        <translation>Wskaż ścieżki do dodania do listy odtwarzania</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1985"/>
        <location filename="../../qmc2main.ui" line="1988"/>
        <source>Remove selected tracks from playlist</source>
        <translation>Usuń wybrane ścieżki z listy odtwarzania</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1933"/>
        <location filename="../../qmc2main.ui" line="1936"/>
        <location filename="../../qmc2main.ui" line="2071"/>
        <location filename="../../qmc2main.ui" line="2074"/>
        <source>Audio player volume</source>
        <translation>Głośność odtwarzacza dźwięku</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2055"/>
        <location filename="../../qmc2main.ui" line="2058"/>
        <source>Fade in and out on pause / resume</source>
        <translation>Pogłaśniaj i sciszaj przy wstrzymaniu / wznowieniu </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6682"/>
        <source>audio player: track info: title = &apos;%1&apos;, artist = &apos;%2&apos;, album = &apos;%3&apos;, genre = &apos;%4&apos;</source>
        <translation>odtwarzacz dźwięku: informacje o ścieżce: tytuł = &apos;%1&apos;, wykonawca = &apos;%2&apos;, album = &apos;%3&apos;, gatunek = &apos;%4&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3125"/>
        <source>Raise volume</source>
        <translation>Zwiększ głośność</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3128"/>
        <source>Raise audio player volume</source>
        <translation>Zwiększ głośność odtwarzacza dźwięku</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3131"/>
        <source>Ctrl+Alt+PgUp</source>
        <translation>Ctrl+Alt+PgUp</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3139"/>
        <source>Lower volume</source>
        <translation>Zmniejsz głośność</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3142"/>
        <source>Lower audio player volume</source>
        <translation>Zmniejsz głośność odtwarzacza dźwięku</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3145"/>
        <source>Ctrl+Alt+PgDown</source>
        <translation>Ctrl+Alt+PgDown</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3157"/>
        <source>&amp;Export ROM status...</source>
        <translation>&amp;Eksportuj stan ROM-ów...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3160"/>
        <location filename="../../qmc2main.ui" line="3163"/>
        <source>Export ROM status</source>
        <translation>Eksportuj stan ROM-ów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3166"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5340"/>
        <source>destroying ROM status exporter</source>
        <translation>niszczenie eksportera stanu ROM-ów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6773"/>
        <source>WARNING: can&apos;t create SDLMESS output notifier FIFO, path = %1</source>
        <translation>UWAGA: nie można otworzyć FIFO powiadamiacza wyjścia SDLMESS, ścieżka = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6800"/>
        <source>SDLMESS output notifier FIFO created</source>
        <translation>Utworzono FIFO powiadamiacza wyjścia SDLMESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6804"/>
        <location filename="../../qmc2main.cpp" line="6807"/>
        <source>WARNING: can&apos;t open SDLMESS output notifier FIFO for reading, path = %1</source>
        <translation>UWAGA: nie można otworzyć FIFO powiadamiacza wyjścia SDLMESS do odczytu, ścieżka = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6919"/>
        <location filename="../../qmc2main.cpp" line="6990"/>
        <source>unhandled MESS output notification: game = %1, class = %2, what = %3, state = %4</source>
        <translation>nieobsługiwane powiadomienie wyjścia MESS: gra = %1, klasa = %2, co = %3, stan = %4</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="528"/>
        <location filename="../../qmc2main.cpp" line="1080"/>
        <source>Machine / Attribute</source>
        <translation>Maszyna / Atrybut</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="529"/>
        <location filename="../../qmc2main.cpp" line="1120"/>
        <source>Machine / Clones</source>
        <translation>Maszyna / Klony</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="531"/>
        <source>Machine &amp;info</source>
        <translation>&amp;Informacje o maszynie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1417"/>
        <source>Game &amp;info</source>
        <translation>&amp;Informacje o grze</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="55"/>
        <source>&amp;Game list</source>
        <translation>&amp;Lista gier</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="534"/>
        <location filename="../../qmc2main.cpp" line="535"/>
        <source>Favorite machines</source>
        <translation>Ulubione maszyny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="536"/>
        <location filename="../../qmc2main.cpp" line="537"/>
        <source>Machines last played</source>
        <translation>Ostatnio uruchamiane maszyny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="532"/>
        <location filename="../../qmc2main.cpp" line="533"/>
        <source>Detailed machine info</source>
        <translation>Szczegółowe informacje o maszynie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="556"/>
        <source>Machine / Notifier</source>
        <translation>Maszyna / Powiadomienie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="538"/>
        <location filename="../../qmc2main.cpp" line="539"/>
        <source>Play current machine</source>
        <translation>Graj na bieżącej maszynie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="558"/>
        <location filename="../../qmc2main.cpp" line="559"/>
        <location filename="../../qmc2main.cpp" line="765"/>
        <location filename="../../qmc2main.cpp" line="817"/>
        <location filename="../../qmc2main.cpp" line="928"/>
        <source>Add current machine to favorites</source>
        <translation>Dodaj bieżącą maszynę do ulubionych</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="562"/>
        <location filename="../../qmc2main.cpp" line="563"/>
        <source>Reload entire machine list</source>
        <translation>Przeładuj całą listę maszyn</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="564"/>
        <location filename="../../qmc2main.cpp" line="565"/>
        <source>View machine list with full detail</source>
        <translation>Oglądaj listę maszyn ze wszystkimi szczegółami</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="570"/>
        <location filename="../../qmc2main.cpp" line="571"/>
        <location filename="../../qmc2main.cpp" line="775"/>
        <location filename="../../qmc2main.cpp" line="827"/>
        <location filename="../../qmc2main.cpp" line="870"/>
        <location filename="../../qmc2main.cpp" line="938"/>
        <source>Check current machine&apos;s ROM state</source>
        <translation>Sprawdź stan ROM-u bieżącej maszyny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="572"/>
        <location filename="../../qmc2main.cpp" line="573"/>
        <source>Analyse current machine with ROMAlyzer</source>
        <translation>Analizuj bieżącą maszynę za pomocą ROMAlyzera</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="574"/>
        <source>M&amp;achine</source>
        <translation>M&amp;aszyna</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="575"/>
        <source>Machine list with full detail (filtered)</source>
        <translation>Lista maszyn ze wszystkimi szczegółami (filtrowana)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="576"/>
        <location filename="../../qmc2main.cpp" line="577"/>
        <source>Select between detailed machine list and parent / clone hierarchy</source>
        <translation>Wybierz spośród szczegółowej listy maszyn i hierarchii macierzysty / klon</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="78"/>
        <location filename="../../qmc2main.ui" line="81"/>
        <source>Switch between detailed game list and parent / clone hierarchy</source>
        <translation>Wybierz spośród szczegółowej listy gier i hierarchii macierzysty / klon</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="85"/>
        <source>Game list with full detail (filtered)</source>
        <translation>Lista gier ze wszystkimi szczegółami (filtrowana)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2637"/>
        <location filename="../../qmc2main.ui" line="2640"/>
        <source>Reload entire game list</source>
        <translation>Przeładuj całą listę gier</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2671"/>
        <location filename="../../qmc2main.ui" line="2674"/>
        <source>Recreate template configuration map</source>
        <translation>Odtwórz szablonową mapę konfiguracji</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2713"/>
        <location filename="../../qmc2main.ui" line="2716"/>
        <source>View game list with full detail</source>
        <translation>Oglądaj listę gier ze wszystkimi szczegółami</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2838"/>
        <location filename="../../qmc2main.ui" line="2841"/>
        <source>Analyse current game with ROMAlyzer</source>
        <translation>Analizuj bieżącą grę za pomocą ROMAlyzera</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5234"/>
        <source>destroying current machine&apos;s emulator configuration</source>
        <translation>niszczenie konfiguracji emulatora bieżącej maszyny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="578"/>
        <location filename="../../qmc2main.cpp" line="579"/>
        <location filename="../../qmc2main.cpp" line="584"/>
        <source>Machine status indicator</source>
        <translation>Wskaźnik stanu maszyny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1366"/>
        <location filename="../../qmc2main.ui" line="1369"/>
        <source>Game status indicator</source>
        <translation>Wskaźnik stanu gry</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="853"/>
        <source>&lt;b&gt;&lt;font color=black&gt;L:?&lt;/font&gt; &lt;font color=#00cc00&gt;C:?&lt;/font&gt; &lt;font color=#a2c743&gt;M:?&lt;/font&gt; &lt;font color=#f90000&gt;I:?&lt;/font&gt; &lt;font color=#7f7f7f&gt;N:?&lt;/font&gt; &lt;font color=#0000f9&gt;U:?&lt;/font&gt; &lt;font color=chocolate&gt;S:?&lt;/font&gt;&lt;/b&gt;</source>
        <translation>&lt;b&gt;&lt;font color=black&gt;W:?&lt;/font&gt; &lt;font color=#00cc00&gt;P:?&lt;/font&gt; &lt;font color=#a2c743&gt;Wp:?&lt;/font&gt; &lt;font color=#f90000&gt;Np:?&lt;/font&gt; &lt;font color=#7f7f7f&gt;Nz:?&lt;/font&gt; &lt;font color=#0000f9&gt;Nn:?&lt;/font&gt; &lt;font color=chocolate&gt;S:?&lt;/font&gt;&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="585"/>
        <source>Show vertical machine status indicator in machine details</source>
        <translation>Pokaż pionowy wskaźnik stanu maszyny wśród szczegółów maszyny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="586"/>
        <source>Show the machine status indicator only when the machine list is not visible due to the current layout</source>
        <translation>Pokaż wskaźnik stanu maszyny tylko gdy lista maszyn nie jest widoczna w bieżącym układzie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="587"/>
        <source>Show machine name</source>
        <translation>Pokaż nazwę maszyny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="589"/>
        <source>Show machine&apos;s description only when the machine list is not visible due to the current layout</source>
        <translation>Pokaż opis maszyny tylko gdy lista maszyn nie jest widoczna w bieżącym układzie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5224"/>
        <source>saving current machine&apos;s device configurations</source>
        <translation>zapisywanie konfiguracji urządzeń bieżącej maszyny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1562"/>
        <source>No devices</source>
        <translation>Brak urządzeń</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1282"/>
        <source>&amp;Correct</source>
        <translation>&amp;Poprawny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1288"/>
        <source>&amp;Mostly correct</source>
        <translation>&amp;W większości poprawny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1294"/>
        <source>&amp;Incorrect</source>
        <translation>&amp;Niepoprawny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1300"/>
        <source>&amp;Not found</source>
        <translation>Nie&amp;znaleziony</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1306"/>
        <source>&amp;Unknown</source>
        <translation>Nieznan&amp;y</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="112"/>
        <location filename="../../qmc2main.ui" line="115"/>
        <source>Toggle individual ROM states</source>
        <translation>Przełącz poszczególne stany ROM-ów</translation>
    </message>
    <message>
        <location filename="../../macros.h" line="427"/>
        <location filename="../../macros.h" line="433"/>
        <location filename="../../qmc2main.cpp" line="527"/>
        <source>M.E.S.S. Catalog / Launcher II</source>
        <translation>M.E.S.S. Catalog / Launcher II</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3178"/>
        <source>QMC2 for SDLMAME</source>
        <translation>QMC2 dla SDLMAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3181"/>
        <location filename="../../qmc2main.ui" line="3184"/>
        <source>Launch QMC2 for SDLMAME</source>
        <translation>Uruchom QMC2 dla SDLMAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3199"/>
        <source>QMC2 for SDLMESS</source>
        <translation>QMC2 dla SDLMESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3202"/>
        <location filename="../../qmc2main.ui" line="3205"/>
        <source>Launch QMC2 for SDLMESS</source>
        <translation>Uruchom QMC2 dla SDLMESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3187"/>
        <source>Ctrl+Alt+1</source>
        <translation>Ctrl+Alt+1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3208"/>
        <source>Ctrl+Alt+2</source>
        <translation>Ctrl+Alt+2</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2451"/>
        <location filename="../../qmc2main.cpp" line="2453"/>
        <location filename="../../qmc2main.cpp" line="2552"/>
        <location filename="../../qmc2main.cpp" line="2554"/>
        <source>variant &apos;%1&apos; launched</source>
        <translation>uruchomiono wariant &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2491"/>
        <location filename="../../qmc2main.cpp" line="2493"/>
        <location filename="../../qmc2main.cpp" line="2592"/>
        <location filename="../../qmc2main.cpp" line="2594"/>
        <source>WARNING: failed to launch variant &apos;%1&apos;</source>
        <translation>UWAGA: nie udało się uruchomić wariantu &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="582"/>
        <location filename="../../qmc2main.cpp" line="583"/>
        <source>Progress indicator for machine list processing</source>
        <translation>Wskaźnik postępu przetwarzania listy maszyn</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="866"/>
        <location filename="../../qmc2main.ui" line="869"/>
        <source>Progress indicator for game list processing</source>
        <translation>Wskaźnik postępu przetwarzania listy gier</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2655"/>
        <location filename="../../qmc2main.cpp" line="2674"/>
        <source>WARNING: this feature is not yet working!</source>
        <translation>UWAGA: ta funkcja jeszcze nie działa!</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5196"/>
        <source>destroying arcade view</source>
        <translation>niszczenie widoku salonowego</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2386"/>
        <source>&amp;Arcade</source>
        <translation>Tryb &amp;salonowy</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2904"/>
        <location filename="../../qmc2main.cpp" line="1016"/>
        <source>&amp;Setup...</source>
        <translation>&amp;Ustawienia...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2907"/>
        <location filename="../../qmc2main.ui" line="2910"/>
        <source>Setup arcade mode</source>
        <translation>Ustawienia trybu salonowego</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2916"/>
        <source>Ctrl+Shift+A</source>
        <translation>Ctrl+Shift+A</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5201"/>
        <source>destroying arcade setup dialog</source>
        <translation>niszczenie okna dialogowego trybu salonowego</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5250"/>
        <source>destroying game list</source>
        <translation>niszczenie listy gier</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5252"/>
        <source>destroying machine list</source>
        <translation>niszczenie listy maszyn</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7645"/>
        <source>ArcadeView is not currently active, can&apos;t take screen shot</source>
        <translation>ArcadeView nie jest obecnie aktywny, nie można pobrać zrzutu ekranu</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3219"/>
        <source>Show FPS</source>
        <translation>Pokaż FPS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3222"/>
        <location filename="../../qmc2main.ui" line="3225"/>
        <source>Toggle FPS display</source>
        <translation>Przełącz wyświetlanie FPS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3228"/>
        <source>Meta+F</source>
        <translation>Meta+F</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3240"/>
        <source>Screen shot</source>
        <translation>Zrzut ekranu</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3243"/>
        <source>Save a screen shot from the current arcade scene</source>
        <translation>Zapisz zrzut ekranu obecnego salonu</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3246"/>
        <source>Take screen shot from arcade scene</source>
        <translation>Pobierz zrzut ekranu salonu</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3249"/>
        <source>Meta+F12</source>
        <translation>Meta+F12</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2931"/>
        <source>&amp;Toggle arcade</source>
        <translation>Przełącz &amp;salon</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2934"/>
        <location filename="../../qmc2main.ui" line="2937"/>
        <source>Toggle arcade mode</source>
        <translation>Przełącz tryb salonowy</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5361"/>
        <source>destroying machine info DB</source>
        <translation>niszczenie bazy danych informacji o maszynach</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5811"/>
        <source>loading machine info DB</source>
        <translation>wczytywanie bazy danych informacji o maszynach</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5920"/>
        <source>WARNING: missing &apos;$end&apos; in machine info DB %1</source>
        <translation>UWAGA: brakujący &apos;$end&apos; w bazie danych informacji o maszynach %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5927"/>
        <source>WARNING: missing &apos;$bio&apos; in machine info DB %1</source>
        <translation>UWAGA: brakujący &apos;$bio&apos; w bazie danych informacji o maszynach %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5934"/>
        <source>WARNING: missing &apos;$info&apos; in machine info DB %1</source>
        <translation>UWAGA: brakujący &apos;$info&apos; w bazie danych informacji o maszynach %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5948"/>
        <source>WARNING: can&apos;t open machine info DB %1</source>
        <translation>UWAGA: nie można otworzyć bazy danych informacji o maszynach %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5959"/>
        <source>done (loading machine info DB, elapsed time = %1)</source>
        <translation>ukończono (wczytywanie bazy danych informacji o maszynach, miniony czas = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5960"/>
        <source>%n machine info record(s) loaded</source>
        <translation>
            <numerusform>załadowano %n rekord informacji o maszynach</numerusform>
            <numerusform>załadowano %n rekordy informacji o maszynach</numerusform>
            <numerusform>załadowano %n rekordów informacji o maszynach</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5962"/>
        <source>invalidating machine info DB</source>
        <translation>unieważnianie bazy danych informacji o maszynach</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1596"/>
        <source>&amp;Front end log</source>
        <translation>Dziennik i&amp;nterfejsu</translation>
    </message>
    <message>
        <source>L:Listed - C:Correct - M:Mostly correct - I:Incorrect - N:Not found - U:Unknown - S:Search</source>
        <translation type="obsolete">W:Wymieniony - P:Poprawny - Wp:W większości poprawny - Np:Niepoprawny - Nz:Nieznaleziony - Nn:Nieznany - S:Szukany</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="443"/>
        <source>QMC2 for MAME</source>
        <translation>QMC2 dla MAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="444"/>
        <location filename="../../qmc2main.cpp" line="445"/>
        <source>Launch QMC2 for MAME</source>
        <translation>Uruchom QMC2 dla MAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="446"/>
        <source>QMC2 for MESS</source>
        <translation>QMC2 dla MESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="447"/>
        <location filename="../../qmc2main.cpp" line="448"/>
        <source>Launch QMC2 for MESS</source>
        <translation>Uruchom QMC2 dla MESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2958"/>
        <source>Toggle &amp;full screen</source>
        <translation>Przełącz tryb &amp;pełnoekranowy</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2961"/>
        <source>Toggle full screen</source>
        <translation>Przełącz tryb pełnoekranowy</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2964"/>
        <source>Toggle full screen / windowed mode</source>
        <translation>Przełącz pomiędzy trybem pełnoekranowym a oknem</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="743"/>
        <location filename="../../qmc2main.cpp" line="795"/>
        <location filename="../../qmc2main.cpp" line="847"/>
        <location filename="../../qmc2main.cpp" line="906"/>
        <source>Start selected machine</source>
        <translation>Uruchom wybraną maszynę</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="782"/>
        <location filename="../../qmc2main.cpp" line="834"/>
        <location filename="../../qmc2main.cpp" line="877"/>
        <location filename="../../qmc2main.cpp" line="945"/>
        <source>Analyse current game&apos;s ROM set with ROMAlyzer</source>
        <translation>Analizuj zestaw ROM-ów bieżącej gry za pomocą ROMAlyzera</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="784"/>
        <location filename="../../qmc2main.cpp" line="836"/>
        <location filename="../../qmc2main.cpp" line="879"/>
        <location filename="../../qmc2main.cpp" line="947"/>
        <source>Analyse current machine&apos;s ROM set with ROMAlyzer</source>
        <translation>Analizuj zestaw ROM-ów bieżącej maszyny za pomocą ROMAlyzera</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="972"/>
        <location filename="../../qmc2main.cpp" line="994"/>
        <location filename="../../qmc2main.cpp" line="1022"/>
        <source>Set tab position north</source>
        <translation>Ustaw położenie kart na północ</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="973"/>
        <location filename="../../qmc2main.cpp" line="995"/>
        <location filename="../../qmc2main.cpp" line="1023"/>
        <source>&amp;North</source>
        <translation>&amp;Północ</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="977"/>
        <location filename="../../qmc2main.cpp" line="999"/>
        <location filename="../../qmc2main.cpp" line="1027"/>
        <source>Set tab position south</source>
        <translation>Ustaw położenie kart na południe</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="978"/>
        <location filename="../../qmc2main.cpp" line="1000"/>
        <location filename="../../qmc2main.cpp" line="1028"/>
        <source>&amp;South</source>
        <translation>P&amp;ołudnie</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="982"/>
        <location filename="../../qmc2main.cpp" line="1004"/>
        <location filename="../../qmc2main.cpp" line="1032"/>
        <source>Set tab position west</source>
        <translation>Ustaw położenie kart na zachód</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="983"/>
        <location filename="../../qmc2main.cpp" line="1005"/>
        <location filename="../../qmc2main.cpp" line="1033"/>
        <source>&amp;West</source>
        <translation>&amp;Zachód</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="987"/>
        <location filename="../../qmc2main.cpp" line="1009"/>
        <location filename="../../qmc2main.cpp" line="1037"/>
        <source>Set tab position east</source>
        <translation>Ustaw położenie kart na wschód</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="988"/>
        <location filename="../../qmc2main.cpp" line="1010"/>
        <location filename="../../qmc2main.cpp" line="1038"/>
        <source>&amp;East</source>
        <translation>&amp;Wschód</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="588"/>
        <source>Show machine&apos;s description at the bottom of any images</source>
        <translation>Pokaż opis maszyny na dole obrazów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5286"/>
        <source>destroying cabinet</source>
        <translation>niszczenie automatu</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5290"/>
        <source>destroying controller</source>
        <translation>niszczenie kontrolera</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1453"/>
        <source>Em&amp;ulator info</source>
        <translation>Informacje o &amp;emulatorze</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1507"/>
        <source>Ca&amp;binet</source>
        <translation>Auto&amp;mat</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1516"/>
        <source>C&amp;ontroller</source>
        <translation>K&amp;ontroler</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1534"/>
        <source>Titl&amp;e</source>
        <translation>Ekran &amp;tytułowy</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5294"/>
        <source>destroying marquee</source>
        <translation>niszczenie planszy tytułowej</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5298"/>
        <source>destroying title</source>
        <translation>niszczenie ekranu tytułowego</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Wartość</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1015"/>
        <source>Detail setup</source>
        <translation>Ustawienia szczegółów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5345"/>
        <source>destroying detail setup</source>
        <translation>niszczenie ustawień szczegółów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3502"/>
        <source>Fetching MAWS page for &apos;%1&apos;, please wait...</source>
        <translation>Pobieranie strony MAWS dla &apos;%1&apos;, proszę czekać...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3464"/>
        <source>MAWS page for &apos;%1&apos;</source>
        <translation>Strona MAWS dla &apos;%1&apos;</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2046"/>
        <source>freed %n byte(s) in %1</source>
        <translation>
            <numerusform>uwolniono %n bajt w %1</numerusform>
            <numerusform>uwolniono %n bajty w %1</numerusform>
            <numerusform>uwolniono %n bajtów w %1</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2046"/>
        <source>%n entry(s)</source>
        <translation>
            <numerusform>%n wpis</numerusform>
            <numerusform>%n wpisy</numerusform>
            <numerusform>%n wpisów</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2048"/>
        <source>MAWS in-memory cache cleared (%1)</source>
        <translation>Wyczyszczono pamięciowy bufor MAWS (%1)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3257"/>
        <location filename="../../qmc2main.ui" line="3260"/>
        <source>Clear MAWS cache</source>
        <translation>Wyczyść bufor MAWS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3263"/>
        <source>Ctrl+M</source>
        <translation>Ctrl+M</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2064"/>
        <location filename="../../qmc2main.cpp" line="2091"/>
        <source>removed %n byte(s) in %1</source>
        <translation>
            <numerusform>usunięto %n bajt w %1</numerusform>
            <numerusform>usunięto %n bajty w %1</numerusform>
            <numerusform>usunięto %n bajtów w %1</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2064"/>
        <location filename="../../qmc2main.cpp" line="2091"/>
        <source>%n file(s)</source>
        <translatorcomment>%n plików</translatorcomment>
        <translation>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2065"/>
        <source>MAWS on-disk cache cleared (%1)</source>
        <translation>Wyczyszczono dyskowy bufor MAWS (%1)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2112"/>
        <location filename="../../qmc2main.ui" line="2115"/>
        <source>List of active/inactive downloads</source>
        <translation>Lista aktywnych/nieaktywnych pobierań</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2145"/>
        <source>Progress</source>
        <translation>Postęp</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2153"/>
        <location filename="../../qmc2main.ui" line="2156"/>
        <source>Clear finished / stopped downloads from list</source>
        <translation>Usuń skończone / zatrzymane pobierania z listy</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2170"/>
        <location filename="../../qmc2main.ui" line="2173"/>
        <source>Reload selected downloads</source>
        <translation>Odśwież wybrane pobierania</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2187"/>
        <location filename="../../qmc2main.ui" line="2190"/>
        <source>Stop selected downloads</source>
        <translation>Zatrzymaj wybrane pobierania</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5310"/>
        <source>destroying MiniWebBrowser</source>
        <translation>niszczenie MiniWebBrowser</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5315"/>
        <source>destroying MAWS lookup</source>
        <translation>niszczenie odwołania MAWS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8155"/>
        <source>Choose file to store download</source>
        <translation>Wybierz plik do zapisania pobierania</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5554"/>
        <source>loading style sheet &apos;%1&apos;</source>
        <translation>ładowanie arkusza stylu &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5563"/>
        <source>FATAL: can&apos;t open style sheet file &apos;%1&apos;, please check</source>
        <translation>FATALNIE: nie można otworzyć pliku arkusza stylu &apos;%1&apos;, proszę sprawdzić</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5566"/>
        <source>removing current style sheet</source>
        <translation>usuwanie bieżącego arkusza stylu</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7790"/>
        <source>Quick download links for MAWS data usable by QMC2</source>
        <translation>Szybko pobieraj odnośniki do danych MAWS użytecznych dla QMC2</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7952"/>
        <source>Setup...</source>
        <translation>Ustawienia...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7815"/>
        <source>Cabinet art</source>
        <translation>Oprawa graficzna automatu</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="286"/>
        <location filename="../../qmc2main.cpp" line="300"/>
        <source>last message repeated %n time(s)</source>
        <translation>
            <numerusform>ostatnia wiadomość powtarza się %n raz</numerusform>
            <numerusform>ostatnia wiadomość powtarza się %n razy</numerusform>
            <numerusform>ostatnia wiadomość powtarza się %n razy</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="467"/>
        <source>Toggle automatic pausing of embedded emulators (hold down for menu)</source>
        <translation>Włącz/wyłącz automatyczne pauzowanie osadzonych emulatorów (przytrzymaj aby pokazać menu)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="476"/>
        <source>Scan the pause key used by the emulator</source>
        <translation>Skanuj klawisz pauzujący emulator</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="477"/>
        <source>Scan pause key...</source>
        <translation>Skanowanie klawisza pauzy...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="521"/>
        <source>Game</source>
        <translation>Gra</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="540"/>
        <location filename="../../qmc2main.cpp" line="541"/>
        <source>Play all tagged machines</source>
        <translation>Graj na wszystkich zaznaczonych maszynach</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="542"/>
        <location filename="../../qmc2main.cpp" line="543"/>
        <source>Clear machine list cache</source>
        <translation>Wyczyść bufor listy maszyn</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="544"/>
        <location filename="../../qmc2main.cpp" line="545"/>
        <source>Forcedly clear (remove) the machine list cache</source>
        <translation>Wymuś wyczyszczenie (usunięcie) bufora listy maszyn</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="546"/>
        <source>List of all supported machines</source>
        <translation>Lista wszystkich obsługiwanych maszyn</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="550"/>
        <location filename="../../qmc2main.cpp" line="551"/>
        <source>Play all tagged machines (embedded)</source>
        <translation>Graj osadzając na wszystkich zaznaczonych maszynach</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="554"/>
        <source>Machine</source>
        <translation>Maszyna</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="560"/>
        <location filename="../../qmc2main.cpp" line="561"/>
        <source>Add all tagged machines to favorites</source>
        <translation>Dodaj wszystkie zaznaczone maszyny do ulubionych</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1719"/>
        <source>game list reload is already active</source>
        <translation>przeładowywanie listy gier jest już uruchomione</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1721"/>
        <source>machine list reload is already active</source>
        <translation>przeładowywanie listy maszyn jest już uruchomione</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4171"/>
        <location filename="../../qmc2main.cpp" line="4172"/>
        <source>Toggle embedder options (hold down for menu)</source>
        <translation>Przełącz opcje osadzacza (przytrzymaj dla menu)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4179"/>
        <source>To favorites</source>
        <translation>Do ulubionych</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4185"/>
        <source>Terminate emulator</source>
        <translation>Zakończ emulator</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4186"/>
        <source>&amp;Terminate emulator</source>
        <translation>Za&amp;kończ emulator</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4190"/>
        <source>Kill emulator</source>
        <translation>Usuń emulator</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4191"/>
        <source>&amp;Kill emulator</source>
        <translation>&amp;Usuń emulator</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4219"/>
        <source>Couldn&apos;t find the window ID of one or more
emulator(s) within a reasonable timeframe.

Retry embedding?</source>
        <translation>Nie udało się odnaleźć ID okna jednego lub większej
liczby emulatorów w sensownym czasie.

Ponowić próbę osadzania?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4229"/>
        <source>Information</source>
        <translation>Informacja</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4229"/>
        <source>Sorry, the emulator meanwhile died a sorrowful death :(.</source>
        <translation>Przykro mi, w międzyczasie emulator zginął bolesną śmiercią :(.</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5260"/>
        <source>destroying audio effects dialog</source>
        <translation>niszczenie okna dialogowego efektów dźwięku</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5745"/>
        <source>loading YouTube video info map</source>
        <translation>ładowanie mapy informacji o filmie YouTube</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5762"/>
        <source>YouTube index - %p%</source>
        <translation>Indeks YouTube - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5791"/>
        <source>done (loading YouTube video info map)</source>
        <translation>ukończono (ładowanie mapy informacji o filmie YouTube)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5792"/>
        <source>%n video info record(s) loaded</source>
        <translation>
            <numerusform>załadowano %n rekord informacji o filmie</numerusform>
            <numerusform>załadowano %n rekordy informacji o filmie</numerusform>
            <numerusform>załadowano %n rekordów informacji o filmie</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5843"/>
        <source>Game info - %p%</source>
        <translation>Informacje o grze - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5845"/>
        <source>Machine info - %p%</source>
        <translation>Informacje o maszynie - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6021"/>
        <source>Emu info - %p%</source>
        <translation>Informacje o emulatorze - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6395"/>
        <source>Add URL</source>
        <translation>Dodaj adres URL</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6395"/>
        <source>Enter valid MP3 stream URL:</source>
        <translation>Wprowadź poprawny adres URL strumienia MP3:</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6696"/>
        <source>Buffering %p%</source>
        <translation>Buforowanie %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8067"/>
        <source>Cabinet</source>
        <translation>Automat</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8069"/>
        <source>Controller</source>
        <translation>Kontroler</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8075"/>
        <source>PCB</source>
        <translation>Płytka drukowana</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8071"/>
        <source>Flyer</source>
        <translation>Ulotka</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8073"/>
        <source>Marquee</source>
        <translation>Plansza tytułowa</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7846"/>
        <source>No cabinet art</source>
        <translation>Brak oprawy graficznej automatu</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7858"/>
        <source>Previews</source>
        <translation>Podglądy</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7904"/>
        <location filename="../../qmc2main.cpp" line="8077"/>
        <source>preview</source>
        <translation>podgląd</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7889"/>
        <source>No previews</source>
        <translation>Bez podglądów</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7911"/>
        <source>Titles</source>
        <translation>Ekrany tytułowe</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7944"/>
        <location filename="../../qmc2main.cpp" line="8079"/>
        <source>title</source>
        <translation>ekran tytułowy</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7929"/>
        <source>No titles</source>
        <translation>Bez ekranów tytułowych</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8113"/>
        <source>Choose file to store the icon</source>
        <translation>Proszę wybrać plik do zapisania ikony</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8119"/>
        <source>icon image for &apos;%1&apos; stored as &apos;%2&apos;</source>
        <translation>obraz ikony dla &apos;%1&apos; zapisany jako &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8126"/>
        <source>FATAL: icon image for &apos;%1&apos; couldn&apos;t be stored as &apos;%2&apos;</source>
        <translation>FATALNIE: obraz ikony dla &apos;%1&apos; nie mógł zostać zapisany jako &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Physical memory:</source>
        <translation>Pamięć fizyczna:</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Total: %1 MB</source>
        <translation>Całkowita: %1 MB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Free: %1 MB</source>
        <translation>Wolna: %1 MB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Used: %1 MB</source>
        <translation>W użyciu: %1 MB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4099"/>
        <source>emulator #%1 is already embedded</source>
        <translation>emulator #%1 jest już osadzony</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4148"/>
        <source>WARNING: multiple windows for emulator #%1 found, choosing window ID %2 for embedding</source>
        <translation>UWAGA: znaleziono wiele okien dla emulatora #%1, wybrano okno o ID %2 do osadzenia</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4156"/>
        <source>embedding emulator #%1, window ID = %2</source>
        <translation>osadzanie emulatora #%1, ID okna = %2</translation>
    </message>
</context>
<context>
    <name>Marquee</name>
    <message>
        <location filename="../../marquee.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>Kopiuj do schowka</translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="56"/>
        <location filename="../../marquee.cpp" line="57"/>
        <source>Game marquee image</source>
        <translation>Obraz planszy tytułowej gry</translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="59"/>
        <location filename="../../marquee.cpp" line="60"/>
        <source>Machine marquee image</source>
        <translation>Obraz planszy tytułowej maszyny</translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="68"/>
        <location filename="../../marquee.cpp" line="72"/>
        <source>FATAL: can&apos;t open marquee file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku planszy tytułowej, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Oczekiwanie na dane...</translation>
    </message>
</context>
<context>
    <name>MawsQuickDownloadSetup</name>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="14"/>
        <source>MAWS quick download setup</source>
        <translation>Ustawienia szybkiego pobierania MAWS</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="395"/>
        <source>Previews</source>
        <translation>Podglądy</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="424"/>
        <source>Path to store preview images</source>
        <translation>Ścieżka do przechowywania obrazów podglądów</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="437"/>
        <source>Browse path to store preview images</source>
        <translation>Wskaż ścieżkę do przechowywania obrazów podglądów</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="32"/>
        <source>Flyers</source>
        <translation>Ulotki</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="20"/>
        <source>Icons and cabinet art</source>
        <translation>Ikony i oprawa graficzna automatu</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="61"/>
        <source>Path to store flyer images</source>
        <translation>Ścieżka do przechowywania obrazów ulotek</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="74"/>
        <source>Browse path to store flyer images</source>
        <translation>Wskaż ścieżkę do przechowywania obrazów ulotek</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="91"/>
        <source>Cabinets</source>
        <translation>Automaty</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="120"/>
        <source>Path to store cabinet images</source>
        <translation>Ścieżka do przechowywania obrazów automatów</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="133"/>
        <source>Browse path to store cabinet images</source>
        <translation>Wskaż ścieżkę do przechowywania obrazów automatów</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="150"/>
        <source>Controllers</source>
        <translation>Kontrolery</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="179"/>
        <source>Path to store controller images</source>
        <translation>Ścieżka do przechowywania obrazów kontrolerów</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="192"/>
        <source>Browse path to store controller images</source>
        <translation>Wskaż ścieżkę do przechowywania obrazów kontrolerów</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="209"/>
        <source>Marquees</source>
        <translation>Plansze tytułowe</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="238"/>
        <source>Path to store marquee images</source>
        <translation>Ścieżka do przechowywania obrazów plansz tytułowych</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="251"/>
        <source>Browse path to store marquee images</source>
        <translation>Wskaż ścieżkę do przechowywania obrazów plansz tytułowych</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="383"/>
        <source>Previews and titles</source>
        <translation>Podglądy i ekrany tytułowe</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="498"/>
        <source>Titles</source>
        <translation>Ekrany tytułowe</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="527"/>
        <source>Path to store title images</source>
        <translation>Ścieżka do przechowywania obrazów ekranów tytułowych</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="540"/>
        <source>Browse path to store title images</source>
        <translation>Wskaż ścieżkę do przechowywania obrazów ekranów tytułowych</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="268"/>
        <source>PCBs</source>
        <translation>Płytki drukowane</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="297"/>
        <source>Path to store PCB images</source>
        <translation>Ścieżka do przechowywania obrazów płytek drukowanych</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="310"/>
        <source>Browse path to store PCB images</source>
        <translation>Wskaż ścieżkę do przechowywania obrazów płytek drukowanych</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="612"/>
        <source>Apply MAWS quick download setup and close dialog</source>
        <translation>Zastosuj ustawienia szybkiego pobierania MAWS i zamknij okno dialogowe</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="615"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="626"/>
        <source>Cancel MAWS quick download setup and close dialog</source>
        <translation>Anuluj ustawienia szybkiego pobierania MAWS i zamknij okno dialogowe</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="629"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="408"/>
        <source>Automatically download preview images</source>
        <translation>Pobieraj obrazy podglądów automatycznie</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="48"/>
        <location filename="../../mawsqdlsetup.ui" line="107"/>
        <location filename="../../mawsqdlsetup.ui" line="166"/>
        <location filename="../../mawsqdlsetup.ui" line="225"/>
        <location filename="../../mawsqdlsetup.ui" line="284"/>
        <location filename="../../mawsqdlsetup.ui" line="330"/>
        <location filename="../../mawsqdlsetup.ui" line="411"/>
        <location filename="../../mawsqdlsetup.ui" line="514"/>
        <source>Auto</source>
        <translation>Automat</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="45"/>
        <source>Automatically download flyer images</source>
        <translation>Pobieraj obrazy ulotek automatycznie</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="104"/>
        <source>Automatically download cabinet images</source>
        <translation>Pobieraj obrazy automatów automatycznie</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="163"/>
        <source>Automatically download controller images</source>
        <translation>Pobieraj obrazy kontrolerów automatycznie</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="222"/>
        <source>Automatically download marquee images</source>
        <translation>Pobieraj obrazy plansz tytułowych automatycznie</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="511"/>
        <source>Automatically download title images</source>
        <translation>Pobieraj obrazy ekranów tytułowych automatycznie</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="281"/>
        <source>Automatically download PCB images</source>
        <translation>Pobieraj obrazy płytek drukowanych automatycznie</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="373"/>
        <source>Icons</source>
        <translation>Ikony</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="327"/>
        <source>Automatically download icon images</source>
        <translation>Pobieraj obrazy ikon automatycznie</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="450"/>
        <location filename="../../mawsqdlsetup.ui" line="553"/>
        <source>Preferred collection</source>
        <translation>Preferowana kolekcja</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="463"/>
        <source>Select the preferred image collection for in-game previews (auto-download)</source>
        <translation>Wybierz preferowaną kolekcję obrazów dla podglądów gier (pobieranie automatyczne)</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="467"/>
        <location filename="../../mawsqdlsetup.ui" line="570"/>
        <location filename="../../mawsqdlsetup.cpp" line="97"/>
        <location filename="../../mawsqdlsetup.cpp" line="102"/>
        <source>AntoPISA progettoSNAPS</source>
        <translation>AntoPISA progettoSNAPS</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="472"/>
        <source>MAME World Snap Collection</source>
        <translation>MAME World Snap Collection</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="477"/>
        <location filename="../../mawsqdlsetup.ui" line="575"/>
        <source>CrashTest Snap Collection</source>
        <translation>CrashTest Snap Collection</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="482"/>
        <source>Enaitz Jar Snaps</source>
        <translation>Enaitz Jar Snaps</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="566"/>
        <source>Select the preferred image collection for titles (auto-download)</source>
        <translation>Wybierz preferowaną kolekcję obrazów dla ekranów tytułowych (pobieranie automatyczne)</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="343"/>
        <source>Path to store icon images</source>
        <translation>Ścieżka do przechowywania obrazów ikon</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="356"/>
        <source>Browse path to store icon images</source>
        <translation>Wskaż ścieżkę do przechowywania obrazów ikon</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="113"/>
        <source>Choose icon directory</source>
        <translation>Wybierz katalog ikon</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="126"/>
        <source>Choose flyer directory</source>
        <translation>Wybierz katalog ulotek</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="139"/>
        <source>Choose cabinet directory</source>
        <translation>Wybierz katalog automatów</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="152"/>
        <source>Choose controller directory</source>
        <translation>Wybierz katalog kontrolerów</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="165"/>
        <source>Choose marquee directory</source>
        <translation>Wybierz katalog plansz tytułowych</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="178"/>
        <source>Choose PCB directory</source>
        <translation>Wybierz katalog płytek drukowanych</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="191"/>
        <source>Choose preview directory</source>
        <translation>Wybierz katalog podglądów</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="204"/>
        <source>Choose title directory</source>
        <translation>Wybierz katalog ekranów tytułowych</translation>
    </message>
</context>
<context>
    <name>MiniWebBrowser</name>
    <message>
        <location filename="../../miniwebbrowser.ui" line="15"/>
        <source>Mini Web Browser</source>
        <translation>Mini Przeglądarka Sieci</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="33"/>
        <location filename="../../miniwebbrowser.ui" line="36"/>
        <location filename="../../miniwebbrowser.cpp" line="96"/>
        <source>Go back</source>
        <translation>Idź wstecz</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="56"/>
        <location filename="../../miniwebbrowser.ui" line="59"/>
        <location filename="../../miniwebbrowser.cpp" line="98"/>
        <source>Go forward</source>
        <translation>Idź naprzód</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="79"/>
        <location filename="../../miniwebbrowser.ui" line="82"/>
        <source>Reload current URL</source>
        <translation>Odśwież bieżący adres URL</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="102"/>
        <location filename="../../miniwebbrowser.ui" line="105"/>
        <source>Stop loading of current URL</source>
        <translation>Zatrzymaj ładowanie bieżącego adresu URL</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="145"/>
        <location filename="../../miniwebbrowser.ui" line="148"/>
        <source>Enter current URL</source>
        <translation>Wpisz żądany adres URL</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="170"/>
        <location filename="../../miniwebbrowser.ui" line="173"/>
        <source>Load URL</source>
        <translation>Idź do adresu URL</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="205"/>
        <location filename="../../miniwebbrowser.ui" line="208"/>
        <source>Current progress loading URL</source>
        <translation>Postęp ładowania bieżącego adresu URL</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="125"/>
        <location filename="../../miniwebbrowser.ui" line="128"/>
        <source>Go home (first page)</source>
        <translation>Idź do strony domowej (pierwszej)</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="86"/>
        <source>Open link</source>
        <translation>Otwórz element docelowy</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="88"/>
        <source>Save link as...</source>
        <translation>Zapisz element docelowy jako...</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="90"/>
        <source>Copy link</source>
        <translation>Kopiuj adres odnośnika</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="92"/>
        <source>Save image as...</source>
        <translation>Zapisz obraz jako...</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="94"/>
        <source>Copy image</source>
        <translation>Kopiuj adres obrazka</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="100"/>
        <source>Reload</source>
        <translation>Odśwież</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="102"/>
        <source>Stop</source>
        <translation>Zatrzymaj</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="104"/>
        <source>Copy</source>
        <translation>Skopiuj</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="107"/>
        <source>Inspect</source>
        <translation>Zbadaj</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="477"/>
        <source>WARNING: invalid network reply and/or network error</source>
        <translation>UWAGA: niepoprawna odpowiedź sieci lub błąd sieci</translation>
    </message>
</context>
<context>
    <name>Options</name>
    <message>
        <location filename="../../options.ui" line="15"/>
        <source>Options</source>
        <translation>Opcje</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="41"/>
        <source>&amp;GUI</source>
        <translation>&amp;GUI</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="106"/>
        <source>Application language</source>
        <translation>Język aplikacji</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="110"/>
        <source>DE (German)</source>
        <translation>DE (niemiecki)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="130"/>
        <source>US (English)</source>
        <translation>US (angielski)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="91"/>
        <source>Language</source>
        <translation>Język</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="619"/>
        <source>Image cache size in MB</source>
        <translation>Rozmiar bufora obrazów w MB</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="622"/>
        <source> MB</source>
        <translation> MB</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="606"/>
        <source>Image cache size</source>
        <translation>Rozmiar bufora obrazów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="502"/>
        <source>Application font</source>
        <translation>Czcionka aplikacji</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="241"/>
        <source>Show short description of current processing in progress bar</source>
        <translation>Pokaż krótkie opisy bieżącego przetwarzania w pasku postępu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="566"/>
        <source>Smooth image scaling (nicer, but slower)</source>
        <translation>Wygładzone skalowanie obrazów (ładniejsze, ale wolniejsze)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="569"/>
        <source>Smooth scaling</source>
        <translation>Wygładzone skalowanie</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="576"/>
        <source>Retry loading images which weren&apos;t found before?</source>
        <translation>Spróbować ponownie wczytać nieznalezione wcześniej obrazy?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="579"/>
        <source>Retry loading images</source>
        <translation>Spróbuj ponownie wczytać obrazy</translation>
    </message>
    <message>
        <source>Save game selection at exit and before reloading the gamelist</source>
        <translation type="obsolete">Zapisz wybór gry przy wyjściu i przed przeładowaniem listy gier</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="151"/>
        <source>Save game selection</source>
        <translation>Zapisz wybór gry</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="178"/>
        <location filename="../../options.ui" line="188"/>
        <location filename="../../options.ui" line="198"/>
        <location filename="../../options.ui" line="208"/>
        <location filename="../../options.ui" line="356"/>
        <location filename="../../options.ui" line="366"/>
        <location filename="../../options.ui" line="422"/>
        <source>Scale image to fit frame size (otherwise use original size)</source>
        <translation>Skaluj obraz do rozmiaru ramki (w przeciwnym wypadku użyj oryginalnego rozmiaru)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="181"/>
        <source>Scaled preview</source>
        <translation>Skalowany podgląd</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="251"/>
        <source>Kill emulators on exit?</source>
        <translation>Zabijać emulatory przy wyjściu?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="254"/>
        <source>Kill emulators</source>
        <translation>Zabijać emulatory</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="138"/>
        <source>Save window layout at exit</source>
        <translation>Zapisz układ okien przy wyjściu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="141"/>
        <source>Save layout</source>
        <translation>Zapisz układ</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="658"/>
        <source>Use standard or custom color palette?</source>
        <translation>Użyć palety kolorów standardowej czy użytkownika?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="661"/>
        <source>Standard color palette</source>
        <translation>Standardowa paleta kolorów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="369"/>
        <source>Scaled flyer</source>
        <translation>Skalowana ulotka</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="158"/>
        <source>Restore saved window layout at start</source>
        <translation>Przywracaj zapisany układ okien przy starcie</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="161"/>
        <source>Restore layout</source>
        <translation>Przywracaj układ</translation>
    </message>
    <message>
        <source>Restore saved game selection at start and after reloading the gamelist</source>
        <translation type="obsolete">Przywracaj zapisany wybór gry przy starcie i po przeładowaniu listy gier</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="171"/>
        <source>Restore game selection</source>
        <translation>Przywróć wybór gry</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="546"/>
        <source>GUI style</source>
        <translation>Styl GUI</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="382"/>
        <source>Application font (= system default if empty)</source>
        <translation>Czcionka aplikacji  (= domyślna systemowa jeśli puste)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="398"/>
        <source>Browse application font</source>
        <translation>Wskaż czcionkę aplikacji</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="767"/>
        <location filename="../../options.ui" line="3757"/>
        <source>F&amp;iles / Directories</source>
        <translation>Pl&amp;iki / Katalogi</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="970"/>
        <source>Browse frontend data directory</source>
        <translation>Wskaż katalog danych interfejsu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="957"/>
        <source>Frontend data directory (read)</source>
        <translation>Katalog danych interfejsu (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="944"/>
        <source>Data directory</source>
        <translation>Katalog danych</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1291"/>
        <source>Switch between specifying an icon directory or a ZIP-compressed icon file</source>
        <translation>Przełącz pomiędzy określeniem katalogu ikon a skompresowanym plikiem ZIP ikon</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1294"/>
        <location filename="../../options.cpp" line="1910"/>
        <location filename="../../options.cpp" line="1967"/>
        <location filename="../../options.cpp" line="2869"/>
        <source>Icon directory</source>
        <translation>Katalog ikon</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1188"/>
        <source>Switch between specifying a flyer directory or a ZIP-compressed flyer file</source>
        <translation>Przełącz pomiędzy określeniem katalogu ulotek a skompresowanym plikiem ZIP ulotek</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1191"/>
        <location filename="../../options.cpp" line="1905"/>
        <location filename="../../options.cpp" line="1962"/>
        <location filename="../../options.cpp" line="2858"/>
        <source>Flyer directory</source>
        <translation>Katalog ulotek</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1085"/>
        <source>Switch between specifying a preview directory or a ZIP-compressed preview file</source>
        <translation>Przełącz pomiędzy określeniem katalogu podglądów a skompresowanym plikiem ZIP podglądów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1088"/>
        <location filename="../../options.cpp" line="1900"/>
        <location filename="../../options.cpp" line="1957"/>
        <location filename="../../options.cpp" line="2847"/>
        <source>Preview directory</source>
        <translation>Katalog podglądów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3983"/>
        <source>Browse gamelist cache file</source>
        <translation>Wskaż plik bufora listy gier</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3970"/>
        <source>Gamelist cache file (write)</source>
        <translation>Plik bufora listy gier (zapis)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4020"/>
        <source>Browse ROM state cache file</source>
        <translation>Wskaż plik bufora stanów ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4007"/>
        <source>ROM state cache file (write)</source>
        <translation>Plik bufora stanów ROM-ów (zapis)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3994"/>
        <source>ROM state cache</source>
        <translation>Bufor stanów ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="920"/>
        <source>Play history file (write)</source>
        <translation>Plik historii gier (zapis)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="796"/>
        <source>Temporary file</source>
        <translation>Plik tymczasowy</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="870"/>
        <source>Favorites file</source>
        <translation>Plik ulubionych</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="907"/>
        <source>Play history file</source>
        <translation>Plik historii gier</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="933"/>
        <source>Browse play history file</source>
        <translation>Wskaż plik historii gier</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="883"/>
        <source>Game favorites file (write)</source>
        <translation>Plik ulubionych gier (zapis)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="896"/>
        <source>Browse game favorites file</source>
        <translation>Wskaż plik ulubionych gier</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="809"/>
        <source>Temporary work file (write)</source>
        <translation>Tymczasowy plik roboczy (zapis)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="822"/>
        <source>Browse temporary work file</source>
        <translation>Wskaż tymczasowy plik roboczy</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1329"/>
        <source>Icon directory (read)</source>
        <translation>Katalog ikon (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1342"/>
        <source>Browse icon directory</source>
        <translation>Wskaż katalog ikon</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1366"/>
        <source>ZIP-compressed icon file (read)</source>
        <translation>Skompresowany plik ZIP ikon (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1379"/>
        <source>Browse ZIP-compressed icon file</source>
        <translation>Wskaż skompresowany plik ZIP ikon</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1226"/>
        <source>Flyer directory (read)</source>
        <translation>Katalog ulotek (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1239"/>
        <source>Browse flyer directory</source>
        <translation>Wskaż katalog ulotek</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1263"/>
        <source>ZIP-compressed flyer file (read)</source>
        <translation>Skompresowany plik ZIP ulotek (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1276"/>
        <source>Browse ZIP-compressed flyer file</source>
        <translation>Wskaż skompresowany plik ZIP ulotek</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1123"/>
        <source>Preview directory (read)</source>
        <translation>Katalog podglądów (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1136"/>
        <source>Browse preview directory</source>
        <translation>Wskaż katalog podglądów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1160"/>
        <source>ZIP-compressed preview file (read)</source>
        <translation>Skompresowany plik ZIP podglądów (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1173"/>
        <source>Browse ZIP-compressed preview file</source>
        <translation>Wskaż skompresowany plik ZIP podglądów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2617"/>
        <source>Auto-trigger ROM check</source>
        <translation>Auto-uruchamianie sprawdzania ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2445"/>
        <source>Update delay</source>
        <translation>Opóźnienie aktualizacji</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2429"/>
        <source>immediate</source>
        <translation>natychmiastowe</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2464"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2413"/>
        <source>Responsiveness</source>
        <translation>Reakcja</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2388"/>
        <source>Select sort order</source>
        <translation>Wybierz porządek sortowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2392"/>
        <source>Ascending</source>
        <translation>Rosnący</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2401"/>
        <source>Descending</source>
        <translation>Malejący</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2323"/>
        <source>Select sort criteria</source>
        <translation>Wybierz kryterium sortowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2327"/>
        <source>Game description</source>
        <translation>Opis gry</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2332"/>
        <source>ROM state</source>
        <translation>Stan ROM-u</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2342"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2347"/>
        <source>Manufacturer</source>
        <translation>Producent</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2375"/>
        <source>Sort order</source>
        <translation>Porządek sortowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2310"/>
        <source>Sort criteria</source>
        <translation>Kryterium sortowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2184"/>
        <source>ROM state filter</source>
        <translation>Filtr stanu ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2199"/>
        <source>Show ROM state C (correct)?</source>
        <translation>Pokazać stan ROM-ów P (poprawny)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2222"/>
        <source>Show ROM state M (mostly correct)?</source>
        <translation>Pokazać stan ROM-ów Wp (w większości poprawny)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2245"/>
        <source>Show ROM state I (incorrect)?</source>
        <translation>Pokazać stan ROM-ów Np (niepoprawny)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2268"/>
        <source>Show ROM state N (not found)?</source>
        <translation>Pokazać stan ROM-ów Nz (nieznaleziony)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2291"/>
        <source>Show ROM state U (unknown)?</source>
        <translation>Pokazać stan ROM-ów Nn (nieznany)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2744"/>
        <source>&amp;Shortcuts / Keys</source>
        <translation>&amp;Skróty / Klawisze</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2793"/>
        <source>Reset key sequence to default</source>
        <translation>Zresetuj sekwencję klawiszy do domyślnej</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2796"/>
        <source>Reset</source>
        <translation>Zresetuj</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2776"/>
        <location filename="../../options.ui" line="2779"/>
        <source>Redefine key sequence</source>
        <translation>Redefiniuj sekwencję klawiszy</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2813"/>
        <source>Active shortcut definitions; double-click to redefine key sequence</source>
        <translation>Definicje skrótów aktywnych; kliknij podwójnie aby redefiniować sekwencję klawiszy</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2837"/>
        <location filename="../../options.cpp" line="386"/>
        <location filename="../../options.cpp" line="387"/>
        <location filename="../../options.cpp" line="3392"/>
        <source>Default</source>
        <translation>Domyślny</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2842"/>
        <source>Custom</source>
        <translation>Użytkownika</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3343"/>
        <source>Zip tool</source>
        <translation>Narzędzie zip</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3375"/>
        <source>Browse for zip tool</source>
        <translation>Wskaż narzędzie zip</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3399"/>
        <source>Zip tool argument list to remove entries from the ZIP archive (i. e. &quot;$ARCHIVE$ -d $FILELIST$&quot;)</source>
        <translation>Parametr narzędzia zip do usunięcia wpisów z archiwum ZIP (np. &quot;$ARCHIVE$ -d $FILELIST$&quot;)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3409"/>
        <source>File removal tool</source>
        <translation>Narzędzie usuwające pliki</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3465"/>
        <source>File removal tool argument list (i. e. &quot;-f -v $FILELIST$&quot;)</source>
        <translation>Lista parametrów narzędzia usuwającego pliki (np. &quot;-f -v $FILELIST$&quot;)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3386"/>
        <location filename="../../options.ui" line="3452"/>
        <location filename="../../options.ui" line="3531"/>
        <location filename="../../options.ui" line="4388"/>
        <location filename="../../options.ui" line="4497"/>
        <source>Arguments</source>
        <translation>Parametry</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3441"/>
        <source>Browse for file removal tool</source>
        <translation>Wskaż narzędzie usuwające pliki</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3721"/>
        <source>&lt;font size=&quot;-1&quot;&gt;&lt;b&gt;WARNING: stored passwords are &lt;u&gt;weakly&lt;/u&gt; encrypted!&lt;/b&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&quot;-1&quot;&gt;&lt;b&gt;UWAGA: zapisane hasła są &lt;u&gt;słabo&lt;/u&gt; zaszyfrowane!&lt;/b&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3742"/>
        <source>E&amp;mulator</source>
        <translation>E&amp;mulator</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3752"/>
        <source>&amp;Global configuration</source>
        <translation>&amp;Globalna konfiguracja</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3946"/>
        <source>Browse XML gamelist cache file</source>
        <translation>Wskaż plik bufora listy gier XML</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3933"/>
        <source>Cache file for the output of mame -listxml / -lx (write)</source>
        <translation>Plik bufora dla wyjścia mame -listxml / -lx (zapis)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3872"/>
        <source>Browse emulator log file</source>
        <translation>Wskaż plik dziennika emulatora</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3859"/>
        <source>Emulator log file (write)</source>
        <translation>Plik dziennika emulatora (zapis)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3846"/>
        <source>Emulator log file</source>
        <translation>Plik dziennika emulatora</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3835"/>
        <location filename="../../options.ui" line="4483"/>
        <source>Browse emulator executable file</source>
        <translation>Wskaż plik wykonywalny emulatora</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3807"/>
        <source>Executable file</source>
        <translation>Plik wykonywalny</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3909"/>
        <source>Browse options template file</source>
        <translation>Wskaż plik szablonu opcji</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3896"/>
        <source>Options template file (read)</source>
        <translation>Plik szablonu opcji (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="125"/>
        <source>PT (Portuguese)</source>
        <translation>PT (portugalski)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="244"/>
        <source>Show progress texts</source>
        <translation>Pokazuj teksty postępu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="425"/>
        <source>Scaled PCB</source>
        <translation>Skalowana płytka drukowana</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="509"/>
        <source>Style sheet</source>
        <translation>Arkusz stylu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="522"/>
        <source>Qt style sheet file (*.qss, leave empty for no style sheet)</source>
        <translation>Plik arkusza stylu Qt (*.qss, proszę zostawić pusty dla żadnego)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="535"/>
        <source>Browse Qt style sheet file</source>
        <translation>Wskaż plik arkusza stylu Qt</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="638"/>
        <source>Open the detail setup dialog</source>
        <translation>Otwórz okno dialogowe ustawień szczegółów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="641"/>
        <source>Detail setup...</source>
        <translation>Ustawienia szczegółów...</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="651"/>
        <source>Show memory  usage</source>
        <translation>Pokaż zużycie pamięci</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="668"/>
        <source>Exit this QMC2 variant when launching another?</source>
        <translation>Zakończyć ten wariant QMC2 przy uruchamianiu innego?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="671"/>
        <source>Exit on variant launch</source>
        <translation>Zakończ przy uruchamianiu wariantu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="678"/>
        <source>Log font</source>
        <translation>Czcionka dziennika</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="691"/>
        <source>Font used in logs (= application font if empty)</source>
        <translation>Czcionka używana w dziennikach (taka sama jak aplikacji jeśli puste)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="707"/>
        <source>Browse font used in logs</source>
        <translation>Wskaż czcionkę używaną w dziennikach</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1806"/>
        <source>Switch between specifying a PCB directory or a ZIP-compressed PCB file</source>
        <translation>Przełącz pomiędzy określeniem katalogu płytek drukowanych a skompresowanym plikiem ZIP płytek drukowanych</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1844"/>
        <source>PCB directory (read)</source>
        <translation>Katalog płytek drukowanych (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1857"/>
        <source>Browse PCB directory</source>
        <translation>Wskaż katalog płytek drukowanych</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1881"/>
        <source>ZIP-compressed PCB file (read)</source>
        <translation>Skompresowany plik ZIP płytek drukowanych (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1894"/>
        <source>Browse ZIP-compressed PCB file</source>
        <translation>Wskaż skompresowany plik ZIP płytek drukowanych</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1041"/>
        <source>Enable the use of catver.ini -- get the newest version from http://www.progettoemma.net/?catlist</source>
        <translation>Włącz korzystanie z catver.ini -- najnowszą wersję można pobrać z http://www.progettoemma.net/?catlist</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1044"/>
        <source>Use catver.ini</source>
        <translation>Używaj catver.ini</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1061"/>
        <source>Path to catver.ini (read)</source>
        <translation>Ścieżka do catver.ini (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1074"/>
        <source>Browse path to catver.ini</source>
        <translation>Wskaż ścieżkę do catver.ini</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2357"/>
        <source>ROM types</source>
        <translation>Rodzaje ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2646"/>
        <source>Hide primary game list while loading (recommended, much faster)</source>
        <translation>Ukryj główną listę gier podczas ładowania (zalecane, znacznie szybsze)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2649"/>
        <source>Hide while loading</source>
        <translation>Ukryj podczas ładowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2672"/>
        <source>SW snap position</source>
        <translation>Położenie zrzutu ekranu oprogramowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2689"/>
        <source>Above / Left</source>
        <translation>Na górze / z lewej</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2694"/>
        <source>Above / Center</source>
        <translation>Na górze / pośrodku</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2699"/>
        <source>Above / Right</source>
        <translation>Na górze / z prawej</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2704"/>
        <source>Below / Left</source>
        <translation>Na dole / z lewej</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2709"/>
        <source>Below / Center</source>
        <translation>Na dole / pośrodku</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2714"/>
        <source>Below / Right</source>
        <translation>Na dole / z prawej</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4133"/>
        <source>Software list cache file (write)</source>
        <translation>Plik bufora listy oprogramowania (zapis)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4146"/>
        <source>Browse software list cache file</source>
        <translation>Wskaż plik bufora listy oprogramowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4170"/>
        <source>Directory used as the default software folder for the MESS device configurator (if a sub-folder named as the current machine exists, that folder will be selected instead)</source>
        <translation>Katalog używany przez konfigurator urządzeń MESS jako domyślny folder oprogramowania (jeżeli istnieje podfolder nazwany jak bieżąca maszyna, będzie on wybrany w zamian)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2630"/>
        <source>Launch emulation directly when an item is activated in the search-, favorites- or played-lists (instead of jumping to the master list)</source>
        <translation>Uruchamiaj emulację bezpośrednio po aktywacji elementu w liście wyników wyszukiwania, ulubionych bądź granych (zamiast przeskakiwać do listy głównej)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2123"/>
        <source>Switch between specifying a software snap directory or a ZIP-compressed software snap file</source>
        <translation>Przełącz pomiędzy określeniem katalogu zrzutów ekranu oprogramowania a skompresowanym plikiem ZIP zrzutów ekranu oprogramowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2126"/>
        <location filename="../../options.ui" line="2139"/>
        <location filename="../../options.cpp" line="1940"/>
        <location filename="../../options.cpp" line="1997"/>
        <location filename="../../options.cpp" line="2935"/>
        <source>SW snap folder</source>
        <translation>Katalog zrzutów ekranu oprogramowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1931"/>
        <source>Software snap-shot directory (read)</source>
        <translation>Katalog zrzutów ekranu oprogramowania (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1944"/>
        <source>Browse software snap-shot directory</source>
        <translation>Wskaż katalog zrzutów ekranu oprogramowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1968"/>
        <source>ZIP-compressed software snap-shot file (read)</source>
        <translation>Skompresowany plik ZIP zrzutów ekranu oprogramowania (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1981"/>
        <source>Browse ZIP-compressed software snap-shot file</source>
        <translation>Wskaż skompresowany plik ZIP zrzutów ekranu oprogramowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2362"/>
        <source>Players</source>
        <translation>Gracze</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2633"/>
        <source>Play on sub-list activation</source>
        <translation>Graj po aktywacji w listach podległych</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2578"/>
        <source>Select the cursor position QMC2 uses when auto-scrolling to the current item (this setting applies to all views and lists!)</source>
        <translation>Wybór pozycji kursora używanej przez QMC2 podczas automatycznego przewijania do bieżącego elementu (to ustawienie ma zastosowanie do wszystkich widoków i list!)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2585"/>
        <source>Visible</source>
        <translation>Widoczny</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2590"/>
        <source>Top</source>
        <translation>Góra</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2595"/>
        <source>Bottom</source>
        <translation>Dół</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2600"/>
        <source>Center</source>
        <translation>Środek</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2565"/>
        <source>Cursor position</source>
        <translation>Pozycja kursora</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="148"/>
        <source>Save game selection on exit and before reloading the game list</source>
        <translation>Zapisz wybraną grę podczas wyjścia i przed przeładowaniem listy gier</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="168"/>
        <source>Restore saved game selection at start and after reloading the game list</source>
        <translation>Przywróć wybraną grę podczas uruchomienia i po przeładowaniu listy gier</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="748"/>
        <source>Use a unifed tool- and title-bar on Mac OS X</source>
        <translation>Używaj wspólnego paska narzędzi i tytułowego pod Mac OS X</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="751"/>
        <source>Unify with title</source>
        <translation>Wspólnie z tytułem</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2337"/>
        <source>Tag</source>
        <translation>Zaznaczenie</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2367"/>
        <source>Driver status</source>
        <translation>Stan sterownika</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2515"/>
        <source>Display ROM status icons in master lists?</source>
        <translation>Wyświetlać ikony stanów ROM-ów na listach głównych?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2518"/>
        <source>Show ROM status icons</source>
        <translation>Pokazać ikony stanów ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2685"/>
        <source>Select the position where sofware snap-shots are displayed within software lists</source>
        <translation>Wybierz pozycję na której w ramach listy oprogramowania są wyświetlane zrzuty ekranu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2719"/>
        <source>Disable snaps</source>
        <translation>Bez zrzutów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2727"/>
        <source>Display software snap-shots when hovering the software list with the mouse cursor</source>
        <translation>Wyświetlanie zrzutów ekranu oprogramowania po najechaniu kursorem na listę oprogramowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2730"/>
        <source>SW snaps on mouse hover</source>
        <translation>Zrzuty ekranu oprogramowania po najechaniu kursorem</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3314"/>
        <source>Proxy / &amp;Tools</source>
        <translation>Pośrednik / Narzę&amp;dzia</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3488"/>
        <source>ROM tool</source>
        <translation>Narzędzie do obsługi ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3507"/>
        <source>External ROM tool (it&apos;s completely up to you...)</source>
        <translation>Zewnętrzne narzędzie do obsługi ROM-ów (masz wolny wybór...)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3520"/>
        <source>Browse ROM tool</source>
        <translation>Wskaż narzędzie do obsługo ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3544"/>
        <source>ROM tool argument list (i. e. &quot;$ID$ $DESCRIPTION$&quot;)</source>
        <translation>Lista parametrów narzędzia do obsługi ROM-ów (np. &quot;$ID$ $DESCRIPTION$&quot;)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3577"/>
        <source>Browse working directory of the ROM tool</source>
        <translation>Wskaż katalog roboczy narzędzia ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3594"/>
        <source>Copy the tool&apos;s output to the front end log (keeping it for debugging)</source>
        <translation>Kopiowanie wyjścia narzędzia do dziennika interfejsu (w celu debugowania)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3597"/>
        <source>Copy tool output to front end log</source>
        <translation>Kopiuj wyjście narzędzia do dziennika interfejsu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3604"/>
        <source>Automatically close the tool-executor dialog when the external process finished</source>
        <translation>Automatyczne zamykanie okna dialogowego wykonawcy narzędzia po zakończeniu zewnętrznego procesu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3607"/>
        <source>Close dialog automatically</source>
        <translation>Zamknij okno dialogowe automatycznie</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3883"/>
        <source>Options template file</source>
        <translation>Plik szablonu opcji</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3920"/>
        <source>XML game list cache</source>
        <translation>Bufor XML listy gier</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3957"/>
        <source>Game list cache</source>
        <translation>Bufor listy gier</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4120"/>
        <source>Software list cache</source>
        <translation>Bufor listy oprogramowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4194"/>
        <source>MAME variant exe</source>
        <translation>Alternatywny plik wykonywalny MAME</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4207"/>
        <source>Specify the exe file for the MAME variant (leave empty when all variants are installed in the same directory)</source>
        <translation>Proszę wskazać alternatywny plik wykonywalny MAME (lub zostawić puste jeśli wszystkie warianty są zainstalowane w tym samym katalogu)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4220"/>
        <source>Browse MAME variant exe</source>
        <translation>Wskaż alternatywny plik wykonywalny MAME</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4231"/>
        <source>MESS variant exe</source>
        <translatorcomment>Alternatywny plik wykonywalny MESS</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4244"/>
        <source>Specify the exe file for the MESS variant (leave empty when all variants are installed in the same directory)</source>
        <translation>Proszę wskazać alternatywny plik wykonywalny MESS (lub zostawić puste jeśli wszystkie warianty są zainstalowane w tym samym katalogu)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4257"/>
        <source>Browse MESS variant exe</source>
        <translation>Wskaż alternatywny plik wykonywalny MESS</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3551"/>
        <location filename="../../options.ui" line="4268"/>
        <location filename="../../options.ui" line="4383"/>
        <location filename="../../options.ui" line="4528"/>
        <source>Working directory</source>
        <translation>Katalog roboczy</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4281"/>
        <location filename="../../options.ui" line="4541"/>
        <source>Working directory that&apos;s used when the emulator is executed (if empty, QMC2&apos;s current working directory is used)</source>
        <translation>Katalog roboczy używany podczas pracy emulatora (jeśli pusty, będzie używany bieżący katalog roboczy QMC2)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4294"/>
        <location filename="../../options.ui" line="4554"/>
        <source>Browse working directory</source>
        <translation>Wskaż katalog roboczy</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4157"/>
        <source>General software folder</source>
        <translation>Ogólny folder oprogramowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3564"/>
        <source>Working directory that&apos;s used when the ROM tool is executed (if empty, QMC2&apos;s current working directory is used)</source>
        <translation>Katalog roboczy używany podczas pracy narzędzi do obsługi ROM-ów (jeśli puste, używany jest bieżący katalog roboczy QMC2)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4183"/>
        <source>Browse general software folder</source>
        <translation>Wskaż ogólny folder oprogramowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4312"/>
        <source>Additional &amp;Emulators</source>
        <translation>Dodatkowe &amp;emulatory</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4339"/>
        <source>Registered emulators -- you may select one of these in the game-specific emulator configuration</source>
        <translation>Zarejestrowane emulatory -- można wybrać jeden z nich w specyficznej dla gry konfiguracji emulatora</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4373"/>
        <location filename="../../options.ui" line="4443"/>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4378"/>
        <location filename="../../options.ui" line="4457"/>
        <source>Executable</source>
        <translation>Plik wykonywalny</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4399"/>
        <source>Register emulator</source>
        <translation>Zarejestruj emulator</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4429"/>
        <source>Deregister emulator</source>
        <translation>Wyrejestruj emulator</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4450"/>
        <source>Registered emulator&apos;s name</source>
        <translation>Nazwa zarejestrowanego emulatora</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4470"/>
        <source>Command to execute the emulator (path to the executable file)</source>
        <translation>Komenda uruchamiająca emulator (ścieżka do pliku wykonywalnego)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4504"/>
        <source>Arguments passed to the emulator -- use $ID$ as a placeholder for the unique game/machine ID (its short name)</source>
        <translation>Parametry przekazywane emulatorowi -- proszę użyć $ID$ jako zamiennika unikalnej krótkiej nazwy gry bądź maszyny</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4514"/>
        <source>Replace emulator registration</source>
        <translation>Zastąp rejestrację emulatora</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4582"/>
        <source>Apply settings</source>
        <translation>Zastosuj ustawienia</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4585"/>
        <source>&amp;Apply</source>
        <translation>&amp;Zastosuj</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4596"/>
        <source>Restore currently applied settings</source>
        <translation>Przywróć bieżąco zastosowane ustawienia</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4599"/>
        <source>&amp;Restore</source>
        <translation>&amp;Przywróć</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4610"/>
        <source>Reset to default settings (click &lt;i&gt;Restore&lt;/i&gt; to restore currently applied settings!)</source>
        <translation>Zresetuj do ustawień domyślnych (kliknij &lt;i&gt;Przywróć&lt;/i&gt; aby przywrócić bieżąco zastosowane ustawienia!)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4613"/>
        <source>&amp;Default</source>
        <translation>&amp;Domyślne</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4637"/>
        <source>Close and apply settings</source>
        <translation>Zamknij i zastosuj ustawienia</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4640"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4647"/>
        <source>Close and discard changes</source>
        <translation>Zamknij i porzuć zmiany</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4650"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="266"/>
        <source>Check all ROM states</source>
        <translation>Sprawdź stany wszystkich ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="267"/>
        <source>Check all sample sets</source>
        <translation>Sprawdź stany wszystkich sampli</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="268"/>
        <source>Check preview images</source>
        <translation>Sprawdź obrazy podglądów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="269"/>
        <source>Check flyer images</source>
        <translation>Sprawdź obrazy ulotek</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="270"/>
        <source>Check icon images</source>
        <translation>Sprawdź obrazy ikon</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="271"/>
        <source>About QMC2</source>
        <translation>O QMC2</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="275"/>
        <source>Copy game to favorites</source>
        <translation>Skopiuj grę do ulubionych</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="277"/>
        <source>Online documentation</source>
        <translation>Dokumentacja on-line</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="278"/>
        <source>Clear image cache</source>
        <translation>Wyczyść bufor obrazów</translation>
    </message>
    <message>
        <source>Demo mode</source>
        <translation type="obsolete">Tryb demonstracyjny</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="281"/>
        <source>Clear icon cache</source>
        <translation>Wyczyść bufor ikon</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="282"/>
        <source>Open options dialog</source>
        <translation>Otwórz okno dialogowe opcji</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="287"/>
        <source>About Qt</source>
        <translation>O Qt</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="288"/>
        <source>Reload gamelist</source>
        <translation>Przeładuj listę gier</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="289"/>
        <source>Check game&apos;s ROM state</source>
        <translation>Sprawdź stan ROM-u gry</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="290"/>
        <source>Check states of tagged ROMs</source>
        <translation>Sprawdź stan zaznaczonych ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="291"/>
        <source>Recreate template map</source>
        <translation>Odtwórz mapę szablonu</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="292"/>
        <source>Check template map</source>
        <translation>Sprawdź mapę szablonu</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="293"/>
        <source>Stop processing / exit QMC2</source>
        <translation>Zatrzymaj przetwarzanie / wyjdź z QMC2</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="295"/>
        <source>Clear YouTube cache</source>
        <translation>Wyczyść bufor YouTube</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="312"/>
        <source>Tag current set</source>
        <translation>Zaznacz bieżący zestaw</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="313"/>
        <source>Untag current set</source>
        <translation>Odznacz bieżący zestaw</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="314"/>
        <source>Toggle tag mark</source>
        <translation>Odwróć zaznaczenie</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="315"/>
        <source>Tag all sets</source>
        <translation>Zaznacz wszystkie zestawy</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="316"/>
        <source>Untag all sets</source>
        <translation>Odznacz wszystkie zestawy</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="317"/>
        <source>Invert all tags</source>
        <translation>Odwróć wszystkie zaznaczenia</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="318"/>
        <source>Gamelist with full detail</source>
        <translation>Lista gier ze wszystkimi szczegółami</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="321"/>
        <source>View games by category</source>
        <translation>Wyświetlaj gry według kategorii</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="322"/>
        <source>View games by version</source>
        <translation>Wyświetlaj gey według wersji</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="324"/>
        <source>Run external ROM tool</source>
        <translation>Uruchom zewnętrzne narzędzie do obsługi ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="325"/>
        <source>Run ROM tool for tagged sets</source>
        <translation>Uruchom narzędzie obsługi ROM-ów dla zaznaczonych zestawów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="343"/>
        <source>Plus (+)</source>
        <translation>Plus (+)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="344"/>
        <source>Minus (-)</source>
        <translation>Minus (-)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="345"/>
        <source>Cursor down</source>
        <translation>Kursor w dół</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="346"/>
        <source>End</source>
        <translation>End</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="347"/>
        <source>Escape</source>
        <translation>Escape</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="348"/>
        <source>Cursor left</source>
        <translation>Kursor w lewo</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="349"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="350"/>
        <source>Page down</source>
        <translation>Page down</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="351"/>
        <source>Page up</source>
        <translation>Page up</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="352"/>
        <source>Enter key</source>
        <translation>Klawisz Enter</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="353"/>
        <source>Cursor right</source>
        <translation>Kursor w prawo</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="354"/>
        <source>Tabulator</source>
        <translation>Tabulator</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="355"/>
        <source>Cursor up</source>
        <translation>Kursor w górę</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="358"/>
        <source>WARNING: configuration is not writeable, please check access permissions for </source>
        <translation>UWAGA: konfiguracja nie jest zapisywalna, proszę sprawdzić uprawnienia dostępu dla </translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="363"/>
        <location filename="../../options.cpp" line="370"/>
        <source>Reset to default font</source>
        <translation>Zresetuj do domyślnej czcionki</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="388"/>
        <location filename="../../options.cpp" line="389"/>
        <source>Search in the folder we were called from</source>
        <translation>Szukaj w folderze z którego zostaliśmy wywołani</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="825"/>
        <source>image cache size set to %1 MB</source>
        <translation>rozmiar bufora obrazów ustawiony na %1 MB</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1206"/>
        <source>Confirm</source>
        <translation>Potwierdź</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1212"/>
        <source>&amp;Local</source>
        <translation>&amp;Lokalne</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1212"/>
        <source>&amp;Overwrite</source>
        <translation>&amp;Nadpisz</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1212"/>
        <source>Do&amp;n&apos;t apply</source>
        <translation>&amp;Nie zastosowuj</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1317"/>
        <source>invalidating machine info DB</source>
        <translation>unieważnianie bazy danych informacji o maszynach</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1351"/>
        <source>please reload game list for some changes to take effect</source>
        <translation>proszę przeładować listę gier aby pewne zmiany mogły odnieść skutek</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1353"/>
        <source>please reload machine list for some changes to take effect</source>
        <translation>proszę przeładować listę maszyn aby pewne zmiany mogły odnieść skutek</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1369"/>
        <source>re-sort of machine list impossible at this time, please wait for ROM verification to finish and try again</source>
        <translation>ponownie posortowanie listy maszyn jest obecnie niemożliwe, proszę poczekać na ukończenie weryfikacji ROM-ów i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1424"/>
        <location filename="../../options.cpp" line="1426"/>
        <source>ascending</source>
        <translation>rosnącym</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1424"/>
        <location filename="../../options.cpp" line="1426"/>
        <source>descending</source>
        <translation>malejącym</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1438"/>
        <source>Waiting for data...</source>
        <translation>Oczekiwanie na dane...</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1609"/>
        <location filename="../../options.cpp" line="1613"/>
        <source>FATAL: can&apos;t open preview file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku podglądu, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1627"/>
        <location filename="../../options.cpp" line="1631"/>
        <source>FATAL: can&apos;t open flyer file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku ulotki, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1717"/>
        <location filename="../../options.cpp" line="1721"/>
        <source>FATAL: can&apos;t open PCB file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku płytki drukowanej, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1735"/>
        <location filename="../../options.cpp" line="1739"/>
        <source>FATAL: can&apos;t open software snap-shot file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku zrzutu ekranu oprogramowania, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1752"/>
        <location filename="../../options.cpp" line="1756"/>
        <source>FATAL: can&apos;t open icon file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku ikony, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1900"/>
        <location filename="../../options.cpp" line="1957"/>
        <location filename="../../options.cpp" line="2847"/>
        <source>Preview file</source>
        <translation>Plik podglądu</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1905"/>
        <location filename="../../options.cpp" line="1962"/>
        <location filename="../../options.cpp" line="2858"/>
        <source>Flyer file</source>
        <translation>Plik ulotki</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1910"/>
        <location filename="../../options.cpp" line="1967"/>
        <location filename="../../options.cpp" line="2869"/>
        <source>Icon file</source>
        <translation>Plik ikony</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1935"/>
        <location filename="../../options.cpp" line="1992"/>
        <location filename="../../options.cpp" line="2924"/>
        <source>PCB file</source>
        <translation>Plik płytek drukowanych</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1809"/>
        <location filename="../../options.cpp" line="1935"/>
        <location filename="../../options.cpp" line="1992"/>
        <location filename="../../options.cpp" line="2924"/>
        <source>PCB directory</source>
        <translation>Katalog płytek drukowanych</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2271"/>
        <source>Choose Qt style sheet file</source>
        <translation>Wybierz plik arkusza stylu Qt</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2271"/>
        <source>Qt Style Sheets (*.qss)</source>
        <translation>Akrusze stylu Qt (*.qss)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2283"/>
        <source>Choose temporary work file</source>
        <translation>Wybierz tymczasowy plik roboczy</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2283"/>
        <location filename="../../options.cpp" line="2407"/>
        <location filename="../../options.cpp" line="2419"/>
        <location filename="../../options.cpp" line="2432"/>
        <location filename="../../options.cpp" line="2444"/>
        <location filename="../../options.cpp" line="2495"/>
        <location filename="../../options.cpp" line="2507"/>
        <location filename="../../options.cpp" line="2519"/>
        <location filename="../../options.cpp" line="2531"/>
        <location filename="../../options.cpp" line="2543"/>
        <location filename="../../options.cpp" line="2567"/>
        <location filename="../../options.cpp" line="2579"/>
        <location filename="../../options.cpp" line="2591"/>
        <location filename="../../options.cpp" line="2605"/>
        <location filename="../../options.cpp" line="2646"/>
        <location filename="../../options.cpp" line="2672"/>
        <location filename="../../options.cpp" line="2698"/>
        <location filename="../../options.cpp" line="2710"/>
        <location filename="../../options.cpp" line="2723"/>
        <location filename="../../options.cpp" line="2944"/>
        <location filename="../../options.cpp" line="2956"/>
        <location filename="../../options.cpp" line="2968"/>
        <location filename="../../options.cpp" line="2980"/>
        <location filename="../../options.cpp" line="2992"/>
        <location filename="../../options.cpp" line="3004"/>
        <location filename="../../options.cpp" line="3016"/>
        <location filename="../../options.cpp" line="3028"/>
        <location filename="../../options.cpp" line="3054"/>
        <location filename="../../options.cpp" line="3266"/>
        <source>All files (*)</source>
        <translation>Wszystkie pliki (*)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2295"/>
        <source>Choose preview directory</source>
        <translation>Wybierz katalog podglądów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2309"/>
        <source>Choose flyer directory</source>
        <translation>Wybierz katalog ulotek</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2323"/>
        <source>Choose icon directory</source>
        <translation>Wybierz katalog ikon</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2393"/>
        <source>Choose PCB directory</source>
        <translation>Wybierz katalog płytek drukowanych</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2407"/>
        <source>Choose options template file</source>
        <translation>Wybierz plik szablonu opcji</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2419"/>
        <location filename="../../options.cpp" line="3266"/>
        <source>Choose emulator executable file</source>
        <translation>Wybierz plik wykonywalny emulatora</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2432"/>
        <source>Choose MAME variant&apos;s exe file</source>
        <translation>Wybierz alternatywny plik wykonywalny MAME</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2444"/>
        <source>Choose MESS variant&apos;s exe file</source>
        <translation>Wybierz alternatywny plik wykonywalny MESS</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2459"/>
        <source>MAME variant arguments</source>
        <translation>Parametry wariantu MAME</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2460"/>
        <source>Specify command line arguments passed to the MAME variant
(empty means: &apos;pass the arguments we were called with&apos;):</source>
        <translation>Proszę podać parametry wiersza poleceń do przekazania wariantowi MAME
(puste oznacza: &apos;te same, które nam przekazano&apos;):</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2478"/>
        <source>MESS variant arguments</source>
        <translation>Parametry wariantu MESS</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2479"/>
        <source>Specify command line arguments passed to the MESS variant
(empty means: &apos;pass the arguments we were called with&apos;):</source>
        <translation>Proszę podać parametry wiersza poleceń do przekazania wariantowi MESS
(puste oznacza: &apos;te same, które nam przekazano&apos;):</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2495"/>
        <source>Choose emulator log file</source>
        <translation>Wybierz plik dziennika emulatora</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2507"/>
        <source>Choose XML gamelist cache file</source>
        <translation>Wybierz plik bufora listy gier XML</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2519"/>
        <source>Choose zip tool</source>
        <translation>Wybierz narzędzie zip</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2531"/>
        <source>Choose file removal tool</source>
        <translation>Wybierz narzędzie usuwające pliki</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2543"/>
        <source>Choose ROM tool</source>
        <translation>Wybierz narzędzie do obsługi ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2567"/>
        <source>Choose game favorites file</source>
        <translation>Wybierz plik ulubionych gier</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2579"/>
        <source>Choose play history file</source>
        <translation>Wybierz plik historii gier</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2591"/>
        <source>Choose gamelist cache file</source>
        <translation>Wybierz plik bufora listy gier</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2605"/>
        <source>Choose ROM state cache file</source>
        <translation>Wybierz plik bufora stanów ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2555"/>
        <location filename="../../options.cpp" line="2617"/>
        <location filename="../../options.cpp" line="3278"/>
        <source>Choose working directory</source>
        <translation>Wybierz katalog roboczy</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2646"/>
        <source>Choose software list cache file</source>
        <translation>Wybierz plik bufora listy oprogramowania</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2658"/>
        <source>Choose general software folder</source>
        <translation>Wybierz ogólny folder oprogramowania</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2684"/>
        <source>Choose data directory</source>
        <translation>Wybierz katalog danych</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2723"/>
        <source>Choose catver.ini file</source>
        <translation>Wybierz plik catver.ini</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2944"/>
        <source>Choose ZIP-compressed preview file</source>
        <translation>Wybierz skompresowany plik ZIP podglądów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2956"/>
        <source>Choose ZIP-compressed flyer file</source>
        <translation>Wybierz skompresowany plik ZIP ulotek</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2968"/>
        <source>Choose ZIP-compressed icon file</source>
        <translation>Wybierz skompresowany plik ZIP ikon</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3016"/>
        <source>Choose ZIP-compressed title file</source>
        <translation>Wybierz skompresowany plik ZIP ekranów tytułowych</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3028"/>
        <source>Choose ZIP-compressed PCB file</source>
        <translation>Wybierz skompresowany plik ZIP płytek drukowanych</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3040"/>
        <source>Choose software snap directory</source>
        <translation>Wybierz katalog zrzutów ekranu oprogramowania</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3054"/>
        <source>Choose ZIP-compressed software snap file</source>
        <translation>Wybierz skompresowany plik ZIP zrzutów ekranu oprogramowania</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3236"/>
        <source>shortcut map is clean</source>
        <translation>mapa skrótów jest czysta</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3238"/>
        <source>WARNING: shortcut map contains duplicates</source>
        <translation>UWAGA: mapa skrótów zawiera duplikaty</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="120"/>
        <source>PL (Polish)</source>
        <translation>PL (polski)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3362"/>
        <source>External zip tool, i.e. &quot;zip&quot; (read and execute)</source>
        <translation>Zewnętrzne narzędzie zip, np. &quot;zip&quot; (odczyt i wykonanie)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3428"/>
        <source>External file removal tool, i.e. &quot;rm&quot; (read and execute)</source>
        <translation>Zewnętrzne narzędzie usuwające pliki, np. &quot;rm&quot; (odczyt i wykonanie)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3822"/>
        <source>Emulator executable file (read and execute)</source>
        <translation>Plik wykonywalny emulatora (odczyt i wykonanie)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="272"/>
        <source>Analyze current game</source>
        <translation>Analizuj bieżącą grę</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="297"/>
        <source>Open ROMAlyzer dialog</source>
        <translation>Otwórz okno dialogowe ROMAlyzera</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="115"/>
        <source>FR (French)</source>
        <translation>FR (francuski)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2352"/>
        <source>Game name</source>
        <translation>Nazwa gry</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2857"/>
        <source>&amp;Joystick</source>
        <translation>Dżo&amp;jstik</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2886"/>
        <source>Enable GUI control via joystick</source>
        <translation>Włącz kontrolowanie GUI za pomocą dżojstika</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2889"/>
        <source>Enable joystick control</source>
        <translation>Włącz kontrolowanie dżojstikiem</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2905"/>
        <source>Rescan available joysticks</source>
        <translation>Przeskanuj ponownie dostępne dżojstiki</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2908"/>
        <source>Rescan joysticks</source>
        <translation>Przeskanuj ponownie dżojstiki</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2928"/>
        <source>Select joystick</source>
        <translation>Wybierz dżojstik</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2938"/>
        <source>List of available joysticks - select the one you want to use for GUI control</source>
        <translation>Lista dostępnych dżojstików - wybierz ten, którego chcesz używać do kontrolowania GUI</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2942"/>
        <location filename="../../options.cpp" line="3419"/>
        <location filename="../../options.cpp" line="3442"/>
        <location filename="../../options.cpp" line="3495"/>
        <location filename="../../options.cpp" line="3582"/>
        <source>No joysticks found</source>
        <translation>Nie znaleziono dżojstików</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2953"/>
        <source>Joystick information and settings</source>
        <translation>Informacje o i ustawienia dżojstika</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2970"/>
        <source>Number of joystick axes</source>
        <translation>Liczba osi dżojstika</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2973"/>
        <location filename="../../options.ui" line="2994"/>
        <location filename="../../options.ui" line="3015"/>
        <location filename="../../options.ui" line="3036"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2984"/>
        <source>Buttons:</source>
        <translation>Przyciski:</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2991"/>
        <source>Number of joystick buttons</source>
        <translation>Liczba przycisków dżojstika</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3005"/>
        <source>Hats:</source>
        <translation>Kapturki:</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3012"/>
        <source>Number of coolie hats</source>
        <translation>Liczba kapturków</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3026"/>
        <source>Trackballs:</source>
        <translation>Trackballe:</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3033"/>
        <source>Number of trackballs</source>
        <translation>Liczba trackballi</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3152"/>
        <source>Calibrate joystick axes</source>
        <translation>Skalibruj osie dżojstika</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3174"/>
        <source>Test all joystick functions</source>
        <translation>Przetestuj wszystkie funkcje dżojstika</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2963"/>
        <source>Axes:</source>
        <translation>Osie:</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3062"/>
        <source>Automatically repeat joystick functions after specified delay</source>
        <translation>Automatycznie powtórz funkcje dżojstika po podanym opóźnieniu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3065"/>
        <source>Auto repeat after</source>
        <translation>Powtórz automatycznie po</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3081"/>
        <source>Repeat all joystick functions after how many milliseconds?</source>
        <translation>Powtarzać wszystkie funkcje dżojstika po ilu milisekundach?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3084"/>
        <location filename="../../options.ui" line="3129"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3110"/>
        <source>Event timeout</source>
        <translation>Czas oczekiwania zdarzeń</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3126"/>
        <source>Process joystick events after how many milliseconds?</source>
        <translation>Przetwarzać zdarzenia dżojstika po ilu milisekundach?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2832"/>
        <location filename="../../options.ui" line="3288"/>
        <source>Function / Key</source>
        <translation>Funkcja / Klawisz</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3155"/>
        <source>Calibrate</source>
        <translation>Kalibruj</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3177"/>
        <source>Test</source>
        <translation>Testuj</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3199"/>
        <source>Map</source>
        <translation>Przypisz</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3235"/>
        <source>Remap</source>
        <translation>Przypisz ponownie</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3252"/>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3269"/>
        <source>Active joystick mappings; double-click to remap joystick function</source>
        <translation>Aktywne przypisania dżojstika, kliknij dwa razy aby ponownie przypisać funkcję dżojstika</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3293"/>
        <source>Joystick function</source>
        <translation>Funkcja dżojstika</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3196"/>
        <source>Map joystick functions to GUI functions</source>
        <translation>Przypisz funkcje dżojstika do funkcji GUI</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3232"/>
        <source>Remap a joystick function to the selected GUI function</source>
        <translation>Przypisz ponownie funkcję dżojstika do wybranej funkcji GUI</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3249"/>
        <source>Remove joystick mapping from selected GUI function</source>
        <translation>Usuń przypisanie dżojstika do wybranej funkcji GUI</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1141"/>
        <source>WARNING: can&apos;t initialize joystick</source>
        <translation>UWAGA: nie można zainicjować dżojstika</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1940"/>
        <location filename="../../options.cpp" line="1997"/>
        <location filename="../../options.cpp" line="2935"/>
        <source>SW snap file</source>
        <translation>Plik zrzutów ekranu oprogramowania</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3755"/>
        <source>joystick map is clean</source>
        <translation>mapa dżojstika jest czysta</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3757"/>
        <source>WARNING: joystick map contains duplicates</source>
        <translation>UWAGA: mapa dżojstika zawiera duplikaty</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2698"/>
        <source>Choose game info DB</source>
        <translation>Wybierz bazę danych informacji o grach</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2050"/>
        <source>Game info DB</source>
        <translation>Baza danych informacji o grach</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="987"/>
        <source>Game information database - MAME history.dat (read)</source>
        <translation>Baza danych informacji o grach - MAME history.dat (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1000"/>
        <source>Browse game information database (MAME history.dat)</source>
        <translation>Wskaż bazę danych informacji o grach (MAME history.dat)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2067"/>
        <source>Use in-memory compression for game info DB (a bit slower, but consumes distinctly less memory; compression rate is usually about 1:16)</source>
        <translation>Używaj kompresji w pamięci dla bazy danych informacji o grach (troszkę wolniejsze, lecz zużywa dobitnie mniej pamięci; stopień kompresji to zwykle około 1:16)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1357"/>
        <source>please restart QMC2 for some changes to take effect</source>
        <translation>proszę zrestartować QMC2 aby niektóre zmiany mogły odnieść skutek</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="328"/>
        <location filename="../../options.ui" line="344"/>
        <source>Option requires a restart of QMC2 to take effect</source>
        <translation>Opcja wymaga restartu QMC2 do odniesienia skutku</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2017"/>
        <location filename="../../options.ui" line="2537"/>
        <location filename="../../options.ui" line="4052"/>
        <source>Option requires a reload of the gamelist to take effect</source>
        <translation>Opcja wymaga przeładowania listy gier do odniesienia skutku</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1315"/>
        <source>invalidating game info DB</source>
        <translation>unieważnianie bazy danych informacji o grach</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2047"/>
        <source>Load game information database (MAME history.dat)</source>
        <translation>Ładuj bazę danych informacji o grach (MAME history.dat)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="347"/>
        <source>restart required</source>
        <translation>wymagany restart</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2036"/>
        <location filename="../../options.ui" line="2556"/>
        <location filename="../../options.ui" line="4071"/>
        <source>reload required</source>
        <translation>wymagane przeładowanie</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2458"/>
        <source>Delay update of any game details (preview, flyer, info, configuration, ...) by how many milliseconds?</source>
        <translation>Opóźnić aktualizację dowolnych detali gry (podgląd, ulotka, informacje, konfiguracja, ...) o ile milisekund?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2461"/>
        <source>none</source>
        <translation>brak</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1334"/>
        <source>invalidating emulator info DB</source>
        <translation>unieważnianie bazy danych informacji o emulatorze</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2710"/>
        <source>Choose emulator info DB</source>
        <translation>Wybierz bazę danych informacji o emulatorze</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2085"/>
        <source>Load emulator information database (mameinfo.dat)</source>
        <translation>Ładuj bazę danych informacji o emulatorze (mameinfo.dat)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2105"/>
        <source>Use in-memory compression for emulator info DB (a bit slower, but consumes distinctly less memory; compression rate is usually about 1:16)</source>
        <translation>Używaj kompresji w pamięci dla bazy danych informacji o emulatorze (troszkę wolniejsze, lecz zużywa dobitnie mniej pamięci; stopień kompresji to zwykle około 1:16)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2088"/>
        <source>Emu info DB</source>
        <translation>Baza danych informacji o emulatorze</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1017"/>
        <source>Emulator information database - mameinfo.dat (read)</source>
        <translation>Baza danych informacji o emulatorze - mameinfo.dat (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1030"/>
        <source>Browse emulator information database (mameinfo.dat)</source>
        <translation>Wskaż bazę danych informacji o emulatorze (mameinfo.dat)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="448"/>
        <location filename="../../options.ui" line="483"/>
        <source>unlimited</source>
        <translation>nieograniczony</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="451"/>
        <location filename="../../options.ui" line="486"/>
        <source> lines</source>
        <translation> linii</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="432"/>
        <source>Emulator log size</source>
        <translation>Rozmiar dziennika emulatora</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="445"/>
        <source>Maximum number of lines to keep in emulator log browser</source>
        <translation>Maksymalna liczba linii do zachowania w przeglądarce dziennika emulatora</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="331"/>
        <source>Previous track (audio player)</source>
        <translation>Poprzednia ścieżka (odtwarzacz dźwięku)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="332"/>
        <source>Next track (audio player)</source>
        <translation>Następna ścieżka (odtwarzacz dźwięku)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="333"/>
        <source>Fast backward (audio player)</source>
        <translation>Przewijanie wstecz (odtwarzacz dźwięku)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="334"/>
        <source>Fast forward (audio player)</source>
        <translation>Przewijanie do przodu (odtwarzacz dźwięku)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="335"/>
        <source>Stop track (audio player)</source>
        <translation>Zatrzymaj ścieżkę (odtwarzacz dźwięku)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="336"/>
        <source>Pause track (audio player)</source>
        <translation>Wstrzymaj ścieżkę (odtwarzacz dźwięku)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="337"/>
        <source>Play track (audio player)</source>
        <translation>Odtwarzaj ścieżkę (odtwarzacz dźwięku)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="319"/>
        <source>Parent / clone hierarchy</source>
        <translation>Hierarchia macierzysty / klon</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="338"/>
        <source>Raise volume (audio player)</source>
        <translation>Zwiększ głośność (odtwarzacz dźwięku)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="339"/>
        <source>Lower volume (audio player)</source>
        <translation>Zmniejsz głośność (odtwarzacz dźwięku)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="274"/>
        <source>Export ROM Status</source>
        <translation>Eksportuj stan ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="586"/>
        <source>Fall back to the parent&apos;s image if an indivual image is missing but there&apos;s one for the parent</source>
        <translation>Wycofaj się do obrazu ROM-u macierzystego jeśli obrazu nie ma, lecz istnieje dla ROM-u macierzystego</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="589"/>
        <source>Parent image fallback</source>
        <translation>Wycofanie do obrazu ROM-u macierzystego</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="271"/>
        <source>Show vertical game status indicator in game details</source>
        <translation>Pokaż pionowy wskaźnik stanu gry wsród szczegółów gry</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="274"/>
        <source>Game status indicator</source>
        <translation>Wskaźnik stanu gry</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="284"/>
        <source>Show the game status indicator only when the game list is not visible due to the current layout</source>
        <translation>Pokaż wskaźnik stanu gry tylko gdy lista gier nie jest widoczna w bieżącym układzie</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="287"/>
        <location filename="../../options.ui" line="313"/>
        <source>Only when required</source>
        <translation>Tylko kiedy potrzeba</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="297"/>
        <source>Show game name</source>
        <translation>Pokaż nazwę gry</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="310"/>
        <source>Show game&apos;s description only when the game list is not visible due to the current layout</source>
        <translation>Pokaż opis gry tylko gdy lista gier nie jest widoczna w bieżącym układzie</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="231"/>
        <source>Show the menu bar</source>
        <translation>Pokazuj pasek menu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="234"/>
        <source>Show menu bar</source>
        <translation>Pokaż pasek menu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="218"/>
        <location filename="../../options.ui" line="221"/>
        <source>Show status bar</source>
        <translation>Pokaż pasek stanu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="729"/>
        <location filename="../../options.ui" line="732"/>
        <source>Show tool bar</source>
        <translation>Pokaż pasek narzędzi</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="298"/>
        <source>Toggle ROM state C</source>
        <translation>Przełącz stan ROM-ów P</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="299"/>
        <source>Toggle ROM state M</source>
        <translation>Przełącz stan ROM-ów Wp</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="300"/>
        <source>Toggle ROM state I</source>
        <translation>Przełącz stan ROM-ów Np</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="301"/>
        <source>Toggle ROM state N</source>
        <translation>Przełącz stan ROM-ów Nz</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="302"/>
        <source>Toggle ROM state U</source>
        <translation>Przełącz stan ROM-ów Nn</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="261"/>
        <source>Check for other instances of this QMC2 variant on startup</source>
        <translation>Sprawdź, czy nie ma innych instancji tego wariantu QMC2 podczas startu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="264"/>
        <source>Check single instance</source>
        <translation>Sprawdź pojedynczą instancję</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="308"/>
        <source>Launch QMC2 for SDLMAME</source>
        <translation>Uruchom QMC2 dla SDLMAME</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="309"/>
        <source>Launch QMC2 for SDLMESS</source>
        <translation>Uruchom QMC2 dla SDLMESS</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="223"/>
        <source>Machine info DB</source>
        <translation>Baza danych informacji o maszynach</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="226"/>
        <source>Machine information database - MESS sysinfo.dat (read)</source>
        <translation>Baza danych informacji o maszynach - MESS sysinfo.dat (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="227"/>
        <source>Browse machine information database (MESS sysinfo.dat)</source>
        <translation>Wskaż bazę danych informacji o maszynach (MESS sysinfo.dat)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="596"/>
        <source>Minimize application windows when launching another variant</source>
        <translation>Minimalizuj okna aplikacji podczas uruchamiania innego wariantu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="599"/>
        <source>Minimize on variant launch</source>
        <translation>Minimalizuj podczas uruchamiania wariantu</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="327"/>
        <source>Toggle arcade mode</source>
        <translation>Przełącz tryb salonowy</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="328"/>
        <source>Show FPS (arcade mode)</source>
        <translation>Pokaż FPS (tryb salonowy)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="329"/>
        <source>Take snapshot (arcade mode)</source>
        <translation>Pobierz zrzut ekranu (tryb salonowy)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="279"/>
        <source>Setup arcade mode</source>
        <translation>Ustawienia trybu salonowego</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="229"/>
        <source>Machine description</source>
        <translation>Opis maszyny</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="230"/>
        <source>Machine name</source>
        <translation>Nazwa maszyny</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="231"/>
        <source>Number of item insertions between machine list updates during reload (higher means faster, but makes the GUI less responsive)</source>
        <translation>Liczba wstawień elementów pomiędzy aktualizacjami listy maszyn podczas przeładowywania (wyższa oznacza większą szybkość, lecz czyni interfejs mniej czułym)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="232"/>
        <source>Delay update of any machine details (preview, flyer, info, configuration, ...) by how many milliseconds?</source>
        <translation>Opóźnić aktualizację dowolnych detali maszyny (podgląd, ulotka, informacje, konfiguracja, ...) o ile milisekund?</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="233"/>
        <source>Sort machine list while reloading (slower)</source>
        <translation>Sortuj listę maszyn podczas przeładowywania (wolniejsze)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2662"/>
        <source>Sort game list while reloading (slower)</source>
        <translation>Sortuj listę gier podczas przeładowywania (wolniejsze)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2426"/>
        <source>Number of item insertions between game list updates during reload (higher means faster, but makes the GUI less responsive)</source>
        <translation>Liczba wstawień elementów pomiędzy aktualizacjami listy gier podczas przeładowywania (wyższa oznacza większą szybkość, lecz czyni interfejs mniej czułym)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="25"/>
        <source>&amp;Front end</source>
        <translation>I&amp;nterfejs</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="559"/>
        <source>Application style (Default = use system&apos;s default style)</source>
        <translation>Styl aplikacji (Domyślny = używaj domyślnego stylu systemowego)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2614"/>
        <source>Automatically trigger a ROM check if necessary</source>
        <translation>Automatycznie uruchamiaj sprawdzanie ROM-ów jeśli konieczne</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="305"/>
        <source>Launch QMC2 for MAME</source>
        <translation>Uruchom QMC2 dla MAME</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="306"/>
        <source>Launch QMC2 for MESS</source>
        <translation>Uruchom QMC2 dla MESS</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3349"/>
        <location filename="../../options.ui" line="3415"/>
        <location filename="../../options.ui" line="3494"/>
        <source>Command</source>
        <translation>Polecenie</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2665"/>
        <source>Sort while loading</source>
        <translation>Sortuj podczas ładowania</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2486"/>
        <source>Launch emulation on double-click events (may be annoying)</source>
        <translation>Rozpoczynanie emulacji po zdarzeniach dwukrotnego kliknięcia (może być denerwujące)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2489"/>
        <source>Double-click activation</source>
        <translation>Uruchamianie podwójnym kliknięciem</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1424"/>
        <source>sorting game list by %1 in %2 order</source>
        <translation>sortowanie listy gier według %1 w porządku %2</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1426"/>
        <source>sorting machine list by %1 in %2 order</source>
        <translation>sortowanie listy maszyn według %1 w porządku %2</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1208"/>
        <source>An open game-specific emulator configuration has been detected.
Use local game-settings, overwrite with global settings or don&apos;t apply?</source>
        <translation>Wykryto otwartą, dostosowaną do gry konfigurację emulatora.
Użyć ustawień lokalnych gry, nadpisać ustawieniami globalnymi czy nie zastosowywać?</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1210"/>
        <source>An open machine-specific emulator configuration has been detected.
Use local machine-settings, overwrite with global settings or don&apos;t apply?</source>
        <translation>Wykryto otwartą, dostosowaną do maszyny konfigurację emulatora.
Użyć ustawień lokalnych maszyny, nadpisać ustawieniami globalnymi czy nie zastosowywać?</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="326"/>
        <source>Toggle full screen</source>
        <translation>Przełącz tryb pełnoekranowy</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="201"/>
        <location filename="../../options.cpp" line="211"/>
        <source>Specify arguments...</source>
        <translation>Proszę podać parametry...</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="204"/>
        <location filename="../../options.cpp" line="214"/>
        <source>Reset to default (same path assumed)</source>
        <translation>Przywrócenie domyślnych (ta sama ścieżka)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="220"/>
        <source>Browse emulator information database (messinfo.dat)</source>
        <translation>Wskaż bazę danych informacji o emulatorze (messinfo.dat)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="221"/>
        <source>Load emulator information database (messinfo.dat)</source>
        <translation>Ładuj bazę danych informacji o emulatorze (messinfo.dat)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="222"/>
        <source>Emulator information database - messinfo.dat (read)</source>
        <translation>Baza danych informacji o emulatorze - messinfo.dat (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="224"/>
        <source>Load machine information database (MESS sysinfo.dat)</source>
        <translation>Ładuj bazę danych informacji o maszynach (plik sysinfo.dat MESS)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="225"/>
        <source>Use in-memory compression for machine info DB (a bit slower, but consumes distinctly less memory; compression rate is usually about 1:16)</source>
        <translation>Używaj kompresji w pamięci dla bazy danych informacji o maszynach (troszkę wolniejsze, lecz zużywa dobitnie mniej pamięci; stopień kompresji to zwykle około 1:16)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="249"/>
        <location filename="../../options.cpp" line="250"/>
        <source>Option requires a reload of the entire machine list to take effect</source>
        <translation>Opcja wymaga przeładowania całej listy maszyn do odniesienia skutku</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="251"/>
        <source>Hide primary machine list while loading (recommended, much faster)</source>
        <translation>Ukryj główną listę maszyn podczas ładowania (zalecane, znacznie szybsze)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="252"/>
        <source>Registered emulators -- you may select one of these in the machine-specific emulator configuration</source>
        <translation>Zarejestrowane emulatory -- można wybrać jeden z nich w specyficznej dla maszyny konfiguracji emulatora</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="253"/>
        <source>Save machine selection</source>
        <translation>Zapisz wybór maszyny</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="254"/>
        <source>Save machine selection on exit and before reloading the machine list</source>
        <translation>Zapisz wybraną maszynę podczas wyjścia i przed przeładowaniem listy maszyn</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="255"/>
        <source>Restore machine selection</source>
        <translation>Przywróć wybór maszyny</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="256"/>
        <source>Restore saved machine selection at start and after reloading the machine list</source>
        <translation>Przywróć wybraną maszyny podczas uruchomienia i po przeładowaniu listy maszyn</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="258"/>
        <location filename="../../options.cpp" line="964"/>
        <source>Category</source>
        <translation>Kategoria</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="259"/>
        <location filename="../../options.cpp" line="965"/>
        <source>Version</source>
        <translation>Wersja</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="273"/>
        <source>Analyze tagged sets</source>
        <translation>Analizuj zaznaczone zestawy</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="276"/>
        <source>Copy tagged sets to favorites</source>
        <translation>Skopiuj zaznaczone zestawy do ulubionych</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="283"/>
        <source>Play (independent)</source>
        <translation>Graj (niezależnie)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="285"/>
        <source>Play (embedded)</source>
        <translation>Graj (osadzaj)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="378"/>
        <location filename="../../options.cpp" line="385"/>
        <source>No style sheet</source>
        <translation>Brak arkusza stylu</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="968"/>
        <source>View games by category (not filtered)</source>
        <translation>Wyświetlaj gry według kategorii (niefiltrowane)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="970"/>
        <source>View games by emulator version (not filtered)</source>
        <translation>Wyświetlaj gry według wersji emulatora (niefiltrowane)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1367"/>
        <source>re-sort of game list impossible at this time, please wait for ROM verification to finish and try again</source>
        <translation>ponownie posortowanie listy gier jest obecnie niemożliwe, proszę poczekać na ukończenie weryfikacji ROM-ów i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1645"/>
        <location filename="../../options.cpp" line="1649"/>
        <source>FATAL: can&apos;t open cabinet file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku obrazu automatu, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1663"/>
        <location filename="../../options.cpp" line="1667"/>
        <source>FATAL: can&apos;t open controller file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku kontrolera, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1764"/>
        <source>triggering automatic reload of game list</source>
        <translation>uruchamianie automatycznego przeładowania listy gier</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1766"/>
        <source>triggering automatic reload of machine list</source>
        <translation>uruchamianie automatycznego przeładowania listy maszyn</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1915"/>
        <location filename="../../options.cpp" line="1972"/>
        <location filename="../../options.cpp" line="2880"/>
        <source>Cabinet file</source>
        <translation>Plik automatów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1397"/>
        <location filename="../../options.cpp" line="1915"/>
        <location filename="../../options.cpp" line="1972"/>
        <location filename="../../options.cpp" line="2880"/>
        <source>Cabinet directory</source>
        <translation>Katalog automatów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1920"/>
        <location filename="../../options.cpp" line="1977"/>
        <location filename="../../options.cpp" line="2891"/>
        <source>Controller file</source>
        <translation>Plik kontrolerów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1500"/>
        <location filename="../../options.cpp" line="1920"/>
        <location filename="../../options.cpp" line="1977"/>
        <location filename="../../options.cpp" line="2891"/>
        <source>Controller directory</source>
        <translation>Katalog kontrolerów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2337"/>
        <source>Choose cabinet directory</source>
        <translation>Wybierz katalog automatów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2351"/>
        <source>Choose controller directory</source>
        <translation>Wybierz katalog kontrolerów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2980"/>
        <source>Choose ZIP-compressed cabinet file</source>
        <translation>Wybierz skompresowany plik ZIP automatów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2992"/>
        <source>Choose ZIP-compressed controller file</source>
        <translation>Wybierz skompresowany plik ZIP kontrolerów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="294"/>
        <source>Show game&apos;s description at the bottom of any images</source>
        <translation>Pokaż opis gry na dole obrazów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2033"/>
        <location filename="../../options.ui" line="2553"/>
        <location filename="../../options.ui" line="4068"/>
        <source>Option requires a reload of the entire game list to take effect</source>
        <translation>Opcja wymaga przeładowania całej listy gier do odniesienia skutku</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="191"/>
        <source>Scaled cabinet</source>
        <translation>Skalowany automat</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="201"/>
        <source>Scaled controller</source>
        <translation>Skalowany kontroler</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="648"/>
        <source>Show indicator for current memory usage</source>
        <translation>Pokaż wskaźnik bieżącego zużycia pamięci</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1432"/>
        <source>Cabinet directory (read)</source>
        <translation>Katalog automatów (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1445"/>
        <source>Browse cabinet directory</source>
        <translation>Wskaż katalog automatów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1469"/>
        <source>ZIP-compressed cabinet file (read)</source>
        <translation>Skompresowany plik ZIP automatów (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1482"/>
        <source>Browse ZIP-compressed cabinet file</source>
        <translation>Wskaż skompresowany plik ZIP automatów (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1394"/>
        <source>Switch between specifying a cabinet directory or a ZIP-compressed cabinet file</source>
        <translation>Przełącz pomiędzy określeniem katalogu automatów a skompresowanym plikiem ZIP automatów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1535"/>
        <source>Controller directory (read)</source>
        <translation>Katalog kontrolerów (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1548"/>
        <source>Browse controller directory</source>
        <translation>Wskaż katalog kontrolerów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1572"/>
        <source>ZIP-compressed controller file (read)</source>
        <translation>Skompresowany plik ZIP kontrolerów (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1585"/>
        <source>Browse ZIP-compressed controller file</source>
        <translation>Wskaż skompresowany plik ZIP kontrolerów</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1497"/>
        <source>Switch between specifying a controller directory or a ZIP-compressed controller file</source>
        <translation>Przełącz pomiędzy określeniem katalogu kontrolerów a skompresowanym plikiem ZIP kontrolerów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1681"/>
        <location filename="../../options.cpp" line="1685"/>
        <source>FATAL: can&apos;t open marquee file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku planszy tytułowej, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1699"/>
        <location filename="../../options.cpp" line="1703"/>
        <source>FATAL: can&apos;t open title file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku ekranu tytułowego, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1925"/>
        <location filename="../../options.cpp" line="1982"/>
        <location filename="../../options.cpp" line="2902"/>
        <source>Marquee file</source>
        <translation>Plik plansz tytułowych</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1603"/>
        <location filename="../../options.cpp" line="1925"/>
        <location filename="../../options.cpp" line="1982"/>
        <location filename="../../options.cpp" line="2902"/>
        <source>Marquee directory</source>
        <translation>Katalog plansz tytułowych</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1930"/>
        <location filename="../../options.cpp" line="1987"/>
        <location filename="../../options.cpp" line="2913"/>
        <source>Title file</source>
        <translation>Plik ekranów tytułowych</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1706"/>
        <location filename="../../options.cpp" line="1930"/>
        <location filename="../../options.cpp" line="1987"/>
        <location filename="../../options.cpp" line="2913"/>
        <source>Title directory</source>
        <translation>Katalog ekranów tytułowych</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2365"/>
        <source>Choose marquee directory</source>
        <translation>Wybierz katalog plansz tytułowych</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2379"/>
        <source>Choose title directory</source>
        <translation>Wybierz katalog ekranów tytułowych</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3004"/>
        <source>Choose ZIP-compressed marquee file</source>
        <translation>Wybierz skompresowany plik ZIP plansz tytułowych</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="211"/>
        <source>Scaled marquee</source>
        <translation>Skalowana plansza tytułowa</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="359"/>
        <source>Scaled title</source>
        <translation>Skalowany ekran tytułowy</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1638"/>
        <source>Marquee directory (read)</source>
        <translation>Katalog plansz tytułowych (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1651"/>
        <source>Browse marquee directory</source>
        <translation>Wskaż katalog plansz tytułowych</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1675"/>
        <source>ZIP-compressed marquee file (read)</source>
        <translation>Skompresowany plik ZIP plansz tytułowych (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1688"/>
        <source>Browse ZIP-compressed marquee file</source>
        <translation>Wskaż skompresowany plik ZIP plansz tytułowych</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1600"/>
        <source>Switch between specifying a marquee directory or a ZIP-compressed marquee file</source>
        <translation>Przełącz pomiędzy określeniem katalogu plansz tytułowych a skompresowanym plikiem ZIP plansz tytułowych</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1741"/>
        <source>Title directory (read)</source>
        <translation>Katalog ekranów tytułowych (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1754"/>
        <source>Browse title directory</source>
        <translation>Wskaż katalog ekranów tytułowych</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1778"/>
        <source>ZIP-compressed title file (read)</source>
        <translation>Skompresowany plik ZIP ekranów tytułowych (odczyt)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1791"/>
        <source>Browse ZIP-compressed title file</source>
        <translation>Wskaż skompresowany plik ZIP ekranów tytułowych</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1703"/>
        <source>Switch between specifying a title directory or a ZIP-compressed title file</source>
        <translation>Przełącz pomiędzy określeniem katalogu ekranów tytułowych a skompresowanym plikiem ZIP ekranów tytułowych</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="228"/>
        <source>Machine &amp;list</source>
        <translation>&amp;Lista maszyn</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2155"/>
        <source>Game &amp;list</source>
        <translation>&amp;Lista gier</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="467"/>
        <source>Front end log size</source>
        <translation>Rozmiar dziennika interfejsu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="480"/>
        <source>Maximum number of lines to keep in front end log browser</source>
        <translation>Maksymalna liczba linii zachowywana w przeglądarce dziennika interfejsu </translation>
    </message>
    <message>
        <location filename="../../options.ui" line="833"/>
        <source>Front end log file</source>
        <translation>Plik dziennika interfejsu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="846"/>
        <source>Front end log file (write)</source>
        <translation>Plik dziennika interfejsu (zapis)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="859"/>
        <source>Browse front end log file</source>
        <translation>Wskaż plik dziennika interfejsu</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2672"/>
        <source>Choose front end log file</source>
        <translation>Wybierz plik dziennika interfejsu</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2632"/>
        <source>Choose MAWS cache directory</source>
        <translation>Wybierz katalog bufora MAWS</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4080"/>
        <source>MAWS cache directory</source>
        <translation>Katalog bufora MAWS</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4093"/>
        <source>MAWS cache directory (write)</source>
        <translation>Katalog bufora MAWS (zapis)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4106"/>
        <source>Browse MAWS cache directory</source>
        <translation>Wskaż katalog bufora MAWS</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3620"/>
        <source>Use HTTP proxy</source>
        <translation>Używaj pośrednika HTTP</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3632"/>
        <source>Host / IP</source>
        <translation>Serwer / IP</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3645"/>
        <source>Hostname or IP address of the HTTP proxy server</source>
        <translation>Nazwa serwera lub adres IP pośrednika HTTP</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3652"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3665"/>
        <source>Port to access the HTTP proxy service</source>
        <translation>Port do dostępu do usługi pośrednika HTTP</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3678"/>
        <source>User ID</source>
        <translation>ID użytkownika</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3691"/>
        <source>User ID to access the HTTP proxy service (empty = no authentication)</source>
        <translation>ID użytkownika dostępu do usługi pośrednika HTTP (puste = bez autoryzacji)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3698"/>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3617"/>
        <source>Enable / disable the use of an HTTP proxy on any web lookups</source>
        <translation>Korzystaj / nie korzystaj z pośrednika HTTP do jakichkolwiek odwołań do sieci</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3711"/>
        <source>Password to access the HTTP proxy service (empty = no authentication)</source>
        <translation>Hasło dostępu do usługi pośrednika HTTP (puste = bez autoryzacji)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="280"/>
        <source>Clear MAWS cache</source>
        <translation>Wyczyść bufor MAWS</translation>
    </message>
</context>
<context>
    <name>PCB</name>
    <message>
        <location filename="../../pcb.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>Kopiuj do schowka</translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="56"/>
        <location filename="../../pcb.cpp" line="57"/>
        <source>Game PCB image</source>
        <translation>Obraz płytki drukowanej gry</translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="59"/>
        <location filename="../../pcb.cpp" line="60"/>
        <source>Machine PCB image</source>
        <translation>Obraz płytki drukowanej maszyny</translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="68"/>
        <location filename="../../pcb.cpp" line="72"/>
        <source>FATAL: can&apos;t open PCB file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku płytki drukowanej, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Oczekiwanie na dane...</translation>
    </message>
</context>
<context>
    <name>Preview</name>
    <message>
        <location filename="../../preview.cpp" line="51"/>
        <source>Copy to clipboard</source>
        <translation>Kopiuj do schowka</translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="58"/>
        <location filename="../../preview.cpp" line="59"/>
        <source>Game preview image</source>
        <translation>Obraz podglądu gry</translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="70"/>
        <location filename="../../preview.cpp" line="74"/>
        <source>FATAL: can&apos;t open preview file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku podglądu, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="102"/>
        <source>Waiting for data...</source>
        <translation>Oczekiwanie na dane...</translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="61"/>
        <location filename="../../preview.cpp" line="62"/>
        <source>Machine preview image</source>
        <translation>Obraz podglądu maszyny</translation>
    </message>
</context>
<context>
    <name>ProcessManager</name>
    <message>
        <location filename="../../procmgr.cpp" line="99"/>
        <location filename="../../procmgr.cpp" line="101"/>
        <source>starting emulator #%1, command = %2</source>
        <translation>uruchamianie emulatora #%1, polecenie = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="131"/>
        <source>terminating emulator #%1, PID = %2</source>
        <translation>kończenie emulatora #%1, PID = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="154"/>
        <source>killing emulator #%1, PID = %2</source>
        <translation>zabijanie emulatora #%1, PID = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="236"/>
        <source>emulator #%1 finished, exit code = %2, exit status = %3, remaining emulators = %4</source>
        <translation>emulator #%1 zakończył pracę, kod wyjścia = %2, stan wyjścia = %3, pozostałych emulatorów = %4</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="236"/>
        <source>normal</source>
        <translation>normalny</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="236"/>
        <source>crashed</source>
        <translation>przestał działać</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="281"/>
        <source>emulator #%1 started, PID = %2, running emulators = %3</source>
        <translation>emulator #%1 uruchomiony, PID = %2, uruchomionych emulatorów = %3</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="320"/>
        <source>FATAL: failed to start emulator #%1</source>
        <translation>FATALNIE: nie udało się uruchomić emulatora #%1</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="325"/>
        <source>WARNING: emulator #%1 crashed</source>
        <translation>UWAGA: emulator #%1 uległ awarii</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="329"/>
        <source>WARNING: failed to write to emulator #%1</source>
        <translation>UWAGA: nie udało się zapisać do emulatora #%1</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="333"/>
        <source>WARNING: failed to read from emulator #%1</source>
        <translation>UWAGA: nie udało się odczytać z emulatora #%1</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="337"/>
        <source>WARNING: unhandled error for emulator #%1, error code = %2</source>
        <translation>UWAGA: nieobsługiwany błąd emulatora #%1, kod błędu = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="355"/>
        <source>no error</source>
        <translation>bez błędu</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="356"/>
        <source>failed validity checks</source>
        <translation>nie powiodły się testy walidacji</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="357"/>
        <source>missing files</source>
        <translation>brakujące pliki</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="358"/>
        <source>fatal error</source>
        <translation>błąd krytyczny</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="359"/>
        <source>device initialization error</source>
        <translation>błąd inicjalizacji urządzenia</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="361"/>
        <source>game doesn&apos;t exist</source>
        <translation>gra nie istnieje</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="363"/>
        <source>machine doesn&apos;t exist</source>
        <translation>maszyna nie istnieje</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="365"/>
        <source>invalid configuration</source>
        <translation>nieprawidłowa konfiguracja</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="366"/>
        <source>identified all non-ROM files</source>
        <translation>zidentyfikowano wszystkie pliki nie będące ROM-ami</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="367"/>
        <source>identified some files but not all</source>
        <translation>zidentyfikowano niektóre pliki, jednakże nie wszystkie</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="368"/>
        <source>identified no files</source>
        <translation>nie zidentyfikowano żadnych plików</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="369"/>
        <source>unknown error</source>
        <translation>nieznany błąd</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="185"/>
        <source>stdout[#%1]:</source>
        <translation>stdout[#%1]:</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="61"/>
        <source>WARNING: ProcessManager::start(): the specified working directory &apos;%1&apos; is not a directory -- ignored</source>
        <translation>UWAGA: ProcessManager::start(): wskazany katalog roboczy &apos;%1&apos; nie jest katalogiem -- zignorowano</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="64"/>
        <source>WARNING: ProcessManager::start(): the specified working directory &apos;%1&apos; does not exist -- ignored</source>
        <translation>UWAGA: ProcessManager::start(): wskazany katalog roboczy &apos;%1&apos; nie istnieje -- zignorowano</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="203"/>
        <source>stderr[#%1]:</source>
        <translation>stderr[#%1]:</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="145"/>
        <source>WARNING: ProcessManager::terminate(ushort index = %1): trying to terminate a null process</source>
        <translation>UWAGA: ProcessManager::terminate(ushort index = %1): próba zakończenia procesu zerowego</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="168"/>
        <source>WARNING: ProcessManager::kill(ushort index = %1): trying to kill a null process</source>
        <translation>UWAGA: ProcessManager::terminate(ushort index = %1): próba zabicia procesu zerowego</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="233"/>
        <source>WARNING: ProcessManager::finished(...): trying to remove a null item</source>
        <translation>UWAGA: ProcessManager::finished(...): próba usunięcia elementu zerowego</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../gamelist.cpp" line="2086"/>
        <location filename="../../options.cpp" line="1409"/>
        <source>players</source>
        <translation>gracze</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2093"/>
        <location filename="../../options.cpp" line="1416"/>
        <source>category</source>
        <translation>kategoria</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2096"/>
        <location filename="../../options.cpp" line="1419"/>
        <source>version</source>
        <translation>wersja</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2083"/>
        <location filename="../../options.cpp" line="1406"/>
        <source>ROM types</source>
        <translation>Rodzaje ROM-ów</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="802"/>
        <location filename="../../options.cpp" line="1845"/>
        <source>Default</source>
        <translation>Domyślny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3597"/>
        <location filename="../../qmc2main.cpp" line="3598"/>
        <source>Export game-specific MAME configuration</source>
        <translation>Eksportuj specyficzną dla gry konfigurację MAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3603"/>
        <location filename="../../qmc2main.cpp" line="9499"/>
        <source>Import from...</source>
        <translation>Importuj z...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3605"/>
        <location filename="../../qmc2main.cpp" line="3606"/>
        <source>Import game-specific MAME configuration</source>
        <translation>Importuj specyficzną dla gry konfigurację MAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9440"/>
        <location filename="../../qmc2main.cpp" line="9442"/>
        <source>M.A.M.E. Catalog / Launcher II v</source>
        <translation>M.A.M.E. Catalog / Launcher II wersja </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9446"/>
        <source>SVN r%1</source>
        <translation>SVN wersja %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9477"/>
        <source>processing global emulator configuration</source>
        <translation>przetwarzanie globalnej konfiguracji emulatora</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9491"/>
        <source>Export to...</source>
        <translation>Eksportuj do...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9493"/>
        <location filename="../../qmc2main.cpp" line="9494"/>
        <source>Export global MAME configuration</source>
        <translation>Eksportuj globalną konfigurację MAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9501"/>
        <location filename="../../qmc2main.cpp" line="9502"/>
        <source>Import global MAME configuration</source>
        <translation>Importuj globalną konfigurację MAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9517"/>
        <location filename="../../qmc2main.cpp" line="9525"/>
        <source>&lt;inipath&gt;/mame.ini</source>
        <translation>&lt;ścieżka ini&gt;/mame.ini</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9521"/>
        <location filename="../../qmc2main.cpp" line="9529"/>
        <source>Select file...</source>
        <translation>Wybierz plik...</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2059"/>
        <location filename="../../options.cpp" line="1381"/>
        <source>game description</source>
        <translation>opisu gry</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2065"/>
        <location filename="../../options.cpp" line="1387"/>
        <source>ROM state</source>
        <translation>stan ROM-u</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2068"/>
        <location filename="../../options.cpp" line="1390"/>
        <source>tag</source>
        <translation>zaznaczenie</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2071"/>
        <location filename="../../options.cpp" line="1393"/>
        <source>year</source>
        <translation>roku</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2074"/>
        <location filename="../../options.cpp" line="1396"/>
        <source>manufacturer</source>
        <translation>producenta</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2089"/>
        <location filename="../../options.cpp" line="1412"/>
        <source>driver status</source>
        <translation>stan sterownika</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3012"/>
        <source>correct</source>
        <translation>poprawny</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3047"/>
        <source>incorrect</source>
        <translation>niepoprawny</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3082"/>
        <source>mostly correct</source>
        <translation>w większości poprawny</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3117"/>
        <source>not found</source>
        <translation>nieznaleziony</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3152"/>
        <location filename="../../gamelist.cpp" line="3190"/>
        <location filename="../../romalyzer.cpp" line="2843"/>
        <location filename="../../romalyzer.cpp" line="2876"/>
        <location filename="../../romalyzer.cpp" line="2888"/>
        <source>unknown</source>
        <translation>nieznany</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2827"/>
        <source>ROM</source>
        <translation>ROM</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="757"/>
        <location filename="../../romalyzer.cpp" line="2827"/>
        <source>CHD</source>
        <translation>CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2831"/>
        <location filename="../../romalyzer.cpp" line="2873"/>
        <location filename="../../romalyzer.cpp" line="2893"/>
        <source>good</source>
        <translation>dobry</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="758"/>
        <location filename="../../romalyzer.cpp" line="2835"/>
        <location filename="../../romalyzer.cpp" line="2882"/>
        <source>no dump</source>
        <translation>brak zrzutu</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2839"/>
        <location filename="../../romalyzer.cpp" line="2885"/>
        <source>bad dump</source>
        <translation>zły zrzut</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2879"/>
        <source>no / bad dump</source>
        <translation>brak zrzutu / zły zrzut</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2078"/>
        <location filename="../../options.cpp" line="1400"/>
        <source>game name</source>
        <translation>nazwa gry</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9464"/>
        <source>OpenGL features enabled</source>
        <translation>Funkcje OpenGL włączone</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9496"/>
        <location filename="../../qmc2main.cpp" line="9497"/>
        <source>Export global MESS configuration</source>
        <translation>Eksportuj globalną konfigurację MESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9504"/>
        <location filename="../../qmc2main.cpp" line="9505"/>
        <source>Import global MESS configuration</source>
        <translation>Importuj globalną konfigurację MESS</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2061"/>
        <location filename="../../options.cpp" line="1383"/>
        <source>machine description</source>
        <translation>opisu maszyny</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2080"/>
        <location filename="../../options.cpp" line="1402"/>
        <source>machine name</source>
        <translation>nazwy maszyny</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9473"/>
        <source>SDL joystick support enabled - using SDL v%1.%2.%3</source>
        <translation>Wsparcie dla dżojstika SDL włączone - używanie SDL w wersji %1.%2.%3</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9519"/>
        <location filename="../../qmc2main.cpp" line="9527"/>
        <source>&lt;inipath&gt;/mess.ini</source>
        <translation>&lt;ścieżka ini&gt;/mess.ini</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3600"/>
        <location filename="../../qmc2main.cpp" line="3601"/>
        <source>Export machine-specific MESS configuration</source>
        <translation>Eksportuj specyficzną dla maszyny konfigurację MESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3608"/>
        <location filename="../../qmc2main.cpp" line="3609"/>
        <source>Import machine-specific MESS configuration</source>
        <translation>Importuj specyficzną dla maszyny konfigurację MESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9438"/>
        <source>M.E.S.S. Catalog / Launcher II v</source>
        <translation>M.E.S.S. Catalog / Launcher II wersja </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9468"/>
        <source>Phonon features enabled - using Phonon v%1</source>
        <translation>Funkcje Phonona włączone - używanie Phonon w wersji %1</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1619"/>
        <source>video player: XML error: fatal error on line %1, column %2: %3</source>
        <translation>odtwarzacz filmów: błąd XML: fatalny błąd w linii %1, kolumna %2: %3</translation>
    </message>
</context>
<context>
    <name>ROMAlyzer</name>
    <message>
        <location filename="../../romalyzer.cpp" line="2511"/>
        <source>checksum wizard: FATAL: can&apos;t open ZIP archive &apos;%1&apos; for reading</source>
        <translation>asystent sum kontrolnych: FATALNIE: nie można otworzyć archiwum ZIP &apos;%1&apos; do odczytu</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2534"/>
        <source>Repairing set &apos;%1&apos; - %2</source>
        <translation>Naprawianie seta &apos;%1&apos; - &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2535"/>
        <source>checksum wizard: repairing %1 file &apos;%2&apos; in &apos;%3&apos; from repro template</source>
        <translation>asystent sum kontrolnych: naprawianie pliku %1 &apos;%2&apos; w &apos;%3&apos; z szablonu repro</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2620"/>
        <source>checksum wizard: FATAL: can&apos;t open file &apos;%1&apos; in ZIP archive &apos;%2&apos; for writing</source>
        <translation>asystent sum kontrolnych: FATALNIE: nie można otworzyć pliku &apos;%1&apos; w archiwum ZIP &apos;%2&apos; do zapisu</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2626"/>
        <source>Fixed by QMC2 v%1 (%2)</source>
        <translation>Naprawione przez QMC2 w wersji %1 (%2)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2632"/>
        <source>checksum wizard: FATAL: can&apos;t open ZIP archive &apos;%1&apos; for writing</source>
        <translation>asystent sum kontrolnych: FATALNIE: nie można otworzyć archiwum ZIP &apos;%1&apos; do zapisu</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2647"/>
        <source>repaired</source>
        <translation>naprawiono</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2649"/>
        <source>checksum wizard: successfully repaired %1 file &apos;%2&apos; in &apos;%3&apos; from repro template</source>
        <translation>asystent sum kontrolnych: pomyślnie naprawiono plik %1 &apos;%2&apos; w &apos;%3&apos; z szablonu repro</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2766"/>
        <source>Choose local DB output path</source>
        <translation>Wybierz ścieżkę wyjściową lokalnej bazy danych</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2728"/>
        <source>Connection check -- succeeded!</source>
        <translation>Test połączenia -- udany!</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2738"/>
        <source>Connection check -- failed!</source>
        <translation>Test połączenia -- nieudany!</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2729"/>
        <source>database connection check successful</source>
        <translation>test połączenia z bazą danych powiódł się</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1754"/>
        <source>Choose CHD manager executable file</source>
        <translation>Wybierz plik wykonywalny zarządcy CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1754"/>
        <source>All files (*)</source>
        <translation>Wszystkie pliki (*)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1766"/>
        <source>Choose temporary working directory</source>
        <translation>Wybierz tymczasowy katalog roboczy</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1809"/>
        <source>CHD manager: external process started</source>
        <translation>Menedżer CHD: uruchomiono zewnętrzny proces</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1820"/>
        <location filename="../../romalyzer.cpp" line="2005"/>
        <source>unknown</source>
        <translation>nieznany</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1822"/>
        <source>normal</source>
        <translation>normalny</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1823"/>
        <source>crashed</source>
        <translation>przestał działać</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1825"/>
        <source>CHD manager: external process finished (exit code = %1, exit status = %2)</source>
        <translation>Menedżer CHD: zewnętrzny proces zakończono (kod wyjścia = %1, stan wyjścia = %2)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1841"/>
        <source>CHD manager: stdout: %1</source>
        <translation>Menedżer CHD: stdout: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1863"/>
        <source>CHD manager: stderr: %1</source>
        <translation>Menedżer CHD: stderr: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1890"/>
        <source>CHD manager: failed to start</source>
        <translation>Menedżer CHD: uruchamianie nie powiodło się</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1894"/>
        <source>CHD manager: crashed</source>
        <translation>Menedżer CHD: przestał działać</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1898"/>
        <source>CHD manager: write error</source>
        <translation>Menedżer CHD: błąd zapisu</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1902"/>
        <source>CHD manager: read error</source>
        <translation>Menedżer CHD: błąd odczytu</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1906"/>
        <source>CHD manager: unknown error %1</source>
        <translation>Menedżer CHD: nieznany błąd %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="15"/>
        <source>ROMAlyzer</source>
        <translation>ROMAlyzer</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="42"/>
        <location filename="../../romalyzer.cpp" line="1039"/>
        <source>&amp;Analyze</source>
        <translation>&amp;Analizuj</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="82"/>
        <source>Analysis report</source>
        <translation>Raport z analizy</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="127"/>
        <location filename="../../romalyzer.ui" line="1346"/>
        <source>SHA1</source>
        <translation>SHA1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="132"/>
        <source>MD5</source>
        <translation>MD5</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="599"/>
        <source>Enable ROM database support (repository access may be slow)</source>
        <translation>Włącz obsługę bazy danych ROM-ów (dostęp do repozytorium może być wolny)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="698"/>
        <source>Server / IP</source>
        <translation>Serwer / IP</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="685"/>
        <source>Name or IP address of the database server</source>
        <translation>Nazwa lub adres IP serwera bazy danych</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="614"/>
        <source>Driver</source>
        <translation>Sterownik</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="627"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="643"/>
        <source>default</source>
        <translation>domyślny</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="711"/>
        <source>Username</source>
        <translation>Nazwa użytkownika</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="724"/>
        <source>Username used to access the database</source>
        <translation>Nazwa użytkownika używana do dostępu do bazy danych</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="798"/>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="824"/>
        <source>Automatically download missing / bad files from the database (if they are available in the repository)</source>
        <translation>Automatyczne pobieranie brakujących / uszkodzonych plików z bazy danych (jeśli są dostępne w repozytorium)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="827"/>
        <source>Download</source>
        <translation>Pobierz</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="840"/>
        <source>Automatically upload good files to the database (if they are missing in the repository)</source>
        <translation>Automatyczne wysyłanie dobrych plików do bazy danych (jeśli nie ma ich w repozytorium)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="662"/>
        <source>SQL driver to use</source>
        <translation>Używany sterownik SQL</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="289"/>
        <source>Temporary directory used by the CHD manager (make sure it has enough room to store the biggest CHDs)</source>
        <translation>Katalog tymczasowy używany przez menedżera CHD (należy się upewnić czy wystarczy miejsca na zapisanie największych CHD)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="302"/>
        <source>Browse temporary directory used by the CHD manager</source>
        <translation>Wskaż katalog tymczasowy używany przez menedżera CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="366"/>
        <source>General analysis flags and limits</source>
        <translation>Ogólne ustawienia i limity analizy</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="472"/>
        <source>&lt;b&gt;Limits:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Limity:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="479"/>
        <source>File size</source>
        <translation>Rozmiar pliku</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="492"/>
        <source>Maximum size (in MB) of files to be loaded, files are skipped when they are bigger than that (0 = no limit)</source>
        <translation>Maksymalny rozmiar (w MB) wczytywanych plików, większe są pomijane (0 = bez ograniczeń)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="514"/>
        <source>Log size</source>
        <translation>Rozmiar dziennika</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="549"/>
        <source>Reports</source>
        <translation>Raporty</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="562"/>
        <source>Maximum number of reported sets held in memory (0 = no limit)</source>
        <translation>Maksymalna liczba raportów trzymanych w pamięci (0 = bez ograniczeń)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="666"/>
        <source>MySQL</source>
        <translation>MySQL</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="671"/>
        <source>SQLite</source>
        <translation>SQLite</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="737"/>
        <source>Check the connection to the database</source>
        <translation>Proszę sprawdzić połączenie z bazą danych</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="859"/>
        <source>Overwrite existing data in the database</source>
        <translation>Nadpisz dane istniejące w bazie danych</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="862"/>
        <source>Overwrite</source>
        <translation>Nadpisz</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="875"/>
        <source>Output path</source>
        <translation>Ścieżka wyjściowa</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="888"/>
        <source>Local output directory where downloaded ROMs &amp; CHDs will be created (WARNING: existing files will be overwritten!)</source>
        <translation>Lokalny katalog wyjściowy, w którym będą zapisane pobrane ROM-y i CHD (UWAGA: istniejące pliki zostaną nadpisane!)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="901"/>
        <source>Browse local output directory</source>
        <translation>Wskaż lokalny katalog wyjściowy</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="915"/>
        <source>Enable set rewriter</source>
        <translation>Włącz przepisywanie setów na nowo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="946"/>
        <source>Output path for the set rewriter (WARNING: existing files will be overwritten!) -- you should NEVER use one of your primary ROM paths here!!!</source>
        <translation>Ścieżka wyjściowa dla setów przepisanych na nowo (UWAGA: istniejące pliki będą nadpisane!) -- NIGDY nie należy podawać tutaj głównych ścieżek do ROM-ów!!!</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1108"/>
        <source>Browse output path for the set rewriter</source>
        <translation>Wskaż ścieżkę wyjściową do przepisywania setów na nowo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="972"/>
        <source>Rewrite sets while analyzing them (otherwise sets will only be rewritten on demand / through the context menu)</source>
        <translation>Przepisywanie setów podczas analizy (w przeciwnym razie sety będą przepisywane jedynie na żądanie / za pomocą menu kontekstowego)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="933"/>
        <source>Output directory</source>
        <translation>Katalog wyjściowy</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="959"/>
        <source>General settings</source>
        <translation>Ustawienia ogólne</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="988"/>
        <source>Create sets that do not need parent sets (otherwise create merged sets, which is recommended)</source>
        <translation>Tworzenie setów niepotrzebujących setów macierzystych (w przeciwnym razie tworzenie setów scalonych, co jest zalecane)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="998"/>
        <source>Rewrite sets only when they are &apos;good&apos; (otherwise, &apos;bad&apos; sets will be included)</source>
        <translation>Przepisywanie setów tylko kiedy są &apos;dobre&apos; (w przeciwnym razie będą przepisane tównież &apos;złe&apos;)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1001"/>
        <source>Good sets only</source>
        <translation>Jedynie dobre sety</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1014"/>
        <source>Reproduction type</source>
        <translation>Rodzaj reprodukcji</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1029"/>
        <source>Produce ZIP archived sets (recommended)</source>
        <translation>Tworzenie setów skompresowanych zip (zalecane)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1045"/>
        <source>Level </source>
        <translation>Poziom</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1069"/>
        <source>Unique CRCs</source>
        <translation>Unikalne CRC</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1125"/>
        <source>Additional ROM path</source>
        <translation>Dodatkowa ścieżka do ROM-ów</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1151"/>
        <source>Browse additional ROM path</source>
        <translation>Wskaż dodatkową ścieżkę ROM-ów</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1166"/>
        <source>Checksum wizard</source>
        <translation>Asystent sum kontrolnych</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1194"/>
        <source>Search</source>
        <translation>Szukaj</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1264"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1269"/>
        <source>Filename</source>
        <translation>Nazwa pliku</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1274"/>
        <source>Status</source>
        <translation>Stan</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1284"/>
        <source>Path</source>
        <translation>Ścieżka</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1225"/>
        <source>Repair selected &apos;bad&apos; sets using the file from the first selected &apos;good&apos; set (at least 1 good and 1 bad set must be selected)</source>
        <translation>Naprawianie wybranych &apos;złych&apos; setów przy pomocy plików z pierwszego wybranego &apos;dobrego&apos; seta (muszą być wybrane co najmniej 1 dobry i 1 zły set)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1228"/>
        <source>Repair bad sets</source>
        <translation>Naprawianie złych setów</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1208"/>
        <source>Analyze all selected sets in order to qualify them</source>
        <translation>Sprawdzenie wszystkich wybranych setów w celu kwalifikacji</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1138"/>
        <source>Specify an additional source ROM path used when the set rewriter is active</source>
        <translation>Wskazuje dodatkową ścieżkę ROM-ów, używaną gdy przepisywanie zestawów jest aktywne</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1178"/>
        <source>Checksum to be searched</source>
        <translation>Szukana suma kontrolna</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1191"/>
        <source>Search for the checksum now</source>
        <translation>Rozpoczęcie poszukiwania sumy kontrolnej</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1211"/>
        <source>Analyze selected sets</source>
        <translation>Analiza wybranych setów</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1239"/>
        <source>Search results for the current checksum</source>
        <translation>Przeszukiwanie wyników pod kątem obecjen sumy kontrolnej</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1294"/>
        <source>Level of automation</source>
        <translation>Poziom automatyzacji</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1307"/>
        <source>Choose the level of automated wizard operations</source>
        <translation>Wybierz poziom automatyzacji operacji asystenta</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1311"/>
        <source>Do nothing automatically</source>
        <translation>Brak automatyzacji</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1316"/>
        <source>Automatically select matches</source>
        <translation>Automatyczny wybór dopasowań</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1321"/>
        <source>Automatically select matches and analyze sets</source>
        <translation>Automatyczny wybór dopasowań i analiza setów</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1326"/>
        <source>Automatically select matches, analyze sets and repair bad ones</source>
        <translation>Automatyczny wybór dopasowań, analiza setów i naprawa złych</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1342"/>
        <source>Select the checksum type</source>
        <translation>Wybór typu sumy kontrolnej</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1368"/>
        <source>Close ROMAlyzer</source>
        <translation>Zamknij ROMAlyzer</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1371"/>
        <source>&amp;Close</source>
        <translation>&amp;Zamknij</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="29"/>
        <source>Shortname of game to be analyzed - wildcards allowed, use space as delimiter for multiple games</source>
        <translation>Krótka nazwa gry do zanalizowania - dozwolone dzikie karty, spacja oddziela różne gry</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="372"/>
        <source>If set, analysis output is appended (otherwise the report is cleared before the analysis)</source>
        <translation>Jeśli włączone, wyjście analizy jest dopisywane (w przeciwnym wypadku raport jest czyszczony przed analizą)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="785"/>
        <source>Database</source>
        <translation>Baza danych</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="772"/>
        <source>Name of the database on the server</source>
        <translation>Nazwa bazy danych na serwerze</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="743"/>
        <location filename="../../romalyzer.cpp" line="2755"/>
        <source>Connection check</source>
        <translation>Test połączenia</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1388"/>
        <source>Current ROMAlyzer status</source>
        <translation>Bieżący stan ROMAlyzera</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1391"/>
        <location filename="../../romalyzer.cpp" line="598"/>
        <location filename="../../romalyzer.cpp" line="1046"/>
        <location filename="../../romalyzer.cpp" line="2014"/>
        <location filename="../../romalyzer.cpp" line="2659"/>
        <source>Idle</source>
        <translation>Bezczynny</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1407"/>
        <source>Analysis progress indicator</source>
        <translation>Wskaźnik postępu analizy</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="102"/>
        <location filename="../../romalyzer.ui" line="1279"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="73"/>
        <source>Report</source>
        <translation>Raport</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="141"/>
        <source>Log</source>
        <translation>Dziennik</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="147"/>
        <source>Analysis log</source>
        <translation>Dziennik analizy</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="210"/>
        <source>please wait for reload to finish and try again</source>
        <translation>proszę poczekać za zakończenie przeładowywania i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="548"/>
        <source>&amp;Stop</source>
        <translation>&amp;Zatrzymaj</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="213"/>
        <source>stopping analysis</source>
        <translation>zatrzymywanie analizy</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="217"/>
        <source>starting analysis</source>
        <translation>rozpoczynanie analizy</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="556"/>
        <source>analysis started</source>
        <translation>analiza rozpoczęta</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="558"/>
        <source>determining list of games to analyze</source>
        <translation>ustalanie listy gier do zanalizowania</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="578"/>
        <source>Searching games</source>
        <translation>Szukanie gier</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1049"/>
        <source>analysis ended</source>
        <translation>analiza zakończona</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="606"/>
        <source>done (determining list of games to analyze)</source>
        <translation>ukończono (ustalanie listy gier do zanalizowania)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="607"/>
        <source>%n game(s) to analyze</source>
        <translation>
            <numerusform>%n gra do zanalizowania</numerusform>
            <numerusform>%n gry do zanalizowania</numerusform>
            <numerusform>%n gier do zanalizowania</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="657"/>
        <source>analyzing &apos;%1&apos;</source>
        <translation>analizowanie &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="659"/>
        <source>Analyzing &apos;%1&apos;</source>
        <translation>Analizowanie &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1029"/>
        <source>done (analyzing &apos;%1&apos;)</source>
        <translation>ukończono (analizowanie &apos;%1&apos;)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="1031"/>
        <source>%n game(s) left</source>
        <translation>
            <numerusform>pozostała %n gra</numerusform>
            <numerusform>pozostały %n gry</numerusform>
            <numerusform>pozostało %n gier</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="670"/>
        <source>parsing XML data for &apos;%1&apos;</source>
        <translation>parsowanie danych XML dla &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="677"/>
        <source>done (parsing XML data for &apos;%1&apos;)</source>
        <translation>ukończono (parsowanie danych XML dla &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="679"/>
        <source>error (parsing XML data for &apos;%1&apos;)</source>
        <translation>błąd (parsowanie danych XML dla &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="92"/>
        <source>Game / File</source>
        <translation>Gra / Plik</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="97"/>
        <source>Merge</source>
        <translation>Scalanie</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="107"/>
        <source>Emu status</source>
        <translation>Stan emulatora</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="211"/>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="375"/>
        <source>Append to report</source>
        <translation>Dopisz do raportu</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="112"/>
        <source>File status</source>
        <translation>Stan pliku</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="122"/>
        <location filename="../../romalyzer.ui" line="1351"/>
        <source>CRC</source>
        <translation>CRC</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="117"/>
        <source>Size</source>
        <translation>Rozmiar</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="688"/>
        <source>checking %n file(s) for &apos;%1&apos;</source>
        <translation>
            <numerusform>sprawdzanie %n pliku dla &apos;%1&apos;</numerusform>
            <numerusform>sprawdzanie %n plików dla &apos;%1&apos;</numerusform>
            <numerusform>sprawdzanie %n plików dla &apos;%1&apos;</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="740"/>
        <location filename="../../romalyzer.cpp" line="834"/>
        <location filename="../../romalyzer.cpp" line="911"/>
        <location filename="../../romalyzer.cpp" line="1997"/>
        <location filename="../../romalyzer.cpp" line="2475"/>
        <location filename="../../romalyzer.cpp" line="2538"/>
        <source>ROM</source>
        <translation>ROM</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="749"/>
        <location filename="../../romalyzer.cpp" line="801"/>
        <location filename="../../romalyzer.cpp" line="915"/>
        <location filename="../../romalyzer.cpp" line="949"/>
        <location filename="../../romalyzer.cpp" line="956"/>
        <source>not found</source>
        <translation>nieznaleziony</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="828"/>
        <source>Checksums</source>
        <translation>Sumy kontrolne</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="845"/>
        <source>SIZE </source>
        <translation>ROZMIAR </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="860"/>
        <source>CRC </source>
        <translation>CRC </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="872"/>
        <source>SHA1 </source>
        <translation>SHA1 </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="897"/>
        <source>MD5 </source>
        <translation>MD5 </translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="941"/>
        <source>interrupted (checking %n file(s) for &apos;%1&apos;)</source>
        <translation>
            <numerusform>przerwane (sprawdzanie %n pliku dla &apos;%1&apos;)</numerusform>
            <numerusform>przerwane (sprawdzanie %n plików dla &apos;%1&apos;)</numerusform>
            <numerusform>przerwane (sprawdzanie %n plików dla &apos;%1&apos;)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="964"/>
        <source>good / not found</source>
        <translation>dobry / nieznaleziony</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="883"/>
        <location filename="../../romalyzer.cpp" line="953"/>
        <location filename="../../romalyzer.cpp" line="976"/>
        <location filename="../../romalyzer.cpp" line="2260"/>
        <location filename="../../romalyzer.cpp" line="2452"/>
        <source>good</source>
        <translation>dobry</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="126"/>
        <source>Search checksum</source>
        <translation>Szukaj sumy kontrolnej</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="135"/>
        <source>Rewrite set</source>
        <translation>Przepisz set</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="793"/>
        <location filename="../../romalyzer.cpp" line="814"/>
        <location filename="../../romalyzer.cpp" line="883"/>
        <location filename="../../romalyzer.cpp" line="996"/>
        <location filename="../../romalyzer.cpp" line="2261"/>
        <location filename="../../romalyzer.cpp" line="2450"/>
        <source>bad</source>
        <translation>zły</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="834"/>
        <source>WARNING: %1 file &apos;%2&apos; loaded from &apos;%3&apos; may be obsolete, should be merged from parent set &apos;%4&apos;</source>
        <translation>UWAGA: plik %1 &apos;%2&apos; wczytany z &apos;%3&apos; może być przestarzały, powinien być scalony z setu macierzystego &apos;%4&apos;</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="1016"/>
        <source>done (checking %n file(s) for &apos;%1&apos;)</source>
        <translation>
            <numerusform>ukończono (sprawdzanie %n pliku dla &apos;%1&apos;)</numerusform>
            <numerusform>ukończono (sprawdzanie %n plików dla &apos;%1&apos;)</numerusform>
            <numerusform>ukończono (sprawdzanie %n plików dla &apos;%1&apos;)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1182"/>
        <location filename="../../romalyzer.cpp" line="1206"/>
        <source>  logical size: %1 (%2 B)</source>
        <translation>  rozmiar logiczny: %1 (%2 B)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1243"/>
        <source>Verify - %p%</source>
        <translation>Weryfikacja - %p%</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1245"/>
        <source>CHD manager: verifying and fixing CHD</source>
        <translation>Menedżer CHD: weryfikacja i naprawa CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1248"/>
        <source>CHD manager: verifying CHD</source>
        <translation>Menedżer CHD: weryfikacja CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1258"/>
        <source>Update - %p%</source>
        <translation>Aktualizacja - %p%</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1259"/>
        <source>CHD manager: updating CHD (v%1 -&gt; v%2)</source>
        <translation>Menedżer CHD: aktualizacja CHD ( wersja %1 -&gt; wersja %2)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1264"/>
        <location filename="../../romalyzer.cpp" line="1276"/>
        <source>CHD manager: using header checksums for CHD verification</source>
        <translation>Menedżer CHD: używanie sum kontrolnych nagłówka do weryfikacji CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1284"/>
        <source>CHD manager: no header checksums available for CHD verification</source>
        <translation>Menedżer CHD: brak sum kontrolnych dostępnych do weryfikacji CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1312"/>
        <source>CHD manager: terminating external process</source>
        <translation>Menedżer CHD: kończenie zewnętrznego procesu</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1333"/>
        <location filename="../../romalyzer.cpp" line="1335"/>
        <source>CHD manager: CHD file integrity is good</source>
        <translation>Menedżer CHD: integralność danych CHD jest dobra</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1337"/>
        <source>CHD manager: WARNING: CHD file integrity is bad</source>
        <translation>Menedżer CHD:UWAGA: integralność dannych CHD jest zła</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1341"/>
        <location filename="../../romalyzer.cpp" line="1353"/>
        <source>CHD manager: using CHD v%1 header checksums for CHD verification</source>
        <translation>Menedżer CHD: używanie nagłówka CHD w wersji %1 do walidacji CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1361"/>
        <source>CHD manager: WARNING: no header checksums available for CHD verification</source>
        <translation>Menedżer CHD: UWAGA: brak dostępnych sum kontrolnych nagłówka do walidacji CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1368"/>
        <source>CHD manager: replacing CHD</source>
        <translation>Menedżer CHD: zastępowanie CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1370"/>
        <source>Copy</source>
        <translation>Kopiowanie</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1376"/>
        <source>CHD manager: CHD replaced</source>
        <translation>Menedżer CHD: CHD zastąpiono</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1379"/>
        <source>CHD manager: FATAL: failed to replace CHD -- updated CHD preserved as &apos;%1&apos;, please copy it to &apos;%2&apos; manually!</source>
        <translation>Menedżer CHD: FATALNIE: nie udało się zastąpić CHD -- zaktualizowany CHD zachowany jako &apos;%1&apos;, proszę skopiować go do &apos;%2&apos; ręcznie!</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1383"/>
        <source>CHD manager: cleaning up</source>
        <translation>Menedżer CHD: czyszczenie</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1395"/>
        <location filename="../../romalyzer.cpp" line="1407"/>
        <source>using CHD v%1 header checksums for CHD verification</source>
        <translation>używanie sum kontrolnych nagłówka w wersji %1 do weryfikacji CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1415"/>
        <source>WARNING: no header checksums available for CHD verification</source>
        <translation>UWAGA: brak sum kontrolnych dostępnych do weryfikacji CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1461"/>
        <location filename="../../romalyzer.cpp" line="1592"/>
        <source>WARNING: found &apos;%1&apos; but can&apos;t read from it - check permission</source>
        <translation>UWAGA: znaleziono &apos;%1&apos;, lecz nie można z niego czytać - sprawdź uprawnienia</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1499"/>
        <source>WARNING: unable to identify &apos;%1&apos; from &apos;%2&apos; by CRC &apos;%3&apos;</source>
        <translation>UWAGA: nie można zidentyfikować &apos;%1&apos; z &apos;%2&apos; na podstawie CRC &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1496"/>
        <source>WARNING: unable to identify &apos;%1&apos; from &apos;%2&apos; by CRC (no dump exists / CRC unknown)</source>
        <translation>UWAGA: nie można zidentyfikować &apos;%1&apos; z &apos;%2&apos; na podstawie CRC (brak istniejącego zrzutu / nieznana CRC)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="142"/>
        <source>Copy to clipboard</source>
        <translation>Kopiuj do schowka</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="822"/>
        <location filename="../../romalyzer.cpp" line="917"/>
        <source>no dump</source>
        <translation>brak zrzutu</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="968"/>
        <source>good / no dump / skipped</source>
        <translation>dobry / brak zrzutu / pominięty</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="970"/>
        <source>good / no dump</source>
        <translation>dobry / brak zrzutu</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="988"/>
        <source>bad / no dump / skipped</source>
        <translation>zły / brak zrzutu / pominięty</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="990"/>
        <source>bad / no dump</source>
        <translation>zły / brak zrzutu</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1519"/>
        <source>loading &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos; as &apos;%4&apos;%5</source>
        <translation>wczytywanie &apos;%1&apos; przy pomocy CRC &apos;%2&apos; z &apos;%3&apos; jako &apos;%4&apos;%5</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1586"/>
        <source>WARNING: unable to decompress &apos;%1&apos; from &apos;%2&apos; - check file integrity</source>
        <translation>UWAGA: nie można wypakować &apos;%1&apos; z &apos;%2&apos; - sprawdź integralność pliku</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1590"/>
        <source>WARNING: found &apos;%1&apos; but can&apos;t open it for decompression - check file integrity</source>
        <translation>UWAGA: znaleziono &apos;%1&apos;, lecz nie można otworzyć go do dekompresji - sprawdź integralność pliku</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1780"/>
        <source>Choose output directory</source>
        <translation>Wybierz katalog wyjściowy</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1794"/>
        <source>Choose additional ROM path</source>
        <translation>Wybierz dodatkową ścieżkę ROM-ów</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1940"/>
        <source>Checksum search</source>
        <translation>Szukanie sum kontrolnych</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2079"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path is not a directory</source>
        <translation>przepisywanie setów na nowo: UWAGA: nie można przepisać seta &apos;%1&apos;, ścieżka wyjściowa nie jest katalogiem</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2083"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path is not writable</source>
        <translation>przepisywanie setów na nowo: UWAGA: nie można przepisać seta &apos;%1&apos;, nie można zapisywać do ścieżki wyjściowej</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2088"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path does not exist</source>
        <translation>przepisywanie setów na nowo: UWAGA: nie można przepisać seta &apos;%1&apos;, ścieżka wyjściowa nie istnieje</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2092"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path is empty</source>
        <translation>przepisywanie setów na nowo: UWAGA: nie można przepisać seta &apos;%1&apos;, ścieżka wyjściowa jest pusta</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2106"/>
        <source>space-efficient</source>
        <translation>oszczędzającego miejsce</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2107"/>
        <source>self-contained</source>
        <translation>samowystarczalnego</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2109"/>
        <source>set rewriter: rewriting %1 set &apos;%2&apos; to &apos;%3&apos;</source>
        <translation>przepisywanie setów na nowo: przepisywanie %1 seta &apos;%2&apos; do &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2125"/>
        <source>set rewriter: skipping &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos; as &apos;%4&apos;</source>
        <translation>przepisywanie setów na nowo: pomijanie &apos;%1&apos; przy pomocy CRC &apos;%2&apos; z &apos;%3&apos; jako &apos;%4&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2136"/>
        <source>set rewriter: FATAL: can&apos;t load &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos;, aborting</source>
        <translation>przepisywanie setów na nowo: FATALNIE: nie można wczytać &apos;%1&apos; przy pomocy CRC &apos;%2&apos; z &apos;%3&apos;, przerywanie</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2139"/>
        <source>set rewriter: WARNING: can&apos;t load &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos;, ignoring this file</source>
        <translation>przepisywanie setów na nowo: nie można wczytać &apos;%1&apos; przy pomocy CRC &apos;%2&apos; z &apos;%3&apos;, pomijanie tego pliku</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2166"/>
        <source>set rewriter: writing new %1 set &apos;%2&apos; in &apos;%3&apos;</source>
        <translation>przepisywanie setów na nowo: zapisywanie nowego %1 seta &apos;%2&apos; do &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2167"/>
        <source>Writing &apos;%1&apos; - %2</source>
        <translation>Zapisywanie &apos;%1&apos; - %2</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2169"/>
        <source>set rewriter: new %1 set &apos;%2&apos; in &apos;%3&apos; successfully created</source>
        <translation>przepisywanie setów na nowo: tworzenie nowego %1 seta &apos;%2&apos; w &apos;%3&apos; powiodło się</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2171"/>
        <source>set rewriter: FATAL: failed to create new %1 set &apos;%2&apos; in &apos;%3&apos;</source>
        <translation>przepisywanie setów na nowo: FATALNIE: tworzenie nowego %1 seta &apos;%2&apos; w &apos;%3&apos; nie powiodło się</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2181"/>
        <source>set rewriter: done (rewriting %1 set &apos;%2&apos; to &apos;%3&apos;)</source>
        <translation>przepisywanie setów na nowo: ukończono (przepisywanie %1 seta &apos;%2&apos; do %3&apos;)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="2464"/>
        <source>checksum wizard: repairing %n bad set(s)</source>
        <translation>
            <numerusform>asystent sum kontrolnych: naprawianie %n złego seta</numerusform>
            <numerusform>asystent sum kontrolnych: naprawianie %n złych setów</numerusform>
            <numerusform>asystent sum kontrolnych: naprawianie %n złych setów</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2470"/>
        <source>checksum wizard: using %1 file &apos;%2&apos; from &apos;%3&apos; as repro template</source>
        <translation>asystent sum kontrolnych: używanie pliku %1 &apos;%2&apos; z &apos;%3&apos; jako szablonu repro</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2493"/>
        <source>checksum wizard: successfully identified &apos;%1&apos; from &apos;%2&apos; by CRC, filename in ZIP archive is &apos;%3&apos;</source>
        <translation>asystent sum kontrolnych: pomyślnie zidentyfikowano &apos;%1&apos; z &apos;%2&apos; przy pomocy CRC, nazwa pliku w archiwum ZIP to &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2502"/>
        <source>checksum wizard: template data loaded, uncompressed size = %1</source>
        <translation>asystent sum kontrolnych: wczytano dane szablonu, nieskompresowany rozmiar = %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2506"/>
        <source>checksum wizard: FATAL: unable to identify &apos;%1&apos; from &apos;%2&apos; by CRC</source>
        <translation>asystent sum kontrolnych: FATALNIE: nie udało się zidentyfikować &apos;%1&apos; z &apos;%2&apos; przy pomocy CRC</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2516"/>
        <location filename="../../romalyzer.cpp" line="2637"/>
        <source>checksum wizard: sorry, no support for regular files yet</source>
        <translation>asystent sum kontrolnych: niestety nie ma jeszcze obsługi regularnych plików</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2521"/>
        <location filename="../../romalyzer.cpp" line="2642"/>
        <source>checksum wizard: sorry, no support for CHD files yet</source>
        <translation>asystent sum kontrolnych: niestety nie ma jeszcze obsługi plików CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2547"/>
        <source>checksum wizard: target ZIP exists, loading complete data and structure</source>
        <translation>asystent sum kontrolnych: docelowy ZIP istnieje, wczytywanie pełnych danych i struktury</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2549"/>
        <source>checksum wizard: target ZIP successfully loaded</source>
        <translation>asystent sum kontrolnych: docelowy ZIP wczytany pomyślnie</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2551"/>
        <source>checksum wizard: an entry with the CRC &apos;%1&apos; already exists, recreating the ZIP from scratch to replace the bad file</source>
        <translation>asystent sum kontrolnych: wpis o CRC &apos;%1&apos; już istnieje, odtwarzanie ZIP-a na nowo w celu zastąpienia błędnego pliku</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2554"/>
        <source>checksum wizard: backup file &apos;%1&apos; successfully created</source>
        <translation>asystent sum kontrolnych: plik kopii bezpieczeństwa &apos;%1&apos; utworzony pomyślnie</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2558"/>
        <source>checksum wizard: FATAL: failed to create backup file &apos;%1&apos;, aborting</source>
        <translation>asystent sum kontrolnych: FATALNIE: nie udało się utworzyć pliku kopii bezpieczeństwa &apos;%1&apos;, przerywanie</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2562"/>
        <source>checksum wizard: no entry with the CRC &apos;%1&apos; was found, adding the missing file to the existing ZIP</source>
        <translation>asystent sum kontrolnych: nie znaleziono wpisu o CRC &apos;%1&apos;, dodawanie brakującego pliku do istniejącego ZIP-a</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2565"/>
        <source>checksum wizard: FATAL: failed to load target ZIP, aborting</source>
        <translation>asystent sum kontrolnych: FATALNIE: nie udało się wczytać docelowego ZIP-a, przerywanie</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2570"/>
        <source>checksum wizard: the target ZIP does not exist, creating a new ZIP with just the missing file</source>
        <translation>asystent sum kontrolnych: docelowy ZIP nie istnieje, tworzenie ZIP-a zawierającego jedynie brakujący plik</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2431"/>
        <location filename="../../romalyzer.cpp" line="2628"/>
        <source>Created by QMC2 v%1 (%2)</source>
        <translation>Utworzone przez QMC2 w wersji %1 (%2)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2102"/>
        <source>Reading &apos;%1&apos; - %2</source>
        <translation>Czytanie &apos;%1&apos; - %2</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2129"/>
        <source>set rewriter: loading &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos; as &apos;%4&apos;</source>
        <translation>przepisywanie setów na nowo: wczytywanie &apos;%1&apos; przy pomocy CRC &apos;%2&apos; z &apos;%3&apos; jako &apos;%4&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2156"/>
        <source>set rewriter: removing redundant file &apos;%1&apos; with CRC &apos;%2&apos; from output data</source>
        <translation>przepisywanie setów na nowo: usuwanie zbytecznego pliku &apos;%1&apos; o CRC &apos;%2&apos; z danych wyjściowych</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2175"/>
        <source>set rewriter: INFORMATION: no output data available, thus not rewriting set &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>przepisywanie setów na nowo: INFORMACJA: z powodu braku dostępnych danych wyjściowych nie nastąpi przepisanie seta &apos;%1&apos; do &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2405"/>
        <source>set rewriter: deflating &apos;%1&apos; (uncompressed size: %2)</source>
        <translation>przepisywanie setów na nowo: kompresja &apos;%1&apos; (nieskompresowany rozmiar %2)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2425"/>
        <source>set rewriter: WARNING: failed to deflate &apos;%1&apos;</source>
        <translation>przepisywanie setów na nowo: UWAGA: nie udało się skompresować &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2651"/>
        <source>repair failed</source>
        <translation>naprawa nie powiodła się</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2653"/>
        <source>checksum wizard: FATAL: failed to repair %1 file &apos;%2&apos; in &apos;%3&apos; from repro template</source>
        <translation>asystent sum kontrolnych: FATALNIE :nie udało się naprawić %1 pliku &apos;%2&apos; w &apos;%3&apos; z szablonu repro</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2663"/>
        <source>checksum wizard: FATAL: can&apos;t find any good set</source>
        <translation>asystent sum kontrolnych: FATALNIE: nie znaleziono żadnego dobrego seta</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="2665"/>
        <source>checksum wizard: done (repairing %n bad set(s))</source>
        <translation>
            <numerusform>asystent sum kontrolnych: ukończono (naprawianie %n złego seta)</numerusform>
            <numerusform>asystent sum kontrolnych: ukończono (naprawianie %n złych setów)</numerusform>
            <numerusform>asystent sum kontrolnych: ukończono (naprawianie %n złych setów)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2739"/>
        <source>database connection check failed -- errorNumber = %1, errorText = &apos;%2&apos;</source>
        <translation>test połączenia z bazą danych nieudany -- numer błędu = %1, treść błędu = &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="395"/>
        <source>Calculate CRC-32 checksum</source>
        <translation>Oblicz sumę kontrolną CRC-32</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="398"/>
        <source>Calculate CRC</source>
        <translation>Oblicz CRC</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="382"/>
        <source>Automatically scroll to the currently analyzed game</source>
        <translation>Przewiń automatycznie do aktualnie analizowanej gry</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="385"/>
        <source>Auto scroll</source>
        <translation>Przewiń automatycznie</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="444"/>
        <source>Calculate SHA1 hash</source>
        <translation>Oblicz skrót SHA1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="447"/>
        <source>Calculate SHA1</source>
        <translation>Oblicz SHA1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="421"/>
        <source>Automatically expand file info</source>
        <translation>Automatycznie rozwiń informacje o pliku</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="424"/>
        <source>Expand file info</source>
        <translation>Rozwiń informacje o pliku</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="411"/>
        <source>Calculate MD5</source>
        <translation>Oblicz MD5</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="434"/>
        <source>Automatically expand checksums</source>
        <translation>Automatycznie rozwiń sumy kontrolne</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="437"/>
        <source>Expand checksums</source>
        <translation>Rozwiń sumy kontrolne</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="408"/>
        <source>Calculate MD5 hash</source>
        <translation>Oblicz skrót MD5</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="984"/>
        <source>bad / not found</source>
        <translation>zły / nieznaleziony</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1458"/>
        <source>WARNING: found &apos;%1&apos; but can&apos;t read from it although permissions seem okay - check file integrity</source>
        <translation>UWAGA: znaleziono &apos;%1&apos;, lecz nie można z niego czytać, mimo że uprawnienia wydają się poprawne - sprawdź integralność pliku</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="231"/>
        <source>pausing analysis</source>
        <translation>wstrzymywanie analizy</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="234"/>
        <source>resuming analysis</source>
        <translation>wznawianie analizy</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="56"/>
        <location filename="../../romalyzer.cpp" line="235"/>
        <location filename="../../romalyzer.cpp" line="551"/>
        <source>&amp;Pause</source>
        <translation>&amp;Wstrzymaj</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="619"/>
        <source>analysis paused</source>
        <translation>analiza wstrzymana</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="620"/>
        <source>&amp;Resume</source>
        <translation>&amp;Wznów</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="623"/>
        <source>Paused</source>
        <translation>Wstrzymano</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="53"/>
        <source>Pause / resume active analysis</source>
        <translation>Wstrzymaj / wznów aktywną analizę</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1141"/>
        <source>loading &apos;%1&apos;%2</source>
        <translation>wczytywanie &apos;%1&apos;%2</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1141"/>
        <location filename="../../romalyzer.cpp" line="1519"/>
        <source> (merged)</source>
        <translation> (scalony)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1426"/>
        <source>File I/O progress indicator</source>
        <translation>Wskaźnik postępu we/wy pliku</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="720"/>
        <location filename="../../romalyzer.cpp" line="772"/>
        <location filename="../../romalyzer.cpp" line="775"/>
        <location filename="../../romalyzer.cpp" line="913"/>
        <source>skipped</source>
        <translation>pominięty</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="962"/>
        <source>good / not found / skipped</source>
        <translation>dobry / nieznaleziony / pominięty</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="974"/>
        <source>good / skipped</source>
        <translation>dobry / pominięty</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="982"/>
        <source>bad / not found / skipped</source>
        <translation>zły / nieznaleziony / pominięty</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="994"/>
        <source>bad / skipped</source>
        <translation>zły / pominięty</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="498"/>
        <location filename="../../romalyzer.cpp" line="1711"/>
        <location filename="../../romalyzer.cpp" line="1725"/>
        <source> MB</source>
        <translation> MB</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>none</source>
        <translation>brak</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="84"/>
        <source>Automatically scroll to the currently analyzed machine</source>
        <translation>Przewiń automatycznie do obecnie analizowanej maszyny</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="85"/>
        <source>Shortname of machine to be analyzed - wildcards allowed, use space as delimiter for multiple machines</source>
        <translation>Krótka nazwa maszyny do zanalizowania - dozwolone dzikie karty, spacja oddziela różne maszyny</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>zlib</source>
        <translation>zlib</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>zlib+</source>
        <translation>zlib+</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>A/V codec</source>
        <translation>kodek A/V</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="640"/>
        <source>report limit reached, removing %n set(s) from the report</source>
        <translation>
            <numerusform>Limit raportów osiągnięty, usuwanie %n seta z raportu</numerusform>
            <numerusform>Limit raportów osiągnięty, usuwanie %n setów z raportu</numerusform>
            <numerusform>Limit raportów osiągnięty, usuwanie %n setów z raportu</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="778"/>
        <source>error</source>
        <translation>błąd</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="911"/>
        <source>WARNING: %1 file &apos;%2&apos; loaded from &apos;%3&apos; has incorrect / unexpected checksums</source>
        <translation>UWAGA: plik %1 &apos;%2&apos; wczytany z &apos;%3&apos; posiada nieprawidłowe / nieoczekiwane sumy kontrolne</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1164"/>
        <source>CHD header information:</source>
        <translation>Informacje nagłówka CHD:</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1168"/>
        <source>  version: %1</source>
        <translation>  wersja: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1169"/>
        <location filename="../../romalyzer.cpp" line="1377"/>
        <source>CHD v%1</source>
        <translation>CHD wersja %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1174"/>
        <location filename="../../romalyzer.cpp" line="1198"/>
        <source>  compression: %1</source>
        <translation>  kompresja: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1178"/>
        <location filename="../../romalyzer.cpp" line="1202"/>
        <source>  number of total hunks: %1</source>
        <translation>  całkowita liczba kawałków: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1180"/>
        <location filename="../../romalyzer.cpp" line="1204"/>
        <source>  number of bytes per hunk: %1</source>
        <translation>  liczba bajtów na kawałek: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1184"/>
        <source>  MD5 checksum: %1</source>
        <translation>  suma kontrolna MD5: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1186"/>
        <location filename="../../romalyzer.cpp" line="1208"/>
        <source>  SHA1 checksum: %1</source>
        <translation>  suma kontrolna SHA1: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1219"/>
        <source>only CHD v3 and v4 headers supported -- rest of header information skipped</source>
        <translation>obsługiwane są jedynie CHD w wersjach 3 i 4 -- pominięto resztę informacji z nagłówka</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1423"/>
        <source>WARNING: can&apos;t read CHD header information</source>
        <translation>UWAGA: nie można odczytać informacji z nagłówka CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1570"/>
        <source>WARNING: the CRC for &apos;%1&apos; from &apos;%2&apos; is unknown to the emulator, the set rewriter will use the recalculated CRC &apos;%3&apos; to qualify the file</source>
        <translation>UWAGA: CRC dla &apos;%1&apos; z &apos;%2&apos; nie jest znana emulatorowi, przepisywanie setów na nowo skorzysta z przeliczonej CRC &apos;%3&apos; do zakwalifikowania pliku</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1572"/>
        <source>WARNING: unable to determine the CRC for &apos;%1&apos; from &apos;%2&apos;, the set rewriter will NOT store this file in the new set</source>
        <translation>UWAGA: nie udało się określić CRC dla &apos;%1&apos; z &apos;%2&apos;, przepisywanie setów na nowo NIE zapisze tego pliku w nowym secie</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1708"/>
        <location filename="../../romalyzer.cpp" line="1722"/>
        <source> KB</source>
        <translation> KB</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1714"/>
        <location filename="../../romalyzer.cpp" line="1728"/>
        <source> GB</source>
        <translation> GB</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1717"/>
        <source> TB</source>
        <translation> TB</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="179"/>
        <source>&amp;Forward</source>
        <translation>&amp;Naprzód</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="196"/>
        <source>&amp;Backward</source>
        <translation>Ws&amp;tecz</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="460"/>
        <source>Select game</source>
        <translation>Wybierz grę</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="169"/>
        <source>Search string</source>
        <translation>Szukaj ciągu</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="176"/>
        <source>Search string forward</source>
        <translation>Szukaj ciągu naprzód</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="193"/>
        <source>Search string backward</source>
        <translation>Szukaj ciągu wstecz</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="230"/>
        <source>Enable CHD manager (may be slow)</source>
        <translation>Włącz zarządcę CHD (może być wolne)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="239"/>
        <source>CHD manager (chdman)</source>
        <translation>Menedżer CHD (chdman)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="252"/>
        <source>CHD manager executable file (read and execute)</source>
        <translation>Plik wykonywalny zarządcy CHD (odczyt i uruchamianie)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="265"/>
        <source>Browse CHD manager executable file</source>
        <translation>Wskaż plik wykonywalny zarządcy CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="276"/>
        <source>Temporary working directory</source>
        <translation>Tymczasowy katalog roboczy</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="315"/>
        <source>Verify CHDs through &apos;chdman -verify&apos;</source>
        <translation>Weryfikuj CHD za pomocą &apos;chdman -verify&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="318"/>
        <source>Verify CHDs</source>
        <translation>Weryfikuj CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="328"/>
        <source>Also try to fix CHDs using &apos;chdman -verifyfix&apos;</source>
        <translation>Również próbuj naprawiać CHD za pomocą &apos;chdman -verifyfix&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="331"/>
        <source>Fix CHDs</source>
        <translation>Naprawiaj CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="338"/>
        <source>Try to update CHDs if their header indicates an older version (&apos;chdman -update&apos;)</source>
        <translation>Próbuj aktualizować CHD, jeżeli ich nagłówek wskazuje na starszą wersję (&apos;chdman -update&apos;)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="341"/>
        <source>Update CHDs</source>
        <translation>Aktualizuj CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="527"/>
        <source>Maximum number of lines in log (0 = no limit)</source>
        <translation>Maksymalna liczba linii w dzienniku (0 = bez ograniczeń)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="533"/>
        <source> lines</source>
        <translation> linii</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="640"/>
        <source>Database server port (0 = default)</source>
        <translation>Port serwera bazy danych (0 = domyślny)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="756"/>
        <source>Password used to access the database (WARNING: stored passwords are &lt;u&gt;weakly&lt;/u&gt; encrypted!)</source>
        <translation>Hasło używane do dostępu do bazy danych (UWAGA: zapisane hasła są &lt;u&gt;słabo&lt;/u&gt; zaszyfrowane!)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="811"/>
        <source>Mode</source>
        <translation>Tryb</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="843"/>
        <source>Upload</source>
        <translation>Wyślij</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="975"/>
        <source>Rewrite while analyzing</source>
        <translation>Przepisywanie podczas analizy</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="991"/>
        <source>Self-contained</source>
        <translation>Samowystarczalne</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1032"/>
        <source>ZIPs</source>
        <translation>ZIP-y</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1042"/>
        <source>Select the ZIP compression level (0 = lowest / fastest, 9 = highest / slowest)</source>
        <translation>Wybór poziom kompresji ZIP (0 =najniższy / najszybszy, 9 = najwyższy / najwolniejszy)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1066"/>
        <source>When a set contains multiple files with the same CRC, should the produced ZIP include all files individually or just the first one (which is actually sufficient)?</source>
        <translation>Kiedy set zawiera wiele plików o tej samej CRC, czy tworzony ZIP ma zawierać wszystkie pliki oddzielnie czy tylko jeden (co w istocie jest wystarczające)?</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1079"/>
        <source>Produce sets in individual sub-directories (not recommended -- and not supported yet!)</source>
        <translation>Tworzenie setów w oddzielnych podkatalogach (niezalecane - jak również jeszcze nieobsługiwane!)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1082"/>
        <source>Directories</source>
        <translation>Katalogi</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="495"/>
        <location filename="../../romalyzer.ui" line="530"/>
        <location filename="../../romalyzer.ui" line="565"/>
        <source>unlimited</source>
        <translation>nieograniczony</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1166"/>
        <source>  tag: %1</source>
        <translation>  zaznaczenie: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1189"/>
        <source>  parent CHD&apos;s MD5 checksum: %1</source>
        <translation>  suma kontrolna MD5 CHD macierzystego: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1191"/>
        <location filename="../../romalyzer.cpp" line="1211"/>
        <source>  parent CHD&apos;s SHA1 checksum: %1</source>
        <translation>  suma kontrolna SHA1 CHD macierzystego: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>  flags: %1, %2</source>
        <translation>  flagi: %1, %2</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>has parent</source>
        <translation>ma macierzysty</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>no parent</source>
        <translation>brak macierzystego</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>allows writes</source>
        <translation>pozwala na zapis</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>read only</source>
        <translation>tylko do odczytu</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1464"/>
        <source>WARNING: CHD file &apos;%1&apos; not found</source>
        <translation>UWAGA: plik CHD &apos;%1&apos; nieznaleziony</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="834"/>
        <location filename="../../romalyzer.cpp" line="911"/>
        <location filename="../../romalyzer.cpp" line="1106"/>
        <location filename="../../romalyzer.cpp" line="1994"/>
        <source>CHD</source>
        <translation>CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="82"/>
        <source>Select machine</source>
        <translation>Wybierz maszynę</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="83"/>
        <source>Select machine in machine list if selected in analysis report?</source>
        <translation>Wybrać maszynę na liście maszyn jeśli wybrana w raporcie z analizy?</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="457"/>
        <source>Select game in game list if selected in analysis report?</source>
        <translation>Wybrać grę na liście gier jeśli wybrana w raporcie z analizy?</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="81"/>
        <source>Machine / File</source>
        <translation>Maszyna / Plik</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="560"/>
        <source>determining list of machines to analyze</source>
        <translation>ustalanie listy maszyn do zanalizowania</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="580"/>
        <source>Searching machines</source>
        <translation>Szukanie maszyn</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="609"/>
        <source>done (determining list of machines to analyze)</source>
        <translation>ukończono (ustalanie listy maszyn do zanalizowania)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="610"/>
        <source>%n machine(s) to analyze</source>
        <translation>
            <numerusform>%n maszyna do zanalizowania</numerusform>
            <numerusform>%n maszyny do zanalizowania</numerusform>
            <numerusform>%n maszyn do zanalizowania</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="1033"/>
        <source>%n machine(s) left</source>
        <translation>
            <numerusform>pozostała %n maszyna</numerusform>
            <numerusform>pozostały %n maszyny</numerusform>
            <numerusform>pozostało %n maszyn</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1049"/>
        <source>elapsed time = %1</source>
        <translation>miniony czas = %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="39"/>
        <source>Start / stop analysis</source>
        <translation>Rozpocznij / zatrzymaj analizę</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1214"/>
        <source>  raw SHA1 checksum: %1</source>
        <translation>  surowa suma kontrolna SHA1: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1132"/>
        <source>size of &apos;%1&apos; is greater than allowed maximum -- skipped</source>
        <translation>rozmiar &apos;%1&apos; jest większy niż dozwolone maksimum -- pominięto</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1510"/>
        <source>size of &apos;%1&apos; from &apos;%2&apos; is greater than allowed maximum -- skipped</source>
        <translation>rozmiar &apos;%1&apos; z &apos;%2&apos; jest większy niż dozwolone maksimum -- pominięto</translation>
    </message>
</context>
<context>
    <name>ROMStatusExporter</name>
    <message>
        <location filename="../../romstatusexport.ui" line="15"/>
        <source>ROM status export</source>
        <translation>Eksport stanu ROM-ów</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="23"/>
        <location filename="../../romstatusexport.ui" line="36"/>
        <source>Select output format</source>
        <translation>Wybierz format wyjściowy</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="40"/>
        <source>ASCII</source>
        <translation>ASCII</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="45"/>
        <source>CSV</source>
        <translation>CSV</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="339"/>
        <source>Browse ASCII export file</source>
        <translation>Wskaż plik ASCII eksportu</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="342"/>
        <source>ASCII file</source>
        <translation>Plik ASCII</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="362"/>
        <source>ASCII export file</source>
        <translation>Plik ASCII eksportu</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="369"/>
        <source>Column width</source>
        <translation>Szerokość kolumny</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="385"/>
        <source>Maximum column width for ASCII export (0 = unlimited)</source>
        <translation>Maksymalna szerokość kolumny dla eksportu ASCII (0 = nieograniczona)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="388"/>
        <source>unlimited</source>
        <translation>nieograniczona</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="444"/>
        <source>Browse CSV export file</source>
        <translation>Wskaż plik CSV eksportu</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="447"/>
        <source>CSV file</source>
        <translation>Plik CSV</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="467"/>
        <source>CSV export file</source>
        <translation>Plik CSV eksportu</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="474"/>
        <source>Separator</source>
        <translation>Separator pola</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="515"/>
        <source>Field separator for CSV export</source>
        <translation>Separator pola dla eksportu do CSV</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="518"/>
        <source>;</source>
        <translation>;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="531"/>
        <source>Delimiter</source>
        <translation>Separator tekstu</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="559"/>
        <source>Text delimiter for CSV export</source>
        <translation>Separator tekstu dla eksportu do CSV</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="562"/>
        <source>&quot;</source>
        <translation>&quot;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="81"/>
        <source>Export ROM state C (correct)?</source>
        <translation>Eksportować stan ROM-ów P (poprawny)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="98"/>
        <source>Export ROM state M (mostly correct)?</source>
        <translation>Eksportować stan ROM-ów Wp (w większości poprawny)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="115"/>
        <source>Export ROM state I (incorrect)?</source>
        <translation>Eksportować stan ROM-ów Np (niepoprawny)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="132"/>
        <source>Export ROM state N (not found)?</source>
        <translation>Eksportować stan ROM-ów Nz (nieznaleziony)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="149"/>
        <source>Export ROM state U (unknown)?</source>
        <translation>Eksportować stan ROM-ów Nn (nieznany)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="269"/>
        <source>Include some header information in export</source>
        <translation>Zawrzyj pewne infomacje nagłówkowe w eksporcie</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="272"/>
        <source>Include header</source>
        <translation>Zawrzyj nagłówek</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="282"/>
        <source>Include statistical overview of the ROM state in export</source>
        <translation>Zawrzyj przegląd statystyczny stanu ROM-ów w eksporcie</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="285"/>
        <source>Include ROM statistics</source>
        <translation>Zawrzyj statystyki ROM-ów</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="728"/>
        <source>Close ROM status export</source>
        <translation>Zamknij eksport stanu ROM-ów</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="731"/>
        <source>&amp;Close</source>
        <translation>&amp;Zamknij</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="714"/>
        <source>Export now!</source>
        <translation>Eksportuj teraz!</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="742"/>
        <source>Export progress indicator</source>
        <translation>Wskaźnik postępu eksportu</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="748"/>
        <source>%p%</source>
        <translation>%p%</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="717"/>
        <source>&amp;Export</source>
        <translation>&amp;Eksportuj</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="72"/>
        <source>Exported ROM states</source>
        <translation>Eksportowane stany ROM-ów</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="50"/>
        <source>HTML</source>
        <translation>HTML</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="295"/>
        <source>Export to the system clipboard instead of a file</source>
        <translation>Eksportuj do schowka systemowego zamiast do pliku</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="298"/>
        <source>Export to clipboard</source>
        <translation>Eksportuj do schowka</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="308"/>
        <source>Overwrite existing files without asking what to do</source>
        <translation>Nadpisz istniejące pliki bez pytania co robić</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="311"/>
        <source>Overwrite blindly</source>
        <translation>Nadpisuj na ślepo</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="618"/>
        <source>Browse HTML export file</source>
        <translation>Wskaż plik HTML eksportu</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="621"/>
        <source>HTML file</source>
        <translation>Plik HTML</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="641"/>
        <source>HTML export file</source>
        <translation>Plik HTML eksportu</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="181"/>
        <source>Sort criteria</source>
        <translation>Kryterium sortowania</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="194"/>
        <source>Select sort criteria</source>
        <translation>Wybierz kryterium sortowania</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="198"/>
        <source>Game description</source>
        <translation>Opis gry</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="203"/>
        <source>ROM state</source>
        <translation>Stan ROM-u</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="208"/>
        <location filename="../../romstatusexport.cpp" line="225"/>
        <location filename="../../romstatusexport.cpp" line="334"/>
        <location filename="../../romstatusexport.cpp" line="555"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="213"/>
        <location filename="../../romstatusexport.cpp" line="224"/>
        <location filename="../../romstatusexport.cpp" line="335"/>
        <location filename="../../romstatusexport.cpp" line="556"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Manufacturer</source>
        <translation>Producent</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="218"/>
        <source>Game name</source>
        <translation>Nazwa gry</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="231"/>
        <source>Sort order</source>
        <translation>Porządek sortowania</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="244"/>
        <source>Select sort order</source>
        <translation>Wybierz porządek sortowania</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="248"/>
        <source>Ascending</source>
        <translation>Rosnący</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="257"/>
        <source>Descending</source>
        <translation>Malejący</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="907"/>
        <source>Choose ASCII export file</source>
        <translation>Wybierz plik ASCII eksportu</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="907"/>
        <location filename="../../romstatusexport.cpp" line="919"/>
        <location filename="../../romstatusexport.cpp" line="931"/>
        <source>All files (*)</source>
        <translation>Wszystkie pliki (*)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="919"/>
        <source>Choose CSV export file</source>
        <translation>Wybierz plik CSV eksportu</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="931"/>
        <source>Choose HTML export file</source>
        <translation>Wybierz plik HTML eksportu</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="134"/>
        <location filename="../../romstatusexport.cpp" line="440"/>
        <location filename="../../romstatusexport.cpp" line="658"/>
        <source>Confirm</source>
        <translation>Potwierdź</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="135"/>
        <location filename="../../romstatusexport.cpp" line="441"/>
        <location filename="../../romstatusexport.cpp" line="659"/>
        <source>Overwrite existing file?</source>
        <translation>Nadpisać istniejący plik?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="150"/>
        <source>exporting ROM status in ASCII format to &apos;%1&apos;</source>
        <translation>eksportowanie stanów ROM-ów w formacie ASCII do &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="152"/>
        <source>WARNING: can&apos;t open ASCII export file &apos;%1&apos; for writing, please check permissions</source>
        <translation>UWAGA: nie można otworzyć pliku ASCII eksportu &apos;%1&apos; do zapisu, proszę sprawdzić uprawnienia</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="158"/>
        <source>exporting ROM status in ASCII format to clipboard</source>
        <translation>eksportowanie stanów ROM-ów w formacie ASCII do schowka</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="196"/>
        <location filename="../../romstatusexport.cpp" line="271"/>
        <location filename="../../romstatusexport.cpp" line="385"/>
        <location filename="../../romstatusexport.cpp" line="488"/>
        <location filename="../../romstatusexport.cpp" line="622"/>
        <location filename="../../romstatusexport.cpp" line="711"/>
        <location filename="../../romstatusexport.cpp" line="873"/>
        <source>unknown</source>
        <translation>nieznany</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="169"/>
        <location filename="../../romstatusexport.cpp" line="198"/>
        <location filename="../../romstatusexport.cpp" line="490"/>
        <location filename="../../romstatusexport.cpp" line="717"/>
        <source>Emulator</source>
        <translation>Emulator</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="170"/>
        <location filename="../../romstatusexport.cpp" line="199"/>
        <location filename="../../romstatusexport.cpp" line="491"/>
        <location filename="../../romstatusexport.cpp" line="719"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="171"/>
        <location filename="../../romstatusexport.cpp" line="200"/>
        <location filename="../../romstatusexport.cpp" line="492"/>
        <location filename="../../romstatusexport.cpp" line="721"/>
        <source>Time</source>
        <translation>Godzina</translation>
    </message>
    <message>
        <source>Total games</source>
        <translation type="obsolete">Wszystkich gier</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="173"/>
        <location filename="../../romstatusexport.cpp" line="208"/>
        <location filename="../../romstatusexport.cpp" line="499"/>
        <location filename="../../romstatusexport.cpp" line="735"/>
        <source>Correct</source>
        <translation>Poprawnych</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="174"/>
        <location filename="../../romstatusexport.cpp" line="209"/>
        <location filename="../../romstatusexport.cpp" line="500"/>
        <location filename="../../romstatusexport.cpp" line="737"/>
        <source>Mostly correct</source>
        <translation>W większości poprawnych</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="175"/>
        <location filename="../../romstatusexport.cpp" line="210"/>
        <location filename="../../romstatusexport.cpp" line="501"/>
        <location filename="../../romstatusexport.cpp" line="739"/>
        <source>Incorrect</source>
        <translation>Niepoprawnych</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="176"/>
        <location filename="../../romstatusexport.cpp" line="211"/>
        <location filename="../../romstatusexport.cpp" line="502"/>
        <location filename="../../romstatusexport.cpp" line="741"/>
        <source>Not found</source>
        <translation>Nieznalezionych</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="177"/>
        <location filename="../../romstatusexport.cpp" line="212"/>
        <location filename="../../romstatusexport.cpp" line="503"/>
        <location filename="../../romstatusexport.cpp" line="743"/>
        <source>Unknown</source>
        <translation>Nieznanych</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="188"/>
        <location filename="../../romstatusexport.cpp" line="480"/>
        <location filename="../../romstatusexport.cpp" line="703"/>
        <source>SDLMAME</source>
        <translation>SDLMAME</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="190"/>
        <location filename="../../romstatusexport.cpp" line="482"/>
        <location filename="../../romstatusexport.cpp" line="705"/>
        <source>SDLMESS</source>
        <translation>SDLMESS</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="192"/>
        <location filename="../../romstatusexport.cpp" line="484"/>
        <location filename="../../romstatusexport.cpp" line="707"/>
        <source>MAME</source>
        <translation>MAME</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="194"/>
        <location filename="../../romstatusexport.cpp" line="486"/>
        <location filename="../../romstatusexport.cpp" line="709"/>
        <source>MESS</source>
        <translation>MESS</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="223"/>
        <location filename="../../romstatusexport.cpp" line="226"/>
        <location filename="../../romstatusexport.cpp" line="336"/>
        <location filename="../../romstatusexport.cpp" line="557"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>ROM types</source>
        <translation>Rodzaje ROM-ów</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="419"/>
        <source>done (exporting ROM status in ASCII format to clipboard)</source>
        <translation>ukończono (eksportowanie stanów ROM-ów w formacie ASCII do schowka)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="422"/>
        <source>done (exporting ROM status in ASCII format to &apos;%1&apos;)</source>
        <translation>ukończono (eksportowanie stanów ROM-ów w formacie ASCII do &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="944"/>
        <source>gamelist is not ready, please wait</source>
        <translation>lista gier nie jest gotowa, proszę czekać</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="222"/>
        <location filename="../../romstatusexport.cpp" line="331"/>
        <location filename="../../romstatusexport.cpp" line="552"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="223"/>
        <location filename="../../romstatusexport.cpp" line="332"/>
        <location filename="../../romstatusexport.cpp" line="553"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Status</source>
        <translation>Stan</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="221"/>
        <location filename="../../romstatusexport.cpp" line="333"/>
        <location filename="../../romstatusexport.cpp" line="554"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="242"/>
        <location filename="../../romstatusexport.cpp" line="368"/>
        <location filename="../../romstatusexport.cpp" line="577"/>
        <location filename="../../romstatusexport.cpp" line="820"/>
        <source>correct</source>
        <translation>poprawny</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="249"/>
        <location filename="../../romstatusexport.cpp" line="372"/>
        <location filename="../../romstatusexport.cpp" line="588"/>
        <location filename="../../romstatusexport.cpp" line="833"/>
        <source>mostly correct</source>
        <translation>w większości poprawny</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="256"/>
        <location filename="../../romstatusexport.cpp" line="376"/>
        <location filename="../../romstatusexport.cpp" line="599"/>
        <location filename="../../romstatusexport.cpp" line="846"/>
        <source>incorrect</source>
        <translation>niepoprawny</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="263"/>
        <location filename="../../romstatusexport.cpp" line="380"/>
        <location filename="../../romstatusexport.cpp" line="610"/>
        <location filename="../../romstatusexport.cpp" line="859"/>
        <source>not found</source>
        <translation>nieznaleziony</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="219"/>
        <source>sorting, filtering and analyzing export data</source>
        <translation>sortowanie, filtrowanie i analizowanie eksportowanych danych</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="172"/>
        <location filename="../../romstatusexport.cpp" line="207"/>
        <location filename="../../romstatusexport.cpp" line="498"/>
        <location filename="../../romstatusexport.cpp" line="733"/>
        <source>Total sets</source>
        <translation>Wszystkich zestawów</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="328"/>
        <source>done (sorting, filtering and analyzing export data)</source>
        <translation>ukończono (sortowanie, filtrowanie i analizowanie eksportowanych danych)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="329"/>
        <location filename="../../romstatusexport.cpp" line="550"/>
        <location filename="../../romstatusexport.cpp" line="794"/>
        <source>writing export data</source>
        <translation>zapisywanie eksportowanych danych</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="415"/>
        <location filename="../../romstatusexport.cpp" line="633"/>
        <location filename="../../romstatusexport.cpp" line="887"/>
        <source>done (writing export data)</source>
        <translation>ukończono (zapisywanie eksportowanych danych)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="456"/>
        <source>exporting ROM status in CSV format to &apos;%1&apos;</source>
        <translation>eksportowanie stanów ROM-ów w formacie CSV do &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="458"/>
        <source>WARNING: can&apos;t open CSV export file &apos;%1&apos; for writing, please check permissions</source>
        <translation>UWAGA: nie można otworzyć pliku CSV eksportu &apos;%1&apos; do zapisu, proszę sprawdzić uprawnienia</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="464"/>
        <source>exporting ROM status in CSV format to clipboard</source>
        <translation>eksportowanie stanów ROM-ów w formacie CSV do schowka</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="509"/>
        <location filename="../../romstatusexport.cpp" line="753"/>
        <source>sorting and filtering export data</source>
        <translation>sortowanie i filtrowanie eksportowanych danych</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="637"/>
        <source>done (exporting ROM status in CSV format to clipboard)</source>
        <translation>ukończono (eksportowanie stanów ROM-ów w formacie CSV do schowka)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="640"/>
        <source>done (exporting ROM status in CSV format to &apos;%1&apos;)</source>
        <translation>ukończono (eksportowanie stanów ROM-ów w formacie CSV do &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="185"/>
        <location filename="../../romstatusexport.cpp" line="186"/>
        <location filename="../../romstatusexport.cpp" line="478"/>
        <location filename="../../romstatusexport.cpp" line="697"/>
        <location filename="../../romstatusexport.cpp" line="713"/>
        <source>ROM Status Export - created by QMC2 %1</source>
        <translation>Eksport stanu ROM-ów - utworzony przez QMC2 %1</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="205"/>
        <location filename="../../romstatusexport.cpp" line="206"/>
        <location filename="../../romstatusexport.cpp" line="497"/>
        <location filename="../../romstatusexport.cpp" line="729"/>
        <source>Overall ROM Status</source>
        <translation>Ogólny stan ROM-ów</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="215"/>
        <location filename="../../romstatusexport.cpp" line="216"/>
        <location filename="../../romstatusexport.cpp" line="506"/>
        <location filename="../../romstatusexport.cpp" line="749"/>
        <source>Detailed ROM Status</source>
        <translation>Szczegółowy stan ROM-ów</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="674"/>
        <source>exporting ROM status in HTML format to &apos;%1&apos;</source>
        <translation>eksportowanie stanów ROM-ów w formacie HTML do &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="676"/>
        <source>WARNING: can&apos;t open HTML export file &apos;%1&apos; for writing, please check permissions</source>
        <translation>UWAGA: nie można otworzyć pliku HTML eksportu &apos;%1&apos; do zapisu, proszę sprawdzić uprawnienia</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="682"/>
        <source>exporting ROM status in HTML format to clipboard</source>
        <translation>eksportowanie stanów ROM-ów w formacie HTML do schowka</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="891"/>
        <source>done (exporting ROM status in HTML format to clipboard)</source>
        <translation>ukończono (eksportowanie stanów ROM-ów w formacie HTML do schowka)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="894"/>
        <source>done (exporting ROM status in HTML format to &apos;%1&apos;)</source>
        <translation>ukończono (eksportowanie stanów ROM-ów w formacie HTML do &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="648"/>
        <source>Border width</source>
        <translation>Szerokość granicy</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="664"/>
        <source>Border line width for tables (0 = no border)</source>
        <translation>Szerokość linii granicznej dla tabel (0 = bez granicy)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="549"/>
        <location filename="../../romstatusexport.cpp" line="793"/>
        <source>done (sorting and filtering export data)</source>
        <translation>ukończono (sortowanie i filtrowanie eksportowanych danych)</translation>
    </message>
    <message>
        <source>Total machines</source>
        <translation type="obsolete">Wszystkich maszyn</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="34"/>
        <source>Machine description</source>
        <translation>Opis maszyny</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="35"/>
        <source>Machine name</source>
        <translation>Nazwa maszyny</translation>
    </message>
</context>
<context>
    <name>SampleChecker</name>
    <message>
        <location filename="../../sampcheck.ui" line="15"/>
        <source>Check samples</source>
        <translation>Sprawdź sample</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="94"/>
        <source>Select game in gamelist when selecting a sample set?</source>
        <translation>Wybrać grę na liście gier podczas wyboru zestawu sampli?</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="97"/>
        <source>Select &amp;game</source>
        <translation>Wybierz &amp;grę</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="120"/>
        <source>Close sample check dialog</source>
        <translation>Zamknij okno sprawdzania sampli</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="123"/>
        <source>C&amp;lose</source>
        <translation>&amp;Zamknij</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="84"/>
        <source>Remove obsolete sample sets</source>
        <translation>Usuń przestarzałe zestawy sampli</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="87"/>
        <source>&amp;Remove obsolete</source>
        <translation>&amp;Usuń przestarzałe</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="71"/>
        <source>Check samples / stop check</source>
        <translation>Sprawdź sample / zatrzymaj sprawdzanie</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="74"/>
        <location filename="../../sampcheck.cpp" line="232"/>
        <source>&amp;Check samples</source>
        <translation>&amp;Sprawdź sample</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="29"/>
        <location filename="../../sampcheck.cpp" line="134"/>
        <source>Good: 0</source>
        <translation>Dobre: 0</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="55"/>
        <location filename="../../sampcheck.cpp" line="138"/>
        <source>Obsolete: 0</source>
        <translation>Przestarzałe: 0</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="42"/>
        <location filename="../../sampcheck.cpp" line="136"/>
        <source>Bad: 0</source>
        <translation>Złe: 0</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="132"/>
        <source>verifying samples</source>
        <translation>weryfikacja sampli</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="140"/>
        <source>check pass 1: sample status</source>
        <translation>1. przebieg sprawdzający: stan sampli</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="194"/>
        <source>check pass 2: obsolete sample sets</source>
        <translation>2. przebieg sprawdzający: przestarzałe zestawy sampli</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="216"/>
        <source>Obsolete: %1</source>
        <translation>Przestarzałe: %1</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="226"/>
        <source>done (verifying samples, elapsed time = %1)</source>
        <translation>ukończono (weryfikację sampli, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="227"/>
        <source>%1 good, %2 bad (or missing), %3 obsolete</source>
        <translation>%1 dobrych, %2 złe (lub brakujące), %3 przestarzałe</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="267"/>
        <location filename="../../sampcheck.cpp" line="318"/>
        <source>Good: %1</source>
        <translation>Dobrych: %1</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="273"/>
        <location filename="../../sampcheck.cpp" line="324"/>
        <source>Bad: %1</source>
        <translation>Złych: %1</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="361"/>
        <source>please wait for reload to finish and try again</source>
        <translation>proszę poczekać za zakończenie przeładowywania i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="366"/>
        <source>please wait for ROM state filter to finish and try again</source>
        <translation>proszę poczekać za zakończenie filtra stanu ROM-ów i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="371"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation>proszę poczekać za zakończenie weryfikacji ROM-ów i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="376"/>
        <source>please wait for image check to finish and try again</source>
        <translation>proszę poczekać za zakończenie sprawdzania obrazów i spróbować ponownie</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="381"/>
        <source>stopping sample check upon user request</source>
        <translation>zatrzymywanie sprawdzania sampli na żądanie użytkownika</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="386"/>
        <source>&amp;Stop check</source>
        <translation>Za&amp;trzymaj sprawdzanie</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="178"/>
        <source>WARNING: emulator audit call didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation>UWAGA: wywołanie audytu emulatora nie zakończyło się czysto -- exitCode = %1, exitStatus = %2</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="178"/>
        <source>normal</source>
        <translation>normalny</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="178"/>
        <source>crashed</source>
        <translation>przestał działać</translation>
    </message>
</context>
<context>
    <name>SnapshotViewer</name>
    <message>
        <location filename="../../embedderopt.cpp" line="156"/>
        <source>Snapshot viewer</source>
        <translation>Przeglądarka zrzutów ekranu</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="165"/>
        <source>Use as preview</source>
        <translation>Użyj jako podglądu</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="171"/>
        <source>Use as title</source>
        <translation>Użyj jako ekranu tytułowego</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="180"/>
        <source>Save as...</source>
        <translation>Zapisz jako...</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="186"/>
        <source>Copy to clipboard</source>
        <translation>Kopiuj do schowka</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="291"/>
        <source>Choose PNG file to store image</source>
        <translation>Wybierz plik PNG do zapisania obrazu</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="291"/>
        <source>PNG images (*.png)</source>
        <translation>Obrazy PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="296"/>
        <source>FATAL: couldn&apos;t save snapshot image to &apos;%1&apos;</source>
        <translation>FATALNIE: nie udało się zapisać zrzutu ekranu do &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SoftwareList</name>
    <message>
        <location filename="../../softwarelist.ui" line="18"/>
        <source>Software list</source>
        <translation>Lista oprogramowania</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="35"/>
        <location filename="../../softwarelist.ui" line="38"/>
        <source>Reload all information</source>
        <translation>Przeładuj wszysktie informacje</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="55"/>
        <location filename="../../softwarelist.ui" line="58"/>
        <source>Select a pre-defined device configuration to be added to the software setup</source>
        <translation>Wybierz predefiniowaną konfigurację urządzeń do dodania do ustawień oprogramowania</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="62"/>
        <location filename="../../softwarelist.cpp" line="877"/>
        <location filename="../../softwarelist.cpp" line="1377"/>
        <source>No additional devices</source>
        <translation>Brak dodatkowych urządzeń</translation>
    </message>
    <message>
        <source>Add the currently selected software and device setup to the favorites list</source>
        <translation type="obsolete">Dodaj obecnie wybrane oprogramowanie i ustawienia urządzeń do listy ulubionych</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="109"/>
        <location filename="../../softwarelist.ui" line="112"/>
        <source>Remove the currently selected favorite software configuration</source>
        <translation>Usuń obecnie wybraną ulubioną konfigurację oprogramowania</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="129"/>
        <location filename="../../softwarelist.ui" line="132"/>
        <source>Play the selected software configuration</source>
        <translation>Uruchom używając wybranej konfiguracji oprogramowania</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="149"/>
        <location filename="../../softwarelist.ui" line="152"/>
        <source>Play the selected software configuration (embedded)</source>
        <translation>Uruchom używając wybranej konfiguracji oprogramowania (tryb osadzony)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="184"/>
        <source>Known software</source>
        <translation>Znane oprogramowanie</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="187"/>
        <source>Complete list of known software for the current system</source>
        <translation>Pełna lista znanego oprogramowania na bieżący system</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="275"/>
        <source>View / manage your favorite software list for the current system</source>
        <translation>Zobacz / zarządzaj listą ulubionego oprogramowania bieżącego systemu</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="368"/>
        <source>Search within the list of known software for the current system</source>
        <translation>Szukanie wśród listy znanego oprogramowania bieżącego systemu</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="202"/>
        <location filename="../../softwarelist.ui" line="205"/>
        <source>List of known software</source>
        <translation>Lista znanego oprogramowania</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="89"/>
        <location filename="../../softwarelist.ui" line="92"/>
        <source>Add the currently selected software and device setup to the favorites list (or overwrite existing favorite)</source>
        <translation>Dodaj obecnie wybrane oprogramowanie i ustawienia urządzeń do listy ulubionych (lub nadpisz istniejące)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="221"/>
        <location filename="../../softwarelist.ui" line="309"/>
        <location filename="../../softwarelist.ui" line="421"/>
        <location filename="../../softwarelist.cpp" line="128"/>
        <location filename="../../softwarelist.cpp" line="147"/>
        <location filename="../../softwarelist.cpp" line="168"/>
        <source>Title</source>
        <translation>Tytuł</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="226"/>
        <location filename="../../softwarelist.ui" line="314"/>
        <location filename="../../softwarelist.ui" line="426"/>
        <location filename="../../softwarelist.cpp" line="130"/>
        <location filename="../../softwarelist.cpp" line="149"/>
        <location filename="../../softwarelist.cpp" line="170"/>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="231"/>
        <location filename="../../softwarelist.ui" line="319"/>
        <location filename="../../softwarelist.ui" line="431"/>
        <location filename="../../softwarelist.cpp" line="132"/>
        <location filename="../../softwarelist.cpp" line="151"/>
        <location filename="../../softwarelist.cpp" line="172"/>
        <source>Publisher</source>
        <translation>Wydawca</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="236"/>
        <location filename="../../softwarelist.ui" line="324"/>
        <location filename="../../softwarelist.ui" line="436"/>
        <location filename="../../softwarelist.cpp" line="134"/>
        <location filename="../../softwarelist.cpp" line="153"/>
        <location filename="../../softwarelist.cpp" line="174"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="241"/>
        <location filename="../../softwarelist.ui" line="329"/>
        <location filename="../../softwarelist.ui" line="441"/>
        <location filename="../../softwarelist.cpp" line="136"/>
        <location filename="../../softwarelist.cpp" line="155"/>
        <location filename="../../softwarelist.cpp" line="176"/>
        <source>Part</source>
        <translation>Część</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="246"/>
        <location filename="../../softwarelist.ui" line="334"/>
        <location filename="../../softwarelist.ui" line="446"/>
        <location filename="../../softwarelist.cpp" line="138"/>
        <location filename="../../softwarelist.cpp" line="157"/>
        <location filename="../../softwarelist.cpp" line="178"/>
        <source>Interface</source>
        <translation>Interfejs</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="251"/>
        <location filename="../../softwarelist.ui" line="339"/>
        <location filename="../../softwarelist.ui" line="451"/>
        <location filename="../../softwarelist.cpp" line="140"/>
        <location filename="../../softwarelist.cpp" line="159"/>
        <location filename="../../softwarelist.cpp" line="180"/>
        <source>List</source>
        <translation>Lista</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="272"/>
        <source>Favorites</source>
        <translation>Ulubione</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="290"/>
        <location filename="../../softwarelist.ui" line="293"/>
        <source>Favorite software configurations</source>
        <translation>Ulubione konfiguracje oprogramowania</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="344"/>
        <location filename="../../softwarelist.cpp" line="161"/>
        <source>Device configuration</source>
        <translation>Konfiguracja urządzeń</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="365"/>
        <source>Search</source>
        <translation>Szukaj</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="383"/>
        <location filename="../../softwarelist.ui" line="386"/>
        <source>Search for known software (not case-sensitive)</source>
        <translation>Szukaj znanego oprogramowania (bez uwzględniania wielkości liter)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="402"/>
        <location filename="../../softwarelist.ui" line="405"/>
        <source>Search results</source>
        <translation>Wyniki wyszukiwania</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="475"/>
        <source>Loading software-lists, please wait...</source>
        <translation>Wczytywanie list oprogramowania, proszę czekać...</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="54"/>
        <source>Add the currently selected software to the favorites list</source>
        <translation>Dodaj obecnie wybrane oprogramowanie do listy ulubionych</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="61"/>
        <source>Enter search string</source>
        <translation>Wpisz szukany ciąg</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="88"/>
        <source>Play selected software</source>
        <translation>Uruchom wybrane oprogramowanie</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="89"/>
        <source>&amp;Play</source>
        <translation>&amp;Graj</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="94"/>
        <source>Play selected software (embedded)</source>
        <translation>Uruchom wybrane oprogramowanie (tryb osadzony)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="95"/>
        <source>Play &amp;embedded</source>
        <translation>Graj osad&amp;zając</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="101"/>
        <source>Add to favorite software list</source>
        <translation>Dodaj do listy ulubionego oprogramowania</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="102"/>
        <source>&amp;Add to favorites</source>
        <translation>&amp;Dodaj do ulubionych</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="106"/>
        <source>Remove from favorite software list</source>
        <translation>Usuń z listy ulubionego oprogramowania</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="107"/>
        <source>&amp;Remove from favorites</source>
        <translation>&amp;Usuń z ulubionych</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="221"/>
        <source>WARNING: software list &apos;%1&apos; not found</source>
        <translation>UWAGA: nie znaleziono listy oprogramowania &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="335"/>
        <source>Known software (%1)</source>
        <translation>Znane oprogramowanie (%1)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="336"/>
        <source>Favorites (%1)</source>
        <translation>Ulubione (%1)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="337"/>
        <source>Search (%1)</source>
        <translation>Wyszukiwanie (%1)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="351"/>
        <location filename="../../softwarelist.cpp" line="545"/>
        <source>Known software (no data available)</source>
        <translation>Znane oprogramowanie (brak dostępnych danych)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="352"/>
        <location filename="../../softwarelist.cpp" line="546"/>
        <source>Favorites (no data available)</source>
        <translation>Ulubione (brak dostępnych danych)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="353"/>
        <location filename="../../softwarelist.cpp" line="547"/>
        <source>Search (no data available)</source>
        <translation>Wyszukiwanie (brak dostępnych danych)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="419"/>
        <source>loading XML software list data from cache</source>
        <translation>Ładowanie listy XML oprogramowania z bufora</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="424"/>
        <source>SWL cache - %p%</source>
        <translation>Bufor LOp - %p%</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="452"/>
        <source>done (loading XML software list data from cache, elapsed time = %1)</source>
        <translation>ukończono (wczytywanie listy oprogramowania XML z bufora, miniony czas = %1)</translation>
    </message>
    <message>
        <source>done (loading XML software list data from cache, elapsed time = %1</source>
        <translation type="obsolete">ukończono (wczytywanie listy XML oprogramowania z bufora, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="460"/>
        <source>ERROR: the file name for the MAME software list cache is empty -- please correct this and reload the game list afterwards</source>
        <translation>BŁĄD: nazwa pliku bufora listy oprogramowania MAME jest pusta -- proszę to poprawić, a następnie przeładować listę maszyn</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="462"/>
        <source>ERROR: the file name for the MESS software list cache is empty -- please correct this and reload the machine list afterwards</source>
        <translation>BŁĄD: nazwa pliku bufora listy oprogramowania MESS jest pusta -- proszę to poprawić, a następnie przeładować listę maszyn</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="478"/>
        <source>loading XML software list data and (re)creating cache</source>
        <translation>wczytywanie danych listy XML oprogramowania i tworzenie (odtwarzanie) bufora</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="481"/>
        <source>SWL data - %p%</source>
        <translation>Dane LOp - %p%</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="487"/>
        <source>ERROR: can&apos;t open the MAME software list cache for writing, path = %1 -- please check/correct access permissions and reload the game list afterwards</source>
        <translation>BŁĄD: nie można otworzyć bufora listy oprogramowania MAME do zapisu, ścieżka = %1 -- proszę sprawdzić/poprawić uprawnienia, a następnie przeładować listę maszyn</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="489"/>
        <source>ERROR: can&apos;t open the MESS software list cache for writing, path = %1 -- please check/correct access permissions and reload the machine list afterwards</source>
        <translation>BŁĄD: nie można otworzyć bufora listy oprogramowania MESS do zapisu, ścieżka = %1 -- proszę sprawdzić/poprawić uprawnienia, a następnie przeładować listę maszyn</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="579"/>
        <source>FATAL: error while parsing XML data for software list &apos;%1&apos;</source>
        <translation>FATALNIE: błąd podczas parsowania danych XML listy oprogramowania &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="759"/>
        <source>WARNING: the external process called to load the MAME software lists didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation>UWAGA: zewnętrzny proces wezwany do wczytania listy oprogramowania MAME nie zakończył się czysto -- exitCode = %1, exitStatus = %2</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="759"/>
        <location filename="../../softwarelist.cpp" line="761"/>
        <source>normal</source>
        <translation>normalny</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="759"/>
        <location filename="../../softwarelist.cpp" line="761"/>
        <source>crashed</source>
        <translation>przestał działać</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="761"/>
        <source>WARNING: the external process called to load the MESS software lists didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation>UWAGA: zewnętrzny proces wezwany do wczytania listy oprogramowania MESS nie zakończył się czysto -- exitCode = %1, exitStatus = %2</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="765"/>
        <source>done (loading XML software list data and (re)creating cache, elapsed time = %1)</source>
        <translation>ukończono (wczytywanie listy XML oprogramowania i tworzenie (odtwarzanie) bufora, miniony czas = %1)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="818"/>
        <source>WARNING: the currently selected MAME emulator doesn&apos;t support software lists</source>
        <translation>UWAGA: obecnie wybrany emulator MAME nie obsługuje list oprogramowania</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="820"/>
        <source>WARNING: the currently selected MESS emulator doesn&apos;t support software lists</source>
        <translation>UWAGA: obecnie wybrany emulator MESS nie obsługuje list oprogramowania</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="837"/>
        <source>WARNING: the external process called to load the MAME software lists caused an error -- processError = %1</source>
        <translation>UWAGA: zewnętrzny proces uruchomiony do wczytania list oprogramowania MAME spowodował błąd -- processError = %1</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="839"/>
        <source>WARNING: the external process called to load the MESS software lists caused an error -- processError = %1</source>
        <translation>UWAGA: zewnętrzny proces uruchomiony do wczytania list oprogramowania MESS spowodował błąd -- processError = %1</translation>
    </message>
</context>
<context>
    <name>SoftwareSnap</name>
    <message>
        <location filename="../../softwarelist.cpp" line="1585"/>
        <source>Snapshot viewer</source>
        <translation>Przeglądarka zrzutów ekranu</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="1598"/>
        <source>Copy to clipboard</source>
        <translation>Kopiuj do schowka</translation>
    </message>
</context>
<context>
    <name>Title</name>
    <message>
        <location filename="../../title.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>Kopiuj do schowka</translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="56"/>
        <location filename="../../title.cpp" line="57"/>
        <source>Game title image</source>
        <translation>Obraz ekranu tytułowego gry</translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="59"/>
        <location filename="../../title.cpp" line="60"/>
        <source>Machine title image</source>
        <translation>Obraz ekranu tytułowego maszyny</translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="68"/>
        <location filename="../../title.cpp" line="72"/>
        <source>FATAL: can&apos;t open title file, please check access permissions for %1</source>
        <translation>FATALNIE: nie można otworzyć pliku ekranu tytułowego, proszę sprawdzić uprawnienia dostępu dla %1</translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Oczekiwanie na dane...</translation>
    </message>
</context>
<context>
    <name>ToolExecutor</name>
    <message>
        <location filename="../../toolexec.ui" line="15"/>
        <source>Executing tool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="29"/>
        <source>Command</source>
        <translation>Polecenie</translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="42"/>
        <source>Executed command</source>
        <translation>Wykonane polecenie</translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="91"/>
        <source>Close tool execution dialog</source>
        <translation>Zamknij okno dialogowe uruchamiania narzędzi</translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="94"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="54"/>
        <source>Output from tool</source>
        <translation>Wyjście z narzędzia</translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="60"/>
        <location filename="../../toolexec.cpp" line="62"/>
        <source>### tool started, output below ###</source>
        <translation>### narzędzie uruchomiono, wyjście poniżej ###</translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="62"/>
        <location filename="../../toolexec.cpp" line="81"/>
        <location filename="../../toolexec.cpp" line="128"/>
        <source>tool control: </source>
        <translation>sterowanie narzędziem: </translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="79"/>
        <location filename="../../toolexec.cpp" line="81"/>
        <source>### tool finished, exit code = %1, exit status = %2 ###</source>
        <translation>### narzędzie ukończyło pracę, kod wyjścia = %1, stan wyjścia = %2 ###</translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="100"/>
        <location filename="../../toolexec.cpp" line="116"/>
        <source>tool output: </source>
        <translation>wyjście narzędzia: </translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="100"/>
        <source>stdout: %1</source>
        <translation>stdout: %1</translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="116"/>
        <source>stderr: %1</source>
        <translation>stderr: %1</translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="126"/>
        <location filename="../../toolexec.cpp" line="128"/>
        <source>### tool error, process error = %1 ###</source>
        <translation>### błąd narzędzia, błąd procesu = %1 ###</translation>
    </message>
</context>
<context>
    <name>VideoItemWidget</name>
    <message>
        <location filename="../../videoitemwidget.cpp" line="173"/>
        <source>Title:</source>
        <translation>Tytuł:</translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="178"/>
        <location filename="../../videoitemwidget.cpp" line="180"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="178"/>
        <source>Open author URL with the default browser</source>
        <translation>Otwórz URL autora w domyślnej przeglądarce</translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="186"/>
        <source>Open video URL with the default browser</source>
        <translation>Otwórz URL filmu w domyślnej przeglądarce</translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="186"/>
        <location filename="../../videoitemwidget.cpp" line="188"/>
        <source>Video:</source>
        <translation>Film:</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../../welcome.ui" line="15"/>
        <source>Welcome to QMC2</source>
        <translation>Witamy w QMC2</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="606"/>
        <source>Browse sample path</source>
        <translation>Wskaż ścieżkę sampli</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="582"/>
        <source>Browse ROM path</source>
        <translation>Wskaż ścieżkę ROM-ów</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="575"/>
        <source>Path to ROM images</source>
        <translation>Ścieżka do obrazów ROM-ów</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="599"/>
        <source>Path to samples</source>
        <translation>Ścieżka do sampli</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="630"/>
        <source>Emulator executable</source>
        <translation>Plik wykonywalny emulatora</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="637"/>
        <source>ROM path</source>
        <translation>Ścieżka do ROM-ów</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="644"/>
        <source>Sample path</source>
        <translation>Ścieżka do sampli</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="651"/>
        <source>Hash path</source>
        <translation>Ścieżka do skrótów</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="664"/>
        <source>Path to hash files</source>
        <translation>Ścieżka do plików skrótów</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="671"/>
        <source>Browse hash path</source>
        <translation>Wskaż ścieżkę skrótów</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="716"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="709"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="528"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Welcome to QMC2!&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This appears to be your first start of QMC2 because no valid configuration was found. In order to use QMC2 as a front end for an emulator, you must specify the path to the emulator&apos;s executable file below.&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The paths below the executable file are optional, but you should specify as many of them as you can right now to avoid problems or confusion later (of course, you can change the paths in the emulator&apos;s global configuration at any time later).&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;It&apos;s strongly recommended that you specify the ROM path you are going to use at least!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Welcome to QMC2!&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Nie znaleziono poprawnej konfiguracji, co wskazuje na to, iż jest to pierwsze uruchomienie QMC2. Aby używać QMC2 jako interfejsu graficznego do emulatora, należy podać ścieżkę do jego pliku wykonywalnego poniżej.&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Ścieżki poniżej pliku wykonywalnego są opcjonalne, jednakże powinno się podać teraz jak najwięcej z nich aby później uniknąć problemów i niejasności (ścieżki można potem oczywiście zmienić w globalnej konfiguracji emulatora).&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Zdecydowanie zalecane jest podanie co najmniej ścieżki ROM-ów, z której zamierza się korzystać!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="551"/>
        <source>Emulator executable file</source>
        <translation>Plik wykonywalny emulatora</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="558"/>
        <source>Browse emulator executable file</source>
        <translation>Wskaż plik wykonywalny emulatora</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="94"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="94"/>
        <source>The specified file isn&apos;t executable!</source>
        <translation>Wybrany plik nie jest wykonywalny!</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="109"/>
        <source>Choose emulator executable file</source>
        <translation>Wybierz plik wykonywalny emulatora</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="109"/>
        <source>All files (*)</source>
        <translation>Wszystkie pliki (*)</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="121"/>
        <source>Choose ROM path</source>
        <translation>Wybierz ścieżkę ROM-ów</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="134"/>
        <source>Choose sample path</source>
        <translation>Wybierz ścieżkę sampli</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="221"/>
        <source>It appears that another instance of %1 is already running.
However, this can also be the leftover of a previous crash.

Exit now, accept once or ignore completely?</source>
        <translation>Wydaje się, że jest już uruchomiona inna instancja %1.
Jednakowoż, może to być pozostałość po poprzedniej awarii.

Wyjść teraz, zaakceptować raz czy zupełnie zignorować?</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="47"/>
        <source>Unsupported emulator</source>
        <translation>Nieobsługiwany emulator</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="33"/>
        <source>SDLMAME</source>
        <translation>SDLMAME</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="35"/>
        <source>SDLMESS</source>
        <translation>SDLMESS</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="40"/>
        <source>MAME</source>
        <translation>MAME</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="42"/>
        <source>MESS</source>
        <translation>MESS</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="49"/>
        <source>%1 executable file</source>
        <translation>%1 plik wykonywalny</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="147"/>
        <source>Choose hash path</source>
        <translation>Wybierz ścieżkę skrótów</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="220"/>
        <source>Single-instance check</source>
        <translation>Sprawdzenie jednej instancji</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="222"/>
        <source>&amp;Exit</source>
        <translation>&amp;Wyjdź</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="222"/>
        <source>&amp;Once</source>
        <translation>&amp;Raz</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="222"/>
        <source>&amp;Ignore</source>
        <translation>Z&amp;ignoruj</translation>
    </message>
</context>
<context>
    <name>YouTubeVideoPlayer</name>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="40"/>
        <source>Attached videos</source>
        <translation>Załączone filmy</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="74"/>
        <source>Start playing / select next video automatically</source>
        <translation>Rozpocznij odtwarzanie / wybierz następny film automatycznie</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="77"/>
        <source>Play-O-Matic</source>
        <translation>Play-O-Matic</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="93"/>
        <source>Mode:</source>
        <translation>Tryb:</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="112"/>
        <source>Choose the video selection mode</source>
        <translation>Wybierz tryb wyboru filmu</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="116"/>
        <source>sequential</source>
        <translation>kolejno</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="121"/>
        <source>random</source>
        <translation>losowo</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="138"/>
        <source>Allow videos to be repeated (otherwise stop after last video)</source>
        <translation>Pozwalaj na powtarzanie filmów (w przeciwnym wypadku zatrzymaj po ostatnim filmie)</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="141"/>
        <source>Allow repeat</source>
        <translation>Pozwalaj na powtarzanie</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="174"/>
        <source>Video player</source>
        <translation>Odtwarzacz filmów</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="196"/>
        <source>Select the preferred video format (automatically falls back to the next available format)</source>
        <translation>Wybierz preferowany format filmu (automatycznie wycofuje się do następnego dostępnego)</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="203"/>
        <location filename="../../youtubevideoplayer.cpp" line="79"/>
        <source>FLV 240P</source>
        <translation>FLV 240P</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="212"/>
        <location filename="../../youtubevideoplayer.cpp" line="80"/>
        <source>FLV 360P</source>
        <translation>FLV 360P</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="221"/>
        <location filename="../../youtubevideoplayer.cpp" line="81"/>
        <source>MP4 360P</source>
        <translation>MP4 360P</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="230"/>
        <location filename="../../youtubevideoplayer.cpp" line="82"/>
        <source>FLV 480P</source>
        <translation>FLV 480P</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="239"/>
        <location filename="../../youtubevideoplayer.cpp" line="83"/>
        <source>MP4 720P</source>
        <translation>MP4 720P</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="248"/>
        <location filename="../../youtubevideoplayer.cpp" line="84"/>
        <source>MP4 1080P</source>
        <translation>MP4 1080P</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="257"/>
        <location filename="../../youtubevideoplayer.cpp" line="85"/>
        <source>MP4 3072P</source>
        <translation>MP4 3072P</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="275"/>
        <location filename="../../youtubevideoplayer.cpp" line="146"/>
        <source>Start / pause / resume video playback</source>
        <translation>Uruchom / wstrzymaj / wznów odtwarzanie filmu</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="315"/>
        <source>Remaining playing time</source>
        <translation>Pozostały czas odtwarzania</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="334"/>
        <source>Current buffer fill level</source>
        <translation>Obecny poziom zapełnienia bufora</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="363"/>
        <source>Search videos</source>
        <translation>Szukaj filmów</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="378"/>
        <source>Search pattern -- use the &apos;hint&apos; button to get a suggestion</source>
        <translation>Wzorzec szukania -- użyj przycisku &apos;podpowiedź&apos; aby otrzymać propozycję</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="391"/>
        <source>Search YouTube videos using the specified search pattern</source>
        <translation>Szukaj filmów YouTube przy pomocy podanego wzorca</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="435"/>
        <source>Maximum number of results per search request</source>
        <translation>Maksymalna liczba wyników na zapytanie</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="466"/>
        <source>Start index for the search request</source>
        <translation>Początkowy indeks zapytania</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="491"/>
        <source>SI:</source>
        <translation>PI:</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="498"/>
        <source>R:</source>
        <translation>W:</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="418"/>
        <source>Suggest a search pattern (hold down for menu)</source>
        <translation>Zaproponuj wzorzec szukania (przytrzymaj dla menu)</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="61"/>
        <source>Mute / unmute audio output</source>
        <translation>Wyciszenie / aktywacja wyjścia dźwięku</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="96"/>
        <source>Video progress</source>
        <translation>Postęp filmu</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="105"/>
        <location filename="../../youtubevideoplayer.cpp" line="724"/>
        <location filename="../../youtubevideoplayer.cpp" line="748"/>
        <location filename="../../youtubevideoplayer.cpp" line="767"/>
        <location filename="../../youtubevideoplayer.cpp" line="814"/>
        <source>Current buffer fill level: %1%</source>
        <translation>Obecny poziom zapełnienia bufora: %1%</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="112"/>
        <location filename="../../youtubevideoplayer.cpp" line="179"/>
        <source>Play this video</source>
        <translation>Odtwarzaj ten film</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="119"/>
        <location filename="../../youtubevideoplayer.cpp" line="161"/>
        <location filename="../../youtubevideoplayer.cpp" line="190"/>
        <source>Copy video URL</source>
        <translation>Kopiuj URL filmu</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="125"/>
        <location filename="../../youtubevideoplayer.cpp" line="166"/>
        <location filename="../../youtubevideoplayer.cpp" line="195"/>
        <source>Copy author URL</source>
        <translation>Kopiuj URL autora</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="131"/>
        <source>Paste video URL</source>
        <translation>Wklej URL filmu</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="138"/>
        <source>Remove selected videos</source>
        <translation>Usuń wybrane filmy</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="154"/>
        <location filename="../../youtubevideoplayer.cpp" line="1246"/>
        <source>Full screen (return with toggle-key)</source>
        <translation>Pełny ekran (powrót za pomocą klawisza przełączającego)</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="172"/>
        <location filename="../../youtubevideoplayer.cpp" line="184"/>
        <source>Attach this video</source>
        <translation>Załącz ten film</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="202"/>
        <source>Auto-suggest a search pattern?</source>
        <translation>Autosugestia wzorca szukania?</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="203"/>
        <source>Auto-suggest</source>
        <translation>Automatyczna sugestia</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="208"/>
        <source>Enter string to be appended</source>
        <translation>Proszę wprowadzić ciąg do dołączenia</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="209"/>
        <source>Append...</source>
        <translation>Dołącz...</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="220"/>
        <source>Enter search string</source>
        <translation>Wpisz szukany ciąg</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="324"/>
        <source>Appended string</source>
        <translation>Dołączony ciąg</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="325"/>
        <source>Enter the string to be appended when suggesting a pattern:</source>
        <translation>Wprowadź ciąg, który ma być dołączony podczas propozycji wzorca:</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="587"/>
        <source>video player: a video with the ID &apos;%1&apos; is already attached, ignored</source>
        <translation>odtwarzacz filmów: film o ID &apos;%1&apos; jest już załączony, zignorowano</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="797"/>
        <source>video player: playback error: %1</source>
        <translation>odtwarzacz filmów: błąd odtwarzania: %1</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="946"/>
        <source>video player: video info error: ID = &apos;%1&apos;, status = &apos;%2&apos;, errorCode = &apos;%3&apos;, errorText = &apos;%4&apos;</source>
        <translation>odtwarzacz filmów: błąd informacji o filmie: ID = &apos;%1&apos;, stan = &apos;%2&apos;, kod błędu = &apos;%3, tekst błędu = &apos;%4&apos;</translation>
    </message>
    <message>
        <source>video player: video info error: status = &apos;%1&apos;, errorCode = &apos;%2&apos;, errorText = &apos;%3&apos;</source>
        <translation type="obsolete">odtwarzacz filmów: błąd informacji o filmie: stan = &apos;%1&apos;, kod błędu = &apos;%2&apos;, tekst błędu = &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1015"/>
        <source>video player: video info error: timeout occurred</source>
        <translation>odtwarzacz filmów: błąd informacji o filmie: przekroczono czas</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1051"/>
        <source>video player: video info error: %1</source>
        <translation>odtwarzacz filmów: błąd informacji o filmie: %1</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1244"/>
        <source>Full screen (press %1 to return)</source>
        <translation>Pełny ekran (powrót za pomocą %1)</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1419"/>
        <source>video player: video image info error: %1</source>
        <translation>odtwarzacz filmów: błąd informacji o obrazie: %1</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1450"/>
        <source>video player: search request error: %1</source>
        <translation>odtwarzacz filmów: błąd zapytania: %1</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1478"/>
        <source>video player: search error: can&apos;t parse XML data</source>
        <translation>odtwarzacz filmów: błąd szukania: nie można parsować danych XML</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1511"/>
        <source>video player: can&apos;t determine the video ID from the reply URL &apos;%1&apos; -- please inform developers</source>
        <translation>odtwarzacz filmów: nie można ustalić ID filmu na podstawie zwróconego URL &apos;%1&apos; -- proszę powiadomić programistów</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1517"/>
        <source>video player: can&apos;t associate the returned image for video ID &apos;%1&apos; -- please inform developers</source>
        <translation>odtwarzacz filmów: nie można powiązać zwróconego obrazu z ID filmu &apos;%1&apos; -- proszę powiadomić programistów</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1539"/>
        <source>video player: can&apos;t save the image for video ID &apos;%1&apos; to the YouTube cache directory &apos;%2&apos; -- please check permissions</source>
        <translation>odtwarzacz filmów: nie można zapisać obrazu dla ID filmu &apos;%1&apos; w katalogu bufora YouTube &apos;%2&apos; -- proszę sprawdzić uprawnienia</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1541"/>
        <source>video player: can&apos;t save the image for video ID &apos;%1&apos;, the YouTube cache directory &apos;%2&apos; does not exist -- please correct</source>
        <translation>odtwarzacz filmów: nie można zapisać obrazu dla ID filmu &apos;%1&apos;, katalog bufora YouTube &apos;%2&apos; nie istnieje -- proszę poprawić</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1543"/>
        <source>video player: image download failed for video ID &apos;%1&apos;, retrieved image is not valid</source>
        <translation>odtwarzacz filmów: pobieranie obrazu nieudane dla ID filmu &apos;%1&apos;, uzyskany obraz jest nieprawidłowy</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1545"/>
        <source>video player: image download failed for video ID &apos;%1&apos;, error text = &apos;%2&apos;</source>
        <translation>odtwarzacz filmów: pobieranie obrazu nieudane dla ID filmu &apos;%1&apos;, tekst błędu = &apos;%2&apos;</translation>
    </message>
</context>
</TS>
