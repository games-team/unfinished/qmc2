<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pt_BR">
<context>
    <name>About</name>
    <message>
        <location filename="../../about.cpp" line="59"/>
        <source>Mac OS X 10.3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="60"/>
        <source>Mac OS X 10.4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="61"/>
        <source>Mac OS X 10.5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="62"/>
        <source>Mac OS X 10.6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="63"/>
        <source>Mac (unkown)</source>
        <translation>Mac (desconhecido)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="67"/>
        <source>Windows NT (Windows 4.0)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="68"/>
        <source>Windows 2000 (Windows 5.0)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="69"/>
        <source>Windows XP (Windows 5.1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="70"/>
        <source>Windows Server 2003, Windows Server 2003 R2, Windows Home Server or Windows XP Professional x64 Edition (Windows 5.2)</source>
        <translation>Windows Server 2003, Windows Server 2003 R2, Windows Home Server ou Windows XP Professional x64 Edition (Windows 5.2)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="71"/>
        <source>Windows Vista or Windows Server 2008 (Windows 6.0)</source>
        <translation>Windows Vista ou Windows Server 2008 (Windows 6.0)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="72"/>
        <source>Windows 7 or Windows Server 2008 R2 (Windows 6.1)</source>
        <translation>Windows 7 ou Windows Server 2008 R2 (Windows 6.1)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="73"/>
        <source>Windows (unknown)</source>
        <translation>Windows (desconhecido)</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="119"/>
        <source>Qt 4 based multi-platform/multi-emulator front end</source>
        <translation>Front end multi-plataforma/multi-emulador baseado em Qt4</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="120"/>
        <source>Version </source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="122"/>
        <source>SVN r%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="124"/>
        <source>built for</source>
        <translation>compilado para</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="136"/>
        <source>Copyright</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="136"/>
        <source>Germany</source>
        <translation>Alemanha</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="140"/>
        <source>Project homepage:</source>
        <translation>Página do projeto:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="141"/>
        <source>Development site:</source>
        <translation>Página de desenvolvimento:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="142"/>
        <source>QMC2 development mailing list:</source>
        <translation>Lista de desenvolvimento do QMC2:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="142"/>
        <source>subscription required</source>
        <translation>cadastro requerido</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="143"/>
        <source>List subscription:</source>
        <translation>Cadastro na lista:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="144"/>
        <source>Bug tracking system:</source>
        <translation>Sistema de gerenciamento de bugs:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="149"/>
        <source>Build OS:</source>
        <translation>SO de Compilação:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="152"/>
        <location filename="../../about.cpp" line="154"/>
        <source>Running OS:</source>
        <translation>SO corrente:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="156"/>
        <source>Emulator version:</source>
        <translation>Versão do emulador:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Template information:</source>
        <translation>Informação do template:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Emulator:</source>
        <translation>Emulador:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Version:</source>
        <translation>Versão:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="157"/>
        <source>Format:</source>
        <translation>Formato:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <source>Qt version:</source>
        <translation>Versão do Qt:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <location filename="../../about.cpp" line="160"/>
        <source>Compile-time:</source>
        <translation>Compilação:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <location filename="../../about.cpp" line="160"/>
        <location filename="../../about.cpp" line="163"/>
        <source>Run-time:</source>
        <translation>Execução:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="158"/>
        <source>Build key:</source>
        <translation>Chave de compilação:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="163"/>
        <source>Phonon version:</source>
        <translation>Versão do Phonon:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="160"/>
        <source>SDL version:</source>
        <translation>Versão do SDL:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="163"/>
        <source>Phonon backend / supported MIME types:</source>
        <translation>Backend Phonon / tipos MIME suportados:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Physical memory:</source>
        <translation>Memória:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Total: %1 MB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Free: %1 MB</source>
        <translation>Livre:%1 MB</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="172"/>
        <source>Used: %1 MB</source>
        <translation>Usada: %1 MB</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="174"/>
        <source>Number of CPUs:</source>
        <translation>Número de CPUs:</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="174"/>
        <source>unknown</source>
        <translation>desconhecido</translation>
    </message>
    <message>
        <location filename="../../about.cpp" line="175"/>
        <source>Environment variables:</source>
        <translation>Variáveis de ambiente:</translation>
    </message>
    <message>
        <location filename="../../about.ui" line="15"/>
        <source>About QMC2</source>
        <translation>Sobre o QMC2</translation>
    </message>
    <message>
        <location filename="../../about.ui" line="515"/>
        <source>Project details</source>
        <translation>Detalhes do projeto</translation>
    </message>
    <message utf8="true">
        <location filename="../../about.ui" line="527"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;QMC2 - M.A.M.E. Catalog / Launcher II&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Qt 4 based UNIX multi-emulator frontend&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Version X.Y[.bZ], built for SDLMAME&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright © 2006 - 2008 R. Reucher, Germany&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;QMC2 - M.A.M.E. Catalog / Launcher II&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Frontend multi-emulador escrito em Qt 4&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Versão X.Y[.bZ], compilado para SDLMAME&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright © 2006 - 2008 R. Reucher, Alemanha&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../about.ui" line="549"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Project homepage:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;http://www.mameworld.net/mamecat&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Development site:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;http://sourceforge.net/projects/qmc2&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;QMC2 development mailing list:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;qmc2-devel@lists.sourceforge.net (subscribers only)&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;List subscription:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;https://lists.sourceforge.net/lists/listinfo/qmc2-devel&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Página do projeto:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;http://www.mameworld.net/mamecat&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Página de desenvolvimento:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;http://sourceforge.net/projects/qmc2&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Lista de desenvolvimento do QMC2:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;qmc2-devel@lists.sourceforge.net (subscribers only)&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Cadastro à lista:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;https://lists.sourceforge.net/lists/listinfo/qmc2-devel&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../about.ui" line="579"/>
        <source>System information</source>
        <translation>Informação do sistema</translation>
    </message>
</context>
<context>
    <name>ArcadeScene</name>
    <message>
        <location filename="../../arcade/arcadescene.cpp" line="36"/>
        <source>FPS: --</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescene.cpp" line="258"/>
        <location filename="../../arcade/arcadescene.cpp" line="260"/>
        <source>FPS: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescene.cpp" line="303"/>
        <location filename="../../arcade/arcadescene.cpp" line="306"/>
        <source>Paused</source>
        <translation>Pausado</translation>
    </message>
</context>
<context>
    <name>ArcadeScreenshotSaverThread</name>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="48"/>
        <source>ArcadeScreenshotSaverThread: Started</source>
        <translation>ThreadGravadoraDeScreenshotsDoArcade: Iniciada</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="66"/>
        <source>ArcadeScreenshotSaverThread: Saving screen shot</source>
        <translation>ThreadGravadoraDeScreenshotsDoArcade: Salvando screenshot</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="71"/>
        <source>ArcadeScreenshotSaverThread: Failed to create screen shot directory &apos;%1&apos; - aborting screen shot creation</source>
        <translation>ThreadGravadoraDeScreenshotsDoArcade: Falha ao criar o diretório de screenshots &apos;%1&apos; - abortando criação de screenshot</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="80"/>
        <source>ArcadeScreenshotSaverThread: Screen shot successfully saved as &apos;%1&apos;</source>
        <translation>ThreadGravadoraDeScreenshotsDoArcade: Screenshot salvo com sucesso como &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="82"/>
        <source>ArcadeScreenshotSaverThread: Failed to save screen shot as &apos;%1&apos;</source>
        <translation>ThreadGravadoraDeScreenshotsDoArcade: Falha salvando screenshot como &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="83"/>
        <source>Saving screen shot</source>
        <translation>Salvando screenshot</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadescreenshotsaverthread.cpp" line="92"/>
        <source>ArcadeScreenshotSaverThread: Ended</source>
        <translation>ThreadGravadoraDeScreenshotsDoArcade: Finalizada</translation>
    </message>
</context>
<context>
    <name>ArcadeSetupDialog</name>
    <message>
        <location filename="../../arcade/arcadesetupdialog.cpp" line="25"/>
        <source>Machine list</source>
        <translation>Lista de máquinas</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.cpp" line="26"/>
        <source>Control display of machine list</source>
        <translation>Controla a tela da lista de máquinas</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="15"/>
        <source>Arcade setup</source>
        <translation>Configuração do Arcade</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="25"/>
        <source>Graphics mode settings</source>
        <translation>Configurações gráficas</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="58"/>
        <source>General</source>
        <translation>Geral</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="72"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="171"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="786"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1007"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1228"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1449"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1670"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1891"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2112"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2333"/>
        <source>X:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="79"/>
        <source>X coordinate of scene window position</source>
        <translation>Coordenada X da posição da janela da cena</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="95"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="194"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="812"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1033"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1254"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1475"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1696"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1917"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2138"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2359"/>
        <source>Y:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="102"/>
        <source>Y coordinate of scene window position</source>
        <translation>Coordenada Y da posição da janela da cena</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="118"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="698"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="838"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1059"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1280"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1501"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1722"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1943"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2164"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2385"/>
        <source>W:</source>
        <translation>L:</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="125"/>
        <source>Width of scene window</source>
        <translation>Largura da janela de cena</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="141"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="731"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="864"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1085"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1306"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1527"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1748"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1969"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2190"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2411"/>
        <source>H:</source>
        <translation>A:</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="148"/>
        <source>Height of scene window</source>
        <translation>Altura da janela</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="158"/>
        <source>Aspect ratio</source>
        <translation>Proporção</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="178"/>
        <source>X portion of scene aspect ratio (should be equal to screen&apos;s aspect ratio)</source>
        <translation>Porção horizontal da razão da cena (deve ser igual à razão da tela)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="201"/>
        <source>Y portion of scene aspect ratio (should be equal to screen&apos;s aspect ratio)</source>
        <translation>Porção vertical da razão da cena (deve ser igual à razão da tela)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="216"/>
        <source>Display arcade scene in full screen mode or windowed</source>
        <translation>Mostrar cena do arcade em tela cheia ou janela</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="219"/>
        <source>Full screen</source>
        <translation>Tela cheia</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="229"/>
        <source>Use window resolution in full screen mode (for slow systems)</source>
        <translation>Usar resolução da janela em tela cheia (para sistemas mais lentos)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="232"/>
        <source>Use window resolution</source>
        <translation>Usar resolução da janela</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="239"/>
        <source>Show frames per second counter in the lower left corner</source>
        <translation>Mostar o contador de quadros por segundo no canto inferior esquerdo</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="242"/>
        <source>Show FPS</source>
        <translation>Mostrar FPS</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="253"/>
        <source>Enable anti aliasing on primitive drawing</source>
        <translation>Habilitar anti aliasing em desenhos primitivos</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="256"/>
        <source>Primitive AA</source>
        <translation>AA Primitivo</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="263"/>
        <source>Scale items smoothly</source>
        <translation>Esticar itens suavemente</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="266"/>
        <source>Smooth item scaling</source>
        <translation>Esticar itens suavemente</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="273"/>
        <source>Keep aspect ratio when resizing scene window</source>
        <translation>Mostrar proporção da tela quando redimensiona a janela da cena</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="276"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="590"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="634"/>
        <source>Keep aspect ratio</source>
        <translation>Manter proporção</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="285"/>
        <source>Center arcade window on screen</source>
        <translation>Centrar janela do arcade na tela</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="288"/>
        <source>Center window</source>
        <translation>Centrar janela</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="295"/>
        <source>Snapshot directory</source>
        <translation>Diretório de snapshots</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="304"/>
        <source>Directory to store snapshots (write)</source>
        <translation>Diretório para guardar os snapshots (escrever)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="311"/>
        <source>Browse snapshot directory</source>
        <translation>Procurar pelo diretório de snapshots</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="329"/>
        <source>OpenGL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="343"/>
        <source>Enable direct rendering</source>
        <translation>Habilitar renderização direta (direct rendering)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="346"/>
        <source>Direct rendering</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="353"/>
        <source>Enable enhanced OpenGL anti aliasing</source>
        <translation>Habilitar anti aliasing avançado para o OpenGL</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="356"/>
        <source>Anti aliasing</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="363"/>
        <source>Synchronize buffer swaps with screen</source>
        <translation>Sincroniza trocas de buffer com a tela</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="366"/>
        <source>Sync to screen</source>
        <translation>Sincronização com a tela</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="373"/>
        <source>Enable double buffering (avoids flicker)</source>
        <translation>Habilitar buffer duplo (evitar flicker)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="376"/>
        <source>Double buffering</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="383"/>
        <source>Enable depth buffering (Z-buffer)</source>
        <translation>Habilitar buffer de profundidade (Buffer Eixo Z)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="386"/>
        <source>Depth buffering</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="393"/>
        <source>Use RGBA color mode</source>
        <translation>Usar modo de cor RGBA</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="396"/>
        <source>RGBA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="403"/>
        <source>Use alpha channel information for transparency</source>
        <translation>Usar informações do canal alfa para transparência</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="406"/>
        <source>Alpha channel</source>
        <translation>Canal Alfa</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="413"/>
        <source>Enable multi sampling</source>
        <translation>Habilitar multipla amostragem</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="416"/>
        <source>Multi sampling</source>
        <translation>Multipla amostragem</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="423"/>
        <source>Enable OpenGL overlays</source>
        <translation>Habilitar OpenGL overlays</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="426"/>
        <source>Overlays</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="433"/>
        <source>Enable accumulator buffer</source>
        <translation>Habilitar buffer acumulador</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="436"/>
        <source>Accumulator buffer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="443"/>
        <source>Enable stencil buffer</source>
        <translation>Habilitar stencil buffer</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="446"/>
        <source>Stencil buffer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="453"/>
        <source>Enable stereo buffer</source>
        <translation>Habilitar buffer stereo</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="456"/>
        <source>Stereo buffer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="486"/>
        <source>Scene layout</source>
        <translation>Layout da cena</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="492"/>
        <source>Layout name</source>
        <translation>Nome do layout</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="505"/>
        <source>Select the layout you want to edit / use</source>
        <translation>Selecionar o layout que você quer editar/usar</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="518"/>
        <source>Items, placements and parameters</source>
        <translation>Itens, posições e parâmetros</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="553"/>
        <source>Use a background image</source>
        <translation>Usar imagem de fundo</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="556"/>
        <source>Background image</source>
        <translation>Imagem de fundo</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="563"/>
        <source>Background image file (read)</source>
        <translation>Arquivo da imagem de fundo (leitura)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="576"/>
        <source>Browse background image file</source>
        <translation>Procurar pelo arquivo da imagem de fundo</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="587"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="631"/>
        <source>Keep image&apos;s aspect ratio when scaling</source>
        <translation>Manter proporções da imagem quando esticar</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="597"/>
        <source>Use a foreground image</source>
        <translation>Usar imagem de frente</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="600"/>
        <source>Foreground image</source>
        <translation>Imagem de frente</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="607"/>
        <source>Foreground image file (read)</source>
        <translation>Arquivo da imagem de frente (leitura)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="620"/>
        <source>Browse foreground image file</source>
        <translation>Procurar pelo arquivo da imagem de frente</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="641"/>
        <source>Arcade font</source>
        <translation>Fonte do arcade</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="648"/>
        <source>Arcade font (= system default if empty)</source>
        <translation>Fonte do arcade (= padrão do sistema se vazio)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="658"/>
        <source>Browse arcade font</source>
        <translation>Procurar por fonte do arcade</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="669"/>
        <source>Select font color</source>
        <translation>Selecionar cor da fonte</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="672"/>
        <source>Font color</source>
        <translation>Cor da fonte</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="683"/>
        <source>Virtual resolution</source>
        <translation>Resolução virtual</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="711"/>
        <source>Virtual width of scene</source>
        <translation>Largura virtual</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="744"/>
        <source>Virtual height of scene</source>
        <translation>Altura virtual da cena</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="756"/>
        <source>Control display of game list</source>
        <translation>Controla a tela da lista de jogos</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="759"/>
        <source>Game list</source>
        <translation>Lista de jogos</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="771"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="992"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1213"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1434"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1655"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1876"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2097"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2318"/>
        <source>Geometry</source>
        <translation>Geometria</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="796"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1017"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1238"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1459"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1680"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1901"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2122"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2343"/>
        <source>X coordinate of item position</source>
        <translation>Coordenada X da posição do item</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="822"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1043"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1264"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1485"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1706"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1927"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2148"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2369"/>
        <source>Y coordinate of item position</source>
        <translation>Coordenada Y da posição do item</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="848"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1069"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1290"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1511"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1732"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1953"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2174"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2395"/>
        <source>Item width</source>
        <translation>Largura do item</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="874"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1095"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1316"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1537"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1758"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1979"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2200"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2421"/>
        <source>Item height</source>
        <translation>Altura do item</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="886"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1107"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1328"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1549"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1770"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1991"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2212"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2433"/>
        <source>Use background for this item</source>
        <translation>Usar fundo para esse item</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="889"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1110"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1331"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1552"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1773"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1994"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2215"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2436"/>
        <source>Background</source>
        <translation>Fundo</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="904"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1125"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1346"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1567"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1788"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2009"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2230"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2451"/>
        <source>Select background color</source>
        <translation>Selecionar a cor de fundo</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="918"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1139"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1360"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1581"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1802"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2023"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2244"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2465"/>
        <source>T:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="925"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1146"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1367"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1588"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1809"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2030"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2251"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2472"/>
        <source>Select background transparency</source>
        <translation>Selecionar a transparência do fundo</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="928"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1149"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1370"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1591"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1812"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2033"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2254"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2475"/>
        <source>%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="938"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1159"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1380"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1601"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1822"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2043"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2264"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2485"/>
        <source>Use texture bitmap for background</source>
        <translation>Usar bitmap de textura para o fundo</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="941"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1162"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1383"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1604"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1825"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2046"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2267"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2488"/>
        <source>Texture</source>
        <translation>Textura</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="948"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1169"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1390"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1611"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1832"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2053"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2274"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2495"/>
        <source>Texture bitmap for background</source>
        <translation>Bitmap de textura para o fundo</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="961"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1182"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1403"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1624"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1845"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2066"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2287"/>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2508"/>
        <source>Browse texture bitmap for background</source>
        <translation>Procurar o bitmap de textura para o fundo</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="977"/>
        <source>Control display of preview image</source>
        <translation>Controla a tela de imagens de preview</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="980"/>
        <source>Preview image</source>
        <translation>Imagem de preview</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1198"/>
        <source>Control display of flyer image</source>
        <translation>Controla a tela de imagem do flyer</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1201"/>
        <source>Flyer image</source>
        <translation>Imagem de flyer</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1419"/>
        <source>Control display of cabinet image</source>
        <translation>Controla a tela de imagem do gabinete</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1422"/>
        <source>Cabinet image</source>
        <translation>Imagem do gabinete</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1640"/>
        <source>Control display of controller image</source>
        <translation>Controla a tela de imagem do controle</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1643"/>
        <source>Controller image</source>
        <translation>Imagem do controle</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1861"/>
        <source>Control display of marquee image</source>
        <translation>Controla a tela da imagem de marquee</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="1864"/>
        <source>Marquee image</source>
        <translation>Imagem de marquee</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2082"/>
        <source>Control display of title image</source>
        <translation>Controla a tela de imagem de título</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2085"/>
        <source>Title image</source>
        <translation>Imagem de título</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2303"/>
        <source>Control display of MAWS lookup</source>
        <translation>Controla a tela de visualização de MAWS</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2306"/>
        <source>MAWS lookup</source>
        <translation>Visualização de MAWS</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2530"/>
        <source>Scene rotation</source>
        <translation>Rotação da cena</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2537"/>
        <source>Scene rotation angle in degrees</source>
        <translation>Ângulo da rotação da cena em graus</translation>
    </message>
    <message utf8="true">
        <location filename="../../arcade/arcadesetupdialog.ui" line="2540"/>
        <source>°</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2584"/>
        <source>Apply settings</source>
        <translation>Aplicar configurações</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2587"/>
        <source>&amp;Apply</source>
        <translation>&amp;Aplicar</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2601"/>
        <source>Restore currently applied settings</source>
        <translation>Restaurar configurações corrente</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2604"/>
        <source>&amp;Restore</source>
        <translation>&amp;Restaurar</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2618"/>
        <source>Reset to default settings (click &lt;i&gt;Restore&lt;/i&gt; to restore currently applied settings!)</source>
        <translation>Reiniciar para as configurações padrões (clique em &lt;i&gt;Restaurar&lt;/i&gt; para restaurar as configurações alteradas!)</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2621"/>
        <source>&amp;Default</source>
        <translation>&amp;Padrão</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2645"/>
        <source>Close and apply settings</source>
        <translation>Fechar e aplicar configurações</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2648"/>
        <source>&amp;Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2655"/>
        <source>Close and discard changes</source>
        <translation>Fechar e descartar alterações</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadesetupdialog.ui" line="2658"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
</context>
<context>
    <name>ArcadeView</name>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="53"/>
        <source>QMC2 - ArcadeView</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="88"/>
        <source>ArcadeView: Cleaning up</source>
        <translation>ArcadeView: Limpando</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="120"/>
        <source>ArcadeView: Switching to windowed mode</source>
        <translation>ArcadeView: Mudando para modo janela</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="127"/>
        <source>ArcadeView: Switching to full screen mode</source>
        <translation>ArcadeView: Mudando para tela cheia</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="130"/>
        <source>ArcadeView: Resolution switching is not yet supported</source>
        <translation>ArcadeView: Troca de resolução não suportada ainda</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="146"/>
        <source>ArcadeView: Setting window size to %1x%2</source>
        <translation>ArcadeView: Alterando tamanho da tela para %1x%2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="156"/>
        <source>ArcadeView: Setting window position to %1, %2</source>
        <translation>ArcadeView: Alterando posição da janela para %1, %2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="170"/>
        <source>ArcadeView: This system does not appear to support OpenGL -- reverting to non-OpenGL / software renderer</source>
        <translation>ArcadeView: Esse sistema aparentemente não suporta OpenGl -- revertendo para não OpenGL / renderização por software</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="175"/>
        <source>ArcadeView: Using OpenGL renderer</source>
        <translation>ArcadeView: Usando renderizador OpenGL</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="179"/>
        <source>ArcadeView: This system does not appear to support vertical syncing -- disabling SyncToScreen</source>
        <translation>ArcadeView: Esse sistema aparentemente não suporta sincronização vertical -- desabilitando Sincronização com a Tela</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="182"/>
        <source>ArcadeView: OpenGL: SyncToScreen: %1</source>
        <translation>ArcadeView: OpenGL: Sincronização com a Tela: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="182"/>
        <location filename="../../arcade/arcadeview.cpp" line="184"/>
        <location filename="../../arcade/arcadeview.cpp" line="186"/>
        <location filename="../../arcade/arcadeview.cpp" line="188"/>
        <location filename="../../arcade/arcadeview.cpp" line="190"/>
        <location filename="../../arcade/arcadeview.cpp" line="192"/>
        <location filename="../../arcade/arcadeview.cpp" line="194"/>
        <location filename="../../arcade/arcadeview.cpp" line="196"/>
        <location filename="../../arcade/arcadeview.cpp" line="198"/>
        <location filename="../../arcade/arcadeview.cpp" line="204"/>
        <location filename="../../arcade/arcadeview.cpp" line="210"/>
        <location filename="../../arcade/arcadeview.cpp" line="212"/>
        <source>on</source>
        <translation>ligado</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="182"/>
        <location filename="../../arcade/arcadeview.cpp" line="184"/>
        <location filename="../../arcade/arcadeview.cpp" line="186"/>
        <location filename="../../arcade/arcadeview.cpp" line="188"/>
        <location filename="../../arcade/arcadeview.cpp" line="190"/>
        <location filename="../../arcade/arcadeview.cpp" line="192"/>
        <location filename="../../arcade/arcadeview.cpp" line="194"/>
        <location filename="../../arcade/arcadeview.cpp" line="196"/>
        <location filename="../../arcade/arcadeview.cpp" line="198"/>
        <location filename="../../arcade/arcadeview.cpp" line="204"/>
        <location filename="../../arcade/arcadeview.cpp" line="210"/>
        <location filename="../../arcade/arcadeview.cpp" line="212"/>
        <source>off</source>
        <translation>desligado</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="184"/>
        <source>ArcadeView: OpenGL: DoubleBuffer: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="186"/>
        <source>ArcadeView: OpenGL: DepthBuffer: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="188"/>
        <source>ArcadeView: OpenGL: RGBA: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="190"/>
        <source>ArcadeView: OpenGL: AlphaChannel: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="192"/>
        <source>ArcadeView: OpenGL: AccumulatorBuffer: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="194"/>
        <source>ArcadeView: OpenGL: StencilBuffer: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="196"/>
        <source>ArcadeView: OpenGL: Stereo: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="198"/>
        <source>ArcadeView: OpenGL: DirectRendering: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="201"/>
        <source>ArcadeView: This system does not appear to support OpenGL overlays -- disabling OpenGL overlays</source>
        <translation>ArcadeView: Esse sistema aparentemente não suporta OpenGL overlays -- desabilitando OpenGL overlays</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="204"/>
        <source>ArcadeView: OpenGL: Overlay: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="207"/>
        <source>ArcadeView: This system does not appear to support OpenGL multi sampling -- disabling MultiSample</source>
        <translation>ArcadeView: OpenGL: Esse sistema aparentemente não suporta multi amostragem do OpenGL -- desabilitando multi amostragem</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="210"/>
        <source>ArcadeView: OpenGL: MultiSample: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="212"/>
        <source>ArcadeView: OpenGL: AntiAliasing: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="222"/>
        <source>ArcadeView: Using software renderer</source>
        <translation>ArcadeView: Usando renderização por software</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="227"/>
        <source>ArcadeView: X11: Screen number: %1</source>
        <translation>ArcadeView: X11: Número da tela: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="228"/>
        <source>ArcadeView: X11: Color depth: %1</source>
        <translation>ArcadeView: X11: Profundidade de cores: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="229"/>
        <source>ArcadeView: X11: DPI-X: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="230"/>
        <source>ArcadeView: X11: DPI-Y: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="231"/>
        <source>ArcadeView: X11: Compositing manager: %1</source>
        <translation>ArcadeView: X11: Gerenciador de composição: %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="231"/>
        <source>running</source>
        <translation>executando</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="231"/>
        <source>not running</source>
        <translation>não executando</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="235"/>
        <source>ArcadeView: Screen geometry: %1x%2</source>
        <translation>ArcadeView: Geometria da tela: %1x%2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="236"/>
        <source>ArcadeView: Virtual resolution: %1x%2</source>
        <translation>ArcadeView: Resolução virtual: %1x%2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="237"/>
        <source>ArcadeView: Selected aspect ratio: %1:%2</source>
        <translation>ArcadeView: Proporção selecionada: %1:%2</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="238"/>
        <source>ArcadeView: Scene rotation angle: %1 degrees</source>
        <translation>ArcadeView: Ângulo de rotação da cena: %1 graus</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="241"/>
        <source>ArcadeView: Virtual resolution doesn&apos;t fit aspect ratio -- scene coordinates may be stretched or compressed</source>
        <translation>ArcadeView: Resolução virtual não é compatível com a proporção -- coordenadas da cena podem estar esticadas ou comprimidas</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="242"/>
        <source>ArcadeView: Aspect ratio will be maintained</source>
        <translation>ArcadeView: Proporção será mantida</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="242"/>
        <source>ArcadeView: Aspect ratio will not be maintained</source>
        <translation>ArcadeView: Proporção não será mantida</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="243"/>
        <source>ArcadeView: FPS counter display %1</source>
        <translation>ArcadeView: Contador de FPS %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="243"/>
        <location filename="../../arcade/arcadeview.cpp" line="246"/>
        <source>activated</source>
        <translation>ativado</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="243"/>
        <location filename="../../arcade/arcadeview.cpp" line="246"/>
        <source>deactivated</source>
        <translation>desativado</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="246"/>
        <source>ArcadeView: Primitive antialiasing %1</source>
        <translation>ArcadeView: Antialiasing primitivo %1</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="250"/>
        <source>ArcadeView: Centering window on screen</source>
        <translation>ArcadeView: Centrando janela na tela</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="254"/>
        <source>ArcadeView: Restoring saved window position</source>
        <translation>ArcadeView: Restaurando posição salva da janela</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="281"/>
        <source>ArcadeView: Rendering screen shot</source>
        <translation>ArcadeView: Renderizando screenshot</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="290"/>
        <source>Saving screen shot</source>
        <translation>Salvando screenshot</translation>
    </message>
    <message>
        <location filename="../../arcade/arcadeview.cpp" line="300"/>
        <source>ArcadeView: Adjusting window size to %1x%2 to maintain the aspect ratio</source>
        <translation>ArcadeView: Ajustando tamanho da tela para %1x%2 para manter proporção</translation>
    </message>
</context>
<context>
    <name>AudioEffectDialog</name>
    <message>
        <location filename="../../audioeffects.cpp" line="35"/>
        <location filename="../../audioeffects.cpp" line="194"/>
        <location filename="../../audioeffects.cpp" line="199"/>
        <source>Enable effect &apos;%1&apos;</source>
        <translation>Habilitar efeito &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="52"/>
        <source>Setup effect &apos;%1&apos;</source>
        <translation>Configurar efeito &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="186"/>
        <location filename="../../audioeffects.cpp" line="207"/>
        <source>Disable effect &apos;%1&apos;</source>
        <translation>Desabilitar efeito &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="195"/>
        <source>WARNING: audio player: can&apos;t insert effect &apos;%1&apos;</source>
        <translation>AVISO: reprodutor de áudio: impossível inserir efeito &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../audioeffects.cpp" line="208"/>
        <source>WARNING: audio player: can&apos;t remove effect &apos;%1&apos;</source>
        <translation>AVISO: reprodutor de áudio: impossível remover efeito &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="14"/>
        <source>Audio effects</source>
        <translation>Efeitos de áudio</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="36"/>
        <source>Close audio effects dialog</source>
        <translation>Fechar janela de efeitos de áudio</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="39"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="59"/>
        <location filename="../../audioeffects.ui" line="62"/>
        <source>List of available audio effects</source>
        <translation>Lista de efeitos de áudio disponíveis</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="81"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="86"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="91"/>
        <source>Enable</source>
        <translation>Habilitar</translation>
    </message>
    <message>
        <location filename="../../audioeffects.ui" line="96"/>
        <source>Setup</source>
        <translation>Configurar</translation>
    </message>
</context>
<context>
    <name>Cabinet</name>
    <message>
        <location filename="../../cabinet.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>Copiar para área de transferência</translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="56"/>
        <location filename="../../cabinet.cpp" line="57"/>
        <source>Game cabinet image</source>
        <translation>Imagem de gabinete do jogo</translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="59"/>
        <location filename="../../cabinet.cpp" line="60"/>
        <source>Machine cabinet image</source>
        <translation>Imagem de gabinete da máquina</translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="68"/>
        <location filename="../../cabinet.cpp" line="72"/>
        <source>FATAL: can&apos;t open cabinet file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo de gabinete. por favor verifique as permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../cabinet.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Aguardando dados...</translation>
    </message>
</context>
<context>
    <name>Controller</name>
    <message>
        <location filename="../../controller.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>Copiar para área de transferência</translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="56"/>
        <location filename="../../controller.cpp" line="57"/>
        <source>Game controller image</source>
        <translation>Imagem do controle do jogo</translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="59"/>
        <location filename="../../controller.cpp" line="60"/>
        <source>Machine controller image</source>
        <translation>Imagem do controle da máquina</translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="68"/>
        <location filename="../../controller.cpp" line="72"/>
        <source>FATAL: can&apos;t open controller file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo de controle. por favor verifique as permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../controller.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Aguardando dados...</translation>
    </message>
</context>
<context>
    <name>DemoModeDialog</name>
    <message>
        <location filename="../../demomode.cpp" line="105"/>
        <source>demo mode stopped</source>
        <translation>modo demonstração parado</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="263"/>
        <location filename="../../demomode.cpp" line="107"/>
        <source>Run &amp;demo</source>
        <translation>Iniciar &amp;demonstração</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="260"/>
        <location filename="../../demomode.cpp" line="108"/>
        <source>Run demo now</source>
        <translation>Iniciar demonstração agora</translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="124"/>
        <source>please wait for reload to finish and try again</source>
        <translation>por favor espere finalizar o recarregamento e tente novamente</translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="128"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation>por favor espere a verificação de ROM terminar e tente novamente</translation>
    </message>
    <message numerus="yes">
        <location filename="../../demomode.cpp" line="170"/>
        <source>demo mode started -- %n game(s) selected by filter</source>
        <translation>
            <numerusform>modo demonstração iniciado -- %n jogo selecionado pelo filtro</numerusform>
            <numerusform>modo demonstração iniciado -- %n jogos selecionados pelo filtro</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="172"/>
        <source>demo mode cannot start -- no games selected by filter</source>
        <translation>modo demonstração não pode iniciar -- nenhum jogo selecionado pelo filtro</translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="176"/>
        <source>Stop &amp;demo</source>
        <translation>Parar &amp;demonstração</translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="177"/>
        <source>Stop demo now</source>
        <translation>Parar demonstração agora</translation>
    </message>
    <message>
        <location filename="../../demomode.cpp" line="240"/>
        <source>starting emulation in demo mode for &apos;%1&apos;</source>
        <translation>iniciando emulação em modo demonstração para &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="14"/>
        <source>Demo mode</source>
        <translation>Modo demonstração</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="26"/>
        <source>ROM state filter</source>
        <translation>Filtro de estado da ROM</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="44"/>
        <source>Select ROM state C (correct)?</source>
        <translation>Selecionar estado de ROM C (correto)?</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="67"/>
        <source>Select ROM state M (mostly correct)?</source>
        <translation>Selecionar estado de ROM M (maioria correto)?</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="90"/>
        <source>Select ROM state I (incorrect)?</source>
        <translation>Selecionar estado de ROM I (incorreto)?</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="110"/>
        <source>Select ROM state N (not found)?</source>
        <translation>Selecionar estado de ROM (não encontrado)?</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="130"/>
        <source>Select ROM state U (unknown)?</source>
        <translation>Selecionar estado de ROM D (desconhecido)?</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="152"/>
        <source>Seconds to run</source>
        <translation>Segundos de execução</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="159"/>
        <source>Number of seconds to run an emulator in demo mode</source>
        <translation>Número de segundos para executar um emulador em modo demonstração</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="213"/>
        <source>Use only tagged games</source>
        <translation>Usar somente jogos etiquetados</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="216"/>
        <source>Tagged</source>
        <translation>Etiquetados</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="289"/>
        <source>Pause (seconds)</source>
        <translation>Pausar (segundos)</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="296"/>
        <source>Number of seconds to pause between emulator runs</source>
        <translation>Número de segundos de pausa entre execuções do emulador</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="174"/>
        <source>Start emulators in full screen mode (otherwise use windowed mode)</source>
        <translation>Iniciar emuladores em modo tela cheia (senão use modo janela)</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="177"/>
        <source>Full screen</source>
        <translation>Tela cheia</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="190"/>
        <source>Maximize emulators when in windowed mode</source>
        <translation>Maximizar emuladores quando em modo janela</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="193"/>
        <source>Maximized</source>
        <translation>Maximizado</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="203"/>
        <source>Embed windowed emulators</source>
        <translation>Embutir emuladores em modo janela</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="206"/>
        <source>Embedded</source>
        <translation>Embutido</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="231"/>
        <source>Close this dialog (and stop running demo)</source>
        <translation>Fechar essa janela (e parar demonstração em execução)</translation>
    </message>
    <message>
        <location filename="../../demomode.ui" line="234"/>
        <source>&amp;Close</source>
        <translation>&amp;Fechar</translation>
    </message>
</context>
<context>
    <name>DetailSetup</name>
    <message>
        <location filename="../../detailsetup.cpp" line="21"/>
        <location filename="../../detailsetup.cpp" line="98"/>
        <source>Pre&amp;view</source>
        <translation>Pre&amp;visualização</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="22"/>
        <source>Game preview image</source>
        <translation>Imagem de preview do jogo</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="24"/>
        <location filename="../../detailsetup.cpp" line="101"/>
        <source>Fl&amp;yer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="25"/>
        <source>Game flyer image</source>
        <translation>Imagem do flyer do jogo</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="27"/>
        <source>Game &amp;info</source>
        <translation>&amp;Informação do jogo</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="28"/>
        <source>Game information</source>
        <translation>Informação do jogo</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="30"/>
        <location filename="../../detailsetup.cpp" line="107"/>
        <source>Em&amp;ulator info</source>
        <translation>Informação do Em&amp;ulador</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="31"/>
        <location filename="../../detailsetup.cpp" line="108"/>
        <source>Emulator information</source>
        <translation>Informação do emulador</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="33"/>
        <location filename="../../detailsetup.cpp" line="110"/>
        <source>&amp;Configuration</source>
        <translation>&amp;Configuração</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="34"/>
        <location filename="../../detailsetup.cpp" line="111"/>
        <source>Emulator configuration</source>
        <translation>Configuração do emulador</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="36"/>
        <location filename="../../detailsetup.cpp" line="119"/>
        <source>Ca&amp;binet</source>
        <translation>Ga&amp;binete</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="37"/>
        <source>Arcade cabinet image</source>
        <translation>Imagem de gabinete do arcade</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="39"/>
        <source>C&amp;ontroller</source>
        <translation>C&amp;ontrole</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="40"/>
        <source>Control panel image</source>
        <translation>Imagem do painel de controle</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="42"/>
        <source>Mar&amp;quee</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="43"/>
        <source>Marquee image</source>
        <translation>Imagem de Marquee</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="45"/>
        <source>Titl&amp;e</source>
        <translation>Títul&amp;o</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="46"/>
        <source>Title screen image</source>
        <translation>Imagem da tela de título</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="48"/>
        <source>MA&amp;WS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="49"/>
        <source>MAWS page (web lookup)</source>
        <translation>Página do MAWS (acesso web)</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="51"/>
        <location filename="../../detailsetup.cpp" line="116"/>
        <source>&amp;PCB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="52"/>
        <location filename="../../detailsetup.cpp" line="117"/>
        <source>PCB image</source>
        <translation>Imagem de PCB</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="58"/>
        <location filename="../../detailsetup.cpp" line="126"/>
        <source>&amp;YouTube</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="59"/>
        <location filename="../../detailsetup.cpp" line="127"/>
        <source>YouTube videos</source>
        <translation>Vídeos do YouTube</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="99"/>
        <source>Machine preview image</source>
        <translation>Imagem de preview da máquina</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="102"/>
        <source>Machine flyer image</source>
        <translation>Imagem do flyer da máquina</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="104"/>
        <source>Machine &amp;info</source>
        <translation>&amp;Informação da máquina</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="105"/>
        <source>Machine information</source>
        <translation>Informação da máquina</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="113"/>
        <source>De&amp;vices</source>
        <translation>Dispositi&amp;vos</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="114"/>
        <source>Device configuration</source>
        <translation>Configurações de dispositivo</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="54"/>
        <location filename="../../detailsetup.cpp" line="122"/>
        <source>Softwar&amp;e list</source>
        <translation>Lista de Softwar&amp;e</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="55"/>
        <location filename="../../detailsetup.cpp" line="123"/>
        <source>Software list</source>
        <translation>Lista de Software</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="120"/>
        <source>Machine cabinet image</source>
        <translation>Imagem de gabinete da máquina</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="367"/>
        <source>MAWS configuration (1/2)</source>
        <translation>Configuração do MAWS (1/2)</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="368"/>
        <source>MAWS URL pattern (use %1 as placeholder for game ID):</source>
        <translation>Padrão da URL do MAWS (use %1 como placeholder para o ID do jogo):</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="377"/>
        <location filename="../../detailsetup.cpp" line="382"/>
        <source>Yes</source>
        <translation>Sim</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="377"/>
        <source>No</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="379"/>
        <source>MAWS configuration (2/2)</source>
        <translation>Configuração do MAWS (2/2)</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="379"/>
        <source>Enable MAWS quick download?</source>
        <translation>Habilitar download rápido do MAWS?</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="402"/>
        <source>Choose the YouTube cache directory</source>
        <translation>Escolha o diretório de cache do YouTube</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="414"/>
        <source>FATAL: can&apos;t create new YouTube cache directory, path = %1</source>
        <translation>FATAL: impossível criar o novo diretório de cache do YouTube, caminho = %1</translation>
    </message>
    <message>
        <location filename="../../detailsetup.cpp" line="444"/>
        <source>INFO: the configuration tab can&apos;t be removed</source>
        <translation>INFO: a aba de configuração não pode ser removida</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="15"/>
        <source>Detail setup</source>
        <translation>Configurações de detalhes</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="30"/>
        <source>Available details</source>
        <translation>Detalhes disponíveis</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="36"/>
        <source>List of available details</source>
        <translation>Lista dos detalhes disponíveis</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="51"/>
        <source>Configure current detail</source>
        <translation>Configurar detalhe corrente</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="54"/>
        <source>Configure...</source>
        <translation>Configurar...</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="68"/>
        <source>Activate selected details</source>
        <translation>Ativar delahes selecionados</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="85"/>
        <source>Active details</source>
        <translation>Ativar detalhes</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="91"/>
        <source>List of active details and their order</source>
        <translation>Lista dos detalhes ativos e sua ordem</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="106"/>
        <source>Deactivate selected details</source>
        <translation>Desativar detalhes selecionados</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="120"/>
        <source>Move selected detail up</source>
        <translation>Mover detalhe selecionado para cima</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="137"/>
        <source>Move selected detail down</source>
        <translation>Mover detalhe selecionado para baixo</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="170"/>
        <source>Apply detail setup and close dialog</source>
        <translation>Aplicar configurações de detalhes e fechar janela</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="173"/>
        <source>&amp;Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="184"/>
        <source>Apply detail setup</source>
        <translation>Aplicar configurações de detalhes</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="187"/>
        <source>&amp;Apply</source>
        <translation>&amp;Aplicar</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="198"/>
        <source>Cancel detail setup and close dialog</source>
        <translation>Cancelar configurações de detalhes e fechar janela</translation>
    </message>
    <message>
        <location filename="../../detailsetup.ui" line="201"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
</context>
<context>
    <name>DirectoryEditWidget</name>
    <message>
        <location filename="../../direditwidget.cpp" line="44"/>
        <source>Choose directory</source>
        <translation>Escolha a pasta</translation>
    </message>
</context>
<context>
    <name>DocBrowser</name>
    <message>
        <location filename="../../docbrowser.ui" line="15"/>
        <location filename="../../docbrowser.cpp" line="86"/>
        <location filename="../../docbrowser.cpp" line="91"/>
        <location filename="../../docbrowser.cpp" line="93"/>
        <location filename="../../docbrowser.cpp" line="96"/>
        <source>MiniWebBrowser</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Embedder</name>
    <message>
        <location filename="../../embedder.cpp" line="92"/>
        <source>emulator #%1 released, window ID = 0x%2</source>
        <translation>emulador #%1 liberado, ID da janela = 0x%2</translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="103"/>
        <source>emulator #%1 embedded, window ID = 0x%2</source>
        <translation>emulador #%1 embutido, ID da janela = 0x%2</translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="128"/>
        <source>emulator #%1 closed, window ID = 0x%2</source>
        <translation>emulador #%1 fechado, ID da janela = 0x%2</translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="148"/>
        <source>WARNING: embedder: unknown error, window ID = 0x%1</source>
        <translation>AVISO: embedder: erro desconhecido, ID da janela = 0%1</translation>
    </message>
    <message>
        <location filename="../../embedder.cpp" line="143"/>
        <source>WARNING: embedder: invalid window ID = 0x%1</source>
        <translation>AVISO: embedder: ID da janela inválido = 0%1</translation>
    </message>
</context>
<context>
    <name>EmbedderOptions</name>
    <message>
        <location filename="../../embedderopt.ui" line="14"/>
        <source>Embedder options</source>
        <translation>Opções para embutir</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="31"/>
        <source>Snapshots</source>
        <translation>Capturas de tela</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="37"/>
        <source>Take a snapshot of the current window content -- hold to take snapshots repeatedly (every 100ms)</source>
        <translation>Capturar a tela do conteúdo atual da janela -- segure para tirar snapshots repetidamente (a cada 100ms)</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="40"/>
        <source>Take snapshot</source>
        <translation>Capturar tela</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="98"/>
        <source>Clear snapshots</source>
        <translation>Limpar snapshots</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="101"/>
        <source>Clear</source>
        <translation>Limpar</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="115"/>
        <source>Scale snapshots to the native resolution</source>
        <translation>Redimensionar capturas de tela para a resolução nativa</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="118"/>
        <source>Native resolution</source>
        <translation>Resolução nativa</translation>
    </message>
    <message>
        <location filename="../../embedderopt.ui" line="130"/>
        <source>Movies</source>
        <translation>Filmes</translation>
    </message>
</context>
<context>
    <name>EmulatorOptionDelegate</name>
    <message>
        <location filename="../../emuopt.cpp" line="159"/>
        <source>All files (*)</source>
        <translation>Todos os arquivos (*)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="164"/>
        <location filename="../../emuopt.cpp" line="176"/>
        <source>Browse: </source>
        <translation>Procurar:</translation>
    </message>
</context>
<context>
    <name>EmulatorOptions</name>
    <message>
        <location filename="../../emuopt.cpp" line="393"/>
        <location filename="../../emuopt.cpp" line="845"/>
        <location filename="../../emuopt.cpp" line="911"/>
        <location filename="../../emuopt.cpp" line="959"/>
        <location filename="../../emuopt.cpp" line="960"/>
        <location filename="../../emuopt.cpp" line="961"/>
        <location filename="../../emuopt.cpp" line="1053"/>
        <location filename="../../emuopt.cpp" line="1055"/>
        <location filename="../../emuopt.cpp" line="1057"/>
        <source>unknown</source>
        <translation>desconhecido</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="399"/>
        <source>Game specific emulator configuration</source>
        <translation>Configuração de emulador específico de jogo</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="401"/>
        <source>Machine specific emulator configuration</source>
        <translation>Configuração de emulador específico de máquina</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="405"/>
        <source>Global emulator configuration</source>
        <translation>Configuração global do emulador</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="413"/>
        <source>Option / Attribute</source>
        <translation>Opção / Atributo</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="414"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="858"/>
        <source>true</source>
        <translation>verdadeiro</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="860"/>
        <source>false</source>
        <translation>falso</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="806"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="809"/>
        <source>bool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="813"/>
        <source>int</source>
        <translation>inteiro</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="817"/>
        <source>float</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="821"/>
        <source>float2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="825"/>
        <source>float3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="829"/>
        <source>file</source>
        <translation>arquivo</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="833"/>
        <source>directory</source>
        <translation>diretório</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="837"/>
        <source>choice</source>
        <translation>escolha</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="841"/>
        <source>string</source>
        <translation>texto</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="850"/>
        <source>Short name</source>
        <translation>Nome curto</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="855"/>
        <source>Default</source>
        <translation>Padrão</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="870"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="954"/>
        <source>creating template configuration map</source>
        <translation>criando mapa de configuração de template</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="991"/>
        <source>FATAL: XML error reading template: &apos;%1&apos; in file &apos;%2&apos; at line %3, column %4</source>
        <translation>FATAL: erro de XML lendo template: %1 no arquivo %2 na linha %3 coluna %4</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1044"/>
        <source>template info: emulator = %1, version = %2, format = %3</source>
        <translation>informação de template: emulador = %1, versão = %2, formato = %3</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1051"/>
        <source>FATAL: can&apos;t open options template file</source>
        <translation>FATAL: impossível abrir arquivo de template de opções</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1054"/>
        <source>WARNING: couldn&apos;t determine emulator type of template</source>
        <translation>AVISO: impossível determinar o tipo de emulador do template</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1056"/>
        <source>WARNING: couldn&apos;t determine template version</source>
        <translation>AVISO: impossível determinar versão do template</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1058"/>
        <source>WARNING: couldn&apos;t determine template format</source>
        <translation>AVISO: impossível determinar o formato do template</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1068"/>
        <source>please wait for reload to finish and try again</source>
        <translation>por favor aguarde finalizar o recarregamento e tente novamente</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1077"/>
        <source>checking template configuration map against selected emulator</source>
        <translation>verificando template de mapa de configuração contra o emulador selecionado</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1109"/>
        <source>FATAL: can&apos;t start MAME executable within a reasonable time frame, giving up</source>
        <translation>FATAL: impossível iniciar o executável do MAME dentro de um tempo razoável, desistindo</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1111"/>
        <source>FATAL: can&apos;t start MESS executable within a reasonable time frame, giving up</source>
        <translation>FATAL: impossível iniciar o executável do MESS dentro de um tempo razoável, desistindo</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1167"/>
        <source>FATAL: can&apos;t create temporary file, please check emulator executable and permissions</source>
        <translation>FATAL: impossível criar aquivo temporário, por favor verifique o executável do emulador e suas permissões</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1184"/>
        <location filename="../../emuopt.cpp" line="1193"/>
        <location filename="../../emuopt.cpp" line="1202"/>
        <location filename="../../emuopt.cpp" line="1211"/>
        <location filename="../../emuopt.cpp" line="1223"/>
        <location filename="../../emuopt.cpp" line="1240"/>
        <source>emulator uses a different default value for option &apos;%1&apos; (&apos;%2&apos; vs. &apos;%3&apos;); assumed option type is &apos;%4&apos;</source>
        <translation>o emulador usa um valor padrão diferente para a opção &apos;%1&apos; (&apos;%2&apos; vs. &apos;%3&apos;); assumido que o tipo da opção é &apos;%4&apos;</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1247"/>
        <source>template option &apos;%1&apos; is unknown to the emulator</source>
        <translation>opção do template &apos;%1&apos; é desconhecida para o emulador</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1257"/>
        <source>emulator option &apos;%1&apos; with default value &apos;%2&apos; is unknown to the template</source>
        <translation>opção do emulador &apos;%1&apos; com valor padrão &apos;%2&apos; é desconhecido para o template</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1261"/>
        <source>done (checking template configuration map against selected emulator)</source>
        <translation>feito (verificando mapa de configuração do template contra o emulador selecionado)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../emuopt.cpp" line="1262"/>
        <source>check results: %n difference(s)</source>
        <translation>
            <numerusform>verificando resultados: %n diferença(s)</numerusform>
            <numerusform>verificando resultados: %n diferença(s)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1370"/>
        <source>WARNING: ini-export: no writable ini-paths found</source>
        <translation>AVISO: ini-export: nenhum caminho de ini com permissões de escrita encontrado</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1374"/>
        <location filename="../../emuopt.cpp" line="1534"/>
        <source>Path selection</source>
        <translation>Seleção de caminho</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1375"/>
        <source>Multiple ini-paths detected. Select path(s) to export to:</source>
        <translation>Multiplos caminhos de ini detectados. Selecione os caminho(s) para exportar para:</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1394"/>
        <source>WARNING: ini-export: no path selected (or invalid inipath)</source>
        <translation>AVISO: ini-export: nenhum caminho selecionado (ou caminho do ini inválido)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1406"/>
        <location filename="../../emuopt.cpp" line="1566"/>
        <source>Waiting for data...</source>
        <translation>Aguardando dados...</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1422"/>
        <source>FATAL: can&apos;t open export file for writing, path = %1</source>
        <translation>FATAL: impossível abrir arquivo de exportação para escrita, caminho = %1</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1428"/>
        <source>exporting %1 MAME configuration to %2</source>
        <translation>exportando configuração do MAME %1 para %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1428"/>
        <location filename="../../emuopt.cpp" line="1430"/>
        <location filename="../../emuopt.cpp" line="1481"/>
        <location filename="../../emuopt.cpp" line="1483"/>
        <location filename="../../emuopt.cpp" line="1589"/>
        <location filename="../../emuopt.cpp" line="1591"/>
        <location filename="../../emuopt.cpp" line="1690"/>
        <location filename="../../emuopt.cpp" line="1692"/>
        <source>global</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1428"/>
        <location filename="../../emuopt.cpp" line="1481"/>
        <location filename="../../emuopt.cpp" line="1589"/>
        <location filename="../../emuopt.cpp" line="1690"/>
        <source>game-specific</source>
        <translation>específico de jogo</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1430"/>
        <source>exporting %1 MESS configuration to %2</source>
        <translation>exportando configuração do MESS %1 para %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1430"/>
        <location filename="../../emuopt.cpp" line="1483"/>
        <location filename="../../emuopt.cpp" line="1591"/>
        <location filename="../../emuopt.cpp" line="1692"/>
        <source>machine-specific</source>
        <translation>específico de máquina</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1481"/>
        <source>done (exporting %1 MAME configuration to %2, elapsed time = %3)</source>
        <translation>feito (exportando configuração do MAME %1 para %2, tempo = %3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1483"/>
        <source>done (exporting %1 MESS configuration to %2, elapsed time = %3)</source>
        <translation>feito (exportando configuração do MESS %1 para %2, tempo = %3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1530"/>
        <source>WARNING: ini-import: no readable ini-paths found</source>
        <translation>AVISO: ini-import: nenhum caminho de ini com permissões de leitura encontrado</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1535"/>
        <source>Multiple ini-paths detected. Select path(s) to import from:</source>
        <translation>Multiplos caminhos de ini encontrados. Selecione os caminho(s) para importar de:</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1554"/>
        <source>WARNING: ini-import: no path selected (or invalid inipath)</source>
        <translation>AVISO: ini-import: nenhum caminho selecionado (ou caminho de ini inválido)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1583"/>
        <source>FATAL: can&apos;t open import file for reading, path = %1</source>
        <translation>FALTAL: impossível abrir arquivo de importação para leitura: caminho = %1</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1589"/>
        <source>importing %1 MAME configuration from %2</source>
        <translation>importando configuração do MAME %1 para %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1591"/>
        <source>importing %1 MESS configuration from %2</source>
        <translation>importando configuração do MESS %1 para %2</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1680"/>
        <source>WARNING: unknown option &apos;%1&apos; at line %2 (%3) ignored</source>
        <translation>AVISO: ignorada opção desconhecida &apos;%1&apos; na linha %2 (%3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1683"/>
        <source>WARNING: invalid syntax at line %1 (%2) ignored</source>
        <translation>AVISO: ignorada sintaxe inválida na linha %1 (%2)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1690"/>
        <source>done (importing %1 MAME configuration from %2, elapsed time = %3)</source>
        <translation>feito (importando configuração do MAME %1 para %2, tempo = %3)</translation>
    </message>
    <message>
        <location filename="../../emuopt.cpp" line="1692"/>
        <source>done (importing %1 MESS configuration from %2, elapsed time = %3)</source>
        <translation>feito (importando configuração do MESS %1 para %2, tempo = %3)</translation>
    </message>
</context>
<context>
    <name>FileEditWidget</name>
    <message>
        <location filename="../../fileeditwidget.cpp" line="49"/>
        <source>Choose file</source>
        <translation>Ecolha o arquivo</translation>
    </message>
</context>
<context>
    <name>FileSystemModel</name>
    <message>
        <location filename="../../filesystemmodel.h" line="274"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="274"/>
        <source>Size</source>
        <translation>Tamanho</translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="470"/>
        <location filename="../../filesystemmodel.h" line="484"/>
        <source> KB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="473"/>
        <location filename="../../filesystemmodel.h" line="487"/>
        <source> MB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="476"/>
        <location filename="../../filesystemmodel.h" line="490"/>
        <source> GB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="479"/>
        <source> TB</source>
        <translation></translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Tipo</translation>
    </message>
    <message>
        <location filename="../../filesystemmodel.h" line="274"/>
        <source>Date modified</source>
        <translation>Modificado</translation>
    </message>
</context>
<context>
    <name>Flyer</name>
    <message>
        <location filename="../../flyer.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>Copiar para área de transferência</translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="56"/>
        <location filename="../../flyer.cpp" line="57"/>
        <source>Game flyer image</source>
        <translation>Imagem de flyer do jogo</translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="59"/>
        <location filename="../../flyer.cpp" line="60"/>
        <source>Machine flyer image</source>
        <translation>Imagem de flyer da máquina</translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="68"/>
        <location filename="../../flyer.cpp" line="72"/>
        <source>FATAL: can&apos;t open flyer file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo de flyer. por favor verifique as permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../flyer.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Aguardando dados...</translation>
    </message>
</context>
<context>
    <name>Gamelist</name>
    <message>
        <location filename="../../gamelist.cpp" line="129"/>
        <location filename="../../gamelist.cpp" line="152"/>
        <location filename="../../gamelist.cpp" line="510"/>
        <location filename="../../gamelist.cpp" line="514"/>
        <location filename="../../gamelist.cpp" line="515"/>
        <location filename="../../gamelist.cpp" line="525"/>
        <location filename="../../gamelist.cpp" line="529"/>
        <location filename="../../gamelist.cpp" line="530"/>
        <location filename="../../gamelist.cpp" line="535"/>
        <location filename="../../gamelist.cpp" line="536"/>
        <location filename="../../gamelist.cpp" line="602"/>
        <location filename="../../gamelist.cpp" line="605"/>
        <source>unknown</source>
        <translation>desconhecido</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>good</source>
        <translation>bom</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>bad</source>
        <translation>ruim</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>preliminary</source>
        <translation>preliminar</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>supported</source>
        <translation>suportado</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="150"/>
        <source>unsupported</source>
        <translation>não suportado</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>imperfect</source>
        <translation>imperfeito</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>yes</source>
        <translation>sim</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>no</source>
        <translation>não</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>baddump</source>
        <translation>dump ruim</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="151"/>
        <source>nodump</source>
        <translation>sem dump</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <source>vertical</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <source>horizontal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <source>raster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="152"/>
        <location filename="../../gamelist.cpp" line="1492"/>
        <location filename="../../gamelist.cpp" line="1755"/>
        <source>Unknown</source>
        <translation>Desconhecido</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>On</source>
        <translation>Ligado</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>Off</source>
        <translation>Desligado</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>audio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>unused</source>
        <translation>não usado</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>Unused</source>
        <translation>Não usado</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="153"/>
        <source>cpu</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>vector</source>
        <translation>vetor</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>lcd</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>joy4way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>joy8way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="154"/>
        <source>trackball</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>joy2way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>doublejoy8way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>dial</source>
        <translation>discador</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>paddle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="155"/>
        <source>pedal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>stick</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>vjoy2way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>lightgun</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>doublejoy4way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="156"/>
        <source>vdoublejoy2way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>doublejoy2way</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>printer</source>
        <translation>impressora</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>cdrom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>cartridge</source>
        <translation>cartucho</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="157"/>
        <source>cassette</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>quickload</source>
        <translation>carregamento rápido</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>floppydisk</source>
        <translation>disquete</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>serial</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>snapshot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="158"/>
        <source>original</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="159"/>
        <source>compatible</source>
        <translation>compatível</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="165"/>
        <location filename="../../gamelist.cpp" line="169"/>
        <source>FATAL: can&apos;t open icon file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo do ícone, por favor verifique permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="387"/>
        <location filename="../../gamelist.cpp" line="389"/>
        <location filename="../../gamelist.cpp" line="392"/>
        <location filename="../../gamelist.cpp" line="394"/>
        <location filename="../../gamelist.cpp" line="1575"/>
        <location filename="../../gamelist.cpp" line="1838"/>
        <location filename="../../gamelist.cpp" line="2115"/>
        <location filename="../../gamelist.cpp" line="2951"/>
        <source>Waiting for data...</source>
        <translation>Aguardando dados...</translation>
    </message>
    <message>
        <source>determining emulator version and supported games</source>
        <translation type="obsolete">determinando a versão do emulador e jogos suportados</translation>
    </message>
    <message>
        <source>determining emulator version and supported machines</source>
        <translation type="obsolete">determinando a versão do emulador e máquinas suportados</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="509"/>
        <source>FATAL: selected executable file is not MAME</source>
        <translation>FATAL: arquivo executável selecionado não é MAME</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="524"/>
        <source>FATAL: selected executable file is not MESS</source>
        <translation>FATAL: arquivo executável selecionado não é MESS</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="534"/>
        <location filename="../../gamelist.cpp" line="598"/>
        <source>FATAL: can&apos;t create temporary file, please check emulator executable and permissions</source>
        <translation>FATAL: impossível criar aquivo temporário, por favor verifique o executável do emulador e suas permissões</translation>
    </message>
    <message>
        <source>done (determining emulator version and supported games, elapsed time = %1)</source>
        <translation type="obsolete">feito (determinando versão do emulador e jogos suportados, tempo = %1)</translation>
    </message>
    <message>
        <source>done (determining emulator version and supported machines, elapsed time = %1)</source>
        <translation type="obsolete">feito (determinando versão do emulador e máquinas suportadas, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="603"/>
        <source>emulator info: type = %1, version = %2</source>
        <translation>informação do emulador: tipo = %1, versão = %2</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="606"/>
        <source>FATAL: couldn&apos;t determine emulator type and version</source>
        <translation>FATAL: impossível determinar o tipo e versão do emulador</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="609"/>
        <source>FATAL: couldn&apos;t determine emulator version, type identification string is &apos;%1&apos; -- please inform developers if you&apos;re sure that this is a valid MAME binary</source>
        <translation>FATAL: impossível determinar a versão do emulador, texto de identificação de tipo é &apos;%1&apos; -- por favor informar os desenvolvedores se você está certo de que este executável do MAME é válido</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="611"/>
        <source>FATAL: couldn&apos;t determine emulator version, type identification string is &apos;%1&apos; -- please inform developers if you&apos;re sure that this is a valid MESS binary</source>
        <translation>FATAL: impossível determinar a versão do emulador, texto de identificação de tipo é &apos;%1&apos; -- por favor informar os desenvolvedores se você está certo de que este executável do MESS é válido</translation>
    </message>
    <message numerus="yes">
        <source>%n supported game(s)</source>
        <translation type="obsolete">
            <numerusform>%n jogo suportado</numerusform>
            <numerusform>%n jogos suportados</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n supported machine(s)</source>
        <translation type="obsolete">
            <numerusform>%n máquina suportada</numerusform>
            <numerusform>%n máquinas suportadas</numerusform>
        </translation>
    </message>
    <message>
        <source>FATAL: couldn&apos;t determine supported games</source>
        <translation type="obsolete">FATAL: impossível determinar jogos suportados</translation>
    </message>
    <message>
        <source>FATAL: couldn&apos;t determine supported machines</source>
        <translation type="obsolete">FATAL: impossível determinar máquinas suportadas</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="663"/>
        <source>loading XML game list data from cache</source>
        <translation>carregando lista de jogos (XML) do cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="665"/>
        <source>loading XML machine list data from cache</source>
        <translation>carregando lista de máquinas (XML) do cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="668"/>
        <source>XML cache - %p%</source>
        <translation>cache XML - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="706"/>
        <source>done (loading XML game list data from cache, elapsed time = %1)</source>
        <translation>feito (carregando lista de jogos (XML) do cache, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="708"/>
        <source>WARNING: XML game list cache is incomplete, invalidating XML game list cache</source>
        <translation>AVISO: lista de jogos (XML) do cache está imcompleto, invalidando lista de jogos (XML) do cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="710"/>
        <source>done (loading XML machine list data from cache, elapsed time = %1)</source>
        <translation>feito (carregando lista de máquinas (XML) do cache, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="712"/>
        <source>WARNING: XML machine list cache is incomplete, invalidating XML machine list cache</source>
        <translation>AVISO: lista de máquinas (XML) do cache está imcompleto, invalidando lista de máquinas (XML) do cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="776"/>
        <source>loading XML game list data and (re)creating cache</source>
        <translation>carregando lista de jogos (XML) e (re)criando cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="778"/>
        <source>loading XML machine list data and (re)creating cache</source>
        <translation>carregando lista de máquinas (XML) e (re)criando cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="781"/>
        <source>XML data - %p%</source>
        <translation>dados XML - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="798"/>
        <source>WARNING: can&apos;t open XML game list cache for writing, please check permissions</source>
        <translation>AVISO: impossível abrir lista de jogos (XML) cache para escrita, por favor verifique as permissões</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="800"/>
        <source>WARNING: can&apos;t open XML machine list cache for writing, please check permissions</source>
        <translation>AVISO: impossível abrir lista de máquinas (XML) cache para escrita, por favor verifique as permissões</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="845"/>
        <source>verifying ROM status for &apos;%1&apos;</source>
        <translation>verificando estado da ROM para &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="887"/>
        <source>verifying ROM status for all games</source>
        <translation>verificando estado da ROM para todos os jogos</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="889"/>
        <source>verifying ROM status for all machines</source>
        <translation>verificando estado da ROM para todas as máquinas</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="989"/>
        <source>retrieving game information for &apos;%1&apos;</source>
        <translation>obtendo informação do jogo para &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="991"/>
        <source>retrieving machine information for &apos;%1&apos;</source>
        <translation>obtendo informação da máquina para &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1006"/>
        <source>WARNING: couldn&apos;t find game information for &apos;%1&apos;</source>
        <translation>AVISO: impossível encontrar informações do jogo para &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1008"/>
        <source>WARNING: couldn&apos;t find machine information for &apos;%1&apos;</source>
        <translation>AVISO: impossível encontrar informações de máquina para &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Source file</source>
        <translation>Arquivo fonte</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Is BIOS?</source>
        <translation>É BIOS?</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Runnable</source>
        <translation>Executável</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Clone of</source>
        <translation>Clone de</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>ROM of</source>
        <translation>ROM de</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Sample of</source>
        <translation>Exemplo de</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1023"/>
        <source>Is device?</source>
        <translation>É dispositivo?</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1036"/>
        <source>Year</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1043"/>
        <source>Manufacturer</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1048"/>
        <location filename="../../gamelist.cpp" line="1475"/>
        <location filename="../../gamelist.cpp" line="1739"/>
        <source>ROM</source>
        <translation>ROM</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>BIOS</source>
        <translation>BIOS</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>Size</source>
        <translation>Tamanho</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>CRC</source>
        <translation>CRC</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>SHA1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Merge</source>
        <translation>Junção</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Region</source>
        <translation>Região</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <source>Offset</source>
        <translation>Desvio</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1161"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <location filename="../../gamelist.cpp" line="1206"/>
        <source>Status</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1058"/>
        <source>Device reference</source>
        <translation>Referência do dispositivo</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1063"/>
        <source>Chip</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1068"/>
        <location filename="../../gamelist.cpp" line="1138"/>
        <location filename="../../gamelist.cpp" line="1235"/>
        <source>Tag</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1068"/>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1068"/>
        <source>Clock</source>
        <translation>Relógio</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1073"/>
        <source>Display</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Rotate</source>
        <translation>Rotação</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Flip-X</source>
        <translation>Inverte-X</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Width</source>
        <translation>Largura</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Height</source>
        <translation>Altura</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Refresh</source>
        <translation>Recarregar</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>Pixel clock</source>
        <translation>Relógio do pixel</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>H-Total</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>H-Bend</source>
        <translation>Curvatura-H</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>HB-Start</source>
        <translation>Início-HB</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>V-Total</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>V-Bend</source>
        <translation>Curvatura-V</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1077"/>
        <source>VB-Start</source>
        <translation>Início-VB</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1082"/>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Sound</source>
        <translation>Som</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1086"/>
        <source>Channels</source>
        <translation>Canais</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1091"/>
        <source>Input</source>
        <translation>Entrada</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Service</source>
        <translation>Serviço</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Tilt</source>
        <translation>Declive</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Players</source>
        <translation>Jogadores</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Buttons</source>
        <translation>Botões</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1095"/>
        <source>Coins</source>
        <translation>Moedas</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1102"/>
        <source>Control</source>
        <translation>Controle</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Minimum</source>
        <translation>Mínimo</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Maximum</source>
        <translation>Máximo</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Sensitivity</source>
        <translation>Sensitividade</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Key Delta</source>
        <translation>Variação da tecla</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1107"/>
        <source>Reverse</source>
        <translation>Reverso</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1114"/>
        <source>DIP switch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1121"/>
        <source>DIP value</source>
        <translation>valor DIP</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1126"/>
        <location filename="../../gamelist.cpp" line="1150"/>
        <location filename="../../gamelist.cpp" line="1171"/>
        <location filename="../../gamelist.cpp" line="1196"/>
        <location filename="../../gamelist.cpp" line="1223"/>
        <location filename="../../gamelist.cpp" line="1273"/>
        <source>Default</source>
        <translation>Padrão</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1133"/>
        <source>Configuration</source>
        <translation>Configuração</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1138"/>
        <source>Mask</source>
        <translation>Máscara</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1145"/>
        <source>Setting</source>
        <translation>Configuração</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1150"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1157"/>
        <source>Driver</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Emulation</source>
        <translation>Emulação</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Color</source>
        <translation>Cor</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Graphic</source>
        <translation>Gráfico</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Cocktail</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Protection</source>
        <translation>Proteção</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Save state</source>
        <translation>Salvar estado</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1161"/>
        <source>Palette size</source>
        <translation>Tamanho da paleta</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1166"/>
        <source>BIOS set</source>
        <translation>Conjunto de BIOS</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1171"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1176"/>
        <source>Sample</source>
        <translation>Exemplo</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1181"/>
        <source>Disk</source>
        <translation>Disco</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>MD5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Index</source>
        <translation>Índice</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1191"/>
        <source>Adjuster</source>
        <translation>Ajustador</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1201"/>
        <source>Software list</source>
        <translation>Lista de Software</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1211"/>
        <source>Category</source>
        <translation>Categoria</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1218"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1230"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1235"/>
        <source>Mandatory</source>
        <translation>Mandatório</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1235"/>
        <source>Interface</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1242"/>
        <source>Instance</source>
        <translation>Instância</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1247"/>
        <source>Brief name</source>
        <translation>Nome resumido</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1254"/>
        <source>Extension</source>
        <translation>Extensão</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1261"/>
        <source>RAM options</source>
        <translation>Opções de RAM</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1265"/>
        <source>Option</source>
        <translation>Opção</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1310"/>
        <source>WARNING: can&apos;t open ROM state cache, please check ROMs</source>
        <translation>AVISO: impossível abrir cache de estado da ROM, por favor verifique as ROMs</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1313"/>
        <source>loading ROM state from cache</source>
        <translation>carregando estado da ROM do cache</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1315"/>
        <source>ROM states - %p%</source>
        <translation>Estados da ROM - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1337"/>
        <source>done (loading ROM state from cache, elapsed time = %1)</source>
        <translation>feito (carregando estado da ROM do cache, tempo = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="1338"/>
        <source>%n cached ROM state(s) loaded</source>
        <translation>
            <numerusform>%n estado de ROM carregado do cache</numerusform>
            <numerusform>%n estados de ROM carregados do cache</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1481"/>
        <location filename="../../gamelist.cpp" line="1482"/>
        <location filename="../../gamelist.cpp" line="1745"/>
        <location filename="../../gamelist.cpp" line="1746"/>
        <source>N/A</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2166"/>
        <source>%n game(s)</source>
        <translation>
            <numerusform>%n jogo</numerusform>
            <numerusform>%n jogos</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2166"/>
        <location filename="../../gamelist.cpp" line="2169"/>
        <source> and %n device(s) loaded</source>
        <translation>
            <numerusform>e %n dispositivo carregado</numerusform>
            <numerusform>e %n dispositivos carregados</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2166"/>
        <location filename="../../gamelist.cpp" line="2169"/>
        <source>, %n BIOS set(s)</source>
        <translation>
            <numerusform>, %n conjuto de BIOS</numerusform>
            <numerusform>, %n conjutos de BIOS</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="2169"/>
        <source>%n machine(s)</source>
        <translation>
            <numerusform>%n máquina</numerusform>
            <numerusform>%n máquinas</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2193"/>
        <location filename="../../gamelist.cpp" line="2923"/>
        <source>ROM state info: L:%1 C:%2 M:%3 I:%4 N:%5 U:%6</source>
        <translation>Informação do estado da ROM: L:%1 C:%2 M:%3 I:%4 N:%5 U:%6</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2205"/>
        <source>WARNING: ROM state cache is incomplete or not up to date, triggering an automatic ROM check</source>
        <translation>AVISO: cache de estado de ROM está imcompleto ou desatualizado, iniciando uma checagem automática de ROM</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2208"/>
        <source>WARNING: ROM state cache is incomplete or not up to date, please re-check ROMs</source>
        <translation>AVISO: cache de estado de ROM está imcompleto ou desatualizado, por favor re-verifique as ROMs</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1347"/>
        <source>processing game list</source>
        <translation>processando lista de jogos</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="421"/>
        <source>determining emulator version and supported sets</source>
        <translation>determinando versão do emulador e conjuntos suportados</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="596"/>
        <source>done (determining emulator version and supported sets, elapsed time = %1)</source>
        <translation>feito (determinando versão do emulador e conjuntos suportados, tempo = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="621"/>
        <source>%n supported set(s)</source>
        <translation>
            <numerusform>%n conjunto suportado</numerusform>
            <numerusform>%n conjuntos suportados</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="623"/>
        <source>FATAL: couldn&apos;t determine the number of supported sets</source>
        <translation>FATAL: impossível determinar o número de conjuntos suportados</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1349"/>
        <source>processing machine list</source>
        <translation>processando lista de máquinas</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1384"/>
        <source>WARNING: couldn&apos;t determine emulator version of game list cache</source>
        <translation>AVISO: impossível determinar versão do emulador do cache da lista de jogos</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1386"/>
        <source>WARNING: couldn&apos;t determine emulator version of machine list cache</source>
        <translation>AVISO: impossível determinar versão do emulador do cache da lista de máquinas</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1391"/>
        <location filename="../../gamelist.cpp" line="1400"/>
        <source>INFORMATION: the game list cache will now be updated due to a new format</source>
        <translation>INFORMAÇÃO: o cache de lista de jogos será atualizado devido ao novo formato</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1393"/>
        <location filename="../../gamelist.cpp" line="1402"/>
        <source>INFORMATION: the machine list cache will now be updated due to a new format</source>
        <translation>INFORMAÇÃO: o cache de lista de máquinas será atualizado devido ao novo formato</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1415"/>
        <source>loading game data from game list cache</source>
        <translation>carregando dados do jogo do cache da lista de jogo</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1417"/>
        <location filename="../../gamelist.cpp" line="1632"/>
        <source>Game data - %p%</source>
        <translation>Dado do jogo - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1419"/>
        <source>loading machine data from machine list cache</source>
        <translation>carregando dado da máquina do cache da lista de máquinas</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1421"/>
        <location filename="../../gamelist.cpp" line="1636"/>
        <source>Machine data - %p%</source>
        <translation>Dado da máquina - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1473"/>
        <location filename="../../gamelist.cpp" line="1737"/>
        <source>ROM, CHD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1477"/>
        <location filename="../../gamelist.cpp" line="1741"/>
        <source>CHD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1493"/>
        <location filename="../../gamelist.cpp" line="1756"/>
        <location filename="../../gamelist.cpp" line="3549"/>
        <location filename="../../gamelist.cpp" line="3652"/>
        <source>?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1609"/>
        <source>done (loading game data from game list cache, elapsed time = %1)</source>
        <translation>feito (carregando dado do jogo do cache da lista de jogos, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1611"/>
        <source>done (loading machine data from machine list cache, elapsed time = %1)</source>
        <translation>feito (carregando dado da máquina do cache da lista de máquinas, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1630"/>
        <source>parsing game data and (re)creating game list cache</source>
        <translation>analisando dados do jogo e (re)criando cache da lista de jogos</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1634"/>
        <source>parsing machine data and (re)creating machine list cache</source>
        <translation>analisando dados da máquina e (re)criando cache da lista de máquinas</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1643"/>
        <source>ERROR: can&apos;t open game list cache for writing, path = %1</source>
        <translation>ERRO: impossível abrir cache da lista de jogos para escrita, caminho = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1645"/>
        <source>ERROR: can&apos;t open machine list cache for writing, path = %1</source>
        <translation>ERRO: impossível abrir cache da lista de máquinas para escrita, caminho = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2101"/>
        <location filename="../../gamelist.cpp" line="2937"/>
        <source>sorting game list by %1 in %2 order</source>
        <translation>ordenando lista de jogos por %1 na ordem %2</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2101"/>
        <location filename="../../gamelist.cpp" line="2103"/>
        <location filename="../../gamelist.cpp" line="2937"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>ascending</source>
        <translation>crescente</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2101"/>
        <location filename="../../gamelist.cpp" line="2103"/>
        <location filename="../../gamelist.cpp" line="2937"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>descending</source>
        <translation>decrescente</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2103"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>sorting machine list by %1 in %2 order</source>
        <translation>ordenando lista de máquinas por %1 na ordem %2</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2135"/>
        <location filename="../../gamelist.cpp" line="2151"/>
        <source>restoring game selection</source>
        <translation>restaurando seleção de jogos</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2137"/>
        <location filename="../../gamelist.cpp" line="2153"/>
        <source>restoring machine selection</source>
        <translation>restaurando seleção de máquinas</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2165"/>
        <source>done (processing game list, elapsed time = %1)</source>
        <translation>feito (processando lista de jogos, tempo = %1)</translation>
    </message>
    <message numerus="yes">
        <source>%n game(s) loaded</source>
        <translation type="obsolete">
            <numerusform>%n jogo carregado</numerusform>
            <numerusform>%n jogos carregados</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2168"/>
        <source>done (processing machine list, elapsed time = %1)</source>
        <translation>feito (processando lista de máquinas, tempo = %1)</translation>
    </message>
    <message numerus="yes">
        <source>%n machine(s) loaded</source>
        <translation type="obsolete">
            <numerusform>%n máquina carregada</numerusform>
            <numerusform>%n máquinas carregadas</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2175"/>
        <source>WARNING: game list not fully parsed, invalidating game list cache</source>
        <translation>AVISO: lista de jogos não totalmente analisada, invalidando cache de lista de jogos</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2178"/>
        <source>WARNING: machine list not fully parsed, invalidating machine list cache</source>
        <translation>AVISO: lista de máquinas não totalmente analisada, invalidando cache de lista de máquinas</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2184"/>
        <source>WARNING: game list cache is out of date, invalidating game list cache</source>
        <translation>AVISO: lista de jogos desatualizada, invalidando cache da lista de jogos</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2187"/>
        <source>WARNING: machine list cache is out of date, invalidating machine list cache</source>
        <translation>AVISO: lista de máquinas desatualizada, invalidando cache da lista de máquinas</translation>
    </message>
    <message>
        <source>ROM state info: C:%1 M:%2 I:%3 N:%4 U:%5</source>
        <translation type="obsolete">Informação do estado da ROM: C:%1 M:%2 I:%3 N:%4 U:%5</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2228"/>
        <source>ROM state filter already active</source>
        <translation>Filtro de estado da ROM já ativo</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2233"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation>por favor aguarde a verificação de ROM terminar e tente novamente</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2238"/>
        <source>please wait for reload to finish and try again</source>
        <translation>por favor aguarde o fim do recarregamente e tente novamente</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2247"/>
        <source>applying ROM state filter</source>
        <translation>aplicando filtro de estado de ROM</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2250"/>
        <source>State filter - %p%</source>
        <translation>Filtro de estado - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2304"/>
        <source>done (applying ROM state filter, elapsed time = %1)</source>
        <translation>feito (aplicando filtro de estado de ROM, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2317"/>
        <source>saving game list</source>
        <translation>salvando lista de jogos</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2318"/>
        <source>done (saving game list)</source>
        <translation>feito (salvando lista de jogos)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2320"/>
        <source>saving machine list</source>
        <translation>salvando lista de máquinas</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2321"/>
        <source>done (saving machine list)</source>
        <translation>feito (salvando lista de máquinas)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2331"/>
        <source>loading favorites</source>
        <translation>carregando favoritos</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2355"/>
        <source>done (loading favorites)</source>
        <translation>feito (carregando favoritos)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2368"/>
        <source>saving favorites</source>
        <translation>salvando favoritos</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2384"/>
        <location filename="../../gamelist.cpp" line="2386"/>
        <source>FATAL: can&apos;t open favorites file for writing, path = %1</source>
        <translation>FATAL: impossível carregar arquivo de favoritos para escrita, caminho = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2390"/>
        <source>done (saving favorites)</source>
        <translation>feito (salvando favoritos)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2399"/>
        <source>loading play history</source>
        <translation>carregando histórico de jogadas</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2422"/>
        <source>done (loading play history)</source>
        <translation>feito (carregando histórico de jogadas)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2435"/>
        <source>saving play history</source>
        <translation>salvando histórico de jogadas</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2451"/>
        <location filename="../../gamelist.cpp" line="2453"/>
        <source>FATAL: can&apos;t open play history file for writing, path = %1</source>
        <translation>FATAL: impossível abrir arquivo de histórico de jogadas para escrita, camiho = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2457"/>
        <source>done (saving play history)</source>
        <translation>feito (salvando histórico de jogadas)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2468"/>
        <source>L:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2469"/>
        <source>C:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2470"/>
        <source>M:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2471"/>
        <source>I:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2472"/>
        <source>N:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2473"/>
        <source>U:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2474"/>
        <source>S:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2475"/>
        <source>T:</source>
        <translation>T:</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2499"/>
        <location filename="../../gamelist.cpp" line="2682"/>
        <source>WARNING: emulator audit call didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation>AVISO: chamada de auditoria do emulador não acabou sem erros -- código de saída = %1, estado de saída = %2</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2499"/>
        <location filename="../../gamelist.cpp" line="2682"/>
        <source>normal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2499"/>
        <location filename="../../gamelist.cpp" line="2682"/>
        <source>crashed</source>
        <translation>quebrado</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2504"/>
        <source>done (loading XML game list data and (re)creating cache, elapsed time = %1)</source>
        <translation>feito (carregando dados da lista de jogos (XML) e (re)criando cache, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2506"/>
        <source>done (loading XML machine list data and (re)creating cache, elapsed time = %1)</source>
        <translation>feito (carregando dados da lista de máquinas (XML) e (re)criando cache, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="879"/>
        <location filename="../../gamelist.cpp" line="901"/>
        <source>ERROR: can&apos;t open ROM state cache for writing, path = %1</source>
        <translation>ERRO: impossível abrir cache de estado da ROM para escrita, caminho = %1</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="471"/>
        <location filename="../../gamelist.cpp" line="567"/>
        <source>FATAL: can&apos;t start MAME executable within a reasonable time frame, giving up</source>
        <translation>FATAL: impossível iniciar o executável do MAME dentro de um tempo razoável, desistindo</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="473"/>
        <location filename="../../gamelist.cpp" line="569"/>
        <source>FATAL: can&apos;t start MESS executable within a reasonable time frame, giving up</source>
        <translation>FATAL: impossível iniciar o executável do MESS dentro de um tempo razoável, desistindo</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="908"/>
        <source>ROM check - %p%</source>
        <translation>Verificação da ROM - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="1053"/>
        <location filename="../../gamelist.cpp" line="1186"/>
        <source>Optional</source>
        <translation>Opcional</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2738"/>
        <location filename="../../gamelist.cpp" line="2874"/>
        <location filename="../../gamelist.cpp" line="3188"/>
        <source>WARNING: can&apos;t find item map entry for &apos;%1&apos; - ROM state cannot be determined</source>
        <translation>AVISO: impossível encontrar entrada para &apos;%1&apos; - estado da ROM não pode ser determinado</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2915"/>
        <source>done (verifying ROM status for &apos;%1&apos;, elapsed time = %2)</source>
        <translation>feito (verificando estado da ROM para &apos;%1&apos;, tempo = %2)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2918"/>
        <source>done (verifying ROM status for all games, elapsed time = %1)</source>
        <translation>feito (verificando estado para todos os jogos, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2920"/>
        <source>done (verifying ROM status for all machines, elapsed time = %1)</source>
        <translation>feito (verificando estado para todas as máquinas, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2937"/>
        <location filename="../../gamelist.cpp" line="2939"/>
        <source>ROM state</source>
        <translation>Estado da ROM</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3203"/>
        <source>ROM status for &apos;%1&apos; is &apos;%2&apos;</source>
        <translation>Estado da ROM para &apos;%1&apos; é &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3293"/>
        <source>pre-caching icons from ZIP archive</source>
        <translation>pre-cache de íconos do arquivo ZIP</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3299"/>
        <location filename="../../gamelist.cpp" line="3378"/>
        <source>Icon cache - %p%</source>
        <translation>Cache de ícones - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3349"/>
        <source>done (pre-caching icons from ZIP archive, elapsed time = %1)</source>
        <translation>feito (pre-cache de íconos do arquivo ZIP, tempo = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../gamelist.cpp" line="3350"/>
        <location filename="../../gamelist.cpp" line="3417"/>
        <source>%n icon(s) loaded</source>
        <translation>
            <numerusform>%n ícone carregado</numerusform>
            <numerusform>%n ícones carregados</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3363"/>
        <source>pre-caching icons from directory</source>
        <translation>pre-cache de ícones do diretório</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3416"/>
        <source>done (pre-caching icons from directory, elapsed time = %1)</source>
        <translation>feito (pre-cache de ícones do diretório, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3445"/>
        <source>loading catver.ini</source>
        <translation>carregando catver.ini</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3451"/>
        <source>Catver.ini - %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3489"/>
        <source>ERROR: can&apos;t open &apos;%1&apos; for reading -- no catver.ini data available</source>
        <translation>ERRO: impossível abrir &apos;%1&apos; para leitura -- arquivo catver.ini não disponível</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3498"/>
        <source>done (loading catver.ini, elapsed time = %1)</source>
        <translation>feito (carregando catver.ini, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3499"/>
        <source>%1 category / %2 version records loaded</source>
        <translation>%1 categoria / %2 versão de registros carregados</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3532"/>
        <source>Category view - %p%</source>
        <translation>Visão de categoria - %p%</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3635"/>
        <source>Version view - %p%</source>
        <translation>Visão de versão - %p%</translation>
    </message>
</context>
<context>
    <name>ImageChecker</name>
    <message>
        <location filename="../../imgcheck.cpp" line="70"/>
        <location filename="../../imgcheck.cpp" line="72"/>
        <source>Select machine</source>
        <translation>Selecionar máquina</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="71"/>
        <location filename="../../imgcheck.cpp" line="73"/>
        <source>Select machine in machine list if selected in check lists?</source>
        <translation>Selecionar máquina na lista de máquinas se selecionada na lista de checagem?</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="104"/>
        <source>checking previews from ZIP archive</source>
        <translation>Verificando previews do arquivo ZIP</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="106"/>
        <source>checking previews from directory</source>
        <translation>verificando previews do diretório</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="109"/>
        <source>Preview check - %p%</source>
        <translation>Verificação de preview - %p%</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="117"/>
        <location filename="../../imgcheck.cpp" line="441"/>
        <location filename="../../imgcheck.cpp" line="767"/>
        <source>WARNING: game list not fully loaded, check results may be misleading</source>
        <translation>AVISO: lista de jogos não completamente carregada, verificação de resultados podem ser enganosos</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="119"/>
        <location filename="../../imgcheck.cpp" line="443"/>
        <location filename="../../imgcheck.cpp" line="769"/>
        <source>WARNING: machine list not fully loaded, check results may be misleading</source>
        <translation>AVISO: lista de máquinas não completamente carregada, verificação de resultados podem ser enganosos</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="123"/>
        <location filename="../../imgcheck.cpp" line="124"/>
        <location filename="../../imgcheck.cpp" line="125"/>
        <location filename="../../imgcheck.cpp" line="447"/>
        <location filename="../../imgcheck.cpp" line="448"/>
        <location filename="../../imgcheck.cpp" line="449"/>
        <location filename="../../imgcheck.cpp" line="773"/>
        <location filename="../../imgcheck.cpp" line="774"/>
        <location filename="../../imgcheck.cpp" line="775"/>
        <source>&amp;Stop check</source>
        <translation>&amp;Parar verificação</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="70"/>
        <location filename="../../imgcheck.ui" line="243"/>
        <location filename="../../imgcheck.ui" line="323"/>
        <location filename="../../imgcheck.cpp" line="137"/>
        <location filename="../../imgcheck.cpp" line="461"/>
        <location filename="../../imgcheck.cpp" line="787"/>
        <source>Found: 0</source>
        <translation>Encontrado: 0</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="83"/>
        <location filename="../../imgcheck.ui" line="230"/>
        <location filename="../../imgcheck.ui" line="336"/>
        <location filename="../../imgcheck.cpp" line="139"/>
        <location filename="../../imgcheck.cpp" line="463"/>
        <location filename="../../imgcheck.cpp" line="789"/>
        <source>Missing: 0</source>
        <translation>Faltando: 0</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="96"/>
        <location filename="../../imgcheck.ui" line="217"/>
        <location filename="../../imgcheck.ui" line="349"/>
        <location filename="../../imgcheck.cpp" line="141"/>
        <location filename="../../imgcheck.cpp" line="465"/>
        <location filename="../../imgcheck.cpp" line="791"/>
        <source>Obsolete: 0</source>
        <translation>Obsoleto: 0</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="146"/>
        <source>check pass 1: found and missing previews</source>
        <translation>primeira passagem de verificação: previews encontrados e faltantes</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="154"/>
        <location filename="../../imgcheck.cpp" line="176"/>
        <location filename="../../imgcheck.cpp" line="478"/>
        <location filename="../../imgcheck.cpp" line="500"/>
        <location filename="../../imgcheck.cpp" line="804"/>
        <location filename="../../imgcheck.cpp" line="826"/>
        <source>Found: %1</source>
        <translation>Encontrado: %1</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="156"/>
        <location filename="../../imgcheck.cpp" line="179"/>
        <location filename="../../imgcheck.cpp" line="480"/>
        <location filename="../../imgcheck.cpp" line="503"/>
        <location filename="../../imgcheck.cpp" line="806"/>
        <location filename="../../imgcheck.cpp" line="829"/>
        <source>Missing: %1</source>
        <translation>Faltando: %1</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="187"/>
        <location filename="../../imgcheck.cpp" line="511"/>
        <location filename="../../imgcheck.cpp" line="837"/>
        <source>check pass 2: obsolete files: reading ZIP directory</source>
        <translation>segunda passagem de verificação: arquivos obsoletos: lendo ZIP</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="219"/>
        <location filename="../../imgcheck.cpp" line="232"/>
        <location filename="../../imgcheck.cpp" line="252"/>
        <location filename="../../imgcheck.cpp" line="261"/>
        <location filename="../../imgcheck.cpp" line="543"/>
        <location filename="../../imgcheck.cpp" line="556"/>
        <location filename="../../imgcheck.cpp" line="576"/>
        <location filename="../../imgcheck.cpp" line="585"/>
        <location filename="../../imgcheck.cpp" line="870"/>
        <location filename="../../imgcheck.cpp" line="883"/>
        <location filename="../../imgcheck.cpp" line="903"/>
        <location filename="../../imgcheck.cpp" line="912"/>
        <source>Obsolete: %1</source>
        <translation>Obsoleto: %1</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="236"/>
        <location filename="../../imgcheck.cpp" line="560"/>
        <location filename="../../imgcheck.cpp" line="887"/>
        <source>check pass 2: obsolete files: reading directory structure</source>
        <translation>segunda passagem de verificação: arquivos obsoletos: lendo estrutura de diretórios</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="278"/>
        <source>done (checking previews from ZIP archive, elapsed time = %1)</source>
        <translation>feito (verificando previews do ZIP, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="280"/>
        <source>done (checking previews from directory, elapsed time = %1)</source>
        <translation>feito (verificando previews do diretório, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="281"/>
        <location filename="../../imgcheck.cpp" line="605"/>
        <location filename="../../imgcheck.cpp" line="932"/>
        <source>%1 found, %2 missing, %3 obsolete</source>
        <translation>%1 encontrados, %2 faltando, %3 obsoletos</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="115"/>
        <location filename="../../imgcheck.cpp" line="287"/>
        <location filename="../../imgcheck.cpp" line="611"/>
        <location filename="../../imgcheck.cpp" line="938"/>
        <source>&amp;Check previews</source>
        <translation>&amp;Verificar previews</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="253"/>
        <location filename="../../imgcheck.cpp" line="288"/>
        <location filename="../../imgcheck.cpp" line="612"/>
        <location filename="../../imgcheck.cpp" line="939"/>
        <source>&amp;Check flyers</source>
        <translation>&amp;Verificar flyers</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="368"/>
        <location filename="../../imgcheck.cpp" line="289"/>
        <location filename="../../imgcheck.cpp" line="613"/>
        <location filename="../../imgcheck.cpp" line="940"/>
        <source>&amp;Check icons</source>
        <translation>&amp;Verificar ícones</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="428"/>
        <source>checking flyers from ZIP archive</source>
        <translation>verificando flyers do ZIP</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="430"/>
        <source>checking flyers from directory</source>
        <translation>verificando flyers do diretório</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="433"/>
        <source>Flyer check - %p%</source>
        <translation>Verificação de flyer - %p%</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="470"/>
        <source>check pass 1: found and missing flyers</source>
        <translation>primeira passagem de verificação: flyers encontrados e faltantes</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="602"/>
        <source>done (checking flyers from ZIP archive, elapsed time = %1)</source>
        <translation>feito (verificando flyers do ZIP, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="604"/>
        <source>done (checking flyers from directory, elapsed time = %1)</source>
        <translation>feito (verificando flyers do diretório, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="754"/>
        <source>checking icons from ZIP archive</source>
        <translation>verificando ícones do ZIP</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="756"/>
        <source>checking icons from directory</source>
        <translation>verificando ícones do diretório</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="759"/>
        <source>Icon check - %p%</source>
        <translation>Verificação de ícones - %p%</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="796"/>
        <source>check pass 1: found and missing icons</source>
        <translation>primeira passagem de verificação: ícones encontrados e faltantes</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="929"/>
        <source>done (checking icons from ZIP archive, elapsed time = %1)</source>
        <translation>feito (verificando ícones do ZIP, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="931"/>
        <source>done (checking icons from directory, elapsed time = %1)</source>
        <translation>feito (verificando ícones do diretório, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1072"/>
        <source>please wait for reload to finish and try again</source>
        <translation>por favor aguarde finalizar o recarregamento e tente novamente</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1077"/>
        <source>please wait for ROMAlyzer to finish the current analysis and try again</source>
        <translation>por favor espere o ROMAlyzer terminar a análise atual e tente novamente</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1082"/>
        <source>please wait for ROM state filter to finish and try again</source>
        <translation>por favor espere o filtro de estado de ROM terminar e tente novamente</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1087"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation>por favor aguarde a verificação de ROM terminar e tente novamente</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1092"/>
        <source>please wait for sample check to finish and try again</source>
        <translation>por favor espere a verificação de exemplos terminar e tente novamente</translation>
    </message>
    <message>
        <location filename="../../imgcheck.cpp" line="1097"/>
        <source>stopping image check upon user request</source>
        <translation>parando verificação de imagem devido à requisição do usuário</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="15"/>
        <source>Check images</source>
        <translation>Verificar imagens</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="36"/>
        <source>Close image check dialog</source>
        <translation>Fecha a janela de verificação de imagens</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="39"/>
        <source>C&amp;lose</source>
        <translation>F&amp;echar</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="56"/>
        <source>&amp;Previews</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="112"/>
        <source>Check preview images / stop check</source>
        <translation>Verificar imagens de preview / parar verificação</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="122"/>
        <location filename="../../imgcheck.ui" line="276"/>
        <location filename="../../imgcheck.ui" line="388"/>
        <source>Select game in game list if selected in check lists?</source>
        <translation>Selecionar jogo na lista quando selecionada na lista de checagem?</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="125"/>
        <location filename="../../imgcheck.ui" line="279"/>
        <location filename="../../imgcheck.ui" line="391"/>
        <source>Select &amp;game</source>
        <translation>Selecionar &amp;jogo</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="164"/>
        <source>Remove obsolete preview images</source>
        <translation>Remover imagens de exemplo obsoletas</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="167"/>
        <location filename="../../imgcheck.ui" line="266"/>
        <location filename="../../imgcheck.ui" line="381"/>
        <source>&amp;Remove obsolete</source>
        <translation>&amp;Remover obsoleto</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="181"/>
        <source>&amp;Flyers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="250"/>
        <source>Check flyer images / stop check</source>
        <translation>Verificar imagens de flyer / parar verificação</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="263"/>
        <source>Remove obsolete flyer images</source>
        <translation>Remover imagens de flyer obsoletas</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="309"/>
        <source>&amp;Icons</source>
        <translation>&amp;Ícones</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="365"/>
        <source>Check icon images / stop check</source>
        <translation>Verificar imagens de ícones / parar verificação</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="378"/>
        <source>Remove obsolete icon images</source>
        <translation>Remover imagens de ícones obsoletas</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="401"/>
        <source>Clear cache before checking icons?</source>
        <translation>Limpar cache antes de verificar ícones?</translation>
    </message>
    <message>
        <location filename="../../imgcheck.ui" line="404"/>
        <source>C&amp;lear cache</source>
        <translation>&amp;Limpar cache</translation>
    </message>
</context>
<context>
    <name>ItemDownloader</name>
    <message>
        <location filename="../../downloaditem.cpp" line="98"/>
        <source>FATAL: can&apos;t open &apos;%1&apos; for writing</source>
        <translation>FATAL: impossível abrir %1 para escrita</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="112"/>
        <location filename="../../downloaditem.cpp" line="190"/>
        <location filename="../../downloaditem.cpp" line="207"/>
        <location filename="../../downloaditem.cpp" line="238"/>
        <source>Source URL: %1</source>
        <translation>Caminho (URL): %1</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="113"/>
        <location filename="../../downloaditem.cpp" line="191"/>
        <location filename="../../downloaditem.cpp" line="208"/>
        <location filename="../../downloaditem.cpp" line="239"/>
        <source>Local path: %2</source>
        <translation>Caminho Local: %2</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="114"/>
        <location filename="../../downloaditem.cpp" line="192"/>
        <location filename="../../downloaditem.cpp" line="209"/>
        <location filename="../../downloaditem.cpp" line="240"/>
        <source>Status: %1</source>
        <translation>Estado: %1</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="114"/>
        <source>initializing download</source>
        <translation>Inicializando download</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="115"/>
        <location filename="../../downloaditem.cpp" line="193"/>
        <location filename="../../downloaditem.cpp" line="210"/>
        <location filename="../../downloaditem.cpp" line="241"/>
        <source>Total size: %1</source>
        <translation>Tamanho total: %1</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="115"/>
        <source>unknown</source>
        <translation>desconhecido</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="116"/>
        <location filename="../../downloaditem.cpp" line="194"/>
        <location filename="../../downloaditem.cpp" line="211"/>
        <location filename="../../downloaditem.cpp" line="242"/>
        <source>Downloaded: %1 (%2%)</source>
        <translation>Baixado: %1 (%2%)</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="117"/>
        <source>download started: URL = %1, local path = %2, reply ID = %3</source>
        <translation>download iniciado: URL = %1, caminho local = %2, ID de reply = %3</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="175"/>
        <source>Error #%1: </source>
        <translation>Erro #%1:</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="181"/>
        <source>download aborted: reason = %1, URL = %2, local path = %3, reply ID = %4</source>
        <translation>download cancelado: razão = %1, URL = %2, caminho local = %3, ID de reply = %4</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="192"/>
        <source>download aborted</source>
        <translation>download cancelado</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="209"/>
        <source>downloading</source>
        <translation>baixando</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="235"/>
        <source>download finished: URL = %1, local path = %2, reply ID = %3</source>
        <translation>download finalizado: URL = %1, caminho local = %2, ID de reply = %3</translation>
    </message>
    <message>
        <location filename="../../downloaditem.cpp" line="240"/>
        <source>download finished</source>
        <translation>download finalizado</translation>
    </message>
</context>
<context>
    <name>ItemSelector</name>
    <message>
        <location filename="../../itemselect.ui" line="15"/>
        <source>Item selection</source>
        <translation>Seleção de Item</translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="21"/>
        <source>Select item(s)</source>
        <translation>Selecione item(s)</translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="54"/>
        <source>Confirm selection</source>
        <translation>Confirmar seleção</translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="57"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../itemselect.ui" line="67"/>
        <location filename="../../itemselect.ui" line="70"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>Joystick</name>
    <message>
        <location filename="../../joystick.cpp" line="23"/>
        <source>ERROR: couldn&apos;t initialize SDL joystick support</source>
        <translation>ERRO: impossível inicializar suporte à joystick do SDL</translation>
    </message>
    <message>
        <location filename="../../joystick.cpp" line="67"/>
        <source>ERROR: couldn&apos;t open SDL joystick #%1</source>
        <translation>ERRO: impossível abrir joystick #%1 do SDL</translation>
    </message>
</context>
<context>
    <name>JoystickCalibrationWidget</name>
    <message>
        <location filename="../../options.cpp" line="3790"/>
        <source>Enable/disable axis %1</source>
        <translation>Habilitar/desabilitar eixo %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3794"/>
        <source>Axis %1:</source>
        <translation>Eixo %1:</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3795"/>
        <source>Reset calibration of axis %1</source>
        <translation>Reiniciar calibração do eixo %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3803"/>
        <source>Current value of axis %1</source>
        <translation>Valor atual do eixo %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3807"/>
        <source>DZ:</source>
        <translation>ZM:</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3809"/>
        <source>Deadzone of axis %1</source>
        <translation>Zona morta do eixo %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3815"/>
        <source>S:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3817"/>
        <source>Sensitivity of axis %1</source>
        <translation>Sensibilidade do eixo %1</translation>
    </message>
</context>
<context>
    <name>JoystickFunctionScanner</name>
    <message>
        <location filename="../../joyfuncscan.ui" line="15"/>
        <location filename="../../joyfuncscan.ui" line="72"/>
        <location filename="../../joyfuncscan.cpp" line="24"/>
        <location filename="../../joyfuncscan.cpp" line="25"/>
        <source>Scanning joystick function</source>
        <translation>Escanear função do joystick</translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="27"/>
        <source>&lt;&lt;&lt;&gt;&gt;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="43"/>
        <source>Accept joystick function</source>
        <translation>Aceitar função do joystick</translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="46"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="56"/>
        <source>Cancel remapping of joystick function</source>
        <translation>Cancelar remapeamento da função do joystick</translation>
    </message>
    <message>
        <location filename="../../joyfuncscan.ui" line="59"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>JoystickTestWidget</name>
    <message>
        <location filename="../../options.cpp" line="4038"/>
        <source>A%1: %v</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4039"/>
        <source>Current value of axis %1</source>
        <translation>Valor atual do eixo %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4049"/>
        <source>B%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4050"/>
        <source>Current state of button %1</source>
        <translation>Estado atual do botão %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4060"/>
        <source>H%1: 0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4061"/>
        <source>Current value of hat %1</source>
        <translation>Valor atual do hat %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4071"/>
        <source>T%1 DX: 0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4072"/>
        <source>Current X-delta of trackball %1</source>
        <translation>Diferença X atual do trackball %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4082"/>
        <source>T%1 DY: 0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4083"/>
        <source>Current Y-delta of trackball %1</source>
        <translation>Diferença Y atual do trackball %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4171"/>
        <source>H%1: %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4205"/>
        <source>T%1 DX: %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="4206"/>
        <source>T%1 DY: %2</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>KeySequenceScanner</name>
    <message>
        <location filename="../../keyseqscan.cpp" line="21"/>
        <location filename="../../keyseqscan.cpp" line="22"/>
        <source>Scanning special key</source>
        <translation>Escaneando tecla especial</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="15"/>
        <location filename="../../keyseqscan.ui" line="72"/>
        <location filename="../../keyseqscan.cpp" line="24"/>
        <location filename="../../keyseqscan.cpp" line="25"/>
        <source>Scanning shortcut</source>
        <translation>Escaneando atalho</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="27"/>
        <source>&lt;&lt;&lt;&gt;&gt;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="43"/>
        <source>Accept key sequence</source>
        <translation>Aceitar sequência de teclas</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="46"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="56"/>
        <source>Cancel redefinition of key sequence</source>
        <translation>Cancelar redefinição de sequência de teclas</translation>
    </message>
    <message>
        <location filename="../../keyseqscan.ui" line="59"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>MESSDeviceConfigurator</name>
    <message>
        <location filename="../../messdevcfg.cpp" line="225"/>
        <source>Select default device directory</source>
        <translation>Selecionar o diretório de dispositivo padrão</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="226"/>
        <source>&amp;Default device directory for &apos;%1&apos;...</source>
        <translation>Diretório de dispositivo &amp;Padrão para &apos;%1&apos;...</translation>
    </message>
    <message>
        <source>Generate device configurations</source>
        <translation type="obsolete">Gerar configurações de dispositivo</translation>
    </message>
    <message>
        <source>&amp;Generate configurations for &apos;%1&apos;...</source>
        <translation type="obsolete">&amp;Gerar configurações para &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="146"/>
        <location filename="../../messdevcfg.cpp" line="385"/>
        <source>Reading slot info, please wait...</source>
        <translation>Lendo informações de slot, por favor espere...</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="149"/>
        <source>Enter configuration name</source>
        <translation>Entre o nome da configuração</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="166"/>
        <source>Enter search string</source>
        <translation>Entre o texto de busca</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="234"/>
        <location filename="../../messdevcfg.cpp" line="276"/>
        <source>Play selected game</source>
        <translation>Jogar selecionado</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="235"/>
        <location filename="../../messdevcfg.cpp" line="277"/>
        <source>&amp;Play</source>
        <translation>&amp;Jogar</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="240"/>
        <location filename="../../messdevcfg.cpp" line="282"/>
        <source>Play selected game (embedded)</source>
        <translation>Jogar jogo selecionado (embutido)</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="241"/>
        <location filename="../../messdevcfg.cpp" line="283"/>
        <source>Play &amp;embedded</source>
        <translation>Jogar &amp;embutido</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="247"/>
        <source>Remove configuration</source>
        <translation>Remover configuração</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="248"/>
        <source>&amp;Remove configuration</source>
        <translation>&amp;Remover configuração</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="256"/>
        <source>Select a file to be mapped to this device instance</source>
        <translation>Selecione um arquivo para ser mapeado para essa instância do dispositivo</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="257"/>
        <source>Select file...</source>
        <translation>Selecionar arquivo...</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="268"/>
        <source>Use as default directory</source>
        <translation>Usar como diretório padrão</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="290"/>
        <location filename="../../messdevcfg.cpp" line="1368"/>
        <source>&amp;Open archive</source>
        <translation>&amp;Abrir arquivo compactado</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="332"/>
        <location filename="../../messdevcfg.cpp" line="614"/>
        <location filename="../../messdevcfg.cpp" line="1516"/>
        <source>No devices available</source>
        <translation>Nenhum dispositivo disponível</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="387"/>
        <source>loading available system slots</source>
        <translation>carregando slots de sistema disponíveis</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="408"/>
        <source>FATAL: can&apos;t start MESS executable within a reasonable time frame, giving up</source>
        <translation>FATAL: impossível iniciar o executável do MESS dentro de um tempo razoável, desistindo</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="409"/>
        <source>Failed to read slot info</source>
        <translation>Falha ao ler informações de slot</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="549"/>
        <source>done (loading available system slots, elapsed time = %1)</source>
        <translation>feito (carregando slots de sistema disponíveis, tempo gasto = %1)</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="639"/>
        <source>not used</source>
        <translation>não usado</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="677"/>
        <location filename="../../messdevcfg.cpp" line="718"/>
        <location filename="../../messdevcfg.cpp" line="879"/>
        <location filename="../../messdevcfg.cpp" line="997"/>
        <source>No devices</source>
        <translation>Nenhum dispositivo</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="754"/>
        <location filename="../../messdevcfg.cpp" line="756"/>
        <source>%1. copy of </source>
        <translation>%1. cópia de</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="962"/>
        <location filename="../../messdevcfg.cpp" line="1531"/>
        <source>%1. variant of </source>
        <translation>%1. variação de</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1067"/>
        <source>Choose default device directory for &apos;%1&apos;</source>
        <translation>Escolha o diretório de dispositivo padrão para &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1368"/>
        <source>&amp;Close archive</source>
        <translation>&amp;Fechar arquivo compactado</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1533"/>
        <source>Choose a unique configuration name</source>
        <translation>Escolha um nome de configuração único</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1533"/>
        <source>Unique configuration name:</source>
        <translation>Nome de configuração único:</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1536"/>
        <source>Name conflict</source>
        <translation>Conflito de nome</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="1536"/>
        <source>A configuration named &apos;%1&apos; already exists.

Do you want to choose a different name?</source>
        <translation>Uma configuração com o nome &apos;%1&apos; já existe

Você quer escolher um nome diferente?</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="15"/>
        <source>MESS device configuration</source>
        <translation>Configurações de dispositivo do MESS</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="37"/>
        <location filename="../../messdevcfg.cpp" line="1124"/>
        <location filename="../../messdevcfg.cpp" line="1129"/>
        <source>Active device configuration</source>
        <translation>Configuração de dispositivo ativa</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="obsolete">Configuração</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="73"/>
        <location filename="../../messdevcfg.ui" line="76"/>
        <source>Device configuration menu</source>
        <translation>Menu de configuração de dispositivo</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="99"/>
        <location filename="../../messdevcfg.ui" line="102"/>
        <source>Name of device configuration</source>
        <translation>Nome das configurações dos dispositivos</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="115"/>
        <location filename="../../messdevcfg.ui" line="118"/>
        <source>Create a new device configuration</source>
        <translation>Criar uma nova configuração de dispositivo</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="138"/>
        <location filename="../../messdevcfg.ui" line="141"/>
        <source>Clone current device configuration</source>
        <translation>Clonar configuração de dispositivo ativa</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="161"/>
        <location filename="../../messdevcfg.ui" line="164"/>
        <source>Remove current device configuration from list of available configurations</source>
        <translation>Remover configuração de dispositivo atual da lista de configurações disponíveis</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="184"/>
        <location filename="../../messdevcfg.ui" line="187"/>
        <source>Save current device configuration to list of available configurations</source>
        <translation>Salva configuração de dispositivo atual para a lista de disponíveis</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="208"/>
        <source>Device mappings</source>
        <translation>Mapeamentos de dispositivos</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="229"/>
        <location filename="../../messdevcfg.ui" line="232"/>
        <source>Device setup of current configuration</source>
        <translation>Setup do dispositivo da configuração atual</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="245"/>
        <source>Device instance</source>
        <translation>Instância do dispositivo</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="250"/>
        <source>Brief name</source>
        <translation>Nome resumido</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="255"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="260"/>
        <source>Tag</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="265"/>
        <source>Extensions</source>
        <translation>Extensões</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="270"/>
        <source>File</source>
        <translation>Arquivo</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="279"/>
        <source>Slot options</source>
        <translation>Opções de slots</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="300"/>
        <location filename="../../messdevcfg.ui" line="303"/>
        <source>Available slot options</source>
        <translation>Opções de slots disponíveis</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="316"/>
        <source>Slot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="321"/>
        <source>Option</source>
        <translation>Opção</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="330"/>
        <source>File chooser</source>
        <translation>Seletor de arquivo</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="369"/>
        <location filename="../../messdevcfg.ui" line="372"/>
        <source>Save selected instance / file as a new device configuration</source>
        <translation>Salvar instância selecionada  / arquivar como nova configuração de dispositivo</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="392"/>
        <location filename="../../messdevcfg.ui" line="395"/>
        <source>Select the device instance the file is mapped to</source>
        <translation>Selecione a instância de dispositivo para o qual o arquivo está mapeado</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="411"/>
        <location filename="../../messdevcfg.ui" line="414"/>
        <source>Automatically select the first matching device instance when selecting a file with a valid extension</source>
        <translation>Selecionar automaticamente a primeira instância condizente quando selecionar um arquivo com a extensão válida</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="457"/>
        <location filename="../../messdevcfg.ui" line="460"/>
        <source>Process ZIP contents on item activation</source>
        <translation>Processar o conteúdo do ZIP na ativação do item</translation>
    </message>
    <message>
        <source>Auto-select</source>
        <translation type="obsolete">Auto selecionar</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="434"/>
        <location filename="../../messdevcfg.ui" line="437"/>
        <source>Filter files by allowed extensions for the current device instance</source>
        <translation>Filtrar arquivos por extensões permitidas para a instância de dispositivo atual</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation type="obsolete">Filtro</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="480"/>
        <location filename="../../messdevcfg.ui" line="483"/>
        <source>Enter search string (case-insensitive)</source>
        <translation>Entre o texto de busca (insensível à caixa)</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="499"/>
        <location filename="../../messdevcfg.ui" line="502"/>
        <source>Clear search string</source>
        <translation>Limpar texto de busca</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="519"/>
        <location filename="../../messdevcfg.ui" line="522"/>
        <source>Number of files scanned</source>
        <translation>Número de arquivos escaneados</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="544"/>
        <location filename="../../messdevcfg.ui" line="547"/>
        <source>Reload directory contents</source>
        <translation>Recarregar conteúdo do diretório</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="564"/>
        <location filename="../../messdevcfg.ui" line="567"/>
        <source>Play the selected configuration</source>
        <translation>Executar a configuração selecionada</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="584"/>
        <location filename="../../messdevcfg.ui" line="587"/>
        <source>Play the selected configuration (embedded)</source>
        <translation>Executar a configuração selecionada (embutido)</translation>
    </message>
    <message>
        <source>Choose directory</source>
        <translation type="obsolete">Escolha o diretório</translation>
    </message>
    <message>
        <source>Choose a file to map</source>
        <translation type="obsolete">Escolha um arquivo para mapear</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="653"/>
        <source>Available device configurations</source>
        <translation>Configurações de dispositivos disponíveis</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.ui" line="665"/>
        <location filename="../../messdevcfg.ui" line="668"/>
        <source>List of available device configurations</source>
        <translation>Lista de configurações de dispositivo disponíveis</translation>
    </message>
</context>
<context>
    <name>MESSDeviceFileDelegate</name>
    <message>
        <location filename="../../messdevcfg.cpp" line="53"/>
        <location filename="../../messdevcfg.cpp" line="73"/>
        <source>All files</source>
        <translation>Todos os arquivos</translation>
    </message>
    <message>
        <location filename="../../messdevcfg.cpp" line="56"/>
        <location filename="../../messdevcfg.cpp" line="58"/>
        <source>Valid device files</source>
        <translation>Arquivos de dispositivo válidos</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../qmc2main.cpp" line="406"/>
        <location filename="../../qmc2main.cpp" line="3530"/>
        <location filename="../../qmc2main.cpp" line="3566"/>
        <location filename="../../qmc2main.cpp" line="3755"/>
        <location filename="../../qmc2main.cpp" line="3844"/>
        <location filename="../../qmc2main.cpp" line="5237"/>
        <location filename="../../qmc2main.cpp" line="5476"/>
        <location filename="../../qmc2main.cpp" line="5515"/>
        <source>Default</source>
        <translation>Padrão</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="443"/>
        <source>QMC2 for MAME</source>
        <translation>QMC2 para MAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="444"/>
        <location filename="../../qmc2main.cpp" line="445"/>
        <source>Launch QMC2 for MAME</source>
        <translation>Iniciar o QMC2 para MAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="446"/>
        <source>QMC2 for MESS</source>
        <translation>QMC2 para MESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="447"/>
        <location filename="../../qmc2main.cpp" line="448"/>
        <source>Launch QMC2 for MESS</source>
        <translation>Iniciar o QMC2 para MESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="488"/>
        <source>Toggle maximization of embedded emulator windows</source>
        <translation>Ativar/desativar maximização das janelas de emuladores embutidos</translation>
    </message>
    <message>
        <location filename="../../macros.h" line="427"/>
        <location filename="../../macros.h" line="433"/>
        <location filename="../../qmc2main.cpp" line="527"/>
        <source>M.E.S.S. Catalog / Launcher II</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="528"/>
        <location filename="../../qmc2main.cpp" line="1080"/>
        <source>Machine / Attribute</source>
        <translation>Máquina / Atributo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="529"/>
        <location filename="../../qmc2main.cpp" line="1120"/>
        <source>Machine / Clones</source>
        <translation>Máquina / Clones</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="530"/>
        <source>M&amp;achine list</source>
        <translation>Lista de &amp;Máquinas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="531"/>
        <source>Machine &amp;info</source>
        <translation>&amp;Informação da máquina</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="532"/>
        <location filename="../../qmc2main.cpp" line="533"/>
        <source>Detailed machine info</source>
        <translation>Informação detalhada da máquina</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="534"/>
        <location filename="../../qmc2main.cpp" line="535"/>
        <source>Favorite machines</source>
        <translation>Máquinas favoritas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="536"/>
        <location filename="../../qmc2main.cpp" line="537"/>
        <source>Machines last played</source>
        <translation>Máquinas jogadas ultimamente</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="556"/>
        <source>Machine / Notifier</source>
        <translation>Máquina / Notificador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="538"/>
        <location filename="../../qmc2main.cpp" line="539"/>
        <source>Play current machine</source>
        <translation>Jogar máquina atual</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="548"/>
        <location filename="../../qmc2main.cpp" line="549"/>
        <source>Play current machine (embedded)</source>
        <translation>Jogar máquina corrente (embutido)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="558"/>
        <location filename="../../qmc2main.cpp" line="559"/>
        <location filename="../../qmc2main.cpp" line="765"/>
        <location filename="../../qmc2main.cpp" line="817"/>
        <location filename="../../qmc2main.cpp" line="928"/>
        <source>Add current machine to favorites</source>
        <translation>Adicionar máquina atual à favoritos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="562"/>
        <location filename="../../qmc2main.cpp" line="563"/>
        <source>Reload entire machine list</source>
        <translation>Recarregar lista de máquinas completamente</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="564"/>
        <location filename="../../qmc2main.cpp" line="565"/>
        <source>View machine list with full detail</source>
        <translation>Ver lista de máquinas com todos os detalhes</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="570"/>
        <location filename="../../qmc2main.cpp" line="571"/>
        <location filename="../../qmc2main.cpp" line="775"/>
        <location filename="../../qmc2main.cpp" line="827"/>
        <location filename="../../qmc2main.cpp" line="870"/>
        <location filename="../../qmc2main.cpp" line="938"/>
        <source>Check current machine&apos;s ROM state</source>
        <translation>Verificar estado das ROMs da máquina atual</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="572"/>
        <location filename="../../qmc2main.cpp" line="573"/>
        <source>Analyse current machine with ROMAlyzer</source>
        <translation>Analisar máquina atual com ROMAlyzer</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="574"/>
        <source>M&amp;achine</source>
        <translation>&amp;Máquina</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="575"/>
        <source>Machine list with full detail (filtered)</source>
        <translation>Lista de máquinas com detalhes (filtrada)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="576"/>
        <location filename="../../qmc2main.cpp" line="577"/>
        <source>Select between detailed machine list and parent / clone hierarchy</source>
        <translation>Selecione entre lista de máquina detalhada e / hierarquia de clones</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="578"/>
        <location filename="../../qmc2main.cpp" line="579"/>
        <location filename="../../qmc2main.cpp" line="584"/>
        <source>Machine status indicator</source>
        <translation>Indicador de estado de máquina</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="582"/>
        <location filename="../../qmc2main.cpp" line="583"/>
        <source>Progress indicator for machine list processing</source>
        <translation>Indicador de progresso para o processamento de lista de máquinas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="585"/>
        <source>Show vertical machine status indicator in machine details</source>
        <translation>Mostrar indicador vertical de estado de máquina nos detalhes de máquina</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="586"/>
        <source>Show the machine status indicator only when the machine list is not visible due to the current layout</source>
        <translation>Mostrar o indicador vertical de estado de máquina somente quando a lista de máquinas não é visível devido ao layout atual</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="587"/>
        <source>Show machine name</source>
        <translation>Mostrar nome da máquina</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="588"/>
        <source>Show machine&apos;s description at the bottom of any images</source>
        <translation>Mostrar descrição da máquina abaixo de qualquer imagem</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="589"/>
        <source>Show machine&apos;s description only when the machine list is not visible due to the current layout</source>
        <translation>Mostrar descrição de máquina somente quando a lista de máquinas não é visível devido ao layout atual</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Valor</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="590"/>
        <location filename="../../qmc2main.cpp" line="591"/>
        <source>Loading machine list, please wait...</source>
        <translation>Carregando lista de máquinas, por favor espere...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="592"/>
        <source>Search for machines (not case-sensitive)</source>
        <translation>Procurar por máquinas (não sensível à caixa)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="593"/>
        <source>Search for machines</source>
        <translation>Procurar por máquinas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="644"/>
        <source>restoring main widget layout</source>
        <translation>restaurando layout do widget principal</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="715"/>
        <source>Embed emulator widget</source>
        <translation>Embutir widget do emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="716"/>
        <source>&amp;Embed</source>
        <translation>&amp;Embutir</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="722"/>
        <source>Terminate selected emulator(s) (sends TERM signal to emulator process(es))</source>
        <translation>Terminando emulador(res) selecionado(s) (envia sinal TERM para o(s) processo(s) do emulador)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="723"/>
        <source>&amp;Terminate</source>
        <translation>&amp;Terminar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="727"/>
        <source>Kill selected emulator(s) (sends KILL signal to emulator process(es))</source>
        <translation>Matar emulador(res) selecionado(s) (envia sinal KILL para o(s) processo(s) do emulador)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="728"/>
        <source>&amp;Kill</source>
        <translation>&amp;Matar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="733"/>
        <location filename="../../qmc2main.cpp" line="4196"/>
        <source>Copy emulator command line to clipboard</source>
        <translation>Copiar linha de comando do emulador para a área de transferência</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="734"/>
        <location filename="../../qmc2main.cpp" line="4197"/>
        <source>&amp;Copy command</source>
        <translation>&amp;Copiar comando</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="741"/>
        <location filename="../../qmc2main.cpp" line="793"/>
        <location filename="../../qmc2main.cpp" line="845"/>
        <location filename="../../qmc2main.cpp" line="904"/>
        <source>Play selected game</source>
        <translation>Jogar jogo selecionado</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="743"/>
        <location filename="../../qmc2main.cpp" line="795"/>
        <location filename="../../qmc2main.cpp" line="847"/>
        <location filename="../../qmc2main.cpp" line="906"/>
        <source>Start selected machine</source>
        <translation>Iniciar máquina selecionada</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2508"/>
        <location filename="../../qmc2main.cpp" line="745"/>
        <location filename="../../qmc2main.cpp" line="797"/>
        <location filename="../../qmc2main.cpp" line="849"/>
        <location filename="../../qmc2main.cpp" line="908"/>
        <source>&amp;Play</source>
        <translation>&amp;Jogar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="752"/>
        <location filename="../../qmc2main.cpp" line="804"/>
        <location filename="../../qmc2main.cpp" line="856"/>
        <location filename="../../qmc2main.cpp" line="915"/>
        <source>Play selected game (embedded)</source>
        <translation>Jogar jogo selecionado (embutido)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="754"/>
        <location filename="../../qmc2main.cpp" line="806"/>
        <location filename="../../qmc2main.cpp" line="858"/>
        <location filename="../../qmc2main.cpp" line="917"/>
        <source>Start selected machine (embedded)</source>
        <translation>Iniciar máquina selecionada (embutido)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3292"/>
        <location filename="../../qmc2main.cpp" line="756"/>
        <location filename="../../qmc2main.cpp" line="808"/>
        <location filename="../../qmc2main.cpp" line="860"/>
        <location filename="../../qmc2main.cpp" line="919"/>
        <source>Play &amp;embedded</source>
        <translation>Jogar &amp;embutido</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2692"/>
        <location filename="../../qmc2main.ui" line="2695"/>
        <location filename="../../qmc2main.cpp" line="763"/>
        <location filename="../../qmc2main.cpp" line="815"/>
        <location filename="../../qmc2main.cpp" line="926"/>
        <source>Add current game to favorites</source>
        <translation>Adicionar jogo atual para os favoritos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2689"/>
        <location filename="../../qmc2main.cpp" line="767"/>
        <location filename="../../qmc2main.cpp" line="819"/>
        <location filename="../../qmc2main.cpp" line="930"/>
        <location filename="../../qmc2main.cpp" line="4180"/>
        <source>To &amp;favorites</source>
        <translation>Para &amp;favoritos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2814"/>
        <location filename="../../qmc2main.ui" line="2817"/>
        <location filename="../../qmc2main.cpp" line="773"/>
        <location filename="../../qmc2main.cpp" line="825"/>
        <location filename="../../qmc2main.cpp" line="868"/>
        <location filename="../../qmc2main.cpp" line="936"/>
        <source>Check current game&apos;s ROM state</source>
        <translation>Verificar estado das ROM do jogo atual</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="777"/>
        <location filename="../../qmc2main.cpp" line="829"/>
        <location filename="../../qmc2main.cpp" line="872"/>
        <location filename="../../qmc2main.cpp" line="940"/>
        <source>Check &amp;ROM state</source>
        <translation>Verificar estado da &amp;ROM</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="782"/>
        <location filename="../../qmc2main.cpp" line="834"/>
        <location filename="../../qmc2main.cpp" line="877"/>
        <location filename="../../qmc2main.cpp" line="945"/>
        <source>Analyse current game&apos;s ROM set with ROMAlyzer</source>
        <translation>Analisar conjunto de ROM do jogo atual com ROMAlyzer</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="784"/>
        <location filename="../../qmc2main.cpp" line="836"/>
        <location filename="../../qmc2main.cpp" line="879"/>
        <location filename="../../qmc2main.cpp" line="947"/>
        <source>Analyse current machine&apos;s ROM set with ROMAlyzer</source>
        <translation>Analisar conjunto de ROM da máquina atual com ROMAlyzer</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="786"/>
        <location filename="../../qmc2main.cpp" line="838"/>
        <location filename="../../qmc2main.cpp" line="881"/>
        <location filename="../../qmc2main.cpp" line="949"/>
        <source>&amp;Analyse ROM...</source>
        <translation>&amp;Analisar ROM...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="886"/>
        <source>Remove from favorites</source>
        <translation>Remover dos favoritos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="887"/>
        <location filename="../../qmc2main.cpp" line="955"/>
        <source>&amp;Remove</source>
        <translation>&amp;Remover</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="891"/>
        <source>Clear all favorites</source>
        <translation>Limpar favoritos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="892"/>
        <location filename="../../qmc2main.cpp" line="960"/>
        <source>&amp;Clear</source>
        <translation>&amp;Limpar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="896"/>
        <source>Save favorites now</source>
        <translation>Salvar favoritos agora</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="897"/>
        <location filename="../../qmc2main.cpp" line="965"/>
        <source>&amp;Save</source>
        <translation>&amp;Salvar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="954"/>
        <source>Remove from played</source>
        <translation>Remover dos jogados</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="959"/>
        <source>Clear all played</source>
        <translation>Limpar todos os jogados</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="964"/>
        <source>Save play-history now</source>
        <translation>Salvar histórico de jogadas agora</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="972"/>
        <location filename="../../qmc2main.cpp" line="994"/>
        <location filename="../../qmc2main.cpp" line="1022"/>
        <source>Set tab position north</source>
        <translation>Definir posição da aba ao norte</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="973"/>
        <location filename="../../qmc2main.cpp" line="995"/>
        <location filename="../../qmc2main.cpp" line="1023"/>
        <source>&amp;North</source>
        <translation>&amp;Norte</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="977"/>
        <location filename="../../qmc2main.cpp" line="999"/>
        <location filename="../../qmc2main.cpp" line="1027"/>
        <source>Set tab position south</source>
        <translation>Definir posição da aba ao sul</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="978"/>
        <location filename="../../qmc2main.cpp" line="1000"/>
        <location filename="../../qmc2main.cpp" line="1028"/>
        <source>&amp;South</source>
        <translation>&amp;Sul</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="982"/>
        <location filename="../../qmc2main.cpp" line="1004"/>
        <location filename="../../qmc2main.cpp" line="1032"/>
        <source>Set tab position west</source>
        <translation>Definir posição da aba ao Oeste</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="983"/>
        <location filename="../../qmc2main.cpp" line="1005"/>
        <location filename="../../qmc2main.cpp" line="1033"/>
        <source>&amp;West</source>
        <translation>&amp;Oeste</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="987"/>
        <location filename="../../qmc2main.cpp" line="1009"/>
        <location filename="../../qmc2main.cpp" line="1037"/>
        <source>Set tab position east</source>
        <translation>Definir posição da aba ao Leste</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="988"/>
        <location filename="../../qmc2main.cpp" line="1010"/>
        <location filename="../../qmc2main.cpp" line="1038"/>
        <source>&amp;East</source>
        <translation>&amp;Leste</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1015"/>
        <source>Detail setup</source>
        <translation>Configuração detalhada</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2904"/>
        <location filename="../../qmc2main.cpp" line="1016"/>
        <source>&amp;Setup...</source>
        <translation>&amp;Configurar...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1045"/>
        <location filename="../../qmc2main.cpp" line="1057"/>
        <source>Flip splitter orientation</source>
        <translation>Mudar orientação do divisor</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1046"/>
        <location filename="../../qmc2main.cpp" line="1058"/>
        <source>&amp;Flip splitter orientation</source>
        <translation>&amp;Mudar orientação do divisor</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1050"/>
        <source>Swap splitter&apos;s sub-layouts</source>
        <translation>Trocar sub-layouts do divisor</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1051"/>
        <source>&amp;Swap splitter&apos;s sub-layouts</source>
        <translation>&amp;Trocar sub-layouts do divisor</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1062"/>
        <source>Swap splitter&apos;s sub-widgets</source>
        <translation>Trocar sub-widgets do divisor</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1063"/>
        <source>&amp;Swap splitter&apos;s sub-widgets</source>
        <translation>&amp;Trocar sub-widgets do divisor</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1282"/>
        <source>&amp;Correct</source>
        <translation>&amp;Correto</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1288"/>
        <source>&amp;Mostly correct</source>
        <translation>&amp;Maioria correto</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1294"/>
        <source>&amp;Incorrect</source>
        <translation>&amp;Incorreto</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1300"/>
        <source>&amp;Not found</source>
        <translation>&amp;Não encontrado</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1306"/>
        <source>&amp;Unknown</source>
        <translation>&amp;Desconhecido</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1562"/>
        <source>No devices</source>
        <translation>Sem dispositivos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="209"/>
        <location filename="../../qmc2main.ui" line="341"/>
        <location filename="../../qmc2main.ui" line="476"/>
        <location filename="../../qmc2main.ui" line="611"/>
        <location filename="../../qmc2main.cpp" line="1675"/>
        <location filename="../../qmc2main.cpp" line="1969"/>
        <location filename="../../qmc2main.cpp" line="1993"/>
        <location filename="../../qmc2main.cpp" line="3124"/>
        <location filename="../../qmc2main.cpp" line="3321"/>
        <location filename="../../qmc2main.cpp" line="3922"/>
        <location filename="../../qmc2main.cpp" line="4030"/>
        <location filename="../../qmc2main.cpp" line="4646"/>
        <location filename="../../qmc2main.cpp" line="4662"/>
        <location filename="../../qmc2main.cpp" line="5618"/>
        <location filename="../../qmc2main.cpp" line="5639"/>
        <location filename="../../qmc2main.cpp" line="7506"/>
        <location filename="../../qmc2main.cpp" line="7523"/>
        <location filename="../../qmc2main.cpp" line="7600"/>
        <location filename="../../qmc2main.cpp" line="7617"/>
        <source>Waiting for data...</source>
        <translation>Aguardando dados...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1693"/>
        <location filename="../../qmc2main.cpp" line="1775"/>
        <location filename="../../qmc2main.cpp" line="1797"/>
        <source>please wait for ROM state filter to finish and try again</source>
        <translation>por favor espere o filtro de estado de ROM terminar e tente novamente</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1698"/>
        <location filename="../../qmc2main.cpp" line="2107"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation>por favor aguarde a verificação de ROM terminar e tente novamente</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1703"/>
        <location filename="../../qmc2main.cpp" line="1781"/>
        <location filename="../../qmc2main.cpp" line="1803"/>
        <source>please wait for image check to finish and try again</source>
        <translation>por favor espere a verificação de imagem terminar e tente novamente</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1708"/>
        <location filename="../../qmc2main.cpp" line="1783"/>
        <location filename="../../qmc2main.cpp" line="1805"/>
        <source>please wait for sample check to finish and try again</source>
        <translation>por favor espere a verificação de exemplos terminar e tente novamente</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1713"/>
        <location filename="../../qmc2main.cpp" line="1785"/>
        <location filename="../../qmc2main.cpp" line="1807"/>
        <location filename="../../qmc2main.cpp" line="1997"/>
        <source>please wait for ROMAlyzer to finish the current analysis and try again</source>
        <translation>por favor espere o ROMAlyzer terminar a análise atual e tente novamente</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1719"/>
        <source>game list reload is already active</source>
        <translation>recarregamento da lista de jogos já está ativa</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1721"/>
        <source>machine list reload is already active</source>
        <translation>recarregamento da lista de máquinas já está ativa</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1741"/>
        <location filename="../../qmc2main.cpp" line="5111"/>
        <source>saving game selection</source>
        <translation>salvando seleção de jogo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1777"/>
        <location filename="../../qmc2main.cpp" line="1799"/>
        <source>ROM verification already active</source>
        <translation>Verificação de ROM já ativa</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1779"/>
        <location filename="../../qmc2main.cpp" line="1801"/>
        <location filename="../../qmc2main.cpp" line="2103"/>
        <location filename="../../qmc2main.cpp" line="2142"/>
        <location filename="../../qmc2main.cpp" line="2179"/>
        <location filename="../../qmc2main.cpp" line="2211"/>
        <location filename="../../qmc2main.cpp" line="3913"/>
        <source>please wait for reload to finish and try again</source>
        <translation>por favor aguarde finalizar o recarregamento e tente novamente</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1811"/>
        <location filename="../../qmc2main.cpp" line="4524"/>
        <location filename="../../qmc2main.cpp" line="4569"/>
        <location filename="../../qmc2main.cpp" line="4993"/>
        <location filename="../../qmc2main.cpp" line="5008"/>
        <location filename="../../qmc2main.cpp" line="5039"/>
        <source>Confirm</source>
        <translation>Confirmar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1812"/>
        <source>The ROM verification process may be very time-consuming.
It will overwrite existing cached data.

Do you really want to check all ROM states now?</source>
        <translation>O processo de verificação de ROM pode consumir muito tempo.
Ele vai sobrescrever dados de cache existente.

Você realmente quer verificar o estado de todas as ROMs agora?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1823"/>
        <source>automatic ROM check triggered</source>
        <translation>verificação automática de ROM ativado</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2025"/>
        <source>image cache cleared</source>
        <translation>cache de imagens limpo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2036"/>
        <source>icon cache cleared</source>
        <translation>cache de ícones limpo</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2046"/>
        <source>freed %n byte(s) in %1</source>
        <translation>
            <numerusform>liberado %n byte em %1</numerusform>
            <numerusform>liberados %n bytes em %1</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2046"/>
        <source>%n entry(s)</source>
        <translation>
            <numerusform>%n entrada</numerusform>
            <numerusform>%n entradas</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2048"/>
        <source>MAWS in-memory cache cleared (%1)</source>
        <translation>Cache em memória do MAWS limpo (%1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2064"/>
        <location filename="../../qmc2main.cpp" line="2091"/>
        <source>removed %n byte(s) in %1</source>
        <translation>
            <numerusform>removido %n byte em %1</numerusform>
            <numerusform>removidos %n bytes em %1</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="2064"/>
        <location filename="../../qmc2main.cpp" line="2091"/>
        <source>%n file(s)</source>
        <translation>
            <numerusform>%n arquivo</numerusform>
            <numerusform>%n arquivos</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2065"/>
        <source>MAWS on-disk cache cleared (%1)</source>
        <translation>Cache em disco do MAWS limpo (%1)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2092"/>
        <source>YouTube on-disk cache cleared (%1)</source>
        <translation>Cache em disco do YouTube limpo (%1)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2451"/>
        <location filename="../../qmc2main.cpp" line="2453"/>
        <location filename="../../qmc2main.cpp" line="2552"/>
        <location filename="../../qmc2main.cpp" line="2554"/>
        <source>variant &apos;%1&apos; launched</source>
        <translation>variação &apos;%1&apos; iniciado</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2491"/>
        <location filename="../../qmc2main.cpp" line="2493"/>
        <location filename="../../qmc2main.cpp" line="2592"/>
        <location filename="../../qmc2main.cpp" line="2594"/>
        <source>WARNING: failed to launch variant &apos;%1&apos;</source>
        <translation>AVISO: falha ao iniciar variação &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2574"/>
        <location filename="../../qmc2main.cpp" line="2646"/>
        <source>About Qt</source>
        <translation>Sobre o Qt</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2655"/>
        <location filename="../../qmc2main.cpp" line="2674"/>
        <source>WARNING: this feature is not yet working!</source>
        <translation>AVISO: este recurso ainda não está funcionando!</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2850"/>
        <source>ERROR: no match found (?)</source>
        <translation>ERRO: nenhuma ocorrência encontrada (?)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3464"/>
        <source>MAWS page for &apos;%1&apos;</source>
        <translation>Página do MAWS para &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3502"/>
        <source>Fetching MAWS page for &apos;%1&apos;, please wait...</source>
        <translation>Baixando página do MAWS para &apos;%1&apos;, por favore espere...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3552"/>
        <source>Emulator for this game</source>
        <translation>Emulador para este jogo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3554"/>
        <source>Emulator for this machine</source>
        <translation>Emulador para esta máquina</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3595"/>
        <source>Export to...</source>
        <translation>Exportar para...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3617"/>
        <location filename="../../qmc2main.cpp" line="3621"/>
        <source>&lt;inipath&gt;/%1.ini</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3618"/>
        <location filename="../../qmc2main.cpp" line="3622"/>
        <source>Select file...</source>
        <translation>Selecionar arquivo...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3697"/>
        <location filename="../../qmc2main.cpp" line="3704"/>
        <source>&lt;p&gt;No data available&lt;/p&gt;</source>
        <translation>&lt;b&gt;Nenhum dado disponível&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3699"/>
        <location filename="../../qmc2main.cpp" line="3706"/>
        <location filename="../../qmc2main.cpp" line="3741"/>
        <location filename="../../qmc2main.cpp" line="3744"/>
        <source>No data available</source>
        <translation>Nenhum dado disponível</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="789"/>
        <location filename="../../qmc2main.cpp" line="4155"/>
        <source>Embedded emulators</source>
        <translation>Emuladores embutidos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4165"/>
        <source>Release emulator</source>
        <translation>Soltar emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4212"/>
        <source>WARNING: no matching window for emulator #%1 found</source>
        <translation>AVISO: nenhuma janela para o emulador #%1 encontrada</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4218"/>
        <source>Embedding failed</source>
        <translation>Embutir falhou</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4243"/>
        <location filename="../../qmc2main.cpp" line="4244"/>
        <source>Scanning pause key</source>
        <translation>Escaneando tecla de pausa</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4525"/>
        <source>Are you sure you want to clear the favorites list?</source>
        <translation>Tem certeza que deseja limpar a lista de favoritos?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4570"/>
        <source>Are you sure you want to clear the play history?</source>
        <translation>Tem certeza que deseja limpar o histórico de jogadas?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4763"/>
        <location filename="../../qmc2main.cpp" line="4822"/>
        <source>Choose export file</source>
        <translation>Escolha o arquivo de exportação</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4763"/>
        <location filename="../../qmc2main.cpp" line="4790"/>
        <location filename="../../qmc2main.cpp" line="4822"/>
        <location filename="../../qmc2main.cpp" line="4858"/>
        <location filename="../../qmc2main.cpp" line="6383"/>
        <location filename="../../qmc2main.cpp" line="8113"/>
        <location filename="../../qmc2main.cpp" line="8155"/>
        <source>All files (*)</source>
        <translation>Todos os arquivos (*)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4790"/>
        <location filename="../../qmc2main.cpp" line="4858"/>
        <source>Choose import file</source>
        <translation>Escolha o arquivo de importação</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4820"/>
        <location filename="../../qmc2main.cpp" line="4855"/>
        <source>WARNING: invalid inipath</source>
        <translation>AVISO: inipath inválido</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4987"/>
        <source>stopping current processing upon user request</source>
        <translation>parando processamento atual devido à requisição do usuário</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4994"/>
        <source>Your configuration changes have not been applied yet.
Really quit?</source>
        <translation>Suas mudanças ainda não foram aplicadas.
Sair realmente?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5009"/>
        <source>There are one or more emulators still running.
Should they be killed on exit?</source>
        <translation>Existem um ou mais emuladores em execução.
Eles devem ser mortos ao sair?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5040"/>
        <source>There are one or more running downloads. Quit anyway?</source>
        <translation>Existem um ou mais downloads em execução.
Sair mesmo assim?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5056"/>
        <source>cleaning up</source>
        <translation>limpando</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5059"/>
        <source>aborting running downloads</source>
        <translation>abortando downloads em execução</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5126"/>
        <source>saving main widget layout</source>
        <translation>salvando layout do widget principal</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5196"/>
        <source>destroying arcade view</source>
        <translation>destruindo view de arcade</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5201"/>
        <source>destroying arcade setup dialog</source>
        <translation>destruindo janela de configuração de arcade</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5213"/>
        <source>saving current game&apos;s favorite software</source>
        <translation>salvando software favorito do jogo atual</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5224"/>
        <source>saving current machine&apos;s device configurations</source>
        <translation>salvando configurações de dispositivos da máquina atual</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5219"/>
        <source>saving current machine&apos;s favorite software</source>
        <translation>salvando softwares favoritos da máquina atual</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="542"/>
        <location filename="../../qmc2main.cpp" line="543"/>
        <source>Clear machine list cache</source>
        <translation>Limpar cache de lista da máquina</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="544"/>
        <location filename="../../qmc2main.cpp" line="545"/>
        <source>Forcedly clear (remove) the machine list cache</source>
        <translation>Forçar limpar (remover) o cache da lista de máquina</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2124"/>
        <source>ROM state cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation>Arquivo de cache &apos;%1&apos; do estado da ROM forçadamente removido devido à requisição do usuário</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2126"/>
        <source>WARNING: cannot remove the ROM state cache file &apos;%1&apos;, please check permissions</source>
        <translation>AVISO: impossível remover o arquivo de cache &apos;%1&apos; do estado da ROM, por favor cheque as permissões</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2160"/>
        <source>game list cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation>arquivo de cache &apos;%1&apos; de lista de jogos forçadamente removido devido à requisição do usuário</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2162"/>
        <source>WARNING: cannot remove the game list cache file &apos;%1&apos;, please check permissions</source>
        <translation>AVISO: impossível remover o arquivo de cache &apos;%1&apos; da lista de jogos, por favor cheque as permissões</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2165"/>
        <source>machine list cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation>arquivo de cache &apos;%1&apos; de lista de máquinas forçadamente removido devido à requisição do usuário</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2167"/>
        <source>WARNING: cannot remove the machine list cache file &apos;%1&apos;, please check permissions</source>
        <translation>AVISO: impossível remover o arquivo de cache &apos;%1&apos; da lista de máquinas, por favor cheque as permissões</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2196"/>
        <source>XML cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation>Arquivo de cache XML &apos;%1&apos; forçadamente removido devido à requisição do usuário</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2198"/>
        <source>WARNING: cannot remove the XML cache file &apos;%1&apos;, please check permissions</source>
        <translation>AVISO: impossível remover arquivo de cache XML &apos;%1&apos;, por favor cheque as permissões</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5071"/>
        <source>saving YouTube video info map</source>
        <translation>salvando mapa de informação de vídeo do YouTube</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5094"/>
        <source>done (saving YouTube video info map)</source>
        <translation>feito (salvando mapa de informação de vídeo do YouTube)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5096"/>
        <location filename="../../qmc2main.cpp" line="5098"/>
        <source>failed (saving YouTube video info map)</source>
        <translation>falhou (salvando mapa de informação de vídeo do YouTube)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5232"/>
        <source>destroying current game&apos;s emulator configuration</source>
        <translation>destruindo configuração do emulador do jogo atual</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5234"/>
        <source>destroying current machine&apos;s emulator configuration</source>
        <translation>destruindo configuração do emulador da máquina atual</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5244"/>
        <source>destroying global emulator options</source>
        <translation>destruindo opções globais do emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5250"/>
        <source>destroying game list</source>
        <translation>destruindo lista de jogos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5252"/>
        <source>destroying machine list</source>
        <translation>destruindo lista de máquinas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5278"/>
        <source>destroying preview</source>
        <translation>destruindo previews</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5282"/>
        <source>destroying flyer</source>
        <translation>destruindo flyer</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5286"/>
        <source>destroying cabinet</source>
        <translation>destruindo gabinete</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5290"/>
        <source>destroying controller</source>
        <translation>destruindo controle</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5294"/>
        <source>destroying marquee</source>
        <translation>destruindo marquee</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5298"/>
        <source>destroying title</source>
        <translation>destruindo título</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5302"/>
        <source>destroying PCB</source>
        <translation>destruindo PCB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5306"/>
        <source>destroying about dialog</source>
        <translation>destruindo sobre</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5310"/>
        <source>destroying MiniWebBrowser</source>
        <translation>destruindo Mini Navegador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5315"/>
        <source>destroying MAWS lookup</source>
        <translation>destruindo MAWS lookup</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5319"/>
        <source>destroying MAWS quick download setup</source>
        <translation>destruindo configuração do download rápido do MAWS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5325"/>
        <source>destroying image checker</source>
        <translation>destruindo verificador de imagem</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5330"/>
        <source>destroying sample checker</source>
        <translation>destruindo verificador de exemplos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5335"/>
        <source>destroying ROMAlyzer</source>
        <translation>destruindo ROMAlyzer</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5340"/>
        <source>destroying ROM status exporter</source>
        <translation>destruindo explorador de estado de ROM</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5345"/>
        <source>destroying detail setup</source>
        <translation>destruindo configuração detalhada</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5351"/>
        <source>destroying demo mode dialog</source>
        <translation>destruindo janela de modo demonstração</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5257"/>
        <source>disconnecting audio source from audio sink</source>
        <translation>desconectando fonte de áudio do sumidouro de áudio</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="521"/>
        <source>Game</source>
        <translation>Jogo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="540"/>
        <location filename="../../qmc2main.cpp" line="541"/>
        <source>Play all tagged machines</source>
        <translation>Jogar todas as máquinas etiquetadas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="546"/>
        <source>List of all supported machines</source>
        <translation>Lista de todas as máquinas suportadas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="550"/>
        <location filename="../../qmc2main.cpp" line="551"/>
        <source>Play all tagged machines (embedded)</source>
        <translation>Jogar todas as máquinas etiquetadas (embutido)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="554"/>
        <source>Machine</source>
        <translation>Máquina</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="560"/>
        <location filename="../../qmc2main.cpp" line="561"/>
        <source>Add all tagged machines to favorites</source>
        <translation>Adicionar todas as máquinas etiquetadas aos favoritos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1216"/>
        <source>Enter search string</source>
        <translation>Entre com o texto de busca</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1392"/>
        <source>sorry, devices cannot run standalone</source>
        <translation>desculpe, dispositivos não podem ser executados sozinhos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="1544"/>
        <source>No devices available</source>
        <translation>Nenhum dispositivo disponível</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2131"/>
        <source>triggering an automatic ROM check on next reload</source>
        <translation>ativando uma verificação de ROM automática na próxima execução</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2228"/>
        <source>software list cache file &apos;%1&apos; forcedly removed upon user request</source>
        <translation>arquivo de cache &apos;%1&apos; de lista de software forçadamente removido devido à requisição do usuário</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="2230"/>
        <source>WARNING: cannot remove the software list cache file &apos;%1&apos;, please check permissions</source>
        <translation>AVISO: impossível remover o arquivo de cache &apos;%1&apos; da lista de software, por favor cheque as permissões</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5270"/>
        <source>destroying YouTube video widget</source>
        <translation>destruindo widget de vídeos do YouTube</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5359"/>
        <source>destroying game info DB</source>
        <translation>destruindo BD de informação de jogos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5361"/>
        <source>destroying machine info DB</source>
        <translation>destruindo BD de informação de máquinas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5377"/>
        <source>destroying emulator info DB</source>
        <translation>destruindo BD de informação de emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5392"/>
        <source>destroying process manager</source>
        <translation>destruindo gerenciador de processo</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5395"/>
        <source>killing %n running emulator(s) on exit</source>
        <translation>
            <numerusform>matando %n emulador em execução na saída</numerusform>
            <numerusform>matando %n emuladores em execução na saída</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5398"/>
        <source>keeping %n running emulator(s) alive</source>
        <translation>
            <numerusform>mantendo %n emulador em execução vivo</numerusform>
            <numerusform>mantendo %n emuladores em execução vivos</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5418"/>
        <source>destroying network access manager</source>
        <translation>destruindo gerenciador de acesso à rede</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5422"/>
        <source>so long and thanks for all the fish</source>
        <translation>até logo e obrigado pelos peixes</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5554"/>
        <source>loading style sheet &apos;%1&apos;</source>
        <translation>carregando estilos &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5563"/>
        <source>FATAL: can&apos;t open style sheet file &apos;%1&apos;, please check</source>
        <translation>FATAL: impossível carregar arquivo de estilos &apos;%1&apos;, por favor verifique</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5566"/>
        <source>removing current style sheet</source>
        <translation>removendo estilo atual</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5745"/>
        <source>loading YouTube video info map</source>
        <translation>carregando mapa de informação de vídeo do YouTube</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5762"/>
        <source>YouTube index - %p%</source>
        <translation>Índice do YouTube - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5791"/>
        <source>done (loading YouTube video info map)</source>
        <translation>feito (carregando mapa de informação de vídeo do YouTube)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5792"/>
        <source>%n video info record(s) loaded</source>
        <translation>
            <numerusform>%n registro de informação de vídeo carregado</numerusform>
            <numerusform>%n registros de informação de vídeo carregados</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5809"/>
        <source>loading game info DB</source>
        <translation>carregando BD de informações de jogos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5811"/>
        <source>loading machine info DB</source>
        <translation>carregando BD de informações de máquinas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5918"/>
        <source>WARNING: missing &apos;$end&apos; in game info DB %1</source>
        <translation>AVISO: faltando &apos;$end&apos; no BD de informações de jogos %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5920"/>
        <source>WARNING: missing &apos;$end&apos; in machine info DB %1</source>
        <translation>AVISO: faltando &apos;$end&apos; no BD de informações de máquinas %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5925"/>
        <source>WARNING: missing &apos;$bio&apos; in game info DB %1</source>
        <translation>AVISO: faltando &apos;$bio&apos; no BD de informações de jogos %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5927"/>
        <source>WARNING: missing &apos;$bio&apos; in machine info DB %1</source>
        <translation>AVISO: faltando &apos;$bio&apos; no BD de informações de máquinas %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5932"/>
        <source>WARNING: missing &apos;$info&apos; in game info DB %1</source>
        <translation>AVISO: faltando &apos;$info&apos; no BD de informações de jogos %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5934"/>
        <source>WARNING: missing &apos;$info&apos; in machine info DB %1</source>
        <translation>AVISO: faltando &apos;$info&apos; no BD de informações de máquinas %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5946"/>
        <source>WARNING: can&apos;t open game info DB %1</source>
        <translation>AVISO: impossível abrir BD de informações de jogos %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5948"/>
        <source>WARNING: can&apos;t open machine info DB %1</source>
        <translation>AVISO: impossível abrir BD de informações de máquinas %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5954"/>
        <source>done (loading game info DB, elapsed time = %1)</source>
        <translation>feito (carregando BD de informações de jogos, tempo = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5955"/>
        <source>%n game info record(s) loaded</source>
        <translation>
            <numerusform>%n registro de informação de jogo carregado</numerusform>
            <numerusform>%n registros de informações de jogos carregados</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5957"/>
        <source>invalidating game info DB</source>
        <translation>invalidando BD de informações de jogos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5959"/>
        <source>done (loading machine info DB, elapsed time = %1)</source>
        <translation>feito (carregando BD de informações de máquinas, tempo = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="5960"/>
        <source>%n machine info record(s) loaded</source>
        <translation>
            <numerusform>%n registro de informação de máquina carregado</numerusform>
            <numerusform>%n registros de informações de máquinas carregados</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5962"/>
        <source>invalidating machine info DB</source>
        <translation>invalidando BD de informações de máquinas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5992"/>
        <source>loading emulator info DB</source>
        <translation>carregando BD de informações de emuladores</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6076"/>
        <source>WARNING: missing &apos;$end&apos; in emulator info DB %1</source>
        <translation>AVISO: faltando &apos;$end&apos; no BD de informações de emuladores %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6079"/>
        <source>WARNING: missing &apos;$mame&apos; in emulator info DB %1</source>
        <translation>AVISO: faltando &apos;$mame&apos; no BD de informações de emuladores %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6082"/>
        <source>WARNING: missing &apos;$info&apos; in emulator info DB %1</source>
        <translation>AVISO: faltando &apos;$info&apos; no BD de informações de emuladores %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6092"/>
        <source>WARNING: can&apos;t open emulator info DB %1</source>
        <translation>AVISO: impossível abrir BD de informações de emuladores %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6095"/>
        <source>done (loading emulator info DB, elapsed time = %1)</source>
        <translation>feito (carregando BD de informações de emuladores, tempo = %1)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="6096"/>
        <source>%n emulator info record(s) loaded</source>
        <translation>
            <numerusform>%n registro de informação de emulador carregado</numerusform>
            <numerusform>%n registros de informação de emuladores carregados</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6098"/>
        <source>invalidating emulator info DB</source>
        <translation>invalidando BD de informações de emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6383"/>
        <source>Select one or more audio files</source>
        <translation>Selecione um ou mais arquivos de audio</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6395"/>
        <source>Add URL</source>
        <translation>Adicionar URL</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6395"/>
        <source>Enter valid MP3 stream URL:</source>
        <translation>Entre com uma URL de stream MP3 válido:</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6682"/>
        <source>audio player: track info: title = &apos;%1&apos;, artist = &apos;%2&apos;, album = &apos;%3&apos;, genre = &apos;%4&apos;</source>
        <translation>reprodutor de músicas: informação da faixa: título = &apos;%1&apos;, artista = &apos;%2&apos;, album = &apos;%3&apos;, gênero = &apos;%4&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6696"/>
        <source>Buffering %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6761"/>
        <source>WARNING: can&apos;t create SDLMAME output notifier FIFO, path = %1</source>
        <translation>AVISO: impossível criar notificador FIFO de saída do SDLMAME, caminho = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6773"/>
        <source>WARNING: can&apos;t create SDLMESS output notifier FIFO, path = %1</source>
        <translation>AVISO: impossível criar notificador FIFO de saída do SDLMESS, caminho = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6789"/>
        <source>SDLMAME output notifier FIFO created</source>
        <translation>cridor notificador FIFO de saída do SDLMAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6793"/>
        <location filename="../../qmc2main.cpp" line="6796"/>
        <source>WARNING: can&apos;t open SDLMAME output notifier FIFO for reading, path = %1</source>
        <translation>AVISO: impossível abrir notificador FIFO de saída do SDLMAME para leitura, caminho = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6800"/>
        <source>SDLMESS output notifier FIFO created</source>
        <translation>criado notificador FIFO de saída do SDLMESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6804"/>
        <location filename="../../qmc2main.cpp" line="6807"/>
        <source>WARNING: can&apos;t open SDLMESS output notifier FIFO for reading, path = %1</source>
        <translation>AVISO: impossível abrir notificador FIFO de saída do SDLMESS para leitura, caminho = %1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6884"/>
        <location filename="../../qmc2main.cpp" line="6951"/>
        <location filename="../../qmc2main.cpp" line="6961"/>
        <source>running</source>
        <translation>executando</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6900"/>
        <source>stopped</source>
        <translation>parado</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6917"/>
        <location filename="../../qmc2main.cpp" line="6988"/>
        <source>unhandled MAME output notification: game = %1, class = %2, what = %3, state = %4</source>
        <translation>notificação de saída do MAME não tratado: jogo = %1, classe = %2, o que = %3, estado = %4</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6919"/>
        <location filename="../../qmc2main.cpp" line="6990"/>
        <source>unhandled MESS output notification: game = %1, class = %2, what = %3, state = %4</source>
        <translation>notificação de saída do MESS não tratado: jogo = %1, classe = %2, o que = %3, estado = %4</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8652"/>
        <location filename="../../qmc2main.cpp" line="8686"/>
        <source>Play tagged - %p%</source>
        <translation>Jogar etiquetados - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8722"/>
        <source>Add favorites - %p%</source>
        <translation>Adicionar aos favoritos - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8755"/>
        <location filename="../../qmc2main.cpp" line="8783"/>
        <location filename="../../qmc2main.cpp" line="8823"/>
        <source>please wait for current activity to finish and try again (this batch-mode operation can only run exclusively)</source>
        <translation>por favor espere a atividade atual terminar e tente novamente (essa opração em lote só pode ser executada exclusivamente)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8829"/>
        <source>ROM tool tagged - %p%</source>
        <translation>Ferramenta de ROM com etiquetados - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8963"/>
        <source>Tag - %p%</source>
        <translation>Etiquetar - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9005"/>
        <source>Untag - %p%</source>
        <translation>Remover Etiqueta - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9047"/>
        <source>Invert tag - %p%</source>
        <translation>Inverter etiqueta - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4157"/>
        <location filename="../../qmc2main.cpp" line="6945"/>
        <location filename="../../qmc2main.cpp" line="6959"/>
        <source>paused</source>
        <translation>pausado</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="467"/>
        <source>Toggle automatic pausing of embedded emulators (hold down for menu)</source>
        <translation>Ativar/desativar pausa automática de emuladores embutidos (segure pressionado para abrir o menu)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="476"/>
        <source>Scan the pause key used by the emulator</source>
        <translation>Escanear tecla de pausa usada pelo emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="477"/>
        <source>Scan pause key...</source>
        <translation>Escanear tecla de pausa...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7645"/>
        <source>ArcadeView is not currently active, can&apos;t take screen shot</source>
        <translation>ArcadeView não está ativa atualmente, impossivel tirar screenshot</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7790"/>
        <source>Quick download links for MAWS data usable by QMC2</source>
        <translation>Links de download rápido de dados do MAWS utilizáveis pelo QMC2</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="296"/>
        <location filename="../../qmc2main.ui" line="431"/>
        <location filename="../../qmc2main.ui" line="566"/>
        <location filename="../../qmc2main.cpp" line="1117"/>
        <location filename="../../qmc2main.cpp" line="1124"/>
        <location filename="../../qmc2main.cpp" line="1157"/>
        <location filename="../../qmc2main.cpp" line="1182"/>
        <location filename="../../qmc2main.cpp" line="7809"/>
        <source>Icon</source>
        <translation>Ícone</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7815"/>
        <source>Cabinet art</source>
        <translation>Arte do gabinete</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8067"/>
        <source>Cabinet</source>
        <translation>Gabinete</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8069"/>
        <source>Controller</source>
        <translation>Controle</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8075"/>
        <source>PCB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8071"/>
        <source>Flyer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8073"/>
        <source>Marquee</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7846"/>
        <source>No cabinet art</source>
        <translation>Sem arte do gabinete</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7858"/>
        <source>Previews</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7904"/>
        <location filename="../../qmc2main.cpp" line="8077"/>
        <source>preview</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7889"/>
        <source>No previews</source>
        <translation>Sem previews</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7911"/>
        <source>Titles</source>
        <translation>Títulos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7944"/>
        <location filename="../../qmc2main.cpp" line="8079"/>
        <source>title</source>
        <translation>título</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7929"/>
        <source>No titles</source>
        <translation>Sem títulos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4128"/>
        <source>FATAL: can&apos;t start XWININFO within a reasonable time frame, giving up</source>
        <translation>FATAL: impossível iniciar XWININFO dentro de um espaço de tempo razoável, desistindo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="7952"/>
        <source>Setup...</source>
        <translation>Configuração...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8113"/>
        <source>Choose file to store the icon</source>
        <translation>Escolha o arquivo para guardar o ícone</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8119"/>
        <source>icon image for &apos;%1&apos; stored as &apos;%2&apos;</source>
        <translation>imagem de ícone para &apos;%1&apos; salvo como &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8126"/>
        <source>FATAL: icon image for &apos;%1&apos; couldn&apos;t be stored as &apos;%2&apos;</source>
        <translation>FATAL: imagem de ícone para &apos;%1&apos; não pode ser salvo como &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8155"/>
        <source>Choose file to store download</source>
        <translation>Escolha o arquivo para salvar o download</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Physical memory:</source>
        <translation>Memória física:</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Total: %1 MB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Free: %1 MB</source>
        <translation>Livre: %1 MB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="8250"/>
        <source>Used: %1 MB</source>
        <translation>Usado: %1 MB</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="15"/>
        <location filename="../../macros.h" line="424"/>
        <location filename="../../macros.h" line="430"/>
        <source>M.A.M.E. Catalog / Launcher II</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="526"/>
        <source>Creating category view, please wait...</source>
        <translation>Criando visão de categoria. por favor espere...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="661"/>
        <source>Creating version view, please wait...</source>
        <translation>Criando visão de versão. por favor espere...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="711"/>
        <source>Search for games (not case-sensitive)</source>
        <translation>Buscar por jogos (não sensível à caixa)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="714"/>
        <source>Search for games</source>
        <translation>Busca por jogos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="55"/>
        <source>&amp;Game list</source>
        <translation>Lista de &amp;Jogos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="147"/>
        <source>List of all supported games</source>
        <translation>Lista de todos os jogos suportados</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="154"/>
        <location filename="../../qmc2main.cpp" line="1073"/>
        <source>Game / Attribute</source>
        <translation>Jogo / Atributo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="164"/>
        <location filename="../../qmc2main.cpp" line="1077"/>
        <location filename="../../qmc2main.cpp" line="1084"/>
        <source>Icon / Value</source>
        <translation>Ícone / Valor</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="169"/>
        <location filename="../../qmc2main.ui" line="301"/>
        <location filename="../../qmc2main.ui" line="436"/>
        <location filename="../../qmc2main.ui" line="571"/>
        <location filename="../../qmc2main.cpp" line="1087"/>
        <location filename="../../qmc2main.cpp" line="1127"/>
        <location filename="../../qmc2main.cpp" line="1159"/>
        <location filename="../../qmc2main.cpp" line="1184"/>
        <source>Year</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="174"/>
        <location filename="../../qmc2main.ui" line="306"/>
        <location filename="../../qmc2main.ui" line="441"/>
        <location filename="../../qmc2main.ui" line="576"/>
        <location filename="../../qmc2main.cpp" line="1089"/>
        <location filename="../../qmc2main.cpp" line="1129"/>
        <location filename="../../qmc2main.cpp" line="1161"/>
        <location filename="../../qmc2main.cpp" line="1186"/>
        <source>Manufacturer</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="179"/>
        <location filename="../../qmc2main.ui" line="311"/>
        <location filename="../../qmc2main.ui" line="446"/>
        <location filename="../../qmc2main.ui" line="581"/>
        <location filename="../../qmc2main.cpp" line="1091"/>
        <location filename="../../qmc2main.cpp" line="1131"/>
        <location filename="../../qmc2main.cpp" line="1163"/>
        <location filename="../../qmc2main.cpp" line="1188"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="184"/>
        <location filename="../../qmc2main.ui" line="316"/>
        <location filename="../../qmc2main.ui" line="451"/>
        <location filename="../../qmc2main.ui" line="586"/>
        <location filename="../../qmc2main.cpp" line="1093"/>
        <location filename="../../qmc2main.cpp" line="1133"/>
        <location filename="../../qmc2main.cpp" line="1165"/>
        <location filename="../../qmc2main.cpp" line="1190"/>
        <source>ROM types</source>
        <translation>Tipos de ROM</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="199"/>
        <location filename="../../qmc2main.ui" line="331"/>
        <location filename="../../qmc2main.ui" line="466"/>
        <location filename="../../qmc2main.ui" line="601"/>
        <location filename="../../qmc2main.cpp" line="1100"/>
        <location filename="../../qmc2main.cpp" line="1140"/>
        <location filename="../../qmc2main.cpp" line="1196"/>
        <source>Category</source>
        <translation>Categoria</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="204"/>
        <location filename="../../qmc2main.ui" line="336"/>
        <location filename="../../qmc2main.ui" line="471"/>
        <location filename="../../qmc2main.ui" line="606"/>
        <location filename="../../qmc2main.cpp" line="1103"/>
        <location filename="../../qmc2main.cpp" line="1143"/>
        <location filename="../../qmc2main.cpp" line="1171"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="259"/>
        <location filename="../../qmc2main.ui" line="391"/>
        <source>Loading game list, please wait...</source>
        <translation>Carregando lista de jogos, por favor espere...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="282"/>
        <source>Parent / clone hierarchy</source>
        <translation>Hierarquia pai / clone</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="286"/>
        <location filename="../../qmc2main.cpp" line="1113"/>
        <source>Game / Clones</source>
        <translation>Jogos / Clones</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="78"/>
        <location filename="../../qmc2main.ui" line="81"/>
        <source>Switch between detailed game list and parent / clone hierarchy</source>
        <translation>Trocar entre lista de jogos detalhada e hierarquia de pai / clone</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="85"/>
        <source>Game list with full detail (filtered)</source>
        <translation>Lista de jogos detalhada (filtrada)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="94"/>
        <source>Parent / clone hierarchy (not filtered)</source>
        <translation>Hierarquia pai / clone (não filtrado)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="112"/>
        <location filename="../../qmc2main.ui" line="115"/>
        <source>Toggle individual ROM states</source>
        <translation>Ativar estados individuais de ROM</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="414"/>
        <source>List of games viewed by category</source>
        <translation>Lista de jogos vistos por categoria</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="421"/>
        <location filename="../../qmc2main.cpp" line="1153"/>
        <source>Category / Game</source>
        <translation>Categoria / Jogo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="549"/>
        <source>List of games viewed by version they were added to the emulator</source>
        <translation>Lista de jogos vistos pela versão que foram adicionados ao emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="556"/>
        <location filename="../../qmc2main.cpp" line="1178"/>
        <source>Version / Game</source>
        <translation>Versão / Jogo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="680"/>
        <source>&amp;Search</source>
        <translation>&amp;Buscar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="695"/>
        <location filename="../../qmc2main.ui" line="698"/>
        <source>Search result</source>
        <translation>Resultado da busca</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="729"/>
        <source>Favo&amp;rites</source>
        <translation>Favo&amp;ritos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="744"/>
        <location filename="../../qmc2main.ui" line="747"/>
        <source>Favorite games</source>
        <translation>Jogos favoritos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="774"/>
        <location filename="../../qmc2main.ui" line="777"/>
        <source>Games last played</source>
        <translation>Últimos jogos executados</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="811"/>
        <source>Emulator</source>
        <translation>Emulador</translation>
    </message>
    <message>
        <source>L:Listed - C:Correct - M:Mostly correct - I:Incorrect - N:Not found - U:Unknown - S:Search</source>
        <translation type="obsolete">L:Listado - C:Correto - M:Maioria correto - I:Incorreto - N:Não encontrado - D:Desconhecido - B:Busca</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="853"/>
        <source>&lt;b&gt;&lt;font color=black&gt;L:?&lt;/font&gt; &lt;font color=#00cc00&gt;C:?&lt;/font&gt; &lt;font color=#a2c743&gt;M:?&lt;/font&gt; &lt;font color=#f90000&gt;I:?&lt;/font&gt; &lt;font color=#7f7f7f&gt;N:?&lt;/font&gt; &lt;font color=#0000f9&gt;U:?&lt;/font&gt; &lt;font color=chocolate&gt;S:?&lt;/font&gt;&lt;/b&gt;</source>
        <translation>&lt;b&gt;&lt;font color=black&gt;L:?&lt;/font&gt; &lt;font color=#00cc00&gt;C:?&lt;/font&gt; &lt;font color=#a2c743&gt;M:?&lt;/font&gt; &lt;font color=#f90000&gt;I:?&lt;/font&gt; &lt;font color=#7f7f7f&gt;N:?&lt;/font&gt; &lt;font color=#0000f9&gt;D:?&lt;/font&gt; &lt;font color=chocolate&gt;B:?&lt;/font&gt;&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="866"/>
        <location filename="../../qmc2main.ui" line="869"/>
        <source>Progress indicator for game list processing</source>
        <translation>Indicador de progresso para o processamento de lista de jogos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="891"/>
        <location filename="../../qmc2main.ui" line="894"/>
        <source>Indicator for current memory usage</source>
        <translation>Indicador para o uso atual de memória</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1366"/>
        <location filename="../../qmc2main.ui" line="1369"/>
        <source>Game status indicator</source>
        <translation>Indicador de estado do jogo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1399"/>
        <source>Pre&amp;view</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1408"/>
        <source>Fl&amp;yer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1417"/>
        <source>Game &amp;info</source>
        <translation>&amp;Informação do jogo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1432"/>
        <location filename="../../qmc2main.ui" line="1435"/>
        <source>Detailed game information</source>
        <translation>Informação detalhada do jogo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1453"/>
        <source>Em&amp;ulator info</source>
        <translation>Informação do Em&amp;ulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1468"/>
        <location filename="../../qmc2main.ui" line="1471"/>
        <source>Detailed emulator information</source>
        <translation>Informação detalhada do emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1489"/>
        <source>&amp;Configuration</source>
        <translation>&amp;Configuração</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1498"/>
        <source>&amp;Devices</source>
        <translation>&amp;Dispositivos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1507"/>
        <source>Ca&amp;binet</source>
        <translation>Ga&amp;binete</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1516"/>
        <source>C&amp;ontroller</source>
        <translation>C&amp;ontrole</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1525"/>
        <source>Mar&amp;quee</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1534"/>
        <source>Titl&amp;e</source>
        <translation>Títul&amp;o</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1543"/>
        <source>MA&amp;WS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1552"/>
        <source>&amp;PCB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1596"/>
        <source>&amp;Front end log</source>
        <translation>Log do &amp;Front end</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1611"/>
        <location filename="../../qmc2main.ui" line="1614"/>
        <source>Frontend log</source>
        <translation>Log do frontend</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1635"/>
        <source>Emulator &amp;log</source>
        <translation>&amp;Log do Emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1650"/>
        <location filename="../../qmc2main.ui" line="1653"/>
        <source>Emulator log</source>
        <translation>Log do Emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1674"/>
        <source>E&amp;mulator control</source>
        <translation>Controle do E&amp;mulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1692"/>
        <location filename="../../qmc2main.ui" line="1695"/>
        <source>Emulator control panel</source>
        <translation>Painel de controle do emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1705"/>
        <source>#</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1710"/>
        <source>Game / Notifier</source>
        <translation>Jogo / Notificação</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1715"/>
        <location filename="../../qmc2main.ui" line="2140"/>
        <source>Status</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1720"/>
        <source>LED0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1725"/>
        <source>LED1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1730"/>
        <source>PID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1735"/>
        <source>Command</source>
        <translation>Comando</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1748"/>
        <source>MP&amp;3 player</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1790"/>
        <location filename="../../qmc2main.ui" line="1793"/>
        <location filename="../../qmc2main.ui" line="2979"/>
        <source>Previous track</source>
        <translation>Faixa anterior</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1804"/>
        <location filename="../../qmc2main.ui" line="1807"/>
        <location filename="../../qmc2main.ui" line="3000"/>
        <source>Next track</source>
        <translation>Próxima faixa</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1818"/>
        <location filename="../../qmc2main.ui" line="1821"/>
        <location filename="../../qmc2main.ui" line="3021"/>
        <source>Fast backward</source>
        <translation>Avanço rápido</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1841"/>
        <location filename="../../qmc2main.ui" line="1844"/>
        <location filename="../../qmc2main.ui" line="3039"/>
        <source>Fast forward</source>
        <translation>Retrocesso rápido</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1864"/>
        <location filename="../../qmc2main.ui" line="1867"/>
        <location filename="../../qmc2main.ui" line="3060"/>
        <source>Stop track</source>
        <translation>Parar faixa</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1881"/>
        <location filename="../../qmc2main.ui" line="1884"/>
        <location filename="../../qmc2main.ui" line="3084"/>
        <source>Pause track</source>
        <translation>Pausar faixa</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1898"/>
        <location filename="../../qmc2main.ui" line="1901"/>
        <location filename="../../qmc2main.ui" line="3108"/>
        <source>Play track</source>
        <translation>Reproduzir faixa</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1915"/>
        <location filename="../../qmc2main.ui" line="1918"/>
        <source>Progress indicator for current track</source>
        <translation>Indicador de progresso para a faixa atual</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1924"/>
        <location filename="../../qmc2main.cpp" line="6560"/>
        <location filename="../../qmc2main.cpp" line="6570"/>
        <source>%vs (%ms total)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1954"/>
        <location filename="../../qmc2main.ui" line="1957"/>
        <source>Browse for tracks to add to playlist</source>
        <translation>Procurar por músicas para adicionar à lista</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1968"/>
        <location filename="../../qmc2main.ui" line="1971"/>
        <source>Enter URL to add to playlist</source>
        <translation>Entre com a URL para adicionar à playlist</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1985"/>
        <location filename="../../qmc2main.ui" line="1988"/>
        <source>Remove selected tracks from playlist</source>
        <translation>Remover faixas selecionadas da lista</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2013"/>
        <location filename="../../qmc2main.ui" line="2016"/>
        <source>Start playing automatically when QMC2 has started</source>
        <translation>Iniciar jogo automaticamente quando o QMC2 iniciar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2019"/>
        <source>Play on start</source>
        <translation>Iniciar ao abrir</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2026"/>
        <location filename="../../qmc2main.ui" line="2029"/>
        <source>Select random tracks from playlist</source>
        <translation>Selecionar músicas aleatórias da lista</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2032"/>
        <source>Shuffle</source>
        <translation>Embaralhar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2039"/>
        <location filename="../../qmc2main.ui" line="2042"/>
        <source>Automatically pause audio playback when at least one emulator is running</source>
        <translation>Pausar automaticamente quando pelo menos um emulador está executando</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2045"/>
        <source>Pause</source>
        <translation>Pausar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2055"/>
        <location filename="../../qmc2main.ui" line="2058"/>
        <source>Fade in and out on pause / resume</source>
        <translation>Fade in e out durante pausa/resumo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2061"/>
        <source>Fade in/out</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1933"/>
        <location filename="../../qmc2main.ui" line="1936"/>
        <location filename="../../qmc2main.ui" line="2071"/>
        <location filename="../../qmc2main.ui" line="2074"/>
        <source>Audio player volume</source>
        <translation>Volume do reprodutor de música</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="189"/>
        <location filename="../../qmc2main.ui" line="321"/>
        <location filename="../../qmc2main.ui" line="456"/>
        <location filename="../../qmc2main.ui" line="591"/>
        <location filename="../../qmc2main.cpp" line="1095"/>
        <location filename="../../qmc2main.cpp" line="1135"/>
        <location filename="../../qmc2main.cpp" line="1167"/>
        <location filename="../../qmc2main.cpp" line="1192"/>
        <source>Players</source>
        <translation>Jogadores</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="759"/>
        <source>Pl&amp;ayed</source>
        <translation>J&amp;ogados</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1561"/>
        <source>Softwar&amp;e list</source>
        <translation>Lista de Softwar&amp;e</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1570"/>
        <source>&amp;YouTube</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1769"/>
        <location filename="../../qmc2main.ui" line="1772"/>
        <source>Playlist (move items by dragging &amp; dropping them)</source>
        <translation>Playlist (mova os itens arranstando e soltando)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="1999"/>
        <location filename="../../qmc2main.ui" line="2002"/>
        <source>Setup available audio effects</source>
        <translation>Configurar efeitos de áudio disponíveis</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2097"/>
        <source>Dow&amp;nloads</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2112"/>
        <location filename="../../qmc2main.ui" line="2115"/>
        <source>List of active/inactive downloads</source>
        <translation>Lista de downloads ativos/inativos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2145"/>
        <source>Progress</source>
        <translation>Progresso</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2153"/>
        <location filename="../../qmc2main.ui" line="2156"/>
        <source>Clear finished / stopped downloads from list</source>
        <translation>Limpar downloads finalizados / parados da lista</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2170"/>
        <location filename="../../qmc2main.ui" line="2173"/>
        <source>Reload selected downloads</source>
        <translation>Recarregar downloads selecionados</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2187"/>
        <location filename="../../qmc2main.ui" line="2190"/>
        <source>Stop selected downloads</source>
        <translation>Parar downloads selecionados</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2204"/>
        <source>Automatically remove successfully finished downloads from this list</source>
        <translation>Remover automáticamente downloads bem sucessidos da lista</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2207"/>
        <source>Remove finished</source>
        <translation>Remover finalizados</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2243"/>
        <source>&amp;Help</source>
        <translation>&amp;Ajuda</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2252"/>
        <source>&amp;Tools</source>
        <translation>&amp;Ferramentas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2274"/>
        <source>&amp;Clean up</source>
        <translation>&amp;Limpeza</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2256"/>
        <source>&amp;Audio player</source>
        <translation>&amp;Reprodutor de música</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="194"/>
        <location filename="../../qmc2main.ui" line="326"/>
        <location filename="../../qmc2main.ui" line="461"/>
        <location filename="../../qmc2main.ui" line="596"/>
        <location filename="../../qmc2main.cpp" line="1097"/>
        <location filename="../../qmc2main.cpp" line="1137"/>
        <location filename="../../qmc2main.cpp" line="1169"/>
        <location filename="../../qmc2main.cpp" line="1194"/>
        <source>Driver status</source>
        <translation>Estado do driver</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2315"/>
        <source>&amp;Game</source>
        <translation>&amp;Jogo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2319"/>
        <source>&amp;Check</source>
        <translation>&amp;Verificar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2336"/>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2382"/>
        <source>&amp;Display</source>
        <translation>&amp;Mostrar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2386"/>
        <source>&amp;Arcade</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2417"/>
        <source>Toolbar</source>
        <translation>Barra de ferramentas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2466"/>
        <source>Check &amp;samples...</source>
        <translation>Verificar &amp;exemplos...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2469"/>
        <location filename="../../qmc2main.ui" line="2472"/>
        <source>Check sample set</source>
        <translation>Verificar conjunto de exemplos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2475"/>
        <source>Ctrl+2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2487"/>
        <source>Check &amp;previews...</source>
        <translation>Verificar &amp;previews...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2490"/>
        <location filename="../../qmc2main.ui" line="2493"/>
        <source>Check preview images</source>
        <translation>Verificar imagens de preview</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2496"/>
        <source>Ctrl+3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2511"/>
        <location filename="../../qmc2main.ui" line="2514"/>
        <source>Play current game</source>
        <translation>Jogar jogo atual</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2517"/>
        <source>Ctrl+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3499"/>
        <source>Play (tagged)</source>
        <translation>Jogar (etiquetados)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3502"/>
        <location filename="../../qmc2main.ui" line="3505"/>
        <source>Play all tagged games</source>
        <translation>Jogar todos os jogos etiquetados</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3517"/>
        <source>Play embedded (tagged)</source>
        <translation>Jogar embutido (etiquetados)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3520"/>
        <location filename="../../qmc2main.ui" line="3523"/>
        <source>Play all tagged games (embedded)</source>
        <translation>Jogar todos os jogos etiquetados (embutido)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3535"/>
        <source>To favorites (tagged)</source>
        <translation>Para favoritos (etiquetados)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3538"/>
        <location filename="../../qmc2main.ui" line="3541"/>
        <source>Add all tagged games to favorites</source>
        <translation>Adicionar todos os jogos etiquetados aos favoritos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3544"/>
        <source>Ctrl+Shift+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3556"/>
        <source>ROM state (tagged)</source>
        <translation>Estado da ROM (etiquetadas)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3559"/>
        <location filename="../../qmc2main.ui" line="3562"/>
        <source>Check ROM states of all tagged sets</source>
        <translation>Checar o estado da ROM de todos os conjuntos etiquetados</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3565"/>
        <source>Ctrl+Shift+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3577"/>
        <source>E&amp;xit / Stop</source>
        <translation>&amp;Sair / Parar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3580"/>
        <location filename="../../qmc2main.ui" line="3583"/>
        <source>Exit program / Stop any active processing</source>
        <translation>Sair do programa / Parar qualquer processamento ativo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3586"/>
        <source>Ctrl+X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2529"/>
        <source>&amp;Documentation...</source>
        <translation>&amp;Documentação...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="159"/>
        <location filename="../../qmc2main.ui" line="291"/>
        <location filename="../../qmc2main.ui" line="426"/>
        <location filename="../../qmc2main.ui" line="561"/>
        <location filename="../../qmc2main.cpp" line="1075"/>
        <location filename="../../qmc2main.cpp" line="1082"/>
        <location filename="../../qmc2main.cpp" line="1115"/>
        <location filename="../../qmc2main.cpp" line="1122"/>
        <location filename="../../qmc2main.cpp" line="1155"/>
        <location filename="../../qmc2main.cpp" line="1180"/>
        <source>Tag</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="847"/>
        <location filename="../../qmc2main.ui" line="850"/>
        <source>L:Listed - C:Correct - M:Mostly correct - I:Incorrect - N:Not found - U:Unknown - S:Search - T:Tagged</source>
        <translation>L:Listado - C:Correto - M:Maioria correto - I:Incorreto - N:Não encontrado - D:Desconhecido - B:Busca - E: Etiquetados</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2349"/>
        <source>&amp;Tag</source>
        <translation>E&amp;tiquetar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2532"/>
        <location filename="../../qmc2main.ui" line="2535"/>
        <source>View online documentation</source>
        <translation>Ver documentação online</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2538"/>
        <source>Ctrl+H</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2550"/>
        <source>&amp;About...</source>
        <translation>&amp;Sobre...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2553"/>
        <location filename="../../qmc2main.ui" line="2556"/>
        <source>About M.A.M.E. Catalog / Launcher II</source>
        <translation>Sobre o M.A.M.E. Catalog / Launcher II</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2559"/>
        <source>Ctrl+A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2571"/>
        <source>About &amp;Qt...</source>
        <translation>Sobre &amp;Qt...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2577"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2589"/>
        <source>Check &amp;ROMs...</source>
        <translation>Verificar &amp;ROMs...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2592"/>
        <location filename="../../qmc2main.ui" line="2595"/>
        <source>Check ROM collection</source>
        <translation>Verificar coleção de ROM</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2598"/>
        <source>Ctrl+1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2610"/>
        <source>&amp;Options...</source>
        <translation>&amp;Opções...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2613"/>
        <source>Options</source>
        <translation>Opções</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2616"/>
        <location filename="../../qmc2main.ui" line="2619"/>
        <source>Frontend setup and global emulator configuration</source>
        <translation>Configuração do frontend e configuração global do emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2622"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2634"/>
        <source>&amp;Reload</source>
        <translation>&amp;Recarregar</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2637"/>
        <location filename="../../qmc2main.ui" line="2640"/>
        <source>Reload entire game list</source>
        <translation>Recarregar lista de jogos completamente</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2643"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2651"/>
        <location filename="../../qmc2main.ui" line="2654"/>
        <location filename="../../qmc2main.ui" line="2657"/>
        <source>Clear image cache</source>
        <translation>Limpar cache de imagem</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2660"/>
        <source>Ctrl+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2668"/>
        <source>Recreate template map</source>
        <translation>Recriar mapa de template</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2671"/>
        <location filename="../../qmc2main.ui" line="2674"/>
        <source>Recreate template configuration map</source>
        <translation>Recriar mapa de configurações de template</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2677"/>
        <source>Ctrl+T</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2698"/>
        <source>Ctrl+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2710"/>
        <source>Full detail</source>
        <translation>Detalhes completos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2713"/>
        <location filename="../../qmc2main.ui" line="2716"/>
        <source>View game list with full detail</source>
        <translation>Ver lista de jogos em detalhes</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2719"/>
        <source>F5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2731"/>
        <source>Parent / clones</source>
        <translation>Pais / clones</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2734"/>
        <location filename="../../qmc2main.ui" line="2737"/>
        <source>View parent / clone hierarchy</source>
        <translation>Ver hierarquia pai / clone</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2740"/>
        <source>F6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2752"/>
        <source>Check &amp;flyers...</source>
        <translation>Verificar &amp;flyers...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2755"/>
        <location filename="../../qmc2main.ui" line="2758"/>
        <source>Check flyer images</source>
        <translation>Verificar imagens de flyers</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2761"/>
        <source>Ctrl+4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2769"/>
        <location filename="../../qmc2main.ui" line="2772"/>
        <source>Clear icon cache</source>
        <translation>Limpar cache de ícones</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2775"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2787"/>
        <source>Check &amp;icons...</source>
        <translation>Verificar &amp;ícones...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2790"/>
        <location filename="../../qmc2main.ui" line="2793"/>
        <source>Check icon images</source>
        <translation>Verificar imagens de ícones</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2796"/>
        <source>Ctrl+5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2808"/>
        <location filename="../../qmc2main.ui" line="2811"/>
        <source>ROM state</source>
        <translation>Estado da ROM</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2820"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2832"/>
        <source>Analyse ROM...</source>
        <translation>Analisar ROM...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2835"/>
        <source>Analyse ROM</source>
        <translation>Analisar ROM</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2838"/>
        <location filename="../../qmc2main.ui" line="2841"/>
        <source>Analyse current game with ROMAlyzer</source>
        <translation>Analisar jogo atual com o ROMAlyzer</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2844"/>
        <source>Ctrl+D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2856"/>
        <source>Analyse ROM (tagged)...</source>
        <translation>Analisar ROM (etiquetada)...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2859"/>
        <source>Analyse ROM (tagged)</source>
        <translation>Analisar ROM (etiquetada)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2862"/>
        <location filename="../../qmc2main.ui" line="2865"/>
        <source>Analyse all tagged sets with ROMAlyzer</source>
        <translation>Analisar todos os conjuntos etiquetados com o ROMAlyzer</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2880"/>
        <source>ROMAly&amp;zer...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2883"/>
        <location filename="../../qmc2main.ui" line="2886"/>
        <source>Open ROMAlyzer dialog</source>
        <translation>Abrir a janela do ROMAlyzer</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2889"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2907"/>
        <location filename="../../qmc2main.ui" line="2910"/>
        <source>Setup arcade mode</source>
        <translation>Configurar modo do arcade</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2916"/>
        <source>Ctrl+Shift+A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2931"/>
        <source>&amp;Toggle arcade</source>
        <translation>&amp;Ativar arcade</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2934"/>
        <location filename="../../qmc2main.ui" line="2937"/>
        <source>Toggle arcade mode</source>
        <translation>Ativar modo arcade</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2943"/>
        <source>F12</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2958"/>
        <source>Toggle &amp;full screen</source>
        <translation>Ativar &amp;tela cheia</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2961"/>
        <source>Toggle full screen</source>
        <translation>Ativar tela cheia</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2964"/>
        <source>Toggle full screen / windowed mode</source>
        <translation>Ativar tela cheia / modo janela</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2967"/>
        <source>F11</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2982"/>
        <source>Play previous track</source>
        <translation>Reproduzir faixa anterior</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2985"/>
        <source>Ctrl+Alt+Left</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3003"/>
        <source>Play next track</source>
        <translation>Reproduzir próxima música</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3006"/>
        <source>Ctrl+Alt+Right</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3024"/>
        <source>Fast backward within track</source>
        <translation>Retrocesso rápido dentro da faixa</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3027"/>
        <source>Ctrl+Alt+B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3042"/>
        <source>Fast forward within track</source>
        <translation>Progresso rápido dentro da faixa</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3045"/>
        <source>Ctrl+Alt+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3063"/>
        <source>Stop current track</source>
        <translation>Parar faixa atual</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3066"/>
        <source>Ctrl+Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3087"/>
        <source>Pause current track</source>
        <translation>Pausar faixa atual</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3090"/>
        <source>Ctrl+Alt+#</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3111"/>
        <source>Play current track</source>
        <translation>Reproduzir faixa atual</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3114"/>
        <source>Ctrl+Alt+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3125"/>
        <source>Raise volume</source>
        <translation>Aumentar volume</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3128"/>
        <source>Raise audio player volume</source>
        <translation>Aumentar o volume do reprodutor de músicas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3131"/>
        <source>Ctrl+Alt+PgUp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3139"/>
        <source>Lower volume</source>
        <translation>Abaixar volume</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3142"/>
        <source>Lower audio player volume</source>
        <translation>Abaixar o volume do reprodutor de músicas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3145"/>
        <source>Ctrl+Alt+PgDown</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3157"/>
        <source>&amp;Export ROM status...</source>
        <translation>&amp;Exportar estado da ROM...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3160"/>
        <location filename="../../qmc2main.ui" line="3163"/>
        <source>Export ROM status</source>
        <translation>Expotar estado da ROM</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3166"/>
        <source>Ctrl+E</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3178"/>
        <source>QMC2 for SDLMAME</source>
        <translation>QMC2 para SDLMAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3181"/>
        <location filename="../../qmc2main.ui" line="3184"/>
        <source>Launch QMC2 for SDLMAME</source>
        <translation>Iniciar QMC2 para SDLMAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3187"/>
        <source>Ctrl+Alt+1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3199"/>
        <source>QMC2 for SDLMESS</source>
        <translation>QMC2 para SDLMESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3202"/>
        <location filename="../../qmc2main.ui" line="3205"/>
        <source>Launch QMC2 for SDLMESS</source>
        <translation>Iniciar QMC2 para SDLMESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3208"/>
        <source>Ctrl+Alt+2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3219"/>
        <source>Show FPS</source>
        <translation>Mostrar FPS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3222"/>
        <location filename="../../qmc2main.ui" line="3225"/>
        <source>Toggle FPS display</source>
        <translation>Mostra/esconde FPS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3228"/>
        <source>Meta+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3240"/>
        <source>Screen shot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3243"/>
        <source>Save a screen shot from the current arcade scene</source>
        <translation>Salvar um screenshot da cena atual do arcade</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3246"/>
        <source>Take screen shot from arcade scene</source>
        <translation>Tirar um screenshot da cena do arcade</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3249"/>
        <source>Meta+F12</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3257"/>
        <location filename="../../qmc2main.ui" line="3260"/>
        <source>Clear MAWS cache</source>
        <translation>Limpar o cache do MAWS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3263"/>
        <source>Ctrl+M</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3271"/>
        <source>Check template map</source>
        <translation>Verificar mapa do template</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3274"/>
        <location filename="../../qmc2main.ui" line="3277"/>
        <source>Check template map against the configuration options of the currently selected emulator</source>
        <translation>Verificar mapa do template contra as opções de configuração do emulador selecionado atualmente</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3280"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3295"/>
        <location filename="../../qmc2main.ui" line="3298"/>
        <source>Play current game (embedded)</source>
        <translation>Jogar jogo atual (embutido)</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3301"/>
        <source>Ctrl+Shift+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3313"/>
        <source>&amp;Demo mode...</source>
        <translation>&amp;Modo demonstração...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3316"/>
        <location filename="../../qmc2main.ui" line="3319"/>
        <source>Open the demo mode dialog</source>
        <translation>Abrir a janela do modo demonstração</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3598"/>
        <source>Set tag</source>
        <translation>Colocar etiqueta</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3601"/>
        <location filename="../../qmc2main.ui" line="3604"/>
        <source>Set tag mark</source>
        <translation>Colocar marca da etiqueta</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3607"/>
        <source>Ctrl+Shift+T</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3619"/>
        <source>Unset tag</source>
        <translation>Remover etiqueta</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3622"/>
        <location filename="../../qmc2main.ui" line="3625"/>
        <source>Unset tag mark</source>
        <translation>Remover marca da etiqueta</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3628"/>
        <source>Ctrl+Shift+U</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3640"/>
        <source>Toggle tag</source>
        <translation>Adicionar/Remover etiqueta</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3643"/>
        <location filename="../../qmc2main.ui" line="3646"/>
        <source>Toggle tag mark</source>
        <translation>Adicionar/Remover marca da etiqueta</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3649"/>
        <source>Ctrl+Shift+G</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3661"/>
        <source>Tag all</source>
        <translation>Etiquetar todos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3664"/>
        <location filename="../../qmc2main.ui" line="3667"/>
        <source>Set tag mark for all sets</source>
        <translation>Colocar marca de etiqueta para todos os conjuntos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3670"/>
        <source>Ctrl+Shift+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3682"/>
        <source>Untag all</source>
        <translation>Remover todas as etiquetas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3685"/>
        <location filename="../../qmc2main.ui" line="3688"/>
        <source>Unset all tag marks</source>
        <translation>Remover todas as marcas de etiquetas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3691"/>
        <source>Ctrl+Shift+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3703"/>
        <source>Invert all tags</source>
        <translation>Inverter todas as etiquetas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3706"/>
        <location filename="../../qmc2main.ui" line="3709"/>
        <source>Invert all tag marks</source>
        <translation>Inverter todas as marcas de etiquetas</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3712"/>
        <source>Ctrl+Shift+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="2868"/>
        <source>Ctrl+Shift+D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3331"/>
        <source>By category</source>
        <translation>Por categoria</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3334"/>
        <location filename="../../qmc2main.ui" line="3337"/>
        <source>View games by category</source>
        <translation>Ver jogos por categoria</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3340"/>
        <source>F7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3352"/>
        <source>By version</source>
        <translation>Por versão</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3355"/>
        <location filename="../../qmc2main.ui" line="3358"/>
        <source>View games by version they were added to the emulator</source>
        <translation>Ver jogos pela versão que foram adicionados ao emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3361"/>
        <source>F8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3373"/>
        <source>Run external ROM tool...</source>
        <translation>Executar ferramenta de ROM externa...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3376"/>
        <location filename="../../qmc2main.ui" line="3379"/>
        <source>Run tool to process ROM data externally</source>
        <translation>Executar ferramenta para processar dados da ROM externamente</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3382"/>
        <source>F9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3394"/>
        <source>Run external ROM tool (tagged)...</source>
        <translation>Executar ferramenta de ROM externa (etiquetados)...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3397"/>
        <location filename="../../qmc2main.ui" line="3400"/>
        <source>Run tool to process ROM data externally for all tagged sets</source>
        <translation>Executar ferramenta para processar dados da ROM externamente para todos os conjuntos etiquetados</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3403"/>
        <source>Ctrl+Shift+F9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3411"/>
        <location filename="../../qmc2main.ui" line="3414"/>
        <source>Clear YouTube cache</source>
        <translation>Limpar cache do YouTube</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3417"/>
        <source>Ctrl+Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3425"/>
        <source>Clear ROM state cache</source>
        <translation>Limpar cache de estado da ROM</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3428"/>
        <location filename="../../qmc2main.ui" line="3431"/>
        <source>Forcedly clear (remove) the ROM state cache</source>
        <translation>Forçar limpar (remover) o cache de estado da ROM</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3439"/>
        <source>Clear game list cache</source>
        <translation>Limpar cache da lista de jogos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3442"/>
        <location filename="../../qmc2main.ui" line="3445"/>
        <source>Forcedly clear (remove) the game list cache</source>
        <translation>Forçar limpar (remover) o cache da lista de jogos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3453"/>
        <source>Clear XML cache</source>
        <translation>Limpar cache XML</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3456"/>
        <location filename="../../qmc2main.ui" line="3459"/>
        <source>Forcedly clear (remove) the XML cache</source>
        <translation>Forçar limpar (remover) o cache XML</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3467"/>
        <source>Clear software list cache</source>
        <translation>Limpar cache de lista de software</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3470"/>
        <location filename="../../qmc2main.ui" line="3473"/>
        <source>Forcedly clear (remove) the software list cache</source>
        <translation>Forçar limpar (remover) o cache da lista de software</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3481"/>
        <source>Clear ALL emulator caches</source>
        <translation>Limpar TODOS os caches de emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.ui" line="3484"/>
        <location filename="../../qmc2main.ui" line="3487"/>
        <source>Forcedly clear (remove) ALL emulator related caches</source>
        <translation>Forçar limpar (remover) TODOS os caches relacionados à emuladores</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5843"/>
        <source>Game info - %p%</source>
        <translation>Informação do jogo - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5845"/>
        <source>Machine info - %p%</source>
        <translation>Informação da máquina - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="6021"/>
        <source>Emu info - %p%</source>
        <translation>Informação do emulador - %p%</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4219"/>
        <source>Couldn&apos;t find the window ID of one or more
emulator(s) within a reasonable timeframe.

Retry embedding?</source>
        <translation>Não foi possível encontrar o ID da janela para um ou mais
emulador(es) dentro de um limite de tempo razoável.

Tentar novamente?</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4229"/>
        <source>Information</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4229"/>
        <source>Sorry, the emulator meanwhile died a sorrowful death :(.</source>
        <translation>Desculpe, o emulador teve uma morte triste :(.</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4171"/>
        <location filename="../../qmc2main.cpp" line="4172"/>
        <source>Toggle embedder options (hold down for menu)</source>
        <translation>Ativar/desativar opções para embutir (segure para exibir o menu)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qmc2main.cpp" line="286"/>
        <location filename="../../qmc2main.cpp" line="300"/>
        <source>last message repeated %n time(s)</source>
        <translation>
            <numerusform>última mensagem se repetiu %n vez</numerusform>
            <numerusform>última mensagem se repetiu %n vezes</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4179"/>
        <source>To favorites</source>
        <translation>Para favoritos</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4185"/>
        <source>Terminate emulator</source>
        <translation>Exterminar emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4186"/>
        <source>&amp;Terminate emulator</source>
        <translation>&amp;Exterminar emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4190"/>
        <source>Kill emulator</source>
        <translation>Matar emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4191"/>
        <source>&amp;Kill emulator</source>
        <translation>&amp;Matar emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="5260"/>
        <source>destroying audio effects dialog</source>
        <translation>destruindo janela de efeitos de áudio</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4099"/>
        <source>emulator #%1 is already embedded</source>
        <translation>emulador #%1 já está embutido</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4148"/>
        <source>WARNING: multiple windows for emulator #%1 found, choosing window ID %2 for embedding</source>
        <translation>AVISO: múltiplas janelas para o emulador #%1 encontradas, escolhando ID de janela %2 para embutir</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="4156"/>
        <source>embedding emulator #%1, window ID = %2</source>
        <translation>embutindo emulador #%1, ID da janela = %2</translation>
    </message>
</context>
<context>
    <name>Marquee</name>
    <message>
        <location filename="../../marquee.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>Copiar para área de transferência</translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="56"/>
        <location filename="../../marquee.cpp" line="57"/>
        <source>Game marquee image</source>
        <translation>Imagem de marquee do jogo</translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="59"/>
        <location filename="../../marquee.cpp" line="60"/>
        <source>Machine marquee image</source>
        <translation>Imagem de marquee da máquina</translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="68"/>
        <location filename="../../marquee.cpp" line="72"/>
        <source>FATAL: can&apos;t open marquee file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo de marquee. por favor verifique as permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../marquee.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Aguardando dados...</translation>
    </message>
</context>
<context>
    <name>MawsQuickDownloadSetup</name>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="467"/>
        <location filename="../../mawsqdlsetup.ui" line="570"/>
        <location filename="../../mawsqdlsetup.cpp" line="97"/>
        <location filename="../../mawsqdlsetup.cpp" line="102"/>
        <source>AntoPISA progettoSNAPS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="113"/>
        <source>Choose icon directory</source>
        <translation>Escolha o diretório de ícones</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="126"/>
        <source>Choose flyer directory</source>
        <translation>Escolha o diretório de flyers</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="139"/>
        <source>Choose cabinet directory</source>
        <translation>Escolha o diretório de gabinetes</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="152"/>
        <source>Choose controller directory</source>
        <translation>Escolha o diretório de controles</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="165"/>
        <source>Choose marquee directory</source>
        <translation>Escolha o diretório de marquee</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="178"/>
        <source>Choose PCB directory</source>
        <translation>Escolha o diretório de PCB</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="191"/>
        <source>Choose preview directory</source>
        <translation>Escolha o diretório de previews</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.cpp" line="204"/>
        <source>Choose title directory</source>
        <translation>Escolha o diretório de títulos</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="14"/>
        <source>MAWS quick download setup</source>
        <translation>Configuração do download rápido do MAWS</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="20"/>
        <source>Icons and cabinet art</source>
        <translation>Ícones e gabinetes</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="32"/>
        <source>Flyers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="45"/>
        <source>Automatically download flyer images</source>
        <translation>Baixar automaticamente imagens de flyers</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="48"/>
        <location filename="../../mawsqdlsetup.ui" line="107"/>
        <location filename="../../mawsqdlsetup.ui" line="166"/>
        <location filename="../../mawsqdlsetup.ui" line="225"/>
        <location filename="../../mawsqdlsetup.ui" line="284"/>
        <location filename="../../mawsqdlsetup.ui" line="330"/>
        <location filename="../../mawsqdlsetup.ui" line="411"/>
        <location filename="../../mawsqdlsetup.ui" line="514"/>
        <source>Auto</source>
        <translation>Automático</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="61"/>
        <source>Path to store flyer images</source>
        <translation>Caminho para guardar imagens de flyers</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="74"/>
        <source>Browse path to store flyer images</source>
        <translation>Procurar caminho para guardar imagens de flyers</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="91"/>
        <source>Cabinets</source>
        <translation>Gabinetes</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="104"/>
        <source>Automatically download cabinet images</source>
        <translation>Baixar automaticamente imagens de gabinetes</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="120"/>
        <source>Path to store cabinet images</source>
        <translation>Caminho para guardar imagens de gabinetes</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="133"/>
        <source>Browse path to store cabinet images</source>
        <translation>Procurar caminho para guardar imagens de gabinetes</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="150"/>
        <source>Controllers</source>
        <translation>Controles</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="163"/>
        <source>Automatically download controller images</source>
        <translation>Baixar automaticamente imagens de controles</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="179"/>
        <source>Path to store controller images</source>
        <translation>Caminho para guardar imagens de controles</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="192"/>
        <source>Browse path to store controller images</source>
        <translation>Procurar caminho para guardar imagens de controles</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="209"/>
        <source>Marquees</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="222"/>
        <source>Automatically download marquee images</source>
        <translation>Baixar automaticamente imagens de marquee</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="238"/>
        <source>Path to store marquee images</source>
        <translation>Caminho para guardar imagens de marquee</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="251"/>
        <source>Browse path to store marquee images</source>
        <translation>Procurar caminho para guardar imagens de marquee</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="268"/>
        <source>PCBs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="281"/>
        <source>Automatically download PCB images</source>
        <translation>Baixar automaticamente imagens de PCBs</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="297"/>
        <source>Path to store PCB images</source>
        <translation>Caminho para guardar imagens de PCBs</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="310"/>
        <source>Browse path to store PCB images</source>
        <translation>Procurar caminho para guardar imagens de PCBs</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="327"/>
        <source>Automatically download icon images</source>
        <translation>Baixar automaticamente imagens de ícones</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="343"/>
        <source>Path to store icon images</source>
        <translation>Caminho para guardar imagens de ícones</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="356"/>
        <source>Browse path to store icon images</source>
        <translation>Procurar caminho para guardar imagens de ícones</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="373"/>
        <source>Icons</source>
        <translation>Ícones</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="383"/>
        <source>Previews and titles</source>
        <translation>Pewviews e títulos</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="395"/>
        <source>Previews</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="408"/>
        <source>Automatically download preview images</source>
        <translation>Baixar automaticamente imagens de preview</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="424"/>
        <source>Path to store preview images</source>
        <translation>Caminho para guardar imagens de preview</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="437"/>
        <source>Browse path to store preview images</source>
        <translation>Procurar caminho para guardar imagens de preview</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="450"/>
        <location filename="../../mawsqdlsetup.ui" line="553"/>
        <source>Preferred collection</source>
        <translation>Colecçao preferida</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="463"/>
        <source>Select the preferred image collection for in-game previews (auto-download)</source>
        <translation>Selecione a coleção de imagens preferidas para previews do jogo (download automático)</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="472"/>
        <source>MAME World Snap Collection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="477"/>
        <location filename="../../mawsqdlsetup.ui" line="575"/>
        <source>CrashTest Snap Collection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="482"/>
        <source>Enaitz Jar Snaps</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="498"/>
        <source>Titles</source>
        <translation>Títulos</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="511"/>
        <source>Automatically download title images</source>
        <translation>Baixar automaticamente imagens de títulos</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="527"/>
        <source>Path to store title images</source>
        <translation>Caminho para guardar imagens de títulos</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="540"/>
        <source>Browse path to store title images</source>
        <translation>Procurar caminho para guardar imagens de títulos</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="566"/>
        <source>Select the preferred image collection for titles (auto-download)</source>
        <translation>Selecione a coleção de imagens preferidas para títulos (download automático)</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="612"/>
        <source>Apply MAWS quick download setup and close dialog</source>
        <translation>Aplicar configuração do download rápido do MAWS</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="615"/>
        <source>&amp;Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="626"/>
        <source>Cancel MAWS quick download setup and close dialog</source>
        <translation>Cancelar configuração do download rápido do MAWS</translation>
    </message>
    <message>
        <location filename="../../mawsqdlsetup.ui" line="629"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
</context>
<context>
    <name>MiniWebBrowser</name>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="86"/>
        <source>Open link</source>
        <translation>Abrir link</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="88"/>
        <source>Save link as...</source>
        <translation>Salvar link como...</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="90"/>
        <source>Copy link</source>
        <translation>Copiar link</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="92"/>
        <source>Save image as...</source>
        <translation>Salvar imagem como...</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="94"/>
        <source>Copy image</source>
        <translation>Copiar imagem</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="33"/>
        <location filename="../../miniwebbrowser.ui" line="36"/>
        <location filename="../../miniwebbrowser.cpp" line="96"/>
        <source>Go back</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="56"/>
        <location filename="../../miniwebbrowser.ui" line="59"/>
        <location filename="../../miniwebbrowser.cpp" line="98"/>
        <source>Go forward</source>
        <translation>Próximo</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="100"/>
        <source>Reload</source>
        <translation>Recarregar</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="102"/>
        <source>Stop</source>
        <translation>Parar</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="104"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="107"/>
        <source>Inspect</source>
        <translation>Inspecionar</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.cpp" line="477"/>
        <source>WARNING: invalid network reply and/or network error</source>
        <translation>AVISO: Resposta inválida da rede/ou erro de rede</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="15"/>
        <source>Mini Web Browser</source>
        <translation>Mini navegador</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="79"/>
        <location filename="../../miniwebbrowser.ui" line="82"/>
        <source>Reload current URL</source>
        <translation>Recarregar URL atual</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="102"/>
        <location filename="../../miniwebbrowser.ui" line="105"/>
        <source>Stop loading of current URL</source>
        <translation>Parar o carregamento da URL atual</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="125"/>
        <location filename="../../miniwebbrowser.ui" line="128"/>
        <source>Go home (first page)</source>
        <translation>Página inicial (primeira página)</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="145"/>
        <location filename="../../miniwebbrowser.ui" line="148"/>
        <source>Enter current URL</source>
        <translation>Entrar na URL atual</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="170"/>
        <location filename="../../miniwebbrowser.ui" line="173"/>
        <source>Load URL</source>
        <translation>Carregar URL</translation>
    </message>
    <message>
        <location filename="../../miniwebbrowser.ui" line="205"/>
        <location filename="../../miniwebbrowser.ui" line="208"/>
        <source>Current progress loading URL</source>
        <translation>Progresso carregando URL</translation>
    </message>
</context>
<context>
    <name>Options</name>
    <message>
        <location filename="../../options.cpp" line="201"/>
        <location filename="../../options.cpp" line="211"/>
        <source>Specify arguments...</source>
        <translation>Especifique os argumentos...</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="204"/>
        <location filename="../../options.cpp" line="214"/>
        <source>Reset to default (same path assumed)</source>
        <translation>Redefinir para o padrão (assumido mesmo caminho)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="220"/>
        <source>Browse emulator information database (messinfo.dat)</source>
        <translation>Procurar por banco de dados de informação de emulador (messinfo.dat)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="221"/>
        <source>Load emulator information database (messinfo.dat)</source>
        <translation>Carregar base de dados de informações de emulador (messinfo.dat)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="222"/>
        <source>Emulator information database - messinfo.dat (read)</source>
        <translation>Banco de dados de informação de emulador - messinfo.dat (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="223"/>
        <source>Machine info DB</source>
        <translation>BD de informação de máquina</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="224"/>
        <source>Load machine information database (MESS sysinfo.dat)</source>
        <translation>Carregar base da dados de informação de máquinas (sysinfo.dat do MESS)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="225"/>
        <source>Use in-memory compression for machine info DB (a bit slower, but consumes distinctly less memory; compression rate is usually about 1:16)</source>
        <translation>Usar compressão em memória para o BD de informações de máquina ( um pouco mais lento, mas consome menos memória: taxa de compressão é geralmente na ordem de 1:16)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="226"/>
        <source>Machine information database - MESS sysinfo.dat (read)</source>
        <translation>Base da dados de informação de máquinas -- sysinfo.dat do MESS (pronto)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="227"/>
        <source>Browse machine information database (MESS sysinfo.dat)</source>
        <translation>Procurar por base da dados de informação de máquinas (sysinfo.dat do MESS)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="228"/>
        <source>Machine &amp;list</source>
        <translation>&amp;lista de máquinas</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="229"/>
        <source>Machine description</source>
        <translation>Descrição da máquina</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="230"/>
        <source>Machine name</source>
        <translation>Nome da máquina</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="231"/>
        <source>Number of item insertions between machine list updates during reload (higher means faster, but makes the GUI less responsive)</source>
        <translation>Número de itens inseridos entre as atualizações da lista de máquinas durante o recarregamento (maior significa mais rápido, mas torna a interface menos disponível)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="232"/>
        <source>Delay update of any machine details (preview, flyer, info, configuration, ...) by how many milliseconds?</source>
        <translation>Atrasar atualização de qualquer detalhe da máquina (preview, flyer, informações, configurações, ....) por quantos milisegundos?</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="233"/>
        <source>Sort machine list while reloading (slower)</source>
        <translation>Ordenar lista de máquinas enquanto recarregar (mais lento)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="249"/>
        <location filename="../../options.cpp" line="250"/>
        <source>Option requires a reload of the entire machine list to take effect</source>
        <translation>Opção requer recarregar a lista de máquinas inteira para ter efeito</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="251"/>
        <source>Hide primary machine list while loading (recommended, much faster)</source>
        <translation>Esconder a lista primária de máquinas enquanto recarrega (recomendado, muito mais rápido)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="252"/>
        <source>Registered emulators -- you may select one of these in the machine-specific emulator configuration</source>
        <translation>Emuladores registrados -- você pode selecionar um desses na configuração de emulador específica de máquina</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="253"/>
        <source>Save machine selection</source>
        <translation>Salvar seleção de máquina</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="254"/>
        <source>Save machine selection on exit and before reloading the machine list</source>
        <translation>Salvar seleção de máquinas ao sair e antes de recarregar a lista de jogos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="255"/>
        <source>Restore machine selection</source>
        <translation>Restaurar seleção de máquinas</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="256"/>
        <source>Restore saved machine selection at start and after reloading the machine list</source>
        <translation>Restaurar seleção de máquinas ao iniciar e antes de recarregar a lista de jogos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="258"/>
        <location filename="../../options.cpp" line="964"/>
        <source>Category</source>
        <translation>Categoria</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="259"/>
        <location filename="../../options.cpp" line="965"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="266"/>
        <source>Check all ROM states</source>
        <translation>Verificar o estado de todas as ROMs</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="267"/>
        <source>Check all sample sets</source>
        <translation>Verificar todos os conjuntos de exemplos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="268"/>
        <source>Check preview images</source>
        <translation>Verificar imagens de preview</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="269"/>
        <source>Check flyer images</source>
        <translation>Verificar imagens de flyer</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="270"/>
        <source>Check icon images</source>
        <translation>Verificar imagens de ícones</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="271"/>
        <source>About QMC2</source>
        <translation>Sobre QMC2</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="272"/>
        <source>Analyze current game</source>
        <translation>Analisar jogo atual</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="273"/>
        <source>Analyze tagged sets</source>
        <translation>Analisar conjuntos etiquetados</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="274"/>
        <source>Export ROM Status</source>
        <translation>Exportar estado da ROM</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="275"/>
        <source>Copy game to favorites</source>
        <translation>Copiar jogo para favoritos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="276"/>
        <source>Copy tagged sets to favorites</source>
        <translation>Copiar conjuntos etiquetados para os favoritos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="277"/>
        <source>Online documentation</source>
        <translation>Documentação online</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="278"/>
        <source>Clear image cache</source>
        <translation>Limpar cache de imagens</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="279"/>
        <source>Setup arcade mode</source>
        <translation>Configurar modo arcade</translation>
    </message>
    <message>
        <source>Demo mode</source>
        <translation type="obsolete">Modo demonstração</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="280"/>
        <source>Clear MAWS cache</source>
        <translation>Limpar cache do MAWS</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="281"/>
        <source>Clear icon cache</source>
        <translation>Limpar cache de ícones</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="282"/>
        <source>Open options dialog</source>
        <translation>Abrir janela de opções</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="283"/>
        <source>Play (independent)</source>
        <translation>Jogar (independente)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="285"/>
        <source>Play (embedded)</source>
        <translation>Jogar (embutido)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="287"/>
        <source>About Qt</source>
        <translation>Sobre o Qt</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="288"/>
        <source>Reload gamelist</source>
        <translation>Recarregar lista de jogos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="289"/>
        <source>Check game&apos;s ROM state</source>
        <translation>Verificar estado das ROMs dos jogos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="290"/>
        <source>Check states of tagged ROMs</source>
        <translation>Verificar o estados das ROMs etiquetadas</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="291"/>
        <source>Recreate template map</source>
        <translation>Recriar mapa de template</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="292"/>
        <source>Check template map</source>
        <translation>Verificar mapa de template</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="293"/>
        <source>Stop processing / exit QMC2</source>
        <translation>Parar processamento / sair do QMC2</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="295"/>
        <source>Clear YouTube cache</source>
        <translation>Limpar cache do YouTube</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="297"/>
        <source>Open ROMAlyzer dialog</source>
        <translation>Abrir janela do ROMAlyzer</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="298"/>
        <source>Toggle ROM state C</source>
        <translation>Mostrar/esconder estado da ROM C</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="299"/>
        <source>Toggle ROM state M</source>
        <translation>Mostrar/esconder estado da ROM M</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="300"/>
        <source>Toggle ROM state I</source>
        <translation>Mostrar/esconder estado da ROM I</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="301"/>
        <source>Toggle ROM state N</source>
        <translation>Mostrar/esconder estado da ROM N</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="302"/>
        <source>Toggle ROM state U</source>
        <translation>Mostrar/esconder estado da ROM D</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="305"/>
        <source>Launch QMC2 for MAME</source>
        <translation>Iniciar o QMC2 para MAME</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="306"/>
        <source>Launch QMC2 for MESS</source>
        <translation>Iniciar o QMC2 para MESS</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="308"/>
        <source>Launch QMC2 for SDLMAME</source>
        <translation>Iniciar QMC2 para SDLMAME</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="309"/>
        <source>Launch QMC2 for SDLMESS</source>
        <translation>Iniciar QMC2 para SDLMESS</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="312"/>
        <source>Tag current set</source>
        <translation>Etiquetar conjunto atual</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="313"/>
        <source>Untag current set</source>
        <translation>Remover etiqueta do conjunto atual</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="314"/>
        <source>Toggle tag mark</source>
        <translation>Alternar marca da etiqueta</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="315"/>
        <source>Tag all sets</source>
        <translation>Etiquetar todos os conjuntos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="316"/>
        <source>Untag all sets</source>
        <translation>Remover etiqueta de todos os conjuntos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="317"/>
        <source>Invert all tags</source>
        <translation>Inverter todas as etiquetas</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="318"/>
        <source>Gamelist with full detail</source>
        <translation>Lista de jogos detalhada</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="319"/>
        <source>Parent / clone hierarchy</source>
        <translation>Hierarquia pai / clone</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="321"/>
        <source>View games by category</source>
        <translation>Ver jogos por categoria</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="322"/>
        <source>View games by version</source>
        <translation>Ver jogos por versão</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="324"/>
        <source>Run external ROM tool</source>
        <translation>Executar ferramenta de ROM externa</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="325"/>
        <source>Run ROM tool for tagged sets</source>
        <translation>Executar ferramenta de ROM para os conjuntos etiquetados</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="326"/>
        <source>Toggle full screen</source>
        <translation>Ativar tela cheia</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="327"/>
        <source>Toggle arcade mode</source>
        <translation>Ativar/desativar modo arcade</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="328"/>
        <source>Show FPS (arcade mode)</source>
        <translation>Mostar FPS (modo arcade)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="329"/>
        <source>Take snapshot (arcade mode)</source>
        <translation>Tirar snapshot (modo arcade)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="331"/>
        <source>Previous track (audio player)</source>
        <translation>Faixa anterior (reprodutor de música)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="332"/>
        <source>Next track (audio player)</source>
        <translation>Próxima faixa (reprodutor de música)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="333"/>
        <source>Fast backward (audio player)</source>
        <translation>Retrocesso rápido (reprodutor de música)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="334"/>
        <source>Fast forward (audio player)</source>
        <translation>Progresso rápido (reprodutor de música)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="335"/>
        <source>Stop track (audio player)</source>
        <translation>Parar faixa (reprodutor de música)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="336"/>
        <source>Pause track (audio player)</source>
        <translation>Pausar faixa (reprodutor de música)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="337"/>
        <source>Play track (audio player)</source>
        <translation>Reproduzir faixa (reprodutor de música)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="338"/>
        <source>Raise volume (audio player)</source>
        <translation>Aumentar volume (reprodutor de música)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="339"/>
        <source>Lower volume (audio player)</source>
        <translation>Baixar volume (reprodutor de música)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="343"/>
        <source>Plus (+)</source>
        <translation>Mais (+)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="344"/>
        <source>Minus (-)</source>
        <translation>Menos (-)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="345"/>
        <source>Cursor down</source>
        <translation>Cursor para baixo</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="346"/>
        <source>End</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="347"/>
        <source>Escape</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="348"/>
        <source>Cursor left</source>
        <translation>Cursor para esquerda</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="349"/>
        <source>Home</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="350"/>
        <source>Page down</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="351"/>
        <source>Page up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="352"/>
        <source>Enter key</source>
        <translation>Tecla Enter</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="353"/>
        <source>Cursor right</source>
        <translation>Cursor para direita</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="354"/>
        <source>Tabulator</source>
        <translation>Tab</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="355"/>
        <source>Cursor up</source>
        <translation>Cursor para cima</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="358"/>
        <source>WARNING: configuration is not writeable, please check access permissions for </source>
        <translation>AVISO: a configuração não pode ser escrita, por favor verifique as permissões de acesso</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="363"/>
        <location filename="../../options.cpp" line="370"/>
        <source>Reset to default font</source>
        <translation>Restaurar para a fonte padrão</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="378"/>
        <location filename="../../options.cpp" line="385"/>
        <source>No style sheet</source>
        <translation>Sem folha de estilo</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="388"/>
        <location filename="../../options.cpp" line="389"/>
        <source>Search in the folder we were called from</source>
        <translation>Procurar na pasta de onde fomos executados</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="825"/>
        <source>image cache size set to %1 MB</source>
        <translation>tamanho do cache de imagem definido para %1 MB</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="968"/>
        <source>View games by category (not filtered)</source>
        <translation>Ver jogos por categoria (não filtrado)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="970"/>
        <source>View games by emulator version (not filtered)</source>
        <translation>Ver jogos pela versão do emulador (não filtrado)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1141"/>
        <source>WARNING: can&apos;t initialize joystick</source>
        <translation>AVISO: impossível inicializar joystick</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1206"/>
        <source>Confirm</source>
        <translation>Confirmar</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1208"/>
        <source>An open game-specific emulator configuration has been detected.
Use local game-settings, overwrite with global settings or don&apos;t apply?</source>
        <translation>Uma configuração de emulador específica para jogo foi detectada.
Usar configurações de jogo local, sobrescrever com as configurações globais ou não aplicar?</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1210"/>
        <source>An open machine-specific emulator configuration has been detected.
Use local machine-settings, overwrite with global settings or don&apos;t apply?</source>
        <translation>Uma configuração de emulador específica para máquina foi detectada.
Usar configurações de máquina local, sobrescrever com as configurações globais ou não aplicar?</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1212"/>
        <source>&amp;Local</source>
        <translation>&amp;Local</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1212"/>
        <source>&amp;Overwrite</source>
        <translation>&amp;Sobrescrever</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1212"/>
        <source>Do&amp;n&apos;t apply</source>
        <translation>&amp;Não aplicar</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1315"/>
        <source>invalidating game info DB</source>
        <translation>Invalidando BD de informações de jogos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1317"/>
        <source>invalidating machine info DB</source>
        <translation>invalidando BD de informações de máquinas</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1334"/>
        <source>invalidating emulator info DB</source>
        <translation>Invalidando BD de informações de emuladores</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1351"/>
        <source>please reload game list for some changes to take effect</source>
        <translation>por favor recarregue a lista de jogos para as alterações terem efeito</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1353"/>
        <source>please reload machine list for some changes to take effect</source>
        <translation>por favor recarregue a lista de máquinas para as alterações terem efeito</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1357"/>
        <source>please restart QMC2 for some changes to take effect</source>
        <translation>por favor reinicie o QMC2 para que algumas mudanças tenha efeito</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1367"/>
        <source>re-sort of game list impossible at this time, please wait for ROM verification to finish and try again</source>
        <translation>reordenação da lista de jogos é impossível no momento, por favor espere a verificação da ROM terminar e tente novamente</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1369"/>
        <source>re-sort of machine list impossible at this time, please wait for ROM verification to finish and try again</source>
        <translation>reordenação da lista de máquinas é impossível no momento, por favor espere a verificação da ROM terminar e tente novamente</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1424"/>
        <source>sorting game list by %1 in %2 order</source>
        <translation>ordenando lista de jogos por %1 na ordem %2</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1424"/>
        <location filename="../../options.cpp" line="1426"/>
        <source>ascending</source>
        <translation>crescente</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1424"/>
        <location filename="../../options.cpp" line="1426"/>
        <source>descending</source>
        <translation>decrescente</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1426"/>
        <source>sorting machine list by %1 in %2 order</source>
        <translation>ordenando lista de máquinas por %1 na ordem %2</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1438"/>
        <source>Waiting for data...</source>
        <translation>Aguardando dados...</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1609"/>
        <location filename="../../options.cpp" line="1613"/>
        <source>FATAL: can&apos;t open preview file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo de preview. por favor verifique as permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1627"/>
        <location filename="../../options.cpp" line="1631"/>
        <source>FATAL: can&apos;t open flyer file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo de flyer. por favor verifique as permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1645"/>
        <location filename="../../options.cpp" line="1649"/>
        <source>FATAL: can&apos;t open cabinet file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo de gabinete. por favor verifique as permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1663"/>
        <location filename="../../options.cpp" line="1667"/>
        <source>FATAL: can&apos;t open controller file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo de controle. por favor verifique as permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1681"/>
        <location filename="../../options.cpp" line="1685"/>
        <source>FATAL: can&apos;t open marquee file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo de marquee. por favor verifique as permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1699"/>
        <location filename="../../options.cpp" line="1703"/>
        <source>FATAL: can&apos;t open title file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo de título. por favor verifique as permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1717"/>
        <location filename="../../options.cpp" line="1721"/>
        <source>FATAL: can&apos;t open PCB file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo de PCB. por favor verifique as permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1735"/>
        <location filename="../../options.cpp" line="1739"/>
        <source>FATAL: can&apos;t open software snap-shot file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo de snapshot de software. por favor verifique as permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1752"/>
        <location filename="../../options.cpp" line="1756"/>
        <source>FATAL: can&apos;t open icon file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo do ícone, por favor verifique permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1764"/>
        <source>triggering automatic reload of game list</source>
        <translation>ativando recarregamento automático da lista de jogos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1766"/>
        <source>triggering automatic reload of machine list</source>
        <translation>ativando recarregamento automático da lista de máquinas</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1900"/>
        <location filename="../../options.cpp" line="1957"/>
        <location filename="../../options.cpp" line="2847"/>
        <source>Preview file</source>
        <translation>Arquivo de preview</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1940"/>
        <location filename="../../options.cpp" line="1997"/>
        <location filename="../../options.cpp" line="2935"/>
        <source>SW snap file</source>
        <translation>Arquivo de snapshot de software</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1088"/>
        <location filename="../../options.cpp" line="1900"/>
        <location filename="../../options.cpp" line="1957"/>
        <location filename="../../options.cpp" line="2847"/>
        <source>Preview directory</source>
        <translation>Diretório de preview</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1905"/>
        <location filename="../../options.cpp" line="1962"/>
        <location filename="../../options.cpp" line="2858"/>
        <source>Flyer file</source>
        <translation>Arquivo de flyer</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1191"/>
        <location filename="../../options.cpp" line="1905"/>
        <location filename="../../options.cpp" line="1962"/>
        <location filename="../../options.cpp" line="2858"/>
        <source>Flyer directory</source>
        <translation>Diretório de flyer</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1910"/>
        <location filename="../../options.cpp" line="1967"/>
        <location filename="../../options.cpp" line="2869"/>
        <source>Icon file</source>
        <translation>Arquivo de ícone</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1294"/>
        <location filename="../../options.cpp" line="1910"/>
        <location filename="../../options.cpp" line="1967"/>
        <location filename="../../options.cpp" line="2869"/>
        <source>Icon directory</source>
        <translation>Diretório de ícone</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1915"/>
        <location filename="../../options.cpp" line="1972"/>
        <location filename="../../options.cpp" line="2880"/>
        <source>Cabinet file</source>
        <translation>Arquivo de gabinete</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1397"/>
        <location filename="../../options.cpp" line="1915"/>
        <location filename="../../options.cpp" line="1972"/>
        <location filename="../../options.cpp" line="2880"/>
        <source>Cabinet directory</source>
        <translation>Diretório de gabinete</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1920"/>
        <location filename="../../options.cpp" line="1977"/>
        <location filename="../../options.cpp" line="2891"/>
        <source>Controller file</source>
        <translation>Arquivo de controle</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1500"/>
        <location filename="../../options.cpp" line="1920"/>
        <location filename="../../options.cpp" line="1977"/>
        <location filename="../../options.cpp" line="2891"/>
        <source>Controller directory</source>
        <translation>Diretório de controle</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1925"/>
        <location filename="../../options.cpp" line="1982"/>
        <location filename="../../options.cpp" line="2902"/>
        <source>Marquee file</source>
        <translation>Arquivo de marquee</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1603"/>
        <location filename="../../options.cpp" line="1925"/>
        <location filename="../../options.cpp" line="1982"/>
        <location filename="../../options.cpp" line="2902"/>
        <source>Marquee directory</source>
        <translation>Diretório de marquee</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1930"/>
        <location filename="../../options.cpp" line="1987"/>
        <location filename="../../options.cpp" line="2913"/>
        <source>Title file</source>
        <translation>Arquivo de título</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1706"/>
        <location filename="../../options.cpp" line="1930"/>
        <location filename="../../options.cpp" line="1987"/>
        <location filename="../../options.cpp" line="2913"/>
        <source>Title directory</source>
        <translation>Diretório de título</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="1935"/>
        <location filename="../../options.cpp" line="1992"/>
        <location filename="../../options.cpp" line="2924"/>
        <source>PCB file</source>
        <translation>Arquivo de PCB</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1809"/>
        <location filename="../../options.cpp" line="1935"/>
        <location filename="../../options.cpp" line="1992"/>
        <location filename="../../options.cpp" line="2924"/>
        <source>PCB directory</source>
        <translation>Diretório de PCB</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2271"/>
        <source>Choose Qt style sheet file</source>
        <translation>Escolha o arquivo de estilo do Qt</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2271"/>
        <source>Qt Style Sheets (*.qss)</source>
        <translation>Estilos do Qt (*.qss)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2283"/>
        <source>Choose temporary work file</source>
        <translation>Escolha arquivo temporário de trabalho</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2283"/>
        <location filename="../../options.cpp" line="2407"/>
        <location filename="../../options.cpp" line="2419"/>
        <location filename="../../options.cpp" line="2432"/>
        <location filename="../../options.cpp" line="2444"/>
        <location filename="../../options.cpp" line="2495"/>
        <location filename="../../options.cpp" line="2507"/>
        <location filename="../../options.cpp" line="2519"/>
        <location filename="../../options.cpp" line="2531"/>
        <location filename="../../options.cpp" line="2543"/>
        <location filename="../../options.cpp" line="2567"/>
        <location filename="../../options.cpp" line="2579"/>
        <location filename="../../options.cpp" line="2591"/>
        <location filename="../../options.cpp" line="2605"/>
        <location filename="../../options.cpp" line="2646"/>
        <location filename="../../options.cpp" line="2672"/>
        <location filename="../../options.cpp" line="2698"/>
        <location filename="../../options.cpp" line="2710"/>
        <location filename="../../options.cpp" line="2723"/>
        <location filename="../../options.cpp" line="2944"/>
        <location filename="../../options.cpp" line="2956"/>
        <location filename="../../options.cpp" line="2968"/>
        <location filename="../../options.cpp" line="2980"/>
        <location filename="../../options.cpp" line="2992"/>
        <location filename="../../options.cpp" line="3004"/>
        <location filename="../../options.cpp" line="3016"/>
        <location filename="../../options.cpp" line="3028"/>
        <location filename="../../options.cpp" line="3054"/>
        <location filename="../../options.cpp" line="3266"/>
        <source>All files (*)</source>
        <translation>Todos os arquivos (*)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2295"/>
        <source>Choose preview directory</source>
        <translation>Escolha o diretório de previews</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2309"/>
        <source>Choose flyer directory</source>
        <translation>Escolha o diretório de flyers</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2323"/>
        <source>Choose icon directory</source>
        <translation>Escolha o diretório de ícones</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2337"/>
        <source>Choose cabinet directory</source>
        <translation>Escolha o diretório de gabinetes</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2351"/>
        <source>Choose controller directory</source>
        <translation>Escolha o diretório de controles</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2365"/>
        <source>Choose marquee directory</source>
        <translation>Escolha o diretório de marquee</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2379"/>
        <source>Choose title directory</source>
        <translation>Escolha o diretório de títulos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2393"/>
        <source>Choose PCB directory</source>
        <translation>Escolha o diretório de PCB</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2407"/>
        <source>Choose options template file</source>
        <translation>Escolha o diretório de arquivo de template de opções</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2419"/>
        <location filename="../../options.cpp" line="3266"/>
        <source>Choose emulator executable file</source>
        <translation>Escolha o arquivo executável do emulador</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2432"/>
        <source>Choose MAME variant&apos;s exe file</source>
        <translation>Escolha o arquivo executável da variante do MAME</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2444"/>
        <source>Choose MESS variant&apos;s exe file</source>
        <translation>Escolha o arquivo executável da variante do MESS</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2459"/>
        <source>MAME variant arguments</source>
        <translation>Argumentos do variante do MAME</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2460"/>
        <source>Specify command line arguments passed to the MAME variant
(empty means: &apos;pass the arguments we were called with&apos;):</source>
        <translation>Especifique os argumentos de linha de comando passados para a variante do MAME
(vazio significa: &apos;use os mesmos argumentos os quais foram passados para nós&apos;):</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2478"/>
        <source>MESS variant arguments</source>
        <translation>Argumentos do variante do MESS</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2479"/>
        <source>Specify command line arguments passed to the MESS variant
(empty means: &apos;pass the arguments we were called with&apos;):</source>
        <translation>Especifique os argumentos de linha de comando passados para a variante do MESS
(vazio significa: &apos;use os mesmos argumentos os quais foram passados para nós&apos;):</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2495"/>
        <source>Choose emulator log file</source>
        <translation>Escolha o arquivo de log do emulador</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2507"/>
        <source>Choose XML gamelist cache file</source>
        <translation>Escolha o arquivo de cache (XML) da lista de jogos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2519"/>
        <source>Choose zip tool</source>
        <translation>Escolha a ferramenta zip</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2531"/>
        <source>Choose file removal tool</source>
        <translation>Escolha a ferramenta de remoção de arquivo</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2543"/>
        <source>Choose ROM tool</source>
        <translation>Escolha a ferramenta de ROM</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2567"/>
        <source>Choose game favorites file</source>
        <translation>Escolha o arquivo de favoritos de jogos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2579"/>
        <source>Choose play history file</source>
        <translation>Escolha o arquivo de histórico de jogos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2591"/>
        <source>Choose gamelist cache file</source>
        <translation>Escolha o arquivo de cache da lista de jogos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2605"/>
        <source>Choose ROM state cache file</source>
        <translation>Escolhar o arquivo de cache do estado das ROMs</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2555"/>
        <location filename="../../options.cpp" line="2617"/>
        <location filename="../../options.cpp" line="3278"/>
        <source>Choose working directory</source>
        <translation>Escolha o diretório de trabalho</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2632"/>
        <source>Choose MAWS cache directory</source>
        <translation>Escolhar o diretório de cache do MAWS</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2646"/>
        <source>Choose software list cache file</source>
        <translation>Escolha o arquivo de cache da lista de software</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2658"/>
        <source>Choose general software folder</source>
        <translation>Escolha a pasta geral de software</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2672"/>
        <source>Choose front end log file</source>
        <translation>Escolha o arquivo de log do frontend</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2684"/>
        <source>Choose data directory</source>
        <translation>Escolha o diretório de dados</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2698"/>
        <source>Choose game info DB</source>
        <translation>Escolha o BD de informação de jogos</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2710"/>
        <source>Choose emulator info DB</source>
        <translation>Escolha o BD de informação de emulador</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2723"/>
        <source>Choose catver.ini file</source>
        <translation>Escolha o arquivo catver.ini</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2944"/>
        <source>Choose ZIP-compressed preview file</source>
        <translation>Escolha o arquivo de previews comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2956"/>
        <source>Choose ZIP-compressed flyer file</source>
        <translation>Escolha o arquivo de flyer comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2968"/>
        <source>Choose ZIP-compressed icon file</source>
        <translation>Escolha o arquivo de ícones comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2980"/>
        <source>Choose ZIP-compressed cabinet file</source>
        <translation>Escolha o arquivo de gabinetes comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="2992"/>
        <source>Choose ZIP-compressed controller file</source>
        <translation>Escolha o arquivo de controles comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3004"/>
        <source>Choose ZIP-compressed marquee file</source>
        <translation>Escolha o arquivo de marquee comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3016"/>
        <source>Choose ZIP-compressed title file</source>
        <translation>Escolha o arquivo de títulos comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3028"/>
        <source>Choose ZIP-compressed PCB file</source>
        <translation>Escolha o arquivo de PCBs comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3040"/>
        <source>Choose software snap directory</source>
        <translation>Escolha diretório de snapshot de software</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3054"/>
        <source>Choose ZIP-compressed software snap file</source>
        <translation>Escolha o arquivo comprimido ZIP de snapshot de software</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3236"/>
        <source>shortcut map is clean</source>
        <translation>mapa de atalhos está limpo</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3238"/>
        <source>WARNING: shortcut map contains duplicates</source>
        <translation>AVISO: mapa de atalhos contém itens duplicados</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2942"/>
        <location filename="../../options.cpp" line="3419"/>
        <location filename="../../options.cpp" line="3442"/>
        <location filename="../../options.cpp" line="3495"/>
        <location filename="../../options.cpp" line="3582"/>
        <source>No joysticks found</source>
        <translation>Nenhum joystick encontrado</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3755"/>
        <source>joystick map is clean</source>
        <translation>mapa de joystick está limpo</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="3757"/>
        <source>WARNING: joystick map contains duplicates</source>
        <translation>AVISO: mapa de joystick contém itens duplicados</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="15"/>
        <source>Options</source>
        <translation>Opções</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="25"/>
        <source>&amp;Front end</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="41"/>
        <source>&amp;GUI</source>
        <translation>&amp;Interface</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="91"/>
        <source>Language</source>
        <translation>Língua</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="106"/>
        <source>Application language</source>
        <translation>Língua da aplicação</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="110"/>
        <source>DE (German)</source>
        <translation>DE (Alemão)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="115"/>
        <source>FR (French)</source>
        <translation>FR (Francês)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="120"/>
        <source>PL (Polish)</source>
        <translation>PL (Polonês)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="125"/>
        <source>PT (Portuguese)</source>
        <translation>PT (Português)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="130"/>
        <source>US (English)</source>
        <translation>US (Inglês)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="138"/>
        <source>Save window layout at exit</source>
        <translation>Salvar layout da janela ao sair</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="141"/>
        <source>Save layout</source>
        <translation>Salvar layout</translation>
    </message>
    <message>
        <source>Save game selection at exit and before reloading the gamelist</source>
        <translation type="obsolete">Salvar seleção de jogos ao sair e antes de recarregar a lista de jogos</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="151"/>
        <source>Save game selection</source>
        <translation>Salvar seleção de jogo</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="158"/>
        <source>Restore saved window layout at start</source>
        <translation>Restaurar layout da janela ao iniciar</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="161"/>
        <source>Restore layout</source>
        <translation>Restaurar layout</translation>
    </message>
    <message>
        <source>Restore saved game selection at start and after reloading the gamelist</source>
        <translation type="obsolete">Restaurar seleção de jogos ao sair e antes de recarregar a lista de jogos</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="171"/>
        <source>Restore game selection</source>
        <translation>Restaurar seleção de jogo</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="178"/>
        <location filename="../../options.ui" line="188"/>
        <location filename="../../options.ui" line="198"/>
        <location filename="../../options.ui" line="208"/>
        <location filename="../../options.ui" line="356"/>
        <location filename="../../options.ui" line="366"/>
        <location filename="../../options.ui" line="422"/>
        <source>Scale image to fit frame size (otherwise use original size)</source>
        <translation>Esticar imagem para ocupar todo o espaço (do contrário, usar tamanho original)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="181"/>
        <source>Scaled preview</source>
        <translation>Esticar preview</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="191"/>
        <source>Scaled cabinet</source>
        <translation>Esticar gabinete</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="201"/>
        <source>Scaled controller</source>
        <translation>Esticar controle</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="211"/>
        <source>Scaled marquee</source>
        <translation>Esticar marquee</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="218"/>
        <location filename="../../options.ui" line="221"/>
        <source>Show status bar</source>
        <translation>Mostar barra de estado</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="729"/>
        <location filename="../../options.ui" line="732"/>
        <source>Show tool bar</source>
        <translation>Mostrar barra de ferramentas</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="231"/>
        <source>Show the menu bar</source>
        <translation>Mostrar a barra de menus</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="234"/>
        <source>Show menu bar</source>
        <translation>Mostrar barra de menu</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="241"/>
        <source>Show short description of current processing in progress bar</source>
        <translation>Mostrar descrição curta do processamento atual na barra de progresso</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="244"/>
        <source>Show progress texts</source>
        <translation>Mostrar texto de progresso</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="251"/>
        <source>Kill emulators on exit?</source>
        <translation>Matar emuladores na saída?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="254"/>
        <source>Kill emulators</source>
        <translation>Matar emuladores</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="261"/>
        <source>Check for other instances of this QMC2 variant on startup</source>
        <translation>Verificar por outras instâncias de variações do QMC2 ao iniciar</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="264"/>
        <source>Check single instance</source>
        <translation>Verificar instância única</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="271"/>
        <source>Show vertical game status indicator in game details</source>
        <translation>Mostar indicador vertical do estado do jogo nos detalhes do jogo</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="274"/>
        <source>Game status indicator</source>
        <translation>Mostrar indicador de estado</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="284"/>
        <source>Show the game status indicator only when the game list is not visible due to the current layout</source>
        <translation>Mostrar o indicador vertical de estado de jogo somente quando a lista de máquinas não é visível devido ao layout atual</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="287"/>
        <location filename="../../options.ui" line="313"/>
        <source>Only when required</source>
        <translation>Somente quando requerido</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="294"/>
        <source>Show game&apos;s description at the bottom of any images</source>
        <translation>Mostrar descrição do jogo abaixo de quaisquer imagens</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="297"/>
        <source>Show game name</source>
        <translation>Mostrar nome do jogo</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="310"/>
        <source>Show game&apos;s description only when the game list is not visible due to the current layout</source>
        <translation>Mostrar descrição de jogo somente quando a lista de máquinas não é visível devido ao layout atual</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2085"/>
        <source>Load emulator information database (mameinfo.dat)</source>
        <translation>Carregar base de dados de informações de emulador (mameinfo.dat)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2105"/>
        <source>Use in-memory compression for emulator info DB (a bit slower, but consumes distinctly less memory; compression rate is usually about 1:16)</source>
        <translation>Usar compressão em memória para o BD de informações de emulador ( um pouco mais lento, mas consome menos memória: taxa de compressão é geralmente na ordem de 1:16)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2047"/>
        <source>Load game information database (MAME history.dat)</source>
        <translation>Carregar base de dados de informações de jogos (history.dat do MAME)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2067"/>
        <source>Use in-memory compression for game info DB (a bit slower, but consumes distinctly less memory; compression rate is usually about 1:16)</source>
        <translation>Usar compressão em memória para o BD de informações de jogos ( um pouco mais lento, mas consome menos memória: taxa de compressão é geralmente na ordem de 1:16)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="328"/>
        <location filename="../../options.ui" line="344"/>
        <source>Option requires a restart of QMC2 to take effect</source>
        <translation>Opção requer reinício do QMC2 para ter efeito</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="347"/>
        <source>restart required</source>
        <translation>requer reinício</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2017"/>
        <location filename="../../options.ui" line="2537"/>
        <location filename="../../options.ui" line="4052"/>
        <source>Option requires a reload of the gamelist to take effect</source>
        <translation>Opção requer recarregamento da lista de jogos para ter efeito</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2033"/>
        <location filename="../../options.ui" line="2553"/>
        <location filename="../../options.ui" line="4068"/>
        <source>Option requires a reload of the entire game list to take effect</source>
        <translation>Opção requer recarregar a lista de jogos inteira para ter efeito</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2036"/>
        <location filename="../../options.ui" line="2556"/>
        <location filename="../../options.ui" line="4071"/>
        <source>reload required</source>
        <translation>requer recarregamento</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="359"/>
        <source>Scaled title</source>
        <translation>Tílulo esticado</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="369"/>
        <source>Scaled flyer</source>
        <translation>Flyer esticado</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="382"/>
        <source>Application font (= system default if empty)</source>
        <translation>Fonte da aplicação ( = padrão do sistema se vazio)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="398"/>
        <source>Browse application font</source>
        <translation>Procurar por fonte da aplicação</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="425"/>
        <source>Scaled PCB</source>
        <translation>PCB esticado</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="432"/>
        <source>Emulator log size</source>
        <translation>Tamanho do log do emulador</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="445"/>
        <source>Maximum number of lines to keep in emulator log browser</source>
        <translation>Número máximo de linhas para manter no browser de log do emulador</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="448"/>
        <location filename="../../options.ui" line="483"/>
        <source>unlimited</source>
        <translation>ilimitado</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="451"/>
        <location filename="../../options.ui" line="486"/>
        <source> lines</source>
        <translation>linhas</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="467"/>
        <source>Front end log size</source>
        <translation>Tamanho do log do frontend</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="480"/>
        <source>Maximum number of lines to keep in front end log browser</source>
        <translation>Número máximo de linhas para manter no browser de log do frontend</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="502"/>
        <source>Application font</source>
        <translation>Fonte da aplicação</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="509"/>
        <source>Style sheet</source>
        <translation>Estilo</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="522"/>
        <source>Qt style sheet file (*.qss, leave empty for no style sheet)</source>
        <translation>Arquivo de estilo do Qt (*.qss, deixe em branco para nenhum estilo)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="535"/>
        <source>Browse Qt style sheet file</source>
        <translation>Procurar por arquivo de estilo do Qt</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="546"/>
        <source>GUI style</source>
        <translation>Estilo da interface</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="559"/>
        <source>Application style (Default = use system&apos;s default style)</source>
        <translation>Estilo da aplicação (Padrão = usar estilo padrão do sistema)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="566"/>
        <source>Smooth image scaling (nicer, but slower)</source>
        <translation>Esticamento suave da imagem (melhor, mas mais lento)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="569"/>
        <source>Smooth scaling</source>
        <translation>Esticar suavemente</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="576"/>
        <source>Retry loading images which weren&apos;t found before?</source>
        <translation>Tentar carregar imagens que não foram encontradas antes?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="579"/>
        <source>Retry loading images</source>
        <translation>Tentar carregar imagens não encontradas</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="586"/>
        <source>Fall back to the parent&apos;s image if an indivual image is missing but there&apos;s one for the parent</source>
        <translation>Usar imagem do pai se uma imagem individual não foi encontrada mas existe uma para o pai</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="589"/>
        <source>Parent image fallback</source>
        <translation>Usar imagem do pai</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="596"/>
        <source>Minimize application windows when launching another variant</source>
        <translation>Minimizar janelas da aplicação quando iniciar outra variação</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="599"/>
        <source>Minimize on variant launch</source>
        <translation>Minimizar ao iniciar variação</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="606"/>
        <source>Image cache size</source>
        <translation>Tamanho do cache de imagens</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="619"/>
        <source>Image cache size in MB</source>
        <translation>Tamanho do cache de imagens em MB</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="622"/>
        <source> MB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="638"/>
        <source>Open the detail setup dialog</source>
        <translation>Abrir a janela de configurações detalhadas</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="641"/>
        <source>Detail setup...</source>
        <translation>Configuração detalhadas...</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="648"/>
        <source>Show indicator for current memory usage</source>
        <translation>Mostar indicador do uso atual de memória</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="651"/>
        <source>Show memory  usage</source>
        <translation>Mostrar uso de memória</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="658"/>
        <source>Use standard or custom color palette?</source>
        <translation>Usar paleta de cores padrão ou customizada?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="661"/>
        <source>Standard color palette</source>
        <translation>Paleta de cores padrão</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="668"/>
        <source>Exit this QMC2 variant when launching another?</source>
        <translation>Sair desta variação do QMC2 quando iniciar outra?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="671"/>
        <source>Exit on variant launch</source>
        <translation>Sair ao iniciar variação</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="678"/>
        <source>Log font</source>
        <translation>Fonte do log</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="691"/>
        <source>Font used in logs (= application font if empty)</source>
        <translation>Fonte usada nos logs (= fonte da aplicação se vazio)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="707"/>
        <source>Browse font used in logs</source>
        <translation>Procurar fonte usada nos logs</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="767"/>
        <location filename="../../options.ui" line="3757"/>
        <source>F&amp;iles / Directories</source>
        <translation>&amp;Arquivos / Diretórios</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="796"/>
        <source>Temporary file</source>
        <translation>Arquivo temporário</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="809"/>
        <source>Temporary work file (write)</source>
        <translation>Arquivo de trabalho temporário</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="822"/>
        <source>Browse temporary work file</source>
        <translation>Procurar por arquivo de trabalho temporário</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="833"/>
        <source>Front end log file</source>
        <translation>Arquivo de log do frontend</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="846"/>
        <source>Front end log file (write)</source>
        <translation>Arquivo de log do frontend (escrita)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="859"/>
        <source>Browse front end log file</source>
        <translation>Procurar por arquivo de log do frontend</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="870"/>
        <source>Favorites file</source>
        <translation>Arquivo de favoritos</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="883"/>
        <source>Game favorites file (write)</source>
        <translation>Arquivo de jogos favoritos (escrita)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="896"/>
        <source>Browse game favorites file</source>
        <translation>Procurar por arquivo de favoritos</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="907"/>
        <source>Play history file</source>
        <translation>Arquivo de histórico de jogadas</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="920"/>
        <source>Play history file (write)</source>
        <translation>Arquivo de histórico de jogadas (escrita)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="933"/>
        <source>Browse play history file</source>
        <translation>Procurar por arquivo de histórico de jogadas</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="944"/>
        <source>Data directory</source>
        <translation>Diretório de dados</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="957"/>
        <source>Frontend data directory (read)</source>
        <translation>Diretório de dados do frontend (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="970"/>
        <source>Browse frontend data directory</source>
        <translation>Procurar por diretório de dados do frontend</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2050"/>
        <source>Game info DB</source>
        <translation>BD de informação de jogos</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="987"/>
        <source>Game information database - MAME history.dat (read)</source>
        <translation>Base de dados de informação de jogos - history.dat do MAME (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1000"/>
        <source>Browse game information database (MAME history.dat)</source>
        <translation>Procurar por base de dados de informação de jogos (history.dat do MAME)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2088"/>
        <source>Emu info DB</source>
        <translation>BD de informação de emulador</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1017"/>
        <source>Emulator information database - mameinfo.dat (read)</source>
        <translation>Banco de dados de informação de emulador - mameinfo.dat (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1030"/>
        <source>Browse emulator information database (mameinfo.dat)</source>
        <translation>Procurar por banco de dados de informação de emulador (mameinfo.dat)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1085"/>
        <source>Switch between specifying a preview directory or a ZIP-compressed preview file</source>
        <translation>Trocar entre especificar um diretório de preview ou um arquivo comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1123"/>
        <source>Preview directory (read)</source>
        <translation>Diretório de preview (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1136"/>
        <source>Browse preview directory</source>
        <translation>Procurar diretório de preview</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1160"/>
        <source>ZIP-compressed preview file (read)</source>
        <translation>Arquivo de preview comprimido (ZIP) (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1173"/>
        <source>Browse ZIP-compressed preview file</source>
        <translation>Procurar por arquivo comprimido de preview (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1188"/>
        <source>Switch between specifying a flyer directory or a ZIP-compressed flyer file</source>
        <translation>Trocar entre especificar um diretório de flyer ou um arquivo comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1226"/>
        <source>Flyer directory (read)</source>
        <translation>Diretório de flyer (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1239"/>
        <source>Browse flyer directory</source>
        <translation>Procurar diretório de flyers</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1263"/>
        <source>ZIP-compressed flyer file (read)</source>
        <translation>Arquivo de flyers comprimido (ZIP) (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1276"/>
        <source>Browse ZIP-compressed flyer file</source>
        <translation>Procurar por arquivo comprimido de flyers (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1291"/>
        <source>Switch between specifying an icon directory or a ZIP-compressed icon file</source>
        <translation>Trocar entre especificar um diretório de ícones ou um arquivo comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1329"/>
        <source>Icon directory (read)</source>
        <translation>Diretório de ícones (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1342"/>
        <source>Browse icon directory</source>
        <translation>Procurar diretório de ícones</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1366"/>
        <source>ZIP-compressed icon file (read)</source>
        <translation>Arquivo de ícones comprimido (ZIP) (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1379"/>
        <source>Browse ZIP-compressed icon file</source>
        <translation>Procurar por arquivo comprimido de ícones (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1432"/>
        <source>Cabinet directory (read)</source>
        <translation>Diretório de gabinetes (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1445"/>
        <source>Browse cabinet directory</source>
        <translation>Procurar diretório de gabinetes</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1469"/>
        <source>ZIP-compressed cabinet file (read)</source>
        <translation>Arquivo de gabinetes comprimido (ZIP) (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1482"/>
        <source>Browse ZIP-compressed cabinet file</source>
        <translation>Procurar por arquivo comprimido de gabinetes (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1394"/>
        <source>Switch between specifying a cabinet directory or a ZIP-compressed cabinet file</source>
        <translation>Trocar entre especificar um diretório de gabinetes ou um arquivo comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1535"/>
        <source>Controller directory (read)</source>
        <translation>Diretório de controle (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1548"/>
        <source>Browse controller directory</source>
        <translation>Procurar diretório de controles</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1572"/>
        <source>ZIP-compressed controller file (read)</source>
        <translation>Arquivo de controles comprimido (ZIP) (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1585"/>
        <source>Browse ZIP-compressed controller file</source>
        <translation>Procurar por arquivo comprimido de controles (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1497"/>
        <source>Switch between specifying a controller directory or a ZIP-compressed controller file</source>
        <translation>Trocar entre especificar um diretório de controles ou um arquivo comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1638"/>
        <source>Marquee directory (read)</source>
        <translation>Diretório de marquee (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1651"/>
        <source>Browse marquee directory</source>
        <translation>Procurar diretório de marquee</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1675"/>
        <source>ZIP-compressed marquee file (read)</source>
        <translation>Arquivo de marquee comprimido (ZIP) (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1688"/>
        <source>Browse ZIP-compressed marquee file</source>
        <translation>Procurar por arquivo comprimido de marquee (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1600"/>
        <source>Switch between specifying a marquee directory or a ZIP-compressed marquee file</source>
        <translation>Trocar entre especificar um diretório de marquee ou um arquivo comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1741"/>
        <source>Title directory (read)</source>
        <translation>Diretório de títulos (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1754"/>
        <source>Browse title directory</source>
        <translation>Procurar diretório de títulos</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1778"/>
        <source>ZIP-compressed title file (read)</source>
        <translation>Arquivo de títulos comprimido (ZIP) (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1791"/>
        <source>Browse ZIP-compressed title file</source>
        <translation>Procurar por arquivo comprimido de títulos (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1703"/>
        <source>Switch between specifying a title directory or a ZIP-compressed title file</source>
        <translation>Trocar entre especificar um diretório de títulos ou um arquivo comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1806"/>
        <source>Switch between specifying a PCB directory or a ZIP-compressed PCB file</source>
        <translation>Trocar entre especificar um diretório de PCB ou um arquivo comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1844"/>
        <source>PCB directory (read)</source>
        <translation>Diretório de PCB (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1857"/>
        <source>Browse PCB directory</source>
        <translation>Procurar diretório de PCB</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1881"/>
        <source>ZIP-compressed PCB file (read)</source>
        <translation>Arquivo de PCB comprimido (ZIP) (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1894"/>
        <source>Browse ZIP-compressed PCB file</source>
        <translation>Procurar por arquivo comprimido de PCB (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1041"/>
        <source>Enable the use of catver.ini -- get the newest version from http://www.progettoemma.net/?catlist</source>
        <translation>Habilitar o uso do arquivo catver.ini -- obtenha a versão mais nova de http://www.progettoemma.net/?catlist</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1044"/>
        <source>Use catver.ini</source>
        <translation>Usar catver.ini</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1061"/>
        <source>Path to catver.ini (read)</source>
        <translation>Caminho para catver.ini (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1074"/>
        <source>Browse path to catver.ini</source>
        <translation>Procurar caminha para catver.ini</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2123"/>
        <source>Switch between specifying a software snap directory or a ZIP-compressed software snap file</source>
        <translation>Trocar entre especificar um diretório de snapshots de software ou um arquivo comprimido (ZIP)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2126"/>
        <location filename="../../options.ui" line="2139"/>
        <location filename="../../options.cpp" line="1940"/>
        <location filename="../../options.cpp" line="1997"/>
        <location filename="../../options.cpp" line="2935"/>
        <source>SW snap folder</source>
        <translation>Diretório de snapshot de software</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1931"/>
        <source>Software snap-shot directory (read)</source>
        <translation>Diretório de snapshots de software (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1944"/>
        <source>Browse software snap-shot directory</source>
        <translation>Procurar pelo diretório de snapshots de software</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1968"/>
        <source>ZIP-compressed software snap-shot file (read)</source>
        <translation>Arquivo comprimido de snapshots de software (ZIP) (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="1981"/>
        <source>Browse ZIP-compressed software snap-shot file</source>
        <translation>Procurar o arquivo comprimido ZIP de snapshots de software</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2155"/>
        <source>Game &amp;list</source>
        <translation>&amp;Lista de jogos</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2184"/>
        <source>ROM state filter</source>
        <translation>Filtro de estado da ROM</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2199"/>
        <source>Show ROM state C (correct)?</source>
        <translation>Mostrar ROM com estado C (correto)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2222"/>
        <source>Show ROM state M (mostly correct)?</source>
        <translation>Mostrar ROM com estado M (Maioria correto)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2245"/>
        <source>Show ROM state I (incorrect)?</source>
        <translation>Mostrar estado de ROM I (incorreto)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2268"/>
        <source>Show ROM state N (not found)?</source>
        <translation>Mostrar estado de ROM N (não encontrado)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2291"/>
        <source>Show ROM state U (unknown)?</source>
        <translation>Mostrar estado de ROM D (desconhecido)?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2310"/>
        <source>Sort criteria</source>
        <translation>Critério de ordenação</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2323"/>
        <source>Select sort criteria</source>
        <translation>Selecione um critério de ordenação</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2327"/>
        <source>Game description</source>
        <translation>Descrição do jogo</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2332"/>
        <source>ROM state</source>
        <translation>Estado da ROM</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2342"/>
        <source>Year</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2347"/>
        <source>Manufacturer</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2352"/>
        <source>Game name</source>
        <translation>Nome do jogo</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2357"/>
        <source>ROM types</source>
        <translation>Tipos de ROM</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2362"/>
        <source>Players</source>
        <translation>Jogadores</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2375"/>
        <source>Sort order</source>
        <translation>Ordem</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2388"/>
        <source>Select sort order</source>
        <translation>Selecione a ordem</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2392"/>
        <source>Ascending</source>
        <translation>Ascendente</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2401"/>
        <source>Descending</source>
        <translation>Descendente</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2413"/>
        <source>Responsiveness</source>
        <translation>Disponibilidade</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2426"/>
        <source>Number of item insertions between game list updates during reload (higher means faster, but makes the GUI less responsive)</source>
        <translation>Número de itens inseridos entre as atualizações da lista de jogos durante o recarregamento (maior significa mais rápido, mas torna a interface menos disponível)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2429"/>
        <source>immediate</source>
        <translation>imediata</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2445"/>
        <source>Update delay</source>
        <translation>Atraso de atualização</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2458"/>
        <source>Delay update of any game details (preview, flyer, info, configuration, ...) by how many milliseconds?</source>
        <translation>Atrasar atualização dos qualquer detalhe do jogo (preview, flyer, informações, configurações, ....) por quantos milisegundos?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2461"/>
        <source>none</source>
        <translation>nenhum</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2464"/>
        <source> ms</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2662"/>
        <source>Sort game list while reloading (slower)</source>
        <translation>Ordenar lista de jogos enquanto recarregando (mais lento)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2665"/>
        <source>Sort while loading</source>
        <translation>Ordenar enquanto recarrega</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2614"/>
        <source>Automatically trigger a ROM check if necessary</source>
        <translation>Verificar ROM automaticamente se necessário</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2617"/>
        <source>Auto-trigger ROM check</source>
        <translation>Verificar ROM automaticamente</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2515"/>
        <source>Display ROM status icons in master lists?</source>
        <translation>Mostrar ícones de estado da ROM nas listas principais?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2518"/>
        <source>Show ROM status icons</source>
        <translation>Mostrar ícones de estado da ROM</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4133"/>
        <source>Software list cache file (write)</source>
        <translation>Arquivo de cache de lista de jogos (escrita)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4146"/>
        <source>Browse software list cache file</source>
        <translation>Procurar arquivo de cache de lista de jogos</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4170"/>
        <source>Directory used as the default software folder for the MESS device configurator (if a sub-folder named as the current machine exists, that folder will be selected instead)</source>
        <translation>Diretório utilizado como diretório padrão de software para o configurador de dispositivos do MESS (se um subdiretório com o mesmo nome da máquina existir, essa pasta será selecionada no lugar)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2486"/>
        <source>Launch emulation on double-click events (may be annoying)</source>
        <translation>Iniciar emulador por clique duplo (pode ser estranho)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2489"/>
        <source>Double-click activation</source>
        <translation>Ativação por clique duplo</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2646"/>
        <source>Hide primary game list while loading (recommended, much faster)</source>
        <translation>Esconder a lista primária de jogos enquanto recarrega (recomendado, muito mais rápido)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2649"/>
        <source>Hide while loading</source>
        <translation>Esconder enquanto carrega</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2630"/>
        <source>Launch emulation directly when an item is activated in the search-, favorites- or played-lists (instead of jumping to the master list)</source>
        <translation>Iniciar emulação diretamente quando um item é ativado na lista de busca, favoritos ou jogados (ao invés de pular para a lista principal)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2633"/>
        <source>Play on sub-list activation</source>
        <translation>Jogar com ativação na sub-lista</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2578"/>
        <source>Select the cursor position QMC2 uses when auto-scrolling to the current item (this setting applies to all views and lists!)</source>
        <translation>Escolher posição do cursor a ser usado pelo QMC2 quando fizer rolagem automática para o ítem atual (essa configuração se aplica a todas as visões e listas!)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2585"/>
        <source>Visible</source>
        <translation>Visível</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2590"/>
        <source>Top</source>
        <translation>Em cima</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2595"/>
        <source>Bottom</source>
        <translation>Em baixo</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2600"/>
        <source>Center</source>
        <translation>Centro</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2565"/>
        <source>Cursor position</source>
        <translation>Posição do cursor</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2672"/>
        <source>SW snap position</source>
        <translation>Posição do snapshot de software</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2689"/>
        <source>Above / Left</source>
        <translation>Acima / Esquerda</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2694"/>
        <source>Above / Center</source>
        <translation>Acima / Centro</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2699"/>
        <source>Above / Right</source>
        <translation>Acima / Direita</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2704"/>
        <source>Below / Left</source>
        <translation>Abaixo / Esquerda</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2709"/>
        <source>Below / Center</source>
        <translation>Abaixo / Centro</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2714"/>
        <source>Below / Right</source>
        <translation>Abaixo / Direita</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2744"/>
        <source>&amp;Shortcuts / Keys</source>
        <translation>Atalho&amp;s / Teclas</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2776"/>
        <location filename="../../options.ui" line="2779"/>
        <source>Redefine key sequence</source>
        <translation>Redefinir sequência de teclas</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2793"/>
        <source>Reset key sequence to default</source>
        <translation>Redefinir sequência de teclas para o padrão</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2796"/>
        <source>Reset</source>
        <translation>Redefinir</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2813"/>
        <source>Active shortcut definitions; double-click to redefine key sequence</source>
        <translation>Definições ativas de atalhos: clique duplo para redefinir sequência de teclas</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2832"/>
        <location filename="../../options.ui" line="3288"/>
        <source>Function / Key</source>
        <translation>Função / Tecla</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3314"/>
        <source>Proxy / &amp;Tools</source>
        <translation>Proxy / &amp;Ferramentas</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3488"/>
        <source>ROM tool</source>
        <translation>Ferramenta de ROM</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3507"/>
        <source>External ROM tool (it&apos;s completely up to you...)</source>
        <translation>Ferramenta de ROM externa (a decisão é toda sua...)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3520"/>
        <source>Browse ROM tool</source>
        <translation>Procurar ferramenta de ROM</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3544"/>
        <source>ROM tool argument list (i. e. &quot;$ID$ $DESCRIPTION$&quot;)</source>
        <translation>Lista de argumentos da ferramenta de ROM (p. e. &quot;$ID $DESCRIÇÃO$&quot;)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3564"/>
        <source>Working directory that&apos;s used when the ROM tool is executed (if empty, QMC2&apos;s current working directory is used)</source>
        <translation>Diretório de trabalho usado quando a ferramenta de ROM é executada (se vazio, o diretório de trabalho atual do QMC2 será usado)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3594"/>
        <source>Copy the tool&apos;s output to the front end log (keeping it for debugging)</source>
        <translation>Copiar a saída da ferramenta para o log do programa (mantenha para verificação)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3597"/>
        <source>Copy tool output to front end log</source>
        <translation>Copiar a saída da ferramenta para o log do frontend</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3604"/>
        <source>Automatically close the tool-executor dialog when the external process finished</source>
        <translation>Fechar automaticamente o diálog de execução da ferramenta quando o processo externo terminar</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3607"/>
        <source>Close dialog automatically</source>
        <translation>Fechar diálogo automaticamente</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3920"/>
        <source>XML game list cache</source>
        <translation>Cache da lista de jogos (XML)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3957"/>
        <source>Game list cache</source>
        <translation>Cache da lista de jogos</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2837"/>
        <location filename="../../options.cpp" line="386"/>
        <location filename="../../options.cpp" line="387"/>
        <location filename="../../options.cpp" line="3392"/>
        <source>Default</source>
        <translation>Padrão</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="148"/>
        <source>Save game selection on exit and before reloading the game list</source>
        <translation>Salvar seleção de jogos ao sair e antes de recarregar a lista de jogos</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="168"/>
        <source>Restore saved game selection at start and after reloading the game list</source>
        <translation>Restaurar seleção de jogos ao iniciar e antes de recarregar a lista de jogos</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="748"/>
        <source>Use a unifed tool- and title-bar on Mac OS X</source>
        <translation>Usar uma ferramenta unificada - e barra de títulos no Mac OS X</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="751"/>
        <source>Unify with title</source>
        <translation>Unificar com título</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2337"/>
        <source>Tag</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2367"/>
        <source>Driver status</source>
        <translation>Estado do driver</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2685"/>
        <source>Select the position where sofware snap-shots are displayed within software lists</source>
        <translation>Selecione a posição onde os snapshots de software são mostrados dentro da lista de software</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2719"/>
        <source>Disable snaps</source>
        <translation>Desabilitar snapshots</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2727"/>
        <source>Display software snap-shots when hovering the software list with the mouse cursor</source>
        <translation>Mostrar snapshots de software quando o mouse passar pela lista de software</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2730"/>
        <source>SW snaps on mouse hover</source>
        <translation>Snapshots de software ao passar o mouse</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2842"/>
        <source>Custom</source>
        <translation>Personalizado</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2857"/>
        <source>&amp;Joystick</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2886"/>
        <source>Enable GUI control via joystick</source>
        <translation>Habilitar controle da interface via joystick</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2889"/>
        <source>Enable joystick control</source>
        <translation>Habilitar controle via joystick</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2905"/>
        <source>Rescan available joysticks</source>
        <translation>Re-escanear joysticks disponíveis</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2908"/>
        <source>Rescan joysticks</source>
        <translation>Re-escanear joysticks</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2928"/>
        <source>Select joystick</source>
        <translation>Selecionar Joystick</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2938"/>
        <source>List of available joysticks - select the one you want to use for GUI control</source>
        <translation>Lista de joysticks disponíveis - selecione aquele que você quer usar para controle da interface</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2953"/>
        <source>Joystick information and settings</source>
        <translation>Informação e configuração do Joystick</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2963"/>
        <source>Axes:</source>
        <translation>Eixos:</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2970"/>
        <source>Number of joystick axes</source>
        <translation>Número de eixos do joystick</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2973"/>
        <location filename="../../options.ui" line="2994"/>
        <location filename="../../options.ui" line="3015"/>
        <location filename="../../options.ui" line="3036"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2984"/>
        <source>Buttons:</source>
        <translation>Botões:</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="2991"/>
        <source>Number of joystick buttons</source>
        <translation>Número de botões do joystick</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3005"/>
        <source>Hats:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3012"/>
        <source>Number of coolie hats</source>
        <translation>Número de coolie hats</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3026"/>
        <source>Trackballs:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3033"/>
        <source>Number of trackballs</source>
        <translation>Número de trackballs</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3062"/>
        <source>Automatically repeat joystick functions after specified delay</source>
        <translation>Repetir automaticamente funções do joystick após um atraso especificado</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3065"/>
        <source>Auto repeat after</source>
        <translation>Repetir automaticamente após</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3081"/>
        <source>Repeat all joystick functions after how many milliseconds?</source>
        <translation>Repetir todas as funções do joystick após quantos milisegundos?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3084"/>
        <location filename="../../options.ui" line="3129"/>
        <source>ms</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3110"/>
        <source>Event timeout</source>
        <translation>Tempo máximo do evento</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3126"/>
        <source>Process joystick events after how many milliseconds?</source>
        <translation>Processar eventos de joystick após quantos milisegundos?</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3152"/>
        <source>Calibrate joystick axes</source>
        <translation>Calibrar eixos do joystick</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3155"/>
        <source>Calibrate</source>
        <translation>Calibrar</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3174"/>
        <source>Test all joystick functions</source>
        <translation>Testar todas as funções do joystick</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3177"/>
        <source>Test</source>
        <translation>Testar</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3196"/>
        <source>Map joystick functions to GUI functions</source>
        <translation>Mapear funções do joystick para funções da interface</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3199"/>
        <source>Map</source>
        <translation>Mapear</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3232"/>
        <source>Remap a joystick function to the selected GUI function</source>
        <translation>Remapear uma função do joystick para uma função selecionada da interface</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3235"/>
        <source>Remap</source>
        <translation>Remapear</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3249"/>
        <source>Remove joystick mapping from selected GUI function</source>
        <translation>Remover mapeamento do joystick para a função da interface selecionada</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3252"/>
        <source>Remove</source>
        <translation>Remover</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3269"/>
        <source>Active joystick mappings; double-click to remap joystick function</source>
        <translation>Mapeamentos ativos do joystick: clique duplo para remapear as funções do joystick</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3293"/>
        <source>Joystick function</source>
        <translation>Função do joystick</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3343"/>
        <source>Zip tool</source>
        <translation>Ferramenta zip</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3349"/>
        <location filename="../../options.ui" line="3415"/>
        <location filename="../../options.ui" line="3494"/>
        <source>Command</source>
        <translation>Comando</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3362"/>
        <source>External zip tool, i.e. &quot;zip&quot; (read and execute)</source>
        <translation>Ferramenta externa de zip, p.e. &quot;zip&quot; (ler e executar)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3375"/>
        <source>Browse for zip tool</source>
        <translation>Procurar pela ferramenta de zip</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3386"/>
        <location filename="../../options.ui" line="3452"/>
        <location filename="../../options.ui" line="3531"/>
        <location filename="../../options.ui" line="4388"/>
        <location filename="../../options.ui" line="4497"/>
        <source>Arguments</source>
        <translation>Argumentos</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3399"/>
        <source>Zip tool argument list to remove entries from the ZIP archive (i. e. &quot;$ARCHIVE$ -d $FILELIST$&quot;)</source>
        <translation>Lista de argumentos da ferramenta zip para remover entradas de um arquivo zip (p. e. &quot;$ARCHIVE$ -d $FILELIST$&quot;)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3409"/>
        <source>File removal tool</source>
        <translation>Ferramenta de remoção de arquivo</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3428"/>
        <source>External file removal tool, i.e. &quot;rm&quot; (read and execute)</source>
        <translation>Ferramenta externa de remoção de arquivo, p.e. &quot;rm&quot; (ler e executar)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3441"/>
        <source>Browse for file removal tool</source>
        <translation>Procurar pela ferramenta de remoção de arquivos</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3465"/>
        <source>File removal tool argument list (i. e. &quot;-f -v $FILELIST$&quot;)</source>
        <translation>Lista de argumentos da ferramenta de remoção de arquivo (p. e. &quot;-f -v $FILELIST$&quot;)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3577"/>
        <source>Browse working directory of the ROM tool</source>
        <translation>Procurar por diretório de trabalho da ferramenta de ROM</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3617"/>
        <source>Enable / disable the use of an HTTP proxy on any web lookups</source>
        <translation>Habilitar / desabilitar uso de proxy HTTP em qualquer acesso à web</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3620"/>
        <source>Use HTTP proxy</source>
        <translation>Usar proxy HTTP</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3632"/>
        <source>Host / IP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3645"/>
        <source>Hostname or IP address of the HTTP proxy server</source>
        <translation>Nome ou endereço IP do servidor de proxy HTTP</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3652"/>
        <source>Port</source>
        <translation>Porta</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3665"/>
        <source>Port to access the HTTP proxy service</source>
        <translation>Porta para acessar o serviço de proxy HTTP</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3678"/>
        <source>User ID</source>
        <translation>ID do usuário</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3691"/>
        <source>User ID to access the HTTP proxy service (empty = no authentication)</source>
        <translation>ID do usuário para acessar o serviço de proxy HTTP (vazio = sem autenticação)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3698"/>
        <source>Password</source>
        <translation>Senha</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3711"/>
        <source>Password to access the HTTP proxy service (empty = no authentication)</source>
        <translation>Senha para acessar o serviço de proxy HTTP (vazio = sem autenticação)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3721"/>
        <source>&lt;font size=&quot;-1&quot;&gt;&lt;b&gt;WARNING: stored passwords are &lt;u&gt;weakly&lt;/u&gt; encrypted!&lt;/b&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&quot;-1&quot;&gt;&lt;b&gt;AVISO: senhas salvas são &lt;u&gt;fracamente&lt;/u&gt; encriptadas!&lt;/b&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3742"/>
        <source>E&amp;mulator</source>
        <translation>E&amp;mulador</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3752"/>
        <source>&amp;Global configuration</source>
        <translation>Configuração &amp;Global</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3807"/>
        <source>Executable file</source>
        <translation>Arquivo executável</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3822"/>
        <source>Emulator executable file (read and execute)</source>
        <translation>Arquivo executável do emulador (leitura e execução)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3835"/>
        <location filename="../../options.ui" line="4483"/>
        <source>Browse emulator executable file</source>
        <translation>Procurar pelo executável do emulador</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3846"/>
        <source>Emulator log file</source>
        <translation>Arquivo de log do emulador</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3859"/>
        <source>Emulator log file (write)</source>
        <translation>Arquivo de log do emulador (escrita)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3872"/>
        <source>Browse emulator log file</source>
        <translation>Procurar pelo arquivo de log do emulador</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3883"/>
        <source>Options template file</source>
        <translation>Arquivo de template de opções</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3896"/>
        <source>Options template file (read)</source>
        <translation>Arquivo de template de opções (leitura)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3909"/>
        <source>Browse options template file</source>
        <translation>Procurar arquivo de template de opções</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3933"/>
        <source>Cache file for the output of mame -listxml / -lx (write)</source>
        <translation>Arquivo de cache para a saída de mame -listxml / -lx (escrita)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3946"/>
        <source>Browse XML gamelist cache file</source>
        <translation>Procurar arquivo de cache da lista de jogos (XML)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3970"/>
        <source>Gamelist cache file (write)</source>
        <translation>Cache da lista de jogos (escrita)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3983"/>
        <source>Browse gamelist cache file</source>
        <translation>Procurar pelo arquivo de cache da lista de jogos</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3994"/>
        <source>ROM state cache</source>
        <translation>Cache de estado das ROMs</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4007"/>
        <source>ROM state cache file (write)</source>
        <translation>Cache de estado das ROMs (escrita)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4020"/>
        <source>Browse ROM state cache file</source>
        <translation>Procurar arquivo de cache de estado das ROMs</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4080"/>
        <source>MAWS cache directory</source>
        <translation>Diretório do cache do MAWS</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4093"/>
        <source>MAWS cache directory (write)</source>
        <translation>Diretório do cache do MAWS (escrita)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4106"/>
        <source>Browse MAWS cache directory</source>
        <translation>Procurar pelo diretório de cache do MAWS</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4120"/>
        <source>Software list cache</source>
        <translation>Cache da lista de Software</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4194"/>
        <source>MAME variant exe</source>
        <translation>Exe da variante do MAME</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4207"/>
        <source>Specify the exe file for the MAME variant (leave empty when all variants are installed in the same directory)</source>
        <translation>Especifique o arquivo exe para a variante do MAME (deixe em branco quando todas as variantes estão instaladas no mesmo diretório)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4220"/>
        <source>Browse MAME variant exe</source>
        <translation>Procurar exe do variante do MAME</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4231"/>
        <source>MESS variant exe</source>
        <translation>Exe da variante do MESS</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4244"/>
        <source>Specify the exe file for the MESS variant (leave empty when all variants are installed in the same directory)</source>
        <translation>Especifique o arquivo exe para a variante do MESS (deixe em branco quando todas as variantes estão instaladas no mesmo diretório)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4257"/>
        <source>Browse MESS variant exe</source>
        <translation>Procurar exe do variante do MESS</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="3551"/>
        <location filename="../../options.ui" line="4268"/>
        <location filename="../../options.ui" line="4383"/>
        <location filename="../../options.ui" line="4528"/>
        <source>Working directory</source>
        <translation>Diretório de trabalho</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4281"/>
        <location filename="../../options.ui" line="4541"/>
        <source>Working directory that&apos;s used when the emulator is executed (if empty, QMC2&apos;s current working directory is used)</source>
        <translation>Diretório de trabalho que é usado quando o emulador é executado (se vazio, o diretório de trabalho do QMC2 é usado)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4294"/>
        <location filename="../../options.ui" line="4554"/>
        <source>Browse working directory</source>
        <translation>Procurar diretório de trabalho</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4157"/>
        <source>General software folder</source>
        <translation>Diretório geral de software</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4183"/>
        <source>Browse general software folder</source>
        <translation>Procurar diretório geral de software</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4312"/>
        <source>Additional &amp;Emulators</source>
        <translation>&amp;Emuladores Adicionais</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4339"/>
        <source>Registered emulators -- you may select one of these in the game-specific emulator configuration</source>
        <translation>Emuladores registrados -- você pode selecionar um desses na configuração de emulador específica de jogo</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4373"/>
        <location filename="../../options.ui" line="4443"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4378"/>
        <location filename="../../options.ui" line="4457"/>
        <source>Executable</source>
        <translation>Executável</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4399"/>
        <source>Register emulator</source>
        <translation>Registrar emulador</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4429"/>
        <source>Deregister emulator</source>
        <translation>Desregistrar emulador</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4450"/>
        <source>Registered emulator&apos;s name</source>
        <translation>Nome do emulador registrado</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4470"/>
        <source>Command to execute the emulator (path to the executable file)</source>
        <translation>Comando para executar o emulador (caminho para o arquivo executável)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4504"/>
        <source>Arguments passed to the emulator -- use $ID$ as a placeholder for the unique game/machine ID (its short name)</source>
        <translation>Argumentos passados para o emulador -- use $ID$ como o ID para o nome do jogo/máquina (seu nome curto)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4514"/>
        <source>Replace emulator registration</source>
        <translation>Substituir registro do emulador</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4582"/>
        <source>Apply settings</source>
        <translation>Aplicar configurações</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4585"/>
        <source>&amp;Apply</source>
        <translation>&amp;Aplicar</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4596"/>
        <source>Restore currently applied settings</source>
        <translation>Restaurar configurações atualmente aplicadas</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4599"/>
        <source>&amp;Restore</source>
        <translation>&amp;Restaurar</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4610"/>
        <source>Reset to default settings (click &lt;i&gt;Restore&lt;/i&gt; to restore currently applied settings!)</source>
        <translation>Redefinir para configurações padrão (clique &lt;i&gt;Restaurar&lt;/i&gt; para restaurar configurações aplicadas atualmente!)</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4613"/>
        <source>&amp;Default</source>
        <translation>&amp;Padrão</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4637"/>
        <source>Close and apply settings</source>
        <translation>Fechar e aplicar configurações</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4640"/>
        <source>&amp;Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4647"/>
        <source>Close and discard changes</source>
        <translation>Fechar e discartar mudanças</translation>
    </message>
    <message>
        <location filename="../../options.ui" line="4650"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
</context>
<context>
    <name>PCB</name>
    <message>
        <location filename="../../pcb.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>Copiar para área de transferência</translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="56"/>
        <location filename="../../pcb.cpp" line="57"/>
        <source>Game PCB image</source>
        <translation>Imagem de PCB do jogo</translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="59"/>
        <location filename="../../pcb.cpp" line="60"/>
        <source>Machine PCB image</source>
        <translation>Imagem de PCB da máquina</translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="68"/>
        <location filename="../../pcb.cpp" line="72"/>
        <source>FATAL: can&apos;t open PCB file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo de PCB. por favor verifique as permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../pcb.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Aguardando dados...</translation>
    </message>
</context>
<context>
    <name>Preview</name>
    <message>
        <location filename="../../preview.cpp" line="51"/>
        <source>Copy to clipboard</source>
        <translation>Copiar para área de transferência</translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="58"/>
        <location filename="../../preview.cpp" line="59"/>
        <source>Game preview image</source>
        <translation>Imagem de preview do jogo</translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="61"/>
        <location filename="../../preview.cpp" line="62"/>
        <source>Machine preview image</source>
        <translation>Imagem de preview da máquina</translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="70"/>
        <location filename="../../preview.cpp" line="74"/>
        <source>FATAL: can&apos;t open preview file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo de preview. por favor verifique as permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../preview.cpp" line="102"/>
        <source>Waiting for data...</source>
        <translation>Aguardando dados...</translation>
    </message>
</context>
<context>
    <name>ProcessManager</name>
    <message>
        <location filename="../../procmgr.cpp" line="61"/>
        <source>WARNING: ProcessManager::start(): the specified working directory &apos;%1&apos; is not a directory -- ignored</source>
        <translation>AVISO: ProcessManager::start(): o diretório de trabalho especificado &apos;%1&apos; não é um diretório -- ignorado</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="64"/>
        <source>WARNING: ProcessManager::start(): the specified working directory &apos;%1&apos; does not exist -- ignored</source>
        <translation>AVISO: ProcessManager::start(): o diretório de trabalho especificado &apos;%1&apos; não existe -- ignorado</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="99"/>
        <location filename="../../procmgr.cpp" line="101"/>
        <source>starting emulator #%1, command = %2</source>
        <translation>iniciando emulador #%1, comando = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="131"/>
        <source>terminating emulator #%1, PID = %2</source>
        <translation>terminando emulador #%1, PID = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="145"/>
        <source>WARNING: ProcessManager::terminate(ushort index = %1): trying to terminate a null process</source>
        <translation>AVISO: ProcessManager::terminate(ushort index = %1): tentando terminar um processo nulo</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="154"/>
        <source>killing emulator #%1, PID = %2</source>
        <translation>matando emulador #%1, PID = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="168"/>
        <source>WARNING: ProcessManager::kill(ushort index = %1): trying to kill a null process</source>
        <translation>AVISO: ProcessManager::kill(ushort index = %1): tentando matar um processo nulo</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="185"/>
        <source>stdout[#%1]:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="203"/>
        <source>stderr[#%1]:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="233"/>
        <source>WARNING: ProcessManager::finished(...): trying to remove a null item</source>
        <translation>AVISO: ProcessManager::finished(...): tentando matar um item nulo</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="236"/>
        <source>emulator #%1 finished, exit code = %2, exit status = %3, remaining emulators = %4</source>
        <translation>emulador #%1 terminou, código de saída = %2, estado da saída = %3, emuladores restantes = %4</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="236"/>
        <source>normal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="236"/>
        <source>crashed</source>
        <translation>quebrado</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="281"/>
        <source>emulator #%1 started, PID = %2, running emulators = %3</source>
        <translation>emulador #%1 iniciado, PID = %2, emuladores em execução = %3</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="320"/>
        <source>FATAL: failed to start emulator #%1</source>
        <translation>FATAL: falha ao iniciar o emulador #%1</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="325"/>
        <source>WARNING: emulator #%1 crashed</source>
        <translation>AVISO: emulador #%1 quebrou</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="329"/>
        <source>WARNING: failed to write to emulator #%1</source>
        <translation>AVISO: falha ao escrever para emulador #%1</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="333"/>
        <source>WARNING: failed to read from emulator #%1</source>
        <translation>AVISO: falha para ler do emulador #%1</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="337"/>
        <source>WARNING: unhandled error for emulator #%1, error code = %2</source>
        <translation>AVISO: erro não tratado para o emulador #%1, código de erro = %2</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="355"/>
        <source>no error</source>
        <translation>nenhum erro</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="356"/>
        <source>failed validity checks</source>
        <translation>checagens de validade falharam</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="357"/>
        <source>missing files</source>
        <translation>arquivos faltantes</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="358"/>
        <source>fatal error</source>
        <translation>erro fatal</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="359"/>
        <source>device initialization error</source>
        <translation>erro na inicialização do dispositivo</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="361"/>
        <source>game doesn&apos;t exist</source>
        <translation>jogo não existe</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="363"/>
        <source>machine doesn&apos;t exist</source>
        <translation>máquina  não existe</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="365"/>
        <source>invalid configuration</source>
        <translation>configuração inválida</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="366"/>
        <source>identified all non-ROM files</source>
        <translation>todos os arquivos não-ROM identificados</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="367"/>
        <source>identified some files but not all</source>
        <translation>nem todos os arquivos foram identificados</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="368"/>
        <source>identified no files</source>
        <translation>nenhum arquivo identificado</translation>
    </message>
    <message>
        <location filename="../../procmgr.cpp" line="369"/>
        <source>unknown error</source>
        <translation>erro desconhecido</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../gamelist.cpp" line="2086"/>
        <location filename="../../options.cpp" line="1409"/>
        <source>players</source>
        <translation>jogadores</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2093"/>
        <location filename="../../options.cpp" line="1416"/>
        <source>category</source>
        <translation>categoria</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2096"/>
        <location filename="../../options.cpp" line="1419"/>
        <source>version</source>
        <translation>versão</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2083"/>
        <location filename="../../options.cpp" line="1406"/>
        <source>ROM types</source>
        <translation>Tipos de ROM</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2059"/>
        <location filename="../../options.cpp" line="1381"/>
        <source>game description</source>
        <translation>descrição do jogo</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2061"/>
        <location filename="../../options.cpp" line="1383"/>
        <source>machine description</source>
        <translation>descrição da máquina</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2065"/>
        <location filename="../../options.cpp" line="1387"/>
        <source>ROM state</source>
        <translation>estado da ROM</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2068"/>
        <location filename="../../options.cpp" line="1390"/>
        <source>tag</source>
        <translation>etiqueta</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2071"/>
        <location filename="../../options.cpp" line="1393"/>
        <source>year</source>
        <translation>ano</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2074"/>
        <location filename="../../options.cpp" line="1396"/>
        <source>manufacturer</source>
        <translation>fabricante</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2078"/>
        <location filename="../../options.cpp" line="1400"/>
        <source>game name</source>
        <translation>nome do jogo</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2080"/>
        <location filename="../../options.cpp" line="1402"/>
        <source>machine name</source>
        <translation>nome da máquina</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="2089"/>
        <location filename="../../options.cpp" line="1412"/>
        <source>driver status</source>
        <translation>estado do driver</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3012"/>
        <source>correct</source>
        <translation>correto</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3047"/>
        <source>incorrect</source>
        <translation>incorreto</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3082"/>
        <source>mostly correct</source>
        <translation>maioria correto</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3117"/>
        <source>not found</source>
        <translation>não encontrado</translation>
    </message>
    <message>
        <location filename="../../gamelist.cpp" line="3152"/>
        <location filename="../../gamelist.cpp" line="3190"/>
        <location filename="../../romalyzer.cpp" line="2843"/>
        <location filename="../../romalyzer.cpp" line="2876"/>
        <location filename="../../romalyzer.cpp" line="2888"/>
        <source>unknown</source>
        <translation>desconhecido</translation>
    </message>
    <message>
        <location filename="../../options.cpp" line="802"/>
        <location filename="../../options.cpp" line="1845"/>
        <source>Default</source>
        <translation>Padrão</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3597"/>
        <location filename="../../qmc2main.cpp" line="3598"/>
        <source>Export game-specific MAME configuration</source>
        <translation>Exportar configuração do MAME específica de jogo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3600"/>
        <location filename="../../qmc2main.cpp" line="3601"/>
        <source>Export machine-specific MESS configuration</source>
        <translation>Exportar configuração do MESS específica de máquina</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3603"/>
        <location filename="../../qmc2main.cpp" line="9499"/>
        <source>Import from...</source>
        <translation>Importar de...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3605"/>
        <location filename="../../qmc2main.cpp" line="3606"/>
        <source>Import game-specific MAME configuration</source>
        <translation>Importar configuração do MAME específica de jogo</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="3608"/>
        <location filename="../../qmc2main.cpp" line="3609"/>
        <source>Import machine-specific MESS configuration</source>
        <translation>Importar configuração do MAME específica de máquina</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9438"/>
        <source>M.E.S.S. Catalog / Launcher II v</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9440"/>
        <location filename="../../qmc2main.cpp" line="9442"/>
        <source>M.A.M.E. Catalog / Launcher II v</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9446"/>
        <source>SVN r%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9464"/>
        <source>OpenGL features enabled</source>
        <translation>Recursos OpenGL habilitados</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9468"/>
        <source>Phonon features enabled - using Phonon v%1</source>
        <translation>Recursos Phonon habilitados - usando Phonon v%1</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9473"/>
        <source>SDL joystick support enabled - using SDL v%1.%2.%3</source>
        <translation>Suporte do SDL à joystick habilitado - usando SDL v%1.%2.%3</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9477"/>
        <source>processing global emulator configuration</source>
        <translation>processando configuração global do emulador</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9491"/>
        <source>Export to...</source>
        <translation>Exportar para...</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9493"/>
        <location filename="../../qmc2main.cpp" line="9494"/>
        <source>Export global MAME configuration</source>
        <translation>Exportar configuração global do MAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9496"/>
        <location filename="../../qmc2main.cpp" line="9497"/>
        <source>Export global MESS configuration</source>
        <translation>Exportar configuração global do MESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9501"/>
        <location filename="../../qmc2main.cpp" line="9502"/>
        <source>Import global MAME configuration</source>
        <translation>Importar configuração global do MAME</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9504"/>
        <location filename="../../qmc2main.cpp" line="9505"/>
        <source>Import global MESS configuration</source>
        <translation>Importar configuração global do MESS</translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9517"/>
        <location filename="../../qmc2main.cpp" line="9525"/>
        <source>&lt;inipath&gt;/mame.ini</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9519"/>
        <location filename="../../qmc2main.cpp" line="9527"/>
        <source>&lt;inipath&gt;/mess.ini</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qmc2main.cpp" line="9521"/>
        <location filename="../../qmc2main.cpp" line="9529"/>
        <source>Select file...</source>
        <translation>Selecionar arquivo...</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="757"/>
        <location filename="../../romalyzer.cpp" line="2827"/>
        <source>CHD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2827"/>
        <source>ROM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2831"/>
        <location filename="../../romalyzer.cpp" line="2873"/>
        <location filename="../../romalyzer.cpp" line="2893"/>
        <source>good</source>
        <translation>bom</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="758"/>
        <location filename="../../romalyzer.cpp" line="2835"/>
        <location filename="../../romalyzer.cpp" line="2882"/>
        <source>no dump</source>
        <translation>sem dump</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2839"/>
        <location filename="../../romalyzer.cpp" line="2885"/>
        <source>bad dump</source>
        <translation>dump ruim</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2879"/>
        <source>no / bad dump</source>
        <translation>sem dump / dump ruim</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1619"/>
        <source>video player: XML error: fatal error on line %1, column %2: %3</source>
        <translation>reprodutor de vídeo: Erro no XML: erro fatal na linha %1, coluna %2: %3</translation>
    </message>
</context>
<context>
    <name>ROMAlyzer</name>
    <message>
        <location filename="../../romalyzer.cpp" line="2511"/>
        <source>checksum wizard: FATAL: can&apos;t open ZIP archive &apos;%1&apos; for reading</source>
        <translation>checksum wizard: FATAL: impossível abrir arquivo ZIP &apos;%1&apos; para leitura</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2102"/>
        <source>Reading &apos;%1&apos; - %2</source>
        <translation>Lendo &apos;%1&apos; - %2</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2129"/>
        <source>set rewriter: loading &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos; as &apos;%4&apos;</source>
        <translation>set rewriter: carregando &apos;%1&apos; com CRC &apos;%2&apos; de &apos;%3&apos; como &apos;%4&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2156"/>
        <source>set rewriter: removing redundant file &apos;%1&apos; with CRC &apos;%2&apos; from output data</source>
        <translation>set rewriter: removendo arquivo redundante &apos;%1&apos; com CRC &apos;%2&apos; dos dados de saída</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2175"/>
        <source>set rewriter: INFORMATION: no output data available, thus not rewriting set &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>set rewriter: INFORMAÇÃO: nenhum dado de saída disponível, portanto não reescrevendo conjunto &apos;%1&apos; para &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2405"/>
        <source>set rewriter: deflating &apos;%1&apos; (uncompressed size: %2)</source>
        <translation>set rewriter: desinflando &apos;%1&apos; (tamanho não comprimido: %2)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2425"/>
        <source>set rewriter: WARNING: failed to deflate &apos;%1&apos;</source>
        <translation>set rewriter: AVISO: falha ao desinflar &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2534"/>
        <source>Repairing set &apos;%1&apos; - %2</source>
        <translation>Reparando conjunto &apos;%1&apos; - %2</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2535"/>
        <source>checksum wizard: repairing %1 file &apos;%2&apos; in &apos;%3&apos; from repro template</source>
        <translation>checksum wizard: reparando arquivo %1&apos;%2&apos; em &apos;%3&apos; do modelo de reprodução</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2620"/>
        <source>checksum wizard: FATAL: can&apos;t open file &apos;%1&apos; in ZIP archive &apos;%2&apos; for writing</source>
        <translation>checksum wizard: FATAL: impossível abrir arquivo &apos;%1&apos; no arquivo ZIP &apos;%2&apos; para escrita</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2626"/>
        <source>Fixed by QMC2 v%1 (%2)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2632"/>
        <source>checksum wizard: FATAL: can&apos;t open ZIP archive &apos;%1&apos; for writing</source>
        <translation>checksum wizard: FATAL: impossível abrir arquivo ZIP &apos;%1&apos; para escrita</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2647"/>
        <source>repaired</source>
        <translation>reparado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2649"/>
        <source>checksum wizard: successfully repaired %1 file &apos;%2&apos; in &apos;%3&apos; from repro template</source>
        <translation>checksum wizard: arquivo %1 &apos;%2&apos; reparado com sucesso em &apos;%3&apos; do modelo de reprodução</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2766"/>
        <source>Choose local DB output path</source>
        <translation>Escolha o caminho local para a base de dados</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2728"/>
        <source>Connection check -- succeeded!</source>
        <translation>Verificação de conexão -- sucesso!</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2738"/>
        <source>Connection check -- failed!</source>
        <translation>Verificação de conexão -- falha!</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2729"/>
        <source>database connection check successful</source>
        <translation>verificação de conexão da base de dados concluída</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1499"/>
        <source>WARNING: unable to identify &apos;%1&apos; from &apos;%2&apos; by CRC &apos;%3&apos;</source>
        <translation>AVISO: incapaz de identificar &apos;%1&apos; de &apos;%2&apos; pelo CRC &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1496"/>
        <source>WARNING: unable to identify &apos;%1&apos; from &apos;%2&apos; by CRC (no dump exists / CRC unknown)</source>
        <translation>AVISO: incapaz de identificar &apos;%1&apos; de &apos;%2&apos; pelo CRC (dump inexistente / CRC desconhecido)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1182"/>
        <location filename="../../romalyzer.cpp" line="1206"/>
        <source>  logical size: %1 (%2 B)</source>
        <translation>  tamanho lógico: %1 (%2 B)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1519"/>
        <source>loading &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos; as &apos;%4&apos;%5</source>
        <translation>carregando &apos;%1&apos; com CRC &apos;%2&apos; de &apos;%3&apos; como &apos;%4&apos;%5</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1570"/>
        <source>WARNING: the CRC for &apos;%1&apos; from &apos;%2&apos; is unknown to the emulator, the set rewriter will use the recalculated CRC &apos;%3&apos; to qualify the file</source>
        <translation>AVISO: o CRC para &apos;%1&apos; de &apos;%2&apos; é desconhecido para o emulador, o reescritor de conjuntos usará o CRC recalculado &apos;%3&apos; para qualificar o arquivo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1572"/>
        <source>WARNING: unable to determine the CRC for &apos;%1&apos; from &apos;%2&apos;, the set rewriter will NOT store this file in the new set</source>
        <translation>AVISO: incapaz de determinar o CRC para &apos;%1&apos; de &apos;%2&apos;, o reescritor de conjuntos NÃO guardará este arquivo no novo conjunto</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1754"/>
        <source>Choose CHD manager executable file</source>
        <translation>Escolha o executável do gerenciador de CHDs</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1754"/>
        <source>All files (*)</source>
        <translation>Todos os arquivos (*)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1766"/>
        <source>Choose temporary working directory</source>
        <translation>Escolha o diretório de trabalho temporário</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1809"/>
        <source>CHD manager: external process started</source>
        <translation>CHD manager: processo externo iniciado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1820"/>
        <location filename="../../romalyzer.cpp" line="2005"/>
        <source>unknown</source>
        <translation>desconhecido</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1822"/>
        <source>normal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1823"/>
        <source>crashed</source>
        <translation>quebrado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1825"/>
        <source>CHD manager: external process finished (exit code = %1, exit status = %2)</source>
        <translation>CHD manager: processo externo finalizado (código de saída = %1, estado da saída = %2)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1841"/>
        <source>CHD manager: stdout: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1863"/>
        <source>CHD manager: stderr: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1890"/>
        <source>CHD manager: failed to start</source>
        <translation>CHD manager: falhou ao iniciar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1894"/>
        <source>CHD manager: crashed</source>
        <translation>CHD manager: quebrado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1898"/>
        <source>CHD manager: write error</source>
        <translation>CHD manager: erro de escrita</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1902"/>
        <source>CHD manager: read error</source>
        <translation>CHD manager: erro de leitura</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1906"/>
        <source>CHD manager: unknown error %1</source>
        <translation>CHD manager: erro desconhecido %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1219"/>
        <source>only CHD v3 and v4 headers supported -- rest of header information skipped</source>
        <translation>suportados cabeçalhos CHD v3 e v4 somente -- pulando resto da informação do cabeçalho</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="81"/>
        <source>Machine / File</source>
        <translation>Máquina / Arquivo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="82"/>
        <source>Select machine</source>
        <translation>Selecione a máquina</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="83"/>
        <source>Select machine in machine list if selected in analysis report?</source>
        <translation>Selecionar a máquina na lista de máquinas se selecionada na análise?</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="84"/>
        <source>Automatically scroll to the currently analyzed machine</source>
        <translation>Ir automaticamente para a máquina analisada atualmente</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="85"/>
        <source>Shortname of machine to be analyzed - wildcards allowed, use space as delimiter for multiple machines</source>
        <translation>Nome curto da máquina a ser analisada - wildcards permitidos, use espaço como delimitador para múltiplos jogos</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>none</source>
        <translation>nenhum</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>zlib</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>zlib+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="97"/>
        <source>A/V codec</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="210"/>
        <source>please wait for reload to finish and try again</source>
        <translation>por favor aguarde finalizar o recarregamento e tente novamente</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="213"/>
        <source>stopping analysis</source>
        <translation>parando análise</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="217"/>
        <source>starting analysis</source>
        <translation>iniciando análise</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="231"/>
        <source>pausing analysis</source>
        <translation>pausando análise</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="234"/>
        <source>resuming analysis</source>
        <translation>continuando análise</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="56"/>
        <location filename="../../romalyzer.cpp" line="235"/>
        <location filename="../../romalyzer.cpp" line="551"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pausar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="548"/>
        <source>&amp;Stop</source>
        <translation>P&amp;arar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="556"/>
        <source>analysis started</source>
        <translation>análise iniciada</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="558"/>
        <source>determining list of games to analyze</source>
        <translation>determinando lista de jogos para analisar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="560"/>
        <source>determining list of machines to analyze</source>
        <translation>determinando lista de máquinas para analisar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="578"/>
        <source>Searching games</source>
        <translation>Procurando jogos</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="580"/>
        <source>Searching machines</source>
        <translation>Procurando máquinas</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1391"/>
        <location filename="../../romalyzer.cpp" line="598"/>
        <location filename="../../romalyzer.cpp" line="1046"/>
        <location filename="../../romalyzer.cpp" line="2014"/>
        <location filename="../../romalyzer.cpp" line="2659"/>
        <source>Idle</source>
        <translation>Ocioso</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="606"/>
        <source>done (determining list of games to analyze)</source>
        <translation>feito (determinando lista de jogos para analisar)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="607"/>
        <source>%n game(s) to analyze</source>
        <translation>
            <numerusform>%n jogo para analisar</numerusform>
            <numerusform>%n jogos para analisar</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="609"/>
        <source>done (determining list of machines to analyze)</source>
        <translation>feito (determinando lista de máquinas para analisar)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="610"/>
        <source>%n machine(s) to analyze</source>
        <translation>
            <numerusform>%n maquina para analisar</numerusform>
            <numerusform>%n máquinas para analisar</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="619"/>
        <source>analysis paused</source>
        <translation>análise pausada</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="620"/>
        <source>&amp;Resume</source>
        <translation>&amp;Continuar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="623"/>
        <source>Paused</source>
        <translation>Pausado</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="640"/>
        <source>report limit reached, removing %n set(s) from the report</source>
        <translation>
            <numerusform>limite de relatório alcançado, removendo %n conjunto do relatório</numerusform>
            <numerusform>limite de relatório alcançado, removendo %n conjuntos do relatório</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="657"/>
        <source>analyzing &apos;%1&apos;</source>
        <translation>analisando &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="659"/>
        <source>Analyzing &apos;%1&apos;</source>
        <translation>Analisando &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="670"/>
        <source>parsing XML data for &apos;%1&apos;</source>
        <translation>parseando dados do XML para &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="677"/>
        <source>done (parsing XML data for &apos;%1&apos;)</source>
        <translation>feito (parseando dados do XML para &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="679"/>
        <source>error (parsing XML data for &apos;%1&apos;)</source>
        <translation>erro (parseando dados do XML para &apos;%1&apos;)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="688"/>
        <source>checking %n file(s) for &apos;%1&apos;</source>
        <translation>
            <numerusform>verificando %n arquivo para &apos;%1&apos;</numerusform>
            <numerusform>verificando %n arquivos para &apos;%1&apos;</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="740"/>
        <location filename="../../romalyzer.cpp" line="834"/>
        <location filename="../../romalyzer.cpp" line="911"/>
        <location filename="../../romalyzer.cpp" line="1997"/>
        <location filename="../../romalyzer.cpp" line="2475"/>
        <location filename="../../romalyzer.cpp" line="2538"/>
        <source>ROM</source>
        <translation>ROM</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="749"/>
        <location filename="../../romalyzer.cpp" line="801"/>
        <location filename="../../romalyzer.cpp" line="915"/>
        <location filename="../../romalyzer.cpp" line="949"/>
        <location filename="../../romalyzer.cpp" line="956"/>
        <source>not found</source>
        <translation>não encontrado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="720"/>
        <location filename="../../romalyzer.cpp" line="772"/>
        <location filename="../../romalyzer.cpp" line="775"/>
        <location filename="../../romalyzer.cpp" line="913"/>
        <source>skipped</source>
        <translation>pulado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="778"/>
        <source>error</source>
        <translation>erro</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="828"/>
        <source>Checksums</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="845"/>
        <source>SIZE </source>
        <translation>TAMANHO</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="860"/>
        <source>CRC </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="872"/>
        <source>SHA1 </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="897"/>
        <source>MD5 </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="911"/>
        <source>WARNING: %1 file &apos;%2&apos; loaded from &apos;%3&apos; has incorrect / unexpected checksums</source>
        <translation>AVISO: arquivo de %1 &apos;%2&apos; carregado de &apos;%3&apos; tem checksums incorretos / inesperados</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="941"/>
        <source>interrupted (checking %n file(s) for &apos;%1&apos;)</source>
        <translation>
            <numerusform>interrompido (verificando %n arquivo para &apos;%1&apos;)</numerusform>
            <numerusform>interrompido (verificando %n arquivos para &apos;%1&apos;)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="962"/>
        <source>good / not found / skipped</source>
        <translation>bom / não encontrado / pulado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="964"/>
        <source>good / not found</source>
        <translation>bom / não encontrado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="974"/>
        <source>good / skipped</source>
        <translation>bom / pulado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="883"/>
        <location filename="../../romalyzer.cpp" line="953"/>
        <location filename="../../romalyzer.cpp" line="976"/>
        <location filename="../../romalyzer.cpp" line="2260"/>
        <location filename="../../romalyzer.cpp" line="2452"/>
        <source>good</source>
        <translation>bom</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="982"/>
        <source>bad / not found / skipped</source>
        <translation>ruim / não encontrado / pulado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="984"/>
        <source>bad / not found</source>
        <translation>ruim / não encontrado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="994"/>
        <source>bad / skipped</source>
        <translation>ruim / pulado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="793"/>
        <location filename="../../romalyzer.cpp" line="814"/>
        <location filename="../../romalyzer.cpp" line="883"/>
        <location filename="../../romalyzer.cpp" line="996"/>
        <location filename="../../romalyzer.cpp" line="2261"/>
        <location filename="../../romalyzer.cpp" line="2450"/>
        <source>bad</source>
        <translation>ruim</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="1016"/>
        <source>done (checking %n file(s) for &apos;%1&apos;)</source>
        <translation>
            <numerusform>feito (verificando %n arquivo para &apos;%1&apos;)</numerusform>
            <numerusform>feito (verificando %n arquivos para &apos;%1&apos;)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1029"/>
        <source>done (analyzing &apos;%1&apos;)</source>
        <translation>feito (analisando &apos;%1&apos;)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="1031"/>
        <source>%n game(s) left</source>
        <translation>
            <numerusform>faltando %n jogo</numerusform>
            <numerusform>faltando %n jogos</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="1033"/>
        <source>%n machine(s) left</source>
        <translation>
            <numerusform>faltando %n máquina</numerusform>
            <numerusform>faltando %n máquinas</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="42"/>
        <location filename="../../romalyzer.cpp" line="1039"/>
        <source>&amp;Analyze</source>
        <translation>&amp;Analisar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1049"/>
        <source>analysis ended</source>
        <translation>análise terminada</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1049"/>
        <source>elapsed time = %1</source>
        <translation>tempo decorrido = %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="834"/>
        <location filename="../../romalyzer.cpp" line="911"/>
        <location filename="../../romalyzer.cpp" line="1106"/>
        <location filename="../../romalyzer.cpp" line="1994"/>
        <source>CHD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="126"/>
        <source>Search checksum</source>
        <translation>Soma da busca</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="135"/>
        <source>Rewrite set</source>
        <translation>Reescrever conjunto</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="142"/>
        <source>Copy to clipboard</source>
        <translation>Copiar para a área de transferência</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="822"/>
        <location filename="../../romalyzer.cpp" line="917"/>
        <source>no dump</source>
        <translation>sem dump</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="834"/>
        <source>WARNING: %1 file &apos;%2&apos; loaded from &apos;%3&apos; may be obsolete, should be merged from parent set &apos;%4&apos;</source>
        <translation>AVISO: arquivo %1 &apos;%2&apos; carregado de &apos;%3&apos; pode estar obsoleto, deveria ser misturado do conjunto pai &apos;%4&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="968"/>
        <source>good / no dump / skipped</source>
        <translation>bom / sem dump / ignorado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="970"/>
        <source>good / no dump</source>
        <translation>bom / sem dump</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="988"/>
        <source>bad / no dump / skipped</source>
        <translation>ruim / sem dump / ignorado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="990"/>
        <source>bad / no dump</source>
        <translation>ruim / sem dump</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1132"/>
        <source>size of &apos;%1&apos; is greater than allowed maximum -- skipped</source>
        <translation>tamanho de &apos;%1&apos; é maior que o máximo permitido -- pulando</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1141"/>
        <source>loading &apos;%1&apos;%2</source>
        <translation>carregando &apos;%1 &apos;%2</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1141"/>
        <location filename="../../romalyzer.cpp" line="1519"/>
        <source> (merged)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1164"/>
        <source>CHD header information:</source>
        <translation>informação do cabeçalho do CDH:</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1166"/>
        <source>  tag: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1168"/>
        <source>  version: %1</source>
        <translation>  versão: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1169"/>
        <location filename="../../romalyzer.cpp" line="1377"/>
        <source>CHD v%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1174"/>
        <location filename="../../romalyzer.cpp" line="1198"/>
        <source>  compression: %1</source>
        <translation>  compressão: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>  flags: %1, %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>has parent</source>
        <translation>tem pais</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>no parent</source>
        <translation>sem pais</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>allows writes</source>
        <translation>permite escrita</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1176"/>
        <location filename="../../romalyzer.cpp" line="1200"/>
        <source>read only</source>
        <translation>somente leitura</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1178"/>
        <location filename="../../romalyzer.cpp" line="1202"/>
        <source>  number of total hunks: %1</source>
        <translation>  número total de blocos:%1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1180"/>
        <location filename="../../romalyzer.cpp" line="1204"/>
        <source>  number of bytes per hunk: %1</source>
        <translation>  número de bytes por bloco :%1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1184"/>
        <source>  MD5 checksum: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1186"/>
        <location filename="../../romalyzer.cpp" line="1208"/>
        <source>  SHA1 checksum: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1189"/>
        <source>  parent CHD&apos;s MD5 checksum: %1</source>
        <translation>  MD5 checksum do CHD pai: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1191"/>
        <location filename="../../romalyzer.cpp" line="1211"/>
        <source>  parent CHD&apos;s SHA1 checksum: %1</source>
        <translation>  SHA1 checksum do CHD pai: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1214"/>
        <source>  raw SHA1 checksum: %1</source>
        <translation>  Checksum SHA1 bruto: %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1243"/>
        <source>Verify - %p%</source>
        <translation>Verificar - %p%</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1245"/>
        <source>CHD manager: verifying and fixing CHD</source>
        <translation>CHD manager: verificando e corrigindo CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1248"/>
        <source>CHD manager: verifying CHD</source>
        <translation>CHD manager: verificando CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1258"/>
        <source>Update - %p%</source>
        <translation>Atualizar - %p%</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1259"/>
        <source>CHD manager: updating CHD (v%1 -&gt; v%2)</source>
        <translation>CHD manager: atualizando CHD (v%1 -&gt; v%2)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1264"/>
        <location filename="../../romalyzer.cpp" line="1276"/>
        <source>CHD manager: using header checksums for CHD verification</source>
        <translation>CHD manager: usando checksums do cabeçalho para verificação do CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1284"/>
        <source>CHD manager: no header checksums available for CHD verification</source>
        <translation>CHD manager: nenhum checksum do cabeçalho para verificação do CHD disponível</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1312"/>
        <source>CHD manager: terminating external process</source>
        <translation>CHD manager: terminando processo externo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1333"/>
        <location filename="../../romalyzer.cpp" line="1335"/>
        <source>CHD manager: CHD file integrity is good</source>
        <translation>CHD manager: integridade do arquivo CHD é boa</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1337"/>
        <source>CHD manager: WARNING: CHD file integrity is bad</source>
        <translation>CHD manager: AVISO: integridade do arquivo CHD é ruim</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1341"/>
        <location filename="../../romalyzer.cpp" line="1353"/>
        <source>CHD manager: using CHD v%1 header checksums for CHD verification</source>
        <translation>CHD manager: usando checksums de cabeçalhos CHD v%1 para verificação de CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1361"/>
        <source>CHD manager: WARNING: no header checksums available for CHD verification</source>
        <translation>CHD manager: AVISO: nenhum checksum de cabeçalho disponível para verificação CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1368"/>
        <source>CHD manager: replacing CHD</source>
        <translation>CHD manager: trocando CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1370"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1376"/>
        <source>CHD manager: CHD replaced</source>
        <translation>CHD manager: CHD trocado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1379"/>
        <source>CHD manager: FATAL: failed to replace CHD -- updated CHD preserved as &apos;%1&apos;, please copy it to &apos;%2&apos; manually!</source>
        <translation>CHD manager: FATAL: falha para trocar CHD -- CHD atualizado preservado como &apos;%1&apos;, por favor copie-o manualmente para &apos;%2&apos;!</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1383"/>
        <source>CHD manager: cleaning up</source>
        <translation>CHD manager: limpando</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1395"/>
        <location filename="../../romalyzer.cpp" line="1407"/>
        <source>using CHD v%1 header checksums for CHD verification</source>
        <translation>usando checksums de cabeçalhos CHD v%1 para verificação de CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1415"/>
        <source>WARNING: no header checksums available for CHD verification</source>
        <translation>AVISO: nenhum checksum de cabeçalho disponível para verificação CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1423"/>
        <source>WARNING: can&apos;t read CHD header information</source>
        <translation>AVISO: impossível ler a informação do cabeçalho do CHD</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1458"/>
        <source>WARNING: found &apos;%1&apos; but can&apos;t read from it although permissions seem okay - check file integrity</source>
        <translation>AVISO: encontrado &apos;%1&apos; mas impossível ler embora as permissões pareçam corretas -- verifique a integridade do arquivo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1461"/>
        <location filename="../../romalyzer.cpp" line="1592"/>
        <source>WARNING: found &apos;%1&apos; but can&apos;t read from it - check permission</source>
        <translation>AVISO: encontrado &apos;%1&apos; mas impossível ler - verifique as permissões</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1464"/>
        <source>WARNING: CHD file &apos;%1&apos; not found</source>
        <translation>AVISO: arquivo CHD &apos;%1&apos; não encontrado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1510"/>
        <source>size of &apos;%1&apos; from &apos;%2&apos; is greater than allowed maximum -- skipped</source>
        <translation>tamanho de &apos;%1&apos; de &apos;%2&apos; é maior do que o máximo permitido -- pulado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1586"/>
        <source>WARNING: unable to decompress &apos;%1&apos; from &apos;%2&apos; - check file integrity</source>
        <translation>AVISO: incapaz de descomprimir &apos;%1&apos; de &apos;%2&apos; - verifique a integridade do arquivo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1590"/>
        <source>WARNING: found &apos;%1&apos; but can&apos;t open it for decompression - check file integrity</source>
        <translation>AVISO: encontrado &apos;%1&apos; mas não pode abrir para descompressão - verifique a integridade do arquivo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1708"/>
        <location filename="../../romalyzer.cpp" line="1722"/>
        <source> KB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="498"/>
        <location filename="../../romalyzer.cpp" line="1711"/>
        <location filename="../../romalyzer.cpp" line="1725"/>
        <source> MB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1714"/>
        <location filename="../../romalyzer.cpp" line="1728"/>
        <source> GB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1717"/>
        <source> TB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1780"/>
        <source>Choose output directory</source>
        <translation>Escolha diretório de destino</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1794"/>
        <source>Choose additional ROM path</source>
        <translation>Escolha o caminho adicional das ROMs</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="1940"/>
        <source>Checksum search</source>
        <translation>Busca de somas</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2079"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path is not a directory</source>
        <translation>set rewriter: AVISO: impossível reescrever conjunto &apos;%1&apos;, caminho de saída não é um diretório</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2083"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path is not writable</source>
        <translation>set rewriter: AVISO: impossível reescrever conjunto &apos;%1&apos;, caminho de saída não tem permissão de escrita</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2088"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path does not exist</source>
        <translation>set rewriter: AVISO: impossível reescrever conjunto &apos;%1&apos;, caminho de saída não existe</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2092"/>
        <source>set rewriter: WARNING: can&apos;t rewrite set &apos;%1&apos;, output path is empty</source>
        <translation>set rewriter: AVISO: impossível reescrever conjunto &apos;%1&apos;, caminho de saída não está vazio</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2106"/>
        <source>space-efficient</source>
        <translation>eficiência de espaço</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2107"/>
        <source>self-contained</source>
        <translation>auto contido</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2109"/>
        <source>set rewriter: rewriting %1 set &apos;%2&apos; to &apos;%3&apos;</source>
        <translation>set rewriter: reescrevendo conjunto %1 &apos;%2&apos; para &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2125"/>
        <source>set rewriter: skipping &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos; as &apos;%4&apos;</source>
        <translation>set rewriter: pulando &apos;%1&apos; com CRC &apos;%2&apos; de &apos;%3&apos; como &apos;%4&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2136"/>
        <source>set rewriter: FATAL: can&apos;t load &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos;, aborting</source>
        <translation>set rewriter: FATAL: impossível carregar &apos;%1&apos; com CRC &apos;%2&apos; de &apos;%3&apos;, abortando</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2139"/>
        <source>set rewriter: WARNING: can&apos;t load &apos;%1&apos; with CRC &apos;%2&apos; from &apos;%3&apos;, ignoring this file</source>
        <translation>set rewriter: AVISO: impossível carregar &apos;%1&apos; com CRC &apos;%2&apos; de &apos;%3&apos;, ignorando este arquivo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2166"/>
        <source>set rewriter: writing new %1 set &apos;%2&apos; in &apos;%3&apos;</source>
        <translation>set rewriter: escrevendo novo conjunto %1 &apos;%2&apos; em &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2167"/>
        <source>Writing &apos;%1&apos; - %2</source>
        <translation>Escrevendo &apos;%1&apos; - %2</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2169"/>
        <source>set rewriter: new %1 set &apos;%2&apos; in &apos;%3&apos; successfully created</source>
        <translation>set rewriter: novo conjunto %1 &apos;%2&apos; em &apos;%3&apos; criado com sucesso</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2171"/>
        <source>set rewriter: FATAL: failed to create new %1 set &apos;%2&apos; in &apos;%3&apos;</source>
        <translation>set rewriter: FATAL: falha para criar novo conjunto %1 &apos;%2&apos; em &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2181"/>
        <source>set rewriter: done (rewriting %1 set &apos;%2&apos; to &apos;%3&apos;)</source>
        <translation>set rewriter: feito (reescrevendo conjunto %1 &apos;%2&apos; para &apos;%3&apos;)</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="2464"/>
        <source>checksum wizard: repairing %n bad set(s)</source>
        <translation>
            <numerusform>checksum wizard: reparando %n conjunto ruim</numerusform>
            <numerusform>checksum wizard: reparando %n conjuntos ruins</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2470"/>
        <source>checksum wizard: using %1 file &apos;%2&apos; from &apos;%3&apos; as repro template</source>
        <translation>checksum wizard: usando arquivo %1&apos;%2&apos; de &apos;%3&apos; como modelo de reprodução</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2493"/>
        <source>checksum wizard: successfully identified &apos;%1&apos; from &apos;%2&apos; by CRC, filename in ZIP archive is &apos;%3&apos;</source>
        <translation>checksum wizard: &apos;%1&apos; de &apos;%2&apos; identificado com sucesso pelo CRC, nome do arquivo no pacote ZIP é &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2502"/>
        <source>checksum wizard: template data loaded, uncompressed size = %1</source>
        <translation>checksum wizard: modelo de dados carregado, tamanho não comprimido = %1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2506"/>
        <source>checksum wizard: FATAL: unable to identify &apos;%1&apos; from &apos;%2&apos; by CRC</source>
        <translation>checksum wizard: FATAL: incapaz de identificar &apos;%1&apos; de &apos;%2&apos; pelo CRC</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2516"/>
        <location filename="../../romalyzer.cpp" line="2637"/>
        <source>checksum wizard: sorry, no support for regular files yet</source>
        <translation>checksum wizard: desculpe, sem suporte para arquivos regulares por enquanto</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2521"/>
        <location filename="../../romalyzer.cpp" line="2642"/>
        <source>checksum wizard: sorry, no support for CHD files yet</source>
        <translation>checksum wizard: desculpe, sem suporte para arquivos CHD por enquanto</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2547"/>
        <source>checksum wizard: target ZIP exists, loading complete data and structure</source>
        <translation>checksum wizard: arquivo de destino ZIP existe, carregando dados e estrutura completos</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2549"/>
        <source>checksum wizard: target ZIP successfully loaded</source>
        <translation>checksum wizard: arquivo ZIP de destino carregado com sucesso</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2551"/>
        <source>checksum wizard: an entry with the CRC &apos;%1&apos; already exists, recreating the ZIP from scratch to replace the bad file</source>
        <translation>checksum wizard: um entrada com o CRC &apos;%1&apos; já existe, recriando o ZIP do início para substituir o arquivo ruim</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2554"/>
        <source>checksum wizard: backup file &apos;%1&apos; successfully created</source>
        <translation>checksum wizard: arquivo de backup &apos;%1&apos; criado com sucesso</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2558"/>
        <source>checksum wizard: FATAL: failed to create backup file &apos;%1&apos;, aborting</source>
        <translation>checksum wizard: FATAL: falha ao criar o arquivo de backup &apos;%1&apos;, abortando</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2562"/>
        <source>checksum wizard: no entry with the CRC &apos;%1&apos; was found, adding the missing file to the existing ZIP</source>
        <translation>checksum wizard: nenhuma entrada com o CRC &apos;%1&apos; foi encontrada, adicionando o arquivo faltando para o ZIP existente</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2565"/>
        <source>checksum wizard: FATAL: failed to load target ZIP, aborting</source>
        <translation>checksum wizard: FATAL: falha ao carregar o arquivo ZIP de destino, abortando</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2570"/>
        <source>checksum wizard: the target ZIP does not exist, creating a new ZIP with just the missing file</source>
        <translation>checksum wizard: o arquivo ZIP de destino não existe, criando um novo arquivo ZIP somente com o arquivo faltante</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2431"/>
        <location filename="../../romalyzer.cpp" line="2628"/>
        <source>Created by QMC2 v%1 (%2)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2651"/>
        <source>repair failed</source>
        <translation>reparo falhou</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2653"/>
        <source>checksum wizard: FATAL: failed to repair %1 file &apos;%2&apos; in &apos;%3&apos; from repro template</source>
        <translation>checksum wizard: FATAL: falha ao reparar arquivo %1 &apos;%2&apos; em &apos;%3&apos; do modelo de reprodução</translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2663"/>
        <source>checksum wizard: FATAL: can&apos;t find any good set</source>
        <translation>checksum wizard: FATAL: nenhum conjunto bom encontrado</translation>
    </message>
    <message numerus="yes">
        <location filename="../../romalyzer.cpp" line="2665"/>
        <source>checksum wizard: done (repairing %n bad set(s))</source>
        <translation>
            <numerusform>checksum wizard: feito (reparando %n conjunto ruim)</numerusform>
            <numerusform>checksum wizard: feito (reparando %n conjuntos ruins)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../romalyzer.cpp" line="2739"/>
        <source>database connection check failed -- errorNumber = %1, errorText = &apos;%2&apos;</source>
        <translation>falha na verificação da conexão com a base de dados -- número do erro = %1. texto do erro = &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="15"/>
        <source>ROMAlyzer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="29"/>
        <source>Shortname of game to be analyzed - wildcards allowed, use space as delimiter for multiple games</source>
        <translation>Nome curto do jogo a ser analisado - wildcards permitidos, use espaço como delimitador para múltiplos jogos</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="39"/>
        <source>Start / stop analysis</source>
        <translation>Iniciar / parar análise</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="53"/>
        <source>Pause / resume active analysis</source>
        <translation>Pausar / continuar análise ativa</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="73"/>
        <source>Report</source>
        <translation>Relatório</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="82"/>
        <source>Analysis report</source>
        <translation>Relatório de análise</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="92"/>
        <source>Game / File</source>
        <translation>Jogo / Arquivo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="97"/>
        <source>Merge</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="102"/>
        <location filename="../../romalyzer.ui" line="1279"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="107"/>
        <source>Emu status</source>
        <translation>Estado do emulador</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="112"/>
        <source>File status</source>
        <translation>Estado do arquivo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="117"/>
        <source>Size</source>
        <translation>Tamanho</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="122"/>
        <location filename="../../romalyzer.ui" line="1351"/>
        <source>CRC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="127"/>
        <location filename="../../romalyzer.ui" line="1346"/>
        <source>SHA1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="132"/>
        <source>MD5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="141"/>
        <source>Log</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="147"/>
        <source>Analysis log</source>
        <translation>Log da análise</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="169"/>
        <source>Search string</source>
        <translation>Frase de busca</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="176"/>
        <source>Search string forward</source>
        <translation>Procurar para frente</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="179"/>
        <source>&amp;Forward</source>
        <translation>&amp;Frente</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="193"/>
        <source>Search string backward</source>
        <translation>Procurar para trás</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="196"/>
        <source>&amp;Backward</source>
        <translation>&amp;Trás</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="211"/>
        <source>Settings</source>
        <translation>Configurações</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="230"/>
        <source>Enable CHD manager (may be slow)</source>
        <translation>Habilitar CHD manager (pode ser lento)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="239"/>
        <source>CHD manager (chdman)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="252"/>
        <source>CHD manager executable file (read and execute)</source>
        <translation>Arquivo executável do gerenciador de CHD (ler e executar)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="265"/>
        <source>Browse CHD manager executable file</source>
        <translation>Procurar pelo arquivo executável do gerenciador de CHDs</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="276"/>
        <source>Temporary working directory</source>
        <translation>Diretório temporário de trabalho</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="315"/>
        <source>Verify CHDs through &apos;chdman -verify&apos;</source>
        <translation>Verfica CHDs atravéz do comando &apos;chdman -verify&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="318"/>
        <source>Verify CHDs</source>
        <translation>Verificar CHDs</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="328"/>
        <source>Also try to fix CHDs using &apos;chdman -verifyfix&apos;</source>
        <translation>Também tentar corrigir os CHDs usando o comando &apos;chdman -verifyfix&apos;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="331"/>
        <source>Fix CHDs</source>
        <translation>Corrigir CHDs</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="338"/>
        <source>Try to update CHDs if their header indicates an older version (&apos;chdman -update&apos;)</source>
        <translation>Tenta atualizar os CHDs se seus cabeçaçhos indicarem uma versão antiga (&apos;chdman -update&apos;)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="341"/>
        <source>Update CHDs</source>
        <translation>Atualizar CHDs</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="372"/>
        <source>If set, analysis output is appended (otherwise the report is cleared before the analysis)</source>
        <translation>Se selecionado, a saída da análise será adicionada (senão o relatório será limpo antes da análise)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="375"/>
        <source>Append to report</source>
        <translation>Adicionar ao relatório</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="382"/>
        <source>Automatically scroll to the currently analyzed game</source>
        <translation>Ir automaticamente para o jogo analisado atualmente</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="385"/>
        <source>Auto scroll</source>
        <translation>Ir automaticamente</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="421"/>
        <source>Automatically expand file info</source>
        <translation>Automaticamente expandir as informações do arquivo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="424"/>
        <source>Expand file info</source>
        <translation>Expandir informações do arquivo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="434"/>
        <source>Automatically expand checksums</source>
        <translation>Automaticamente expandir checksums</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="437"/>
        <source>Expand checksums</source>
        <translation>Expandir checksums</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="640"/>
        <source>Database server port (0 = default)</source>
        <translation>Porta do servidor de banco de dados (0 = padrão)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="756"/>
        <source>Password used to access the database (WARNING: stored passwords are &lt;u&gt;weakly&lt;/u&gt; encrypted!)</source>
        <translation>Senha usada para acessar a base de dados (AVISO: senhas são &lt;u&gt;fracamente&lt;/u&gt; encriptadas!)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="811"/>
        <source>Mode</source>
        <translation>Modo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="843"/>
        <source>Upload</source>
        <translation>Enviar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="972"/>
        <source>Rewrite sets while analyzing them (otherwise sets will only be rewritten on demand / through the context menu)</source>
        <translation>Reescrever conjuntos enquanto são analisados (caso contrário conjuntos serão reescritos apenas sob demanda ou pelo menu de contexto)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="991"/>
        <source>Self-contained</source>
        <translation>Auto contido</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1032"/>
        <source>ZIPs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1045"/>
        <source>Level </source>
        <translation>Nível</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1066"/>
        <source>When a set contains multiple files with the same CRC, should the produced ZIP include all files individually or just the first one (which is actually sufficient)?</source>
        <translation>Quando um conjunto contém vários arquivos com o mesmo CRC, o ZIP produzido deve incluir todos os arquivos individualmente ou apenas o primeiro (o que é atualmente suficiente atualmente)?</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1079"/>
        <source>Produce sets in individual sub-directories (not recommended -- and not supported yet!)</source>
        <translation>Produzir conjuntos em sub diretórios individuais (não recomendado - e não suportado por enquanto!)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1082"/>
        <source>Directories</source>
        <translation>Diretórios</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1125"/>
        <source>Additional ROM path</source>
        <translation>Caminho adicional de ROM</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1151"/>
        <source>Browse additional ROM path</source>
        <translation>Procurar por caminho de ROM adicional</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1178"/>
        <source>Checksum to be searched</source>
        <translation>Checksum a ser procurado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1191"/>
        <source>Search for the checksum now</source>
        <translation>Procurar pelo checksum agora</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1225"/>
        <source>Repair selected &apos;bad&apos; sets using the file from the first selected &apos;good&apos; set (at least 1 good and 1 bad set must be selected)</source>
        <translation>Reparar conjuntos &apos;ruins&apos; selecionados usando arquivos do primeiro conjunto &apos;bom&apos; selecionado (pelo menos 1 conjunto bom e 1 ruim deve ser selecionados)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="743"/>
        <location filename="../../romalyzer.cpp" line="2755"/>
        <source>Connection check</source>
        <translation>Verificação de conexão</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="785"/>
        <source>Database</source>
        <translation>Base de dados</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="772"/>
        <source>Name of the database on the server</source>
        <translation>Nome da base de dados no servidor</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="395"/>
        <source>Calculate CRC-32 checksum</source>
        <translation>Calcular checksum CRC-32</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="398"/>
        <source>Calculate CRC</source>
        <translation>Calcular CRC</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="408"/>
        <source>Calculate MD5 hash</source>
        <translation>Calcular has MD5</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="411"/>
        <source>Calculate MD5</source>
        <translation>Calcular MD5</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="444"/>
        <source>Calculate SHA1 hash</source>
        <translation>Calcular hash SHA1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="447"/>
        <source>Calculate SHA1</source>
        <translation>Calcular SHA1</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="495"/>
        <location filename="../../romalyzer.ui" line="530"/>
        <location filename="../../romalyzer.ui" line="565"/>
        <source>unlimited</source>
        <translation>ilimitado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="527"/>
        <source>Maximum number of lines in log (0 = no limit)</source>
        <translation>Tamanho máximo de linhas no log (0 = ilimitado)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="533"/>
        <source> lines</source>
        <translation>linhas</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="457"/>
        <source>Select game in game list if selected in analysis report?</source>
        <translation>Selecionar jogo na lista de jogo se selecionado na análise?</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="460"/>
        <source>Select game</source>
        <translation>Selecionar jogo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="599"/>
        <source>Enable ROM database support (repository access may be slow)</source>
        <translation>Habilitar suporte à base de dados de ROM (acesso ao repositório pode ser lento)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="698"/>
        <source>Server / IP</source>
        <translation>Servidor / IP</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="685"/>
        <source>Name or IP address of the database server</source>
        <translation>Nome ou endereço IP do servidor de banco de dados</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="614"/>
        <source>Driver</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="627"/>
        <source>Port</source>
        <translation>Porta</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="643"/>
        <source>default</source>
        <translation>padrão</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="711"/>
        <source>Username</source>
        <translation>Nome de usuário</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="724"/>
        <source>Username used to access the database</source>
        <translation>Nome de usuário para acessar a base de dados</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="798"/>
        <source>Password</source>
        <translation>Senha</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="824"/>
        <source>Automatically download missing / bad files from the database (if they are available in the repository)</source>
        <translation>Baixar automaticamente arquivos faltantes / ruins da base de dados (se elas estiverem disponíveis no repositório)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="827"/>
        <source>Download</source>
        <translation>Baixar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="840"/>
        <source>Automatically upload good files to the database (if they are missing in the repository)</source>
        <translation>Enviar automaticamente arquivos bons para a base de dados (se elas não existirem no repositório)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="662"/>
        <source>SQL driver to use</source>
        <translation>Driver SQL para usar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="289"/>
        <source>Temporary directory used by the CHD manager (make sure it has enough room to store the biggest CHDs)</source>
        <translation>Diretório temporário usado pelo CHD manager (garanta que tem espaço suficiente para guardar os CHDs maiores)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="302"/>
        <source>Browse temporary directory used by the CHD manager</source>
        <translation>Escolher diretório temporário que será usado pelo CHD manager</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="366"/>
        <source>General analysis flags and limits</source>
        <translation>Flags e limites de análise geral</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="472"/>
        <source>&lt;b&gt;Limits:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Limites:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="479"/>
        <source>File size</source>
        <translation>Tamanho do arquivo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="492"/>
        <source>Maximum size (in MB) of files to be loaded, files are skipped when they are bigger than that (0 = no limit)</source>
        <translation>Tamanha máximo (em MB) dos arquivos a serem carregados, arquivos são pulados quando são maiores que o limite (0 = ilimitado)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="514"/>
        <source>Log size</source>
        <translation>Tamanho do arquivo de log</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="549"/>
        <source>Reports</source>
        <translation>Relatórios</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="562"/>
        <source>Maximum number of reported sets held in memory (0 = no limit)</source>
        <translation>Número máximo de conjuntos relatados em memória (0 = sem limite)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="666"/>
        <source>MySQL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="671"/>
        <source>SQLite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="737"/>
        <source>Check the connection to the database</source>
        <translation>Verificar a conexão com a base de dados</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="859"/>
        <source>Overwrite existing data in the database</source>
        <translation>Sobrescrever dados existentes na base de dados</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="862"/>
        <source>Overwrite</source>
        <translation>Sobrescrever</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="875"/>
        <source>Output path</source>
        <translation>Caminho de saída</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="888"/>
        <source>Local output directory where downloaded ROMs &amp; CHDs will be created (WARNING: existing files will be overwritten!)</source>
        <translation>Diretório de saída onde ROMs &amp; CHDs baixados serão criados (AVISO: arquivos existentes serão sobrescritos!)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="901"/>
        <source>Browse local output directory</source>
        <translation>Escolher diretório de saída local</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="915"/>
        <source>Enable set rewriter</source>
        <translation>Habilitar reescritor de conjuntos</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="933"/>
        <source>Output directory</source>
        <translation>Diretório de destino</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="946"/>
        <source>Output path for the set rewriter (WARNING: existing files will be overwritten!) -- you should NEVER use one of your primary ROM paths here!!!</source>
        <translation>Caminho de saída para o reescritor de conjuntos (AVISO: arquivos existentes serão sobrescritos!) - você NUNCA deveria usar um de seus caminhos de ROMs aqui!!!</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="959"/>
        <source>General settings</source>
        <translation>Configurações gerais</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1014"/>
        <source>Reproduction type</source>
        <translation>Tipo de reprodução</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1108"/>
        <source>Browse output path for the set rewriter</source>
        <translation>Escolher caminho de saída para o reescritor de conjuntos</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="975"/>
        <source>Rewrite while analyzing</source>
        <translation>Reescrever durante análise</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="988"/>
        <source>Create sets that do not need parent sets (otherwise create merged sets, which is recommended)</source>
        <translation>Criar conjuntos que não precisam de conjuntos pais (caso contrário criar conjuntos mistos, o que é recomendado)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="998"/>
        <source>Rewrite sets only when they are &apos;good&apos; (otherwise, &apos;bad&apos; sets will be included)</source>
        <translation>Reescrever conjuntos somento quando eles estão &apos;bons&apos; (caso contrário, conjuntos &apos;ruins&apos; serão incluídos)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1001"/>
        <source>Good sets only</source>
        <translation>Somente conjuntos bons</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1029"/>
        <source>Produce ZIP archived sets (recommended)</source>
        <translation>Produzir conjuntos em arquivos ZIP (recomendado)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1042"/>
        <source>Select the ZIP compression level (0 = lowest / fastest, 9 = highest / slowest)</source>
        <translation>Escolha o nível de compressão para os arquivos ZIP (0 = menor / mais rápido, 9 = melhor / mais lento)</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1069"/>
        <source>Unique CRCs</source>
        <translation>CRCs únicos</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1166"/>
        <source>Checksum wizard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1194"/>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1264"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1269"/>
        <source>Filename</source>
        <translation>Nome do arquivo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1274"/>
        <source>Status</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1284"/>
        <source>Path</source>
        <translation>Caminho</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1228"/>
        <source>Repair bad sets</source>
        <translation>Reparar conjuntos ruins</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1138"/>
        <source>Specify an additional source ROM path used when the set rewriter is active</source>
        <translation>Especifique um caminho adicional de ROM usado quando o set rewriter (reescritor de conjuntos) está ativo</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1208"/>
        <source>Analyze all selected sets in order to qualify them</source>
        <translation>Analisar todos os conjuntos selecionado para qualificá-los</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1211"/>
        <source>Analyze selected sets</source>
        <translation>Analisar conjuntos selecionados</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1239"/>
        <source>Search results for the current checksum</source>
        <translation>Procurar resultados para o checksum atual</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1294"/>
        <source>Level of automation</source>
        <translation>Nível de automação</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1307"/>
        <source>Choose the level of automated wizard operations</source>
        <translation>Escolha o nível das operações do wizard automatizado</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1311"/>
        <source>Do nothing automatically</source>
        <translation>Não fazer nada automaticamente</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1316"/>
        <source>Automatically select matches</source>
        <translation>Selecionar correspondências automaticamente</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1321"/>
        <source>Automatically select matches and analyze sets</source>
        <translation>Selecionar correspondências e analisar conjuntos automaticamente</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1326"/>
        <source>Automatically select matches, analyze sets and repair bad ones</source>
        <translation>Selecionar correspondências, analisar conjuntos e reparar os ruins automaticamente</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1342"/>
        <source>Select the checksum type</source>
        <translation>Selecione o tipo de checksum</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1368"/>
        <source>Close ROMAlyzer</source>
        <translation>Fechar ROMAlyzer</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1371"/>
        <source>&amp;Close</source>
        <translation>&amp;Fechar</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1388"/>
        <source>Current ROMAlyzer status</source>
        <translation>Estado atual do ROMAlyzer</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1407"/>
        <source>Analysis progress indicator</source>
        <translation>Indicador de progresso da análise</translation>
    </message>
    <message>
        <location filename="../../romalyzer.ui" line="1426"/>
        <source>File I/O progress indicator</source>
        <translation>indicador de progresso de E/S de arquivo</translation>
    </message>
</context>
<context>
    <name>ROMStatusExporter</name>
    <message>
        <location filename="../../romstatusexport.cpp" line="34"/>
        <source>Machine description</source>
        <translation>Descrição da máquina</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="35"/>
        <source>Machine name</source>
        <translation>Nome da máquina</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="134"/>
        <location filename="../../romstatusexport.cpp" line="440"/>
        <location filename="../../romstatusexport.cpp" line="658"/>
        <source>Confirm</source>
        <translation>Confirmar</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="135"/>
        <location filename="../../romstatusexport.cpp" line="441"/>
        <location filename="../../romstatusexport.cpp" line="659"/>
        <source>Overwrite existing file?</source>
        <translation>Sobrescrever arquivo existente?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="150"/>
        <source>exporting ROM status in ASCII format to &apos;%1&apos;</source>
        <translation>exportando estado da ROM no formato ASCII para &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="152"/>
        <source>WARNING: can&apos;t open ASCII export file &apos;%1&apos; for writing, please check permissions</source>
        <translation>AVISO: impossível abrir o arquivo de exportação para ASCII &apos;%1&apos; para escrita, por favor verifique as permissões</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="158"/>
        <source>exporting ROM status in ASCII format to clipboard</source>
        <translation>exportando estado da ROM em formato ASCII para a área de transferência</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="169"/>
        <location filename="../../romstatusexport.cpp" line="198"/>
        <location filename="../../romstatusexport.cpp" line="490"/>
        <location filename="../../romstatusexport.cpp" line="717"/>
        <source>Emulator</source>
        <translation>Emulador</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="170"/>
        <location filename="../../romstatusexport.cpp" line="199"/>
        <location filename="../../romstatusexport.cpp" line="491"/>
        <location filename="../../romstatusexport.cpp" line="719"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="171"/>
        <location filename="../../romstatusexport.cpp" line="200"/>
        <location filename="../../romstatusexport.cpp" line="492"/>
        <location filename="../../romstatusexport.cpp" line="721"/>
        <source>Time</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <source>Total games</source>
        <translation type="obsolete">Total de jogos</translation>
    </message>
    <message>
        <source>Total machines</source>
        <translation type="obsolete">Total de máquinas</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="173"/>
        <location filename="../../romstatusexport.cpp" line="208"/>
        <location filename="../../romstatusexport.cpp" line="499"/>
        <location filename="../../romstatusexport.cpp" line="735"/>
        <source>Correct</source>
        <translation>Correto</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="172"/>
        <location filename="../../romstatusexport.cpp" line="207"/>
        <location filename="../../romstatusexport.cpp" line="498"/>
        <location filename="../../romstatusexport.cpp" line="733"/>
        <source>Total sets</source>
        <translation>Conjuntos totais</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="174"/>
        <location filename="../../romstatusexport.cpp" line="209"/>
        <location filename="../../romstatusexport.cpp" line="500"/>
        <location filename="../../romstatusexport.cpp" line="737"/>
        <source>Mostly correct</source>
        <translation>Maioria correto</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="175"/>
        <location filename="../../romstatusexport.cpp" line="210"/>
        <location filename="../../romstatusexport.cpp" line="501"/>
        <location filename="../../romstatusexport.cpp" line="739"/>
        <source>Incorrect</source>
        <translation>Incorreto</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="176"/>
        <location filename="../../romstatusexport.cpp" line="211"/>
        <location filename="../../romstatusexport.cpp" line="502"/>
        <location filename="../../romstatusexport.cpp" line="741"/>
        <source>Not found</source>
        <translation>Não encontrado</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="177"/>
        <location filename="../../romstatusexport.cpp" line="212"/>
        <location filename="../../romstatusexport.cpp" line="503"/>
        <location filename="../../romstatusexport.cpp" line="743"/>
        <source>Unknown</source>
        <translation>Desconhecido</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="185"/>
        <location filename="../../romstatusexport.cpp" line="186"/>
        <location filename="../../romstatusexport.cpp" line="478"/>
        <location filename="../../romstatusexport.cpp" line="697"/>
        <location filename="../../romstatusexport.cpp" line="713"/>
        <source>ROM Status Export - created by QMC2 %1</source>
        <translation>Exportação de Estado da ROM - criado por QMC2 %1</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="188"/>
        <location filename="../../romstatusexport.cpp" line="480"/>
        <location filename="../../romstatusexport.cpp" line="703"/>
        <source>SDLMAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="190"/>
        <location filename="../../romstatusexport.cpp" line="482"/>
        <location filename="../../romstatusexport.cpp" line="705"/>
        <source>SDLMESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="192"/>
        <location filename="../../romstatusexport.cpp" line="484"/>
        <location filename="../../romstatusexport.cpp" line="707"/>
        <source>MAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="194"/>
        <location filename="../../romstatusexport.cpp" line="486"/>
        <location filename="../../romstatusexport.cpp" line="709"/>
        <source>MESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="196"/>
        <location filename="../../romstatusexport.cpp" line="271"/>
        <location filename="../../romstatusexport.cpp" line="385"/>
        <location filename="../../romstatusexport.cpp" line="488"/>
        <location filename="../../romstatusexport.cpp" line="622"/>
        <location filename="../../romstatusexport.cpp" line="711"/>
        <location filename="../../romstatusexport.cpp" line="873"/>
        <source>unknown</source>
        <translation>desconhecido</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="205"/>
        <location filename="../../romstatusexport.cpp" line="206"/>
        <location filename="../../romstatusexport.cpp" line="497"/>
        <location filename="../../romstatusexport.cpp" line="729"/>
        <source>Overall ROM Status</source>
        <translation>Estado geral da ROM</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="215"/>
        <location filename="../../romstatusexport.cpp" line="216"/>
        <location filename="../../romstatusexport.cpp" line="506"/>
        <location filename="../../romstatusexport.cpp" line="749"/>
        <source>Detailed ROM Status</source>
        <translation>Estado detalhado da ROM</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="219"/>
        <source>sorting, filtering and analyzing export data</source>
        <translation>ordenando, filtrando e analisando exportação de dados</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="223"/>
        <location filename="../../romstatusexport.cpp" line="226"/>
        <location filename="../../romstatusexport.cpp" line="336"/>
        <location filename="../../romstatusexport.cpp" line="557"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>ROM types</source>
        <translation>Tipos de ROM</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="242"/>
        <location filename="../../romstatusexport.cpp" line="368"/>
        <location filename="../../romstatusexport.cpp" line="577"/>
        <location filename="../../romstatusexport.cpp" line="820"/>
        <source>correct</source>
        <translation>correto</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="249"/>
        <location filename="../../romstatusexport.cpp" line="372"/>
        <location filename="../../romstatusexport.cpp" line="588"/>
        <location filename="../../romstatusexport.cpp" line="833"/>
        <source>mostly correct</source>
        <translation>maioria correto</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="256"/>
        <location filename="../../romstatusexport.cpp" line="376"/>
        <location filename="../../romstatusexport.cpp" line="599"/>
        <location filename="../../romstatusexport.cpp" line="846"/>
        <source>incorrect</source>
        <translation>incorreto</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="263"/>
        <location filename="../../romstatusexport.cpp" line="380"/>
        <location filename="../../romstatusexport.cpp" line="610"/>
        <location filename="../../romstatusexport.cpp" line="859"/>
        <source>not found</source>
        <translation>não encontrado</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="328"/>
        <source>done (sorting, filtering and analyzing export data)</source>
        <translation>feito (ordenando, filtrando e analisando exportação de dados)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="329"/>
        <location filename="../../romstatusexport.cpp" line="550"/>
        <location filename="../../romstatusexport.cpp" line="794"/>
        <source>writing export data</source>
        <translation>escrevendo dados de exportação</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="222"/>
        <location filename="../../romstatusexport.cpp" line="331"/>
        <location filename="../../romstatusexport.cpp" line="552"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="223"/>
        <location filename="../../romstatusexport.cpp" line="332"/>
        <location filename="../../romstatusexport.cpp" line="553"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Status</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="221"/>
        <location filename="../../romstatusexport.cpp" line="333"/>
        <location filename="../../romstatusexport.cpp" line="554"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="208"/>
        <location filename="../../romstatusexport.cpp" line="225"/>
        <location filename="../../romstatusexport.cpp" line="334"/>
        <location filename="../../romstatusexport.cpp" line="555"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Year</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="213"/>
        <location filename="../../romstatusexport.cpp" line="224"/>
        <location filename="../../romstatusexport.cpp" line="335"/>
        <location filename="../../romstatusexport.cpp" line="556"/>
        <location filename="../../romstatusexport.cpp" line="798"/>
        <source>Manufacturer</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="415"/>
        <location filename="../../romstatusexport.cpp" line="633"/>
        <location filename="../../romstatusexport.cpp" line="887"/>
        <source>done (writing export data)</source>
        <translation>feito (escrevendo dados de exportação)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="419"/>
        <source>done (exporting ROM status in ASCII format to clipboard)</source>
        <translation>feito (exportando estado da ROM em formato ASCII para a área de transferência)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="422"/>
        <source>done (exporting ROM status in ASCII format to &apos;%1&apos;)</source>
        <translation>feito (exportando estado da ROM no formato ASCII para &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="456"/>
        <source>exporting ROM status in CSV format to &apos;%1&apos;</source>
        <translation>exportando estado da ROM no formato CSV para &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="458"/>
        <source>WARNING: can&apos;t open CSV export file &apos;%1&apos; for writing, please check permissions</source>
        <translation>AVISO: impossível abrir o arquivo de exportação para CSV &apos;%1&apos; para escrita, por favor verifique as permissões</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="464"/>
        <source>exporting ROM status in CSV format to clipboard</source>
        <translation>exportando estado da ROM no formato CSV para a área de transferência</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="509"/>
        <location filename="../../romstatusexport.cpp" line="753"/>
        <source>sorting and filtering export data</source>
        <translation>ordenando e filtrando dados de exportação</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="549"/>
        <location filename="../../romstatusexport.cpp" line="793"/>
        <source>done (sorting and filtering export data)</source>
        <translation>feito (ordenando e filtrando dados de exportação)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="637"/>
        <source>done (exporting ROM status in CSV format to clipboard)</source>
        <translation>feito (exportando estado da ROM no formato CSV para a área de transferência)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="640"/>
        <source>done (exporting ROM status in CSV format to &apos;%1&apos;)</source>
        <translation>feito (exportando estado da ROM no formato CSV para &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="674"/>
        <source>exporting ROM status in HTML format to &apos;%1&apos;</source>
        <translation>exportando estado da ROM no formato HTML para &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="676"/>
        <source>WARNING: can&apos;t open HTML export file &apos;%1&apos; for writing, please check permissions</source>
        <translation>AVISO: impossível abrir o arquivo de exportação para HTML &apos;%1&apos; para escrita, por favor verifique as permissões</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="682"/>
        <source>exporting ROM status in HTML format to clipboard</source>
        <translation>exportando estado da ROM no formato HTML para a área de transferência</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="891"/>
        <source>done (exporting ROM status in HTML format to clipboard)</source>
        <translation>feito (exportando estado da ROM no formato HTML para a área de transferência)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="894"/>
        <source>done (exporting ROM status in HTML format to &apos;%1&apos;)</source>
        <translation>feito (exportando estado da ROM no formato HTML para &apos;%1&apos;)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="907"/>
        <source>Choose ASCII export file</source>
        <translation>Escolha arquivo de exportação em ASCII</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="907"/>
        <location filename="../../romstatusexport.cpp" line="919"/>
        <location filename="../../romstatusexport.cpp" line="931"/>
        <source>All files (*)</source>
        <translation>Todos os arquivos (*)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="919"/>
        <source>Choose CSV export file</source>
        <translation>Escolha arquivo de exportação em CSV</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="931"/>
        <source>Choose HTML export file</source>
        <translation>Escolha arquivo de exportação em HTML</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.cpp" line="944"/>
        <source>gamelist is not ready, please wait</source>
        <translation>lista de jogos não está pronto, por favor espere</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="15"/>
        <source>ROM status export</source>
        <translation>Exportação de estado da ROM</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="23"/>
        <location filename="../../romstatusexport.ui" line="36"/>
        <source>Select output format</source>
        <translation>Selecione o formato de saída</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="40"/>
        <source>ASCII</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="45"/>
        <source>CSV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="50"/>
        <source>HTML</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="72"/>
        <source>Exported ROM states</source>
        <translation>Estados de ROM exportados</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="81"/>
        <source>Export ROM state C (correct)?</source>
        <translation>Exportar estado de ROM C (correto)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="98"/>
        <source>Export ROM state M (mostly correct)?</source>
        <translation>Exportar estado de ROM M (maioria correto)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="115"/>
        <source>Export ROM state I (incorrect)?</source>
        <translation>Exportar estado de ROM I (incorreto)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="132"/>
        <source>Export ROM state N (not found)?</source>
        <translation>Exportar estado de ROM N (não encontrado)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="149"/>
        <source>Export ROM state U (unknown)?</source>
        <translation>Exportar estado de ROM U (desconhecido)?</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="181"/>
        <source>Sort criteria</source>
        <translation>Critério de ordenação</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="194"/>
        <source>Select sort criteria</source>
        <translation>Selecione o critério de ordenação</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="198"/>
        <source>Game description</source>
        <translation>Descrição do jogo</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="203"/>
        <source>ROM state</source>
        <translation>Estado da ROM</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="218"/>
        <source>Game name</source>
        <translation>Nome do jogo</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="231"/>
        <source>Sort order</source>
        <translation>Ordem</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="244"/>
        <source>Select sort order</source>
        <translation>Selecione a ordem</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="248"/>
        <source>Ascending</source>
        <translation>Ascendente</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="257"/>
        <source>Descending</source>
        <translation>Descendente</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="269"/>
        <source>Include some header information in export</source>
        <translation>Incluir cabeçalho na exportação</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="272"/>
        <source>Include header</source>
        <translation>Incluir cabeçalho</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="282"/>
        <source>Include statistical overview of the ROM state in export</source>
        <translation>Incluir estatística geral de estados da ROM na exportação</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="285"/>
        <source>Include ROM statistics</source>
        <translation>Incluir estatísticas das ROMs</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="295"/>
        <source>Export to the system clipboard instead of a file</source>
        <translation>Exportar para a área de transferência ao invés de um arquivo</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="298"/>
        <source>Export to clipboard</source>
        <translation>Exportar para a área de transferência</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="308"/>
        <source>Overwrite existing files without asking what to do</source>
        <translation>Sobrescrever arquivos existentes sem perguntar</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="311"/>
        <source>Overwrite blindly</source>
        <translation>Não perguntar ao sobrescrever</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="339"/>
        <source>Browse ASCII export file</source>
        <translation>Procurar pelo arquivo de exportação ASCII</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="342"/>
        <source>ASCII file</source>
        <translation>Arquivo ASCII</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="362"/>
        <source>ASCII export file</source>
        <translation>Arquivo de exportação ASCII</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="369"/>
        <source>Column width</source>
        <translation>Tamanho da coluna</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="385"/>
        <source>Maximum column width for ASCII export (0 = unlimited)</source>
        <translation>Tamanho máximo da coluna para exportação ASCII (0 = ilimitado)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="388"/>
        <source>unlimited</source>
        <translation>ilimitado</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="444"/>
        <source>Browse CSV export file</source>
        <translation>Procurar pelo arquivo de exportação CSV</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="447"/>
        <source>CSV file</source>
        <translation>Arquivo CSV</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="467"/>
        <source>CSV export file</source>
        <translation>Arquivo de exportação CSV</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="474"/>
        <source>Separator</source>
        <translation>Separador</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="515"/>
        <source>Field separator for CSV export</source>
        <translation>Separador de campo para exportação em CSV</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="518"/>
        <source>;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="531"/>
        <source>Delimiter</source>
        <translation>Delimitador</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="559"/>
        <source>Text delimiter for CSV export</source>
        <translation>Delimitador de texto para exportação em CSV</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="562"/>
        <source>&quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="618"/>
        <source>Browse HTML export file</source>
        <translation>Procurar pelo arquivo de exportação HTML</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="621"/>
        <source>HTML file</source>
        <translation>Arquivo HTML</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="641"/>
        <source>HTML export file</source>
        <translation>Arquivo de exportação HTML</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="648"/>
        <source>Border width</source>
        <translation>Tamanho da borda</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="664"/>
        <source>Border line width for tables (0 = no border)</source>
        <translation>Tamanho da borda para tabelas (0 = sem borda)</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="714"/>
        <source>Export now!</source>
        <translation>Exportar agora!</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="717"/>
        <source>&amp;Export</source>
        <translation>&amp;Exportar</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="728"/>
        <source>Close ROM status export</source>
        <translation>Fechar exportação de estado de ROM</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="731"/>
        <source>&amp;Close</source>
        <translation>&amp;Fechar</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="742"/>
        <source>Export progress indicator</source>
        <translation>Indicador de progresso da exportação</translation>
    </message>
    <message>
        <location filename="../../romstatusexport.ui" line="748"/>
        <source>%p%</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SampleChecker</name>
    <message>
        <location filename="../../sampcheck.cpp" line="132"/>
        <source>verifying samples</source>
        <translation>verificando exemplos</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="29"/>
        <location filename="../../sampcheck.cpp" line="134"/>
        <source>Good: 0</source>
        <translation>Bom: 0</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="42"/>
        <location filename="../../sampcheck.cpp" line="136"/>
        <source>Bad: 0</source>
        <translation>Ruim: 0</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="55"/>
        <location filename="../../sampcheck.cpp" line="138"/>
        <source>Obsolete: 0</source>
        <translation>Obsoleto: 0</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="140"/>
        <source>check pass 1: sample status</source>
        <translation>checagem primeira passagem: estado dos exemplos</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="178"/>
        <source>WARNING: emulator audit call didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation>AVISO:chamada para a auditoria do emulador não terminou corretamente -- código de saída = %1, estado de saída = %2</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="178"/>
        <source>normal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="178"/>
        <source>crashed</source>
        <translation>quebrado</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="194"/>
        <source>check pass 2: obsolete sample sets</source>
        <translation>checagem segunda passagem: conjuntos de exemplos obsoletos</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="216"/>
        <source>Obsolete: %1</source>
        <translation>Obsoleto: %1</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="226"/>
        <source>done (verifying samples, elapsed time = %1)</source>
        <translation>feito (verificando exemplos, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="227"/>
        <source>%1 good, %2 bad (or missing), %3 obsolete</source>
        <translation>%1 bom, %2 ruim (ou faltando), %3 obsoleto</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="74"/>
        <location filename="../../sampcheck.cpp" line="232"/>
        <source>&amp;Check samples</source>
        <translation>&amp;Verificar exemplos</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="267"/>
        <location filename="../../sampcheck.cpp" line="318"/>
        <source>Good: %1</source>
        <translation>Bom: %1</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="273"/>
        <location filename="../../sampcheck.cpp" line="324"/>
        <source>Bad: %1</source>
        <translation>Ruim: %1</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="361"/>
        <source>please wait for reload to finish and try again</source>
        <translation>por favor espere o fim do recarregamento e tente novamente</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="366"/>
        <source>please wait for ROM state filter to finish and try again</source>
        <translation>por favor espere pelo filtro de estado de ROM terminar e tente novamente</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="371"/>
        <source>please wait for ROM verification to finish and try again</source>
        <translation>por favor espere a verificação de ROM terminar e tente novamente</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="376"/>
        <source>please wait for image check to finish and try again</source>
        <translation>por favor espere a verificação de imagem terminar e tente novamente</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="381"/>
        <source>stopping sample check upon user request</source>
        <translation>parando verificação de exemplos devido à requisição do usuário</translation>
    </message>
    <message>
        <location filename="../../sampcheck.cpp" line="386"/>
        <source>&amp;Stop check</source>
        <translation>&amp;Parar verificação</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="15"/>
        <source>Check samples</source>
        <translation>Verificar exemplos</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="71"/>
        <source>Check samples / stop check</source>
        <translation>Verificar exemplos / parar verificação</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="84"/>
        <source>Remove obsolete sample sets</source>
        <translation>Remover conjuntos obsoletos de exemplos</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="87"/>
        <source>&amp;Remove obsolete</source>
        <translation>&amp;Remover obsoleto</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="94"/>
        <source>Select game in gamelist when selecting a sample set?</source>
        <translation>Selecionar jogo na lista quando selecionar um conjunto de exemplos?</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="97"/>
        <source>Select &amp;game</source>
        <translation>Selecionar &amp;jogo</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="120"/>
        <source>Close sample check dialog</source>
        <translation>Fecha a janela de verificação de exemplos</translation>
    </message>
    <message>
        <location filename="../../sampcheck.ui" line="123"/>
        <source>C&amp;lose</source>
        <translation>F&amp;echar</translation>
    </message>
</context>
<context>
    <name>SnapshotViewer</name>
    <message>
        <location filename="../../embedderopt.cpp" line="156"/>
        <source>Snapshot viewer</source>
        <translation>Visualizador de snapshots</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="165"/>
        <source>Use as preview</source>
        <translation>Usar como preview</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="171"/>
        <source>Use as title</source>
        <translation>Usar como título</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="180"/>
        <source>Save as...</source>
        <translation>Salvar como...</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="186"/>
        <source>Copy to clipboard</source>
        <translation>Copiar para área de transferência</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="291"/>
        <source>Choose PNG file to store image</source>
        <translation>Escolher arquivo PNG para salvar a imagem</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="291"/>
        <source>PNG images (*.png)</source>
        <translation>Imagens PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../../embedderopt.cpp" line="296"/>
        <source>FATAL: couldn&apos;t save snapshot image to &apos;%1&apos;</source>
        <translation>FATAL: impossível salvar imagem de snapshot para &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SoftwareList</name>
    <message>
        <location filename="../../softwarelist.ui" line="18"/>
        <source>Software list</source>
        <translation>Lista de Software</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="35"/>
        <location filename="../../softwarelist.ui" line="38"/>
        <source>Reload all information</source>
        <translation>Recarregar todas as informações</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="55"/>
        <location filename="../../softwarelist.ui" line="58"/>
        <source>Select a pre-defined device configuration to be added to the software setup</source>
        <translation>Selecionar uma configuração de dispositivo pré definida para ser adicionada à configuração do software</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="62"/>
        <location filename="../../softwarelist.cpp" line="877"/>
        <location filename="../../softwarelist.cpp" line="1377"/>
        <source>No additional devices</source>
        <translation>Nenhum dispositivo adicional</translation>
    </message>
    <message>
        <source>Add the currently selected software and device setup to the favorites list</source>
        <translation type="obsolete">Adicionar software atualmente selecionado e configuração de dispositivo à lista de favoritos</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="109"/>
        <location filename="../../softwarelist.ui" line="112"/>
        <source>Remove the currently selected favorite software configuration</source>
        <translation>Remover a configuração de software favorita atualmente selecionada</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="129"/>
        <location filename="../../softwarelist.ui" line="132"/>
        <source>Play the selected software configuration</source>
        <translation>Executar a configuração de software selecionada</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="149"/>
        <location filename="../../softwarelist.ui" line="152"/>
        <source>Play the selected software configuration (embedded)</source>
        <translation>Executar a configuração de software selecionada (embutida)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="184"/>
        <source>Known software</source>
        <translation>Softwares conhecidos</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="187"/>
        <source>Complete list of known software for the current system</source>
        <translation>Lista completa de softwares conhecidos para o sistema atual</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="275"/>
        <source>View / manage your favorite software list for the current system</source>
        <translation>Ver / gerenciar sua lista de softwares favoritos para o sistema atual</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="368"/>
        <source>Search within the list of known software for the current system</source>
        <translation>procurar na lista de softwares conhecidos para o sistema atual</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="202"/>
        <location filename="../../softwarelist.ui" line="205"/>
        <source>List of known software</source>
        <translation>Lista de softwares conhecidos</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="89"/>
        <location filename="../../softwarelist.ui" line="92"/>
        <source>Add the currently selected software and device setup to the favorites list (or overwrite existing favorite)</source>
        <translation>Adicionar software e configuração de dispositivo atualmente selecionados à lista de favoritos (ou sobrescrevendo os favoritos existente)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="221"/>
        <location filename="../../softwarelist.ui" line="309"/>
        <location filename="../../softwarelist.ui" line="421"/>
        <location filename="../../softwarelist.cpp" line="128"/>
        <location filename="../../softwarelist.cpp" line="147"/>
        <location filename="../../softwarelist.cpp" line="168"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="226"/>
        <location filename="../../softwarelist.ui" line="314"/>
        <location filename="../../softwarelist.ui" line="426"/>
        <location filename="../../softwarelist.cpp" line="130"/>
        <location filename="../../softwarelist.cpp" line="149"/>
        <location filename="../../softwarelist.cpp" line="170"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="231"/>
        <location filename="../../softwarelist.ui" line="319"/>
        <location filename="../../softwarelist.ui" line="431"/>
        <location filename="../../softwarelist.cpp" line="132"/>
        <location filename="../../softwarelist.cpp" line="151"/>
        <location filename="../../softwarelist.cpp" line="172"/>
        <source>Publisher</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="236"/>
        <location filename="../../softwarelist.ui" line="324"/>
        <location filename="../../softwarelist.ui" line="436"/>
        <location filename="../../softwarelist.cpp" line="134"/>
        <location filename="../../softwarelist.cpp" line="153"/>
        <location filename="../../softwarelist.cpp" line="174"/>
        <source>Year</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="241"/>
        <location filename="../../softwarelist.ui" line="329"/>
        <location filename="../../softwarelist.ui" line="441"/>
        <location filename="../../softwarelist.cpp" line="136"/>
        <location filename="../../softwarelist.cpp" line="155"/>
        <location filename="../../softwarelist.cpp" line="176"/>
        <source>Part</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="246"/>
        <location filename="../../softwarelist.ui" line="334"/>
        <location filename="../../softwarelist.ui" line="446"/>
        <location filename="../../softwarelist.cpp" line="138"/>
        <location filename="../../softwarelist.cpp" line="157"/>
        <location filename="../../softwarelist.cpp" line="178"/>
        <source>Interface</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="251"/>
        <location filename="../../softwarelist.ui" line="339"/>
        <location filename="../../softwarelist.ui" line="451"/>
        <location filename="../../softwarelist.cpp" line="140"/>
        <location filename="../../softwarelist.cpp" line="159"/>
        <location filename="../../softwarelist.cpp" line="180"/>
        <source>List</source>
        <translation>Lista</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="272"/>
        <source>Favorites</source>
        <translation>Favoritos</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="290"/>
        <location filename="../../softwarelist.ui" line="293"/>
        <source>Favorite software configurations</source>
        <translation>Configurações de software favoritas</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="344"/>
        <location filename="../../softwarelist.cpp" line="161"/>
        <source>Device configuration</source>
        <translation>Configurações de dispositivo</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="365"/>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="383"/>
        <location filename="../../softwarelist.ui" line="386"/>
        <source>Search for known software (not case-sensitive)</source>
        <translation>Procura por softwares conhecidos (não sensível à caixa)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="402"/>
        <location filename="../../softwarelist.ui" line="405"/>
        <source>Search results</source>
        <translation>Resultados da busca</translation>
    </message>
    <message>
        <location filename="../../softwarelist.ui" line="475"/>
        <source>Loading software-lists, please wait...</source>
        <translation>Carregando listas de softwre, por favor espere...</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="54"/>
        <source>Add the currently selected software to the favorites list</source>
        <translation>Adicionar o software selecionado atualmente para a lista de favoritos</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="61"/>
        <source>Enter search string</source>
        <translation>Entre com o texto de busca</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="88"/>
        <source>Play selected software</source>
        <translation>Jogar software selecionado</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="89"/>
        <source>&amp;Play</source>
        <translation>&amp;Jogar</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="94"/>
        <source>Play selected software (embedded)</source>
        <translation>Jogar software selecionado (embutido)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="95"/>
        <source>Play &amp;embedded</source>
        <translation>Jogar &amp;embutido</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="101"/>
        <source>Add to favorite software list</source>
        <translation>Adicionar para a lista de software favoritos</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="102"/>
        <source>&amp;Add to favorites</source>
        <translation>&amp;Adicionar a favoritos</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="106"/>
        <source>Remove from favorite software list</source>
        <translation>Remover da lista de software favoritos</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="107"/>
        <source>&amp;Remove from favorites</source>
        <translation>&amp;Remover dos favoritos</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="221"/>
        <source>WARNING: software list &apos;%1&apos; not found</source>
        <translation>AVISO: lista de software &apos;%1&apos; não encontrada</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="335"/>
        <source>Known software (%1)</source>
        <translation>Software conhecidos (%1)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="336"/>
        <source>Favorites (%1)</source>
        <translation>Favoritos (%1)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="337"/>
        <source>Search (%1)</source>
        <translation>Busca (%1)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="351"/>
        <location filename="../../softwarelist.cpp" line="545"/>
        <source>Known software (no data available)</source>
        <translation>Software conhecidos (nenhum dado disponível)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="352"/>
        <location filename="../../softwarelist.cpp" line="546"/>
        <source>Favorites (no data available)</source>
        <translation>Favoritos (nenhum dado disponível)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="353"/>
        <location filename="../../softwarelist.cpp" line="547"/>
        <source>Search (no data available)</source>
        <translation>Busca (nenhum dado disponível)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="419"/>
        <source>loading XML software list data from cache</source>
        <translation>carregando lista de software (XML) do cache</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="424"/>
        <source>SWL cache - %p%</source>
        <translation>Cache SWL - %p%</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="452"/>
        <source>done (loading XML software list data from cache, elapsed time = %1)</source>
        <translation>feito (carregando lista de software (XML) do cache, tempo = %1))</translation>
    </message>
    <message>
        <source>done (loading XML software list data from cache, elapsed time = %1</source>
        <translation type="obsolete">feito (carregando lista de software (XML) do cache, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="460"/>
        <source>ERROR: the file name for the MAME software list cache is empty -- please correct this and reload the game list afterwards</source>
        <translation>ERRO: o nome do arquivo para o cache da lista de software do MAME está vazio -- por favor corrija o problema e recarregue a lista de jogos em seguida</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="462"/>
        <source>ERROR: the file name for the MESS software list cache is empty -- please correct this and reload the machine list afterwards</source>
        <translation>ERRO: o nome do arquivo para o cache da lista de software do MESS está vazio -- por favor corrija o problema e recarregue a lista de máquinas a seguir</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="478"/>
        <source>loading XML software list data and (re)creating cache</source>
        <translation>carregando lista de software (XML) do cache e (re)criando cache</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="481"/>
        <source>SWL data - %p%</source>
        <translation>Dados SWL - %p%</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="487"/>
        <source>ERROR: can&apos;t open the MAME software list cache for writing, path = %1 -- please check/correct access permissions and reload the game list afterwards</source>
        <translation>ERROR: impossível abrir o arquivo de cache da lista de software do MAME para escrita. path = %1 -- por favor verifique/corrija as permissões de acesso e recarregue a lista de máquina a seguir</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="489"/>
        <source>ERROR: can&apos;t open the MESS software list cache for writing, path = %1 -- please check/correct access permissions and reload the machine list afterwards</source>
        <translation>ERROR: impossível abrir o arquivo de cache da lista de software do MESS para escrita. path = %1 -- por favor verifique/corrija as permissões de acesso e recarregue a lista de máquina a seguir</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="579"/>
        <source>FATAL: error while parsing XML data for software list &apos;%1&apos;</source>
        <translation>FATAL: erro durante a análise dos dados do XML para a lista de software &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="759"/>
        <source>WARNING: the external process called to load the MAME software lists didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation>AVISO: o processo externo chamado para carregar a lista de software do MAME não terminou corretamente -- código de saída - %1, estado da saída = %2</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="759"/>
        <location filename="../../softwarelist.cpp" line="761"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="759"/>
        <location filename="../../softwarelist.cpp" line="761"/>
        <source>crashed</source>
        <translation>quebrado</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="761"/>
        <source>WARNING: the external process called to load the MESS software lists didn&apos;t exit cleanly -- exitCode = %1, exitStatus = %2</source>
        <translation>AVISO: o processo externo chamado para carregar as listas de software do MESS não terminaram corretamente -- código da saída = %1, estado de saída = %2</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="765"/>
        <source>done (loading XML software list data and (re)creating cache, elapsed time = %1)</source>
        <translation>feito (carregando dados da lista de software (XML) e (re)criando cache, tempo = %1)</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="818"/>
        <source>WARNING: the currently selected MAME emulator doesn&apos;t support software lists</source>
        <translation>AVISO: o emulador MAME selecionado atualmente não suporta lista de software</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="820"/>
        <source>WARNING: the currently selected MESS emulator doesn&apos;t support software lists</source>
        <translation>AVISO: o emulador MESS selecionado atualmente não suporta lista de software</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="837"/>
        <source>WARNING: the external process called to load the MAME software lists caused an error -- processError = %1</source>
        <translation>AVISO: o processo externo chamado para carregar as listas de software do MAME causou um erro -- Erro do Processo = %1</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="839"/>
        <source>WARNING: the external process called to load the MESS software lists caused an error -- processError = %1</source>
        <translation>AVISO: o processo externo chamado para carregar as listas de software do MESS causou um erro -- Erro do Processo = %1</translation>
    </message>
</context>
<context>
    <name>SoftwareSnap</name>
    <message>
        <location filename="../../softwarelist.cpp" line="1585"/>
        <source>Snapshot viewer</source>
        <translation>Visualizador de snapshots</translation>
    </message>
    <message>
        <location filename="../../softwarelist.cpp" line="1598"/>
        <source>Copy to clipboard</source>
        <translation>Copiar para a área de transferência</translation>
    </message>
</context>
<context>
    <name>Title</name>
    <message>
        <location filename="../../title.cpp" line="49"/>
        <source>Copy to clipboard</source>
        <translation>Copiar para área de transferência</translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="56"/>
        <location filename="../../title.cpp" line="57"/>
        <source>Game title image</source>
        <translation>Imagem do título do jogo</translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="59"/>
        <location filename="../../title.cpp" line="60"/>
        <source>Machine title image</source>
        <translation>Imagem de título da máquina</translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="68"/>
        <location filename="../../title.cpp" line="72"/>
        <source>FATAL: can&apos;t open title file, please check access permissions for %1</source>
        <translation>FATAL: impossível abrir arquivo de título. por favor verifique as permissões de acesso para %1</translation>
    </message>
    <message>
        <location filename="../../title.cpp" line="100"/>
        <source>Waiting for data...</source>
        <translation>Aguardando dados...</translation>
    </message>
</context>
<context>
    <name>ToolExecutor</name>
    <message>
        <location filename="../../toolexec.cpp" line="60"/>
        <location filename="../../toolexec.cpp" line="62"/>
        <source>### tool started, output below ###</source>
        <translation>### ferramenta iniciada, saída abaixo ###</translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="62"/>
        <location filename="../../toolexec.cpp" line="81"/>
        <location filename="../../toolexec.cpp" line="128"/>
        <source>tool control: </source>
        <translation>controle da ferramenta: </translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="79"/>
        <location filename="../../toolexec.cpp" line="81"/>
        <source>### tool finished, exit code = %1, exit status = %2 ###</source>
        <translation>### ferramenta finalizada, código de saída = %1, estado de saída = %2 ###</translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="100"/>
        <location filename="../../toolexec.cpp" line="116"/>
        <source>tool output: </source>
        <translation>saída da ferramenta: </translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="100"/>
        <source>stdout: %1</source>
        <translation>saída: %1</translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="116"/>
        <source>stderr: %1</source>
        <translation>erro: %1</translation>
    </message>
    <message>
        <location filename="../../toolexec.cpp" line="126"/>
        <location filename="../../toolexec.cpp" line="128"/>
        <source>### tool error, process error = %1 ###</source>
        <translation>### erro na ferramenta, erro no processo = %1 ###</translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="15"/>
        <source>Executing tool</source>
        <translation>Ferramenta de execução</translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="29"/>
        <source>Command</source>
        <translation>Comando</translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="42"/>
        <source>Executed command</source>
        <translation>Comando executado</translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="91"/>
        <source>Close tool execution dialog</source>
        <translation>Fechar tela da ferramenta de execução</translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="94"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolexec.ui" line="54"/>
        <source>Output from tool</source>
        <translation>Saída da ferramenta</translation>
    </message>
</context>
<context>
    <name>VideoItemWidget</name>
    <message>
        <location filename="../../videoitemwidget.cpp" line="173"/>
        <source>Title:</source>
        <translation>Título:</translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="178"/>
        <location filename="../../videoitemwidget.cpp" line="180"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="178"/>
        <source>Open author URL with the default browser</source>
        <translation>Abrir URL do autor com o navegador padrão</translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="186"/>
        <source>Open video URL with the default browser</source>
        <translation>Abrir URL do vídeo com o navegador padrão</translation>
    </message>
    <message>
        <location filename="../../videoitemwidget.cpp" line="186"/>
        <location filename="../../videoitemwidget.cpp" line="188"/>
        <source>Video:</source>
        <translation>Vídeo:</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../../welcome.cpp" line="33"/>
        <source>SDLMAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="35"/>
        <source>SDLMESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="40"/>
        <source>MAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="42"/>
        <source>MESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="47"/>
        <source>Unsupported emulator</source>
        <translation>Emulador não suportado</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="49"/>
        <source>%1 executable file</source>
        <translation>Arquivo executável %1</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="94"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="94"/>
        <source>The specified file isn&apos;t executable!</source>
        <translation>O arquivo especificado não é executável!</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="109"/>
        <source>Choose emulator executable file</source>
        <translation>Escolha o arquivo executável do emulador</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="109"/>
        <source>All files (*)</source>
        <translation>Todos os arquivos (*)</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="121"/>
        <source>Choose ROM path</source>
        <translation>Escolha o caminho das ROMs</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="134"/>
        <source>Choose sample path</source>
        <translation>Escolha o caminho dos exemplos (samples)</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="147"/>
        <source>Choose hash path</source>
        <translation>Escolha o caminho para os hashes</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="220"/>
        <source>Single-instance check</source>
        <translation>Verificação de Instância Única</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="221"/>
        <source>It appears that another instance of %1 is already running.
However, this can also be the leftover of a previous crash.

Exit now, accept once or ignore completely?</source>
        <translation>Parece que outra instância de %1 já está executando.
No entanto, ela pode ter sido deixada por um crash anterior.

Sair agora, aceitar uma vez ou ignorar completamente?</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="222"/>
        <source>&amp;Exit</source>
        <translation>&amp;Sair</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="222"/>
        <source>&amp;Once</source>
        <translation>&amp;Uma vez</translation>
    </message>
    <message>
        <location filename="../../welcome.cpp" line="222"/>
        <source>&amp;Ignore</source>
        <translation>&amp;Ignorar</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="15"/>
        <source>Welcome to QMC2</source>
        <translation>Bem vindo ao QMC2</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="528"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Welcome to QMC2!&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This appears to be your first start of QMC2 because no valid configuration was found. In order to use QMC2 as a front end for an emulator, you must specify the path to the emulator&apos;s executable file below.&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The paths below the executable file are optional, but you should specify as many of them as you can right now to avoid problems or confusion later (of course, you can change the paths in the emulator&apos;s global configuration at any time later).&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;It&apos;s strongly recommended that you specify the ROM path you are going to use at least!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Bem-vindo ao QMC2!&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Essa parece ser sua primeira execução do QMC2 pois nenhuma configuração válida foi encontrada. Para poder usar o QMC2 como front end para um emulador, você precisa especificar o caminho para o arquivo executável do emulador abaixo.&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Os campos abaixo ao do arquivo executável são opcionais, mas você deveria especificar todos os que você conseguir agora para evitar problemas ou confusão mais tarde (claro que você pode mudar esses caminhos nas configurações globais do emulador a qualquer tempo).&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;É fortemente recomendado que você especifique pelo menos o caminho de onde se encontram as ROMs que você irá usar!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="551"/>
        <source>Emulator executable file</source>
        <translation>Executável do emulador</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="558"/>
        <source>Browse emulator executable file</source>
        <translation>Procurar pelo arquivo executável do emulador</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="575"/>
        <source>Path to ROM images</source>
        <translation>Caminho para as ROMs</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="582"/>
        <source>Browse ROM path</source>
        <translation>Procurar pelo caminho das ROMs</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="599"/>
        <source>Path to samples</source>
        <translation>Caminho para os exemplos</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="606"/>
        <source>Browse sample path</source>
        <translation>Procurar pelo caminho dos exemplos (samples)</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="630"/>
        <source>Emulator executable</source>
        <translation>Executável do emulador</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="637"/>
        <source>ROM path</source>
        <translation>Caminho das ROMs</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="644"/>
        <source>Sample path</source>
        <translation>Caminho dos exemplos</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="651"/>
        <source>Hash path</source>
        <translation>Caminho dos hashes</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="664"/>
        <source>Path to hash files</source>
        <translation>Caminho para os arquivos de hash</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="671"/>
        <source>Browse hash path</source>
        <translation>Procurar pelo caminho dos arquivos de hash</translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="709"/>
        <source>&amp;Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../welcome.ui" line="716"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
</context>
<context>
    <name>YouTubeVideoPlayer</name>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="40"/>
        <source>Attached videos</source>
        <translation>Vídeos anexados</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="74"/>
        <source>Start playing / select next video automatically</source>
        <translation>Reproduzir / selecionar o próximo vídeo automaticamente</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="77"/>
        <source>Play-O-Matic</source>
        <translation>Reprodutor automático</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="93"/>
        <source>Mode:</source>
        <translation>Modo:</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="112"/>
        <source>Choose the video selection mode</source>
        <translation>Escolha o modo de seleção de vídeos</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="116"/>
        <source>sequential</source>
        <translation>sequencial</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="121"/>
        <source>random</source>
        <translation>aleatório</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="138"/>
        <source>Allow videos to be repeated (otherwise stop after last video)</source>
        <translation>Permitir repetição de vídeos (caso contrário parar após o último)</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="141"/>
        <source>Allow repeat</source>
        <translation>Permitir repetição</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="174"/>
        <source>Video player</source>
        <translation>Reprodutor de vídeos</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="196"/>
        <source>Select the preferred video format (automatically falls back to the next available format)</source>
        <translation>Selecione o formato de vídeo preferido (usa o próximo disponível automaticamente)</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="203"/>
        <location filename="../../youtubevideoplayer.cpp" line="79"/>
        <source>FLV 240P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="212"/>
        <location filename="../../youtubevideoplayer.cpp" line="80"/>
        <source>FLV 360P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="221"/>
        <location filename="../../youtubevideoplayer.cpp" line="81"/>
        <source>MP4 360P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="230"/>
        <location filename="../../youtubevideoplayer.cpp" line="82"/>
        <source>FLV 480P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="239"/>
        <location filename="../../youtubevideoplayer.cpp" line="83"/>
        <source>MP4 720P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="248"/>
        <location filename="../../youtubevideoplayer.cpp" line="84"/>
        <source>MP4 1080P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="257"/>
        <location filename="../../youtubevideoplayer.cpp" line="85"/>
        <source>MP4 3072P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="275"/>
        <location filename="../../youtubevideoplayer.cpp" line="146"/>
        <source>Start / pause / resume video playback</source>
        <translation>Reproduzir / pausar / resumir vídeo</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="315"/>
        <source>Remaining playing time</source>
        <translation>Tempo restante</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="334"/>
        <source>Current buffer fill level</source>
        <translation>Nível atual do buffer</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="363"/>
        <source>Search videos</source>
        <translation>Procurar vídeos</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="378"/>
        <source>Search pattern -- use the &apos;hint&apos; button to get a suggestion</source>
        <translation>Padrão de busca -- use o botão &apos;dica&apos; para obter uma sugestão</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="391"/>
        <source>Search YouTube videos using the specified search pattern</source>
        <translation>Buscar vídeos no YouTube usando o padrão de busca especificado</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="435"/>
        <source>Maximum number of results per search request</source>
        <translation>Número máximo de resultados por requisição de busca</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="466"/>
        <source>Start index for the search request</source>
        <translation>Índice inicial para a requisição de busca</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="491"/>
        <source>SI:</source>
        <translation>IB:</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="498"/>
        <source>R:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.ui" line="418"/>
        <source>Suggest a search pattern (hold down for menu)</source>
        <translation>Sugerir um padrão de busca (segure para mostrar o menu)</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="61"/>
        <source>Mute / unmute audio output</source>
        <translation>Tornar mudo / audível a saída de áudio</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="96"/>
        <source>Video progress</source>
        <translation>Progresso do vídeo</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="105"/>
        <location filename="../../youtubevideoplayer.cpp" line="724"/>
        <location filename="../../youtubevideoplayer.cpp" line="748"/>
        <location filename="../../youtubevideoplayer.cpp" line="767"/>
        <location filename="../../youtubevideoplayer.cpp" line="814"/>
        <source>Current buffer fill level: %1%</source>
        <translation>Nível atual do buffer: %1%</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="112"/>
        <location filename="../../youtubevideoplayer.cpp" line="179"/>
        <source>Play this video</source>
        <translation>Reproduzir esse vídeo</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="119"/>
        <location filename="../../youtubevideoplayer.cpp" line="161"/>
        <location filename="../../youtubevideoplayer.cpp" line="190"/>
        <source>Copy video URL</source>
        <translation>Copiar URL do vídeo</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="125"/>
        <location filename="../../youtubevideoplayer.cpp" line="166"/>
        <location filename="../../youtubevideoplayer.cpp" line="195"/>
        <source>Copy author URL</source>
        <translation>Copiar URL do autor</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="131"/>
        <source>Paste video URL</source>
        <translation>Colar URL do vídeo</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="138"/>
        <source>Remove selected videos</source>
        <translation>Remover vídeos selecionados</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="154"/>
        <location filename="../../youtubevideoplayer.cpp" line="1246"/>
        <source>Full screen (return with toggle-key)</source>
        <translation>Tela cheia (retornar com tecla)</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="172"/>
        <location filename="../../youtubevideoplayer.cpp" line="184"/>
        <source>Attach this video</source>
        <translation>Anexar esse vídeo</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="202"/>
        <source>Auto-suggest a search pattern?</source>
        <translation>Sugerir um padrão de busca automaticamente?</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="203"/>
        <source>Auto-suggest</source>
        <translation>Auto-sugerir</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="208"/>
        <source>Enter string to be appended</source>
        <translation>Entre com o texto a ser anexado</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="209"/>
        <source>Append...</source>
        <translation>Anexar...</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="220"/>
        <source>Enter search string</source>
        <translation>Entre com o texto de busca</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="324"/>
        <source>Appended string</source>
        <translation>Anexar texto</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="325"/>
        <source>Enter the string to be appended when suggesting a pattern:</source>
        <translation>Entre com o texto a ser anexado quando sugerir um padrão:</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="587"/>
        <source>video player: a video with the ID &apos;%1&apos; is already attached, ignored</source>
        <translation>reprodutor de vídeo: um vídeo com ID &apos;%1&apos; já está anexado, ignorado</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="797"/>
        <source>video player: playback error: %1</source>
        <translation>reprodutor de vídeo: erro de reprodução: %1</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="946"/>
        <source>video player: video info error: ID = &apos;%1&apos;, status = &apos;%2&apos;, errorCode = &apos;%3&apos;, errorText = &apos;%4&apos;</source>
        <translation>reprodutor de vídeo: erro nas informações do vídeo: ID = &apos;%1&apos;. estado = &apos;%2&apos;, código de erro = &apos;%3&apos;, texto do erro = &apos;%4&apos;</translation>
    </message>
    <message>
        <source>video player: video info error: status = &apos;%1&apos;, errorCode = &apos;%2&apos;, errorText = &apos;%3&apos;</source>
        <translation type="obsolete">reprodutor de vídeo: erro nas informações do vídeo: estado = &apos;%1&apos;, código de erro = &apos;%2&apos;, texto do erro = &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1015"/>
        <source>video player: video info error: timeout occurred</source>
        <translation>reprodutor de vídeo: erro na informação do vídeo: estouro de tempo</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1051"/>
        <source>video player: video info error: %1</source>
        <translation>reprodutor de vídeo: erro na informação do vídeo: %1</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1244"/>
        <source>Full screen (press %1 to return)</source>
        <translation>Tela cheia (pressione %1 para retornar)</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1419"/>
        <source>video player: video image info error: %1</source>
        <translation>reprodutor de vídeo: erro na informação da imagem do vídeo: %1</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1450"/>
        <source>video player: search request error: %1</source>
        <translation>reprodutor de vídeo: erro na requisição de busca: %1</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1478"/>
        <source>video player: search error: can&apos;t parse XML data</source>
        <translation>reprodutor de vídeo: erro na busca: impossível analisar os dados do XML</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1511"/>
        <source>video player: can&apos;t determine the video ID from the reply URL &apos;%1&apos; -- please inform developers</source>
        <translation>reprodutor de vídeo: impossível determinar o ID do vídeo da URL - &apos;%1&apos; -- por favor informe os desenvolvedores</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1517"/>
        <source>video player: can&apos;t associate the returned image for video ID &apos;%1&apos; -- please inform developers</source>
        <translation>reprodutor de vídeo: impossível associar a imagem para o vídeo com ID &apos;%1&apos; -- por favor informe os desenvolvedores</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1539"/>
        <source>video player: can&apos;t save the image for video ID &apos;%1&apos; to the YouTube cache directory &apos;%2&apos; -- please check permissions</source>
        <translation>reprodutor de vídeo: impossível salvar a imagem para o vídeo com ID &apos;%1&apos; para o diretório de cache do YouTube &apos;%2&apos; -- por favor cheque as permissões</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1541"/>
        <source>video player: can&apos;t save the image for video ID &apos;%1&apos;, the YouTube cache directory &apos;%2&apos; does not exist -- please correct</source>
        <translation>reprodutor de vídeo: impossível salvar a imagem para o vídeo com ID &apos;%1&apos; o diretório de cache do YouTube &apos;%2&apos; não existe -- por favor corrija</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1543"/>
        <source>video player: image download failed for video ID &apos;%1&apos;, retrieved image is not valid</source>
        <translation>reprodutor de vídeo: download da imagem para o vídeo com ID &apos;%1&apos; falhou, a imagem não é válida</translation>
    </message>
    <message>
        <location filename="../../youtubevideoplayer.cpp" line="1545"/>
        <source>video player: image download failed for video ID &apos;%1&apos;, error text = &apos;%2&apos;</source>
        <translation>reprodutor de vídeo: download da imagem para o vídeo com ID &apos;%1&apos; falhou, texto do erro = &apos;%2&apos;</translation>
    </message>
</context>
</TS>
