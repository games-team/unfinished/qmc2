greaterThan(QT_MAJOR_VERSION, 3) {
	greaterThan(QT_MINOR_VERSION, 4) {
		# general project settings
		isEmpty(TARGET):TARGET = qmc2
		CONFIG += qtestlib
		QT += xml webkit network
		TEMPLATE = app
		INCLUDEPATH += minizip/
		FORMS += qmc2main.ui \
			options.ui \
			docbrowser.ui \
			about.ui \
			welcome.ui \
			imgcheck.ui \
			sampcheck.ui \
			keyseqscan.ui \
			joyfuncscan.ui \
			toolexec.ui \
			itemselect.ui \
			romalyzer.ui \
			romstatusexport.ui \
			messdevcfg.ui \
			softwarelist.ui \
			direditwidget.ui \
			fileeditwidget.ui \
			floateditwidget.ui \
			comboeditwidget.ui \
			detailsetup.ui \
			miniwebbrowser.ui \
			youtubevideoplayer.ui \
			videoitemwidget.ui \
			mawsqdlsetup.ui \
			embedderopt.ui \
			demomode.ui \
			audioeffects.ui \
			arcade/arcadesetupdialog.ui
		SOURCES += qmc2main.cpp \
			options.cpp \
			docbrowser.cpp \
			about.cpp \
			welcome.cpp \
			imgcheck.cpp \
			sampcheck.cpp \
			keyseqscan.cpp \
			toolexec.cpp \
			itemselect.cpp \
			romalyzer.cpp \
			gamelist.cpp \
			procmgr.cpp \
			preview.cpp \
			flyer.cpp \
			cabinet.cpp \
			controller.cpp \
			marquee.cpp \
			title.cpp \
			pcb.cpp \
			emuopt.cpp \
			joystick.cpp \
			joyfuncscan.cpp \
			romstatusexport.cpp \
			messdevcfg.cpp \
			softwarelist.cpp \
			direditwidget.cpp \
			fileeditwidget.cpp \
			floateditwidget.cpp \
			comboeditwidget.cpp \
			detailsetup.cpp \
			miniwebbrowser.cpp \
			youtubevideoplayer.cpp \
			videoitemwidget.cpp \
			downloaditem.cpp \
			mawsqdlsetup.cpp \
			embedder.cpp \
			embedderopt.cpp \
			demomode.cpp \
			audioeffects.cpp \
			romdbmgr.cpp \
			minizip/ioapi.c \
			minizip/unzip.c \
			minizip/zip.c \
			arcade/arcadeview.cpp \
			arcade/arcadeitem.cpp \
			arcade/arcademenuitem.cpp \
			arcade/arcadescene.cpp \
			arcade/arcademenuscene.cpp \
			arcade/arcadesettings.cpp \
			arcade/arcadescreenshotsaverthread.cpp \
			arcade/arcadesetupdialog.cpp
		HEADERS += qmc2main.h \
			options.h \
			docbrowser.h \
			about.h \
			welcome.h \
			imgcheck.h \
			sampcheck.h \
			keyseqscan.h \
			toolexec.h \
			itemselect.h \
			romalyzer.h \
			gamelist.h \
			procmgr.h \
			preview.h \
			flyer.h \
			cabinet.h \
			controller.h \
			marquee.h \
			title.h \
			pcb.h \
			emuopt.h \
			joystick.h \
			joyfuncscan.h \
			romstatusexport.h \
			messdevcfg.h \
			softwarelist.h \
			direditwidget.h \
			fileeditwidget.h \
			filesystemmodel.h \
			floateditwidget.h \
			comboeditwidget.h \
			detailsetup.h \
			miniwebbrowser.h \
			youtubevideoplayer.h \
			videoitemwidget.h \
			downloaditem.h \
			mawsqdlsetup.h \
			embedder.h \
			embedderopt.h \
			demomode.h \
			audioeffects.h \
			romdbmgr.h \
			macros.h \
			minizip/ioapi.h \
			minizip/unzip.h \
			minizip/zip.h \
			arcade/arcadeview.h \
			arcade/arcadeitem.h \
			arcade/arcademenuitem.h \
			arcade/arcadescene.h \
			arcade/arcademenuscene.h \
			arcade/arcadesettings.h \
			arcade/arcadescreenshotsaverthread.h \
			arcade/arcadesetupdialog.h
		PRECOMPILED_HEADER = qmc2_prefix.h
		TRANSLATIONS += data/lng/qmc2_us.ts \
			data/lng/qmc2_de.ts \
			data/lng/qmc2_pl.ts \
			data/lng/qmc2_fr.ts \
			data/lng/qmc2_pt.ts
		RESOURCES += qmc2.qrc
		QMAKE_MAKEFILE = Makefile.qmake

		unix {
			# produce pretty (silent) compile output (only when the Qt version is less than 4.7)
			greaterThan(QMC2_PRETTY_COMPILE, 0) { 
				!greaterThan(QT_MAJOR_VERSION, 4) {
				        !greaterThan(QT_MINOR_VERSION, 6) {
						!isEmpty(QMAKE_CXX):QMAKE_CXX = @echo [C++ ] $< && $$QMAKE_CXX
						!isEmpty(QMAKE_CC):QMAKE_CC = @echo [CC\\ \\ ] $< && $$QMAKE_CC
						!isEmpty(QMAKE_LINK):QMAKE_LINK = @echo [LINK] $@ && $$QMAKE_LINK
						!isEmpty(QMAKE_MOC):QMAKE_MOC = @echo [MOC ] `echo $@ | sed -e \'s/moc_//g\' | sed -e \'s/.cpp/.h/g\'` && $$QMAKE_MOC
						!isEmpty(QMAKE_UIC):QMAKE_UIC = @echo [UIC ] $< && $$QMAKE_UIC
						!isEmpty(QMAKE_RCC):QMAKE_RCC = @echo [RCC ] $< && $$QMAKE_RCC
					}
				}
			}
		}

		# platform specific stuff
		macx {
			OBJECTIVE_SOURCES += SDLMain_tmpl.m
			HEADERS += SDLMain_tmpl.h
			LIBS += -framework SDL -framework Cocoa -lz
			greaterThan(QMC2_MAC_UNIVERSAL, 0) {
				CONFIG += x86 ppc
			}
			QMAKE_INFO_PLIST = macx/Info.plist
		} else {
			!win32 {
				LIBS += -lSDL -lz -lX11
			}
		}
		win32 {
			SOURCES += windows_tools.cpp
			DEFINES += PSAPI_VERSION=1
			# use VC++ (default) / MinGW
			greaterThan(QMC2_MINGW, 0) {
				CONFIG += windows
				DEFINES += QMC2_MINGW
				QMAKE_LIBS_QT_ENTRY =
				QMAKE_LFLAGS_CONSOLE =
				LIBS += -lSDL -lSDLmain -lSDL.dll -lz -lpsapi $$quote($$QMC2_LIBS)
				INCLUDEPATH += $$quote($$QMC2_INCLUDEPATH)
				contains(TARGET, qmc2-mame):RC_FILE = qmc2-mame.rc
				contains(TARGET, qmc2-mess):RC_FILE = qmc2-mess.rc
			} else {
				CONFIG += embed_manifest_exe windows
				LIBS += psapi.lib
				contains(TARGET, qmc2-mame):RC_FILE = qmc2-mame.rc
				contains(TARGET, qmc2-mess):RC_FILE = qmc2-mess.rc
			}
		}
	} else {
		error(Qt $$QT_VERSION is insufficient -- Qt 4.5.0+ required)
	}
} else {
	error(Qt $$QT_VERSION is insufficient -- Qt 4.5.0+ required)
}
